# encoding: utf-8
# Under certain circumstances NOT NULL constraints need to be added to
# existing db columns. When we find ourselves in that position, we need sane
# values to set for existing records that have NULL values for newly
# constrained columns.
module DatabaseDefaultValues
  MISSING_NOT_NULL_PRICE = -1.0
  MISSING_NOT_NULL_MILES = 0
  MISSING_NOT_NULL_STRING = '<missing>'.freeze
end
