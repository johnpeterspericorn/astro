# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180330115518) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_types", force: :cascade do |t|
    t.text     "action_type_name"
    t.integer  "type_id"
    t.datetime "date_sent"
    t.string   "status"
    t.text     "emails"
    t.text     "parameters"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "agreement_no"
    t.datetime "email_viewed_at"
  end

  create_table "pawn_access_number_blacklists", force: :cascade do |t|
    t.string   "pawn_access_no",                        null: false
    t.text     "notes",                    default: "",    null: false
    t.date     "expiration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ineligible_for_promise", default: false, null: false
  end

  create_table "pawn_information", force: :cascade do |t|
    t.integer  "promise_offer_id"
    t.string   "pawn_access_no"
    t.string   "badge_no"
    t.float    "buy_fee"
    t.string   "agreement_no"
    t.string   "lane_no",                null: false
    t.string   "run_no",                 null: false
    t.string   "location_initials"
    t.string   "universal_no"
    t.float    "vehicle_purchase_price", null: false
    t.string   "work_order_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sale_no",                null: false
    t.string   "seller_no"
    t.integer  "sblu"
  end

  add_index "pawn_information", ["pawn_access_no"], name: "index_pawn_information_on_pawn_access_no", using: :btree
  add_index "pawn_information", ["agreement_no"], name: "index_pawn_information_on_agreement_no", using: :btree
  add_index "pawn_information", ["promise_offer_id"], name: "index_pawn_information_on_promise_offer_id", using: :btree

  create_table "pawn_locations", force: :cascade do |t|
    t.string   "initials",                                                 null: false
    t.string   "name",                                                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "return_emails"
    t.boolean  "inspections_enabled", default: false
    t.boolean  "accepts_returns",     default: true
    t.boolean  "purchases_enabled",   default: true
    t.text     "return_instructions", default: ""
    t.boolean  "g2g_converted",       default: false,                      null: false
    t.boolean  "bsc_location",        default: false,                      null: false
    t.string   "country",             default: "United States of America", null: false
    t.string   "partner_network",     default: "US-Astroheim",               null: false
    t.float    "tax_rate"
    t.string   "accounting_email"
  end

  create_table "audit_reasons", force: :cascade do |t|
    t.integer  "audit_id",    null: false
    t.text     "description", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "audits", force: :cascade do |t|
    t.integer  "auditee_id",                      null: false
    t.string   "auditee_type",                    null: false
    t.string   "state",        default: "opened", null: false
    t.date     "resolved_by",                     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "emails",       default: "",       null: false
  end

  create_table "batch_coupons", id: false, force: :cascade do |t|
    t.integer  "coupon_id",       null: false
    t.integer  "coupon_batch_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "batch_coupons", ["coupon_id"], name: "index_batch_coupons_on_coupon_id", unique: true, using: :btree

  create_table "bill_of_sales", force: :cascade do |t|
    t.integer  "promise_purchase_id"
    t.integer  "return_invoice_id"
    t.string   "vra_no"
    t.string   "vnum"
    t.string   "make"
    t.string   "model"
    t.integer  "year"
    t.float    "price"
    t.float    "tax"
    t.float    "fees"
    t.integer  "odometer_reading"
    t.string   "seller_5mil"
    t.text     "seller_agreement_name"
    t.text     "seller_address"
    t.string   "seller_postal_code"
    t.string   "seller_city"
    t.string   "seller_phone"
    t.string   "seller_fax"
    t.text     "selling_sales_person_name"
    t.text     "selling_sales_person_signature"
    t.text     "selling_sales_person_registration_no"
    t.text     "selling_hst_registration_no"
    t.text     "selling_qst_registration_no"
    t.text     "buying_sales_person_name"
    t.text     "buying_sales_person_signature"
    t.text     "buying_sales_person_registration_no"
    t.text     "buying_hst_registration_no"
    t.text     "buying_qst_registration_no"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "buyer_information", force: :cascade do |t|
    t.string   "pawn_access_no",                 null: false
    t.string   "name"
    t.text     "emails"
    t.string   "cell_phone"
    t.string   "office_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "send_updates",      default: false, null: false
  end

  add_index "buyer_information", ["pawn_access_no"], name: "index_buyer_information_on_pawn_access_no", unique: true, using: :btree

  create_table "cancelled_purchases", force: :cascade do |t|
    t.string   "buyer_email"
    t.string   "pawn_access_no"
    t.string   "agreement_no"
    t.string   "universal_no"
    t.string   "vnum"
    t.integer  "promise_days_selected"
    t.integer  "promise_miles_selected"
    t.boolean  "promise_if_bid"
    t.boolean  "promise_seller_paid"
    t.boolean  "automatic_purchase"
    t.float    "vehicle_purchase_price"
    t.float    "promise_price"
    t.float    "inspection_price"
    t.string   "transaction_id"
    t.datetime "purchased_at"
    t.string   "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promise_purchase_id"
    t.string   "location_initials"
    t.integer  "purchasing_user_id"
    t.integer  "cancelling_user_id"
    t.float    "buy_fee"
    t.string   "work_order_number"
    t.boolean  "charge_failed"
    t.integer  "promise_offer_id"
    t.string   "promise_kind"
    t.integer  "promise_additional_days"
  end

  add_index "cancelled_purchases", ["promise_purchase_id"], name: "index_cancelled_purchases_on_promise_purchase_id", unique: true, using: :btree

  create_table "charge_code_mappings", force: :cascade do |t|
    t.string  "product",            null: false
    t.boolean "automatic_purchase", null: false
    t.boolean "seller_paid",        null: false
    t.integer "charge_code",        null: false
    t.string  "call_id",            null: false
    t.boolean "percent_coverage",   null: false
    t.boolean "limited_volume",     null: false
  end

  create_table "coupon_batches", force: :cascade do |t|
    t.text     "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "coupons", force: :cascade do |t|
    t.datetime "expiration_date",                                                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code",                  limit: 6,                                      null: false
    t.float    "amount",                                                               null: false
    t.integer  "promise_purchase_id"
    t.string   "country",                         default: "United States of America", null: false
  end

  add_index "coupons", ["code"], name: "index_coupons_on_code", using: :btree
  add_index "coupons", ["promise_purchase_id"], name: "index_coupons_on_promise_purchase_id", using: :btree

  create_table "dashboard_scores", force: :cascade do |t|
    t.integer "buyer_num",              null: false
    t.string  "payment_kind",           null: false
    t.float   "avg_loss"
    t.float   "return_rate"
    t.float   "volume"
    t.float   "margin"
    t.float   "earnings"
    t.integer "return_score"
    t.integer "purchase_quality"
    t.integer "margin_score"
    t.integer "volume_score"
    t.integer "earnings_score"
    t.date    "date"
    t.string  "group_name"
    t.float   "group_avg_loss"
    t.float   "group_return_rate"
    t.float   "group_volume"
    t.float   "group_margin"
    t.float   "group_earnings"
    t.integer "group_return_score"
    t.integer "group_purchase_quality"
    t.integer "group_margin_score"
    t.integer "group_volume_score"
    t.integer "group_earnings_score"
  end

  add_index "dashboard_scores", ["buyer_num"], name: "index_dashboard_scores_on_buyer_num", using: :btree

  create_table "agreement_dashboard_load", force: :cascade do |t|
    t.string  "agreement_no"
    t.integer "ds_360_rate"
    t.text    "agreementship_name"
    t.float   "return_percent_past60"
    t.float   "return_score_past60"
    t.float   "avg_loss_per_vehicle_past60"
    t.float   "buyer_score_past60"
    t.float   "margin_past60"
    t.float   "overall_score_past60"
    t.float   "return_percent_past90"
    t.float   "return_score_past90"
    t.float   "avg_loss_per_vehicle_past90"
    t.float   "buyer_score_past90"
    t.float   "margin_past90"
    t.float   "overall_score_past90"
    t.float   "return_percent_pastYTD"
    t.float   "return_score_pastYTD"
    t.float   "avg_loss_per_vehicle_pastYTD"
    t.float   "buyer_score_pastYTD"
    t.float   "margin_pastYTD"
    t.float   "overall_score_pastYTD"
    t.float   "return_percent_past12"
    t.float   "return_score_past12"
    t.float   "avg_loss_per_vehicle_past12"
    t.float   "buyer_score_past12"
    t.float   "margin_past12"
    t.float   "overall_score_past12"
  end

  create_table "agreement_floor_plan_branch_locks", force: :cascade do |t|
    t.string   "agreement_no"
    t.string   "company_code"
    t.string   "site_no"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "agreement_information", force: :cascade do |t|
    t.string   "agreement_no",       null: false
    t.string   "agreementship_name"
    t.string   "address_street"
    t.string   "address_suite"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_zipcode"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
  end

  add_index "agreement_information", ["agreement_no"], name: "index_agreement_information_on_agreement_no", unique: true, using: :btree

  create_table "agreement_risk_factors", force: :cascade do |t|
    t.string   "agreement_no",      null: false
    t.integer  "risk_factor_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agreement_risk_factors", ["agreement_no"], name: "index_agreement_risk_factors_on_agreement_no", unique: true, using: :btree
  add_index "agreement_risk_factors", ["risk_factor_id"], name: "index_agreement_risk_factors_on_risk_factor_id", using: :btree

  create_table "agreementship_blacklists", force: :cascade do |t|
    t.string   "agreement_no"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "expiration_date"
    t.text     "notes",                    default: "",    null: false
    t.boolean  "ineligible_for_promise", default: false, null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0
    t.integer  "attempts",   default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "discounts", force: :cascade do |t|
    t.float    "amount",             default: 0.0, null: false
    t.text     "description",        default: "",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promise_offer_id",               null: false
  end

  create_table "email_tracking", force: :cascade do |t|
    t.string   "unique_code"
    t.integer  "action_type_id"
    t.string   "email"
    t.string   "ip_address"
    t.datetime "letter_sent"
    t.datetime "letter_opened"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "external_vehicles_batches", force: :cascade do |t|
    t.string   "status"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "external_vehicles_documents", force: :cascade do |t|
    t.string   "document"
    t.integer  "external_vehicles_batch_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "external_vehicles_loads", force: :cascade do |t|
    t.string   "model"
    t.integer  "odometer_reading"
    t.integer  "year"
    t.string   "make",                                                      null: false
    t.string   "vnum",                                                       null: false
    t.string   "lane_no",                                                   null: false
    t.string   "run_no",                                                    null: false
    t.string   "universal_no"
    t.string   "badge_no"
    t.string   "agreement_no"
    t.float    "promise_250"
    t.float    "additional_day"
    t.float    "promise_500"
    t.float    "vehicle_purchase_price",                                    null: false
    t.integer  "odometer_rating"
    t.integer  "autocheck_rating"
    t.integer  "mmr_rating"
    t.integer  "age_rating"
    t.integer  "seller_rating"
    t.integer  "mid_rating"
    t.integer  "bid_risk_rating"
    t.integer  "ineligibility_condition"
    t.boolean  "if_bid"
    t.string   "work_order_number"
    t.string   "location_initials"
    t.boolean  "ove"
    t.boolean  "presale_inspection"
    t.float    "buy_fee"
    t.boolean  "promise_prepaid"
    t.float    "inspection_only_price"
    t.float    "inspection_bundle_price"
    t.boolean  "automatic_purchase"
    t.integer  "preselected_promise_days"
    t.integer  "preselected_promise_miles"
    t.string   "automatic_purchase_email"
    t.boolean  "automatic_purchase_processed", default: false,              null: false
    t.integer  "vehicle_code"
    t.integer  "promise_price_threshold"
    t.boolean  "past_time"
    t.boolean  "seller_paid",                  default: false
    t.string   "sale_no",                                                   null: false
    t.string   "seller_no"
    t.integer  "sblu"
    t.boolean  "bypass_charge",                default: false,              null: false
    t.datetime "purchased_at",                                              null: false
    t.integer  "pending_reason_code"
    t.float    "transport_reimbursement",      default: 0.0,                null: false
    t.string   "channel",                      default: "unknown",          null: false
    t.datetime "must_be_promised_at"
    t.float    "discount",                     default: 0.0,                null: false
    t.text     "discount_description",         default: "",                 null: false
    t.string   "preselected_promise_kind",   default: "purchase_protect", null: false
    t.string   "partner"
    t.integer  "percent_coverage"
    t.string   "limited_volume"
    t.string   "group_code"
    t.string   "group"
    t.string   "source_document_link"
    t.string   "batch_id"
    t.integer  "user_id"
    t.string   "status"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.text     "rejection_comment"
    t.integer  "vehicle_total"
    t.integer  "external_vehicles_batch_id"
    t.integer  "approvnumg_user_id"
    t.integer  "rejecting_user_id"
    t.string   "seller_name"
  end

  create_table "failovers", force: :cascade do |t|
    t.datetime "began_at"
    t.datetime "ended_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flat_rate_inquiries", force: :cascade do |t|
    t.integer  "user_id",                           null: false
    t.string   "pawn_access_no",                 null: false
    t.string   "agreement_name"
    t.string   "agreement_phone"
    t.string   "agreement_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "agreement_no",                         null: false
    t.float    "target_price"
    t.integer  "miles_selected",                    null: false
    t.boolean  "psi_eligible",      default: false, null: false
    t.boolean  "palv"
  end

  create_table "flat_rate_offer_batches", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.string   "pawn_access_no", null: false
    t.string   "user_email"
    t.date     "expiration_date"
    t.string   "sales_rep_email"
  end

  create_table "flat_rate_offers", force: :cascade do |t|
    t.integer  "flat_rate_id",                                                  null: false
    t.float    "price",                                                         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "flat_rate_offer_batch_id",                                      null: false
    t.string   "agreement_no"
    t.integer  "offering_user_id",                                              null: false
    t.integer  "accepting_user_id"
    t.datetime "accepted_at"
    t.string   "state",                                                         null: false
    t.boolean  "psi_eligible",             default: false,                      null: false
    t.integer  "miles_selected",                                                null: false
    t.string   "country",                  default: "United States of America", null: false
    t.boolean  "palv"
    t.float    "target_price"
    t.float    "target_price_for_miles"
  end

  add_index "flat_rate_offers", ["agreement_no"], name: "index_flat_rate_offers_on_agreement_no", using: :btree

  create_table "flat_rates", force: :cascade do |t|
    t.string   "agreement_no",                                                        null: false
    t.float    "target_price_360",                                                 null: false
    t.float    "inspection_adjustment_price",                                      null: false
    t.float    "days_adjustment_price",                                            null: false
    t.float    "miles_adjustment_price",                                           null: false
    t.integer  "ineligibility_condition"
    t.boolean  "target_customer",             default: false,                      null: false
    t.boolean  "major_customer",              default: false,                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "target_price_360_psi",                                             null: false
    t.float    "target_price_500",                                                 null: false
    t.float    "target_price_500_psi",                                             null: false
    t.string   "country",                     default: "United States of America", null: false
    t.string   "lv_tier"
    t.float    "target_price_lv"
    t.boolean  "closed_factory_exclusion",    default: false,                      null: false
  end

  create_table "floor_plan_branch_selections", force: :cascade do |t|
    t.string   "company_code",      null: false
    t.string   "site_no",           null: false
    t.string   "agreement_no",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "return_invoice_id"
  end

  create_table "floor_plan_branches", force: :cascade do |t|
    t.string   "site_no",                        null: false
    t.string   "name"
    t.string   "company_code",                   null: false
    t.string   "address_street"
    t.string   "address_suite"
    t.string   "address_city"
    t.string   "address_state"
    t.string   "address_zipcode"
    t.string   "country_code",    default: "us", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "shipment_name"
    t.string   "oracle_site"
    t.boolean  "active",          default: true, null: false
    t.string   "phone_number"
  end

  add_index "floor_plan_branches", ["site_no", "company_code"], name: "index_floor_plan_branches_on_site_no_and_company_code", unique: true, using: :btree

  create_table "floor_plan_companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "company_code",                           null: false
    t.boolean  "holds_title",             default: true, null: false
    t.boolean  "generate_shipping_label", default: true, null: false
    t.integer  "oracle_id"
    t.boolean  "skip_ds_payment"
  end

  create_table "promise_extensions", force: :cascade do |t|
    t.string   "vnum",                                         null: false
    t.integer  "additional_days"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "reason",          limit: 255
    t.string   "status",          limit: 30,  default: "new"
  end

  create_table "promise_offers", force: :cascade do |t|
    t.integer  "days_selected"
    t.integer  "miles_selected"
    t.float    "promise_price"
    t.string   "promise_status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "emails"
    t.datetime "promised_at"
    t.float    "promise_250"
    t.float    "promise_500"
    t.float    "additional_day"
    t.integer  "ineligibility_condition"
    t.boolean  "if_bid"
    t.boolean  "left_lot",                   default: false,              null: false
    t.boolean  "automatic_purchase"
    t.integer  "promise_price_threshold"
    t.boolean  "seller_paid"
    t.boolean  "paid_by_floor_plan",         default: false,              null: false
    t.boolean  "approved",                   default: false
    t.boolean  "bypass_charge",              default: false,              null: false
    t.datetime "purchased_at"
    t.datetime "invalidated_at"
    t.datetime "approved_at"
    t.integer  "changing_user_id"
    t.integer  "pending_reason_code"
    t.float    "transport_reimbursement",    default: 0.0,                null: false
    t.datetime "must_be_promised_at"
    t.string   "preselected_promise_kind", default: "purchase_protect", null: false
    t.string   "partner"
    t.integer  "percent_coverage"
    t.string   "limited_volume"
    t.string   "group_code"
    t.string   "group"
    t.string   "source_document_link"
    t.integer  "consignment_id"
  end

  create_table "promise_purchase_versions", force: :cascade do |t|
    t.string   "item_type",      limit: 255,  null: false
    t.integer  "item_id",                     null: false
    t.string   "event",          limit: 255,  null: false
    t.string   "whodunnit",      limit: 255
    t.string   "object",         limit: 2000
    t.string   "object_changes", limit: 2000
    t.datetime "created_at"
  end

  add_index "promise_purchase_versions", ["item_type", "item_id"], name: "index_promise_purchase_versions_on_item_type_and_item_id", using: :btree

  create_table "promise_purchases", force: :cascade do |t|
    t.integer  "miles_selected"
    t.integer  "additional_days"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promise_offer_id"
    t.string   "transaction_id"
    t.integer  "purchasing_user_id"
    t.datetime "promised_at",                                               null: false
    t.string   "kind",                           default: "purchase_protect", null: false
    t.string   "channel",                                                     null: false
    t.string   "partner"
    t.boolean  "expire_promise"
    t.string   "order_number"
    t.integer  "extension_days"
    t.string   "extension_reason",   limit: 255
  end

  add_index "promise_purchases", ["promise_offer_id"], name: "index_promise_purchases_on_promise_offer_id", unique: true, using: :btree

  create_table "how_paid_information", force: :cascade do |t|
    t.text     "payment_type", null: false
    t.string   "vnum",          null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "ineligibility_conditions", force: :cascade do |t|
    t.integer  "condition_id"
    t.string   "notice"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inspection_information", force: :cascade do |t|
    t.integer  "promise_offer_id"
    t.boolean  "presale_inspection"
    t.float    "inspection_only_price"
    t.float    "inspection_bundle_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "inspection_information", ["promise_offer_id"], name: "index_inspection_information_on_promise_offer_id", using: :btree

  create_table "inspection_purchases", force: :cascade do |t|
    t.integer  "promise_offer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "price"
    t.string   "transaction_id"
    t.string   "kind",               default: "purchase_protect", null: false
  end

  add_index "inspection_purchases", ["promise_offer_id"], name: "index_inspection_purchases_on_promise_offer_id", using: :btree

  create_table "market_factors", force: :cascade do |t|
    t.string   "title",           limit: 100
    t.text     "description"
    t.string   "color",           limit: 10
    t.integer  "market_index_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "market_indices", force: :cascade do |t|
    t.text     "notes"
    t.integer  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "network_plus_offers", force: :cascade do |t|
    t.integer  "offering_user_id",                       null: false
    t.integer  "accepting_user_id"
    t.text     "emails",                                 null: false
    t.string   "state"
    t.string   "agreement_no"
    t.float    "existing_network_rate",                  null: false
    t.float    "network_plus_surcharge",                 null: false
    t.datetime "expiration_date",                        null: false
    t.boolean  "accepted",               default: false, null: false
    t.datetime "accepted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "letter_sent"
  end

  create_table "new_product_fee_versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "new_product_fee_versions", ["item_type", "item_id"], name: "index_new_product_fee_versions_on_item_type_and_item_id", using: :btree

  create_table "new_product_fees", force: :cascade do |t|
    t.string   "agreement_no"
    t.string   "seller_no"
    t.boolean  "seller_paid", default: false,  null: false
    t.integer  "timeframe",   default: 365,    null: false
    t.integer  "count",       default: 999999, null: false
    t.float    "fee",         default: 0.0,    null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "payment_adjustment_fees", force: :cascade do |t|
    t.string   "item_description"
    t.float    "fee_amount"
    t.string   "adjustment_code"
    t.integer  "payment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_responses", force: :cascade do |t|
    t.integer  "promise_purchase_id", null: false
    t.integer  "payment_id"
    t.string   "error_message"
    t.text     "raw_response",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "json_response"
  end

  add_index "payment_responses", ["payment_id"], name: "index_payment_responses_on_payment_id", unique: true, using: :btree

  create_table "payments", force: :cascade do |t|
    t.string   "order_id",           null: false
    t.string   "authorization_code", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "credit_card_type"
    t.integer  "credit_card_number"
    t.float    "amount"
    t.float    "promise_price"
    t.float    "return_fee"
    t.string   "address_zip"
    t.string   "address_city"
    t.string   "address_state"
    t.text     "address_street"
    t.string   "address_country"
  end

  create_table "pending_reasons", force: :cascade do |t|
    t.integer  "code",       null: false
    t.string   "notice",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pending_reasons", ["code"], name: "index_pending_reasons_on_code", unique: true, using: :btree

  create_table "performance_alerts", force: :cascade do |t|
    t.integer  "current_user_id",                         null: false
    t.text     "emails",                                  null: false
    t.integer  "relationship_manager_id"
    t.string   "agreement_no"
    t.boolean  "return_rate",             default: false, null: false
    t.boolean  "lpc",                     default: false, null: false
    t.boolean  "letter_sent",             default: false, null: false
    t.datetime "follow_up_date",                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree

  create_table "presale_vehicles", force: :cascade do |t|
    t.string  "vnum"
    t.integer "odometer_reading"
    t.integer "year"
    t.string  "model"
    t.string  "make"
    t.integer "average_valuation"
    t.float   "vnum_risk"
    t.integer "odometer_rating"
    t.integer "autocheck_rating"
    t.integer "mmr_rating"
    t.integer "age_rating"
    t.integer "seller_rating"
    t.integer "mid_rating"
    t.float   "promise_250_medium"
    t.string  "lane_no"
    t.string  "run_no"
    t.integer "ineligibility_condition"
    t.string  "location_initials"
    t.boolean "ove"
    t.boolean "promise_prepaid"
    t.boolean "automatic_purchase"
    t.integer "vehicle_code"
    t.integer "promise_price_threshold"
    t.boolean "seller_paid",               default: false
    t.float   "promise_250_low"
    t.float   "promise_250_high"
    t.integer "low_valuation"
    t.integer "high_valuation"
    t.string  "seller_no"
  end

  add_index "presale_vehicles", ["location_initials"], name: "index_presale_vehicles_on_location_initials", using: :btree
  add_index "presale_vehicles", ["vnum"], name: "index_presale_vehicles_on_vnum", using: :btree

  create_table "presale_vehicles_load", force: :cascade do |t|
    t.string  "vnum"
    t.integer "odometer_reading"
    t.integer "year"
    t.string  "model"
    t.string  "make"
    t.integer "average_valuation"
    t.float   "vnum_risk"
    t.integer "odometer_rating"
    t.integer "autocheck_rating"
    t.integer "mmr_rating"
    t.integer "age_rating"
    t.integer "seller_rating"
    t.integer "mid_rating"
    t.float   "promise_250_medium"
    t.string  "lane_no"
    t.string  "run_no"
    t.integer "ineligibility_condition"
    t.string  "location_initials"
    t.boolean "ove"
    t.boolean "promise_prepaid"
    t.boolean "automatic_purchase"
    t.integer "vehicle_code"
    t.integer "promise_price_threshold"
    t.boolean "seller_paid",               default: false
    t.float   "promise_250_low"
    t.float   "promise_250_high"
    t.integer "low_valuation"
    t.integer "high_valuation"
    t.string  "seller_no"
  end

  create_table "price_factors", force: :cascade do |t|
    t.integer  "promise_offer_id"
    t.integer  "age_rating"
    t.integer  "bid_risk_rating"
    t.integer  "autocheck_rating"
    t.integer  "mid_rating"
    t.integer  "mmr_rating"
    t.integer  "odometer_rating"
    t.integer  "seller_rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "price_factors", ["promise_offer_id"], name: "index_price_factors_on_promise_offer_id", using: :btree

  create_table "price_ranges", force: :cascade do |t|
    t.integer  "risk_factor_id", null: false
    t.integer  "vehicle_code",   null: false
    t.float    "lower_limit",    null: false
    t.float    "upper_limit",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "price_ranges", ["risk_factor_id", "vehicle_code"], name: "index_price_ranges_on_risk_factor_id_and_vehicle_code", unique: true, using: :btree

  create_table "pricing_distributions", force: :cascade do |t|
    t.integer  "risk_factor_id",      null: false
    t.integer  "vehicle_category_id", null: false
    t.float    "low_price",           null: false
    t.float    "median_price",        null: false
    t.float    "high_price",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rate_changes", force: :cascade do |t|
    t.integer  "rate_changing_user_id"
    t.integer  "link_clicked_user_id"
    t.text     "emails",                  null: false
    t.integer  "relationship_manager_id"
    t.string   "agreement_no"
    t.float    "return_rate"
    t.datetime "effective_date"
    t.datetime "letter_sent"
    t.datetime "link_clicked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relationship_managers", force: :cascade do |t|
    t.string   "name"
    t.text     "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "return_invoices", force: :cascade do |t|
    t.integer  "promise_purchase_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "odometer_on_return"
    t.integer  "pawn_location_id"
    t.boolean  "mechanical_issues"
    t.boolean  "cosmetic_issues"
    t.boolean  "unable_to_sell"
    t.boolean  "changed_mind"
    t.boolean  "other"
    t.boolean  "no_answer"
    t.string   "user_email"
    t.integer  "title_status"
    t.integer  "returning_user_id"
    t.string   "additional_information"
    t.integer  "shipment_id"
    t.float    "estimated_cost",          default: 0.0,   null: false
    t.boolean  "vehicle_received",        default: false
    t.boolean  "title_received",          default: false
    t.boolean  "refund_processed",        default: false
    t.datetime "cancelled_at"
    t.date     "expiration_date"
    t.text     "reason"
    t.integer  "cancelling_user_id"
    t.float    "new_product_fee"
    t.boolean  "left_lot"
    t.string   "slug"
    t.boolean  "title_notification_sent", default: false
  end

  add_index "return_invoices", ["cancelled_at"], name: "index_return_invoices_on_cancelled_at", using: :btree
  add_index "return_invoices", ["promise_purchase_id"], name: "index_return_invoices_on_promise_purchase_id", using: :btree

  create_table "risk_factors", force: :cascade do |t|
    t.float    "risk_modifier", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "risk_factors", ["risk_modifier"], name: "index_risk_factors_on_risk_modifier", unique: true, using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "shipments", force: :cascade do |t|
    t.string   "easypost_shipment_id", null: false
    t.string   "tracking_code",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.string   "label_url"
    t.string   "carrier"
  end

  create_table "sold_vehicles", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "model"
    t.integer  "odometer_reading"
    t.integer  "year"
    t.string   "make",                                                      null: false
    t.string   "vnum",                                                       null: false
    t.string   "lane_no",                                                   null: false
    t.string   "run_no",                                                    null: false
    t.string   "universal_no"
    t.string   "pawn_access_no"
    t.string   "badge_no"
    t.string   "agreement_no"
    t.float    "promise_250"
    t.float    "additional_day"
    t.float    "promise_500"
    t.float    "vehicle_purchase_price",                                    null: false
    t.integer  "odometer_rating"
    t.integer  "autocheck_rating"
    t.integer  "mmr_rating"
    t.integer  "age_rating"
    t.integer  "seller_rating"
    t.integer  "mid_rating"
    t.integer  "bid_risk_rating"
    t.integer  "ineligibility_condition"
    t.boolean  "if_bid"
    t.string   "work_order_number"
    t.string   "location_initials"
    t.boolean  "ove"
    t.boolean  "presale_inspection"
    t.float    "buy_fee"
    t.boolean  "promise_prepaid"
    t.float    "inspection_only_price"
    t.float    "inspection_bundle_price"
    t.boolean  "automatic_purchase"
    t.integer  "preselected_promise_days"
    t.integer  "preselected_promise_miles"
    t.string   "automatic_purchase_email"
    t.boolean  "automatic_purchase_processed", default: false,              null: false
    t.integer  "vehicle_code"
    t.integer  "promise_price_threshold"
    t.boolean  "seller_paid",                  default: false
    t.string   "sale_no",                                                   null: false
    t.string   "seller_no"
    t.integer  "sblu"
    t.boolean  "bypass_charge",                default: false,              null: false
    t.datetime "purchased_at",                                              null: false
    t.integer  "pending_reason_code"
    t.float    "transport_reimbursement",      default: 0.0,                null: false
    t.datetime "must_be_promised_at"
    t.float    "discount",                     default: 0.0,                null: false
    t.text     "discount_description",         default: "",                 null: false
    t.string   "preselected_promise_kind",   default: "purchase_protect", null: false
    t.string   "partner"
    t.integer  "percent_coverage"
    t.string   "limited_volume"
    t.string   "group_code"
    t.string   "group"
    t.string   "source_document_link"
  end

  add_index "sold_vehicles", ["pawn_access_no"], name: "index_sold_vehicles_on_pawn_access_no", using: :btree
  add_index "sold_vehicles", ["badge_no"], name: "index_sold_vehicles_on_badge_no", using: :btree
  add_index "sold_vehicles", ["agreement_no"], name: "index_sold_vehicles_on_agreement_no", using: :btree
  add_index "sold_vehicles", ["vnum"], name: "index_sold_vehicles_on_vnum", using: :btree

  create_table "sold_vehicles_load", force: :cascade do |t|
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "model"
    t.integer  "odometer_reading"
    t.integer  "year"
    t.string   "make",                                                      null: false
    t.string   "vnum",                                                       null: false
    t.string   "lane_no",                                                   null: false
    t.string   "run_no",                                                    null: false
    t.string   "universal_no"
    t.string   "pawn_access_no"
    t.string   "badge_no"
    t.string   "agreement_no"
    t.float    "promise_250"
    t.float    "additional_day"
    t.float    "promise_500"
    t.float    "vehicle_purchase_price",                                    null: false
    t.integer  "odometer_rating"
    t.integer  "autocheck_rating"
    t.integer  "mmr_rating"
    t.integer  "age_rating"
    t.integer  "seller_rating"
    t.integer  "mid_rating"
    t.integer  "bid_risk_rating"
    t.integer  "ineligibility_condition"
    t.boolean  "if_bid"
    t.string   "work_order_number"
    t.string   "location_initials"
    t.boolean  "ove"
    t.boolean  "presale_inspection"
    t.float    "buy_fee"
    t.boolean  "promise_prepaid"
    t.float    "inspection_only_price"
    t.float    "inspection_bundle_price"
    t.boolean  "automatic_purchase"
    t.integer  "preselected_promise_days"
    t.integer  "preselected_promise_miles"
    t.string   "automatic_purchase_email"
    t.boolean  "automatic_purchase_processed", default: false,              null: false
    t.integer  "vehicle_code"
    t.integer  "promise_price_threshold"
    t.boolean  "seller_paid",                  default: false
    t.string   "sale_no",                                                   null: false
    t.string   "seller_no"
    t.integer  "sblu"
    t.boolean  "bypass_charge",                default: false,              null: false
    t.datetime "purchased_at",                                              null: false
    t.integer  "pending_reason_code"
    t.float    "transport_reimbursement",      default: 0.0,                null: false
    t.datetime "must_be_promised_at"
    t.float    "discount",                     default: 0.0,                null: false
    t.text     "discount_description",         default: "",                 null: false
    t.string   "preselected_promise_kind",   default: "purchase_protect", null: false
    t.string   "partner"
    t.integer  "percent_coverage"
    t.string   "limited_volume"
    t.string   "group_code"
    t.string   "group"
  end

  create_table "tokens", force: :cascade do |t|
    t.string   "service_name"
    t.string   "access_token"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_versions", force: :cascade do |t|
    t.string   "item_type",      limit: 255,  null: false
    t.integer  "item_id",                     null: false
    t.string   "event",          limit: 255,  null: false
    t.string   "whodunnit",      limit: 255
    t.string   "object",         limit: 2000
    t.string   "object_changes", limit: 2000
    t.datetime "created_at"
  end

  add_index "user_versions", ["item_type", "item_id"], name: "index_user_versions_on_item_type_and_item_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",                  default: "",    null: false
    t.string   "encrypted_password",        default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "location_initials"
    t.boolean  "active",                    default: true
    t.string   "email",                     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean  "admin",                     default: false, null: false
    t.boolean  "super_user",                default: false, null: false
    t.boolean  "kiosk_user",                default: false, null: false
    t.string   "authentication_token"
    t.boolean  "api_user",                  default: false, null: false
    t.boolean  "buyer_user",                default: false, null: false
    t.integer  "sign_in_count",             default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "vehicle_creator",           default: false, null: false
    t.boolean  "invoiceable",               default: false, null: false
    t.boolean  "promise_admin_viewer",    default: false, null: false
    t.boolean  "promise_admin_editor",    default: false, null: false
    t.boolean  "loss_prevention_viewer",    default: false, null: false
    t.boolean  "finance_admin_viewer",      default: false, null: false
    t.boolean  "flat_rate_inquiry_viewer",  default: false, null: false
    t.boolean  "flat_rate_editor",          default: false, null: false
    t.boolean  "feature_flag_editor",       default: false, null: false
    t.boolean  "user_admin_editor",         default: false, null: false
    t.boolean  "agreement_transaction_viewer", default: true,  null: false
    t.boolean  "agreement_score_viewer",       default: true,  null: false
    t.string   "payment_customer_id"
    t.boolean  "external_vehicle_enterer",  default: false
    t.boolean  "external_vehicle_approver", default: false
    t.boolean  "agreementcheck_viewer", default: false
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["payment_customer_id"], name: "index_users_on_payment_customer_id", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "vehicle_categories", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "vehicle_information", force: :cascade do |t|
    t.integer  "promise_offer_id"
    t.string   "make",               null: false
    t.string   "model"
    t.integer  "odometer_reading"
    t.boolean  "ove"
    t.string   "vnum",                null: false
    t.integer  "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "vehicle_code"
  end

  add_index "vehicle_information", ["promise_offer_id"], name: "index_vehicle_information_on_promise_offer_id", using: :btree
  add_index "vehicle_information", ["vnum"], name: "index_vehicle_information_on_vnum", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "vnum_blacklists", force: :cascade do |t|
    t.string   "vnum"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "expiration_date"
    t.text     "notes",                    default: "",    null: false
    t.boolean  "ineligible_for_promise", default: false, null: false
  end

  create_table "vnum_scan_records", force: :cascade do |t|
    t.string   "vnum"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pawn_access_no", null: false
    t.string   "zipcode"
    t.float    "latitude"
    t.float    "longitude"
  end

end
