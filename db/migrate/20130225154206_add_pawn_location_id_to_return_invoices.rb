class AddPawnLocationIdToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :pawn_location_id, :integer
  end
end
