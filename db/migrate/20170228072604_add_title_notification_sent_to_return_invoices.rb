class AddTitleNotificationSentToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :title_notification_sent, :boolean, default: false
  end
end
