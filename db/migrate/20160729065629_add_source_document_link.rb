class AddSourceDocumentLink < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :source_document_link, :string
    add_column :promise_offers, :source_document_link, :string
  end
end
