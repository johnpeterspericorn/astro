class AddPurchasingUserToCancelledPurchases < ActiveRecord::Migration
  def change
    change_table :cancelled_purchases do |t|
      t.references :purchasing_user, polymorphic: true
    end
  end
end
