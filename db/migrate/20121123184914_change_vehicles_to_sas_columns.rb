class ChangeVehiclesToSasColumns < ActiveRecord::Migration
  # odometer_reading, year, model,  make,  LANENO, RUNNO, VNUM,               id,             dlrno,     badge, UID,     Promise, Additional_day, Add_transp, g500
  # 138505          , 1999, 3.2 TL, ACURA, 11,     96,    19UUA564XXA052974, svaa2018461196, 100478657, 1010,  5177778, 430.87,    $5,             $50,        557.044
  def change
    remove_column :vehicles, :trim

    add_column :vehicles, :odometer_reading, :string
    add_column :vehicles, :year,             :string
    add_column :vehicles, :make,  :string
    add_column :vehicles, :vnum,   :string

    add_column :vehicles, :laneno,  :string
    add_column :vehicles, :runno,   :string
    add_column :vehicles, :pawn, :string

    add_column :vehicles, :dlrno, :string
    add_column :vehicles, :badge, :string
    add_column :vehicles, :uid,   :string

    add_column :vehicles, :promise,      :float
    add_column :vehicles, :additional_day, :float
    add_column :vehicles, :add_transp,     :float
    add_column :vehicles, :g500,           :float
  end
end
