class AddDisbursementEmailsToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :disbursement_emails, :text
  end
end
