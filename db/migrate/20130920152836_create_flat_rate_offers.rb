class CreateFlatRateOffers < ActiveRecord::Migration
  def change
    create_table :flat_rate_offers do |t|
      t.references :flat_rate,     null: false
      t.references :offering_user, null: false, polymorphic: true

      t.float    :price,       null: false
      t.boolean  :letter_sent, null: false, default: false
      t.boolean  :accepted,    null: false, default: false
      t.datetime :accepted_at

      t.string :agreement_name,  null: false
      t.string :agreement_cell,  null: false
      t.string :agreement_email, null: false
      t.boolean :differs_from_auth_service, null: false, default: false

      t.timestamps
    end
  end
end
