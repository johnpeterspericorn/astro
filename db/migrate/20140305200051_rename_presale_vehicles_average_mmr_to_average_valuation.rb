class RenamePresaleVehiclesAverageMmrToAverageValuation < ActiveRecord::Migration
  def change
    rename_column :presale_vehicles, :average_mmr, :average_valuation
  end
end
