class AddMilesSelectedAndPsiEligibilityToFlatRateOffers < ActiveRecord::Migration
  def change
    add_column :flat_rate_offers, :psi_eligible, :boolean, null: false, default: false
    add_column :flat_rate_offers, :miles_selected, :integer
  end
end
