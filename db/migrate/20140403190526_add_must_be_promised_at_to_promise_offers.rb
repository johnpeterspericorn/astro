class AddMustBePromisedAtToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :must_be_promised_at, :datetime
  end
end
