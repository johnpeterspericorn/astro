class AddIndexToPresaleVehicles < ActiveRecord::Migration
  def change
    add_index :presale_vehicles, :vnum
  end
end
