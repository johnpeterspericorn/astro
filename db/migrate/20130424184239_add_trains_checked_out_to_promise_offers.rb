class AddTrainsCheckedOutToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :checked_out_in_trains, :boolean
  end
end
