class AddNullConstraintToCheckedOutAndPaidByFloorPlanOnPromiseOffers < ActiveRecord::Migration
  def up
    %w(checked_out paid_by_floor_plan).each do |column|
      execute "UPDATE promise_offers SET #{column} = false WHERE #{column} IS NULL"
      change_column_null :promise_offers, column, false
    end
  end

  def down
    change_column_null :promise_offers, :checked_out,        true
    change_column_null :promise_offers, :paid_by_floor_plan, true
  end
end
