class AddVehicleTotalToExternalVehiclesLoad < ActiveRecord::Migration
  def change
    add_column :external_vehicles_loads, :vehicle_total, :integer
  end
end
