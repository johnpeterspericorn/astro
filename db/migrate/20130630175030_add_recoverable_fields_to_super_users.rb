class AddRecoverableFieldsToSuperUsers < ActiveRecord::Migration
  def change
    add_column :super_users, :email,                  :string, default: ""
    add_column :super_users, :reset_password_token,   :string
    add_column :super_users, :reset_password_sent_at, :datetime
  end
end
