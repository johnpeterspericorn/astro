class CreatePawnInformation < ActiveRecord::Migration
  def change
    create_table :pawn_information do |t|
      t.belongs_to :promise_offer, null: false
      t.string     :pawn_access_no
      t.string     :badge_no
      t.float      :buy_fee
      t.string     :agreement_no
      t.string     :lane_no
      t.string     :run_no
      t.string     :location_initials
      t.string     :universal_no
      t.float      :vehicle_purchase_price
      t.string     :work_order_number

      t.timestamps
    end

    add_index :pawn_information, :promise_offer_id
  end
end
