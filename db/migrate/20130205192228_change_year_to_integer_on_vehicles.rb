class ChangeYearToIntegerOnVehicles < ActiveRecord::Migration
  def up
    connection.execute(%q{
      alter table presale_vehicles
      alter column year
      type integer using cast(year as integer)
    })

    connection.execute(%q{
      alter table sold_vehicles
      alter column year
      type integer using cast(year as integer)
    })
  end

  def down
    connection.execute(%q{
      alter table presale_vehicles
      alter column year
      type string using cast(year as string)
    })

    connection.execute(%q{
      alter table sold_vehicles
      alter column year
      type string using cast(year as string)
    })
  end
end
