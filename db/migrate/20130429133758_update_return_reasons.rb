class UpdateReturnReasons < ActiveRecord::Migration
  def change
    rename_column :return_invoices, :not_in_condition_described, :mechanical_issues
    rename_column :return_invoices, :not_as_expected,            :cosmetic_issues
  end
end
