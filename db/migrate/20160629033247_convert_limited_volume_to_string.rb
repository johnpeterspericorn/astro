class ConvertLimitedVolumeToString < ActiveRecord::Migration
  def change
    change_column :sold_vehicles, :limited_volume, :string, default: nil, null: true
    change_column :promise_offers, :limited_volume, :string, default: nil, null: true
  end
end
