class RemoveAccountsReceivableEmailsFromPawnLocations < ActiveRecord::Migration
  def up
    remove_column :pawn_locations, :accounts_receivable_emails
  end

  def down
    add_column :pawn_locations, :accounts_receivable_emails, :text
  end
end
