class AddTransportReimbursementToPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def change
    add_column :promise_offers, :transport_reimbursement, :float, null: false, default: 0.0
    add_column :sold_vehicles,    :transport_reimbursement, :float, null: false, default: 0.0
  end
end
