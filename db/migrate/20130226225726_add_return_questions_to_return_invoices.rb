class AddReturnQuestionsToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :not_in_condition_described, :boolean
    add_column :return_invoices, :not_as_expected, :boolean
    add_column :return_invoices, :unable_to_sell, :boolean
    add_column :return_invoices, :changed_mind, :boolean
    add_column :return_invoices, :other, :boolean
    add_column :return_invoices, :no_answer, :boolean
  end
end
