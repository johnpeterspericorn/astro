class RenamePriceRangeVehicleCategoryToVehicleCode < ActiveRecord::Migration
  def change
    rename_column :price_ranges, :vehicle_category, :vehicle_code
  end
end
