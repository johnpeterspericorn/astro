class AddPaymentCustomerIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :payment_customer_id, :string
    add_index :users, :payment_customer_id
  end
end
