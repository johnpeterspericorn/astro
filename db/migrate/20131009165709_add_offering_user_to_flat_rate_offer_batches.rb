class AddOfferingUserToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    change_table :flat_rate_offer_batches do |t|
      t.references :offering_user, null: false, polymorphic: true
    end
  end
end
