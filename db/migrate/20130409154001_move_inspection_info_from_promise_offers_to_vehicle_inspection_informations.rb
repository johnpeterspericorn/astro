class MoveInspectionInfoFromPromiseOffersToVehicleInspectionInformations < ActiveRecord::Migration
  COLUMNS = %w(
    presale_inspection
    inspection_only_price
    inspection_bundle_price)

  def comma_separated_columns
    COLUMNS.join(', ')
  end

  def column_assignments(source_table)
    COLUMNS.map do |column, assignments|
      "#{column} = #{source_table}.#{column}"
    end.join(', ')
  end

  def up
    execute <<-SQL
      INSERT INTO vehicle_inspection_informations(promise_offer_id, #{comma_separated_columns}, created_at, updated_at)
      SELECT id, #{comma_separated_columns}, created_at, updated_at FROM promise_offers
    SQL

     change_table :promise_offers do |t|
        COLUMNS.each do |column|
        t.remove column
      end
    end
  end

  def down
    change_table :promise_offers do |t|
      t.boolean :presale_inspection
      t.float   :inspection_only_price
      t.float   :inspection_bundle_price
    end

    execute <<-SQL
      UPDATE promise_offers
      SET #{column_assignments('vehicle_inspection_informations')}
      FROM vehicle_inspection_informations
      WHERE vehicle_inspection_informations.promise_offer_id = promise_offers.id
    SQL
  end
end
