class AddSourceDocumentLinkToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :source_document_link, :string
  end
end
