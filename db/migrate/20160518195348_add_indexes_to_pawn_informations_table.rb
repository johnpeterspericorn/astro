class AddIndexesToPawnInformationsTable < ActiveRecord::Migration
  def change
    add_index :pawn_information, :agreement_no
    add_index :pawn_information, :pawn_access_no
    add_index :vehicle_information, :vnum
  end
end
