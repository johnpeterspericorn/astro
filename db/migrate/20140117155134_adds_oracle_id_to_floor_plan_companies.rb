class AddsOracleIdToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :oracle_id, :integer
  end
end
