class AddCountryAndPartnerNetworksToPawnLocation < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :country, :string
    add_column :pawn_locations, :partner_network, :string
  end
end
