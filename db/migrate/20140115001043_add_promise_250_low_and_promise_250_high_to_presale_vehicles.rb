class AddPromise250LowAndPromise250HighToPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :presale_vehicles, :promise_250_low,  :float
    add_column :presale_vehicles, :promise_250_high, :float
  end
end
