class AddClosedFactoryExclusionToFlatRate < ActiveRecord::Migration
  def change
    add_column :flat_rates, :closed_factory_exclusion, :boolean, null: false, default: false
  end
end
