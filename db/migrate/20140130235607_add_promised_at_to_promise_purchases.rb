class AddPromisedAtToPromisePurchases < ActiveRecord::Migration
  def up
    add_column :promise_purchases, :promised_at, :datetime
    execute 'UPDATE promise_purchases SET promised_at = created_at'
    change_column_null :promise_purchases, :promised_at, false
  end

  def down
    remove_column :promise_purchases, :promised_at
  end
end
