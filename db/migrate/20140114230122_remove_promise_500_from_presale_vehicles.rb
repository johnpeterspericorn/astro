class RemovePromise500FromPresaleVehicles < ActiveRecord::Migration
  def up
    remove_column :presale_vehicles, :promise_500
  end

  def down
    add_column :presale_vehicles, :promise_500, :float
  end
end
