class AddSellerNameIntoExternalVehiclesLoad < ActiveRecord::Migration
  def up
    add_column :external_vehicles_loads, :seller_name, :string
  end

  def down
    remove_column :external_vehicles_loads, :seller_name
  end
end
