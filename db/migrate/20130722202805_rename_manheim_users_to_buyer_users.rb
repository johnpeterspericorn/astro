class RenameAstroheimUsersToBuyerUsers < ActiveRecord::Migration
  def change
    rename_table :astroheim_users, :buyer_users
  end
end
