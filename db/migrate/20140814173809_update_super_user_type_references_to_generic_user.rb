class UpdateSuperUserTypeReferencesToGenericUser < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE cancelled_purchases
      SET purchasing_user_type = 'GenericUser'
      WHERE purchasing_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE cancelled_purchases
      SET cancelling_user_type = 'GenericUser'
      WHERE cancelling_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE flat_rate_offers
      SET offering_user_type = 'GenericUser'
      WHERE offering_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE flat_rate_offers
      SET accepting_user_type = 'GenericUser'
      WHERE accepting_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE promise_offers
      SET changing_user_type = 'GenericUser'
      WHERE changing_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE promise_purchases
      SET purchasing_user_type = 'GenericUser'
      WHERE purchasing_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE return_invoices
      SET returning_user_type = 'GenericUser'
      WHERE returning_user_type = 'SuperUser'
    SQL

    execute <<-SQL
      UPDATE return_invoices
      SET cancelling_user_type = 'GenericUser'
      WHERE cancelling_user_type = 'SuperUser'
    SQL
  end

  def down
    execute <<-SQL
      UPDATE cancelled_purchases
      SET purchasing_user_type = 'SuperUser'
      WHERE purchasing_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE cancelled_purchases
      SET cancelling_user_type = 'SuperUser'
      WHERE cancelling_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE flat_rate_offers
      SET offering_user_type = 'SuperUser'
      WHERE offering_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE flat_rate_offers
      SET accepting_user_type = 'SuperUser'
      WHERE accepting_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE promise_offers
      SET changing_user_type = 'SuperUser'
      WHERE changing_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE promise_purchases
      SET purchasing_user_type = 'SuperUser'
      WHERE purchasing_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE return_invoices
      SET returning_user_type = 'SuperUser'
      WHERE returning_user_type = 'GenericUser'
    SQL

    execute <<-SQL
      UPDATE return_invoices
      SET cancelling_user_type = 'SuperUser'
      WHERE cancelling_user_type = 'GenericUser'
    SQL
  end
end
