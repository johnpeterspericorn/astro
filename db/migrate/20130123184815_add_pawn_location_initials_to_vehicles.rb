class AddPawnLocationInitialsToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :location_initials, :string
  end
end
