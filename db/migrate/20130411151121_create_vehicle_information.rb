class CreateVehicleInformation < ActiveRecord::Migration
  def change
    create_table :vehicle_information do |t|
      t.belongs_to :promise_offer, null: false
       t.string    :make
       t.string    :model
       t.integer   :odometer_reading
       t.boolean   :ove
       t.string    :vnum
       t.integer   :year

       t.timestamps
    end

    add_index :vehicle_information, :promise_offer_id
  end
end
