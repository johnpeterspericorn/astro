class AddVehiclePurchasePriceToPromiseSelections < ActiveRecord::Migration
  def change
    add_column :promise_selections, :vehicle_purchase_price, :float
  end
end
