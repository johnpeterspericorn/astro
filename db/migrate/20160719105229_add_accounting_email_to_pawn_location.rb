class AddAccountingEmailToPawnLocation < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :accounting_email, :string
  end
end
