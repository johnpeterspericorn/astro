class RemoveSourceDocumentLinkFromAstro < ActiveRecord::Migration
  def change
    remove_column :sold_vehicles, :source_document_link
    remove_column :promise_offers, :source_document_link
  end
end
