class MovesPriceFactorRatingsFromPromiseOffersToPriceFactors < ActiveRecord::Migration
  COLUMNS = %w(
    age_rating
    bid_risk_rating
    autocheck_rating
    mid_rating
    mmr_rating
    odometer_rating
    seller_rating)

  def comma_separated_columns
    COLUMNS.join(', ')
  end

  def column_assignments(source_table)
    COLUMNS.map do |column, assignments|
      "#{column} = #{source_table}.#{column}"
    end.join(', ')
  end

  def up
    execute <<-SQL
      INSERT INTO price_factors(promise_offer_id, #{comma_separated_columns}, created_at, updated_at)
      SELECT id, #{comma_separated_columns}, created_at, updated_at FROM promise_offers
    SQL

    change_table :promise_offers do |t|
      COLUMNS.each do |column|
        t.remove column
      end
    end
  end

  def down
    change_table :promise_offers do |t|
      COLUMNS.each do |column|
        t.integer column
      end
    end

    execute <<-SQL
      UPDATE promise_offers
      SET #{column_assignments('price_factors')}
      FROM price_factors
      WHERE price_factors.promise_offer_id = promise_offers.id
    SQL
  end
end
