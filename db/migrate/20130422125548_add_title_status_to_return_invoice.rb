class AddTitleStatusToReturnInvoice < ActiveRecord::Migration
  def change
    add_column :return_invoices, :title_status, :integer
  end
end
