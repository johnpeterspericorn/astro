class MoveFloorPlanCompaniesSiteNoAndBranchNameToFloorPlanBranches < ActiveRecord::Migration
  def up
    execute <<-SQL
      INSERT INTO floor_plan_branches
      (
        site_no,
        name,
        company_code,
        address_street,
        address_suite,
        address_city,
        address_state,
        address_zipcode,
        country_code,
        created_at,
        updated_at
      )
      SELECT
        site_no,
        branch_name,
        company_code,
        address_street,
        address_suite,
        address_city,
        address_state,
        address_zipcode,
        country_code,
        created_at,
        updated_at
      FROM floor_plan_companies
    SQL

    remove_columns(
      :floor_plan_companies,
      :site_no,
      :branch_name,
      :address_street,
      :address_suite,
      :address_city,
      :address_state,
      :address_zipcode,
      :country_code,
    )
  end

  def down
    change_table :floor_plan_companies do |t|
      t.string :site_no
      t.string :branch_name
      t.string :address_street
      t.string :address_suite
      t.string :address_city
      t.string :address_state
      t.string :address_zipcode
      t.string :country_code, default: 'us', null: false
    end
    add_index :floor_plan_companies, [:company_code, :site_no], unique: true

    execute <<-SQL
      UPDATE floor_plan_companies
      SET
        site_no = floor_plan_branches.site_no,
        branch_name = floor_plan_branches.name
        address_street = floor_plan_branches.address_street,
        address_suite = floor_plan_branches.address_suite,
        address_city = floor_plan_branches.address_city,
        address_state = floor_plan_branches.address_state,
        address_zipcode = floor_plan_branches.address_zipcode,
        country_code = floor_plan_branches.country_code,
      FROM floor_plan_branches
      WHERE floor_plan_branches.floor_plan_company_id = floor_plan_company.id
    SQL

    change_column_null :floor_plan_companies, :site_no, false

    execute <<-SQL
      DELETE FROM floor_plan_branches
      USING floor_plan_companies
      WHERE floor_plan_branches.floor_plan_company_id = floor_plan_company.id
    SQL
  end
end
