class CreatePromiseSelections < ActiveRecord::Migration
  def change
    create_table :promise_selections do |t|
      t.string   :agreement_no
      t.string   :pawn_access_no
      t.string   :badge_no
      t.string   :universal_no

      t.string   :vnum
      t.string   :year
      t.string   :make
      t.string   :model
      t.string   :odometer_reading

      t.integer  :days_selected
      t.integer  :miles_selected
      t.float    :promise_price
      t.string   :promise_status

      t.timestamps
    end
  end
end
