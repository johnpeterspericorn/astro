class FixSoldVehiclesVehiclePurchasePriceColumnType < ActiveRecord::Migration
  def up
    change_column :sold_vehicles, :vehicle_purchase_price, :float
  end

  def down
    # noop, this migration should be reversible, but there is no reason to go
    # back to a stupid schema state
  end
end
