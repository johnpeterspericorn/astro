class CreatePromisePurchaseVersions < ActiveRecord::Migration
  def self.up
    create_table :promise_purchase_versions do |t|
      t.string   :item_type, :null => false, limit: 255
      t.integer  :item_id,   :null => false
      t.string   :event,     :null => false, limit: 255
      t.string   :whodunnit, limit: 255
      t.string     :object, limit: 2000
      t.string     :object_changes, limit: 2000
      t.datetime :created_at
    end
    add_index :promise_purchase_versions, [:item_type, :item_id]
  end

  def self.down
    remove_index :promise_purchase_versions, [:item_type, :item_id]
    drop_table :promise_purchase_versions
  end
end
