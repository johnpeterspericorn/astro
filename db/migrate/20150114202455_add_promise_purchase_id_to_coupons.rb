class AddPromisePurchaseIdToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :promise_purchase_id, :integer
  end
end
