class MoveAuditReasonsToTable < ActiveRecord::Migration
  class Audit < ActiveRecord::Base
    serialize :reason
    has_many :reasons, class_name: 'MoveAuditReasonsToTable::AuditReason'
  end

  class AuditReason < ActiveRecord::Base
  end

  def up
    Audit.transaction do
      Audit.all.each do |audit|
        if audit.reason.present?
          reasons = audit
            .reason
            .values
            .reject(&:blank?)

          reasons.each do |reason|
            audit.reasons.create!(description: reason)
          end
        end
      end
    end

    remove_column :audits, :reason
  end

  def down
    add_column :audits, :reason, :text, default: '', null: false

    Audit.transaction do
      Audit.all.each do |audit|
        audit.reason = audit
          .reasons
          .each_with_index
          .each_with_object(Hash.new) do |(reason, i), h|
            h["reason#{i+1}"] = reason.description
          end

        audit.save!
      end
    end
  end
end
