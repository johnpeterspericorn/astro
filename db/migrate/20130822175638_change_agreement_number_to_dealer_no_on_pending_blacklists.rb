class ChangeAgreementNumberToAgreementNoOnPendingBlacklists < ActiveRecord::Migration
  def change
    rename_column :pending_blacklists, :agreement_number, :agreement_no
  end
end
