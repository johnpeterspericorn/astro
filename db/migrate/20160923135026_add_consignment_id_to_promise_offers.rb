class AddConsignmentIdToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :consignment_id, :integer
  end
end
