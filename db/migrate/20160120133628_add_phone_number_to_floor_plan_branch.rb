class AddPhoneNumberToFloorPlanBranch < ActiveRecord::Migration
  def change
    add_column :floor_plan_branches, :phone_number, :string
  end
end
