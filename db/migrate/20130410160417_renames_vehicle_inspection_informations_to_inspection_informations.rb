class RenamesVehicleInspectionInformationsToInspectionInformations < ActiveRecord::Migration
  def change
    rename_table :vehicle_inspection_informations, :inspection_informations
  end
end
