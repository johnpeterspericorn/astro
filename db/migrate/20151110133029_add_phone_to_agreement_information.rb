class AddPhoneToAgreementInformation < ActiveRecord::Migration
  def change
    add_column :agreement_information, :phone, :string
  end
end
