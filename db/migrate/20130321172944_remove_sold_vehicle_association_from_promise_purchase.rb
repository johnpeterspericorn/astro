class RemoveSoldVehicleAssociationFromPromisePurchase < ActiveRecord::Migration
  def change
    remove_column :promise_purchases, :sold_vehicle_id
  end
end
