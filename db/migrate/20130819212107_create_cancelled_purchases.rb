class CreateCancelledPurchases < ActiveRecord::Migration
  def change
    create_table :cancelled_purchases do |t|
      t.string   :buyer_email
      t.string   :pawn_access_no
      t.string   :agreement_no
      t.string   :universal_no
      t.string   :vnum
      t.integer  :promise_days_selected
      t.integer  :promise_miles_selected
      t.boolean  :promise_if_bid
      t.boolean  :promise_seller_paid
      t.boolean  :automatic_purchase
      t.float    :vehicle_purchase_price
      t.float    :promise_price
      t.float    :inspection_price
      t.string   :transaction_id
      t.datetime :purchased_at
    end
  end
end
