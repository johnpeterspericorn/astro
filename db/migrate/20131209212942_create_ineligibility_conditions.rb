class CreateIneligibilityConditions < ActiveRecord::Migration
  def change
    create_table :ineligibility_conditions do |t|
      t.integer :condition_id
      t.string :notice

      t.timestamps
    end
  end
end
