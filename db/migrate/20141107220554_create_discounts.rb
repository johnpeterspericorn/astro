class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.float :amount, default: 0.0, null: false
      t.text :description, default: '', null: false
      t.timestamps
    end
  end
end
