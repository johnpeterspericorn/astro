class AddColumnsToFlatRates < ActiveRecord::Migration
  def change
    add_column :flat_rates, :lv_tier, :string
    add_column :flat_rates, :lv_price, :float
  end
end
