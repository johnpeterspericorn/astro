class AddSellerNoToPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :presale_vehicles, :seller_no, :string
  end
end
