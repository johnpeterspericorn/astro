class AddKindToPromisePurchase < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :kind, :string, null: false, default: 'purchase_protect'
  end
end
