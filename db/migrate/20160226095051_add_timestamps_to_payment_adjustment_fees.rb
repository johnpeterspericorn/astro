class AddTimestampsToPaymentAdjustmentFees < ActiveRecord::Migration
  def change
    add_timestamps :payment_adjustment_fees
  end
end
