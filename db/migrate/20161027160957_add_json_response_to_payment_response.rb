class AddJsonResponseToPaymentResponse < ActiveRecord::Migration
  def change
    add_column :payment_responses, :json_response, :json
  end
end
