class ChangeFlatRateOfferBatchesExpirationDateToDateOnlyType < ActiveRecord::Migration
  def up
    change_column :flat_rate_offer_batches, :expiration_date, :date
  end

  def down
    change_column :flat_rate_offer_batches, :expiration_date, :datetime
  end
end
