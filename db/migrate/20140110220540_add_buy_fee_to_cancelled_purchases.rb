class AddBuyFeeToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :buy_fee, :float
  end
end
