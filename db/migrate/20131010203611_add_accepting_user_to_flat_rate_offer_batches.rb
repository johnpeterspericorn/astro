class AddAcceptingUserToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    change_table :flat_rate_offer_batches do |t|
      t.references :accepting_user, polymorphic: true
    end
  end
end
