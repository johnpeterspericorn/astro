class AddIneligibleForPromiseToVinBlacklists < ActiveRecord::Migration
  def change
    add_column :vnum_blacklists, :ineligible_for_promise, :boolean, default: false, null: false
  end
end
