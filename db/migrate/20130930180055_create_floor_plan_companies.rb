class CreateFloorPlanCompanies < ActiveRecord::Migration
  def change
    create_table :floor_plan_companies do |t|
      t.string :name
      t.string :address_street
      t.string :address_suite
      t.string :address_city
      t.string :address_state
      t.string :address_zipcode

      t.timestamps
    end
  end
end
