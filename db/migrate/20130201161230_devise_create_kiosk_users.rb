class DeviseCreateKioskUsers < ActiveRecord::Migration
  def change
    create_table(:kiosk_users) do |t|
      t.string :username,           :null => false, :default => ""
      #t.references :loginable, polymorphic: true
      ## Database authenticatable
      t.string :encrypted_password, :null => false, :default => ""
      t.timestamps
    end

    add_index :kiosk_users, :username,             :unique => true
  end
end
