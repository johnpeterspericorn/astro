class AddAgreementcheckViewerPermissionToUser < ActiveRecord::Migration
  def change
    add_column :users, :agreementcheck_viewer, :boolean, default: false
  end
end
