class AddSiteNoToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :site_no, :string
  end
end
