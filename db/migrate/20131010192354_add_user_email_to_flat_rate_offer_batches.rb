class AddUserEmailToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    add_column :flat_rate_offer_batches, :user_email, :string
  end
end
