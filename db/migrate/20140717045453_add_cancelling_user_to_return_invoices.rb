class AddCancellingUserToReturnInvoices < ActiveRecord::Migration
  def change
    change_table :return_invoices do |t|
      t.references :cancelling_user, polymorphic: true
    end
  end
end
