require_relative '../database_default_values'

class AddNullConstraintToPawnAccessNoOnPawnInformationSoldVehiclesAndVinScanRecords < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    %w(pawn_information sold_vehicles vnum_scan_records).each do |table_name|
      execute "UPDATE #{table_name} SET pawn_access_no = '#{MISSING_NOT_NULL_STRING}' WHERE pawn_access_no IS NULL"
      change_column_null table_name, :pawn_access_no, false
    end
  end

  def down
    %w(pawn_information sold_vehicles vnum_scan_records).each do |table_name|
      change_column_null table_name, :pawn_access_no, true
      execute "UPDATE #{table_name} SET pawn_access_no = NULL WHERE pawn_access_no = '#{MISSING_NOT_NULL_STRING}'"
    end
  end
end
