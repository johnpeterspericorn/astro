class CreatePendingBlacklists < ActiveRecord::Migration
  def change
    create_table :pending_blacklists do |t|
      t.string :agreement_number

      t.timestamps
    end
  end
end
