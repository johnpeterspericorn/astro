class ChangePresaleVehiclesLaneNoAndRunNoToStrings < ActiveRecord::Migration
  def up
    change_column :presale_vehicles, :lane_no, :string
    change_column :presale_vehicles, :run_no,  :string
  end

  def down
    # Postgres cannot easily reverse this direction. If this ever needs
    # reversal, I recommend a new migration
    raise ActiveRecord::IrreversibleMigration
  end
end
