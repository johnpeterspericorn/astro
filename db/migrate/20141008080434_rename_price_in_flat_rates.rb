class RenamePriceInFlatRates < ActiveRecord::Migration
  def change
    rename_column :flat_rates, :low_price, :low_price_250
    rename_column :flat_rates, :high_price, :high_price_250
    rename_column :flat_rates, :target_price, :target_price_250
  end
end
