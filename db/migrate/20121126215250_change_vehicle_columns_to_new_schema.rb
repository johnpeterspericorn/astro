class ChangeVehicleColumnsToNewSchema < ActiveRecord::Migration
  def change
    rename_column :vehicles, :laneno,    :lane_no
    rename_column :vehicles, :runno,     :run_no
    rename_column :vehicles, :promise, :promise_250
    rename_column :vehicles, :g500,      :promise_500
  end
end
