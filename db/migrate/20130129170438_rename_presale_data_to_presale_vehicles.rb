class RenamePresaleDataToPresaleVehicles < ActiveRecord::Migration
  def change
    rename_table :presale_data, :presale_vehicles
  end
end
