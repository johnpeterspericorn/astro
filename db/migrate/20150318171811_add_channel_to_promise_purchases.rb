class AddChannelToPromisePurchases < ActiveRecord::Migration
  def up
    add_column :promise_purchases, :channel, :string

    execute <<-SQL
      UPDATE promise_purchases
      SET channel = promise_offers.channel
      FROM promise_offers
      WHERE promise_offers.id = promise_purchases.promise_offer_id
    SQL

    change_column_null :promise_purchases, :channel, false, "unknown"
  end

  def down
    remove_column :promise_purchases, :channel
  end
end
