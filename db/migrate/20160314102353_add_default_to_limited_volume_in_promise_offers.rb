class AddDefaultToLimitedVolumeInPromiseOffers < ActiveRecord::Migration
  def change
    change_column :promise_offers, :limited_volume, :boolean, default: false, null: false
  end
end
