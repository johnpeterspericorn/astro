class AddUsernameToApiUsers < ActiveRecord::Migration
  def change
    add_column :api_users, :username, :string
  end
end
