class AddPromiseOfferIdToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :promise_offer_id, :integer
  end
end
