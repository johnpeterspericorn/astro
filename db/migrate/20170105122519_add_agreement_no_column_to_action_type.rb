class AddAgreementNoColumnToActionType < ActiveRecord::Migration
  def up
    add_column :action_types, :agreement_no, :string
  end

  def down
    remove_column :action_types, :agreement_no
  end
end
