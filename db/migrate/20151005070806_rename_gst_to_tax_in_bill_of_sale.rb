class RenameGstToTaxInBillOfSale < ActiveRecord::Migration
  def change
    rename_column :bill_of_sales, :gst, :tax
    change_column_null :bill_of_sales, :tax, true
  end
end
