class AddG2gConvertedFlagToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :g2g_converted, :boolean, default: false, null: false
  end
end
