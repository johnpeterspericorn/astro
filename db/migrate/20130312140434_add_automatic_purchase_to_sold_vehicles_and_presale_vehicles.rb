class AddAutomaticPurchaseToSoldVehiclesAndPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles,    :automatic_purchase, :boolean
    add_column :presale_vehicles, :automatic_purchase, :boolean
  end
end
