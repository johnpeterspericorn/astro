class AddPastTimeToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :past_time, :boolean
  end
end
