class AddApprovedAtToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :approved_at, :datetime
  end
end
