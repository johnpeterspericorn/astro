class ChangeColumnLetterSent < ActiveRecord::Migration
  def change
    remove_column :network_plus_offers, :letter_sent
    add_column :network_plus_offers, :letter_sent, :datetime
  end
end
