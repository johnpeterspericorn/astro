class CreatePresaleVehiclesLoad < ActiveRecord::Migration
  def change
    create_table "presale_vehicles_load" do |t|
      t.string  "vnum"
      t.integer "odometer_reading"
      t.integer "year"
      t.string  "model"
      t.string  "make"
      t.integer "average_valuation"
      t.float   "vnum_risk"
      t.integer "odometer_rating"
      t.integer "autocheck_rating"
      t.integer "mmr_rating"
      t.integer "age_rating"
      t.integer "seller_rating"
      t.integer "mid_rating"
      t.float   "promise_250_medium"
      t.string  "lane_no"
      t.string  "run_no"
      t.integer "ineligibility_condition"
      t.string  "location_initials"
      t.boolean "ove"
      t.boolean "promise_prepaid"
      t.boolean "automatic_purchase"
      t.integer "vehicle_code"
      t.integer "promise_price_threshold"
      t.boolean "seller_paid",               :default => false
      t.float   "promise_250_low"
      t.float   "promise_250_high"
      t.integer "low_valuation"
      t.integer "high_valuation"
      t.string  "seller_no"
    end
  end
end
