class RemoveHighLowPricesFromFlatRateInquiries < ActiveRecord::Migration
  def up
    remove_column :flat_rate_inquiries, :high_price
    remove_column :flat_rate_inquiries, :low_price
  end

  def down
    add_column :flat_rate_inquiries, :high_price, :float
    add_column :flat_rate_inquiries, :low_price, :float
  end
end
