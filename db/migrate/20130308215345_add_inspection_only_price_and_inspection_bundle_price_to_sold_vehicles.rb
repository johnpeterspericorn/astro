class AddInspectionOnlyPriceAndInspectionBundlePriceToSoldVehicles < ActiveRecord::Migration
  def change
    change_table :sold_vehicles do |t|
      t.float :inspection_only_price
      t.float :inspection_bundle_price
    end
  end
end
