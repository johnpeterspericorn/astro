class CreateBillOfSales < ActiveRecord::Migration
  def change
    create_table :bill_of_sales do |t|
      t.integer :promise_purchase_id
      t.integer :return_invoice_id
      t.string :vra_no
      t.string :vnum
      t.string :make
      t.string :model
      t.integer :year
      t.float :price
      t.float :gst
      t.float :fees
      t.integer :odometer_reading
      t.string :seller_5mil
      t.string :seller_agreement_name
      t.string :seller_address
      t.string :seller_postal_code
      t.string :seller_city
      t.string :seller_phone
      t.string :seller_fax
      t.string :selling_sales_person_name
      t.string :selling_sales_person_signature
      t.string :selling_sales_person_registration_no
      t.string :selling_hst_registration_no
      t.string :selling_qst_registration_no
      t.string :buying_sales_person_name
      t.string :buying_sales_person_signature
      t.string :buying_sales_person_registration_no
      t.string :buying_hst_registration_no
      t.string :buying_qst_registration_no
      t.timestamps
    end
  end
end
