class MovePawnInfoFromPromiseOffersToPawnInformation < ActiveRecord::Migration
  COLUMNS = %w(
    pawn_access_no
    badge_no
    buy_fee
    agreement_no
    lane_no
    run_no
    location_initials
    universal_no
    vehicle_purchase_price
    work_order_number)

  def comma_separated_columns
    COLUMNS.join(', ')
  end

  def column_assignments(source_table)
    COLUMNS.map do |column, assignments|
      "#{column} = #{source_table}.#{column}"
    end.join(', ')
  end

  def up
    execute <<-SQL
      INSERT INTO pawn_information(promise_offer_id, #{comma_separated_columns}, created_at, updated_at)
      SELECT id, #{comma_separated_columns}, created_at, updated_at FROM promise_offers
    SQL

     change_table :promise_offers do |t|
        COLUMNS.each do |column|
        t.remove column
      end
    end
  end

  def down
    change_table :promise_offers do |t|
      t.string :pawn_access_no
      t.string :badge_no
      t.float  :buy_fee
      t.string :agreement_no
      t.string :lane_no
      t.string :run_no
      t.string :location_initials
      t.string :universal_no
      t.float  :vehicle_purchase_price
      t.string :work_order_number
    end

    execute <<-SQL
      UPDATE promise_offers
      SET #{column_assignments('pawn_information')}
      FROM pawn_information
      WHERE pawn_information.promise_offer_id = promise_offers.id
    SQL
  end
end
