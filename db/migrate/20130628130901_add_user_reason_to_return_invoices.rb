class AddUserReasonToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :additional_information, :string
  end
end
