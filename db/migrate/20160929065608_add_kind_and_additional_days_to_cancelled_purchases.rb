class AddKindAndAdditionalDaysToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :promise_kind, :string
    add_column :cancelled_purchases, :promise_additional_days, :integer
  end
end
