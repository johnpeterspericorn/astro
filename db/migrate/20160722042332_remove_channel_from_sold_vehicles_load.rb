class RemoveChannelFromSoldVehiclesLoad < ActiveRecord::Migration
  def change
    remove_column :sold_vehicles_load, :channel
  end
end
