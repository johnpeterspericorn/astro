class AddAccountsReceivableEmailsToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :accounts_receivable_emails, :text
  end
end
