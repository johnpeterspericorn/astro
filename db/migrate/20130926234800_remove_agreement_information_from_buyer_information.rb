class RemoveAgreementInformationFromBuyerInformation < ActiveRecord::Migration
  def up
    execute <<-SQL
      -- Copy all agreement information to agreement_information table, excluding
      -- duplicate `agreement_no` entries
      INSERT INTO agreement_information (agreement_no, agreementship_name, address_street, address_suite, address_city, address_state, address_zipcode, created_at, updated_at)
      SELECT DISTINCT ON (agreement_no)  agreement_no, agreementship_name, address_street, address_suite, address_city, address_state, address_zipcode, created_at, updated_at
      FROM buyer_information
    SQL

    execute <<-SQL
      -- Delete duplicate `pawn_access_no` entries from buyer_information
      -- Adapted from: http://wiki.postgresql.org/wiki/Deleting_duplicates
      DELETE FROM buyer_information
      WHERE id IN (
        SELECT id
        FROM (
          SELECT id, row_number() over (partition BY pawn_access_no ORDER BY id) AS rnum
          FROM buyer_information
        ) t
        WHERE t.rnum > 1
      );
    SQL

    change_table :buyer_information do |t|
      t.remove_index [:pawn_access_no, :agreement_no]
      t.index :pawn_access_no, unique: true

      t.remove :agreement_no
      t.remove :agreementship_name
      t.remove :address_street
      t.remove :address_suite
      t.remove :address_city
      t.remove :address_state
      t.remove :address_zipcode
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
