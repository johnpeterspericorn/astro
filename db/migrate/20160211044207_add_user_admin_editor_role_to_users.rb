class AddUserAdminEditorRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :user_admin_editor, :boolean, null: false, default: false
  end
end
