class AddPriceAndSelectionToPromisePurchases < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :price, :float
    add_column :promise_purchases, :promise_selection_id, :integer
    rename_column :promise_purchases, :distance, :miles_selected
  end
end
