class RenameInspectionInformationsToInspectionInformation < ActiveRecord::Migration
  def change
    rename_table :inspection_informations, :inspection_information
  end
end
