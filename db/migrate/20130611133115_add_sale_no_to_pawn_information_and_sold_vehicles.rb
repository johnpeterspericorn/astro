require_relative '../database_default_values'

class AddSaleNoToPawnInformationAndSoldVehicles < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    %w(pawn_information sold_vehicles).each do |table_name|
      add_column table_name, :sale_no, :string
      execute "UPDATE #{table_name} SET sale_no = '#{MISSING_NOT_NULL_STRING}' WHERE sale_no IS NULL"
      change_column_null table_name, :sale_no, false
    end
  end

  def down
    %w(pawn_information sold_vehicles).each do |table_name|
      remove_column table_name, :sale_no
    end
  end
end
