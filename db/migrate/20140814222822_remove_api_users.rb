require 'bcrypt'

class RemoveApiUsers < ActiveRecord::Migration
  def update_polymorphic_references(table_name, polymorphic_column)
    execute <<-SQL
      UPDATE #{table_name}
      SET #{polymorphic_column}_type = 'GenericUser', #{polymorphic_column}_id = generic_users.id
      FROM generic_users
      JOIN api_users
      ON api_users.username = generic_users.username
      WHERE #{polymorphic_column}_type = 'ApiUser' AND api_users.id = #{polymorphic_column}_id
    SQL
  end

  def undo_polymorphic_references(table_name, polymorphic_column)
    execute <<-SQL
      UPDATE #{table_name}
      SET #{polymorphic_column}_type = 'ApiUser', #{polymorphic_column}_id = api_users.id
      FROM api_users
      JOIN generic_users
      ON api_users.username = generic_users.username
      WHERE #{polymorphic_column}_type = 'GenericUser' AND generic_users.id = #{polymorphic_column}_id
    SQL
  end

  POLYMORPHIC_TABLES = {
    cancelled_purchases: %w(purchasing_user cancelling_user),
    flat_rate_offers: %w(offering_user accepting_user),
    promise_offers: %w(changing_user),
    promise_purchases: %w(purchasing_user),
    return_invoices: %w(returning_user cancelling_user),
  }

  def up
    execute <<-SQL
      UPDATE api_users
      SET username = api_users.username || '_api'
      FROM generic_users
      WHERE generic_users.username = api_users.username
    SQL

    execute <<-SQL
      INSERT INTO generic_users (username,                          encrypted_password,                             created_at, updated_at, active, authentication_token, api_user)
      SELECT                     COALESCE(username, 'user_' || id), '#{BCrypt::Password.create(SecureRandom.hex)}', created_at, updated_at, true,   authentication_token, true
      FROM api_users
    SQL

    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        update_polymorphic_references(table, column)
      end
    end

    drop_table :api_users
  end

  def down
    create_table :api_users do |t|
      t.string   :username, default: '', null: false
      t.string   :encrypted_password, default: '', null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.string   :location_initials
      t.boolean  :active, default: true
      t.string   :authentication_token
    end

    execute <<-SQL
      INSERT INTO api_users (username, created_at, updated_at, authentication_token)
      SELECT                 username, created_at, updated_at, authentication_token
      FROM generic_users
      WHERE api_user IS TRUE
    SQL

    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        undo_polymorphic_references(table, column)
      end
    end

    execute 'DELETE FROM generic_users WHERE api_user IS TRUE'
  end
end
