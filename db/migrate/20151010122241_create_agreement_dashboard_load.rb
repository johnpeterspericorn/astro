class CreateAgreementDashboardLoad < ActiveRecord::Migration
  def change
    create_table :agreement_dashboard_load do |t|
      t.string :agreement_no
      t.integer :ds_360_rate
      t.text :agreementship_name
      t.float :return_percent_past60
      t.float :return_score_past60
      t.float :avg_loss_per_vehicle_past60
      t.float :buyer_score_past60
      t.float :margin_past60
      t.float :overall_score_past60
      t.float :return_percent_past90
      t.float :return_score_past90
      t.float :avg_loss_per_vehicle_past90
      t.float :buyer_score_past90
      t.float :margin_past90
      t.float :overall_score_past90
      t.float :return_percent_pastYTD
      t.float :return_score_pastYTD
      t.float :avg_loss_per_vehicle_pastYTD
      t.float :buyer_score_pastYTD
      t.float :margin_pastYTD
      t.float :overall_score_pastYTD
      t.float :return_percent_past12
      t.float :return_score_past12
      t.float :avg_loss_per_vehicle_past12
      t.float :buyer_score_past12
      t.float :margin_past12
      t.float :overall_score_past12
    end
  end
end
