class ChangePresaleVehiclesPromisePriceColumnsToFloat < ActiveRecord::Migration
  def up
    change_table :presale_vehicles do |t|
      t.change :promise_250, :float
      t.change :promise_500, :float
    end
  end

  def down
    change_table :presale_vehicles do |t|
      t.change :promise_250, :integer
      t.change :promise_500, :integer
    end
  end
end
