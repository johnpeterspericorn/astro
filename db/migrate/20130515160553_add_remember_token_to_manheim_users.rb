class AddRememberTokenToAstroheimUsers < ActiveRecord::Migration
  def change
    add_column :astroheim_users, :remember_token, :string
  end
end
