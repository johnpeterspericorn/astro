class AddLowAndHighValuationsToPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :presale_vehicles, :low_valuation, :integer
    add_column :presale_vehicles, :high_valuation, :integer
  end
end
