class AddColumnReasonToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :reason, :text
  end
end
