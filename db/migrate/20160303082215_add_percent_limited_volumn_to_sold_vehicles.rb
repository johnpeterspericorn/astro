class AddPercentLimitedVolumnToSoldVehicles < ActiveRecord::Migration
  def change
    change_table :sold_vehicles do |t|
      t.integer :percent_coverage
      t.boolean :limited_volume
    end
  end
end
