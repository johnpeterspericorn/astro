class RemoveNullConstraintsFromExtractedModels < ActiveRecord::Migration
  TABLES = [
    :pawn_information,
    :inspection_information,
    :price_factors,
    :vehicle_information]

  def up
    TABLES.each do |table|
      change_column_null table, :promise_offer_id, null: true
    end
  end

  def down
    TABLES.each do |table|
      change_column_null table, :promise_offer_id, null: false
    end
  end
end
