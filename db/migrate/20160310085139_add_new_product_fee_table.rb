class AddNewProductFeeTable < ActiveRecord::Migration
  def change
    create_table :new_product_fees do |t|
      t.string :agreement_no
      t.string :seller_no
      t.boolean :seller_paid, null: false, default: false
      t.integer :timeframe, null: false, default: 365
      t.integer :count, null: false, default: 999999
      t.float :fee, null: false, default: 0.0
      t.timestamps null: false
    end
  end
end
