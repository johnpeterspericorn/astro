class AddPendingReasonCodeToPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def change
    add_column :promise_offers, :pending_reason_code, :integer
    add_column :sold_vehicles,    :pending_reason_code, :integer
  end
end
