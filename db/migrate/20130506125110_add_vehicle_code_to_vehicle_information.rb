class AddVehicleCodeToVehicleInformation < ActiveRecord::Migration
  def change
    add_column :vehicle_information, :vehicle_code, :integer
  end
end
