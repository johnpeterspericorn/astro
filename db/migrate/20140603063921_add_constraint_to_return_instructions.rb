class AddConstraintToReturnInstructions < ActiveRecord::Migration
  def change
    change_column_default(:pawn_locations, :return_instructions, "")
  end
end
