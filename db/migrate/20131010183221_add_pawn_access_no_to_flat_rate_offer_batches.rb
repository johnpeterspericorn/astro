class AddPawnAccessNoToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    add_column :flat_rate_offer_batches, :pawn_access_no, :string
  end
end
