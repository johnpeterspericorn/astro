class AddAuthorizedForFlatRatesToPawnUsers < ActiveRecord::Migration
  def change
    add_column :pawn_users, :authorized_for_flat_rates, :boolean, null: false, default: false
  end
end
