class ForbidBadEmailsOnSoldVehicles < ActiveRecord::Migration
  def self.up
    failing_sequence = "\xc2\xa0"
    execute "ALTER TABLE sold_vehicles ADD CONSTRAINT check_automatic_purchase_email CHECK (automatic_purchase_email NOT LIKE '%#{failing_sequence}%')"
  end

  def self.down
    execute "ALTER TABLE sold_vehicles DROP CONSTRAINT check_automatic_purchase_email"
  end
end
