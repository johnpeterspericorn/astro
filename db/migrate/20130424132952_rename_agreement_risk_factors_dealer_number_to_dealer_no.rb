class RenameAgreementRiskFactorsAgreementNumberToAgreementNo < ActiveRecord::Migration
  def change
    rename_column :agreement_risk_factors, :agreement_number, :agreement_no
  end
end
