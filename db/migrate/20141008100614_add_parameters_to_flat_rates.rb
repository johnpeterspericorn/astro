class AddParametersToFlatRates < ActiveRecord::Migration
  def change
    add_column :flat_rates, :low_price_250_psi     , :float
    add_column :flat_rates, :target_price_250_psi  , :float
    add_column :flat_rates, :high_price_250_psi    , :float
    add_column :flat_rates, :low_price_500         , :float
    add_column :flat_rates, :target_price_500      , :float
    add_column :flat_rates, :high_price_500        , :float
    add_column :flat_rates, :low_price_500_psi     , :float
    add_column :flat_rates, :target_price_500_psi  , :float
    add_column :flat_rates, :high_price_500_psi    , :float
  end
end
