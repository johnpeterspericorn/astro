class AddSendUpdatesToBuyerInformation < ActiveRecord::Migration
  def change
    add_column :buyer_information, :send_updates, :boolean, null: false, default: false
  end
end
