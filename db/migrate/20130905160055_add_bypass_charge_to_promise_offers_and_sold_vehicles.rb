class AddBypassChargeToPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def change
    [:promise_offers, :sold_vehicles].each do |table|
      add_column table, :bypass_charge, :boolean, null: false, default: false
    end
  end
end
