class AddLossPreventionViewerRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :loss_prevention_viewer, :boolean, null: false, default: false
  end
end
