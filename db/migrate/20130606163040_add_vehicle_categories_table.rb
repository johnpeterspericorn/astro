class AddVehicleCategoriesTable < ActiveRecord::Migration
  def change
    create_table :vehicle_categories do |t|
      t.string :name, null: false
    end
  end
end
