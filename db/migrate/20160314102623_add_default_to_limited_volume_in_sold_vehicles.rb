class AddDefaultToLimitedVolumeInSoldVehicles < ActiveRecord::Migration
  def change
    change_column :sold_vehicles, :limited_volume, :boolean, default: false, null: false
  end
end
