class AddFlatRateInquiryViewerRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :flat_rate_inquiry_viewer, :boolean, null: false, default: false
  end
end
