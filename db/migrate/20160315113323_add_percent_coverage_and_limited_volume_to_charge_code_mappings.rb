class AddPercentCoverageAndLimitedVolumeToChargeCodeMappings < ActiveRecord::Migration
  def up
    ActiveRecord::Base.connection.execute("TRUNCATE charge_code_mappings RESTART IDENTITY")

    add_column :charge_code_mappings, :percent_coverage, :boolean, null: false
    add_column :charge_code_mappings, :limited_volume, :boolean, null: false

    ChargeCodeMapping.insert_into_db
  end

  def down
    remove_column :charge_code_mappings, :percent_coverage
    remove_column :charge_code_mappings, :limited_volume
  end
end
