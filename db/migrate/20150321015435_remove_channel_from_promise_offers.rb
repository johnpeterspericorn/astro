class RemoveChannelFromPromiseOffers < ActiveRecord::Migration
  def up
    remove_column :promise_offers, :channel
  end

  def down
    add_column :promise_offers, :channel, :string, null: false, default: "unknown"
  end
end
