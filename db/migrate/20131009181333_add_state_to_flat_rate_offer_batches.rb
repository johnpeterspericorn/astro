class AddStateToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    add_column :flat_rate_offer_batches, :state, :string
  end
end
