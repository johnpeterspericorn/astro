class ModifyUserVersion < ActiveRecord::Migration
  def change
    execute(<<-SQL)
      ALTER TABLE user_versions ALTER COLUMN item_type TYPE VARCHAR(255);
      ALTER TABLE user_versions ALTER COLUMN event TYPE VARCHAR(255);
      ALTER TABLE user_versions ALTER COLUMN whodunnit TYPE VARCHAR(255);
      ALTER TABLE user_versions ALTER COLUMN object_changes TYPE VARCHAR(2000);
      ALTER TABLE user_versions ALTER COLUMN object TYPE VARCHAR(2000);
    SQL
  end
end
