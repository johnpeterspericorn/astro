class AddEmailToAudits < ActiveRecord::Migration
  def change
    add_column :audits, :email, :string, default: '', null: false
  end
end
