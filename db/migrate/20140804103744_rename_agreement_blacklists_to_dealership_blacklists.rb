class RenameAgreementBlacklistsToAgreementshipBlacklists < ActiveRecord::Migration
  def change
    rename_table :agreement_blacklists, :agreementship_blacklists
  end
end
