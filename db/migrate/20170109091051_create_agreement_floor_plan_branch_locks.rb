class CreateAgreementFloorPlanBranchLocks < ActiveRecord::Migration
  def change
    create_table :agreement_floor_plan_branch_locks do |t|
      t.string :agreement_no
      t.string :company_code
      t.string :site_no

      t.timestamps null: false
    end
  end
end
