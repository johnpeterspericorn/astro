class AddGroupAndGroupCodeColumnsToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :group_code, :string
    add_column :promise_offers, :group, :string
  end
end
