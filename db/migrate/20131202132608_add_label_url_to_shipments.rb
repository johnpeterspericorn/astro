class AddLabelUrlToShipments < ActiveRecord::Migration
  def change
    add_column :shipments, :label_url, :string
  end
end
