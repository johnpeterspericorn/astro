class AddStateToFlatRateOffers < ActiveRecord::Migration
  def up
    add_column :flat_rate_offers, :state, :string

    execute <<-SQL
      UPDATE flat_rate_offers
      SET state = flat_rate_offer_batches.state
      FROM flat_rate_offer_batches
      WHERE
        flat_rate_offer_batches.id = flat_rate_offers.flat_rate_offer_batch_id
        AND
        flat_rate_offer_batches.state = 'approved'
    SQL

    change_column_null :flat_rate_offers, :state, false, 'unapproved'
  end

  def down
    remove_column :flat_rate_offers, :state
  end
end
