class ChangePromiseSelectionsToPromiseOffers < ActiveRecord::Migration
  def change
    rename_table :promise_selections, :promise_offers
    rename_column :promise_purchases, :promise_selection_id, :promise_offer_id
  end
end
