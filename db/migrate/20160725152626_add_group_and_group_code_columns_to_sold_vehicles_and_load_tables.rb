class AddGroupAndGroupCodeColumnsToSoldVehiclesAndLoadTables < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :group_code, :string
    add_column :sold_vehicles_load, :group_code, :string
    add_column :sold_vehicles, :group, :string
    add_column :sold_vehicles_load, :group, :string
  end
end
