class AddCountryToFlatRate < ActiveRecord::Migration
  def change
    add_column :flat_rates, :country, :string, null: false, default: 'United States of America'
  end
end
