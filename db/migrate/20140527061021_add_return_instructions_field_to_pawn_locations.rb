class AddReturnInstructionsFieldToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :return_instructions, :text
  end
end
