class CreateShipments < ActiveRecord::Migration
  def change
    create_table :shipments do |t|
      t.string :easypost_shipment_id, null: false
      t.string :tracking_code,        null: false
      t.timestamps
    end
  end
end
