class AddSalesRepEmailToFlatRateOfferBatches < ActiveRecord::Migration
  def change
  	add_column :flat_rate_offer_batches, :sales_rep_email, :string
  end
end
