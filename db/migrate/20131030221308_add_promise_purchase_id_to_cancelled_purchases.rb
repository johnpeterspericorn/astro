class AddPromisePurchaseIdToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :promise_purchase_id, :integer
    add_index  :cancelled_purchases, :promise_purchase_id, unique: true
  end
end
