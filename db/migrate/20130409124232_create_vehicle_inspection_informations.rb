class CreateVehicleInspectionInformations < ActiveRecord::Migration
  def change
    create_table :vehicle_inspection_informations do |t|
      t.belongs_to :promise_offer, null: false
      t.boolean    :presale_inspection
      t.float      :inspection_only_price
      t.float      :inspection_bundle_price

      t.timestamps
    end

    add_index :vehicle_inspection_informations, :promise_offer_id
  end
end
