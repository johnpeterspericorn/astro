class AddExpirationDateToFlatRateOffeBatches < ActiveRecord::Migration
  def change
    add_column :flat_rate_offer_batches, :expiration_date, :datetime
  end
end
