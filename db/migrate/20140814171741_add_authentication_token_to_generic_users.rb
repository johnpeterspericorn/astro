class AddAuthenticationTokenToGenericUsers < ActiveRecord::Migration
  def change
    add_column :generic_users, :authentication_token, :string
  end
end
