class AddPartnerFieldInOffersAndPurchases < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :partner, :string
    add_column :promise_offers, :partner, :string
    add_column :promise_purchases, :partner, :string
  end
end
