require_relative '../database_default_values'

class AddNullConstraintToFlatRateOffersMilesSelected < ActiveRecord::Migration
  include DatabaseDefaultValues

  def change
    change_column_null :flat_rate_offers, :miles_selected, false, MISSING_NOT_NULL_MILES
  end
end
