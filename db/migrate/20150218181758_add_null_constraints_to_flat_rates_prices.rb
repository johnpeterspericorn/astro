require_relative '../database_default_values'

class AddNullConstraintsToFlatRatesPrices < ActiveRecord::Migration
  include DatabaseDefaultValues

  COLUMNS = [
      :low_price_500,
      :high_price_500,
      :target_price_500,
      :low_price_250_psi,
      :high_price_250_psi,
      :target_price_250_psi,
      :low_price_500_psi,
      :high_price_500_psi,
      :target_price_500_psi,
  ]

  def up
    COLUMNS.each do |column|
      change_column_null :flat_rates, column, false, MISSING_NOT_NULL_PRICE
    end
  end

  def down
    COLUMNS.each do |column|
      change_column_null :flat_rates, column, true
    end
  end
end
