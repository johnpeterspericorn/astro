class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :promise_purchase, null: false
      t.string :order_id
      t.string :authorization_code
      t.string :error_message
      t.timestamps
    end
  end
end
