class AddOraclesiteToFloorPlanBranches < ActiveRecord::Migration
  def change
    add_column :floor_plan_branches, :oracle_site, :string
  end
end
