class AddUsernameToPawnUsers < ActiveRecord::Migration
  def change
    add_column :pawn_users, :username, :string
  end
end
