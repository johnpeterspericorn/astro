class AddPromisePrepaidColumnToSoldVehiclesAndPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :promise_prepaid, :boolean
    add_column :presale_vehicles, :promise_prepaid, :boolean
  end
end
