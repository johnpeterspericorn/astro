class RemoveChannelFromSoldVehicles < ActiveRecord::Migration
  def up
    remove_column :sold_vehicles, :channel
  end

  def down
    add_column :sold_vehicles, :channel, :string, null: false, default: "unknown"
  end
end
