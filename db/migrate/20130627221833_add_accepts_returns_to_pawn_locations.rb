class AddAcceptsReturnsToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :accepts_returns, :boolean, default: true
  end
end
