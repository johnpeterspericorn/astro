class RenameSuperUsersToGenericUsers < ActiveRecord::Migration
  def change
    rename_table :super_users, :generic_users
  end
end
