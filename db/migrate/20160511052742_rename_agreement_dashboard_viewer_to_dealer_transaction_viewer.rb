class RenameAgreementDashboardViewerToAgreementTransactionViewer < ActiveRecord::Migration
  def change
    rename_column :users, :agreement_dashboard_viewer, :agreement_transaction_viewer
  end
end
