class RemoveResponseColumnsFromPayments < ActiveRecord::Migration
  def up
    execute <<-SQL
      INSERT INTO payment_responses (payment_id, promise_purchase_id, error_message, raw_response, created_at, updated_at)
      SELECT id, promise_purchase_id, error_message, '<MISSING>', current_timestamp, current_timestamp
      FROM payments
    SQL

    change_table :payments do |t|
      t.remove :promise_purchase_id
      t.remove :error_message
    end
  end

  def down
    change_table :payments do |t|
      t.belongs_to :promise_purchase
      t.string :error_message
    end

    execute <<-SQL
      WITH responses AS (
        DELETE FROM payment_responses
        WHERE payment_id IS NOT NULL
        RETURNING *
      )
      UPDATE payments
      SET promise_purchase_id = responses.promise_purchase_id,
          error_message = responses.error_message
      FROM responses
      WHERE payments.id = responses.payment_id
    SQL

    change_column_null :payments, :promise_purchase_id, false
  end
end
