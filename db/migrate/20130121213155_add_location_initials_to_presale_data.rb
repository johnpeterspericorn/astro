class AddLocationInitialsToPresaleData < ActiveRecord::Migration
  def change
    add_column :presale_data, :location_initials, :string
  end
end
