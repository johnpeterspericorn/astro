class AddLocationInitialsToPawnUser < ActiveRecord::Migration
  def change
    add_column :pawn_users, :location_initials, :string
  end
end
