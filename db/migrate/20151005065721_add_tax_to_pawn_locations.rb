class AddTaxToPawnLocations < ActiveRecord::Migration
  def change
    change_table :pawn_locations do |t|
      t.float :tax_rate
    end
  end
end
