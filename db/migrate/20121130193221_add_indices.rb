class AddIndices < ActiveRecord::Migration
  def change
    add_index :vehicles, :vnum
    add_index :vehicles, :agreement_no
    add_index :vehicles, :pawn_access_no
    add_index :vehicles, :badge_no
  end
end
