class UpdatePawnLocations < ActiveRecord::Migration
  def change
    change_column_null :pawn_locations, :country, false, "United States of America"
    change_column_null :pawn_locations, :partner_network, false, "US-Astroheim"
  end
end
