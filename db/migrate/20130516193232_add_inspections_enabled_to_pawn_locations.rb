class AddInspectionsEnabledToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :inspections_enabled, :boolean, default: false
  end
end
