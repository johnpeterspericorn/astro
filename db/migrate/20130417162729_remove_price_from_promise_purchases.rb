class RemovePriceFromPromisePurchases < ActiveRecord::Migration
  def up
    remove_column :promise_purchases, :price
  end

  def down
    add_column :promise_purchases, :price, :float
  end
end
