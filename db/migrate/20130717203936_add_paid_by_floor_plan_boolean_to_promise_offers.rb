class AddPaidByFloorPlanBooleanToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :paid_by_floor_plan, :boolean
  end
end
