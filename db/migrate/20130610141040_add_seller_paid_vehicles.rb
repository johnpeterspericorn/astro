class AddSellerPaidVehicles < ActiveRecord::Migration
  def change
    add_column :presale_vehicles, :seller_paid, :boolean, default: false
    add_column :sold_vehicles,    :seller_paid, :boolean, default: false
  end
end
