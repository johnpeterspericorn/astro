class AddChangingUserToPromiseOffers < ActiveRecord::Migration
  def change
    change_table :promise_offers do |t|
      t.references :changing_user, polymorphic: true
    end
  end
end
