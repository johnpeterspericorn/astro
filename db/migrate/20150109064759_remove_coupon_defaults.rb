class RemoveCouponDefaults < ActiveRecord::Migration
  def change
    remove_column :coupons, :code
    add_column :coupons, :code, :string, limit: 6, null: false
    remove_column :coupons, :amount
    add_column :coupons, :amount, :float, null: false
  end
end
