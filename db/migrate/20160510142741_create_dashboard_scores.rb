class CreateDashboardScores < ActiveRecord::Migration
  def change
    create_table :dashboard_scores do |t|
      t.integer :buyer_num, null: false, index: true
      t.string :payment_kind, null: false
      t.float :avg_loss, :return_rate, :volume, :margin, :earnings
      t.integer :return_score, :purchase_quality, :margin_score, :volume_score, :earnings_score
      t.date :date
    end
  end
end
