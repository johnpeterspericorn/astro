class AddExtensionDaysToPromisePurchases < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :extension_days, :integer
    add_column :promise_purchases, :extension_reason, :string, limit: 255
  end
end
