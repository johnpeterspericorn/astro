class CreatePawnLocations < ActiveRecord::Migration
  def change
    create_table :pawn_locations do |t|
      t.string :initials
      t.string :name

      t.timestamps
    end
  end
end
