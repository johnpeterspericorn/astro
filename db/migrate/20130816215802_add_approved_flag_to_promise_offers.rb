class AddApprovedFlagToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :approved, :boolean, default: false
  end
end
