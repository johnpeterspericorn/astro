class AddExpirationDateToAgreementBlacklists < ActiveRecord::Migration
  def change
    add_column :agreement_blacklists, :expiration_date, :date
  end
end
