class RenameColumnEmailInPromiseOffersToEmails < ActiveRecord::Migration
  def change
    rename_column :promise_offers, :email, :emails
    change_column :promise_offers, :emails, :text
  end
end
