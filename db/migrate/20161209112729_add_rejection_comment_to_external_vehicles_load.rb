class AddRejectionCommentToExternalVehiclesLoad < ActiveRecord::Migration
  def change
    add_column :external_vehicles_loads, :rejection_comment, :text
  end
end
