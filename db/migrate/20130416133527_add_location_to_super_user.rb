class AddLocationToSuperUser < ActiveRecord::Migration
  def change
    add_column :super_users, :location_initials, :string
  end
end
