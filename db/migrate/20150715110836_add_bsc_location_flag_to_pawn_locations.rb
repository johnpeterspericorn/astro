class AddBscLocationFlagToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :bsc_location, :boolean, default: false, null: false
  end
end
