class AddBuyFeeToPromiseSelectionsAndSoldVehicles < ActiveRecord::Migration
  def change
    add_column :promise_selections, :buy_fee, :float
    add_column :sold_vehicles,        :buy_fee, :float
  end
end
