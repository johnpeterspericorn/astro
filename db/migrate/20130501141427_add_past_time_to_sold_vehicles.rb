class AddPastTimeToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :past_time, :boolean
  end
end
