class CreateAgreementRiskFactors < ActiveRecord::Migration
  def change
    create_table :agreement_risk_factors do |t|
      t.string     :agreement_number, null: false
      t.belongs_to :risk_factor,   null: false
      t.timestamps
    end
    add_index :agreement_risk_factors, :agreement_number, unique: true
    add_index :agreement_risk_factors, :risk_factor_id
  end
end
