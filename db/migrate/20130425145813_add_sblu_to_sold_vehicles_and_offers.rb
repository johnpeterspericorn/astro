class AddSbluToSoldVehiclesAndOffers < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :sblu, :string
    add_column :pawn_information, :sblu, :string
  end
end
