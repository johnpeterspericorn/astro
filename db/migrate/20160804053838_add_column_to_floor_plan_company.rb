class AddColumnToFloorPlanCompany < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :skip_ds_payment, :boolean
  end
end
