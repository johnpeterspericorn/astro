class CreateFloorPlanBranchSelections < ActiveRecord::Migration
  def change
    create_table :floor_plan_branch_selections do |t|
      t.string :company_code, null: false
      t.string :site_no, null: false
      t.string :agreement_no, null: false
      t.timestamps
    end
  end
end
