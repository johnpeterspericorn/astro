class RemovePromiseOffersCheckedOutInTrainsColumn < ActiveRecord::Migration
  def change
    remove_column :promise_offers, :checked_out_in_trains
  end
end
