class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :code, default: '', null: false
      t.float :amount, default: 0.0, null: false
      t.datetime :expiration_date, null: false

      t.timestamps
    end
  end
end
