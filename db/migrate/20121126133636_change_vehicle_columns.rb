class ChangeVehicleColumns < ActiveRecord::Migration
  def change
    rename_column :vehicles, :pawn, :universal_no
    rename_column :vehicles, :dlrno,   :pawn_access_no
    rename_column :vehicles, :uid,     :agreement_no
    rename_column :vehicles, :badge,   :badge_no
  end
end
