class AddCarrierToShipment < ActiveRecord::Migration
  def change
    add_column :shipments, :carrier, :string
  end
end
