class AddPriceColumnsToFlatRateOffers < ActiveRecord::Migration
  def up
    add_column :flat_rate_offers, :target_price, :float
    add_column :flat_rate_offers, :target_price_for_miles, :float
  end

  def down
    remove_column :flat_rate_offers, :target_price
    remove_column :flat_rate_offers, :target_price_for_miles
  end
end
