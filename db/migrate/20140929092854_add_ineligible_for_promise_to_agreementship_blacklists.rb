class AddIneligibleForPromiseToAgreementshipBlacklists < ActiveRecord::Migration
  def change
    add_column :agreementship_blacklists, :ineligible_for_promise, :boolean, default: false, null: false
  end
end
