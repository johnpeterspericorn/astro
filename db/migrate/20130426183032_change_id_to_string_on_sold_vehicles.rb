class ChangeIdToStringOnSoldVehicles < ActiveRecord::Migration
  def change
    change_column :sold_vehicles, :id, :string
  end
end
