class AddIndexToInspectionPurchasesTable < ActiveRecord::Migration
  def change
    add_index :inspection_purchases, :promise_offer_id
  end
end
