class ChangeIdToStringOnPresaleVehicles < ActiveRecord::Migration
  def change
    change_column :presale_vehicles, :id, :string
  end
end
