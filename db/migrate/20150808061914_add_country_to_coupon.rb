class AddCountryToCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :country, :string, null: false, default: 'United States of America'
  end
end
