class AddFlatRateOfferBatchIdToFlatRateOffers < ActiveRecord::Migration
  def change
    add_column :flat_rate_offers, :flat_rate_offer_batch_id, :integer, null: false
  end
end
