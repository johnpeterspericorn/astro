class RemoveNullConstraintsFromFlatRateOffers < ActiveRecord::Migration
  def change
    change_column_null :flat_rate_offers, :agreement_name, true
    change_column_null :flat_rate_offers, :agreement_email, true
    change_column_null :flat_rate_offers, :agreement_cell, true
  end
end
