class AddOveFlagToVehiclesAndPresaleData < ActiveRecord::Migration
  def change
    add_column :vehicles,     :ove, :boolean
    add_column :presale_data, :ove, :boolean
  end
end
