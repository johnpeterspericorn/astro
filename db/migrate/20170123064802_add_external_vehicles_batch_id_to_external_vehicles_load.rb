class AddExternalVehiclesBatchIdToExternalVehiclesLoad < ActiveRecord::Migration
  def change
    add_column :external_vehicles_loads, :external_vehicles_batch_id, :integer
  end
end
