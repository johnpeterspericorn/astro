class AddLocationInitialsToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :location_initials, :string
  end
end
