class AddSellerNo < ActiveRecord::Migration
  def change
    add_column :sold_vehicles,       :seller_no, :string
    add_column :pawn_information, :seller_no, :string
  end
end
