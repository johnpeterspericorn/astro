class AddFinanceAdminViewerRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :finance_admin_viewer, :boolean, null: false, default: false
  end
end
