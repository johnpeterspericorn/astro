class CreatePriceFactors < ActiveRecord::Migration
  def change
    create_table :price_factors do |t|
      t.belongs_to :promise_offer, null: false
      t.integer :age_rating
      t.integer :bid_risk_rating
      t.integer :autocheck_rating
      t.integer :mid_rating
      t.integer :mmr_rating
      t.integer :odometer_rating
      t.integer :seller_rating

      t.timestamps
    end

    add_index :price_factors, :promise_offer_id
  end
end
