class CreateFailovers < ActiveRecord::Migration
  def change
    create_table :failovers do |t|
      t.datetime :began_at
      t.datetime :ended_at

      t.timestamps
    end
  end
end
