class AddPromisePurchase < ActiveRecord::Migration
  def change
    create_table :promise_purchases do |t|
      t.integer :vehicle_id
      t.integer :distance
      t.integer :additional_days

      t.timestamps
    end
  end
end
