class MoveSoldVehiclesPastTimeToMustBePromisedAt < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE sold_vehicles
      SET
        must_be_promised_at = '2018-05-19 17:00:00'
      WHERE sold_vehicles.past_time = true
    SQL

    remove_column :sold_vehicles, :past_time
  end

  def down

    add_column :sold_vehicles, :past_time, :boolean

    execute <<-SQL
      UPDATE sold_vehicles
      SET
        past_time = true
      WHERE sold_vehicles.must_be_promised_at IS NOT NULL
    SQL

  end
end
