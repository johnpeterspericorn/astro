class CreateTokenTable < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.string   :service_name
      t.string   :access_token
      t.datetime :expires_at
      t.timestamps
    end
  end
end
