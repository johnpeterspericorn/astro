class RemoveLocationInitialsFromReturnInvoices < ActiveRecord::Migration
  def change
    remove_column :return_invoices, :location_initials
  end
end
