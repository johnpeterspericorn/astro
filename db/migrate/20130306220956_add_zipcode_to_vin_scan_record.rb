class AddZipcodeToVinScanRecord < ActiveRecord::Migration
  def change
    add_column :vnum_scan_records, :zipcode, :string
  end
end
