class RemovePromise115500FromPresaleVehicles < ActiveRecord::Migration
  def up
    remove_column :presale_vehicles, :promise115_500
  end

  def down
    add_column :presale_vehicles, :promise115_500, :integer
  end
end
