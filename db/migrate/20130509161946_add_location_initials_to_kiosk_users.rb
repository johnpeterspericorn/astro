class AddLocationInitialsToKioskUsers < ActiveRecord::Migration
  def change
    add_column :kiosk_users, :location_initials, :string
  end
end
