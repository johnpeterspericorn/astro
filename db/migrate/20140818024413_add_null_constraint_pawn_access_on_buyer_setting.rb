class AddNullConstraintPawnAccessOnBuyerSetting < ActiveRecord::Migration
  def change
    execute <<-SQL
      DELETE FROM buyer_settings
      WHERE buyer_settings.pawn_access_no IS NULL
    SQL

    change_column_null :buyer_settings, :pawn_access_no, false
  end
end
