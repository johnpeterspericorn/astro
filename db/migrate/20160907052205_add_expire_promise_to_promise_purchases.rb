class AddExpirePromiseToPromisePurchases < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :expire_promise, :boolean
  end
end
