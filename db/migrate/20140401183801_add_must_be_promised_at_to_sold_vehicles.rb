class AddMustBePromisedAtToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :must_be_promised_at, :datetime
  end
end
