class RemoveKioskUsers < ActiveRecord::Migration
  def update_polymorphic_references(table_name, polymorphic_column)
    execute <<-SQL
      UPDATE #{table_name}
      SET #{polymorphic_column}_type = 'GenericUser', #{polymorphic_column}_id = generic_users.id
      FROM generic_users
      JOIN kiosk_users
      ON kiosk_users.username = generic_users.username
      WHERE #{polymorphic_column}_type = 'KioskUser' AND kiosk_users.id = #{polymorphic_column}_id
    SQL
  end

  def undo_polymorphic_references(table_name, polymorphic_column)
    execute <<-SQL
      UPDATE #{table_name}
      SET #{polymorphic_column}_type = 'KioskUser', #{polymorphic_column}_id = kiosk_users.id
      FROM kiosk_users
      JOIN generic_users
      ON kiosk_users.username = generic_users.username
      WHERE #{polymorphic_column}_type = 'GenericUser' AND generic_users.id = #{polymorphic_column}_id
    SQL
  end

  POLYMORPHIC_TABLES = {
    cancelled_purchases: %w(purchasing_user cancelling_user),
    flat_rate_offers: %w(offering_user accepting_user),
    promise_offers: %w(changing_user),
    promise_purchases: %w(purchasing_user),
    return_invoices: %w(returning_user cancelling_user),
  }

  def up
    execute <<-SQL
      INSERT INTO generic_users (username, encrypted_password, created_at, updated_at, location_initials, active, authentication_token, kiosk_user)
      SELECT                     username, encrypted_password, created_at, updated_at, location_initials, active, authentication_token, true
      FROM kiosk_users
    SQL

    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        update_polymorphic_references(table, column)
      end
    end

    drop_table :kiosk_users
  end

  def down
    create_table :kiosk_users do |t|
      t.string   :username, default: '', null: false
      t.string   :encrypted_password, default: '', null: false
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.string   :location_initials
      t.boolean  :active, default: true
      t.string   :authentication_token
    end

    execute <<-SQL
      INSERT INTO kiosk_users (username, encrypted_password, created_at, updated_at, location_initials, active, authentication_token)
      SELECT                   username, encrypted_password, created_at, updated_at, location_initials, active, authentication_token
      FROM generic_users
      WHERE kiosk_user IS TRUE
    SQL

    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        undo_polymorphic_references(table, column)
      end
    end

    execute 'DELETE FROM generic_users WHERE kiosk_user IS TRUE'
  end
end
