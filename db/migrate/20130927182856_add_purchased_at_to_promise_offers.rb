class AddPurchasedAtToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :purchased_at, :datetime
  end
end
