class AddReturnInvoiceIdToFloorPlanBranchSelections < ActiveRecord::Migration
  def change
    add_column :floor_plan_branch_selections, :return_invoice_id, :integer
  end
end
