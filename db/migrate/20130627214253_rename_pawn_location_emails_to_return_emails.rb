class RenamePawnLocationEmailsToReturnEmails < ActiveRecord::Migration
  def change
    rename_column :pawn_locations, :emails, :return_emails
  end
end
