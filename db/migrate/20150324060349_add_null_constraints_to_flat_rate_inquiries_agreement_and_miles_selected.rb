require_relative '../database_default_values'

class AddNullConstraintsToFlatRateInquiriesAgreementAndMilesSelected < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    change_column_null :flat_rate_inquiries, :miles_selected, false, MISSING_NOT_NULL_MILES
    change_column_null :flat_rate_inquiries, :agreement_no, false, MISSING_NOT_NULL_STRING
  end

  def down
    change_column_null :flat_rate_inquiries, :miles_selected, true, MISSING_NOT_NULL_MILES
    change_column_null :flat_rate_inquiries, :agreement_no, true, MISSING_NOT_NULL_STRING
  end
end
