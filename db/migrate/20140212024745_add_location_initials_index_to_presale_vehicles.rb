class AddLocationInitialsIndexToPresaleVehicles < ActiveRecord::Migration
  def change
    add_index :presale_vehicles, :location_initials
  end
end
