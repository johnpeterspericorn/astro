class DropErrorsTable < ActiveRecord::Migration
  def up
    drop_table :errors
  end

  def down
    create_table :errors do |t|
      t.string :message, null: false
      t.text   :backtrace
      t.timestamps
    end
  end
end
