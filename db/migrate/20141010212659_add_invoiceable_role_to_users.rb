class AddInvoiceableRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :invoiceable, :boolean, null: false, default: false
  end
end
