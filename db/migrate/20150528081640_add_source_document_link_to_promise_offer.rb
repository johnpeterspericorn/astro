class AddSourceDocumentLinkToPromiseOffer < ActiveRecord::Migration
  def change
    add_column :promise_offers, :source_document_link, :string
  end
end
