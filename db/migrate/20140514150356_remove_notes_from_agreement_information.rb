class RemoveNotesFromAgreementInformation < ActiveRecord::Migration
  def up
    remove_column :agreement_information, :notes
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
