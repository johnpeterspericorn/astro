class AddColumnsToSoldVehiclesLoad < ActiveRecord::Migration
  def change
    change_table :sold_vehicles_load do |t|
      t.string   "partner"
      t.integer  "percent_coverage"
      t.boolean  "limited_volume", default: false, null: false
    end
  end
end
