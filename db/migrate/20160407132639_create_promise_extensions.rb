class CreatePromiseExtensions < ActiveRecord::Migration
  def change
    create_table :promise_extensions do |t|
      t.string :vnum, null: false
      t.integer :additional_days

      t.timestamps
    end
  end
end
