class AddLocationInitialsToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :location_initials, :string
  end
end
