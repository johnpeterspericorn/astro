class AddHoldsTitleToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :holds_title, :boolean, null: false, default: true
  end
end
