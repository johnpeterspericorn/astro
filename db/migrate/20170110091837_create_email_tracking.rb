class CreateEmailTracking < ActiveRecord::Migration
  def change
    create_table :email_tracking do |t|
      t.string    :unique_code
      t.integer   :action_type_id
      t.string    :email
      t.string    :ip_address
      t.datetime  :letter_sent
      t.datetime  :letter_opened
      t.timestamps
    end
  end
end
