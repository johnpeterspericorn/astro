class RenamePromiseOffersCheckedOutToLeftLot < ActiveRecord::Migration
  def change
    rename_column :promise_offers, :checked_out, :left_lot
  end
end
