class CreateFlatRateOfferChangeProposals < ActiveRecord::Migration
  def change
    create_table :flat_rate_offer_change_proposals do |t|
      t.belongs_to :flat_rate_offer, null: false
      t.float      :price,           null: false
    end
  end
end
