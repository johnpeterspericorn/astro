class AddOdometerOnReturnToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :odometer_on_return, :integer
  end
end
