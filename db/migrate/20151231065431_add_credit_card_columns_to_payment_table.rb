class AddCreditCardColumnsToPaymentTable < ActiveRecord::Migration
  def change
    change_table :payments do |t|
      t.string :credit_card_type
      t.integer :credit_card_number
    end
  end
end
