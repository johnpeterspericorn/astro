require_relative '../database_default_values'

class AddNullConstraintToMakeAndVinOnSoldVehiclesAndVehicleInformation < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    %w(sold_vehicles vehicle_information).each do |table_name|
      %w(make vnum).each do |column_name|
        execute "UPDATE #{table_name} SET #{column_name} = '#{MISSING_NOT_NULL_STRING}' WHERE #{column_name} IS NULL"
        change_column_null table_name, column_name, false
      end
    end
  end

  def down
    %w(sold_vehicles vehicle_information).each do |table_name|
      %w(make vnum).each do |column_name|
        change_column_null table_name, column_name, false
        execute "UPDATE #{table_name} SET #{column_name} = NULL WHERE #{column_name} = '#{MISSING_NOT_NULL_STRING}'"
      end
    end
  end
end
