class CreateFloorPlanBranches < ActiveRecord::Migration
  def change
    create_table :floor_plan_branches do |t|
      t.string :site_no, null: false
      t.string :name
      t.string :company_code, null: false

      t.string :address_street
      t.string :address_suite
      t.string :address_city
      t.string :address_state
      t.string :address_zipcode
      t.string :country_code, default: 'us', null: false

      t.timestamps
    end

    add_index :floor_plan_branches, :site_no, unique: true
  end
end
