class RenamePendingBlacklistsToAgreementBlacklists < ActiveRecord::Migration
  def change
    rename_table :pending_blacklists, :agreement_blacklists
  end
end
