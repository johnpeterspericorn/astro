class CreateInspectionPurchases < ActiveRecord::Migration
  def change
    create_table :inspection_purchases do |t|
      t.integer :promise_offer_id

      t.timestamps
    end
  end
end
