class AddVehicleCodeAndPromisePriceThresholdToPresaleVehicles < ActiveRecord::Migration
  def change
    add_column :presale_vehicles, :vehicle_code,              :integer
    add_column :presale_vehicles, :promise_price_threshold, :integer
  end
end
