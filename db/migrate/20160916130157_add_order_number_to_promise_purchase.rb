class AddOrderNumberToPromisePurchase < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :order_number, :string
  end
end
