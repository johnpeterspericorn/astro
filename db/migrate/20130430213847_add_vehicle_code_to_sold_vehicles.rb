class AddVehicleCodeToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :vehicle_code, :integer
  end
end
