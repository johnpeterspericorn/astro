class AddPercentLimitedVolumnToPromiseOffers < ActiveRecord::Migration
 def change
    change_table :promise_offers do |t|
      t.integer :percent_coverage
      t.boolean :limited_volume
    end
  end
end
