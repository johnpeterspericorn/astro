class CreateBuyerInformation < ActiveRecord::Migration
  def change
    create_table :buyer_information do |t|
      t.string :pawn_access_no, null: false
      t.string :agreement_no,         null: false

      t.string :name
      t.string :email
      t.string :cell_phone
      t.string :office_phone

      t.timestamps
    end

    add_index :buyer_information, [:pawn_access_no, :agreement_no], unique: true
  end
end
