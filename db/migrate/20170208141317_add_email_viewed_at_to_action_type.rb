class AddEmailViewedAtToActionType < ActiveRecord::Migration
  def up
    add_column :action_types, :email_viewed_at, :datetime
  end

  def down
    remove_column :action_types, :email_viewed_at
  end
end
