class AddSuperUserFlagToSuperUsers < ActiveRecord::Migration
  def change
    add_column :super_users, :super_user, :boolean
    change_column_null :super_users, :super_user, false, true
    change_column_default :super_users, :super_user, false
  end
end
