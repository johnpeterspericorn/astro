class AddKioskUserToGenericUsers < ActiveRecord::Migration
  def change
    add_column :generic_users, :kiosk_user, :boolean, null: false, default: false
  end
end
