class RenameVehiclesToSoldVehicles < ActiveRecord::Migration
  def change
    rename_table :vehicles, :sold_vehicles
  end
end
