class CreateRelationshipManagers < ActiveRecord::Migration
    def up
    create_table :relationship_managers do |t|
      t.string :name
      t.text :email
      t.timestamps null: false
    end
    execute <<-SQL
      INSERT INTO relationship_managers(name, email, created_at, updated_at) VALUES ('Felicia Rawles', 'felicia.rawles@AgreementShield.com', now(), now());
      INSERT INTO relationship_managers(name, email, created_at, updated_at) VALUES ('Kyle Moody', 'Kyle.Moody@AgreementShield.com', now(), now());
      INSERT INTO relationship_managers(name, email, created_at, updated_at) VALUES ('Ryan Stokeling', 'Ryan.Stokeling@Astroheim.com', now(), now());
    SQL
  end

  def down
    drop_table :relationship_managers
  end
end
