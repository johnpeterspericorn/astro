class CreatePaymentResponses < ActiveRecord::Migration
  def change
    create_table :payment_responses do |t|
      t.belongs_to :promise_purchase, null: false
      t.belongs_to :payment

      t.string :error_message
      t.text   :raw_response, null: false

      t.timestamps
    end

    add_index :payment_responses, :payment_id, unique: true
  end
end
