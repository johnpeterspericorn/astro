class DropBuyerSettings < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE buyer_information
      SET send_updates = COALESCE(buyer_settings.send_updates, false)
      FROM buyer_settings
      WHERE buyer_information.pawn_access_no = buyer_settings.pawn_access_no
    SQL

    drop_table :buyer_settings
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
