class AddCancelledAtToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :cancelled_at, :datetime
    add_index :return_invoices, :cancelled_at
  end
end
