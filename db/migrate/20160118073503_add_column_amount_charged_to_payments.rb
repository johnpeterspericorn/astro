class AddColumnAmountChargedToPayments < ActiveRecord::Migration
  def up
    add_column :payments, :amount, :float
  end

  def down
    remove_column :payments, :amount
  end
end
