class AddPromisedAtToPromiseSelection < ActiveRecord::Migration
  def change
    add_column :promise_selections, :promised_at, :datetime
  end
end
