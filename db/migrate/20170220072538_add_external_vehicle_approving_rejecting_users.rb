class AddExternalVehicleApprovnumgRejectingUsers < ActiveRecord::Migration
  def up
    add_column :external_vehicles_loads, :approvnumg_user_id, :integer
    add_column :external_vehicles_loads, :rejecting_user_id, :integer
  end

  def down
    remove_column :external_vehicles_loads, :approvnumg_user_id
    remove_column :external_vehicles_loads, :rejecting_user_id
  end
end
