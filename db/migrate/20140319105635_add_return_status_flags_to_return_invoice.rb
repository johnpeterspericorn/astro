class AddReturnStatusFlagsToReturnInvoice < ActiveRecord::Migration
  def change
    add_column :return_invoices, :vehicle_received, :boolean, default: false
    add_column :return_invoices, :title_received, :boolean, default: false
    add_column :return_invoices, :refund_processed, :boolean, default: false
  end
end
