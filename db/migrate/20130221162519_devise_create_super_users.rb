class DeviseCreateSuperUsers < ActiveRecord::Migration
  def change
    create_table(:super_users) do |t|
      ## Database authenticatable
      t.string :username,           :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""
      t.timestamps
    end

    add_index :super_users, :username,             :unique => true
  end
end
