class AddDefaultValuesToCountyAndPartnerNetworkInPawnLocations < ActiveRecord::Migration
  def change
    change_column_default(:pawn_locations, :partner_network, "US-Astroheim")
    change_column_default(:pawn_locations, :country, "United States of America")
  end
end
