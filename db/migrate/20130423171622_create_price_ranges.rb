class CreatePriceRanges < ActiveRecord::Migration
  def change
    create_table :price_ranges do |t|
      t.belongs_to :risk_factor,      null: false
      t.integer    :vehicle_category, null: false
      t.float      :lower_limit,      null: false
      t.float      :upper_limit,      null: false
      t.timestamps
    end
    add_index :price_ranges, [:risk_factor_id, :vehicle_category], unique: true
  end
end
