class AddLocationInitialsToPromiseSelections < ActiveRecord::Migration
  def change
    add_column :promise_selections, :location_initials, :string
  end
end
