class CreateExternalVehiclesLoads < ActiveRecord::Migration
  def change
    create_table :external_vehicles_loads do |t|
      t.string   "model"
      t.integer  "odometer_reading"
      t.integer  "year"
      t.string   "make",                                                :null => false
      t.string   "vnum",                                                 :null => false
      t.string   "lane_no",                                             :null => false
      t.string   "run_no",                                              :null => false
      t.string   "universal_no"
      t.string   "pawn_access_no",                                   :null => false
      t.string   "badge_no"
      t.string   "agreement_no"
      t.float    "promise_250"
      t.float    "additional_day"
      t.float    "promise_500"
      t.float    "vehicle_purchase_price",                              :null => false
      t.integer  "odometer_rating"
      t.integer  "autocheck_rating"
      t.integer  "mmr_rating"
      t.integer  "age_rating"
      t.integer  "seller_rating"
      t.integer  "mid_rating"
      t.integer  "bid_risk_rating"
      t.integer  "ineligibility_condition"
      t.boolean  "if_bid"
      t.string   "work_order_number"
      t.string   "location_initials"
      t.boolean  "ove"
      t.boolean  "presale_inspection"
      t.float    "buy_fee"
      t.boolean  "promise_prepaid"
      t.float    "inspection_only_price"
      t.float    "inspection_bundle_price"
      t.boolean  "automatic_purchase"
      t.integer  "preselected_promise_days"
      t.integer  "preselected_promise_miles"
      t.string   "automatic_purchase_email"
      t.boolean  "automatic_purchase_processed", :default => false,     :null => false
      t.integer  "vehicle_code"
      t.integer  "promise_price_threshold"
      t.boolean  "past_time"
      t.boolean  "seller_paid",                  :default => false
      t.string   "sale_no",                                             :null => false
      t.string   "seller_no"
      t.integer  "sblu"
      t.boolean  "bypass_charge",                :default => false,     :null => false
      t.datetime "purchased_at",                                        :null => false
      t.integer  "pending_reason_code"
      t.float    "transport_reimbursement",      :default => 0.0,       :null => false
      t.string   "channel",                      :default => "unknown", :null => false
      t.datetime "must_be_promised_at"
      t.float    "discount",                     default: 0.0,                null: false
      t.text     "discount_description",         default: "",                 null: false
      t.string   "preselected_promise_kind",   default: "purchase_protect", null: false
      t.string   "partner"
      t.integer  "percent_coverage"
      t.string   "limited_volume"
      t.string   "group_code"
      t.string   "group"
      t.string   "source_document_link"
      t.string   "batch_id"
      t.integer  "user_id"
      t.string   "status"
      t.timestamps null: false
    end
  end
end
