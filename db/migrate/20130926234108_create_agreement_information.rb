class CreateAgreementInformation < ActiveRecord::Migration
  def change
    create_table :agreement_information do |t|
      t.string :agreement_no, null: false
      t.string :agreementship_name
      t.string :address_street
      t.string :address_suite
      t.string :address_city
      t.string :address_state
      t.string :address_zipcode

      t.timestamps
    end

    add_index :agreement_information, :agreement_no, unique: true
  end
end
