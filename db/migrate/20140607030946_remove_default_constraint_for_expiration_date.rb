class RemoveDefaultConstraintForExpirationDate < ActiveRecord::Migration
  def change
    remove_column :return_invoices, :expiration_date
    add_column :return_invoices, :expiration_date, :date
  end
end
