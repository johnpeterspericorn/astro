class ChangeBillOfSalesColumnSize < ActiveRecord::Migration
  def change
      change_column :bill_of_sales, :seller_agreement_name, :text
      change_column :bill_of_sales, :seller_address, :text 
      change_column :bill_of_sales, :selling_sales_person_name, :text 
      change_column :bill_of_sales, :selling_sales_person_signature, :text 
      change_column :bill_of_sales, :selling_sales_person_registration_no, :text 
      change_column :bill_of_sales, :selling_hst_registration_no, :text 
      change_column :bill_of_sales, :selling_qst_registration_no, :text 
      change_column :bill_of_sales, :buying_sales_person_name, :text 
      change_column :bill_of_sales, :buying_sales_person_signature, :text 
      change_column :bill_of_sales, :buying_sales_person_registration_no, :text 
      change_column :bill_of_sales, :buying_hst_registration_no, :text 
      change_column :bill_of_sales, :buying_qst_registration_no, :text 
  end
end
