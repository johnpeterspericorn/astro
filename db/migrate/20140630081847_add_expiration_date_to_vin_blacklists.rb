class AddExpirationDateToVinBlacklists < ActiveRecord::Migration
  def change
    add_column :vnum_blacklists, :expiration_date, :date
  end
end
