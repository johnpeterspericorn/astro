class AddBuyerUserFlagToGenericUsers < ActiveRecord::Migration
  def change
    add_column :generic_users, :buyer_user, :boolean, null: false, default: false
  end
end
