class AddReasonAndStatusToPromiseExtensions < ActiveRecord::Migration
  def change
    add_column :promise_extensions, :reason, :string, limit: 255
    add_column :promise_extensions, :status, :string, limit: 30, default: 'new'
  end
end
