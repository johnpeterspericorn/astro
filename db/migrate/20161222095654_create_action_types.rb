class CreateActionTypes < ActiveRecord::Migration
  def up
    create_table :action_types do |t|
      t.text :action_type_name
      t.integer :type_id
      t.datetime :date_sent
      t.string :status
      t.text :emails
      t.text :parameters

      t.timestamps null: false
    end
  end

  def down
    drop_table :action_types
  end
end
