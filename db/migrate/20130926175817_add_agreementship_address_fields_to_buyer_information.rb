class AddAgreementshipAddressFieldsToBuyerInformation < ActiveRecord::Migration
  def change
    %w(agreementship_name address_street address_suite address_city address_state address_zipcode).each do |column|
      add_column :buyer_information, column, :string
    end
  end
end
