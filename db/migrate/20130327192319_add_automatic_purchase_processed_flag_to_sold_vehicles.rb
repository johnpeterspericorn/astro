class AddAutomaticPurchaseProcessedFlagToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :automatic_purchase_processed, :boolean, default: false, null: false
  end
end
