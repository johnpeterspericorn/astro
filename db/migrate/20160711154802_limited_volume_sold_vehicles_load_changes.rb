class LimitedVolumeSoldVehiclesLoadChanges < ActiveRecord::Migration
  def change
    change_column :sold_vehicles_load, :limited_volume, :string, default: nil, null: true
  end
end
