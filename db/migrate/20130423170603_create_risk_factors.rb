class CreateRiskFactors < ActiveRecord::Migration
  def change
    create_table :risk_factors do |t|
      t.float :risk_modifier, null: false
      t.timestamps
    end
    add_index :risk_factors, :risk_modifier, unique: true
  end
end
