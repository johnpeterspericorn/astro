class AddActivityFlagToUsers < ActiveRecord::Migration
  def change
    add_column :super_users, :active, :boolean, default: true
    add_column :kiosk_users, :active, :boolean, default: true
  end
end
