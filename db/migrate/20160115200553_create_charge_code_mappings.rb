require 'yaml'
require_relative '../../app/models/charge_code_mapping'
class CreateChargeCodeMappings < ActiveRecord::Migration
  def self.up
    create_table :charge_code_mappings do |t|
      t.string :product, null: false
      t.boolean :automatic_purchase, null: false
      t.boolean :seller_paid, null: false
      t.integer :charge_code, null: false
      t.string :call_id, null: false
    end
  end

  def self.down
    drop_table :charge_code_mappings
  end
end

