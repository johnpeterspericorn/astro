class ChangeCouponCodeCase < ActiveRecord::Migration
  def change
    execute "UPDATE coupons SET code = UPPER(code)"
  end
end
