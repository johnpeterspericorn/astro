class MoveVehicleInfoFromPromiseOffersToVehicleInformation < ActiveRecord::Migration
  COLUMNS = %w(
    make
    model
    odometer_reading
    ove
    vnum
    year)

  def comma_separated_columns
    COLUMNS.join(', ')
  end

  def column_assignments(source_table)
    COLUMNS.map do |column, assignments|
      "#{column} = #{source_table}.#{column}"
    end.join(', ')
  end

  def up
    execute <<-SQL
      INSERT INTO vehicle_information(promise_offer_id, #{comma_separated_columns}, created_at, updated_at)
      SELECT id, #{comma_separated_columns}, created_at, updated_at FROM promise_offers
    SQL

     change_table :promise_offers do |t|
        COLUMNS.each do |column|
        t.remove column
      end
    end
  end

  def down
    change_table :promise_offers do |t|
      t.string  :make
      t.string  :model
      t.integer :odometer_reading
      t.boolean :ove
      t.string  :vnum
      t.integer :year
    end

    execute <<-SQL
      UPDATE promise_offers
      SET #{column_assignments('vehicle_information')}
      FROM vehicle_information
      WHERE vehicle_information.promise_offer_id = promise_offers.id
    SQL
  end
end
