class AddPendingReasonToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :pending_reason, :string
  end
end
