class AddAgreementNoMilesSelectedLowHighTargetPricesAndPsiToFlatRateInquiries < ActiveRecord::Migration
  def change
    add_column :flat_rate_inquiries, :agreement_no, :string
    add_column :flat_rate_inquiries, :low_price, :float
    add_column :flat_rate_inquiries, :high_price, :float
    add_column :flat_rate_inquiries, :target_price, :float
    add_column :flat_rate_inquiries, :miles_selected, :integer
    add_column :flat_rate_inquiries, :psi_eligible, :boolean, default: false, null: false
  end
end
