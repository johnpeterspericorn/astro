class RemovePaymentResponseErrors < ActiveRecord::Migration
  def up
    drop_table :payment_response_errors
  end

  def down
    create_table :payment_response_errors do |t|
      t.string :error_message, null: false
      t.string :notice_en, null: false
      t.string :notice_fr_CA, null: false
      t.timestamps null: false
    end
  end
end
