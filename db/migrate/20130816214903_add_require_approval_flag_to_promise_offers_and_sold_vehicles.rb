class AddRequireApprovalFlagToPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def change
    %w(promise_offers sold_vehicles).each do |table|
      add_column table, :require_approval, :boolean, default: false
    end
  end
end
