class AddExpirationDateToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :expiration_date, :date, :null => false, :default => Time.current
  end
end
