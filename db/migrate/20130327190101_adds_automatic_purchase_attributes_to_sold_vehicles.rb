class AddsAutomaticPurchaseAttributesToSoldVehicles < ActiveRecord::Migration
  def up
    change_table :sold_vehicles do |t|
      t.integer :preselected_promise_days
      t.integer :preselected_promise_miles
      t.string  :automatic_purchase_email
    end
  end

  def down
    change_table :sold_vehicles do |t|
      t.remove :preselected_promise_days
      t.remove :preselected_promise_miles
      t.remove :automatic_purchase_email
    end
  end
end
