class AddAdminFlagToSuperUsers < ActiveRecord::Migration
  def change
    add_column :super_users, :admin, :boolean, null: false, default: false
  end
end
