class AddAuthenticationTokenToAstroheimUsers < ActiveRecord::Migration
  def change
    add_column :astroheim_users, :authentication_token, :string
    add_index  :astroheim_users, :authentication_token, unique: true
  end
end
