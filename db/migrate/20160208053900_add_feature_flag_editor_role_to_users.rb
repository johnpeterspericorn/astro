class AddFeatureFlagEditorRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :feature_flag_editor, :boolean, null: false, default: false
  end
end
