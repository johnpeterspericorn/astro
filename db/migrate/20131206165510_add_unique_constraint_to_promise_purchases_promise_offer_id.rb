class AddUniqueConstraintToPromisePurchasesPromiseOfferId < ActiveRecord::Migration
  def change
    add_index :promise_purchases, :promise_offer_id, unique: true
  end
end
