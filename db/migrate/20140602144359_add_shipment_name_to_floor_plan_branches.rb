class AddShipmentNameToFloorPlanBranches < ActiveRecord::Migration
  def change
    add_column :floor_plan_branches, :shipment_name, :string
  end
end
