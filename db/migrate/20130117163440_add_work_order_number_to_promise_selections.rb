class AddWorkOrderNumberToPromiseSelections < ActiveRecord::Migration
  def change
    add_column :promise_selections, :work_order_number, :string
  end
end
