class RemoveDisbursementEmailsFromPawnLocations < ActiveRecord::Migration
  def up
    remove_column :pawn_locations, :disbursement_emails
  end

  def down
    add_column :pawn_locations, :disbursement_emails, :text
  end
end
