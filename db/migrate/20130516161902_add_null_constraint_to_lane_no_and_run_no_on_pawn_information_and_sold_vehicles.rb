require_relative '../database_default_values'

class AddNullConstraintToLaneNoAndRunNoOnPawnInformationAndSoldVehicles < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    %w(pawn_information sold_vehicles).each do |table_name|
      %w(lane_no run_no).each do |column_name|
        execute "UPDATE #{table_name} SET #{column_name} = '#{MISSING_NOT_NULL_STRING}' WHERE #{column_name} IS NULL"
        change_column_null table_name, column_name, false
      end
    end
  end

  def down
    %w(pawn_information sold_vehicles).each do |table_name|
      %w(lane_no run_no).each do |column_name|
        change_column_null table_name, column_name, true
        execute "UPDATE #{table_name} SET #{column_name} = NULL WHERE #{column_name} = '#{MISSING_NOT_NULL_STRING}'"
      end
    end
  end
end
