class AddVehicleInfoToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :model, :string
    add_column :vehicles, :trim, :string
  end
end
