class AddLatLngToVinScanRecord < ActiveRecord::Migration
  def change
    add_column :vnum_scan_records, :latitude, :float
    add_column :vnum_scan_records, :longitude, :float
  end
end
