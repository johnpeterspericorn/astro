class AddAgreementNumberIndexForFlatRateOffers < ActiveRecord::Migration
  def change
    add_index :flat_rate_offers, :agreement_no
  end
end
