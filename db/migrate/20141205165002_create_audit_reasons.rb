class CreateAuditReasons < ActiveRecord::Migration
  def change
    create_table :audit_reasons do |t|
      t.belongs_to :audit, null: false
      t.text :description, null: false
      t.timestamps
    end
  end
end
