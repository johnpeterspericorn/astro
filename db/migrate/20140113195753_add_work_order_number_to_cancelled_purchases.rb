class AddWorkOrderNumberToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :work_order_number, :string
  end
end
