class AddTransactionIdToInspectionPurchases < ActiveRecord::Migration
  def change
    add_column :inspection_purchases, :transaction_id, :string
  end
end
