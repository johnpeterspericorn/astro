class AddPurchasingUserToPromisePurchases < ActiveRecord::Migration
  def change
    change_table :promise_purchases do |t|
      t.references :purchasing_user, polymorphic: true
    end
  end
end
