class RemovePawnUsers < ActiveRecord::Migration
  def up
    drop_table :pawn_users
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
