class AddChannelToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :channel, :string, null: false, default: "unknown"
  end
end
