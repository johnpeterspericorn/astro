require_relative '../database_default_values'

class AddNullConstraintToVehiclePurchasePriceOnPawnInformationAndSoldVehicles < ActiveRecord::Migration
  include DatabaseDefaultValues

  def up
    %w(pawn_information sold_vehicles).each do |table_name|
      execute "UPDATE #{table_name} SET vehicle_purchase_price = %f WHERE vehicle_purchase_price IS NULL" % MISSING_NOT_NULL_PRICE
      change_column_null table_name, :vehicle_purchase_price, false
    end
  end

  def down
    %w(pawn_information sold_vehicles).each do |table_name|
      change_column_null table_name, :vehicle_purchase_price, true
      execute "UPDATE #{table_name} SET vehicle_purchase_price = NULL WHERE vehicle_purchase_price = %f" % MISSING_NOT_NULL_PRICE
    end
  end
end
