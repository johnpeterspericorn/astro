class AddDiscountFieldToSoldVehiclesLoad < ActiveRecord::Migration
  def up
    add_column :sold_vehicles_load, :discount, :float, :null => false, :default => 0.0
  end

  def down
    remove_column :sold_vehicles_load, :discount
  end
end
