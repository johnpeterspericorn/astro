class AddEmailToPromiseSelections < ActiveRecord::Migration
  def change
    add_column :promise_selections, :email, :string
  end
end
