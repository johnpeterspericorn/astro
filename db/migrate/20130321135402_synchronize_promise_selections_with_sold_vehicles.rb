class SynchronizePromiseSelectionsWithSoldVehicles < ActiveRecord::Migration
  def change
    add_column :promise_selections, :lane_no, :string
    add_column :promise_selections, :run_no,  :string

    add_column :promise_selections, :promise_250,  :float
    add_column :promise_selections, :promise_500,  :float
    add_column :promise_selections, :additional_day, :float
    add_column :promise_selections, :add_transp,     :float
    add_column :promise_selections, :inspection_only_price,   :float
    add_column :promise_selections, :inspection_bundle_price, :float

    add_column :promise_selections, :odometer_rating,         :integer
    add_column :promise_selections, :autocheck_rating,        :integer
    add_column :promise_selections, :mmr_rating,              :integer
    add_column :promise_selections, :age_rating,              :integer
    add_column :promise_selections, :seller_rating,           :integer
    add_column :promise_selections, :mid_rating,              :integer
    add_column :promise_selections, :bid_risk_rating,         :integer
    add_column :promise_selections, :ineligibility_condition, :integer

    add_column :promise_selections, :if_bid,      :boolean
    add_column :promise_selections, :checked_out, :boolean
    add_column :promise_selections, :ove,         :boolean
    add_column :promise_selections, :presale_inspection, :boolean
    add_column :promise_selections, :automatic_purchase, :boolean
  end
end
