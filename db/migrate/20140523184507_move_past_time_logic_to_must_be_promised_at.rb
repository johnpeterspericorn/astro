class MovePastTimeLogicToMustBePromisedAt < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE promise_offers
      SET
        must_be_promised_at = '2018-05-19 17:00:00'
      WHERE promise_offers.past_time = true
    SQL

    remove_column :promise_offers, :past_time
  end

  def down

    add_column :promise_offers, :past_time, :boolean

    execute <<-SQL
      UPDATE promise_offers
      SET
        past_time = true
      WHERE promise_offers.must_be_promised_at IS NOT NULL
    SQL

  end
end
