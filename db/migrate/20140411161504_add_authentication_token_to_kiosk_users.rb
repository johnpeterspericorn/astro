class AddAuthenticationTokenToKioskUsers < ActiveRecord::Migration
  def change
    add_column :kiosk_users, :authentication_token, :string
  end
end
