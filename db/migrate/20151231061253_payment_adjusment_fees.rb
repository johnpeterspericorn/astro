class PaymentAdjusmentFees < ActiveRecord::Migration
  def change
    create_table :payment_adjustment_fees do |t|
      t.string :item_description
      t.float  :fee_amount
      t.string :adjustment_code
      t.integer :payment_id
    end
  end
end
