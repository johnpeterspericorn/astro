class AddAgreementScoreViewerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :agreement_score_viewer, :boolean
  end
end
