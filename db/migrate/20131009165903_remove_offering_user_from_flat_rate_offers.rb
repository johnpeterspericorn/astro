class RemoveOfferingUserFromFlatRateOffers < ActiveRecord::Migration
  def change
    remove_column :flat_rate_offers, :offering_user_id
    remove_column :flat_rate_offers, :offering_user_type
  end
end
