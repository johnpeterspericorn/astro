class RemoveNullContraintPawnAccessNo < ActiveRecord::Migration
  def change
    change_column_null :sold_vehicles, :pawn_access_no, true
    change_column_null :pawn_information, :pawn_access_no, true
    change_column_null :sold_vehicles_load, :pawn_access_no, true
  end
end
