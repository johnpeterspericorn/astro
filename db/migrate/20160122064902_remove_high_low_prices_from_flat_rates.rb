class RemoveHighLowPricesFromFlatRates < ActiveRecord::Migration
  def up
    remove_column :flat_rates, :low_price_360
    remove_column :flat_rates, :low_price_500
    remove_column :flat_rates, :low_price_360_psi
    remove_column :flat_rates, :low_price_500_psi

    remove_column :flat_rates, :high_price_360
    remove_column :flat_rates, :high_price_500
    remove_column :flat_rates, :high_price_360_psi
    remove_column :flat_rates, :high_price_500_psi
  end

  def down
    add_column :flat_rates, :low_price_360, :float
    add_column :flat_rates, :low_price_500, :float
    add_column :flat_rates, :low_price_360_psi, :float
    add_column :flat_rates, :low_price_500_psi, :float

    add_column :flat_rates, :high_price_360, :float
    add_column :flat_rates, :high_price_500, :float
    add_column :flat_rates, :high_price_360_psi, :float
    add_column :flat_rates, :high_price_500_psi, :float
  end
end
