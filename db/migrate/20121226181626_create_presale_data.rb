class CreatePresaleData < ActiveRecord::Migration
  def change
    create_table :presale_data do |t|
      #odometer_reading, year, model,     make,      LANENO, RUNNO, VNUM,               average_mmr, VNUMrisk,  odometer_rating, autocheck_rating, mmr_rating, age_rating, seller_rating, MID_rating, Promise, g115mmr
      #94459,            2000, MALIBU LS, CHEVROLET, 1,      1,     1G1NE52JXY6101977, 1800,        -0.19889, 2,               5,                5,          1,          5,             5,          125,       225

      t.string   :vnum

      t.string   :odometer_reading
      t.string   :year
      t.string   :model
      t.string   :make

      t.string   :lane_no
      t.string   :run_no

      t.integer  :average_mmr
      t.float    :vnum_risk
      t.integer  :odometer_rating
      t.integer  :autocheck_rating
      t.integer  :mmr_rating
      t.integer  :age_rating
      t.integer  :seller_rating
      t.integer  :mid_rating

      t.integer  :promise_250
      t.integer  :g115_mmr
    end
  end
end
