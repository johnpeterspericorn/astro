class RemoveUnneededFlatRateOffersColumns < ActiveRecord::Migration
  def change
    remove_column :flat_rate_offers, :accepted
    remove_column :flat_rate_offers, :accepted_at
    remove_column :flat_rate_offers, :letter_sent
    remove_column :flat_rate_offers, :agreement_name
    remove_column :flat_rate_offers, :agreement_cell
    remove_column :flat_rate_offers, :agreement_email
    remove_column :flat_rate_offers, :differs_from_auth_service
  end
end
