class CreateFlatRateInquiries < ActiveRecord::Migration
  def change
    create_table :flat_rate_inquiries do |t|
      t.integer  :user_id,           null: false
      t.string   :pawn_access_no, null: false
      t.string   :agreement_name
      t.string   :agreement_phone
      t.string   :agreement_email

      t.timestamps
    end
  end
end
