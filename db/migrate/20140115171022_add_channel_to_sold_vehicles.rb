class AddChannelToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :channel, :string, null: false, default: "unknown"
  end
end
