class CreateReturnInvoices < ActiveRecord::Migration
  def change
    create_table :return_invoices do |t|
      t.belongs_to :promise_purchase

      t.timestamps
    end
    add_index :return_invoices, :promise_purchase_id
  end
end
