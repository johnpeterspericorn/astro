class RemoveBuyerUsers < ActiveRecord::Migration
  def update_polymorphic_references(table_name, polymorphic_column)
    execute <<-SQL
      UPDATE #{table_name}
      SET #{polymorphic_column}_type = 'GenericUser', #{polymorphic_column}_id = generic_users.id
      FROM generic_users
      JOIN buyer_users
      ON buyer_users.username = generic_users.username
      WHERE #{polymorphic_column}_type = 'BuyerUser' AND buyer_users.id = #{polymorphic_column}_id
    SQL
  end

  POLYMORPHIC_TABLES = {
    cancelled_purchases: %w(purchasing_user cancelling_user),
    flat_rate_offers: %w(offering_user accepting_user),
    promise_offers: %w(changing_user),
    promise_purchases: %w(purchasing_user),
    return_invoices: %w(returning_user cancelling_user),
  }

  def up
    execute <<-SQL
      DELETE FROM buyer_users
      USING generic_users
      WHERE buyer_users.username = generic_users.username
    SQL

    execute <<-SQL
      INSERT INTO generic_users (username, encrypted_password,                             created_at, updated_at, active, authentication_token, buyer_user)
      SELECT                     username, '#{BCrypt::Password.create(SecureRandom.hex)}', created_at, updated_at, true,   authentication_token, true
      FROM buyer_users
    SQL

    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        update_polymorphic_references(table, column)
      end
    end

    drop_table :buyer_users
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
