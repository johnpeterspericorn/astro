class CreateRateChanges < ActiveRecord::Migration
 def self.up
    create_table :rate_changes do |t|
      t.references :rate_changing_user
      t.references :link_clicked_user
      t.text :emails, null: false
      t.references :relationship_manager
      t.string :agreement_no
      t.float    :return_rate
      t.datetime :effective_date
      t.datetime  :letter_sent
      t.datetime :link_clicked_at

      t.timestamps
    end
  end

    def self.down
      drop_table :rate_changes
    end
end
