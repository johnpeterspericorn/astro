class RemoveAcceptingUserFromFlatRateOfferBatches < ActiveRecord::Migration
  def up
    remove_column :flat_rate_offer_batches, :accepting_user_id
    remove_column :flat_rate_offer_batches, :accepting_user_type
  end

  def down
    add_column :flat_rate_offer_batches, :accepting_user_id,   :integer
    add_column :flat_rate_offer_batches, :accepting_user_type, :string
  end
end
