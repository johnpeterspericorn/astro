class MoveSoldVehiclesLoadPastTimeToMustBePromisedAt < ActiveRecord::Migration
   def up
    execute <<-SQL
      UPDATE sold_vehicles_load
      SET
        must_be_promised_at = '2018-05-19 17:00:00'
      WHERE sold_vehicles_load.past_time = true
    SQL

    remove_column :sold_vehicles_load, :past_time
  end

  def down

    add_column :sold_vehicles_load, :past_time, :boolean

    execute <<-SQL
      UPDATE sold_vehicles_load
      SET
        past_time = true
      WHERE sold_vehicles_load.must_be_promised_at IS NOT NULL
    SQL

  end
end