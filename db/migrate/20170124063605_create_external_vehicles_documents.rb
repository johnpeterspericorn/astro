class CreateExternalVehiclesDocuments < ActiveRecord::Migration
  def change
    create_table :external_vehicles_documents do |t|
      t.string :document
      t.integer :external_vehicles_batch_id

      t.timestamps null: false
    end
  end
end
