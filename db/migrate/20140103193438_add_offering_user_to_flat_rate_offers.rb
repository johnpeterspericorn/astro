class AddOfferingUserToFlatRateOffers < ActiveRecord::Migration
  def up
    add_column :flat_rate_offers, :offering_user_id,   :integer
    add_column :flat_rate_offers, :offering_user_type, :string

    execute <<-SQL
      UPDATE flat_rate_offers
      SET offering_user_id = flat_rate_offer_batches.offering_user_id, offering_user_type = flat_rate_offer_batches.offering_user_type
      FROM flat_rate_offer_batches
      WHERE flat_rate_offer_batches.id = flat_rate_offers.flat_rate_offer_batch_id
    SQL

    change_column_null :flat_rate_offers, :offering_user_id,   false
    change_column_null :flat_rate_offers, :offering_user_type, false
  end

  def down
    remove_column :flat_rate_offers, :offering_user_id
    remove_column :flat_rate_offers, :offering_user_type
  end
end
