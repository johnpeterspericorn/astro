class AddNullConstraintsToPaymentsOrderIdAndAuthorizationCode < ActiveRecord::Migration
  def up
    %w(order_id authorization_code).each do |column|
      execute <<-SQL
        UPDATE payments
        SET #{column} = '<MISSING>'
        WHERE #{column} IS NULL
      SQL

      change_column_null :payments, column, false
    end
  end

  def down
    change_column_null :payments, :order_id,           false
    change_column_null :payments, :authorization_code, false
  end
end
