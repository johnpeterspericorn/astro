class AddTimestampsToCancelledPurchases < ActiveRecord::Migration
  def change
    add_timestamps :cancelled_purchases
  end
end
