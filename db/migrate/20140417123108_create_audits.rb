class CreateAudits < ActiveRecord::Migration
  def change
    create_table :audits do |t|
      t.belongs_to :auditee, null: false, polymorphic: true
      t.string     :state,   null: false, default: 'opened'
      t.date       :resolved_by, null: false

      t.timestamps
    end
  end
end
