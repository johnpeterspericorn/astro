class DropFlatRateOfferChangeProposals < ActiveRecord::Migration
  def up
    drop_table :flat_rate_offer_change_proposals
  end

  def down
    create_table :flat_rate_offer_change_proposals do |t|
      t.belongs_to :flat_rate_offer, null: false
      t.float      :price,           null: false
      t.timestamps null: false
    end
  end
end
