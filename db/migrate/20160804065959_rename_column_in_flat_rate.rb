class RenameColumnInFlatRate < ActiveRecord::Migration
  def change
    rename_column :flat_rates, :lv_price, :target_price_lv
  end
end
