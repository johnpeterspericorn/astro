class AddNewProductFeeInReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :new_product_fee, :float
  end
end
