class AddIndexesToCouponsTable < ActiveRecord::Migration
  def change
    add_index :coupons, :code
    add_index :coupons, :promise_purchase_id
  end
end
