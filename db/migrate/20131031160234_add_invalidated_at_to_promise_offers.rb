class AddInvalidatedAtToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :invalidated_at, :datetime
  end
end
