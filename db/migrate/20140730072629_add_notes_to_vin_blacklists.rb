class AddNotesToVinBlacklists < ActiveRecord::Migration
  def change
    add_column :vnum_blacklists, :notes, :text, null: false, default: ''
  end
end
