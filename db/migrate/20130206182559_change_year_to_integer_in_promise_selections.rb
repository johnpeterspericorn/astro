class ChangeYearToIntegerInPromiseSelections < ActiveRecord::Migration
  def up
    connection.execute(%q{
      alter table promise_selections
      alter column year
      type integer using cast(year as integer)
    })
  end

  def down
    connection.execute(%q{
      alter table promise_selections
      alter column year
      type integer using cast(year as string)
    })
  end
end
