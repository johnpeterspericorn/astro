class AddCancellingUserToCancelledPurchases < ActiveRecord::Migration
  def change
    change_table :cancelled_purchases do |t|
      t.references :cancelling_user, polymorphic: true
    end
  end
end
