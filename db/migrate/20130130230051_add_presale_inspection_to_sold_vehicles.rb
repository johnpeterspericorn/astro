class AddPresaleInspectionToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :presale_inspection, :boolean
  end
end
