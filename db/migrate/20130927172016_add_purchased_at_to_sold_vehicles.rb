class AddPurchasedAtToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :purchased_at, :datetime

    SoldVehicle.find_each do |sold_vehicle|
      sold_vehicle.update_attributes!(purchased_at: sold_vehicle.created_at)
    end

    change_column :sold_vehicles, :purchased_at, :datetime, null: false
  end
end
