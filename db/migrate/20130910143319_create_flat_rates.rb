class CreateFlatRates < ActiveRecord::Migration
  def change
    create_table :flat_rates do |t|
      t.string :pawn_access_no, null: false
      t.string :agreement_no,         null: false

      t.float :low_price,    null: false
      t.float :high_price,   null: false
      t.float :target_price, null: false

      t.float :inspection_adjustment_price, null: false
      t.float :days_adjustment_price,       null: false
      t.float :miles_adjustment_price,      null: false

      t.integer :ineligibility_condition

      t.boolean :target_customer, null: false, default: false
      t.boolean :major_customer,  null: false, default: false

      t.timestamps
    end
  end
end
