class RenameVehicleIdToSoldVehicleIdOnPromisePurchases < ActiveRecord::Migration
  def change
    rename_column :promise_purchases, :vehicle_id, :sold_vehicle_id
  end
end
