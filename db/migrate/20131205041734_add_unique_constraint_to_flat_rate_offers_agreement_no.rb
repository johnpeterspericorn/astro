class AddUniqueConstraintToFlatRateOffersAgreementNo < ActiveRecord::Migration
  def up
    execute <<-SQL
      -- Delete oldest duplicate `agreement_no` entries from flat_rate_offers
      -- Adapted from: http://wiki.postgresql.org/wiki/Deleting_duplicates
      DELETE FROM flat_rate_offers
      WHERE id IN (
        SELECT id
        FROM (
          SELECT id, row_number() over (partition BY agreement_no ORDER BY id DESC) AS rnum
          FROM flat_rate_offers
        ) t
        WHERE t.rnum > 1
      );
    SQL

    add_index :flat_rate_offers, :agreement_no, unique: true
  end

  def down
    # This migration is reversible, but some rows were deleted to allow its
    # original application. Those rows are irrecoverable.
    remove_index :flat_rate_offers, :agreement_no
  end
end
