class AddGroupColumnsToDashboardScore < ActiveRecord::Migration
  def change
    add_column :dashboard_scores, :group_name, :string
    add_column :dashboard_scores, :group_avg_loss, :float
    add_column :dashboard_scores, :group_return_rate, :float
    add_column :dashboard_scores, :group_volume, :float
    add_column :dashboard_scores, :group_margin, :float
    add_column :dashboard_scores, :group_earnings, :float
    add_column :dashboard_scores, :group_return_score, :integer
    add_column :dashboard_scores, :group_purchase_quality, :integer
    add_column :dashboard_scores, :group_margin_score, :integer
    add_column :dashboard_scores, :group_volume_score, :integer
    add_column :dashboard_scores, :group_earnings_score, :integer
  end
end
