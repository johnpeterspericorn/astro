class DeleteTableObfuscatedLinks < ActiveRecord::Migration
  def up
     drop_table :obfuscated_links
  end

  def down
    create_table :obfuscated_links do |t|
      t.string :uuid
      t.text :link

      t.timestamps
    end
  end
end
