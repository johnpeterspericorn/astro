class AddUniqueConstraintToFloorPlanBranches < ActiveRecord::Migration
  def change
    remove_index :floor_plan_branches, [:site_no, :company_code]
    add_index :floor_plan_branches, [:site_no, :company_code], unique: true
  end
end
