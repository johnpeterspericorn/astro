class AddEligibilityIfBidAndCheckedOutColumns < ActiveRecord::Migration
  def change
    add_column :vehicles,     :ineligibility_condition, :integer
    add_column :presale_data, :ineligibility_condition, :integer

    add_column :vehicles, :if_bid,      :boolean
    add_column :vehicles, :checked_out, :boolean
  end
end
