class AddStatusToShipments < ActiveRecord::Migration
  def change
    add_column :shipments, :status, :string
  end
end
