class AddPromiseViewerAndEditorToUsers < ActiveRecord::Migration
  def change
    add_column :users, :promise_admin_viewer, :boolean, :null => false, default: false
    add_column :users, :promise_admin_editor, :boolean, :null => false, default: false
  end
end
