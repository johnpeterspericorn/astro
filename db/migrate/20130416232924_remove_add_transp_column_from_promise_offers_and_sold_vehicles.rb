class RemoveAddTranspColumnFromPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def up
    remove_column :promise_offers, :add_transp
    remove_column :sold_vehicles,    :add_transp
  end

  def down
    add_column :promise_offers, :add_transp, :float
    add_column :sold_vehicles,    :add_transp, :float
  end
end
