class AddWorkOrderNumberToVehicles < ActiveRecord::Migration
  def change
    add_column :vehicles, :work_order_number, :string
  end
end
