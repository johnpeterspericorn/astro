class AddCountryToFlatRateOffers < ActiveRecord::Migration
  def change
    add_column :flat_rate_offers, :country, :string, null: false, default: 'United States of America'
  end
end
