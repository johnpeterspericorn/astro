class Rename250PriceInFlatRates < ActiveRecord::Migration
  def change
    rename_column :flat_rates, :low_price_250, :low_price_360
    rename_column :flat_rates, :high_price_250, :high_price_360
    rename_column :flat_rates, :target_price_250, :target_price_360

    rename_column :flat_rates, :low_price_250_psi, :low_price_360_psi
    rename_column :flat_rates, :high_price_250_psi, :high_price_360_psi
    rename_column :flat_rates, :target_price_250_psi, :target_price_360_psi
  end
end
