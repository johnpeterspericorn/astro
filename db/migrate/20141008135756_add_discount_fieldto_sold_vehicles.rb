class AddDiscountFieldtoSoldVehicles < ActiveRecord::Migration
  def up
    add_column :sold_vehicles, :discount, :float, :null => false, :default => 0.0
  end

  def down
    remove_column :sold_vehicles, :discount
  end
end
