class AddVehicleCreatorRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :vehicle_creator, :boolean, null: false, default: false
  end
end
