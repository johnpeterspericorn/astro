class AddNotesToAgreementBlacklists < ActiveRecord::Migration
  def change
    add_column :agreement_blacklists, :notes, :text, null: false, default: ''
  end
end
