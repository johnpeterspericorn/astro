class UpdatePresaleMmrColumns < ActiveRecord::Migration
  def change
    add_column    :presale_data, :promise_500, :integer
    rename_column :presale_data, :g115_mmr, :promise115_250
    add_column    :presale_data, :promise115_500, :integer
  end
end
