class CreatePerformanceAlerts < ActiveRecord::Migration
  def change
    create_table :performance_alerts do |t|
      t.references :current_user, null: false
      t.text :emails, null: false
      t.references :relationship_manager
      t.string :agreement_no
      t.boolean  :return_rate, null: false, default: false
      t.boolean  :lpc, null: false, default: false
      t.boolean  :letter_sent, null: false, default: false
      t.datetime :follow_up_date,       null: false

      t.timestamps
    end
  end
end
