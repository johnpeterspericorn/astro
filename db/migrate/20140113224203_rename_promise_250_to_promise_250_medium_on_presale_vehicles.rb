class RenamePromise250ToPromise250MediumOnPresaleVehicles < ActiveRecord::Migration
  def change
    rename_column :presale_vehicles, :promise_250, :promise_250_medium
  end
end
