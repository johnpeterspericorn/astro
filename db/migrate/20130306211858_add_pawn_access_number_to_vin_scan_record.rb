class AddPawnAccessNumberToVinScanRecord < ActiveRecord::Migration
  def change
    add_column :vnum_scan_records, :pawn_access_no, :string
  end
end
