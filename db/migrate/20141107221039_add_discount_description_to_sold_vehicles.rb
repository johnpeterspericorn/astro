class AddDiscountDescriptionToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :discount_description, :text, default: '', null: false
    add_column :sold_vehicles_load, :discount_description, :text, default: '', null: false
  end
end
