class AddKindColumnToInspectionPurchases < ActiveRecord::Migration
  def change
    add_column :inspection_purchases, :kind, :string, null: false, default: 'purchase_protect'
  end
end
