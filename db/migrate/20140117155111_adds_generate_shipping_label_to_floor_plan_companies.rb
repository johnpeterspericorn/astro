class AddsGenerateShippingLabelToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :generate_shipping_label, :boolean, null: false, default: true
  end
end
