class RemovePawnAccessNoFromExternalVehiclesLoads < ActiveRecord::Migration
  def change
    remove_column :external_vehicles_loads, :pawn_access_no
  end
end
