class AddUniquenessConstraintOnFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_index :floor_plan_companies, [:company_code, :site_no], unique: true
  end
end
