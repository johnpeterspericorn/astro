class CreatePricingDistributions < ActiveRecord::Migration
  def change
    create_table :pricing_distributions do |t|
      t.references :risk_factor,      null: false
      t.references :vehicle_category, null: false
      t.float :low_price,    null: false
      t.float :median_price, null: false
      t.float :high_price,   null: false
      t.timestamps
    end
  end
end
