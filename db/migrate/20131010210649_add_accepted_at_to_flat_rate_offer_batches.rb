class AddAcceptedAtToFlatRateOfferBatches < ActiveRecord::Migration
  def change
    add_column :flat_rate_offer_batches, :accepted_at, :datetime
  end
end
