class AddSlugToReturnInvoice < ActiveRecord::Migration
  def change
    add_column :return_invoices, :slug, :string
  end
end
