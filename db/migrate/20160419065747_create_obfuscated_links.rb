class CreateObfuscatedLinks < ActiveRecord::Migration
  def change
    create_table :obfuscated_links do |t|
      t.string :uuid
      t.text :link

      t.timestamps
    end
  end
end
