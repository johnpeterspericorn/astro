class CreateErrors < ActiveRecord::Migration
  def change
    create_table :errors do |t|
      t.string :message, null: false
      t.text   :backtrace
      t.timestamps
    end
  end
end
