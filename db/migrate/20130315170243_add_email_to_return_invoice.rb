class AddEmailToReturnInvoice < ActiveRecord::Migration
  def change
    add_column :return_invoices, :user_email, :string
  end
end
