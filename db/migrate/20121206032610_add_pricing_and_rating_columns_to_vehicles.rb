class AddPricingAndRatingColumnsToVehicles < ActiveRecord::Migration
  def change
    if Rails.env.development?
      #vehicle_purchase_price,odometer_rating,autocheck_rating,mmr_rating,age_rating,seller_rating,mid_rating,bid_risk_rating
      add_column :vehicles, :vehicle_purchase_price, :float
      add_column :vehicles, :odometer_rating,        :integer
      add_column :vehicles, :autocheck_rating,       :integer
      add_column :vehicles, :mmr_rating,             :integer
      add_column :vehicles, :age_rating,             :integer
      add_column :vehicles, :seller_rating,          :integer
      add_column :vehicles, :mid_rating,             :integer
      add_column :vehicles, :bid_risk_rating,        :integer
    end
  end
end
