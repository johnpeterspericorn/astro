class RenameEmailColumnInBuyerInformationToEmails < ActiveRecord::Migration
  def change
    rename_column :buyer_information, :email, :emails
    change_column :buyer_information, :emails, :text
  end
end
