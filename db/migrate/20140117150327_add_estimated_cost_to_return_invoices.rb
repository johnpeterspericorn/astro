class AddEstimatedCostToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :estimated_cost, :float, null: false, default: 0
  end
end
