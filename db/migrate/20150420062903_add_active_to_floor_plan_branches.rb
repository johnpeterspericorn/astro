class AddActiveToFloorPlanBranches < ActiveRecord::Migration
  def change
    add_column :floor_plan_branches, :active, :boolean, null: false, default: true
  end
end
