class CreateBatchCouponsJoinTable < ActiveRecord::Migration
  def change
    create_table :batch_coupons, id: false do |t|
      t.references :coupon, null: false
      t.integer :coupon_batch_id, null: false
      t.timestamps null: false
    end
    add_index :batch_coupons, :coupon_id, unique: true
  end
end
