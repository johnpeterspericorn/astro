class ChangeSoldVehiclesAndPawnInformationSbluToInteger < ActiveRecord::Migration
  def up
    %w(pawn_information sold_vehicles).each do |table|
      add_column table, :new_sblu, :integer

      table.classify.constantize.all.each do |record|
        record.new_sblu = record.sblu
        record.save!
      end

      remove_column table, :sblu
      rename_column table, :new_sblu, :sblu
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
