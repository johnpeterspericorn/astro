class TaxRateUpdate < ActiveRecord::Migration
 def up
 	execute <<-SQL
   update pawn_locations set tax_rate = '13.0' where initials = 'TAA';
   update pawn_locations set tax_rate = '5.0' where initials = 'MTRL';
   update pawn_locations set tax_rate = '15.0' where initials = 'HLFX';
   update pawn_locations set tax_rate = '5.0' where initials = 'CANM';
  SQL
 end

 def down
 end
end
