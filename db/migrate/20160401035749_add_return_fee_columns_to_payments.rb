class AddReturnFeeColumnsToPayments < ActiveRecord::Migration
  def up
    change_table :payments do |t|
      t.float :promise_price
      t.float :return_fee
    end
    execute 'UPDATE payments SET promise_price = amount'
  end

  def down
    remove_column :payments, :promise_price
    remove_column :payments, :return_fee
  end
end
