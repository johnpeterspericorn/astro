class AddShipmentIdToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :shipment_id, :integer
  end
end
