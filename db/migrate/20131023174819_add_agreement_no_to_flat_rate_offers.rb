class AddAgreementNoToFlatRateOffers < ActiveRecord::Migration
  def change
    add_column :flat_rate_offers, :agreement_no, :string
  end
end
