class AddChargeFailedToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :charge_failed, :bool
  end
end
