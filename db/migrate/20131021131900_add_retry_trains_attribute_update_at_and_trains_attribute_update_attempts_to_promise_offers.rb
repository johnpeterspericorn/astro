class AddRetryTrainsAttributeUpdateAtAndTrainsAttributeUpdateAttemptsToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :retry_trains_attribute_update_at, :datetime, null: false, default: 'epoch'
    add_column :promise_offers, :trains_attribute_update_attempts, :integer,  null: false, default: 0
  end
end
