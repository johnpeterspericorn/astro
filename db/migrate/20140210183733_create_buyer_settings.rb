class CreateBuyerSettings < ActiveRecord::Migration
  def change
    create_table :buyer_settings do |t|
      t.string :pawn_access_no
      t.boolean :send_updates

      t.timestamps
    end
  end
end
