class AddReasonToAudits < ActiveRecord::Migration
  def change
    add_column :audits, :reason, :text, null: false, default: ''
  end
end
