class UndoPrimaryKeyChanges < ActiveRecord::Migration
  def up
    connection.execute(%q{
      alter table presale_vehicles
      alter column id
      type integer using cast(id as integer)
    })

    connection.execute(%q{
      alter table sold_vehicles
      alter column id
      type integer using cast(id as integer)
    })
  end

  def down
    connection.execute(%q{
      alter table presale_vehicles
      alter column id
      type string using cast(id as string)
    })

    connection.execute(%q{
      alter table sold_vehicles
      alter column id
      type string using cast(id as string)
    })
  end
end
