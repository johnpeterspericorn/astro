class AddPromisePriceThresholdToSoldVehicles < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :promise_price_threshold, :integer
  end
end
