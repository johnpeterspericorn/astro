class RenameGenericUsersToUsers < ActiveRecord::Migration
  def change
    rename_table :generic_users, :users
  end
end
