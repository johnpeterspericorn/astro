class CreatePendingReasons < ActiveRecord::Migration
  def change
    create_table :pending_reasons do |t|
      t.integer :code,   null: false
      t.string  :notice, null: false
      t.timestamps
    end

    add_index :pending_reasons, :code, unique: true
  end
end
