class AddKindToOffer < ActiveRecord::Migration
  def change
    add_column :promise_offers, :preselected_promise_kind, :string, null: false, default: 'purchase_protect'
  end
end
