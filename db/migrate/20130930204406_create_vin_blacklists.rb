class CreateVinBlacklists < ActiveRecord::Migration
  def change
    create_table :vnum_blacklists do |t|
      t.string :vnum

      t.timestamps
    end
  end
end
