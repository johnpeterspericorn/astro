class AddPromisePriceThresholdToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :promise_price_threshold, :integer
  end
end
