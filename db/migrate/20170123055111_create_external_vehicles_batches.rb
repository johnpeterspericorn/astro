class CreateExternalVehiclesBatches < ActiveRecord::Migration
  def change
    create_table :external_vehicles_batches do |t|
      t.string :status
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
