class AddTransactionIdToPromisePurchase < ActiveRecord::Migration
  def change
    add_column :promise_purchases, :transaction_id, :string
  end
end
