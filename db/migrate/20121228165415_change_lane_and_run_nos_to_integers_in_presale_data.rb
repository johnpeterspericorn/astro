class ChangeLaneAndRunNosToIntegersInPresaleData < ActiveRecord::Migration
  def change
    remove_column :presale_data, :lane_no
    add_column :presale_data, :lane_no, :integer
    remove_column :presale_data, :run_no
    add_column :presale_data, :run_no, :integer
  end
end
