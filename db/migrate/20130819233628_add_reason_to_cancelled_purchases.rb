class AddReasonToCancelledPurchases < ActiveRecord::Migration
  def change
    add_column :cancelled_purchases, :reason, :string
  end
end
