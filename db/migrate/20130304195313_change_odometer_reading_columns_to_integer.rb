class ChangeOdometerReadingColumnsToInteger < ActiveRecord::Migration
  def up
    %w(promise_selections presale_vehicles sold_vehicles).each do |table|
      connection.execute(
        %Q{
        alter table #{table}
        alter column odometer_reading
        type integer using cast(odometer_reading as integer)
        }
      )
    end
  end

  def down
    %w(promise_selections presale_vehicles sold_vehicles).each do |table|
      connection.execute(
        %Q{
        alter table #{table}
        alter column odometer_reading
        type string using cast(odometer_reading as string)
        }
      )
    end
  end
end
