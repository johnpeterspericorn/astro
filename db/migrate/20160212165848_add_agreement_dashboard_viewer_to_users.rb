class AddAgreementDashboardViewerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :agreement_dashboard_viewer, :boolean, :null => false, default: false
  end
end
