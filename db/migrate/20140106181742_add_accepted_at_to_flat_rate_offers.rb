class AddAcceptedAtToFlatRateOffers < ActiveRecord::Migration
  def up
    add_column :flat_rate_offers, :accepted_at, :datetime

    execute <<-SQL
      UPDATE flat_rate_offers
      SET accepted_at = flat_rate_offer_batches.accepted_at
      FROM flat_rate_offer_batches
      WHERE flat_rate_offer_batches.id = flat_rate_offers.flat_rate_offer_batch_id
    SQL
  end

  def down
    remove_column :flat_rate_offers, :accepted_at
  end
end
