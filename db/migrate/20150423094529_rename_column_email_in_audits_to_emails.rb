class RenameColumnEmailInAuditsToEmails < ActiveRecord::Migration
  def change
    rename_column :audits, :email, :emails
    change_column :audits, :emails, :text
  end
end
