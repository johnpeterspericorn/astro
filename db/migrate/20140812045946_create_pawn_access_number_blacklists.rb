class CreatePawnAccessNumberBlacklists < ActiveRecord::Migration
  def change
    create_table :pawn_access_number_blacklists do |t|
      t.string :pawn_access_no,   null: false
      t.text   :notes,               null: false,   default: ''
      t.date   :expiration_date

      t.timestamps
    end
  end
end
