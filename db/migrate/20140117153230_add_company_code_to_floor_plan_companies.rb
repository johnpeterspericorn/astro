class AddCompanyCodeToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :company_code, :string
    change_column_null :floor_plan_companies, :company_code, false, '<missing>'
  end
end
