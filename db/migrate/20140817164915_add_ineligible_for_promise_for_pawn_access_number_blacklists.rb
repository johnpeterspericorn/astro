class AddIneligibleForPromiseForPawnAccessNumberBlacklists < ActiveRecord::Migration
  def change
    add_column :pawn_access_number_blacklists, :ineligible_for_promise, :boolean, default: false, null: false
  end
end
