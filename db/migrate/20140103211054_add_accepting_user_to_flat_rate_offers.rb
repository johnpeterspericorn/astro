class AddAcceptingUserToFlatRateOffers < ActiveRecord::Migration
  def up
    add_column :flat_rate_offers, :accepting_user_id,   :integer
    add_column :flat_rate_offers, :accepting_user_type, :string

    execute <<-SQL
      UPDATE flat_rate_offers
      SET accepting_user_id = flat_rate_offer_batches.accepting_user_id, accepting_user_type = flat_rate_offer_batches.accepting_user_type
      FROM flat_rate_offer_batches
      WHERE flat_rate_offer_batches.id = flat_rate_offers.flat_rate_offer_batch_id
    SQL
  end

  def down
    remove_column :flat_rate_offers, :accepting_user_id
    remove_column :flat_rate_offers, :accepting_user_type
  end
end
