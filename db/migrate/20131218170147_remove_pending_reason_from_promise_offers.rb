class RemovePendingReasonFromPromiseOffers < ActiveRecord::Migration
  REASONS_MAP = {
    'approval_required'            => PendingConditions::APPROVAL_REQUIRED,
    'blacklisted'                  => PendingConditions::BLACKLISTED,
    'promise_price_over_limit'   => PendingConditions::GUARANTEE_PRICE_EXCEEDED,
    'vehicle_price_ratio_exceeded' => PendingConditions::VEHICLE_PRICE_RATIO_EXCEEDED,
    'vehicle_price_over_limit'     => PendingConditions::VEHICLE_PRICE_EXCEEDED,
    'if_bid'                       => PendingConditions::IF_BID,
  }

  def up
    PromiseOffer.transaction do
      PromiseOffer.where('pending_reason IS NOT NULL').each do |offer|
        offer.pending_reason_code = REASONS_MAP[offer.pending_reason]
        offer.save!
      end
    end

    remove_column :promise_offers, :pending_reason
  end

  def down
    add_column :promise_offers, :pending_reason, :string

    PromiseOffer.transaction do
      PromiseOffer.where('pending_reason_code IS NOT NULL').each do |offer|
        offer.pending_reason = REASONS_MAP.key(offer.pending_reason_code)
        offer.save!
      end
    end
  end
end
