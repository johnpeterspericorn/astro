class RemoveSoldVehiclesCheckedOutColumn < ActiveRecord::Migration
  def change
    remove_column :sold_vehicles, :checked_out
  end
end
