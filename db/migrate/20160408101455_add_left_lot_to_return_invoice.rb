class AddLeftLotToReturnInvoice < ActiveRecord::Migration
  def change
    add_column :return_invoices, :left_lot, :boolean
  end
end
