class CreateNetworkPlusOffers < ActiveRecord::Migration
  def self.up
    create_table :network_plus_offers do |t|
      t.references :offering_user, null: false
      t.references :accepting_user
      t.text :emails, null: false
      t.string :state
      t.string :agreement_no
      t.float    :existing_network_rate,       null: false
      t.float    :network_plus_surcharge,       null: false
      t.datetime :expiration_date,       null: false
      t.boolean  :letter_sent, null: false, default: false
      t.boolean  :accepted,    null: false, default: false
      t.datetime :accepted_at

      t.timestamps
    end
  end

    def self.down
      drop_table :network_plus_offers
    end
end
