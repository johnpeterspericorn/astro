class AddReturningUserInformationToReturnInvoices < ActiveRecord::Migration
  def change
    add_column :return_invoices, :returning_user_type, :string
    add_column :return_invoices, :returning_user_id,   :integer
  end
end
