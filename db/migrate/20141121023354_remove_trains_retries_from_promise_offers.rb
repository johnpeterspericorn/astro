class RemoveTrainsRetriesFromPromiseOffers < ActiveRecord::Migration
  def up
    change_table :promise_offers do |t|
      t.remove :retry_trains_attribute_update_at
      t.remove :trains_attribute_update_attempts
    end
  end

  def down
    change_table :promise_offers do |t|
      t.datetime :retry_trains_attribute_update_at, default: '1970-01-01 00:00:00', null: false
      t.integer :trains_attribute_update_attempts, default: 0, null: false
    end
  end
end
