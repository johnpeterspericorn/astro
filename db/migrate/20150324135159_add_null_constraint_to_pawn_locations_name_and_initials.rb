require_relative '../database_default_values'

class AddNullConstraintToPawnLocationsNameAndInitials < ActiveRecord::Migration
  include DatabaseDefaultValues

  def change
    change_column_null :pawn_locations, :name, false, MISSING_NOT_NULL_STRING
    change_column_null :pawn_locations, :initials, false, MISSING_NOT_NULL_STRING
  end
end
