class AddPurchasesEnabledFlagToPawnLocations < ActiveRecord::Migration
  def change
    add_column :pawn_locations, :purchases_enabled, :bool, default: true
  end
end
