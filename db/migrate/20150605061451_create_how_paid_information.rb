class CreateHowPaidInformation < ActiveRecord::Migration
  def change
    create_table :how_paid_information do |t|
      t.text :payment_type, null: false
      t.string :vnum, null: false
      t.timestamps null: false
    end
  end
end
