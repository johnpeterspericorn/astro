class AddBranchNameToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :branch_name, :string
  end
end
