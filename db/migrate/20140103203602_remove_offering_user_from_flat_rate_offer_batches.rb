class RemoveOfferingUserFromFlatRateOfferBatches < ActiveRecord::Migration
  def up
    remove_column :flat_rate_offer_batches, :offering_user_id
    remove_column :flat_rate_offer_batches, :offering_user_type
  end

  def down
    add_column :flat_rate_offer_batches, :offering_user_id,   :integer
    add_column :flat_rate_offer_batches, :offering_user_type, :string

    execute <<-SQL
      UPDATE flat_rate_offer_batches
      SET offering_user_id = flat_rate_offers.offering_user_id, offering_user_type = flat_rate_offers.offering_user_type
      FROM flat_rate_offers
      WHERE flat_rate_offers.id = flat_rate_offers.flat_rate_offer_batch_id
    SQL

    change_column_null :flat_rate_offer_batches, :offering_user_id,   false
    change_column_null :flat_rate_offer_batches, :offering_user_type, false
  end
end
