class RemovePawnAccessNoFromFlatRates < ActiveRecord::Migration
  def change
    remove_column :flat_rates, :pawn_access_no
  end
end
