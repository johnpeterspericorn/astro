class CreateMarketIndices < ActiveRecord::Migration
  def change
    create_table :market_indices do |t|
      t.text :notes, limit: 1000
      t.integer :value

      t.timestamps null: false
    end
  end
end
