class AddCountryCodeToFloorPlanCompanies < ActiveRecord::Migration
  def change
    add_column :floor_plan_companies, :country_code, :string, default: 'us'
    change_column_null :floor_plan_companies, :country_code, false, 'us'
  end
end
