class RemoveAgreementNoIndexOnFlatRateOffers < ActiveRecord::Migration
  def change
    remove_index :flat_rate_offers, name: :index_flat_rate_offers_on_agreement_no
  end
end
