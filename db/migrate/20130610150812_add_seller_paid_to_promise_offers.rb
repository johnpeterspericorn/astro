class AddSellerPaidToPromiseOffers < ActiveRecord::Migration
  def change
    add_column :promise_offers, :seller_paid, :boolean
  end
end
