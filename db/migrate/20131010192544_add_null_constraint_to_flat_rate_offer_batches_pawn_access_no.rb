class AddNullConstraintToFlatRateOfferBatchesPawnAccessNo < ActiveRecord::Migration
  def change
    change_column_null :flat_rate_offer_batches, :pawn_access_no, false
  end
end
