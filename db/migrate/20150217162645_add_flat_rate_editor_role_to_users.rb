class AddFlatRateEditorRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :flat_rate_editor, :boolean, null: false, default: false
  end
end
