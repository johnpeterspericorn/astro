class DeviseCreateAstroheimUsers < ActiveRecord::Migration
  def change
    create_table(:astroheim_users) do |t|
      t.string :username
      #t.references :loginable, polymorphic: true
      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.timestamps
    end

    add_index :astroheim_users, :username, :unique => true
  end
end
