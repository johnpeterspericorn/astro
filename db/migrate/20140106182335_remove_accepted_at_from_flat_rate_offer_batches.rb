class RemoveAcceptedAtFromFlatRateOfferBatches < ActiveRecord::Migration
  def up
    remove_column :flat_rate_offer_batches, :accepted_at
  end

  def down
    add_column :flat_rate_offer_batches, :accepted_at, :datetime
  end
end
