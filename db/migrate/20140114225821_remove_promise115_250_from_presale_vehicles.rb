class RemovePromise115250FromPresaleVehicles < ActiveRecord::Migration
  def up
    remove_column :presale_vehicles, :promise115_250
  end

  def down
    add_column :presale_vehicles, :promise115_250, :integer
  end
end
