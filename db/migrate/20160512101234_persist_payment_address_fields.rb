class PersistPaymentAddressFields < ActiveRecord::Migration
  def up
    change_table :payments do |t|
      t.string :address_zip
      t.string :address_city
      t.string :address_state
      t.text :address_street
    end
  end

  def down
    remove_column :payments, :address_zip
    remove_column :payments, :address_city
    remove_column :payments, :address_state
    remove_column :payments, :address_street
  end
end
