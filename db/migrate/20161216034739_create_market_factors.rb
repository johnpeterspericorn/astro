class CreateMarketFactors < ActiveRecord::Migration
  def change
    create_table :market_factors do |t|
      t.string :title, limit: 100
      t.text :description, limit: 500
      t.string :color, limit: 10
      t.references :market_index

      t.timestamps null: false
    end
  end
end
