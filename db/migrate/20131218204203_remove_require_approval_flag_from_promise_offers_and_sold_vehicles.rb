class RemoveRequireApprovalFlagFromPromiseOffersAndSoldVehicles < ActiveRecord::Migration
  def up
    execute <<-SQL
      UPDATE promise_offers
      SET pending_reason_code = #{PendingConditions::APPROVAL_REQUIRED}
      WHERE require_approval IS TRUE
    SQL

    execute <<-SQL
      UPDATE sold_vehicles
      SET pending_reason_code = #{PendingConditions::APPROVAL_REQUIRED}
      WHERE require_approval IS TRUE
    SQL

    remove_column :promise_offers, :require_approval
    remove_column :sold_vehicles,    :require_approval
  end

  def down
    add_column :promise_offers, :require_approval, :boolean, null: false, default: false
    add_column :sold_vehicles,    :require_approval, :boolean, null: false, default: false

    execute <<-SQL
      UPDATE promise_offers
      SET require_approval = TRUE
      WHERE pending_reason_code = #{PendingConditions::APPROVAL_REQUIRED}
    SQL

    execute <<-SQL
      UPDATE sold_vehicles
      SET require_approval = TRUE
      WHERE pending_reason_code = #{PendingConditions::APPROVAL_REQUIRED}
    SQL
  end
end
