class CreateCouponBatches < ActiveRecord::Migration
  def change
    create_table :coupon_batches do |t|
      t.text :name, null: false
      t.timestamps null: false
    end
  end
end
