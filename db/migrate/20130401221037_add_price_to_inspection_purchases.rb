class AddPriceToInspectionPurchases < ActiveRecord::Migration
  def change
    add_column :inspection_purchases, :price, :float
  end
end
