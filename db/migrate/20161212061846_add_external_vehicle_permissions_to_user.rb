class AddExternalVehiclePermissionsToUser < ActiveRecord::Migration
  def change
    add_column :users, :external_vehicle_enterer, :boolean, default: false
    add_column :users, :external_vehicle_approver, :boolean, default: false
  end
end
