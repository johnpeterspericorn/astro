class CreateVinScanRecord < ActiveRecord::Migration
  def change
    create_table :vnum_scan_records do |t|
      t.string :vnum
      t.timestamps
    end
  end
end
