class AddPromiseOfferToDiscounts < ActiveRecord::Migration
  def change
    add_column :discounts, :promise_offer_id, :integer, null: false
  end
end
