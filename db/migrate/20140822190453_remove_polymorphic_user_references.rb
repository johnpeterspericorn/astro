class RemovePolymorphicUserReferences < ActiveRecord::Migration
  POLYMORPHIC_TABLES = {
    cancelled_purchases: %w(purchasing_user_type cancelling_user_type),
    flat_rate_offers: %w(offering_user_type accepting_user_type),
    promise_offers: %w(changing_user_type),
    promise_purchases: %w(purchasing_user_type),
    return_invoices: %w(returning_user_type cancelling_user_type),
  }

  def up
    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        remove_column table, column
      end
    end
  end

  def down
    POLYMORPHIC_TABLES.each do |table, columns|
      columns.each do |column|
        add_column table, column, :string
        execute "UPDATE #{table} SET #{column} = 'GenericUser'"
      end
    end
  end
end
