class AddApiUserFlagToGenericUsers < ActiveRecord::Migration
  def change
    add_column :generic_users, :api_user, :boolean, null: false, default: false
  end
end
