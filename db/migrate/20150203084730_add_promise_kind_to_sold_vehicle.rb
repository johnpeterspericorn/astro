class AddPromiseKindToSoldVehicle < ActiveRecord::Migration
  def change
    add_column :sold_vehicles, :preselected_promise_kind, :string, null: false, default: 'purchase_protect'
  end
end
