class SetDefaultTrueToDashboardPermissions < ActiveRecord::Migration
  def change
    change_column :users, :agreement_transaction_viewer, :boolean, default: true, null: false
    change_column :users, :agreement_score_viewer, :boolean, default: true, null: false
  end
end
