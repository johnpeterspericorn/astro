class AddNotesToAgreementInformation < ActiveRecord::Migration
  def change
    add_column :agreement_information, :notes, :text
  end
end
