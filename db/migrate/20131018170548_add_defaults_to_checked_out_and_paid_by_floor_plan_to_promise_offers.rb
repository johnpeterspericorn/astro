class AddDefaultsToCheckedOutAndPaidByFloorPlanToPromiseOffers < ActiveRecord::Migration
  def change
    change_column_default :promise_offers, :checked_out,        false
    change_column_default :promise_offers, :paid_by_floor_plan, false
  end
end
