# encoding: utf-8
require 'spec_helper'

describe ShipmentDecorator do
  context 'decorated attributes' do
    it 'should have a ups tracking url if the carrier is ups' do
      shipment = FactoryGirl.create(:shipment)
      decorator = shipment.decorate
      expect(decorator.tracking_link).to include('ups')
    end

    it 'should have a fedex tracking url if the carrier is fedex' do
      shipment = FactoryGirl.create(:shipment, carrier: 'FedEx')
      decorator = shipment.decorate
      expect(decorator.tracking_link).to include('fedex')
    end
  end
end
