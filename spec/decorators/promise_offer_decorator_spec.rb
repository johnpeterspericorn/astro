# encoding: utf-8
require 'spec_helper'

describe PromiseOfferDecorator do
  context 'decorated attributes' do
    let(:make)  { 'Honda'    }
    let(:model) { 'Civic Ex' }
    let(:year)  { '1990'     }
    subject(:decorator) { PromiseOffer.new.decorate }
    before do
      decorator.build_vehicle_information(make: make, model: model, year: year)
    end

    describe '#vehicle_full_description' do
      describe '#vehicle_full_description' do
        subject { super().vehicle_full_description }
        it { is_expected.to match make }
      end

      describe '#vehicle_full_description' do
        subject { super().vehicle_full_description }
        it { is_expected.to match model }
      end

      describe '#vehicle_full_description' do
        subject { super().vehicle_full_description }
        it { is_expected.to match year }
      end
    end

    describe '#vehicle_code' do
      ('A'..'E').each_with_index do |letter, vehicle_code|
        specify do
          decorator.source.vehicle_information.vehicle_code = vehicle_code + 1
          expect(decorator.vehicle_code).to eql letter
        end
      end

      [0, 6].each do |out_of_bound_code|
        specify do
          decorator.source.vehicle_information.vehicle_code = out_of_bound_code
          expect(decorator.vehicle_code).to be_empty
        end
      end
    end
  end

  describe '.search_criteria' do
    it 'displays a badge number correctly' do
      badge_number = SasDataHelper::DEFAULT_BADGE_NUMBER
      filter_query_data = FilterQueryData.new(badge_number: badge_number)
      criteria = PromiseOfferDecorator.search_criteria(filter_query_data)
      expect(criteria).to eql I18n.t('promise_offers.greetings.badge_number', badge_number: badge_number)
    end

    it 'displayes a username based on AA#' do
      agreement_name = 'Agreement Name'
      pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
      agreement_number         = SasDataHelper::DEFAULT_DEALER_NUMBER
      allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_contact_info).and_return(double(name: agreement_name))
      filter_query_data = FilterQueryData.new(pawn_access_number: pawn_access_number, agreement_numbers: [agreement_number])
      criteria = PromiseOfferDecorator.search_criteria(filter_query_data)
      expect(criteria).to eql I18n.t('promise_offers.greetings.pawn_access_number', agreement_name: agreement_name)
    end
  end

  describe '#email' do
    it 'returns the email if it is provided' do
      selection = PromiseOffer.new(emails: 'foo@bar.com').decorate
      expect(selection.emails).to eql 'foo@bar.com'
    end
    it 'returns an indicator string if no email is provided' do
      selection = PromiseOffer.new.decorate
      expect(selection.emails).not_to match(/@/)
    end
  end

  describe '#selection_description' do
    let(:days_selected) { 5 }
    let(:miles_selected) { 123 }
    let(:offer)         { double 'PromiseOffer', days_selected: days_selected, miles_selected: miles_selected }

    subject(:decorator) { described_class.new(offer) }

    it 'is the days and months selected in the format "_D _M"' do
      expect(decorator.selection_description).to eq("#{days_selected}D #{miles_selected}M")
    end
  end
end
