# encoding: utf-8
require 'spec_helper'

describe PromisePurchaseDecorator do
  let(:purchase) { double :promise_purchase }
  let(:other_purchase) { OpenStruct.new }

  subject(:decorator) { described_class.new purchase }

  describe '#price_in_cents' do
    let(:purchase) { FactoryGirl.create(:promise_purchase) }

    it 'is the price of the promise purchase in currency' do
      expect(decorator.price_in_cents).to equal(10_000)
    end

    it 'is the price of return fee for the promise purchase' do
      session = {
        return_fee: '150'
      }
      expect(decorator.price_in_cents(session)).to equal(15_000)
    end
  end

  describe '#displayable_for_return?' do
    before do
      allow(purchase).to receive(:max_return_time).and_return(Time.current)
    end

    it 'is if purchase is eligible for return' do
      allow(purchase).to receive(:eligible_for_return?).and_return(true)
      expect(decorator.displayable_for_return?).to be_truthy
    end

    it 'is if expired recently' do
      allow(purchase).to receive(:eligible_for_return?).and_return(false)
      expect(decorator.displayable_for_return?).to be_truthy
    end

    it 'is not otherwise' do
      allow(purchase).to receive(:max_return_time).and_return(100.days.ago)
      allow(purchase).to receive(:eligible_for_return?).and_return(false)
      expect(decorator.displayable_for_return?).to be_falsey
    end
  end
end
