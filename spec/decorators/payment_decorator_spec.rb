# encoding: utf-8
require 'spec_helper'

describe PaymentDecorator do
  let(:payment) { double 'Payment' }
  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
  let(:cancelled_purchase) { double 'CancelledPurchase' }

  subject(:decorator) { described_class.new(payment) }

  describe 'delegated attributes' do
    ATTRS = %w(agreement_no promise_price location_initials vnum).freeze
    before do
      allow(payment).to receive(:promise_purchase).and_return(promise_purchase)
      allow(payment).to receive(:amount).and_return(112.34)
      allow_any_instance_of(PaymentDecorator).to receive(:promise_price).and_return(122)
    end

    ATTRS.each do |attr|
      it "delegates #{attr} to guaratee_purchase" do
        expect(promise_purchase).to receive(attr) if attr != 'promise_price'

        decorator.public_send(attr)
      end
    end
  end
end
