# encoding: utf-8
require 'spec_helper'

describe PresaleVehicleDecorator do
  let(:year)  { '1990' }
  let(:make)  { 'FORD' }
  let(:model) { 'FIESTA' }
  let(:presale_vehicle) { double(:presale_vehicle, year: year, make: make, model: model) }
  subject { PresaleVehicleDecorator.decorate(presale_vehicle) }

  describe '#full_description' do
    describe '#full_description' do
      subject { super().full_description }
      it { is_expected.to match year     }
    end

    describe '#full_description' do
      subject { super().full_description }
      it { is_expected.to match(/#{make.titleize}/) }
    end

    describe '#full_description' do
      subject { super().full_description }
      it { is_expected.to match(/#{model.titleize}/) }
    end
  end
end
