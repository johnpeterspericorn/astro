# encoding: utf-8
require 'spec_helper'

describe PendingOfferDecorator do
  let(:offer) { double 'PromiseOffer' }

  subject(:decorator) { described_class.new(offer) }

  describe '#changing_user' do
    it 'is a NullUser when decorated model has not changing_user' do
      allow(offer).to receive(:changing_user)

      expect(decorator.changing_user).to be_a(PendingOfferDecorator::NullUser)
    end
  end
end
