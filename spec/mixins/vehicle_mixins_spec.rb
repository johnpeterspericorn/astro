# encoding: utf-8
require 'spec_helper'

class VehicleAlike < Struct.new(:ineligibility_condition)
  include VehicleMixins::DomainConcerns
end

describe VehicleMixins::DomainConcerns do
  subject { VehicleAlike.new(nil) }

  describe '#eligible?' do
    it 'should be eligible with no ineligibility condition' do
      expect(subject).to be_eligible
    end

    it 'should be eligible with a zero for ineligibility condition' do
      subject.ineligibility_condition = 0
      expect(subject).to be_eligible
    end

    it 'should be ineligible for other ineligibility conditions' do
      subject.ineligibility_condition = -1
      expect(subject).to be_ineligible
    end
  end
end
