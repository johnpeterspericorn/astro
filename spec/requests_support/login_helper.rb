# encoding: utf-8
def log_in_user(user)
  visit new_user_session_path
  fill_in 'user_username', with: user.username
  fill_in 'user_password', with: user.password
  click_on 'Sign in'
  expect(page).not_to have_content 'Sign in'
end

def log_in_kiosk_user(location_initials = nil)
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_pawn_access_number).and_return(SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  kiosk_user = User.where(kiosk_user: true, username: "kiosk#{location_initials}").first_or_create!
  kiosk_user.update_attributes(password: 'password', location_initials: location_initials)
  kiosk_user.update_attribute(:invoiceable, true)
  kiosk_user.update_attribute(:payment_customer_id, 'cus_9RmQGa1id6aeaq')

  allow(kiosk_user).to receive(:password).and_return('password')
  log_in_user kiosk_user
end

def log_in_super_user(options = {})
  super_user = User.where(super_user: true, username: 'super', email: 'super@astro.local').first_or_create!
  super_user.update_attribute(:password, 'password')
  super_user.update_attribute(:payment_customer_id, 'cus_9RmQGa1id6aeaq')
  super_user.update_attribute(:admin, options.fetch(:admin, false))
  super_user.update_attribute(:vehicle_creator, options.fetch(:vehicle_creator, false))
  super_user.update_attribute(:promise_admin_viewer, options.fetch(:promise_admin_viewer, true))
  super_user.update_attribute(:promise_admin_editor, options.fetch(:promise_admin_editor, false))
  super_user.update_attribute(:user_admin_editor, options.fetch(:user_admin_editor, false))
  super_user.update_attribute(:invoiceable, options.fetch(:invoiceable, true))
  super_user.update_attribute(:loss_prevention_viewer, true)
  super_user.update_attribute(:finance_admin_viewer, options.fetch(:finance_admin_viewer, false))
  super_user.update_attribute(:flat_rate_inquiry_viewer, options.fetch(:flat_rate_inquiry_viewer, false))
  super_user.update_attribute(:flat_rate_editor, options.fetch(:flat_rate_editor, false))
  super_user.update_attribute(:feature_flag_editor, options.fetch(:feature_flag_editor, false))
  super_user.update_attribute(:external_vehicle_enterer, options.fetch(:external_vehicle_enterer, false))
  super_user.update_attribute(:external_vehicle_approver, options.fetch(:external_vehicle_approver, false))

  allow(super_user).to receive(:password).and_return('password')
  pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_pawn_access_number).and_return(pawn_access_number)
  log_in_user super_user
end

def log_in_buyer_user(options = {})
  options = { username: 'buyer_user', password: 'password' }.merge(options)

  buyer_user = User.where(buyer_user: true, username: options[:username]).first_or_create!
  buyer_user.update_attribute(:payment_customer_id, 'cus_9RmQGa1id6aeaq')
  allow(buyer_user).to receive(:password).and_return(options[:password]) # Buyer Users don't have passwords locally but need to present them
  buyer_user.update_attribute(:flat_rate_editor, options.fetch(:flat_rate_editor, false))
  buyer_user.update_attribute(:invoiceable, options.fetch(:invoiceable, true))
  allow(User).to receive(:authenticate_with_astroheim_auth).and_return(buyer_user)
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_pawn_access_number).and_return(SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_name).and_return('Buyer Agreement')
  log_in_user buyer_user
  buyer_user
end

def stub_buyer_authorization(options = {})
  options.reverse_merge!(
    name: 'A Great Agreement',
    email: 'agreement@agreement.com',
    phone: '1234567890'
  )

  options[:agreementship_name] ||= options[:name]

  agreement_numbers = Array(options[:agreement_numbers])
  agreement_numbers << SasDataHelper::DEFAULT_DEALER_NUMBER

  agreement_contact_info = double(name: options[:name], email: options[:email], phone: options[:phone])
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_contact_info).and_return(agreement_contact_info)

  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return(agreement_numbers)

  agreement_numbers_to_names = agreement_numbers.map do |agreement_number|
    { agreement_number => options[:agreementship_name] }
  end.reduce(:merge)
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers_to_names).and_return(agreement_numbers_to_names)

  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreementship_addr).and_return(OpenStruct.new(line1: '1989 meso Ave.', city: 'Chicago', state: 'GA', zip: '30317'))
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:has_authorization_details?).and_return(true)
  allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:has_contact_details?).and_return(true)
end

def basic_auth(name, password)
  if page.driver.respond_to?(:basic_auth)
    page.driver.basic_auth(name, password)
  elsif page.driver.respond_to?(:basic_authorize)
    page.driver.basic_authorize(name, password)
  elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:authenticate)
    page.driver.browser.authenticate(name, password)
  elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
    page.driver.browser.basic_authorize(name, password)
  else
    raise "I don't know how to log in!"
  end
end
