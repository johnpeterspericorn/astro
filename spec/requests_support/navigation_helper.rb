# encoding: utf-8
def visit_filter_page
  visit root_path
  click_on I18n.t('navigation.purchase')
end

def navigate_to_research_page
  visit root_path
  click_on I18n.t('navigation.quote')
end

def view_vehicles_by_pawn_access_number(pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  visit root_path

  click_on I18n.t('navigation.purchase')
  click_on I18n.t('forms.pawn_access.input_label')

  fill_in 'pawn_access_number', with: pawn_access_number
  click_button I18n.t('forms.enter')
end

def view_vehicles_by_agreement_number(agreement_number = SasDataHelper::DEFAULT_DEALER_NUMBER)
  visit root_path

  click_on I18n.t('navigation.purchase')
  click_on I18n.t('forms.agreement_number.input_label')

  fill_in 'agreement_number', with: agreement_number
  click_button I18n.t('forms.enter')
end

def view_vehicles_by_pawn_access_number_as_kiosk_user(pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  Settings.show_badge_ui = false
  visit root_path

  click_on I18n.t('navigation.purchase')
  click_on I18n.t('forms.pawn_access.input_label') if Settings.show_badge_ui

  fill_in 'pawn_access_number', with: pawn_access_number
  submit_pawn_access_number_form_for_kiosk_user pawn_access_number
end

def view_vehicles_by_badge_number(badge_number = SasDataHelper::DEFAULT_BADGE_NUMBER)
  Settings.show_badge_ui = true
  visit root_path

  click_on I18n.t('navigation.purchase')
  click_on I18n.t('forms.badge_number.input_label')

  fill_in 'badge_number', with: badge_number
  click_button I18n.t('forms.enter')
end

def visit_admin_page
  visit admin_index_path
end
