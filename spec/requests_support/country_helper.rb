# encoding: utf-8
def currency_symbol(name)
  Country.find_country_by_name(name).currency['symbol']
end

def country_currency_code(name)
  Country.find_country_by_name(name).currency['code']
end

def america
  SupportedCountry::NAMES[0]
end

def canada
  SupportedCountry::NAMES[1]
end
