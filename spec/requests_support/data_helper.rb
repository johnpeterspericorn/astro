# encoding: utf-8
def create_location_data
  PawnLocation.create!(name: 'Drive',       initials: 'DRIV', country: 'United States of America')
  PawnLocation.create!(name: 'Statesville', initials: 'SVAA', inspections_enabled: true, country: 'United States of America')
end

def display_vehicle_for_offer(vehicles)
  vehicles = [vehicles] unless vehicles.is_a?(Enumerable) || vehicles.is_a?(ActiveRecord::Relation)
  PromiseOfferRecorder.record_offers vehicles
end

def purchase_promise_for_vehicle(vehicles, days = 7, miles = 250)
  vehicles = [vehicles] unless vehicles.is_a?(Enumerable) || vehicles.is_a?(ActiveRecord::Relation)
  vehicles.each do |vehicle|
    display_vehicle_for_offer(vehicle) unless vehicle.promise_offers.present?
  end

  purchases = vehicles.map do |vehicle|
    PromisePurchase.from_options(vehicle.vnum, miles, days).decorate
  end
  PromiseSelectionRecorder.record_selected(purchases)
  vehicles.map(&:reload)
end

def purchase_inspection_for_offer(offers, price = nil)
  offers = [offers] unless offers.is_a?(Enumerable) || offers.is_a?(ActiveRecord::Relation)

  offers.each do |offer|
    price ||= offer.promise_purchase.present? ? offer.inspection_bundle_price : offer.inspection_only_price

    inspection = offer.build_inspection_purchase
    inspection.price = price
    inspection.save!
  end

  offers.map(&:reload)
end

def promise_and_return_vehicle(vehicles, returning_user)
  vehicles = [vehicles] unless vehicles.is_a?(Enumerable) || vehicles.is_a?(ActiveRecord::Relation)
  purchase_promise_for_vehicle(vehicles)

  vehicles.each do |vehicle|
    odometer_on_return = vehicle.odometer_reading
    promise_purchase = vehicle.promise_offers.last.promise_purchase

    return_invoice = ReturnInvoice.new(
      promise_purchase: promise_purchase,
      odometer_on_return: odometer_on_return,
      no_answer: true,
      title_status: 2,
      pawn_location: PawnLocation.first
    )
    return_invoice.returning_user = returning_user

    return_invoice.save!
  end
end
