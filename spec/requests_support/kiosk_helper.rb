# encoding: utf-8
def submit_pawn_access_number_form_for_kiosk_user(pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  # kiosk users will not have an "Enter" button anymore so rather than forcing
  # every kiosk user story to be a javascript enabled story (and take the
  # associated time hit) we play a little trick here and submit the form
  # directly
  #
  # From SO: http://stackoverflow.com/questions/2797752/cucumber-capybara-and-selenium-submiting-a-form-without-a-button
  #          http://stackoverflow.com/a/13313332
  if Capybara.current_driver == Capybara.javascript_driver
    page.execute_script "parseTrackData(#{pawn_access_card_track_data(pawn_access_number)});"
  else ## Probably RackTest?
    fill_in 'pawn_access_number', with: pawn_access_number
    submit_form_by_id_for_kiosk_user('pawn_access_number_filter')
  end
end

def submit_form_by_id_for_kiosk_user(form_id)
  form_element = find_by_id(form_id)
  Capybara::RackTest::Form.new(page.driver, form_element.native).submit name: nil
end

def pawn_access_card_track_data(pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  "'%0000000^#{pawn_access_number}^05?;0000000=#{pawn_access_number}=05?'"
end
