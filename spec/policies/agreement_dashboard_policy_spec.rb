# encoding: utf-8
require 'spec_helper'

describe AgreementDashboardPolicy do
  describe '#show?' do
    let(:user) { double(super_user?: false, buyer_user?: false, pawn_access_number: '100897831') }

    it 'is true when user is "super"' do
      policy = described_class.new(user, nil)
      allow(user).to receive(:super_user?) { true }

      expect(policy.show?).to eq(true)
    end

    it 'is true when user is "kiosk"' do
      policy = described_class.new(user, nil)
      allow(user).to receive(:kiosk_user?) { true }

      expect(policy.show?).to eq(true)
    end

    it 'is true when user is "buyer" and has matching pawn_access_number' do
      policy = described_class.new(user, user.pawn_access_number)
      allow(user).to receive(:buyer_user?) { true }

      expect(policy.show?).to eq(true)
    end

    it 'is false when user is "buyer" and AA# is of another buyer' do
      policy = described_class.new(user, '100686866')
      allow(user).to receive(:buyer_user?) { true }
      allow(user).to receive(:kiosk_user?) { false }

      expect(policy.show?).to eq(false)
    end
  end
end
