# encoding: utf-8
require 'spec_helper'

describe SoldVehiclePolicy do
  let(:user) { double :user }

  before do
    allow(user).to receive(:vehicle_creator?) { false }
  end

  subject(:policy) { described_class.new(user) }

  describe '#show?' do
    it 'is false for other users' do
      expect(policy.show?).to eq(false)
    end

    it "is true for users with 'vehicle creator' role" do
      allow(user).to receive(:vehicle_creator?) { true }

      expect(policy.show?).to eq(true)
    end
  end

  describe '#new?' do
    it 'is false for other users' do
      expect(policy.new?).to eq(false)
    end

    it "is true for users with 'vehicle creator' role" do
      allow(user).to receive(:vehicle_creator?) { true }

      expect(policy.new?).to eq(true)
    end
  end

  describe '#edit?' do
    it 'is false for other users' do
      expect(policy.edit?).to eq(false)
    end

    it "is true for users with 'vehicle creator' role" do
      allow(user).to receive(:vehicle_creator?) { true }

      expect(policy.edit?).to eq(true)
    end
  end

  describe '#create?' do
    it 'is false for other users' do
      expect(policy.create?).to eq(false)
    end

    it "is true for users with 'vehicle creator' role" do
      allow(user).to receive(:vehicle_creator?) { true }

      expect(policy.create?).to eq(true)
    end
  end

  describe '#update?' do
    it 'is false for other users' do
      expect(policy.update?).to eq(false)
    end

    it "is true for users with 'vehicle creator' role" do
      allow(user).to receive(:vehicle_creator?) { true }

      expect(policy.update?).to eq(true)
    end
  end
end
