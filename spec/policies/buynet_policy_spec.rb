# encoding: utf-8
require 'spec_helper'

describe BuynetPolicy do
  describe '#skip_payment?' do
    let(:user) { double(super_user?: false, buyer_user?: false) }

    it 'is true when user is "super"' do
      policy = described_class.new(nil, user)

      allow(user).to receive(:super_user?) { true }
      allow(user).to receive(:invoiceable?) { true }

      expect(policy.skip_payment?).to eq(true)
    end

    it 'is true when user is "buyer" and non-reimbursable fees total > 0' do
      fees = double(total: 123)
      policy = described_class.new(fees, user)

      allow(user).to receive(:buyer_user?) { true }
      allow(user).to receive(:invoiceable?) { true }

      expect(policy.skip_payment?).to eq(true)
    end

    it 'is false when user is "buyer" and non-reimbursable fees total 0' do
      fees = double(total: 0)
      policy = described_class.new(fees, user)

      allow(user).to receive(:buyer_user?) { true }
      allow(user).to receive(:invoiceable?) { true }

      expect(policy.skip_payment?).to eq(false)
    end

    it 'is false when user is not "super" or "buyer"' do
      policy = described_class.new(nil, user)

      expect(policy.skip_payment?).to eq(false)
    end
  end
end
