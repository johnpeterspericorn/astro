# encoding: utf-8
require 'spec_helper'

describe FlatRatePolicy do
  let(:user) { double 'User' }

  before do
    allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(false)
  end

  describe '#index?' do
    subject(:policy) { described_class.new(user) }

    it 'authorizes super users' do
      allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)
      expect(policy.index?).to be_truthy
    end
  end

  describe '#build_offer?' do
    subject { described_class.new(user, flat_rate) }
    let(:user) { double 'User' }

    context 'when the flat rate is ineligible and unpersisted' do
      let(:flat_rate) { double 'FlatRate', eligible?: false, persisted?: false }

      it 'forbids all users' do
        expect(subject.build_offer?).to be_falsey
      end
    end

    context 'when the flat rate is persisted but ineligible' do
      let(:flat_rate) { double 'FlatRate', persisted?: true, eligible?: false }

      it 'permits super users' do
        allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)
        expect(subject.build_offer?).to be_truthy
      end
    end

    context 'when the flat rate is eligible' do
      let(:flat_rate) { double 'FlatRate', eligible?: true }

      it 'permits all users' do
        expect(subject.build_offer?).to be_truthy
      end
    end
  end
end
