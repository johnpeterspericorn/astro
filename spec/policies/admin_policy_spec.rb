# encoding: utf-8
require 'spec_helper'

describe AdminPolicy do
  let(:user) { double 'User' }

  subject(:policy) { described_class.new(user) }

  describe '#authorized?' do
    it 'is authorized when user is an admin' do
      allow(user).to receive(:admin?).and_return(true)

      expect(policy).to be_authorized
    end

    it 'is not authorized when user is not an admin' do
      allow(user).to receive(:admin?).and_return(false)

      expect(policy).to_not be_authorized
    end
  end
end
