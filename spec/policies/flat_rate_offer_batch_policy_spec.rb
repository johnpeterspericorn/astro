# encoding: utf-8
require 'spec_helper'

describe FlatRateOfferBatchPolicy do
  let(:user) { double :user }
  let(:batch) { double 'FlatRateOfferBatch' }

  subject(:policy) { described_class.new(user, batch) }

  before do
    allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(false)
    allow(user).to receive(:pawn_access_number).and_return(double)
    allow(user).to receive(:agreement_numbers).and_return([double])
    allow(batch).to receive(:pawn_access_no).and_return(double)
    allow(batch).to receive(:offered_agreement_numbers).and_return([double])
  end

  describe '#show?' do
    it 'authorizes super user' do
      allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)

      expect(policy.show?).to be_truthy
    end

    it "authorizes user with 100m matching batch's" do
      pawn_access_number = double :pawn_access_number
      allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
      allow(batch).to receive(:pawn_access_no).and_return(pawn_access_number)

      expect(policy.show?).to be_truthy
    end

    it 'authorizes user havnumg all offered 5m associated with their account' do
      user_agreement_numbers    = [1, 2, 3, 4]
      offered_agreement_numbers = [2, 4]
      allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
      allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

      expect(policy.show?).to be_truthy
    end

    it 'forbids user havnumg some offered 5m associated with their account' do
      user_agreement_numbers    = [1, 2, 3]
      offered_agreement_numbers = [2, 4]
      allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
      allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

      expect(policy.show?).to be_falsey
    end

    it 'forbids other users' do
      expect(policy.show?).to be_falsey
    end
  end

  describe '#edit?' do
    before do
      allow(user).to receive(:flat_rate_editor?) { false }
    end

    context 'user is a flat rate editor' do
      before do
        allow(user).to receive(:flat_rate_editor?) { true }
      end

      it 'authorizes super user' do
        allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)

        expect(policy.edit?).to be_truthy
      end

      it "authorizes user with 100m matching batch's" do
        pawn_access_number = double :pawn_access_number
        allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
        allow(batch).to receive(:pawn_access_no).and_return(pawn_access_number)

        expect(policy.edit?).to be_truthy
      end

      it 'authorizes user havnumg all offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3, 4]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.edit?).to be_truthy
      end

      it 'forbids user havnumg some offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.edit?).to be_falsey
      end
    end

    it 'forbids other users' do
      expect(policy.edit?).to be_falsey
    end
  end

  describe '#update?' do
    before do
      allow(user).to receive(:flat_rate_editor?) { false }
    end

    context 'user is a flat rate editor' do
      before do
        allow(user).to receive(:flat_rate_editor?) { true }
      end

      it 'authorizes super user' do
        allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)

        expect(policy.update?).to be_truthy
      end

      it "authorizes user with 100m matching batch's" do
        pawn_access_number = double :pawn_access_number
        allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
        allow(batch).to receive(:pawn_access_no).and_return(pawn_access_number)

        expect(policy.update?).to be_truthy
      end

      it 'authorizes user havnumg all offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3, 4]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.update?).to be_truthy
      end

      it 'forbids user havnumg some offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.update?).to be_falsey
      end
    end

    it 'forbids other users' do
      expect(policy.update?).to be_falsey
    end
  end

  describe '#create?' do
    before do
      allow(user).to receive(:flat_rate_editor?) { false }
    end

    context 'user is a flat rate editor' do
      before do
        allow(user).to receive(:flat_rate_editor?) { true }
      end

      it 'authorizes super user' do
        allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)

        expect(policy.create?).to be_truthy
      end

      it "authorizes user with 100m matching batch's" do
        pawn_access_number = double :pawn_access_number
        allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
        allow(batch).to receive(:pawn_access_no).and_return(pawn_access_number)

        expect(policy.create?).to be_truthy
      end

      it 'authorizes user havnumg all offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3, 4]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.create?).to be_truthy
      end

      it 'forbids user havnumg some offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.create?).to be_falsey
      end
    end

    it 'forbids other users' do
      expect(policy.create?).to be_falsey
    end
  end

  describe '#destroy?' do
    before do
      allow(user).to receive(:flat_rate_editor?) { false }
    end

    context 'user is a flat rate editor' do
      before do
        allow(user).to receive(:flat_rate_editor?) { true }
      end

      it 'authorizes super user' do
        allow(user).to receive(:flat_rate_inquiry_viewer?).and_return(true)

        expect(policy.destroy?).to be_truthy
      end

      it "authorizes user with 100m matching batch's" do
        pawn_access_number = double :pawn_access_number
        allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
        allow(batch).to receive(:pawn_access_no).and_return(pawn_access_number)

        expect(policy.destroy?).to be_truthy
      end

      it 'authorizes user havnumg all offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3, 4]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.destroy?).to be_truthy
      end

      it 'forbids user havnumg some offered 5m associated with their account' do
        user_agreement_numbers    = [1, 2, 3]
        offered_agreement_numbers = [2, 4]
        allow(user).to receive(:agreement_numbers).and_return(user_agreement_numbers)
        allow(batch).to receive(:offered_agreement_numbers).and_return(offered_agreement_numbers)

        expect(policy.destroy?).to be_falsey
      end
    end

    it 'forbids other users' do
      expect(policy.destroy?).to be_falsey
    end
  end
end
