# encoding: utf-8
require 'spec_helper'

describe ReturnInvoicePolicy do
  let(:pawn_access_number) { double :pawn_access_number }
  let(:return_invoice)       { double 'ReturnInvoice' }
  let(:user)                 { double 'User' }

  before do
    allow(user).to receive(:super_user?).and_return(false)
    allow(user).to receive(:buyer_user?).and_return(false)
    allow(user).to receive(:kiosk_user?).and_return(false)
  end

  describe '#create?' do
    subject(:policy) { described_class.new(user, return_invoice) }

    it 'is authorized for super users' do
      allow(user).to receive(:super_user?).and_return(true)
      expect(policy.create?).to be_truthy
    end

    it 'is authorized for kiosk users' do
      allow(user).to receive(:super_user?).and_return(false)
      allow(user).to receive(:kiosk_user?).and_return(true)
      expect(policy.create?).to be_truthy
    end

    context 'for buyer user' do
      let(:promise_offer) { double(pawn_access_number: pawn_access_number) }
      it 'is authorized when users pawn_access_number matches the promise' do
        allow(return_invoice).to receive(:promise_offer).and_return(promise_offer)
        allow(user).to receive(:super_user?).and_return(false)
        allow(user).to receive(:kiosk_user?).and_return(false)
        allow(user).to receive(:username).and_return('testacular')
        allow(user).to receive(:owns_offer?).with(promise_offer) { true }

        expect(policy.create?).to be_truthy
      end
    end
  end

  describe '#show?' do
    let(:agreement_number) { double :agreement_number }

    it 'does not call Astro::BUYER_AUTHORIZATION_CLASS if missing AA# and not buyer user' do
      allow(user).to receive(:buyer_user?).and_return(false)
      policy = described_class.new(user, return_invoice)

      expect_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).not_to receive(:get_agreement_numbers)
      expect(policy.show?).to be_falsey
    end

    context 'super users' do
      subject(:policy) { described_class.new(user, return_invoice, pawn_access_number) }
      before do
        allow(user).to receive(:super_user?).and_return(true)
      end
      specify { expect(policy.show?).to be_truthy }
    end

    context 'kiosk users' do
      subject(:policy) { described_class.new(user, return_invoice, pawn_access_number) }
      before do
        allow(user).to receive(:buyer_user?).and_return(false)
        allow(user).to receive(:super_user?).and_return(false)
        allow(user).to receive(:kiosk_user?).and_return(true)
      end

      it 'is true even without an AA#' do
        policy = described_class.new(user, return_invoice, nil)
        expect(policy.show?).to be_truthy
      end

      it 'should be true even if the AA# does not match our passed AA#' do
        allow(return_invoice).to receive(:promise_purchase).and_return(double(promise_offer: double(pawn_access_no: 'bad AA#')))
        expect(policy.show?).to be_truthy
      end

      it 'should be true if the invoice AA# matches our passed AA#' do
        allow(return_invoice).to receive(:promise_purchase).and_return(double(promise_offer: double(pawn_access_no: pawn_access_number)))
        expect(policy.show?).to be_truthy
      end
    end

    context 'buyer user' do
      subject(:policy) { described_class.new(user, return_invoice) }
      let(:promise_offer) { double(pawn_access_number: pawn_access_number) }

      before do
        allow(user).to receive(:kiosk_user?).and_return(false)
        allow(user).to receive(:buyer_user?).and_return(true)
        allow(user).to receive(:pawn_access_number).and_return(pawn_access_number)
        allow(return_invoice).to receive(:promise_offer).and_return(promise_offer)
      end

      it 'is authorized if buyer users agreement numbers include returns agreement number' do
        allow(user).to receive(:owns_offer?).with(promise_offer) { true }
        expect(policy.show?).to be_truthy
      end

      it 'is not authorized if buyer user does not own the offer' do
        allow(user).to receive(:owns_offer?).with(promise_offer) { false }
        expect(policy.show?).to be_falsey
      end
    end
  end

  describe '#update?' do
    subject(:policy) { described_class.new(user, return_invoice) }

    context 'super user' do
      before do
        allow(return_invoice).to receive(:vehicle_received).and_return(false)
        allow(user).to receive(:super_user?).and_return(true)
      end

      specify { expect(policy.update?).to be_truthy }
    end

    context 'kiosk user' do
      before do
        allow(user).to receive(:kiosk_user?).and_return(true)
      end

      specify { expect(policy.update?).to be_falsey }
    end

    context 'buyer user' do
      before do
        allow(user).to receive(:buyer_user?).and_return(true)
      end

      specify { expect(policy.update?).to be_falsey }
    end
  end
end
