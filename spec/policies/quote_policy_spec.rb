# encoding: utf-8
require 'spec_helper'

describe QuotePolicy do
  describe '#show?' do
    it 'is authorized when user has requested buyer number' do
      agreement_number = '5000000'
      user   = double :user
      quote  = double :quote
      policy = described_class.new(user, quote)

      allow(quote).to receive(:buyer_number).and_return(agreement_number)
      allow(user).to receive(:api_user?).and_return(false)
      allow(user).to receive(:agreement_numbers).and_return([agreement_number])

      expect(policy.show?).to be_truthy
    end
  end
end
