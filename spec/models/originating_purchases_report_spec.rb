# encoding: utf-8
require 'spec_helper'

describe OriginatingPurchasesReport do
  let(:return1) { FactoryGirl.create(:return_invoice) }
  let(:return2) { FactoryGirl.create(:return_invoice) }
  let(:returns) { [return1, return2] }
  let(:pawn_location) { FactoryGirl.create(:pawn_location) }
  let(:date_from) { Date.current.at_beginning_of_week }
  let(:date_to) { Date.current.end_of_day }

  it 'accepts a list of returns' do
    expect { OriginatingPurchasesReport.new(returns, pawn_location, date_from, date_to) }.not_to raise_error
  end

  describe 'instance methods' do
    subject { OriginatingPurchasesReport.new(returns, pawn_location, date_from, date_to) }
    let(:csv_file) { StringIO.new }
    let(:local_file) { '/tmp/abcd.csv' }

    before do
      allow(subject).to receive(:local_file_path).and_return(local_file)
      allow(File).to receive(:open).with(local_file, 'w', anything).and_return(csv_file)
      allow_any_instance_of(S3File).to receive(:upload).with(local_file)
      allow_any_instance_of(S3File).to receive(:presigned_url)
    end

    it 'creates and uploads a csv file with the cancelled returns' do
      expect_any_instance_of(S3File).to receive(:upload).with(local_file)
      subject.create_and_upload
      expect(csv_file.string).to include(return1.id.to_s, return2.id.to_s)
    end

    it 'provides a link to the presigned_url for the s3 resource' do
      expect_any_instance_of(S3File).to receive(:presigned_url)

      subject.create_and_upload
      subject.presigned_url
    end
  end
end
