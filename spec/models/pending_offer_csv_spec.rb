# encoding: utf-8
require 'spec_helper'

describe PendingOfferCsv do
  describe 'Sanity check' do
    let(:promise_offer) { FactoryGirl.create(:promise_offer) }

    subject { described_class.new([promise_offer]) }

    it 'yields a CSV string' do
      expect(subject.to_s).to be_present
    end
  end
end
