# encoding: utf-8
require 'spec_helper'

describe Coupon do
  describe '.new_with_code' do
    it 'initializes a new Coupon with provided args' do
      amount = 12.3
      coupon = described_class.new_with_code(amount: amount)

      expect(coupon.amount).to eq(amount)
    end

    it 'generates a CODE_LENGTH code for the new Coupon' do
      coupon = described_class.new_with_code

      expect(coupon.code.length).to eq(described_class::CODE_LENGTH)
    end
  end
end
