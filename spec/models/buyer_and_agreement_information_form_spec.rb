# encoding: utf-8
require 'spec_helper'

describe BuyerAndAgreementInformationForm do
  describe '#update' do
    let(:pawn_access_number) { '1234' }
    let(:agreement_number)        { '4321' }
    let(:enable_verification)  { 'true' }
    let(:country)              { SupportedCountry::DEFAULT }

    subject(:info_form) { described_class.new(pawn_access_number, agreement_number, enable_verification, country) }

    before do
      allow_any_instance_of(Address).to receive(:valid?).and_return(true)
    end

    context 'existing records' do
      let(:email)                { 'test@example.com' }
      let(:agreementship_name)      { 'Fast Ridez'       }

      let(:buyer_information) { BuyerInformation.find_by_pawn_access_no(pawn_access_number) }
      let(:agreement_information) { AgreementInformation.find_by_agreement_no(agreement_number) }

      before do
        BuyerInformation.create!(pawn_access_no: pawn_access_number)
        AgreementInformation.create!(agreement_no: agreement_number)
      end

      it 'updates BuyerInformation' do
        info_form.update(emails: email)

        expect(buyer_information.emails).to eq(email)
      end

      it 'updates AgreementInformation' do
        info_form.update(agreementship_name: agreementship_name)

        expect(agreement_information.agreementship_name).to eq(agreementship_name)
      end
    end

    context 'new records' do
      it 'creates BuyerInformation' do
        expect { info_form.update({}) }.to change { BuyerInformation.count }.by(1)
      end

      it 'creates AgreementInformation' do
        expect { info_form.update({}) }.to change { AgreementInformation.count }.by(1)
      end
    end
  end
end
