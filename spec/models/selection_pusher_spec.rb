# encoding: utf-8
require 'spec_helper'

describe SelectionPusher do
  describe '.push_to_dashboard' do
    let(:sold_vehicle) { FactoryGirl.create(:sold_vehicle, vehicle_attributes) }
    let(:email) { 'foo@bar.com' }
    let!(:promise_offer) { PromiseOfferRecorder.record_offer(sold_vehicle).decorate }
    let(:promise_purchase) { PromisePurchase.from_options(sold_vehicle.vnum, 250, 7).decorate }
    let(:updater) { PromiseSelectionRecorder.new(promise_purchase, email) }

    it 'pushes to the dashboard' do
      event = 'record_offered'
      expect(Pusher).to receive(:trigger).with(described_class::DASHBOARD_CHANNEL, event, kind_of(String))
      described_class.new(updater).send(:push_to_dashboard, event)
    end

    it 'logs if an exception is thrown' do
      message = 'Exception'
      expect(Pusher).to receive(:trigger).and_raise Pusher::Error.new(message)
      expect(Rails.logger).to receive(:error)
      described_class.new(updater).send(:push_to_dashboard, 'some event')
    end
  end
end
