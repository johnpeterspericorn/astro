# encoding: utf-8
require 'spec_helper'
describe ExternalVehiclesLoad do
  describe 'sold_vehicles_columns' do
    it 'should ensure the columns in sold_vehicles and external_vehicles_load table match up' do
      external_load_specif_col = %w(batch_id channel past_time status user_id rejection_comment vehicle_total external_vehicles_batch_id approvnumg_user_id rejecting_user_id seller_name)
      expect(SoldVehicle.column_names.reject { |col| %w(pawn_access_no).include?(col) }.sort).to match_array(ExternalVehiclesLoad.column_names.reject { |col| external_load_specif_col.include?(col) }.sort)
    end
  end
end
