# encoding: utf-8
require 'spec_helper'

describe TransactionIdGenerator do
  describe 'initialization' do
    it 'defaults pawn access number to zero' do
      generator = described_class.new

      expect(generator.pawn_access_number).to eq(0)
    end

    it 'defaults nil pawn access number to zero' do
      generator = described_class.new(nil)

      expect(generator.pawn_access_number).to eq(0)
    end
  end

  describe '#generate' do
    let(:pawn_access_number) { '10000001' }
    let(:now) { 123_456_789 }
    let(:random_blob) { 'random_blob' }

    subject(:generator) { described_class.new(pawn_access_number) }

    it "returns an id including the user's AA#, timestamp, and random string" do
      expect(generator.generate(now, random_blob)).to eq("#{pawn_access_number}_#{now}_#{random_blob}")
    end
  end
end
