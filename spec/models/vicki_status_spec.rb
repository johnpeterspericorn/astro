# encoding: utf-8
require 'spec_helper'

describe VickiStatus do
  let(:client) { double 'AstroheimAuthorization' }

  subject(:vicki_status) { described_class.new(client) }

  describe '#up?' do
    it "is up when it's not down" do
      allow(client).to receive(:five_million_numbers_to_names)

      expect(vicki_status).to be_up
    end
  end

  describe '#down?' do
    it 'is down when vicki client call times out with Timeout::Error' do
      expect(client).to receive(:five_million_numbers_to_names).and_raise(Timeout::Error)

      expect(vicki_status).to be_down
    end

    it 'is down when vicki client call times out with Errno::ETIMEDOUT' do
      expect(client).to receive(:five_million_numbers_to_names).and_raise(Timeout::Error)

      expect(vicki_status).to be_down
    end

    it 'is down when the vicki client call connection is reset' do
      expect(client).to receive(:five_million_numbers_to_names).and_raise(Errno::ECONNRESET)

      expect(vicki_status).to be_down
    end

    it 'is down when the vicki client call connection is refused' do
      expect(client).to receive(:five_million_numbers_to_names).and_raise(Errno::ECONNREFUSED)

      expect(vicki_status).to be_down
    end

    it 'is down when the vicki call results in a socket error' do
      expect(client).to receive(:five_million_numbers_to_names).and_raise(SocketError)

      expect(vicki_status).to be_down
    end
  end
end
