# encoding: utf-8
require 'spec_helper'

describe NetworkPlusOffer do
  describe 'expiration_date_input' do
    it 'calculates if date is current date' do
      npo = FactoryGirl.create(:network_plus_offer, expiration_date: Time.zone.now)
      expect(npo.valid?).to be_truthy
    end

    it 'calculates if date is yesterday' do
      npo = FactoryGirl.build(:network_plus_offer, expiration_date: Time.zone.now - 1.day)
      expect(npo.valid?).to be_falsey
    end

    it 'calculates if date is on 30th day' do
      npo = FactoryGirl.create(:network_plus_offer, expiration_date: Time.zone.now + 30.days)
      expect(npo.valid?).to be_truthy
    end
  end
end
