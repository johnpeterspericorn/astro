# encoding: utf-8
require 'spec_helper'

describe FlatRateAttribute do
  describe 'flat rates for selected miles and psi eligibility' do
    let(:flat_rate) { FactoryGirl.create(:flat_rate) }

    def selected_flat_rate_parameter(miles, psi_eligibility)
      described_class.new(flat_rate: flat_rate, miles: miles, psi: psi_eligibility)
    end

    it 'selects flat rates for 360 miles when the quote is built with 360 miles' do
      flat_rate_param = selected_flat_rate_parameter('360', false)
      expect(flat_rate_param.target_price).to eq(flat_rate.target_price_360)
    end

    it 'selects flat rates for 500 miles when the quote is built with 500 miles' do
      flat_rate_param = selected_flat_rate_parameter('500', false)
      expect(flat_rate_param.target_price).to eq(flat_rate.target_price_500)
    end

    it 'selects psi eligibile 360 miles flat rates when the quote is built with 360 miles and psi eligibility set true' do
      flat_rate_param = selected_flat_rate_parameter('360', true)
      expect(flat_rate_param.target_price).to eq(flat_rate.target_price_360_psi)
    end

    it 'selects psi eligibile 500 miles flat rates when the quote is built with 500 miles and psi eligibility set true' do
      flat_rate_param = selected_flat_rate_parameter('500', true)
      expect(flat_rate_param.target_price).to eq(flat_rate.target_price_500_psi)
    end
  end
end
