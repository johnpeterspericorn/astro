# encoding: utf-8
require 'spec_helper'

RSpec.describe DashboardScore, type: :model do
  let(:record) { FactoryGirl.build(:dashboard_score) }
  describe 'creating new records' do
    it 'creates new record from factory' do
      expect { record.save! }.to change { described_class.count }.by(1)
      expect(record.buyer_num).to eq(5_000_000)
      expect(record.payment_kind).to eq('DS360')
      expect(record.avg_loss).to eq(758.923)
      expect(record.return_rate).to eq(5.808)
      expect(record.volume).to eq(24.89)
      expect(record.margin).to eq(70.62)
      expect(record.earnings).to eq(2636.769)
      expect(record.return_score).to eq(122)
      expect(record.purchase_quality).to eq(162)
      expect(record.margin_score).to eq(129)
      expect(record.volume_score).to eq(155)
      expect(record.earnings_score).to eq(167)
      expect(record.date).to eq(Time.zone.today)
    end

    it 'does not save without required fields' do
      record.date = nil
      record.buyer_num = nil
      record.payment_kind = nil
      record.save
      errors = record.errors
      expect(record).to_not be_persisted
      expect(errors).to include(:date)
      expect(errors).to include(:buyer_num)
      expect(errors).to include(:payment_kind)
    end
  end
end
