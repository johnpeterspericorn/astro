# encoding: utf-8
require 'spec_helper'

describe PurchaseProtectKind do
  let(:days_selected) { 7 }
  let(:offer) { FactoryGirl.create(:promise_offer, days_selected: days_selected, purchased_at: purchase_date) }
  let(:purchase_date) { Date.new(2018, 4, 9) }
  let(:return_date) { purchase_date + 1.day }
  let(:purchase) { FactoryGirl.create(:promise_purchase, promise_offer: offer, additional_days: 0) }

  subject { described_class.new(purchase) }

  describe 'promise extension' do
    before do
      purchase.update(extension_days: 2)
    end

    it 'extends max_return_time by additional days of promise extension' do
      additional_promise_duration = purchase_date + days_selected.days + 2.days
      expect(subject.max_return_time).to eql additional_promise_duration.end_of_day
    end

    it 'extends receive_by_date by additional days of promise extension' do
      receive_by_date = purchase_date + (days_selected + 1).days + 2.days
      expect(subject.receive_by_date.to_date).to eql(receive_by_date.to_date)
    end
  end

  describe 'Program360' do
    before do
      allow_any_instance_of(PromisePurchase).to receive(:program360?).and_return(true)
    end

    describe '#min_return_time' do
      it 'is the end of day on promised_at date' do
        expect(subject.min_return_time).to eql purchase_date.end_of_day
      end
    end

    describe '#max_return_time' do
      it 'ends at the end of day on "days selected" days after the purchase date' do
        additional_promise_duration = purchase_date + days_selected.days

        expect(subject.max_return_time).to eql additional_promise_duration.end_of_day
      end
    end

    describe '#receive_by_date' do
      it 'should be days selected days + extra days after purchase' do
        receive_by_date = purchase_date + (days_selected + 1).days
        expect(subject.receive_by_date.to_date).to eql(receive_by_date.to_date)
      end
    end

    describe '#expiration_date and #receive_by_date' do
      it 'will expire 3 days after receive_by_date when return attempted before promise_expiration_date' do
        promise_expiration_date = Date.new(2018, 5, 6) # a tuesday
        allow(subject).to receive(:promise_expiration_date).and_return(promise_expiration_date)
        Timecop.freeze(subject.promise_expiration_date - 1.day) do
          expect(subject.receive_by_date.to_date).to eql(promise_expiration_date.to_date)
          expect(subject.return_expiration_date).to eq(DateUtils.resolve_to_weekday(promise_expiration_date + 3.days))
        end
      end

      it 'will expire 2 days after receive_by_date when return attempted on promise_expiration_date' do
        promise_expiration_date = Date.new(2018, 5, 6) # a tuesday
        allow(subject).to receive(:promise_expiration_date).and_return(promise_expiration_date)
        Timecop.freeze(subject.promise_expiration_date) do
          expect(subject.receive_by_date.to_date).to eql(promise_expiration_date.to_date + 1.day)
          expect(subject.return_expiration_date).to eq(DateUtils.resolve_to_weekday(promise_expiration_date + 3.days))
        end
      end
    end
  end
  describe 'NonProgram360' do
    describe '#expiration_date and #receive_by_date' do
      it 'receive_by_date will be the promise_expiration_date if returned before promise_expiration_date' do
        promise_expiration_date = Date.new(2018, 5, 6) # a tuesday
        allow(subject).to receive(:promise_expiration_date).and_return(promise_expiration_date)
        Timecop.freeze(subject.promise_expiration_date - 1.day) do
          expect(subject.receive_by_date.to_date).to eql(promise_expiration_date.to_date)
          expect(subject.expiration_date).to eq(DateUtils.resolve_to_weekday(subject.receive_by_date))
        end
      end

      it 'receive_by_date will be one day after promise_expiration_date if returned on promise_expiration_date' do
        promise_expiration_date = Date.new(2018, 5, 6) # a tuesday
        allow(subject).to receive(:promise_expiration_date).and_return(promise_expiration_date)
        Timecop.freeze(subject.promise_expiration_date) do
          expect(subject.receive_by_date.to_date).to eql(promise_expiration_date.to_date + 1.day)
          expect(subject.expiration_date).to eq(DateUtils.resolve_to_weekday(subject.receive_by_date))
        end
      end
    end
  end
end
