# encoding: utf-8
require 'spec_helper'

describe TotalProtectKind do
  let(:offer) { FactoryGirl.create(:promise_offer, days_selected: 21) }
  let(:purchase) { FactoryGirl.create(:promise_purchase, promised_at: Time.current, promise_offer: offer, additional_days: 0) }
  let(:purchase_date) { purchase.promised_at }
  let(:days_selected) { offer.days_selected }

  subject { described_class.new(purchase) }

  describe 'promise extension' do
    before do
      purchase.update(extension_days: 2)
    end

    it 'extends max_return_time by additional days of promise extension' do
      max_return_time = purchase_date.end_of_day + 67.days + 2.days
      expect(subject.max_return_time).to eql max_return_time
    end

    describe '#receive_by_date extension' do
      context 'within first window' do
        it 'should be days selected days + after purchase' do
          purchase.promised_at = 1.day.ago
          receive_by_date = purchase.promised_at.end_of_day + days_selected.days + 2.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'within second window' do
        it 'should be 67 days after purchase' do
          purchase.promised_at = 61.days.ago
          receive_by_date = purchase.promised_at.end_of_day + 67.days + 2.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'before first window' do
        it 'should be days selected days + after purchase' do
          purchase.promised_at = 1.day.from_now
          receive_by_date = purchase.promised_at.end_of_day + days_selected.days + 2.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'after first window' do
        it 'should be 67 days after purchase' do
          purchase.promised_at = 30.days.ago
          receive_by_date = purchase.promised_at.end_of_day + 67.days + 2.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end
    end
  end

  describe 'Program360' do
    before do
      allow_any_instance_of(PromisePurchase).to receive(:program360?).and_return(true)
    end

    describe 'return eligibility within first window' do
      context 'during the first return window' do
        it 'is eligible for return after minimum return time' do
          purchase.promised_at = 1.day.ago
          expect(subject.within_window?).to be_truthy
        end

        it 'is eligible for return before maximum return time' do
          purchase.promised_at = 21.days.ago
          expect(subject.within_window?).to be_truthy
        end
      end
    end

    describe 'return eligibility outside first window' do
      context 'past the first return window' do
        it 'ineligible for return on mase day' do
          purchase.promised_at = Time.current
          expect(subject.within_window?).to be_falsey
        end

        it 'ineligible for return 22 days earlier' do
          purchase.promised_at = 22.days.ago
          expect(subject.within_window?).to be_falsey
        end
      end
    end

    describe 'return eligibility within second window' do
      context 'during the second return window' do
        it 'is eligible for return after minimum return time' do
          purchase.promised_at = 61.days.ago
          expect(subject.within_window?).to be_truthy
        end

        it 'is eligible for return before maximum return time' do
          purchase.promised_at = 67.days.ago
          expect(subject.within_window?).to be_truthy
        end
      end
    end

    describe 'return eligibility outside the second return window' do
      context 'past the second return window' do
        it 'ineligible for return before minimum return time' do
          purchase.promised_at = 60.days.ago
          expect(subject.within_window?).to be_falsey
        end

        it 'ineligible for return after maximum return time' do
          purchase.promised_at = 68.days.ago
          expect(subject.within_window?).to be_falsey
        end
      end
    end

    describe '#max_return_time' do
      it 'is 67 days after promised_at date' do
        expect(subject.max_return_time).to eql purchase_date.end_of_day + 67.days
      end
    end

    describe '#receive_by_date' do
      context 'within first window' do
        it 'should be days selected days + after purchase' do
          purchase.promised_at = 1.day.ago
          receive_by_date = purchase.promised_at.end_of_day + days_selected.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'within second window' do
        it 'should be 67 days after purchase' do
          purchase.promised_at = 61.days.ago
          receive_by_date = purchase.promised_at.end_of_day + 67.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'before first window' do
        it 'should be days selected days + after purchase' do
          purchase.promised_at = 1.day.from_now
          receive_by_date = purchase.promised_at.end_of_day + days_selected.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end

      context 'after first window' do
        it 'should be 67 days after purchase' do
          purchase.promised_at = 30.days.ago
          receive_by_date = purchase.promised_at.end_of_day + 67.days
          expect(subject.receive_by_date).to eql(receive_by_date)
        end
      end
    end

    describe '#expiration_date' do
      context 'return expiration falls on a weekday when receive by date is a monday' do
        it 'will expire on a wednesday' do
          monday = Date.new(2018, 5, 5)
          allow(subject).to receive(:receive_by_date).and_return(monday)
          wednesday = Date.new(2018, 5, 7)
          expect(subject.expiration_date).to eq(wednesday)
        end
      end

      context 'return expiration falls on the weekend when receive by date is a thursday' do
        it 'will expire next week which is a monday' do
          thursday = Date.new(2018, 5, 8)
          monday = Date.new(2018, 5, 12)
          allow(subject).to receive(:receive_by_date).and_return(thursday)
          expect(subject.expiration_date).to eq(monday)
        end
      end
    end
  end
end
