# encoding: utf-8
require 'spec_helper'

describe NewProductFee do
  let(:new_product_fee) { FactoryGirl.create(:new_product_fee) }

  describe '#new_product_fee', versioning: true do
    context 'when new product fee created' do
      it { expect(new_product_fee).to be_versioned }
    end

    context 'when new product fee updated' do
      before do
        new_product_fee.update_attributes!(timeframe: 366)
      end

      it { expect(new_product_fee.versions.last.reify.timeframe).to be 365 }
    end
  end
end
