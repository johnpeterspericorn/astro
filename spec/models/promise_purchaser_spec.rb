# encoding: utf-8
require 'spec_helper'

describe PromisePurchaser do
  let(:days)    { 7   }
  let(:miles)   { 250 }
  let(:email)   { 'test@example.com' }
  let(:offer)   { FactoryGirl.create(:promise_offer) }

  let(:purchaser) do
    PromisePurchaser.new(
      offer: offer,
      email: email,
      additional_days: days,
      miles: miles,
      channel: PromiseChannel::API
    )
  end

  describe '#record_purchase' do
    let(:purchase) { double 'PromisePurchase' }
    let(:mail_stub) { double 'Mailer', deliver_later: nil }

    before do
      Delayed::Worker.delay_jobs = true
    end

    after do
      Delayed::Worker.delay_jobs = false
    end

    it 'creates a new purchase for the offer' do
      expect do
        purchaser.record_purchase
      end.to change(PromisePurchase, :count).by(1)
    end

    it 'enqueues a SOWS job' do
      Delayed::Worker.delay_jobs = false
      Settings.charges_enabled = true
      Settings.sows_enabled = true
      offer.pawn_information.update_attributes!(sblu: '123')
      allow_any_instance_of(PromiseCharge).to receive(:submit)
      expect(SowsPurchaseJob).to receive(:enqueue).with(offer)
      purchaser.record_purchase
    end

    it 'sends a purchase confirmation' do
      expect(PromisePurchaseMailer).to receive(:purchase_confirmation) { mail_stub }
      purchaser.record_purchase
    end

    context 'when offer is pending' do
      before do
        VinBlacklist.create!(vnum: offer.vnum)
      end

      it 'does not query sows' do
        Settings.sows_enabled = true
        expect(SowsPurchaseJob).not_to receive(:enqueue)
        purchaser.record_purchase
      end

      it 'sends a pending confirmation' do
        expect(PromisePurchaseMailer).to receive(:pending_confirmation) { mail_stub }
        purchaser.record_purchase
      end
    end

    context 'when price match is disabled' do
      let(:requested_price) { 100.0 }

      before do
        Settings.disable_purchase_api_price_match = true
      end

      it 'updates the promise_price on the offer when a price is present' do
        purchaser.price = requested_price

        purchaser.record_purchase
        promise_offer = PromiseOffer.find(offer.id)
        expect(promise_offer.promise_price).to eql(purchaser.price)
      end

      it 'does not update the promise_price on the offer when no price is passed' do
        purchaser.record_purchase
        promise_offer = PromiseOffer.find(offer.id)
        expect(promise_offer.promise_price).to_not eql(requested_price)
      end
    end
  end
end
