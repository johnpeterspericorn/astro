# encoding: utf-8
require 'spec_helper'

describe SalesLocations do
  describe '#each' do
    let(:sales_data_class) { double 'SalesData' }

    it 'yields the pawn location and sales data for each location' do
      location = double :location
      sale = double :sale, pawn_location: location
      locations = described_class.new(sale)

      expect(sales_data_class).to receive(:new).and_return(sale)

      expect { |b| locations.each(sales_data_class, &b) }
        .to yield_with_args(location, sale)
    end
  end
end
