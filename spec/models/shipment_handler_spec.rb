# encoding: utf-8
require 'spec_helper'

describe ShipmentHandler do
  let(:agreement_information) { FactoryGirl.build(:agreement_information) }

  subject(:shipment) { described_class.new(agreement_information) }

  ups_vcr_options = { cassette_name: 'easypost/successful_shipment' }
  fedex_vcr_options = { cassette_name: 'easypost/fedex_successful_shipment' }
  fedex_vcr_with_secondary_carrier_rate = { cassette_name: 'easypost/fedex_successful_shipment_with_secondary_carrier_rate' }
  ups_pdf_vcr_options = { cassette_name: 'easypost/ups_pdf_label_successful_shipment' }
  fedex_pdf_vcr_options = { cassette_name: 'easypost/fedex_pdf_label_successful_shipment' }

  describe '#buy', vcr: ups_vcr_options do
    it 'returns an EasyPost::Shipment' do
      easypost_shipment = shipment.buy

      expect(easypost_shipment.id).to match(/^shp_/)
      expect(easypost_shipment.tracking_code).to match(/^1Z/)
    end
  end

  context 'switch carriers' do
    describe 'default UPS carrier', vcr: ups_vcr_options do
      it 'buys UPS NextDayAirShipment rates by default' do
        easypost_shipment = shipment.buy

        expect(shipment.carrier_params).to be(ShipmentHandler::UPS)
        expect(easypost_shipment.rates.map(&:carrier).uniq).to eq(['UPS'])
      end
    end

    describe 'Fedex Carrier', vcr: fedex_vcr_options do
      it 'buys FedEx rates when flag is enabled' do
        Settings.fedex_shipments_enabled = true
        fedex_shipment = ShipmentHandler.new(agreement_information)
        easypost_shipment = shipment.buy
        expect(fedex_shipment.carrier_params).to be(ShipmentHandler::FEDEX)
        expect(easypost_shipment.rates.map(&:carrier).uniq).to eq(['FedEx'])
      end
    end

    describe 'Fedex Carrier', vcr: fedex_vcr_with_secondary_carrier_rate do
      it 'buys FedEx rates when flag is enabled and primary service missing' do
        Settings.fedex_shipments_enabled = true
        easypost_shipment = shipment.buy
        expect(easypost_shipment.rates.map(&:carrier).uniq).to eq(['FedEx'])
      end
    end
  end

  context 'switch shipment url format' do
    describe 'Shipment URL format for fedex should be PDF when flag enabled', vcr: fedex_pdf_vcr_options do
      it 'renders shipment label in pdf format' do
        Settings.shipment_label_pdf_enabled = true
        easypost_shipment = shipment.buy
        expect(easypost_shipment.postage_label['label_url']).to include('.pdf')
      end
    end

    describe 'Shipment URL format for ups should be PDF when flag enabled', vcr: ups_pdf_vcr_options do
      it 'renders shipment label in pdf format' do
        Settings.shipment_label_pdf_enabled = true
        easypost_shipment = shipment.buy

        expect(easypost_shipment.postage_label['label_url']).to include('.pdf')
      end
    end

    describe 'Shipment URL format for fedex should be PNG when flag disabled', vcr: fedex_vcr_options do
      it 'renders shipment label in png format' do
        easypost_shipment = shipment.buy
        expect(easypost_shipment.postage_label['label_url']).to include('.png')
      end
    end

    describe 'Shipment URL format for fedex should be PNG when flag disabled', vcr: ups_vcr_options do
      it 'renders shipment label in png format' do
        easypost_shipment = shipment.buy
        expect(easypost_shipment.postage_label['label_url']).to include('.png')
      end
    end
  end

  context 'swap from and to addresses for fedex' do
    describe 'from and to address should be swapped', vcr: fedex_vcr_options do
      it 'renders from address as the to address and vice versa when fedex flag is enabled' do
        Settings.fedex_shipments_enabled = true
        easypost_shipment = shipment.buy
        expect(easypost_shipment.to_address['company']).to include('Returns Department')
        expect(easypost_shipment.from_address['company']).to include('Attn. Accounting')
      end
    end

    describe 'from and to address should NOT be swapped for ups', vcr: ups_vcr_options do
      it 'renders from address as the from address itself when fedex flag is NOT enabled' do
        Settings.fedex_shipments_enabled = false
        easypost_shipment = shipment.buy
        expect(easypost_shipment.from_address['company']).to include('AgreementShield')
      end
    end
  end
end
