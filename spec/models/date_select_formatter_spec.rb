# encoding: utf-8
require 'spec_helper'

describe DateSelectFormatter do
  describe 'valid date' do
    let(:date) { Date.current }

    subject(:date_select_formatter) { described_class.new('date(1i)' => date.year, 'date(2i)' => date.month, 'date(3i)' => date.day) }

    it 'is valid for correct date' do
      expect(date_select_formatter).to be_valid
    end

    it 'returns formatted date when valid' do
      expect(date_select_formatter.format).to eq(date)
    end
  end

  describe 'invalid date' do
    it 'is invalid when date parameter is blank' do
      invalid_date = { 'date(1i)' => '2018', 'date(2i)' => '', 'date(3i)' => '02' }
      expect(described_class.new(invalid_date)).to_not be_valid
    end

    it 'is invalid when any month parameter is blank' do
      invalid_date = { 'date(1i)' => '2018', 'date(2i)' => '02', 'date(3i)' => '' }
      expect(described_class.new(invalid_date)).to_not be_valid
    end

    it 'is invalid when any year parameter is blank' do
      invalid_date = { 'date(1i)' => '', 'date(2i)' => '', 'date(3i)' => '02' }
      expect(described_class.new(invalid_date)).to_not be_valid
    end

    it 'is invalid when an incorrect date is entered' do
      invalid_date = { 'date(1i)' => '2018', 'date(2i)' => '02', 'date(3i)' => '31' }
      expect(described_class.new(invalid_date)).to_not be_valid
    end

    it 'is invalid when an incorrect month is entered' do
      invalid_date = { 'date(1i)' => '2018', 'date(2i)' => '28', 'date(3i)' => '31' }
      expect(described_class.new(invalid_date)).to_not be_valid
    end

    it 'returns nil when format is called on an invalid date' do
      invalid_date = { 'date(1i)' => '2018', 'date(2i)' => '28', 'date(3i)' => '31' }
      expect(described_class.new(invalid_date).format).to eq nil
    end
  end
end
