# encoding: utf-8
require 'spec_helper'

describe FlatRateOfferBatchApproval do
  let(:batch) { double 'FlatRateOfferBatch' }
  let(:user)  { double :user }
  let(:time)  { double :time }
  let(:mailer) { double :mailer }
  let(:mail)  { double :mail }
  let(:offer) { double 'FlatRateOffer' }
  let(:effective_date) { double :date }

  subject(:approval) { described_class.new(batch, user, effective_date, time, mailer) }

  before do
    allow(batch).to receive(:flat_rate_offers).and_return([offer])
    allow(batch).to receive(:id).and_return(1)
  end

  describe '#approve' do
    before do
      allow(offer).to receive(:accept)
      allow(mailer).to receive(:offer_accepted).and_return(mail)
      allow(mail).to receive(:deliver_later)
      FileUtils.touch('public/flat_rate_offer_doc_1.pdf')
    end

    it 'accepts the offers' do
      expect(offer)
        .to receive(:accept)
        .with(user, time)

      approval.approve
    end

    it 'sends the acceptance notification' do
      expect(mailer)
        .to receive(:offer_accepted)
        .with(batch, user, time.to_s, effective_date.to_s)
        .and_return(mail)
      expect(mail).to receive(:deliver_later)

      approval.approve
    end
  end

  describe '#reject' do
    before do
      allow(offer).to receive(:reject)
      allow(mailer).to receive(:offer_rejection).and_return(mail)
      allow(mail).to receive(:deliver_later)
    end

    it 'rejects the offers' do
      expect(offer).to receive(:reject)

      approval.reject
    end

    it 'sends the rejection notification' do
      expect(mailer)
        .to receive(:offer_rejection)
        .with(batch)
        .and_return(mail)
      expect(mail).to receive(:deliver_later)

      approval.reject
    end
  end
end
