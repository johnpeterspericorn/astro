# encoding: utf-8
require 'spec_helper'

describe Promise do
  let(:promise_250)           { 500.0  }
  let(:promise_500)           { 1000.0 }
  let(:additional_day)          { 5.0    }
  let(:inspection_bundle_price) { 100.0 }
  let(:vnum) { '19UUA564XXA052974' }

  let(:vehicle_attributes) do
    { vnum: vnum }
  end

  let(:inspection_attributes) do
    { inspection_bundle_price: inspection_bundle_price }
  end

  let(:offer_attributes) do
    {
      promise_250: promise_250,
      promise_500: promise_500,
      additional_day: additional_day,
      vehicle_information: FactoryGirl.build(:vehicle_information, vehicle_attributes),
      inspection_information: InspectionInformation.create!(inspection_attributes)
    }
  end

  let(:promise_offer) { FactoryGirl.create(:promise_offer, offer_attributes) }

  describe '#price' do
    subject { described_class.new(promise_offer) }

    it { expect(subject.price(250, 0)).to eql promise_250 }
    it { expect(subject.price(250, 7)).to eql promise_250 + 35 }
    it { expect(subject.price(500, 0)).to eql promise_500 }
    it { expect(subject.price(500, 14)).to eql promise_500 + 70 }

    context 'when sold vehicle is missing additional_day price' do
      before do
        promise_offer.additional_day = nil
        promise_offer.save!
      end

      specify 'price is nil' do
        expect(subject.price(250, 0)).to be_nil
      end
    end

    context 'when base price is missing' do
      before do
        promise_offer.promise_250 = nil
        promise_offer.save!
      end

      specify 'price is nil' do
        expect(subject.price(250, 0)).to be_nil
      end
    end
  end

  describe '#discounted_price' do
    let(:offer) { double :offer }
    let(:miles) { 250 }
    let(:days) { 0 }
    let(:price_250) { 3 }
    let(:discount) { 1 }

    subject(:promise) { described_class.new(offer) }

    before do
      allow(offer).to receive(:flat_rate_price)
      allow(offer).to receive(:additional_day) { 0 }
      allow(offer).to receive(:promise_250) { price_250 }
      allow(offer).to receive(:promise_500) { 0 }
      allow(offer).to receive(:country) { SupportedCountry::DEFAULT }
    end

    it 'reduces the price by the offer discount' do
      discount = 1

      allow(offer).to receive(:discount_amount) { discount }

      expect(promise.discounted_price(miles, days)).to eq(2)
    end

    it 'does not discount to a negative price' do
      discount = 4

      allow(offer).to receive(:discount_amount) { discount }

      expect(promise.discounted_price(miles, days)).to eq(0)
    end
  end
end
