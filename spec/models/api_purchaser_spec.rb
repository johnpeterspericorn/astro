# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe ApiPurchaser do
  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let (:sold_vehicle) { FactoryGirl.create(:sold_vehicle, vehicle_attributes.merge(location_initials: pawn.initials)) }
  let(:days)  { 7   }
  let(:miles) { 250 }
  let(:agreement_number) { SasDataHelper:: DEFAULT_DEALER_NUMBER }
  let(:price) { 100 }

  before do
    allow_any_instance_of(SoldVehicle).to receive(:available_for_purchase?) { true }
    allow_any_instance_of(Promise).to receive(:price) { price }
    stub_buyer_authorization
  end

  let(:valid_params) do
    { email_address: 'adama@caprica.gov', buyer_agreement_number: agreement_number, vnum: sold_vehicle.vnum, days: days, miles: miles, agreementshield_quoted_price: price }
  end

  describe '#additional_days' do
    subject { described_class.new(valid_params) }

    it 'subtracts the base duration from the total days provided' do
      expect(subject.additional_days).to eql(days - Promise::BASE_DURATION)
    end
  end

  describe 'return code' do
    subject { described_class.new(valid_params) }

    before do
      allow(subject).to receive(:vehicle) { sold_vehicle }
    end

    context 'when the purchase is marked as pending' do
      before do
        sold_vehicle.update_attributes!(pending_reason_code: 1)
      end

      it 'is 1 after purchase' do
        subject.purchase_vehicle
        expect(subject.purchase_response[:return_code]).to eql 1
      end
    end

    context 'when the offer is past cutoff time' do
      before do
        sold_vehicle.update_attributes!(must_be_promised_at: 1.hour.ago)
      end

      it 'is 9' do
        expect(subject.purchase_response[:return_code]).to eql 9
      end
    end

    context 'when the offer has already been purchased' do
      let!(:other_purchaser) { subject.dup }
      it 'is 11' do
        other_purchaser.purchase_vehicle
        subject.purchase_vehicle
        expect(subject.purchase_response[:return_code]).to eql 11
      end
    end

    context 'when the vehicle with the given VNUM is missing' do
      let(:vnum) { 'WRONGVNUM' }
      subject { described_class.new(valid_params.merge(vnum: vnum)) }

      before do
        allow(subject).to receive(:vehicle).and_call_original
      end

      it 'is 12' do
        expect(subject.purchase_response[:return_code]).to eql 12
      end
    end

    context 'when the vehicle with the given VNUM exists, but with a different agreement #' do
      let(:agreement_number) { '-1' }

      subject { described_class.new(valid_params.merge(buyer_agreement_number: agreement_number)) }

      before do
        allow(subject).to receive(:vehicle).and_call_original
      end

      it 'is 13' do
        expect(subject.purchase_response[:return_code]).to eql 13
      end
    end
  end

  describe '#purchase_vehicle' do
    context 'when the purchaser is built with a silent parameter' do
      it 'hits the SilentPromisePurchaser' do
        purchaser = ApiPurchaser.new(valid_params.merge(silent: true))
        allow(purchaser).to receive(:valid?) { true }

        expect_any_instance_of(SilentPromisePurchaser).to receive(:record_purchase)

        purchaser.purchase_vehicle
      end
    end

    describe 'bad requests due to missing information' do
      %w(vnum buyer_agreement_number).each do |parameter|
        it "fails without the parameter #{parameter}" do
          purchaser = ApiPurchaser.new(valid_params.merge(parameter => 'nonexistent'))
          expect(purchaser.purchase_vehicle).to be_nil
        end
      end

      %w(miles days).each do |parameter|
        it "raises an error without the parameter #{parameter}" do
          expect do
            purchaser = ApiPurchaser.new(valid_params.merge(parameter => nil))
            purchaser.purchase_vehicle
          end.to raise_error(ArgumentError)
        end
      end
    end

    describe 'happy purchasepath' do
      it 'should return a status of OK' do
        purchaser = described_class.new(valid_params)
        expect(purchaser.purchase_vehicle).not_to be_nil
      end

      it 'should return some valid data' do
        purchaser = described_class.new(valid_params)
        data = purchaser.purchase_vehicle
        expect(data['transaction_id']).to be_present
      end

      it 'sets offer channel as API' do
        described_class.new(valid_params).purchase_vehicle

        expect(PromisePurchase.first.channel).to eq(PromiseChannel::API)
      end
    end
  end
end
