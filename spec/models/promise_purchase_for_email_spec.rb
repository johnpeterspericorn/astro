# encoding: utf-8
require 'spec_helper'

describe PromisePurchaseForEmail do
  subject { described_class.new(promise_purchase) }
  let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information) }
  let(:pawn_information) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location) }
  let(:pawn_location) { FactoryGirl.create(:pawn_location, initials: 'BAR', name: 'foo') }
  before do
    Timecop.freeze(Time.zone.local(2018, 1, 1, 12, 0, 0))
  end

  it 'displays the attributes' do
    expect(subject.pawn_location).to eq('foo')
    expect(subject.created_at).to eq('01/01/16')
    expect(subject.distance).to eq('250 Miles')
    expect(subject.duration).to eq(7)
    expect(subject.expiration_date).to eq('01/08/16')
    expect(subject.price).to eq('$ 100')
  end

  it 'displays the hash' do
    expect(subject.as_json).to eq('pawn_location' => 'foo',
                                  'created_at' => '01/01/16',
                                  'distance' => '250 Miles',
                                  'duration' => 7,
                                  'expiration_date' => '01/08/16',
                                  'price' => '$ 100')
  end
end
