# encoding: utf-8
require 'spec_helper'

describe ReturnInvoice do
  let(:miles_selected)      { 250 }
  let(:odometer_on_return)  { promise_offer.odometer_reading + miles_selected - 1 }
  let(:pawn_location)    { PawnLocation.create!(initials: 'SVAA', name: 'Statesville') }
  let(:promise_offer)     { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago) }
  let(:promise_purchase)  { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer, miles_selected: 250, additional_days: 7) }
  let(:returning_user)      { User.create!(kiosk_user: true, username: 'kiosk', payment_customer_id: 'foo') }
  let(:valid_params) do
    {
      mechanical_issues: '1',
      cosmetic_issues: '0',
      unable_to_sell: '1',
      changed_mind: '0',
      other: '0',
      no_answer: '0',
      odometer_on_return: odometer_on_return,
      promise_purchase: promise_purchase,
      pawn_location: pawn_location,
      title_status: '2'
    }.with_indifferent_access
  end
  subject(:invoice) do
    invoice = ReturnInvoice.new(valid_params)
    invoice.returning_user = returning_user
    invoice
  end

  before do
    Settings.query_ods = true
  end

  context 'scopes' do
    let(:agreement_no) { '555' }
    let(:seller_no) { '333' }
    let(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: agreement_no, seller_no: seller_no) }
    let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, pawn_information: pawn_information, left_lot: true, seller_paid: true) }
    let(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer) }
    let!(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }

    let(:pawn_information_1) { FactoryGirl.create(:pawn_information, agreement_no: agreement_no) }
    let(:promise_offer_1) { FactoryGirl.create(:day_old_promise_offer, pawn_information: pawn_information_1, left_lot: true, limited_volume: false) }
    let(:promise_purchase_1) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_1) }
    let!(:return_invoice_1) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_1, created_at: 12.days.ago) }

    describe '.for_agreement_number' do
      it 'only returns those invoices for that agreement number' do
        expect(described_class.for_agreement_number(agreement_no).size).to eq(2)
      end

      it 'only returns those invoices within given timeframe and agreement number' do
        expect(described_class.for_agreement_number(agreement_no).valid_and_initiated_at_within_timeframe(15).size).to eq(2)
      end
    end

    describe '.for_seller_number' do
      it 'only returns those invoices for that seller' do
        expect(described_class.for_seller_number(seller_no)).to eq([return_invoice])
      end

      it 'only returns those invoices within given timeframe and seller number' do
        expect(described_class.for_seller_number(seller_no).valid_and_initiated_at_within_timeframe(10).size).to eq(1)
      end
    end

    describe '.valid_and_initiated_at_within_timeframe' do
      it 'returns invoices within given timeframe' do
        expect(described_class.valid_and_initiated_at_within_timeframe(10).size).to eq(1)
        expect(described_class.valid_and_initiated_at_within_timeframe(15).size).to eq(2)
      end
    end

    describe '.for_agreement_number_with_palv_buyer_paid' do
      it 'should give back return vehicles that are palv and buyer paid' do
        ai = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
        dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: ai, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false)
        rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
        FactoryGirl.create(:return_invoice, promise_purchase: rgp)

        expect(described_class.for_agreement_number_with_palv_buyer_paid(agreement_no, SupportedCountry::USA).size).to eq(1)
      end

      it 'should NOT give back ANY return vehicles that are palv and NOT buyer paid' do
        ai = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
        dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: ai, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: true)
        rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
        FactoryGirl.create(:return_invoice, promise_purchase: rgp)

        expect(described_class.for_agreement_number_with_palv_buyer_paid(agreement_no, SupportedCountry::USA).size).to eq(0)
      end

      it 'should NOT give back ANY return vehicles that are NOT palv but buyer paid' do
        ai = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
        dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: ai, left_lot: true, limited_volume: false, purchased_at: 8.days.ago, seller_paid: false)
        rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
        FactoryGirl.create(:return_invoice, promise_purchase: rgp)

        expect(described_class.for_agreement_number_with_palv_buyer_paid(agreement_no, SupportedCountry::USA).size).to eq(0)
      end

      it 'should return only Canadian palv buyer paid returns' do
        agreement_no = '5241413'
        pawn_location_a = FactoryGirl.create(:pawn_location, initials: 'CAC', name: 'Canada', country: 'Canada', partner_network: 'CA-Astroheim')
        FactoryGirl.create(:agreement_information, agreement_no: agreement_no)
        promise_offer_one = FactoryGirl.create(:promise_offer, limited_volume: '3', seller_paid: false, purchased_at: 10.days.ago)
        FactoryGirl.create(:pawn_information, pawn_location: pawn_location_a, agreement_no: agreement_no, vehicle_purchase_price: 15_000, promise_offer: promise_offer_one)
        promise_purchase_one = FactoryGirl.create(:promise_purchase, promise_offer: promise_offer_one, promised_at: 10.days.ago)
        FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_one, pawn_location: pawn_location_a, created_at: 1.minute.ago)

        expect(described_class.for_agreement_number_with_palv_buyer_paid(agreement_no, SupportedCountry::Canada).size).to eq(1)
      end
    end

    it 'will save expiration_date in return invoice table' do
      expect(return_invoice.expiration_date).to eq promise_purchase.final_window_expiration_date.to_date
    end
  end

  describe 'custom validations' do
    describe '#must_be_within_miles_range' do
      it 'should be invalid if the odometer is below the minimum' do
        invoice.update_attributes(odometer_on_return: promise_purchase.promise_offer.odometer_reading - 1)
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end

      it 'should be invalid if the odometer is above the maximum' do
        invoice.update_attributes(valid_params.merge(odometer_on_return: odometer_on_return + 100))
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end
    end

    describe '#vehicle_must_be_eligible' do
      it 'should be invalid if the vehicle is not eligible for return' do
        allow(promise_purchase).to receive(:eligible_for_return?).and_return(false)
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end
    end

    describe '#some_reason_is_given' do
      it 'should be invalid without a return reason' do
        invoice.update_attributes(valid_params.merge('mechanical_issues' => '0', 'unable_to_sell' => '0'))
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end
    end

    describe '#title_status_must_be_valid' do
      it 'should be invalid if the title status is not included' do
        invoice.update_attributes(valid_params.merge('title_status' => nil))
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end

      it 'should be invalid if a title status is not in the system' do
        invalid_status = I18n.t('return_invoice.title_statuses').keys.max + 1
        invoice.update_attributes(valid_params.merge('title_status' => invalid_status.to_s))
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end
    end

    describe '#returning_user_is_valid' do
      let(:invalid_user_id) { (User.pluck(:id).max || 0) + 1 }

      it 'should be invalid without a returning user set' do
        invoice.update_attributes(returning_user_id: nil)
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end

      it 'should be invalid for user with no instance in the database' do
        invoice.update_attributes(returning_user_id: invalid_user_id)
        expect(invoice).to be_invalid
        expect(invoice.errors[:base]).not_to be_empty
      end
    end

    it 'should be valid' do
      expect(invoice).to be_valid
    end
  end

  describe '#returning_user' do
    it 'should be a KioskUser' do
      expect(invoice.returning_user).to eql returning_user
    end
  end

  describe '.missing_data?' do
    it 'returns true if odometer_on_return is not present' do
      subject.odometer_on_return = nil
      expect(subject.missing_data?).to be_truthy
    end

    it 'returns true if odometer is above maximum' do
      subject.odometer_on_return = promise_offer.odometer_reading + miles_selected + 1
      expect(subject.missing_data?).to be_truthy
    end
  end

  describe '#shipment_information' do
    context 'when agreement pays for the vehicle' do
      it 'should have necessary fields' do
        expect(invoice.shipment_information).to be_a(AgreementInformation)
        expect(invoice.shipment_information).to respond_to(:to_address)
        expect(invoice.shipment_information).to respond_to(:phone)
        expect(invoice.shipment_information).to respond_to(:name)
        expect(invoice.shipment_information).to respond_to(:company)
      end
    end

    context 'when floor plan pays for the vehicle' do
      let(:floor_plan_branch) { FactoryGirl.create(:floor_plan_branch) }
      let(:floor_plan_branch_selection) do
        FactoryGirl.create(:floor_plan_branch_selection,
                           company_code: floor_plan_branch.company_code,
                           site_no: floor_plan_branch.site_no)
      end

      it 'should have necessary fields' do
        floor_plan_branch_selection.update_attributes(return_invoice_id: invoice.id)
        invoice.floor_plan_branch_selection = floor_plan_branch_selection

        expect(invoice.shipment_information).to be_a(FloorPlanBranchShipmentInfo)
        expect(invoice.shipment_information).to respond_to(:to_address)
        expect(invoice.shipment_information).to respond_to(:phone)
        expect(invoice.shipment_information).to respond_to(:name)
        expect(invoice.shipment_information).to respond_to(:company)
      end
    end
  end

  describe '#finalize' do
    it 'sends an left the lot notification' do
      subject.save!
      expect(ShipmentLabelJob).to receive(:enqueue).with(return_invoice: subject)
      expect do
        subject.finalize
      end.to change { enqueued_deliveries.count }.by(2)
    end

    describe 'create shipment' do
      let(:floor_plan_company) { FactoryGirl.create(:floor_plan_company, generate_shipping_label: false) }
      let(:floor_plan_branch) { FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company) }
      let!(:floor_plan_branch_selection) do
        FactoryGirl.create(:floor_plan_branch_selection,
                           company_code: floor_plan_branch.company_code,
                           site_no: floor_plan_branch.site_no)
      end

      it 'doesnot create shipment if generate labels is false for floor plan' do
        invoice.save
        floor_plan_branch_selection.update(return_invoice_id: invoice.id)

        expect(ShipmentLabelJob).not_to receive(:enqueue).with(return_invoice: subject)
        subject.finalize
      end
    end
  end
end
