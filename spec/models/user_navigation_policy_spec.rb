# encoding: utf-8
require 'spec_helper'

describe UserNavigationPolicy do
  describe '#authorized_to_purchase?' do
    let(:user) { double('user', buyer_user?: true, super_user?: false) }

    subject(:policy) { described_class.new(user) }

    it 'should be true for non-Astroheim Users' do
      allow(user).to receive(:buyer_user?).and_return(false)

      expect(policy.authorized_to_purchase?).to be_truthy
    end

    it 'should be false for Astroheim Users who are not authorized to purchase' do
      allow(user).to receive(:authorized_to_purchase?).and_return(false)

      expect(policy.authorized_to_purchase?).to be_falsey
    end

    it 'should be true for Astroheim Users who are authorized to purchase' do
      allow(user).to receive(:authorized_to_purchase?).and_return(true)

      expect(policy.authorized_to_purchase?).to be_truthy
    end

    it 'should be true for Super Users who are also Buyer Users' do
      allow(user).to receive(:super_user?).and_return(true)

      expect(policy.authorized_to_purchase?).to be_truthy
    end
  end
end
