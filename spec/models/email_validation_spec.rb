# encoding: utf-8
require 'spec_helper'

describe EmailValidation do
  def validation(email_address = nil, options = {})
    described_class.new(email_address, options)
  end

  describe '.valid?' do
    it 'is valid for test@agreement.com' do
      expect(described_class.valid?('test@agreement.com')).to be_truthy
    end

    it 'is not valid for test@agreement' do
      expect(described_class.valid?('test@agreement')).to be_falsy
    end
  end
  it 'is valid for typical email' do
    expect(validation('foo@bar.com')).to be_valid
  end

  it 'is valid for an email address with hypens, plus signs, and dots' do
    expect(validation('foo-by.loo-by+bar@baz-zy.fiz.buzz')).to be_valid
  end

  it 'is not valid if a comma is accidentally used' do
    expect(validation('foo@bar,com')).to_not be_valid
  end

  it 'is valid for multiple email addresses' do
    expect(validation(['foo@bar.com', 'bar@foo.com'])).to be_valid
  end

  describe 'presence' do
    it 'does not require email address presence by default' do
      expect(validation).to be_valid
    end

    it 'may require email address presence' do
      expect(validation('', required: true)).to_not be_valid
    end
  end

  describe '#required?' do
    it 'is not required by default' do
      expect(validation).to_not be_required
    end

    it 'may be required' do
      expect(validation('', required: true)).to be_required
    end
  end
end
