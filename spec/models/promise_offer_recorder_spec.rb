# encoding: utf-8
require 'spec_helper'

describe PromiseOfferRecorder do
  describe '#record_offer' do
    let(:vehicle) { double SoldVehicle }
    let(:replicator) { double ReplicateSoldVehicle }

    context 'previous offer exists' do
      let(:offer) { double PromiseOffer }

      subject(:recorder) { described_class.new(vehicle, [offer], replicator) }

      before do
        allow(offer).to receive(:promise_purchase)
      end

      context 'vehicle has been updated since previous offer' do
        before do
          allow(offer).to receive(:destroy)
          allow(offer).to receive(:updated_at) { 1.minute.ago }
          allow(vehicle).to receive(:updated_at) { Time.current }
          allow(replicator).to receive(:call)
        end

        it 'destroys previous offer' do
          expect(offer).to receive(:destroy)

          recorder.record_offer
        end

        it 'returns new replicated offer' do
          new_offer = double PromiseOffer
          allow(replicator).to receive(:call) { new_offer }

          expect(recorder.record_offer).to eq(new_offer)
        end

        context 'offer has been purchased' do
          let(:purchase) { double PromisePurchase }

          before do
            allow(offer).to receive(:promise_purchase) { purchase }
          end

          it 'returns purchased offer' do
            expect(recorder.record_offer).to eq(offer)
          end
        end
      end

      context 'previous offer is still current' do
        before do
          now = Time.current
          allow(offer).to receive(:updated_at) { now }
          allow(vehicle).to receive(:updated_at) { now }
        end

        it 'returns current offer' do
          expect(recorder.record_offer).to eq(offer)
        end
      end
    end

    context 'no offers exist' do
      subject(:recorder) { described_class.new(vehicle, [], replicator) }

      it 'returns newly replicated offer' do
        new_offer = double PromiseOffer
        allow(replicator).to receive(:call) { new_offer }

        expect(recorder.record_offer).to eq(new_offer)
      end
    end
  end
end
