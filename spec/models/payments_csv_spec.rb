# encoding: utf-8
require 'spec_helper'

describe PaymentsCsv do
  describe '#to_s' do
    let!(:pawn_information) { FactoryGirl.create(:pawn_information) }
    let(:payment) { FactoryGirl.create(:payment) }
    let(:decorated_payment) { subject.decorated_payments.first }
    let(:payments) { [payment] }

    subject(:csv) { described_class.new(payments) }

    it 'includes header line' do
      csv_lines = csv.to_s.split("\n")
      expect(csv_lines.count).to eq(2)
    end

    it 'should report order_id' do
      expect(csv.to_s).to include(decorated_payment.order_id.to_s)
    end

    it 'should report authorization_code' do
      expect(csv.to_s).to include(decorated_payment.authorization_code.to_s)
    end

    it 'should report promise_price' do
      expect(csv.to_s).to include(decorated_payment.promise_price.to_s)
    end

    it 'should report location_initials' do
      expect(csv.to_s).to include(decorated_payment.location_initials.to_s)
    end

    it 'should report created_at' do
      expect(csv.to_s).to include(decorated_payment.created_at.to_s)
    end

    it 'should report vnum' do
      expect(csv.to_s).to include(decorated_payment.vnum.to_s)
    end

    it 'should report agreement_no' do
      expect(csv.to_s).to include(decorated_payment.agreement_no.to_s)
    end
  end
end
