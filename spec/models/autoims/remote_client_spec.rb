# encoding: utf-8
require 'spec_helper'
# VCR cassettes needs to be re-recorded which is pending as we are yet to receive
# the test environment settings story#115249465

# Manual setup required to record VCR cassettes for this test. More information
# in https://github.com/Astroheim/astro/blob/master/doc/autoims_integration_tests.md

describe Autoims::RemoteClient do
  let(:return_invoice) { double('ReturnInvoice') }
  let(:promise_offer) { double('PromiseOffer') }
  let(:purchase_location) { 'ALOH' }
  let(:return_location) { 'ALOH' }
  let(:vehicle_purchase_price) { 12_345.0 }
  let(:purchased_at) { Time.new(2018, 4, 25) }
  let(:returned_at) { Time.new(2018, 1, 5) }
  let(:return_expiration_date) { Time.new(2018, 1, 15) }
  let(:work_order_number) { '22446688' }
  let(:vnum) { '5Y2SL67089Z43TEST' }
  let(:additional_information) { 'Some additional information' }

  subject { Autoims::RemoteClient.new(return_invoice) }

  before do
    allow(return_invoice).to receive(:id).and_return(42)
    allow(return_invoice).to receive(:created_at).and_return(returned_at)
    allow(return_invoice).to receive(:promise_offer).and_return(promise_offer)
    allow(return_invoice).to receive(:pawn_location).and_return(double(initials: return_location))
    allow(return_invoice).to receive(:expiration_date).and_return(return_expiration_date)
    allow(return_invoice).to receive(:additional_information).and_return(additional_information)

    allow(promise_offer).to receive(:vnum).and_return(vnum)
    allow(promise_offer).to receive(:kind).and_return('purchase_protect')
    allow(promise_offer).to receive(:seller_paid?).and_return(true)
    allow(promise_offer).to receive(:location_initials).and_return(purchase_location)
    allow(promise_offer).to receive(:vehicle_purchase_price).and_return(vehicle_purchase_price)
    allow(promise_offer).to receive(:purchased_at).and_return(purchased_at)
    allow(promise_offer).to receive(:work_order_number).and_return(work_order_number)
  end

  describe 'drop notice creation' do
    vcr_opts = {
      match_requests_on: [:method, :uri, :body]
    }

    it 'succeeds', vcr: vcr_opts.merge(cassette_name: 'autoims/create_success') do
      expect do
        described_class.create_drop_notice(return_invoice)
      end.to_not raise_error
    end

    it 'fails', vcr: vcr_opts.merge(cassette_name: 'autoims/create_failure') do
      return_location = 'FAKE-FAKE'
      allow(return_invoice).to receive(:pawn_location).and_return(double(initials: return_location))
      expect do
        described_class.create_drop_notice(return_invoice)
      end.to raise_error(RuntimeError)
    end
  end

  describe 'drop notice deletion' do
    vcr_opts = {
      match_requests_on: [:method, :uri, :body]
    }

    it 'succeeds', vcr: vcr_opts.merge(cassette_name: 'autoims/delete_success') do
      expect do
        described_class.delete_drop_notice(return_invoice)
      end.to_not raise_error
    end

    it 'fails', vcr: vcr_opts.merge(cassette_name: 'autoims/delete_failure') do
      expect do
        described_class.delete_drop_notice(return_invoice)
      end.to raise_error(RuntimeError)
    end
  end
end
