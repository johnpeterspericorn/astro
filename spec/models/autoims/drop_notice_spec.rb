# encoding: utf-8
require 'spec_helper'

describe Autoims::DropNotice do
  let(:return_invoice) { double('ReturnInvoice') }
  let(:promise_offer) { double('PromiseOffer') }
  let(:purchase_location) { 'PURC' }
  let(:return_location) { 'RETU' }
  let(:vehicle_purchase_price) { 12_345.0 }
  let(:purchased_at) { Time.new(2018, 4, 25) }
  let(:returned_at) { Time.new(2018, 1, 5) }
  let(:return_expiration_date) { Time.new(2018, 1, 15) }
  let(:work_order_number) { '22446688' }
  let(:vnum) { SecureRandom.hex }
  let(:additional_information) { 'Some additional information' }

  subject { described_class.new(return_invoice) }

  before do
    allow(return_invoice).to receive(:id).and_return(42)
    allow(return_invoice).to receive(:created_at).and_return(returned_at)
    allow(return_invoice).to receive(:promise_offer).and_return(promise_offer)
    allow(return_invoice).to receive(:pawn_location).and_return(double(initials: return_location))
    allow(return_invoice).to receive(:expiration_date).and_return(return_expiration_date)
    allow(return_invoice).to receive(:additional_information).and_return(additional_information)

    allow(promise_offer).to receive(:vnum).and_return(vnum)
    allow(promise_offer).to receive(:kind).and_return('purchase_protect')
    allow(promise_offer).to receive(:seller_paid?).and_return(true)
    allow(promise_offer).to receive(:location_initials).and_return(purchase_location)
    allow(promise_offer).to receive(:vehicle_purchase_price).and_return(vehicle_purchase_price)
    allow(promise_offer).to receive(:purchased_at).and_return(purchased_at)
    allow(promise_offer).to receive(:work_order_number).and_return(work_order_number)
  end

  describe '#body_xml' do
    let(:xml) { Nokogiri::XML(subject.body_xml) }

    it 'includes the client account number' do
      expect(xml.css('Vehicle ClientAccountNumber').text).to eq('42.12345')
    end

    it 'includes the vehicle VNUM' do
      expect(xml.css('Vehicle VNUM').text).to eq(vnum)
    end

    it 'includes the invoice ID as `ClientField1`' do
      expect(xml.css('Vehicle ClientField1').text).to eq(return_invoice.id.to_s)
    end

    it 'includes the purchase location initials' do
      expect(xml.css('Vehicle ClientField2').text).to eq(purchase_location)
    end

    it 'includes the promise purchase kind and payer as `ClientField3`' do
      expect(xml.css('Vehicle ClientField3').text).to eq('purchase_protect seller')
    end

    it 'includes the return additional information  as `ClientField4`' do
      expect(xml.css('Vehicle ClientField4').text).to eq(additional_information)
    end

    it 'includes the return location initials' do
      expect(xml.css('Vehicle PawnAssignment PawnID').text).to eq(return_location)
    end

    it 'includes the vehicle purchase price' do
      expect(xml.css('Vehicle PrivateBookValue').text.to_f).to be_within(0.001).of(vehicle_purchase_price)
    end

    it 'sets DecodeVNUM to true' do
      expect(xml.css('Vehicle DecodeVNUM').text).to eq('true')
    end

    it "sets PrivateDate1 to the vehicle's purchase date in Y-m-d format" do
      expect(xml.css('PrivateDate1').text).to eql('2018-04-25')
    end

    it 'sets PrivateDate2 to the return expiration date in Y-m-d format' do
      expect(xml.css('PrivateDate2').text).to eql('2018-01-15')
    end

    it 'sets PrivateCode2 to the return location initials' do
      expect(xml.css('PrivateCode2').text).to eql(return_location)
    end

    it 'sets PickupOrDropOffNote to the proper value' do
      expect(xml.css('PickupOrDropOffNote').text).to eql("AgreementShield Return Vehicle OrigWO: $#{purchase_location}$ ##{work_order_number}#")
    end

    it 'sets PickupOrDropOffNote to the proper value' do
      expect(xml.css('Note NoteText').text).to eql("AgreementShield Return Vehicle OrigWO: $#{purchase_location}$ ##{work_order_number}#")
    end

    it "sets DropOffDate to the date of the vehicle's return" do
      expect(xml.css('DropOffDate').text).to eql('2018-01-05')
    end
  end
end
