# encoding: utf-8
require 'spec_helper'

describe AccountsReceivableCsv do
  describe 'Sanity check' do
    let(:promise_offer) { AccountsReceivableOfferDecorator.decorate(FactoryGirl.create(:promise_offer)) }

    let(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
    let(:cancelled_purchase) { CancelledPurchase.create_from_purchase!('test', promise_purchase, nil) }
    let(:decorated_cancelled_purchase) { AccountsReceivableCancelledPurchaseDecorator.decorate(cancelled_purchase) }

    subject { described_class.new([promise_offer, decorated_cancelled_purchase]) }

    it 'yields a CSV string' do
      expect(subject.to_s).to be_present
    end
  end
end
