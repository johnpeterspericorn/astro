# encoding: utf-8
require 'spec_helper'

describe PromiseSelectionRecorder do
  let(:sold_vehicle) { FactoryGirl.build(:sold_vehicle, vehicle_attributes) }

  describe '.record_selected' do
    let(:distance) { 250 }
    let(:additional_days) { 7 }
    let(:promise) { Promise.new(sold_vehicle).decorate }
    let(:promise_purchase) { PromisePurchase.from_options(sold_vehicle.vnum, distance, additional_days).decorate }

    before do
      PromiseOfferRecorder.record_offers [sold_vehicle]
      described_class.record_selected [promise_purchase]
    end
    subject { PromiseOffer.by_vnum(sold_vehicle.vnum).first }

    context 'initial selection' do
      it { is_expected.to be_promised }

      describe '#miles_selected' do
        subject { super().miles_selected }
        it { is_expected.to eql promise_purchase.miles_selected }
      end

      describe '#days_selected' do
        subject { super().days_selected }
        it { is_expected.to eql additional_days + Promise::BASE_DURATION }
      end

      describe '#promise_price' do
        subject { super().promise_price }
        it { is_expected.to eql promise_purchase.model.promise_price }
      end

      describe '#emails' do
        subject { super().emails }
        it { is_expected.to be nil }
      end

      describe '#promise_purchase' do
        subject { super().promise_purchase }
        it { is_expected.to eql promise_purchase.model }
      end
      it { expect(subject.promised_at).to be_present }
    end

    context 'changed promise parameters on second selection' do
      let(:new_distance)        { 500 }
      let(:new_additional_days) { 14  }
      let(:new_promise_purchase) { PromisePurchase.from_options(sold_vehicle.vnum, new_distance, new_additional_days).decorate }
      before do
        described_class.record_selected [new_promise_purchase]
      end

      it { is_expected.to be_promised }

      describe '#miles_selected' do
        subject { super().miles_selected }
        it { is_expected.to eql new_promise_purchase.miles_selected }
      end

      describe '#days_selected' do
        subject { super().days_selected }
        it { is_expected.to eql new_additional_days + Promise::BASE_DURATION }
      end

      describe '#promise_price' do
        subject { super().promise_price }
        it { is_expected.to eql new_promise_purchase.model.promise_price }
      end

      describe '#promise_purchase' do
        subject { super().promise_purchase }
        it { is_expected.to eql new_promise_purchase.model }
      end
      it { expect(subject.updated_at).to be > subject.promised_at }
    end

    context 'did not change promise parameters on second selection' do
      let(:new_promise_purchase) { PromisePurchase.from_options(sold_vehicle.vnum, distance, additional_days).decorate }
      before do
        described_class.record_selected [new_promise_purchase]
      end

      it { is_expected.to be_promised }

      describe '#miles_selected' do
        subject { super().miles_selected }
        it { is_expected.to eql distance }
      end

      describe '#days_selected' do
        subject { super().days_selected }
        it { is_expected.to eql additional_days + Promise::BASE_DURATION }
      end

      describe '#promise_purchase' do
        subject { super().promise_purchase }
        it { is_expected.to eql new_promise_purchase.model }
      end
      it { expect(subject.updated_at).to eq(subject.promised_at) }
    end

    context 'with an email address' do
      let(:email) { 'foo@bar.com' }
      before do
        described_class.record_selected [promise_purchase], email
      end
      subject { PromiseOffer.first }

      describe '#email' do
        subject { super().emails }
        it { is_expected.to eq email }
      end
    end

    context 'dashboard updates' do
      it 'should attempt to push an event to the dashboard' do
        expect_any_instance_of(SelectionPusher).to receive(:push_to_dashboard).with(kind_of(String))
        described_class.record_selected [promise_purchase]
      end
    end
  end

  describe '#validate_recorded_offer' do
    it 'should mail for promised offers without a timestamp' do
      promise_offer = double(:promise_offer, promised?: true, promised_at: nil)
      allow(promise_offer).to receive(:reload).and_return(promise_offer)

      expect(InfrastructureMailer).to receive(:bad_offer).with(promise_offer, anything).and_return(double(deliver_later: nil))

      described_class.new(nil, nil).send(:validate_recorded_offer, promise_offer, nil)
    end

    it 'should mail for promised offers without a purchase' do
      promise_offer = double(:promise_offer, promised?: true, promised_at: Time.current, promise_purchase: nil)
      allow(promise_offer).to receive(:reload).and_return(promise_offer)

      expect(InfrastructureMailer).to receive(:bad_offer).with(promise_offer, anything).and_return(double(deliver_later: nil))

      described_class.new(nil, nil).send(:validate_recorded_offer, promise_offer, nil)
    end
  end

  describe '#validate_recorded_purchase' do
    it 'should mail for a promise purchase without a offer' do
      promise_purchase = double(:promise_purchase, promise_offer: nil)
      allow_any_instance_of(described_class).to receive(:validate_recorded_offer)

      expect(InfrastructureMailer).to receive(:bad_purchase).with(anything, promise_purchase).and_return(double(deliver_later: nil))

      described_class.new(nil, nil).send(:validate_recorded_purchase, nil, promise_purchase)
    end
  end
end
