# encoding: utf-8
require 'spec_helper'

describe PromisePurchaseBuilder do
  let(:user) { double :user }
  let(:purchase_query) { double :purchase_query }
  let(:purchase) { double :purchase }
  let(:purchase_factory) { double :factory }

  before do
    allow(purchase_query).to receive(:vnum)
    allow(purchase_query).to receive(:miles)
    allow(purchase_query).to receive(:days)
    allow(purchase_factory).to receive(:from_options)
      .and_return(purchase)
  end

  subject(:builder) { described_class.new(user, [purchase_query], purchase_factory) }

  describe '#promise_purchases' do
    it 'builds purchases for given queries' do
      expect(builder.promise_purchases).to eq([purchase])
    end
  end

  describe '#changed_purchases_without_existing_inspection' do
    it 'filters built purchases to those that have changed and do not have an inspection bundled' do
      allow(purchase).to receive(:changed?) { true }
      allow(purchase).to receive(:has_inspection?)

      expect(builder.changed_purchases_without_existing_inspection).to eq([purchase])
    end
  end

  describe '#unbundled_promise_purchases' do
    it 'filters built purchases to those that do not have an inspection bundled' do
      allow(purchase_query).to receive(:inspection_selected?)

      expect(builder.unbundled_promise_purchases).to eq([purchase])
    end
  end
end
