# encoding: utf-8
require 'spec_helper'

describe SalesData do
  let(:agreement) { double }
  let(:sale) { double :sale, agreement_no: agreement }

  describe '#count' do
    it 'returns the number of sales' do
      data = described_class.new(sale)

      expect(data.count).to eq(1)
    end
  end

  describe '#agreement_count' do
    it 'returns the number of unique agreements for the sales' do
      another_sale = double :sale, agreement_no: agreement
      data = described_class.new([sale, another_sale])

      expect(data.agreement_count).to eq(1)
    end
  end
end
