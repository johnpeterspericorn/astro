# encoding: utf-8
require 'spec_helper'

describe NewProductKind do
  let(:offer) { FactoryGirl.create(:promise_offer) }
  let(:purchase_date) { Date.new(2018, 4, 1) }
  let(:purchase) { FactoryGirl.create(:promise_purchase, promised_at: purchase_date, promise_offer: offer, additional_days: 0) }

  subject { described_class.new(purchase) }
  before do
    Timecop.freeze(purchase_date)
    allow_any_instance_of(PromisePurchase).to receive(:program360?).and_return(true)
  end

  describe 'promise extension' do
    before do
      purchase.update(extension_days: 2)
    end

    it 'extends max_return_time by additional days of promise extension' do
      expected_end_date = Date.new(2018, 4, 22).end_of_day + 2.days

      expect(subject.max_return_time).to eql expected_end_date
    end

    describe '#receive_by_date extension' do
      it 'if returned before the promise_expiration_date will be the promise_expiration_date extended by additional days' do
        promise_expiration_date = Date.new(2018, 4, 22).end_of_day + 2.days
        expect(subject.receive_by_date).to eql promise_expiration_date
      end
    end
  end

  context 'with autopurchase and seller paid' do
    describe '#min_return_time' do
      it 'is on day 8, day 0 being purchase date' do
        expected_start_date = Date.new(2018, 4, 9).beginning_of_day
        expect(subject.min_return_time).to eql expected_start_date
      end
    end

    describe '#max_return_time' do
      it 'ends at the end of day on day 21' do
        expected_end_date = Date.new(2018, 4, 22).end_of_day

        expect(subject.max_return_time).to eql expected_end_date
      end
    end

    describe '#promise_expiration_date' do
      it 'ends at the end of day on day 21' do
        promise_expiration_date = Date.new(2018, 4, 22).end_of_day

        expect(subject.promise_expiration_date).to eql promise_expiration_date
      end
    end

    describe '#return_expiration_date' do
      it 'promise_expiration_date + 3 days if automatic purchase and not seller paid' do
        promise_expiration_date = Date.new(2018, 4, 22).end_of_day
        expected_return_expiry = promise_expiration_date + 3.days

        expect(subject.return_expiration_date).to eql expected_return_expiry
      end
    end

    describe '#receive_by_date' do
      it 'if returned before the promise_expiration_date will be the promise_expiration_date' do
        promise_expiration_date = Date.new(2018, 4, 22).end_of_day
        expect(subject.receive_by_date).to eql promise_expiration_date
        Timecop.travel(NewProductKind::TWENTY_ONE_DAYS + NewProductKind::EXTRA_DAY)
        expect(subject.receive_by_date).to eql promise_expiration_date + NewProductKind::EXTRA_DAY
      end
    end

    describe '#expiration_date' do
      it 'is the maximum of receive_by_date and return_expiration_date' do
        promise_expiration_date = Date.new(2018, 4, 22).end_of_day
        return_expiry = promise_expiration_date + 3.days

        expect(subject.expiration_date).to eql return_expiry
      end
    end
  end
end
