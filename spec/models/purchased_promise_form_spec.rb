# encoding: utf-8
require 'spec_helper'

describe PurchasedPromiseForm do
  describe '#update' do
    let(:vehicle_information) { double 'VehicleInformation' }
    let(:promise_offer)     { double 'PromiseOffer'     }
    let(:promise_purchase)  { double 'PromisePurchase'  }
    let(:pawn_information) { double 'PawnInformation' }

    before do
      allow(promise_purchase).to receive(:promise_offer) { promise_offer }
      allow(promise_offer).to receive(:vehicle_information) { vehicle_information }
      allow(promise_offer).to receive(:pawn_information) { pawn_information }
      allow(promise_offer).to receive(:assign_attributes)
      allow(promise_offer).to receive(:emails)
      allow(promise_purchase).to receive(:assign_attributes)
      allow(vehicle_information).to receive(:assign_attributes)
      allow(pawn_information).to receive(:assign_attributes)
      allow(promise_purchase).to receive(:send_confirmation)
    end

    subject(:promise_info_form) { described_class.new(promise_purchase) }

    context 'when valid' do
      before do
        allow(promise_purchase).to receive(:valid?) { true }
        allow(promise_offer).to receive(:valid?) { true }
        allow(promise_offer).to receive(:pending?) { false }
        allow(vehicle_information).to receive(:valid?) { true }
        allow(pawn_information).to receive(:valid?) { true }
      end

      it 'updates Promise Purchase Form Information' do
        attributes = { make: double, promised_at: double, agreement_no: double }

        expect(promise_offer).to receive(:save!)
        expect(promise_purchase).to receive(:save!)
        expect(vehicle_information).to receive(:save!)
        expect(pawn_information).to receive(:save!)

        promise_info_form.update(attributes)
      end
    end

    context 'when invalid' do
      before do
        allow(promise_purchase).to receive(:valid?) { false }
        allow(vehicle_information).to receive(:valid?) { false }
      end

      it 'does not update Promise Purchase Form Information' do
        attributes = { promised_at: double }

        expect(promise_purchase).not_to receive(:save!)
        expect(promise_offer).not_to receive(:save!)
        expect(vehicle_information).not_to receive(:save!)
        expect(pawn_information).not_to receive(:save!)

        promise_info_form.update(attributes)
      end
    end

    describe 'purchased promise form update' do
      before do
        allow(promise_purchase).to receive(:valid?) { true }
        allow(promise_offer).to receive(:valid?) { true }
        allow(promise_offer).to receive(:pending?) { true }
        allow(vehicle_information).to receive(:valid?) { true }
        allow(pawn_information).to receive(:valid?) { true }
      end

      it 'returns true on successful update ' do
        allow(promise_purchase).to receive(:save!)
        allow(promise_offer).to receive(:save!)
        allow(vehicle_information).to receive(:save!)
        allow(pawn_information).to receive(:save!)
        allow(promise_purchase).to receive(:send_confirmation)

        expect(promise_info_form.update({})).to eq true
      end

      it 'returns false on failed update' do
        errors = double 'Errors'
        allow(errors).to receive(:exception).and_raise(ActiveRecord::StatementInvalid)
        allow(promise_offer).to receive(:save!).and_raise(errors)

        expect(promise_info_form.update).to eq false
      end
    end
  end
end
