# encoding: utf-8
require 'spec_helper'

describe FtbLocation do
  it 'return ftb location as array' do
    expect(FtbLocation.all.class).to be Array
  end

  it 'returns ftb locations' do
    expect(FtbLocation.all).to include 'Southern Auto Pawn'
  end
end
