# encoding: utf-8
require 'spec_helper'

describe InspectionPurchase do
  describe 'scopes' do
    let (:unbundled_promise_offer)    { PromiseOffer.create!     }
    let (:unbundled_transaction_id)     { 'unbundled_transaction_id' }
    let!(:unbundled_promise_purchase) { unbundled_promise_offer.create_inspection_purchase!(transaction_id: unbundled_transaction_id) }

    let(:bundled_promise_offer) { PromiseOffer.create!   }
    let(:bundled_transaction_id)  { 'bundled_transaction_id' }
    let!(:inspection_purchase)        { bundled_promise_offer.create_inspection_purchase!(transaction_id: bundled_transaction_id) }
    let!(:bundled_promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: bundled_promise_offer, transaction_id: bundled_transaction_id) }

    describe '.unbundled_for_transaction_id' do
      specify { expect(described_class.unbundled_for_transaction_id(unbundled_transaction_id).count).to eq(1) }
      specify { expect(described_class.unbundled_for_transaction_id(unbundled_transaction_id).first.id).to eq(unbundled_promise_purchase.id) }
    end
  end
end
