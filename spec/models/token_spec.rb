# encoding: utf-8
require 'requests_helper'
require 'ods/configuration'

describe Token do
  let(:endpoint) { Ods::Configuration.endpoint }
  let(:username) { Ods::Configuration.username }
  let(:password) { Ods::Configuration.password }
  let(:token) { Token.new }

  it 'will generate token', vcr: { cassette_name: 'ods/successful_token_request', allow_playback_repeats: true } do
    expect(token.request_token(endpoint, username, password)).not_to be_empty
  end
end
