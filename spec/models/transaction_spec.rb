# encoding: utf-8
require 'spec_helper'

describe Transaction do
  before :each do
    Timecop.freeze(Time.local(2018, 1, 1, 12, 0, 0))
  end

  let!(:promise_purchase) { FactoryGirl.create :promise_purchase, promise_offer: promise_offer }
  let(:promise_offer) { FactoryGirl.create :promise_offer, pawn_information: pawn_information, vehicle_information: vehicle_information, days_selected: 7 }
  let(:pawn_information) { FactoryGirl.create :pawn_information, agreement_no: 5_123_456 }
  let(:vehicle_information) do
    FactoryGirl.create :vehicle_information,
                       make: vehicle_data.make,
                       model: vehicle_data.model,
                       vnum: vehicle_data.vnum,
                       year: vehicle_data.year
  end
  let(:vehicle_data) { Vinbot::Vehicle.new }
  let(:agreement_number) { 5_123_456 }
  let(:attributes) { { agreement_number: agreement_number, days: 90, offset: 0, limit: 10, active: false } }

  describe '.by_agreement_number' do
    context 'when there are 1 transaction' do
      it 'returns one record for the matching agreement number' do
        transactions = described_class.by_agreement_number(attributes)
        expect(transactions.count).to eq(1)
        transaction = transactions.first

        expect(transaction.promise_purchase_id).to eq(promise_purchase.id)
        expect(transaction.location_name).to eq('AAA')
        expect(transaction.location_initials).to eq('AAAA')
        expect(transaction.year).to eq(vehicle_data.year)
        expect(transaction.model).to eq(vehicle_data.model)
        expect(transaction.make).to eq(vehicle_data.make)
        expect(transaction.vnum).to eq(vehicle_data.vnum)
        expect(transaction.odometer).to eq(vehicle_information.odometer_reading)
        expect(transaction.days_left).to eq(8)
        expect(transaction.product_name).to eq('Purchase Advantage')
        expect(transaction.expires_at).to eq('Fri, 08 Jan 2018 12:00:00 +0000')
        expect(transaction.purchased_at).to eq('Fri, 01 Jan 2018 12:00:00 +0000')

        Timecop.travel(Time.local(2018, 1, 19, 12, 0, 0))
        transactions = described_class.by_agreement_number(attributes)
        transaction = transactions.first
        expect(transaction.days_left).to eq(0)
      end

      it 'returns no records when the transaction is outside the days requested' do
        Timecop.travel(Time.local(2018, 4, 1, 12, 0, 0))
        transactions = described_class.by_agreement_number(attributes)
        expect(transactions.count).to eq(0)
      end
    end

    context "when there's a transaction but does not match agreement number" do
      let(:agreement_number) { 5_000_000 }
      it 'returns no transactions' do
        expect(described_class.by_agreement_number(attributes).count).to eq(0)
      end
    end
  end

  describe 'filter by location initials' do
    context 'when there are matching records' do
      let(:attributes) { { agreement_number: agreement_number, location_initial: 'AAAA', days: 90, offset: 0, limit: 10, active: false } }
      it 'returns transaction filtered by location intials' do
        expect(described_class.by_agreement_number(attributes).count).to eq(1)
      end
    end

    context 'when there are no matching records' do
      let(:attributes) { { agreement_number: agreement_number, location_initial: 'ABCD', days: 90, offset: 0, limit: 10, active: false } }
      it 'returns 0 rows' do
        expect(described_class.by_agreement_number(attributes).count).to eq(0)
      end
    end
  end
end
