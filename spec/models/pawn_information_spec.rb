# encoding: utf-8
require 'spec_helper'

describe PawnInformation do
  describe '#touch_vehicle_price' do
    let(:price) { 12_345 }
    let(:request_class) { double 'VehicleAdjustmentsRequest' }
    let(:request) { double 'vehicle_adjustments_request' }

    subject(:pawn_information) do
      FactoryGirl.create(:pawn_information_with_offer, vehicle_purchase_price: price)
    end

    before do
      allow(request_class).to receive(:new).and_return(request)
    end

    it 'updates the vehicle price when it changed' do
      new_price = 54_321
      adjustments = double(vehicle_price: new_price.to_s)
      allow(request).to receive(:load).and_yield(adjustments)

      pawn_information.touch_vehicle_price(request_class)

      expect(pawn_information.vehicle_purchase_price).to eq(new_price)
    end

    it 'does not update vehicle price when zero' do
      adjustments = double(vehicle_price: '0')
      allow(request).to receive(:load).and_yield(adjustments)

      pawn_information.touch_vehicle_price(request_class)

      expect(pawn_information.vehicle_purchase_price).to eq(price)
    end
  end
end
