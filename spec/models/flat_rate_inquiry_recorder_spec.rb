# encoding: utf-8
require 'spec_helper'

describe FlatRateInquiryRecorder do
  let(:user) { mock_model('User') }
  let(:miles_selected) { 360 }
  let(:psi_eligible) { true }
  let(:flat_rate) { double('FlatRate') }
  let(:pawn_access_no) { '1000000' }
  let(:decorated_flat_rate) { FlatRatePriceDecorator.new(flat_rate, miles_selected, psi_eligible) }

  describe '#record_flat_rate_inquiry' do
    subject(:recorder) { described_class.new(decorated_flat_rate, pawn_access_no, miles_selected, psi_eligible, user) }

    before do
      allow(flat_rate).to receive(:agreement_no)
      allow(flat_rate).to receive(:target_price_360_psi)
    end

    describe 'flat rate creation' do
      it 'creates a new flat rate inquiry record' do
        expect_any_instance_of(FlatRateInquiry).to receive(:save).and_return(true)

        recorder.create_flat_rate_inquiry
      end
    end
  end
end
