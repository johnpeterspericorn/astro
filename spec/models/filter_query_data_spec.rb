# encoding: utf-8
require 'spec_helper'

describe FilterQueryData do
  let(:badge_number)          { SasDataHelper::DEFAULT_BADGE_NUMBER }
  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:agreement_number)         { SasDataHelper::DEFAULT_DEALER_NUMBER }

  describe '#filter_method' do
    it 'should return badge number if it is provided' do
      expect(described_class.new(badge_number: badge_number).filter_method).to eql :badge_number
    end

    it 'should return pawn access number if it is provided' do
      expect(described_class.new(pawn_access_number: pawn_access_number).filter_method).to eql :pawn_access_number
    end
  end

  describe 'VNUM handling' do
    let(:vnum)         { 'a VNUM'       }
    let(:another_vnum) { 'another VNUM' }

    specify { expect(described_class.new(vnums: [vnum]).vnums.length).to eq(1)              }
    specify { expect(described_class.new(vnums: [vnum, another_vnum]).vnums.length).to eq(2) }
    specify { expect(described_class.new(vnum:  vnum).vnums.length).to eq(1)                }
  end

  describe '#vehicles_for_display' do
    let(:filter_query_data) { FilterQueryData.new(pawn_access_number: pawn_access_number, agreement_numbers: [agreement_number]) }

    it 'returns an empty array when no filter is given' do
      filter_query_data = FilterQueryData.new
      expect(filter_query_data.vehicles_for_display).to be_empty
    end

    describe 'for pawn_access_numbers' do
      let(:sold_vehicle_with_correct_pawn_access_number)                           { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_number) }
      let(:sold_vehicle_with_zero_pawn_access_number_and_correct_agreement_number)    { FactoryGirl.create(:sold_vehicle, pawn_access_no: '0',  agreement_no: agreement_number) }
      let(:sold_vehicle_with_nonzero_pawn_access_number_and_correct_agreement_number) { FactoryGirl.create(:sold_vehicle, pawn_access_no: '-1', agreement_no: agreement_number) }
      subject(:vehicles_for_display) { filter_query_data.vehicles_for_display }

      it { is_expected.to include sold_vehicle_with_correct_pawn_access_number }
      it { is_expected.to include sold_vehicle_with_zero_pawn_access_number_and_correct_agreement_number }
      it { is_expected.not_to include sold_vehicle_with_nonzero_pawn_access_number_and_correct_agreement_number }
    end

    describe 'with VNUMs' do
      let(:sold_vehicle_with_correct_pawn_access_number)                           { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_number) }
      let(:another_sold_vehicle_with_correct_pawn_access_number)                   { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_number) }
      let(:sold_vehicle_with_zero_pawn_access_number_and_correct_agreement_number)    { FactoryGirl.create(:sold_vehicle, pawn_access_no: '0', agreement_no: agreement_number) }
      subject(:vehicles_for_display) { filter_query_data.vehicles_for_display }

      it 'should filter by a list of VNUMs' do
        filter_query_data.vnums = [sold_vehicle_with_correct_pawn_access_number.vnum, another_sold_vehicle_with_correct_pawn_access_number.vnum]
        expect(vehicles_for_display.count).to eq(2)
        expect(vehicles_for_display).to include sold_vehicle_with_correct_pawn_access_number
        expect(vehicles_for_display).to include another_sold_vehicle_with_correct_pawn_access_number
      end
    end
  end
end
