# encoding: utf-8
require 'spec_helper'

describe PaymentResponse do
  describe '#success?' do
    context 'when there\'s no error_message' do
      subject { FactoryGirl.create(:payment_response) }
      it { is_expected.to be_success }
    end

    context 'when there\'s an error_message' do
      subject { FactoryGirl.create(:payment_response_with_error) }
      it { is_expected.not_to be_success }
    end
  end

  describe '#error_code' do
    context 'when there\'s no error' do
      subject { FactoryGirl.create(:payment_response).error_code }
      it { is_expected.to be_nil }
    end

    context 'when there\'s an error_message' do
      subject { FactoryGirl.create(:payment_response_with_error).error_code }
      it { is_expected.to eq(841) }
    end
  end

  describe '#error_message_sanitized' do
    context 'when there\'s no error' do
      subject { FactoryGirl.create(:payment_response).error_message_sanitized }
      it { is_expected.to be_nil }
    end

    context 'when there\'s an error_message' do
      subject { FactoryGirl.create(:payment_response_with_error).error_message_sanitized }
      it { is_expected.to eq('Error validating card/account number range') }
    end
  end

  describe '#error_message_internationalized' do
    context 'when there\'s no error' do
      subject { FactoryGirl.create(:payment_response).error_message_internationalized }
      it { is_expected.to be_nil }
    end

    context 'when there\'s an error_message' do
      subject { FactoryGirl.create(:payment_response_with_error).error_message_internationalized }
      it { is_expected.to eq('Card Error') }
    end
  end
end
