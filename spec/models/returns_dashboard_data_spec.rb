# encoding: utf-8
require 'spec_helper'

describe ReturnsDashboardData do
  include_context 'returns_dashboard_data'

  before do
    create_associated_vehicles
  end

  it 'gives returns by agreement' do
    filtered_returns = ReturnInvoice.where('id > 0')
    @data = ReturnsByAgreement.new(filtered_returns.limit(10))
    @data.each do |agreement, return_data|
      expect(return_data.count).to eq(2) if agreement.agreement_no == agreement_one
      expect(return_data.count).to eq(1) if agreement.agreement_no == agreement_two
    end
  end

  it 'gives returns by location' do
    filtered_returns = ReturnInvoice.where('id > 0')
    @data = ReturnsByLocation.new(filtered_returns.limit(10))
    @data.each do |location, return_data|
      expect(return_data.count).to eq(2) if location.initials == pawn_location_a.initials
      expect(return_data.count).to eq(1) if location.initials == pawn_location_b.initials
    end
  end

  it 'gives returns by vehicle' do
    filtered_returns = ReturnInvoice.where('id > 0')
    @data = ReturnsByVehicle.new(filtered_returns.limit(10))
    vnums = []
    pawn_locations = []
    @data.each do |vehicle_info|
      vnums << vehicle_info.field('vnum')
      pawn_locations << vehicle_info.field('pawn_location_name')
    end
    expect(vnums).to include('12345')
    expect(vnums).to include('121212')
    expect(pawn_locations).to include('Aaaaa')
  end
end
