# encoding: utf-8
require 'requests_helper'

describe SoldVehicle do
  let(:agreement_number)         { SasDataHelper::DEFAULT_DEALER_NUMBER         }
  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }

  let(:filter_query_data) { FilterQueryData.new(pawn_access_number: pawn_access_number, agreement_numbers: [agreement_number]) }

  context 'scopes' do
    describe '.for_pawn_access_number' do
      let(:sold_vehicle_with_correct_pawn_access_number)                           { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_number) }
      let(:sold_vehicle_with_zero_pawn_access_number_and_correct_agreement_number)    { FactoryGirl.create(:sold_vehicle, pawn_access_no: '0',  agreement_no: agreement_number) }
      let(:sold_vehicle_with_nonzero_pawn_access_number_and_correct_agreement_number) { FactoryGirl.create(:sold_vehicle, pawn_access_no: '-1', agreement_no: agreement_number) }
      subject(:sold_vehicles_for_pawn_access_number) { described_class.for_pawn_access_number(filter_query_data) }

      it { is_expected.to include sold_vehicle_with_correct_pawn_access_number }
      it { is_expected.to include sold_vehicle_with_zero_pawn_access_number_and_correct_agreement_number }
      it { is_expected.not_to include sold_vehicle_with_nonzero_pawn_access_number_and_correct_agreement_number }
    end
  end

  describe '#eligible_for_purchase?' do
    let(:valid_params) { { created_at: 15.minutes.ago, ineligibility_condition: 0, must_be_promised_at: 1.day.from_now, sblu: 'some sblu', location_initials: 'DRIV' } }
    let(:too_old_sold_vehicle) { SoldVehicle.new(valid_params.merge(created_at: 2.days.ago)) }
    let(:ineligible_vehicle)   { SoldVehicle.new(valid_params.merge(ineligibility_condition: 1)) }
    let(:past_cutoff_vehicle)    { SoldVehicle.new(valid_params.merge(must_be_promised_at: 1.day.ago)) }
    let(:available_vehicle) { SoldVehicle.new(valid_params) }

    specify { expect(too_old_sold_vehicle).not_to be_available_for_purchase }
    specify { expect(ineligible_vehicle).not_to be_available_for_purchase   }
    specify { expect(past_cutoff_vehicle).not_to be_available_for_purchase }
    specify do
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(true)
      expect(available_vehicle).not_to be_available_for_purchase
    end
    specify do
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(false)
      expect(available_vehicle).to be_available_for_purchase
    end
  end

  describe 'sold_vehicles_load_columns' do
    it 'should ensure the columns in sold_vehicles and sold_vehicles_load table match up' do
      resultset = ActiveRecord::Base.connection.execute("select column_name, data_type from information_schema.columns where table_name='sold_vehicles_load'")
      sold_vehicles_load_columns = resultset.to_a.map { |col| col['column_name'] }
      expect(SoldVehicle.column_names.reject { |col| col == 'source_document_link' }.sort).to match_array(sold_vehicles_load_columns.sort)
    end
  end

  describe 'duplicate check' do
    let!(:pawn_location) { FactoryGirl.create(:svaa_location) }
    let(:vehicle) do
      FactoryGirl.create(:sold_vehicle,
                         automatic_purchase: true,
                         location_initials: 'SVAA',
                         preselected_promise_days: 14,
                         preselected_promise_miles: 250,
                         automatic_purchase_email: 'a@a.com'
                        )
    end
    it 'is invalid if an purchase for the mase vnum/location/work-order/sblu combo exists' do
      AutomatedPromisePurchaser.record_purchase(vehicle) # purchase vehicle
      new_vehicle = vehicle.dup
      vehicle.delete

      expect(new_vehicle).not_to be_valid
    end

    describe 'walkup' do
      let(:walkup_vehicle) { FactoryGirl.create(:sold_vehicle, location_initials: 'SVAA') }

      it 'is invalid if a SoldVehicle exists for the mase vnum/location/work-order/sblu combo' do
        new_vehicle = walkup_vehicle.dup
        expect(new_vehicle).not_to be_valid
      end
    end
  end
end
