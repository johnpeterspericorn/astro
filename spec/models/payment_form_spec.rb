# encoding: utf-8
require 'spec_helper'

describe PaymentForm do
  subject(:payment) do
    described_class.new(
      user:               user,
      promise_purchase: promise_purchase
    )
  end
  let(:user) { FactoryGirl.create(:user_without_payment_profile) }
  let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:promise_offer) { FactoryGirl.build(:promise_offer, promise_250: 250) }
  let(:card_attrs) do
    {
      object: 'card',
      exp_month: '1',
      exp_year: Time.current.year + 2,
      number: '4242424242424242',
      currency: 'usd',
      cvc: 123
    }
  end

  describe '.new' do
    it 'creates an instance' do
      instance = described_class.new user: user, promise_purchase: promise_purchase
      expect(instance.user).to eq(user)
      expect(instance.promise_purchase).to eq(promise_purchase)
    end
  end

  describe '#process', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
    before do
      PaymentService::Customer.create(user)
      user.reload
      PaymentService::Customer.new(user).add_card(card_attrs)
      expect(Payment.count).to eq(0)
    end

    it 'creates payment record' do
      expect { payment.process }.to change { Payment.count }.by(1)
    end

    it 'creates a payment response record' do
      expect { payment.process }.to change { PaymentResponse.count }.by(1)
    end

    describe 'return value' do
      context 'when payment service is unsuccessful' do
        let(:card_attrs) do
          {
            object: 'card',
            exp_month: '1',
            exp_year: Time.current.year + 2,
            number: '4000000000000341', # Attaching this card to a Customer object succeeds, but attempts to charge the customer fail.
            currency: 'usd',
            cvc: 123
          }
        end

        it 'is false', vcr: { cassette_name: 'vehicle_adjustments_api/card_declined' } do
          payment.process
          expect(payment.error_full_message).to eq('Your card was declined.')
          expect(Payment.count).to eq(0)
        end

        it 'creates a payment response record' do
          expect { payment.process }.to change { PaymentResponse.count }.by(1)
          expect(Payment.count).to eq(1)
        end
      end

      it 'is true otherwise' do
        expect(payment.process).to be_truthy
      end
    end
  end
end
