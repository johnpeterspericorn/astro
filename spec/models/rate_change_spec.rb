# encoding: utf-8
require 'spec_helper'

describe RateChange do
  describe 'effective_date_input' do
    before do
      FactoryGirl.create(:agreement_information, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER)
    end

    it 'calculates if tomorrow is valid' do
      perf_alert = FactoryGirl.create(:rate_change, effective_date: Time.zone.now + 1.day)
      expect(perf_alert.valid?).to be_truthy
    end

    it 'calculates if yesterday is valid' do
      perf_alert = FactoryGirl.build(:rate_change, effective_date: Time.zone.now - 1.day)
      expect(perf_alert.valid?).to be_falsey
    end

    it 'calculates if 2 days from now is valid' do
      perf_alert = FactoryGirl.create(:rate_change, effective_date: Time.zone.now + 2.days)
      expect(perf_alert.valid?).to be_truthy
    end
  end
end
