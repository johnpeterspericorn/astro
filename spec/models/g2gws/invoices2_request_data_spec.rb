# encoding: utf-8
require 'spec_helper'

describe G2gws::Invoices2RequestData do
  let(:offer) { double(agreement_no: '12345', sblu: '123', seller_paid?: true) }
  subject { described_class.new(offer) }

  it 'takes in an offer on create' do
    expect { described_class.new }.to raise_error(ArgumentError)
    expect { described_class.new(offer) }.not_to raise_error
  end

  it 'responds to agreement_number and returns offer agreement no' do
    expect(subject.agreement_number).to be(offer.agreement_no)
  end

  it 'responds to sblu and returns offer sblu' do
    expect(subject.sblu).to be(offer.sblu)
  end

  it 'responds to seller_paid? and returns offer seller_paid' do
    expect(subject.seller_paid?).to be(offer.seller_paid?)
  end
end
