# encoding: utf-8
require 'spec_helper'

describe Address do
  let(:street) { '1989 meso Ave.' }
  let(:city) { 'Chicago' }
  let(:state) { 'GA' }
  let(:zip) { '30317' }

  subject(:address) do
    described_class.new(
      street,
      nil, # no suite
      city,
      state,
      zip,
      verify_flag
    )
  end

  before do
    Settings.address_verification_enabled = true
  end

  describe 'address verification enabled' do
    let(:verify_flag) { true }

    describe '#valid?' do
      it 'is valid with a verifiable address' do
        VCR.use_cassette 'easy_post_api/valid_address' do
          expect(address).to be_valid
        end
      end

      context 'with bad data' do
        let(:street) { 'BAD Street' }
        it 'is not valid with bogus information' do
          VCR.use_cassette 'easy_post_api/invalid_street' do
            expect(address).to_not be_valid
          end
        end
      end
    end

    describe 'EasyPost error handling' do
      let(:street) { '15 TANGUAY AVENUE' }
      let(:city) { 'NASHUA' }
      let(:state) { 'NH' }
      let(:zip) { '03062' }
      it 'is valid in spite of an EasyPost error' do
        VCR.use_cassette 'easy_post_api/with_easy_post_error' do
          expect(address).to be_valid
        end
      end
    end
  end

  describe 'post office box addresses' do
    let(:verify_flag) { true }

    post_office_box_variations = [
      'Post Office Box 123',
      'P.O. Box 123',
      'PO. Box 123',
      'PO Box 123',
      'po box 123'
    ]

    tricky_non_post_office_box_variations = [
      '123 Poboy Boxer Street',
      '123 Po Boxten Road'
    ]

    post_office_box_variations.each do |po_addr|
      describe "street is '#{po_addr}'" do
        let(:street) { po_addr }

        it 'is true' do
          expect(address.post_office_box?).to be_truthy
        end
      end
    end

    tricky_non_post_office_box_variations.each do |non_po_addr|
      describe "street is '#{non_po_addr}'" do
        let(:street) { non_po_addr }

        it 'is false' do
          expect(address.post_office_box?).to be_falsey
        end
      end
    end
  end

  describe 'address verification disabled' do
    let(:verify_flag) { false }

    describe 'not valid but continue creating easy post address' do
      it 'is valid with a verifiable address' do
        VCR.use_cassette 'easy_post_api/without_verification' do
          expect(address).to be_valid
        end
      end
    end
  end
end
