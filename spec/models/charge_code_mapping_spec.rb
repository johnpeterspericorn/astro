# encoding: utf-8
require 'spec_helper'

describe ChargeCodeMapping do
  let(:promise_offer) { double 'PromiseOffer' }
  subject { described_class.load(promise_offer) }

  context 'when seller_paid is true' do
    before do
      allow(promise_offer).to receive(:seller_paid?) { 'true' }
    end

    context 'and automatic_purchase is true' do
      before do
        allow(promise_offer).to receive(:automatic_purchase) { 'true' }
      end

      describe 'product is purchase_protect' do
        let(:product) { 'purchase_protect' }

        it 'returns charge_code=963 and call_id=DEALS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.seller_paid).to eq(true)
          expect(subject.automatic_purchase).to eq(true)
          expect(subject.charge_code).to eq(963)
          expect(subject.call_id).to eq('DEALS')
        end

        it 'returns charge_code=927 and call_id=DSNPS if limited_volume is true' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { true }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(927)
          expect(subject.call_id).to eq('DSNPS')
        end

        it 'returns charge_code=927 and call_id=DSNPS if percent_coverage is present' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { 5 }

          expect(subject.charge_code).to eq(927)
          expect(subject.call_id).to eq('DSNPS')
        end
      end

      describe 'product is extended_protect' do
        let(:product) { 'extended_protect' }

        it 'returns charge_code=963 and call_id=DEALS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(963)
          expect(subject.call_id).to eq('DEALS')
        end
      end

      describe 'product is market_protect' do
        let(:product) { 'market_protect' }

        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is total_protect' do
        let(:product) { 'total_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is not in system' do
        let(:product) { 'MISSSING_OR_NEW' }
        it 'returns charge_code=927 and call_id=DSNPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(ChargeCodeMapping::NONEXISTENT_PRODUCT_NAME)
          expect(subject.product).to_not eq(product)
          expect(subject.charge_code).to eq(927)
          expect(subject.call_id).to eq('DSNPS')
        end
      end
    end

    context 'and automatic_purchase is false' do
      before do
        allow(promise_offer).to receive(:automatic_purchase) { 'false' }
      end

      describe 'product is purchase_protect' do
        let(:product) { 'purchase_protect' }
        it 'returns charge_code=963 and call_id=DEALS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.seller_paid).to eq(true)
          expect(subject.automatic_purchase).to eq(false)
          expect(subject.charge_code).to eq(963)
          expect(subject.call_id).to eq('DEALS')
        end
      end

      describe 'product is extended_protect' do
        let(:product) { 'extended_protect' }
        it 'returns charge_code=963 and call_id=DEALS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(963)
          expect(subject.call_id).to eq('DEALS')
        end
      end

      describe 'product is market_protect' do
        let(:product) { 'market_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is total_protect' do
        let(:product) { 'total_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is not in system' do
        let(:product) { 'MISSSING_OR_NEW' }
        it 'returns charge_code=927 and call_id=DSNPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(ChargeCodeMapping::NONEXISTENT_PRODUCT_NAME)
          expect(subject.product).to_not eq(product)
          expect(subject.charge_code).to eq(927)
          expect(subject.call_id).to eq('DSNPS')
        end
      end
    end
  end

  context 'when seller_paid is false' do
    before do
      allow(promise_offer).to receive(:seller_paid?) { 'false' }
    end

    context 'and automatic_purchase is true' do
      before do
        allow(promise_offer).to receive(:automatic_purchase) { 'true' }
      end

      describe 'product is purchase_protect' do
        let(:product) { 'purchase_protect' }
        it 'returns charge_code=962 and call_id=DEALB' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.seller_paid).to eq(false)
          expect(subject.automatic_purchase).to eq(true)
          expect(subject.charge_code).to eq(962)
          expect(subject.call_id).to eq('DEALB')
        end

        it 'returns charge_code=928 and call_id=DSNPB if percent_coverage is present' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { 5 }

          expect(subject.charge_code).to eq(928)
          expect(subject.call_id).to eq('DSNPB')
        end

        it 'returns charge_code=928 and call_id=DSNPB if limited_volume is true' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { true }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(928)
          expect(subject.call_id).to eq('DSNPB')
        end
      end

      describe 'product is extended_protect' do
        let(:product) { 'extended_protect' }
        it 'returns charge_code=962 and call_id=DEALB' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(962)
          expect(subject.call_id).to eq('DEALB')
        end
      end

      describe 'product is market_protect' do
        let(:product) { 'market_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is total_protect' do
        let(:product) { 'total_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is not in system' do
        let(:product) { 'MISSSING_OR_NEW' }
        it 'returns charge_code=928 and call_id=DSNPB' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(ChargeCodeMapping::NONEXISTENT_PRODUCT_NAME)
          expect(subject.product).to_not eq(product)
          expect(subject.charge_code).to eq(928)
          expect(subject.call_id).to eq('DSNPB')
        end
      end
    end

    context 'and automatic_purchase is false' do
      before do
        allow(promise_offer).to receive(:automatic_purchase) { 'false' }
      end

      describe 'product is purchase_protect' do
        let(:product) { 'purchase_protect' }
        it 'returns charge_code=929 and call_id=DEALA' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.seller_paid).to eq(false)
          expect(subject.automatic_purchase).to eq(false)
          expect(subject.charge_code).to eq(929)
          expect(subject.call_id).to eq('DEALA')
        end
      end

      describe 'product is extended_protect' do
        let(:product) { 'extended_protect' }
        it 'returns charge_code=962 and call_id=DEALB' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.charge_code).to eq(962)
          expect(subject.call_id).to eq('DEALB')
        end
      end

      describe 'product is market_protect' do
        let(:product) { 'market_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is total_protect' do
        let(:product) { 'total_protect' }
        it 'returns charge_code=925 and call_id=DSMPS' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(product)
          expect(subject.charge_code).to eq(925)
          expect(subject.call_id).to eq('DSMPS')
        end
      end

      describe 'product is not in system' do
        let(:product) { 'MISSSING_OR_NEW' }
        it 'returns charge_code=928 and call_id=DSNPB' do
          allow(promise_offer).to receive(:preselected_promise_kind) { product }
          allow(promise_offer).to receive(:limited_quantity?) { false }
          allow(promise_offer).to receive(:percent_coverage) { nil }

          expect(subject.product).to eq(ChargeCodeMapping::NONEXISTENT_PRODUCT_NAME)
          expect(subject.product).to_not eq(product)
          expect(subject.charge_code).to eq(926)
          expect(subject.call_id).to eq('DSNPA')
        end
      end
    end
  end
end
