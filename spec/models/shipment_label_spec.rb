# encoding: utf-8
require 'spec_helper'

describe ShipmentLabel do
  let(:shipment) { double 'EasyPost::Shipment' }

  subject(:label) { described_class.new(shipment) }

  describe '.find' do
    it 'retrieves the EasyPost::Shipment by the given id' do
      id = 123

      expect(EasyPost::Shipment).to receive(:retrieve).with(id).and_return(shipment)
      expect(described_class).to receive(:new).with(shipment)

      described_class.find(id)
    end
  end

  describe '.buy' do
    it 'buys a NextDayAirShipment with the given AgreementInformation' do
      agreement_info = double 'AgreementInformation'

      expect(ShipmentHandler).to receive(:new).with(agreement_info).and_return(shipment)
      expect(shipment).to receive(:buy).and_return(shipment)
      expect(described_class).to receive(:new).with(shipment)

      described_class.buy(agreement_info)
    end
  end

  describe '#id' do
    it 'delegates to shipment' do
      id = double :id
      allow(shipment).to receive(:id).and_return(id)

      expect(label.id).to eq(id)
    end
  end

  describe '#url' do
    it 'delegates to shipment\'s postage_label' do
      url     = double :url
      postage = double :postage
      allow(shipment).to receive(:postage_label).and_return(postage)
      allow(postage).to receive(:label_url).and_return(url)

      expect(label.url).to eq(url)
    end
  end

  describe '#tracking_code' do
    it 'delegates to shipment' do
      tracking_code = double :tracking_code
      allow(shipment).to receive(:tracking_code).and_return(tracking_code)

      expect(label.tracking_code).to eq(tracking_code)
    end
  end

  describe '#carrier' do
    it 'delegates to shipment' do
      carrier = double :carrier
      tracker = double :tracker

      allow(shipment).to receive(:tracker).and_return(tracker)
      allow(tracker).to receive(:carrier).and_return(carrier)

      expect(label.carrier).to eq(carrier)
    end
  end
end
