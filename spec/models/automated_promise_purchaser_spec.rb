# encoding: utf-8
require 'spec_helper'

describe AutomatedPromisePurchaser do
  let(:days)    { 7   }
  let(:miles)   { 250 }
  let(:email)   { 'test@example.com' }
  let(:purchase) { double 'PromisePurchase', id: nil }
  let(:offer) { FactoryGirl.build(:promise_offer, pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER) }
  let(:existing_offer)   { FactoryGirl.build(:promise_offer, pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER) }
  let(:vehicle) { double 'SoldVehicle', promise_offers: [], automatic_purchase: true, preselected_promise_days: days, preselected_promise_miles: miles, automatic_purchase_email: email, preselected_promise_kind: PromisePurchase::PURCHASE_PROTECT, partner: 'mas' }

  let(:recorded_offer_attributes) do
    {
      additional_days: vehicle.preselected_promise_days - Promise::BASE_DURATION,
      miles_selected:  vehicle.preselected_promise_miles,
      promise_offer: offer,
      channel: PromiseChannel::AUTOMATIC,
      kind: PromisePurchase::PURCHASE_PROTECT,
      partner: 'mas'
    }
  end

  let(:existing_offer_attributes) do
    { additional_days: vehicle.preselected_promise_days - Promise::BASE_DURATION,
      miles_selected:  vehicle.preselected_promise_miles,
      promise_offer: existing_offer,
      channel: PromiseChannel::AUTOMATIC,
      kind: PromisePurchase::PURCHASE_PROTECT,
      partner: 'mas'
    }
  end

  before do
    Delayed::Worker.delay_jobs = true
    allow(vehicle).to receive(:valid_for_automated_purchase?).and_return(true)
    allow(offer).to receive(:reload)
    allow(offer).to receive(:promise_purchase) { purchase }
    allow(existing_offer).to receive(:reload)
    allow(existing_offer).to receive(:promise_purchase) { purchase }
  end

  after do
    Delayed::Worker.delay_jobs = false
  end

  describe 'constructor error handling' do
    it 'should blow up for invalid days' do
      expect { described_class.new(vehicle, 2) }.to raise_error ArgumentError
    end

    it 'should blow up for invalid miles' do
      expect { described_class.new(vehicle, days, 11) }.to raise_error ArgumentError
    end
  end

  describe '.record_purchase' do
    before do
      allow(offer).to receive(:paid_by_floor_plan?).and_return(false)
      allow(purchase).to receive(:send_confirmation)
      allow(purchase).to receive(:send_confirmation)

      expect(PromiseSelectionRecorder).to receive(:record_selected).with([purchase], email)
      expect(purchase).to receive :update_attributes!
      expect(vehicle).to receive(:update_attributes!).with(automatic_purchase_processed: true)
    end

    it 'delegates to all appropriate objects in order to process the purchase' do
      expect(PromiseOfferRecorder).to receive(:record_offer).with(vehicle).and_return offer
      expect(PromisePurchase).to receive(:create!).with(recorded_offer_attributes).and_return purchase

      described_class.record_purchase(vehicle)
    end

    it 'accepts days and miles for non-automatic purchases' do
      additional_days = 14
      miles_selected = 500
      passed_miles_and_days_attributes = {
        additional_days: additional_days,
        miles_selected:  miles_selected,
        promise_offer: offer,
        channel: PromiseChannel::AUTOMATIC,
        kind: PromisePurchase::PURCHASE_PROTECT,
        partner: 'mas'
      }
      expect(PromiseOfferRecorder).to receive(:record_offer).with(vehicle).and_return offer
      expect(PromisePurchase).to receive(:create!).with(passed_miles_and_days_attributes).and_return purchase

      described_class.record_purchase(vehicle, additional_days + Promise::BASE_DURATION, miles_selected)
    end

    it "uses the vehicle's first offer if there are any" do
      allow(existing_offer).to receive(:paid_by_floor_plan?).and_return(false)
      allow(existing_offer).to receive(:pawn_access_no).and_return(SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)

      expect(PromiseOfferRecorder).to receive(:record_offer).with(vehicle).and_return existing_offer
      expect(PromisePurchase).to receive(:create!).with(existing_offer_attributes).and_return purchase

      described_class.record_purchase(vehicle)
    end

    context 'with preselected promise kind' do
      before do
        allow(vehicle).to receive(:preselected_promise_kind).and_return(PromisePurchase::MARKET_PROTECT)
      end

      it 'accepts preselected promise kind while savnumg purchase' do
        purchase_attributes = recorded_offer_attributes.merge!(kind: PromisePurchase::MARKET_PROTECT)

        expect(PromiseOfferRecorder).to receive(:record_offer).with(vehicle).and_return offer
        expect(PromisePurchase).to receive(:create!).with(purchase_attributes).and_return purchase

        described_class.record_purchase(vehicle)
      end
    end

    context 'charges enabled' do
      let(:charge) { double 'PromiseCharge' }

      before do
        Settings.charges_enabled = true

        allow(PromiseOfferRecorder).to receive(:record_offer).and_return(offer)
        allow(PromisePurchase).to receive(:create!).and_return(purchase)
        allow(PromiseCharge).to receive(:new).and_return(charge)
        allow(offer).to receive(:seller_paid?).and_return(false)
        allow(offer).to receive(:pawn_information)
        allow(offer).to receive(:bypass_charge?).and_return(false)
        allow(offer).to receive(:paid_by_floor_plan?).and_return(false)
        allow(offer).to receive(:pending?).and_return(false)
        allow(offer).to receive(:decorate)
      end

      it 'charges the purchase' do
        expect(PromiseChargeJob).to receive(:enqueue).with(charge)

        described_class.record_purchase(vehicle)
      end

      it 'does not charge seller paid vehicles' do
        allow(offer).to receive(:seller_paid?).and_return(true)

        expect(PromiseChargeJob).not_to receive(:enqueue)

        described_class.record_purchase(vehicle)
      end

      it 'does not charge by bypass is true' do
        allow(offer).to receive(:bypass_charge?).and_return(true)

        expect(PromiseChargeJob).not_to receive(:enqueue)

        described_class.record_purchase(vehicle)
      end
    end
  end

  context 'invalid vehicle' do
    subject(:invalid_vehicle) { FactoryGirl.build(:sold_vehicle, automatic_purchase: true) }
    before :each do
      invalid_vehicle.save(validate: false)
    end

    it '.record_purchase sends an email when a vehicle validation fails' do
      described_class.record_purchase(invalid_vehicle)
      expect(enqueued_deliveries.size).to eq 1
    end

    it 'marks the vehicle as processed' do
      described_class.record_purchase(invalid_vehicle)
      expect(invalid_vehicle).to be_automatic_purchase_processed
    end
  end
end
