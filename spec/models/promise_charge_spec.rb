# encoding: utf-8
require 'spec_helper'

describe PromiseCharge do
  let(:promise_offer_decorator) { double('PromiseOfferDecorator').as_null_object }
  let(:astroheim_charge) { double 'Astroheim::Charge' }

  subject(:charge) { described_class.new(promise_offer_decorator) }

  specify '#submit' do
    expect(Astroheim::Charge).to receive(:new).and_return(astroheim_charge)
    expect(astroheim_charge).to receive(:submit!)

    charge.submit
  end
end
