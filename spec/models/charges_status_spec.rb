# encoding: utf-8
require 'spec_helper'

describe ChargesStatus do
  let(:charge) { double('Astroheim Charge') }
  subject { described_class.new(charge) }

  describe '#up?' do
    context 'when submitting a charge raises a charges error' do
      before do
        allow(charge).to receive(:submit!) { raise Astroheim::Charges::AddChargeError }
      end

      # A charge error indicates the service is responding
      it 'returns true' do
        expect(subject.up?).to be_truthy
      end
    end

    context 'when submitting a charge raises another error' do
      before do
        allow(charge).to receive(:submit!) { raise Errno::ECONNREFUSED }
      end

      it 'returns false' do
        expect(subject.up?).to be_falsey
      end
    end

    # Should never happen, but worth harnessing
    context 'when submitting a charge is "successful"' do
      before do
        allow(charge).to receive(:submit!)
      end

      it 'returns true' do
        expect(subject.up?).to be_truthy
      end
    end
  end
end
