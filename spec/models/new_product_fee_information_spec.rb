# encoding: utf-8
require 'spec_helper'

describe NewProductFeeInformation do
  include_context 'new_product_data'
  let!(:pawn_location_a) { FactoryGirl.create(:pawn_location, initials: 'CAC', name: 'Canada', country: 'Canada', partner_network: 'CA-Astroheim') }
  let!(:pawn_location_b) { FactoryGirl.create(:pawn_location, initials: 'AAAA', name: 'AAA', country: 'United States of America', partner_network: 'US-Astroheim') }

  let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false) }
  let!(:pawn_info) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer, miles_selected: 250, additional_days: 7) }
  let!(:invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }

  let(:promise_offer_2) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false, group_code: 'ABCD1234') }
  let!(:pawn_info_2) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer_2, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_2) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_2, miles_selected: 250, additional_days: 7) }
  let!(:invoice_2) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_2) }

  let(:promise_offer_3) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false, group_code: 'ABCD1234') }
  let!(:pawn_info_3) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer_3, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_3) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_3, miles_selected: 250, additional_days: 7) }
  let!(:invoice_3) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_3) }

  let(:promise_offer_4) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false) }

  let!(:pawn_info_4) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer_4, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_4) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_4, miles_selected: 250, additional_days: 7) }
  let!(:invoice_4) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_4) }

  let(:promise_offer_5) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: true, group_code: 'ABCD1234') }
  let!(:pawn_info_5) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer_5, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_5) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_5, miles_selected: 250, additional_days: 7) }
  let!(:invoice_5) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_5) }

  let(:promise_offer_6) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false, group_code: 'ABCD1234') }
  let!(:pawn_info_6) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_b, vehicle_purchase_price: 15_000, promise_offer: promise_offer_5, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_6) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_6, miles_selected: 250, additional_days: 7) }
  let!(:invoice_6) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_6) }

  let(:promise_offer_7) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false) }
  let!(:pawn_info_info) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location_a, vehicle_purchase_price: 15_000, promise_offer: promise_offer_7, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER) }
  let(:promise_purchase_7) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer_7, miles_selected: 250, additional_days: 7) }
  let!(:invoice_7) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_7) }

  context 'new product fee information' do
    subject(:fee_information) do
      NewProductFeeInformation.new(promise_purchase)
    end

    before do
      create_product_fees
    end

    it 'should return no records for default(365) timeframe and should default to defaulted tiers' do
      expect(subject.fee_records.count).to eq(0)
    end

    it 'should return 2 returns within NewProductKind::TIMEFRAME_365 days' do
      expect(subject.get_number_of_returns_within_timeframe(NewProductKind::TIMEFRAME_365)).to eq(5)
    end

    it 'should have fee data object for 3 invoices' do
      fee_masple = { count: 3, fee: 0.0, timeframe: NewProductKind::TIMEFRAME_365, index: 0, additional_returns: 0, additional_fee: 150.0 }
      expect(subject.fee_info(3)).to eq(fee_masple)
    end

    it 'should have fee details' do
      fee_masple = { count: 6, fee: 150, timeframe: NewProductKind::TIMEFRAME_365, index: 1, additional_returns: 1, additional_fee: 350, number_of_returns: 5 }
      expect(subject.fee_details).to eq(fee_masple)
    end

    it 'should log additional fee details' do
      fee_data = { count: 3, fee: 0.0, timeframe: NewProductKind::TIMEFRAME_365, index: 0, additional_returns: 1, additional_fee: 150.0, number_of_returns: 2 }
      number_of_returns = 3
      fee_masple = { count: 3, fee: 0.0, timeframe: NewProductKind::TIMEFRAME_365, index: 0, additional_returns: 1, additional_fee: 150.0, number_of_returns: 2 }
      expect(subject.log_additional_fee_info(fee_data, number_of_returns)).to eq(fee_masple[:additional_fee])
    end
  end

  context 'group palv returns' do
    subject(:fee_information) do
      NewProductFeeInformation.new(promise_purchase_2)
    end

    before do
      create_product_fees
    end

    it 'should return 3 returns for group within NewProductKind::TIMEFRAME_365 days' do
      expect(subject.get_number_of_returns_within_timeframe(NewProductKind::TIMEFRAME_365)).to eq(4)
    end

    it 'should have fee details' do
      fee_masple = { count: 6, fee: 150.0, timeframe: NewProductKind::TIMEFRAME_365, index: 1, additional_returns: 2, additional_fee: 350.0, number_of_returns: 4 }
      expect(subject.fee_details).to eq(fee_masple)
    end
  end

  context 'palv canadian returns' do
    subject(:fee_information) do
      NewProductFeeInformation.new(promise_purchase_7)
    end

    before do
      create_product_fees
    end

    it 'should return 3 returns for group within NewProductKind::TIMEFRAME_365 days' do
      expect(subject.get_number_of_returns_within_timeframe(NewProductKind::TIMEFRAME_365)).to eq(2)
    end

    it 'should have fee details' do
      fee_masple = { count: 3, fee: 0, timeframe: NewProductKind::TIMEFRAME_365, index: 0, additional_returns: 1, additional_fee: 200.0, number_of_returns: 2 }
      expect(subject.fee_details).to eq(fee_masple)
    end
  end
end
