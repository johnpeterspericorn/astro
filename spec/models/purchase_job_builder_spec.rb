# encoding: utf-8
require 'spec_helper'
require 'bsc_charges/client'

describe PurchaseJobBuilder do
  describe '#enqueue_job' do
    let(:pawn_location) { FactoryGirl.create(:pawn_location, initials: 'ABCD') }
    let(:pawn_information) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location) }
    let(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information) }
    let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }

    before do
      allow(Astroheim::Sows::Client).to receive(:purchase).and_return(true)
      allow(BscCharges::Client).to receive(:purchase).and_return(true)
    end

    it 'enqueues SowsPurchaseJob if sows_enabled and does not belong to BSC' do
      Settings.sows_enabled = true
      allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(true)

      expect(SowsPurchaseJob).to receive(:enqueue).with(promise_offer)
      PurchaseJobBuilder.enqueue_job(promise_offer)
    end

    it 'enqueues BscPurchaseJob if bsc is enabled for the pawn location' do
      Settings.bsc_enabled = true
      Settings.bsc_locations = ['ABCD']

      expect(BscPurchaseJob).to receive(:enqueue).with(promise_offer)
      PurchaseJobBuilder.enqueue_job(promise_offer)
    end

    it 'enqueues G2gwsOrdersApiPurchaseJob if g2gws_orders_api_enabled and does not belong to BSC' do
      Settings.g2gws_orders_api_enabled = true
      allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(false)
      allow_any_instance_of(PromiseOffer).to receive(:g2gws_eligible?).and_return(true)

      expect(G2gwsOrdersApiPurchaseJob).to receive(:enqueue).with(promise_offer)
      PurchaseJobBuilder.enqueue_job(promise_offer)
    end

    it 'does\'t enqueue SowsPurchaseJob if the location is BSC and bsc_enabled is false' do
      Settings.sows_enabled = true
      Settings.bsc_enabled = false
      Settings.bsc_locations = ['ABCD']
      allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(true)

      expect(SowsPurchaseJob).not_to receive(:enqueue).with(promise_offer)
      PurchaseJobBuilder.enqueue_job(promise_offer)
    end

    context 'bypass charge' do
      before do
        promise_offer.update_attributes(bypass_charge: true)
      end

      it 'doesn\'t enqueue SowsPurchaseJob if bypass_charge is true' do
        Settings.sows_enabled = true
        allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(true)

        expect(SowsPurchaseJob).not_to receive(:enqueue).with(promise_offer)
        PurchaseJobBuilder.enqueue_job(promise_offer)
      end

      it 'doesn\'t enqueue BscPurchaseJob if bypass_charge is true' do
        Settings.bsc_enabled = true
        Settings.bsc_locations = ['ABCD']

        expect(BscPurchaseJob).not_to receive(:enqueue).with(promise_offer)
        PurchaseJobBuilder.enqueue_job(promise_offer)
      end
    end
  end
end
