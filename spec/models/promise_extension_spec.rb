# encoding: utf-8
require 'spec_helper'

describe PromiseExtension do
  it 'should be invalid without a VNUM' do
    extension = FactoryGirl.build(:promise_extension, vnum: nil)
    expect(extension.valid?).to be(false)
  end
end
