# encoding: utf-8
require 'spec_helper'

describe PurchaseQuery do
  describe '#eligible_for_inspection?' do
    it 'is delegates to the offer' do
      offer = double :offer

      allow(offer).to receive(:reload) { offer }

      query = described_class.new(nil, {}, offer)

      expect(offer).to receive(:eligible_for_inspection?)

      query.eligible_for_inspection?
    end
  end

  describe '#inspection_selected?' do
    it 'is true when the "inspection" option is exactly the string "true"' do
      query = described_class.new(nil, inspection: 'true')

      expect(query.inspection_selected?).to eq(true)
    end

    it 'is false otherwise' do
      query = described_class.new(nil, inspection: 'True')

      expect(query.inspection_selected?).to eq(false)
    end
  end

  describe '#inspection_only?' do
    it 'is true when the "promise_options" option is exactly the string "inspection_only"' do
      query = described_class.new(nil, promise_options: 'inspection_only')

      expect(query.inspection_only?).to eq(true)
    end

    it 'is false otherwise' do
      query = described_class.new(nil, promise_options: 'Inspection Only')

      expect(query.inspection_only?).to eq(false)
    end
  end

  describe '#inspection_purchase' do
    let(:offer) { double :offer }

    before do
      allow(offer).to receive(:purchased?)
    end

    context 'offer is purchased' do
      before do
        allow(offer).to receive(:purchased?) { true }
      end

      subject(:query) { described_class.new(nil, {}, offer) }

      it 'builds inspection purchase with offers inspection_bundle_price' do
        bundle_price = double :price

        allow(offer).to receive(:inspection_bundle_price) { bundle_price }

        expect(offer).to receive(:build_inspection_purchase)
          .with(price: bundle_price)

        query.inspection_purchase
      end
    end

    context 'offer is not purchased' do
      context 'promise is selected' do
        before do
          allow(offer).to receive(:selected?) { true }
        end

        context 'selection is inspection only' do
          before do
            allow(offer).to receive(:inspection_only?) { true }
          end

          subject(:query) { described_class.new(nil, { promise_options: 'inspection_only' }, offer) }

          it 'builds inspection purchase with offers inspection_only_price' do
            inspection_price = double :price

            allow(offer).to receive(:inspection_only_price) { inspection_price }

            expect(offer).to receive(:build_inspection_purchase)
              .with(price: inspection_price)

            query.inspection_purchase
          end
        end

        context 'selection is not inspection only' do
          subject(:query) { described_class.new(nil, { promise_options: 'YA' }, offer) }

          it 'builds inspection purchase with offers inspection_bundle_price' do
            bundle_price = double :price

            allow(offer).to receive(:inspection_bundle_price) { bundle_price }

            expect(offer).to receive(:build_inspection_purchase)
              .with(price: bundle_price)

            query.inspection_purchase
          end
        end
      end

      context 'promise is not selected' do
        subject(:query) { described_class.new(nil, { promise_options: 'none' }, offer) }

        it 'builds inspection purchase with offers inspection_only_price' do
          inspection_price = double :price

          allow(offer).to receive(:inspection_only_price) { inspection_price }

          expect(offer).to receive(:build_inspection_purchase)
            .with(price: inspection_price)

          query.inspection_purchase
        end
      end
    end
  end

  describe '#promise_selected?' do
    it 'is false when the "promise_options" option is exactly the string "none"' do
      query = described_class.new(nil, promise_options: 'none')

      expect(query.promise_selected?).to eq(false)
    end

    it 'is true otherwise' do
      query = described_class.new(nil, promise_options: 'None')

      expect(query.promise_selected?).to eq(true)
    end
  end

  describe '#miles' do
    it 'is the first part of the "promise_options" option split on underlines' do
      miles = '123'
      query = described_class.new(nil, promise_options: miles.to_s)

      expect(query.miles).to eq(miles)
    end
  end

  describe '#days' do
    it 'is the last part of the "promise_options" option split on underlines' do
      days = '321'
      query = described_class.new(nil, promise_options: "wow_#{days}")

      expect(query.days).to eq(days)
    end
  end
end
