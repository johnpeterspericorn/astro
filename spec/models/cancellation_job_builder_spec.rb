# encoding: utf-8
require 'spec_helper'
require 'bsc_charges/client'

describe CancellationJobBuilder do
  describe '#enqueue_job' do
    let(:pawn_location) { FactoryGirl.create(:pawn_location, initials: 'ABCD') }
    let(:pawn_information) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location) }
    let(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information) }
    let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }

    before do
      allow(Astroheim::Sows::Client).to receive(:cancel).and_return(true)
      allow(BscCharges::Client).to receive(:cancel).and_return(true)
      allow(promise_offer).to receive(:charge_present_in_ods?).and_return(true)
    end

    it 'enqueues SowsCancellationJob if sows_enabled and pawn location does not belong to BSC' do
      Settings.sows_enabled = true
      allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(true)

      expect(SowsCancellationJob).to receive(:enqueue).with(promise_offer)
      CancellationJobBuilder.enqueue_job(promise_offer)
    end

    it 'enqueues G2gwsOrdersApiCancellationJob if g2gws_enabled and pawn location does not belong to BSC' do
      Settings.g2gws_orders_api_enabled = true
      allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(false)
      allow_any_instance_of(PromiseOffer).to receive(:g2gws_eligible?).and_return(true)

      expect(G2gwsOrdersApiCancellationJob).to receive(:enqueue).with(promise_offer)
      CancellationJobBuilder.enqueue_job(promise_offer)
    end

    it 'enqueues BscCancellationJob if bsc is enabled for the pawn location' do
      Settings.bsc_enabled = true
      Settings.bsc_locations = ['ABCD']

      expect(BscCancellationJob).to receive(:enqueue).with(promise_offer)
      CancellationJobBuilder.enqueue_job(promise_offer)
    end

    context 'bypass charge' do
      before do
        promise_offer.update_attributes(bypass_charge: true)
      end

      it 'doesn\'t enqueue SowsCancellationJob if bypass_charge is true' do
        Settings.sows_enabled = true
        allow_any_instance_of(PromiseOffer).to receive(:sows_eligible?).and_return(true)

        expect(SowsCancellationJob).not_to receive(:enqueue).with(promise_offer)
        CancellationJobBuilder.enqueue_job(promise_offer)
      end

      it 'doesn\'t enqueue BscCancellationJob if bypass_charge is true' do
        Settings.bsc_enabled = true
        Settings.bsc_locations = ['ABCD']

        expect(BscCancellationJob).not_to receive(:enqueue).with(promise_offer)
        CancellationJobBuilder.enqueue_job(promise_offer)
      end
    end
  end
end
