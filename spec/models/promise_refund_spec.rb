# encoding: utf-8
require 'spec_helper'

describe PromiseRefund do
  let(:promise_offer_decorator) { double('PromiseOfferDecorator').as_null_object }
  let(:cancelled_purchase)        { double('CancelledPurchase').as_null_object }

  subject(:charge) { described_class.new(promise_offer_decorator, cancelled_purchase) }

  describe '#attributes' do
    it 'includes negative amount' do
      amount = 1
      allow(promise_offer_decorator).to receive(:rounded_promise_price).and_return(amount)

      expect(charge.attributes).to include(amount: -amount)
    end
  end
end
