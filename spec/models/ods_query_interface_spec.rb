# encoding: utf-8
require 'spec_helper'
require 'ods/client'

describe OdsQueryInterface do
  describe '#left_lot?' do
    let(:location_initials) { double :location_initials }
    let(:agreement_no) { double :agreement_no }
    let(:universal_no) { double :universal_no }
    let(:sale_no) { double :sale_no }
    let(:lane_no) { double :lane_no }
    let(:run_no) { double :run_no }

    subject(:query_interface) { described_class.new(agreement_no, location_initials, universal_no, sale_no, lane_no, run_no) }

    before do
      allow(query_interface).to receive(:query_ods_service?) { true }
      allow(query_interface).to receive(:valid_ods_query) { { left_lot: true } }
      allow(universal_no).to receive(:[]).with(any_args).and_return('asdf')
    end

    context 'missing location_initials' do
      let(:location_initials) { nil }

      it 'is not left_lot' do
        expect(query_interface.vehicle_paid_by_floor_plan?).to be_falsey
      end
    end

    it 'delegates to the ods client' do
      expect(query_interface.vehicle_has_left_lot?).to eq(true)
    end

    it 'is not left lot' do
      allow_any_instance_of(Ods::Client).to receive(:left_lot?).and_raise(Errno::ETIMEDOUT)
      expect(query_interface.left_lot?).to be_falsey
    end

    it 'will return false if timed out for agreementshield_charged?' do
      allow_any_instance_of(Ods::Client).to receive(:request_vehicle_info).and_raise(Errno::ETIMEDOUT)
      expect(query_interface.agreementshield_charged?).to be_falsey
    end

    it 'is not paid by floor plan when ods client connection is reset' do
      allow_any_instance_of(Ods::Client).to receive(:left_lot?).and_raise(Errno::ECONNRESET)
      expect(query_interface.left_lot?).to be_falsey
    end
  end
end
