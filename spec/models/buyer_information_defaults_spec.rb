# encoding: utf-8
require 'spec_helper'

describe BuyerInformationDefaults do
  let(:auth_service) { double :auth_service }

  before do
    allow(auth_service).to receive(:new).and_return(auth_service)
  end

  describe 'attributes from auth service' do
    context 'without pawn_access_no set' do
      subject(:defaults) { described_class.new(nil, nil, auth_service) }

      specify '#name is nil' do
        expect(defaults.name).to be_nil
      end

      specify '#agreementship_name is nil' do
        expect(defaults.agreementship_name).to be_nil
      end

      specify '#address_street is nil' do
        expect(defaults.address_street).to be_nil
      end
    end

    context 'with pawn_access_no set' do
      let(:agreement_number) { 123 }
      let(:name)   { double :name    }
      let(:street) { double :street  }
      let(:address) { OpenStruct.new(line1: street) }

      subject(:defaults) { described_class.new(0, agreement_number, auth_service) }

      specify '#name is retrieved from auth' do
        allow(auth_service).to receive(:get_agreement_name).and_return(name)
        expect(defaults.name).to eq(name)
      end

      specify '#agreementship_name is retrieved from auth' do
        expect(auth_service)
          .to receive(:get_agreementship_name_by_agreement_number)
          .with(agreement_number.to_s)
          .and_return(name)

        expect(defaults.agreementship_name).to eq(name)
      end

      specify '#address_street is retrieved from auth' do
        allow(auth_service).to receive(:get_agreementship_addr).and_return(address)
        expect(defaults.address_street).to eq(street)
      end
    end
  end
end
