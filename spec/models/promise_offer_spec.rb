# encoding: utf-8
require 'requests_helper'

describe PromiseOffer do
  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:agreement_number)         { SasDataHelper::DEFAULT_DEALER_NUMBER         }
  let(:badge_number)          { SasDataHelper::DEFAULT_BADGE_NUMBER          }

  describe 'scopes' do
    let(:badge_no) { '42' }
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', badge_no: badge_no) }
    let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', badge_no: badge_no) }

    before do
      PromiseOfferRecorder.record_offers [honda]
    end

    describe '.by_vnum_and_universal_no' do
      it { expect(described_class.by_vnum_and_universal_no(honda).count).to eq(1) }
    end

    describe '.promised' do
      it 'should find promised offers' do
        described_class.create!(promise_status: :promised)
        expect(described_class.count).to eq(2)
        expect(described_class.promised.count).to eq(1)
      end
    end

    describe '.for_agreement' do
      let(:pawn_access_number) { '100686866' }
      let(:wrong_pawn_access_number) { '100000000' }
      let(:agreement_number) { '5432100' }
      let(:wrong_agreement_number) { '5000000' }

      let(:correct_pawn_information) { FactoryGirl.create(:pawn_information, pawn_access_no: pawn_access_number, agreement_no: agreement_number) }
      let!(:pawn_access_offer) { FactoryGirl.create(:promise_offer, pawn_information: correct_pawn_information) }

      let(:correct_agreement_information) { FactoryGirl.create(:pawn_information, pawn_access_no: wrong_pawn_access_number, agreement_no: agreement_number) }
      let!(:agreement_offer) { FactoryGirl.create(:promise_offer, pawn_information: correct_agreement_information) }

      let(:wrong_pawn_information) { FactoryGirl.create(:pawn_information, pawn_access_no: wrong_pawn_access_number, agreement_no: wrong_agreement_number) }
      let!(:wrong_agreement_offer) { FactoryGirl.create(:promise_offer, pawn_information: wrong_pawn_information) }

      let(:buyer_authorization) { double(get_agreement_numbers: [agreement_number]) }

      subject(:scope) { PromiseOffer.for_agreement(pawn_access_number) }

      before do
        expect(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:new) { buyer_authorization }
      end

      it 'returns offers for the given pawn access number' do
        expect(scope).to include pawn_access_offer
      end

      it "returns offers for the AA#'s associated agreement numbers" do
        expect(scope).to include agreement_offer
      end

      it 'does not return unassociated offers' do
        expect(scope).not_to include wrong_agreement_offer
      end
    end

    describe '.for_pawn_access_number' do
      let(:promise_offer_with_correct_pawn_access_number)                           { described_class.create! }
      let(:promise_offer_with_zero_pawn_access_number_and_correct_agreement_number)    { described_class.create! }
      let(:promise_offer_with_nonzero_pawn_access_number_and_correct_agreement_number) { described_class.create! }
      let(:filter_query_data) { FilterQueryData.new(pawn_access_number: pawn_access_number, agreement_numbers: [agreement_number]) }

      before do
        promise_offer_with_correct_pawn_access_number.create_pawn_information!(FactoryGirl.attributes_for(:pawn_information, pawn_access_no: pawn_access_number))
        promise_offer_with_zero_pawn_access_number_and_correct_agreement_number.create_pawn_information!(FactoryGirl.attributes_for(:pawn_information, pawn_access_no: '0', agreement_no: agreement_number))
        promise_offer_with_nonzero_pawn_access_number_and_correct_agreement_number.create_pawn_information!(FactoryGirl.attributes_for(:pawn_information, pawn_access_no: '-1', agreement_no: agreement_number))
      end

      subject(:offers_for_pawn_access_number) { described_class.joins(:pawn_information).for_pawn_access_number(filter_query_data) }

      it { is_expected.to include promise_offer_with_correct_pawn_access_number }
      it { is_expected.to include promise_offer_with_zero_pawn_access_number_and_correct_agreement_number }
      it { is_expected.not_to include promise_offer_with_nonzero_pawn_access_number_and_correct_agreement_number }
    end
  end

  context 'scopes without existing data' do
    describe 'purchasing scopes' do
      before do
        described_class.create!(ineligibility_condition: 0, left_lot: false)
        offer = described_class.create!(ineligibility_condition: 0, left_lot: false)
        FactoryGirl.create(:promise_purchase, promise_offer: offer)
      end

      it { expect(described_class.with_purchase.count).to    eq(1) }
      it { expect(described_class.without_purchase.count).to eq(1) }
    end
  end

  describe '#eligible_for_inspection?' do
    let(:pawn_location)    { PawnLocation.new(inspections_enabled: true) }
    let(:pawn_information) { PawnInformation.new(pawn_location: pawn_location) }
    subject(:offer) { described_class.new(pawn_information: pawn_information) }

    it 'is eligible if it has no promise or inspection purchase' do
      expect(offer.eligible_for_inspection?).to be_truthy
    end

    it 'is ineligible if a promise has been purchased' do
      offer.build_promise_purchase
      expect(offer.eligible_for_inspection?).to be_falsey
    end

    it 'is ineligible if an inspection has been purchased' do
      offer.build_inspection_purchase
      expect(offer.eligible_for_inspection?).to be_falsey
    end

    it 'is ineligible if a location does not have inspections enabled' do
      expect(pawn_location).to receive(:inspections_enabled?).and_return(false)
      expect(offer.eligible_for_inspection?).to be_falsey
    end
  end

  describe '#has_inspection_pricing' do
    let(:inspection_information_params) { { inspection_only_price: 123.0, inspection_bundle_price: 53.0 } }
    let(:inspection_information) { InspectionInformation.new(inspection_information_params) }
    subject { described_class.new(inspection_information: inspection_information) }

    it 'should be false without inspection information' do
      subject.inspection_information = nil
      expect(subject.has_inspection_pricing?).to be_falsey
    end

    it 'should be false without an inspection only price' do
      subject.inspection_information.inspection_only_price = nil
      expect(subject.has_inspection_pricing?).to be_falsey
    end

    it 'should be false without a bundled inspection price' do
      subject.inspection_information.inspection_bundle_price = nil
      expect(subject.has_inspection_pricing?).to be_falsey
    end

    it 'should be true with inspection information containing both prices' do
      expect(subject.has_inspection_pricing?).to be_truthy
    end
  end

  describe 'ineligibility due to invalidity' do
    let(:invalid_condition) { IneligibilityCondition::INVALID_PRICING }

    subject(:offer) { FactoryGirl.build :promise_offer }

    it 'is marked ineligible when saved without promise_250 price' do
      offer.promise_250 = nil
      expect(offer.ineligibility_condition).to_not eq(invalid_condition)
      offer.save!
      expect(offer.ineligibility_condition).to eq(invalid_condition)
    end

    it 'is marked ineligible when saved without promise_250 price' do
      offer.promise_500 = nil
      expect(offer.ineligibility_condition).to_not eq(invalid_condition)
      offer.save!
      expect(offer.ineligibility_condition).to eq(invalid_condition)
    end

    it 'is marked ineligible when saved without promise_250 price' do
      offer.additional_day = nil
      expect(offer.ineligibility_condition).to_not eq(invalid_condition)
      offer.save!
      expect(offer.ineligibility_condition).to eq(invalid_condition)
    end

    it 'invalid pricing does not override existing ineligibility condition' do
      condition = 1
      offer.ineligibility_condition = condition
      offer.promise_250 = nil
      offer.save!
      expect(offer.ineligibility_condition).to eq(condition)
    end

    it 'is eligible when all prices are set' do
      offer.save!
      expect(offer).to be_eligible
    end
  end

  describe '#eligible_for_purchase?' do
    let(:eligibility_conditions) { { ineligibility_condition: 0, left_lot: false, must_be_promised_at: 5.days.from_now } }
    specify { expect(PromiseOffer.new(eligibility_conditions.merge(ineligibility_condition: 1))).not_to be_eligible_for_purchase }
    specify { expect(PromiseOffer.new(eligibility_conditions.merge(left_lot: true))).not_to be_eligible_for_purchase }
    specify { expect(PromiseOffer.new(eligibility_conditions.merge(must_be_promised_at: 5.days.ago))).not_to be_eligible_for_purchase }
    specify { expect(PromiseOffer.new(eligibility_conditions)).to be_eligible_for_purchase }
  end

  describe '#rounded_promise_price' do
    subject(:offer) { described_class.new }

    it 'is nil if price is not set' do
      expect(offer.rounded_promise_price).to be_nil
    end

    it 'rounds up' do
      offer.promise_price = 123.5
      expect(offer.rounded_promise_price).to eq(124)
    end

    it 'rounds down' do
      offer.promise_price = 123.4
      expect(offer.rounded_promise_price).to eq(123)
    end
  end

  describe '#update_ods_attributes' do
    subject(:offer) { FactoryGirl.create :promise_offer }

    before do
      Settings.query_ods = true
    end

    it 'queries ods for left lot and floor plan statuses' do
      expect_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(false)
      expect_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(false)

      offer.update_ods_attributes
    end

    it 'does not query ods again when it is paid by floor plan' do
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(false)
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(false)

      offer.paid_by_floor_plan = true
      offer.update_ods_attributes

      # The value should remain unchanged because TRAINS is not called again
      expect(offer.paid_by_floor_plan).to be_truthy
    end

    it 'does not query ods again when it has left the lot' do
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(false)
      allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(false)

      offer.left_lot = true
      offer.update_ods_attributes

      # The value should remain unchanged because TRAINS is not called again
      expect(offer.left_lot).to be_truthy
    end
  end

  describe '#sows_eligible?' do
    subject(:offer) { described_class.new }

    it 'is not eligible when offer\'s pawn location is converted' do
      allow(offer).to receive(:pawn_location_g2g_converted).and_return(true)

      expect(offer.sows_eligible?).to be_falsey
    end

    it 'is not eligible when offer is pending' do
      offer.promise_status = :pending

      expect(offer.sows_eligible?).to be_falsey
    end

    it 'is not eligible when sblu is missing' do
      expect(offer.sblu).not_to be_present
      expect(offer.sows_eligible?).to be_falsey
    end

    it 'is eligible when offer is not pending, sblu is present and pawn location is not converted' do
      offer.pawn_information = PawnInformation.new(sblu: 'here')
      allow(offer).to receive(:pawn_location_g2g_converted).and_return(false)

      expect(offer).not_to be_pending
      expect(offer.sows_eligible?).to be_truthy
    end
  end

  describe '#g2gws_eligible?' do
    subject(:offer) { described_class.new }

    it 'is not eligible when offer\'s pawn location is not converted' do
      allow(offer).to receive(:pawn_location_g2g_converted).and_return(false)

      expect(offer.g2gws_eligible?).to be_falsey
    end

    it 'is eligible when offer is not pending and pawn location is coverted' do
      allow(offer).to receive(:pawn_location_g2g_converted).and_return(true)

      expect(offer).not_to be_pending
      expect(offer.g2gws_eligible?).to be_truthy
    end
  end

  describe '#fees' do
    let(:offer) { described_class.new }

    before do
      allow_any_instance_of(Sas::VehicleAdjustmentsRequest).to receive(:load)
    end

    it 'delegates to adjustments when they are present' do
      adjustments = double

      allow_any_instance_of(Sas::VehicleAdjustmentsRequest).to receive(:adjustments)
        .and_return(adjustments)

      expect(adjustments).to receive(:fees)

      offer.fees
    end

    it 'returns empty fees when adjustments are not available' do
      allow_any_instance_of(Sas::VehicleAdjustmentsRequest).to receive(:adjustments)
        .and_return(nil)

      expect(offer.fees.count).to eq(0)
    end
  end

  describe '#paid_by_buyer?' do
    subject { described_class.new }
    let(:adjustments) { double }

    before(:each) do
      allow_any_instance_of(PromiseOffer).to receive(:adjustments_request)
        .and_return(Sas::VehicleAdjustmentsRequest.new('vnum', 'agreement-no'))

      allow_any_instance_of(Sas::VehicleAdjustmentsRequest).to receive(:adjustments)
        .and_return(adjustments)
    end

    it 'delegates to adjustments when they are present' do
      expect(adjustments).to receive(:paid_by_buyer?)
      subject.paid_by_buyer?
    end
  end

  describe 'eligible flat rates' do
    let(:flat_rate_canada) { FactoryGirl.create(:flat_rate_canada) }
    let(:flat_rate_offer_canada) { FactoryGirl.create(:flat_rate_offer, price: 3210.0, country: 'Canada', flat_rate: flat_rate_canada) }
    let!(:offer) { FactoryGirl.create(:promise_offer) }

    before do
      allow(offer).to receive(:flat_rate_offers).and_return([flat_rate_offer_canada])
    end

    it 'should check country for eligible flat rates for agreement' do
      expect(offer.country).to eq(SupportedCountry::DEFAULT)
      expect(offer.valid_flat_rate_offer?).to be false
    end
  end

  describe '#value_protected' do
    let(:pawn_info_with_buy_fee) { FactoryGirl.create(:pawn_information, buy_fee: 230.0) }
    let(:offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_info_with_buy_fee, percent_coverage: 50.0) }
    let(:fully_coverd_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_info_with_buy_fee) }

    it 'takes into account the percentage coverage defined in the offer' do
      covered_value = 230.0 + offer.vehicle_purchase_price.to_f * (50 / 100.0)
      expect(offer.value_protected).to eq(covered_value)
    end

    it 'gives the covered value with buy fee' do
      covered_value = 230.0 + offer.vehicle_purchase_price.to_f
      expect(fully_coverd_offer.value_protected).to eq(covered_value)
    end
  end

  describe '#discounted?' do
    let(:offer) { described_class.new }

    describe 'when discounts are enabled' do
      before do
        Settings.discounts_enabled = true
      end

      it 'is discounted when the discount amount is greater than 0' do
        offer.build_discount(amount: 1)
        expect(offer.discounted?).to eq(true)
      end

      it 'is not discounted when discount amount is 0' do
        offer.build_discount(amount: 0)
        expect(offer.discounted?).to eq(false)
      end

      describe 'discounted promise price' do
        it 'returns discounted promise price if discount available' do
          offer.promise_price = 123.5
          offer.build_discount(amount: 1)
          expect(offer.discounted_promise_price).to eq(122.5)
        end

        it 'is nil if price is not set' do
          expect(offer.discounted_promise_price).to be_nil
        end

        it 'returns promise price if no discount available' do
          offer.promise_price = 123.5
          expect(offer.discounted_promise_price).to eq(123.5)
        end

        it 'rounds up' do
          offer.promise_price = 99.5
          expect(offer.rounded_discounted_price).to eq(100)
        end

        it 'rounds down' do
          offer.promise_price = 99.4
          expect(offer.rounded_discounted_price).to eq(99)
        end

        it 'does not return a negative discount' do
          offer.promise_price = 123.5
          offer.build_discount(amount: 133.5)

          expect(offer.discounted_promise_price).to eq(0)
        end
      end
    end
  end
end
