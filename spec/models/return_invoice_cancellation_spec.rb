# encoding: utf-8
require 'spec_helper'

describe ReturnInvoiceCancellation do
  let(:return_invoice)     { FactoryGirl.create(:return_invoice, expiration_date: Time.current) }
  let(:mail)               { double(:mail).as_null_object }
  let(:vnum)                { SecureRandom.hex[0..16].upcase }
  let(:cancelling_user)    { FactoryGirl.create(:super_user) }
  let(:rand_id)            { 'ABC123' }
  let(:an_email)           { 'someone@someplace.com' }

  before do
    allow(ReturnInvoiceMailer).to receive(:cancelled_and_ineligible_for_return).and_return(mail)
    allow(ReturnInvoiceMailer).to receive(:cancelled_and_eligible_for_return).and_return(mail)
    allow(ReturnInvoiceMailer).to receive(:cancellation_notification_to_accountant).and_return(mail)

    allow(DeleteDropNoticeJob).to receive(:enqueue)
    allow(AddInternetNoteJob).to receive(:enqueue)
  end

  subject(:cancellation) { described_class.new(return_invoice: return_invoice, reason: 'a valid reason', user: cancelling_user) }

  describe '#process' do
    it 'returns true if valid' do
      expect(cancellation.process).to be_truthy
    end

    it 'returns false if invalid' do
      return_invoice.promise_purchase = nil
      expect(cancellation.process).to be_falsey
    end

    it 'sends an email if user checks the email checkbox though cancellation steps succeed' do
      cancellation.send_email = true
      expect(ReturnInvoiceMailer).to receive(:cancelled_and_eligible_for_return).and_return(mail)
      cancellation.process
    end

    it 'sends an email to udpated email and support team if user checks the email checkbox' do
      cancellation.send_email = true
      cancellation.user_email = an_email
      expect_any_instance_of(ReturnInvoice).to receive(:update_user_email).with(an_email)
      expect(ReturnInvoiceMailer).to receive(:cancelled_and_eligible_for_return).and_return(mail)
      cancellation.process
    end

    it 'sends an email to support team only if user does not check the email checkbox though cancellation steps succeed' do
      cancellation.send_email = false
      expect_any_instance_of(ReturnInvoice).to_not receive(:update_user_email)
      expect(ReturnInvoiceMailer).to receive(:cancelled_and_eligible_for_return)
      cancellation.process
    end

    it 'does not send an email if the cancellation steps fail' do
      expect(cancellation.return_invoice).to receive(:archive).and_raise(ActiveRecord::Rollback)
      expect(ReturnInvoiceMailer).not_to receive(:cancelled_and_eligible_for_return)
      cancellation.process
    end

    it 'cancels a return' do
      cancellation.process
      expect(return_invoice.reload.cancelling_user).to eq(cancellation.user)
      expect(return_invoice.reload).to be_cancelled
    end

    it 'enqueues a job to delete Autoims drop notice' do
      Settings.return_cancellation_delete_drop_notice = true
      expect(DeleteDropNoticeJob).to receive(:enqueue)
      cancellation.process
    end

    it 'enqueues a job to add internet note' do
      Settings.salvage_internet_notes_enabled = true
      expect(AddInternetNoteJob).to receive(:enqueue)
      cancellation.process
    end

    it 'sends an email if VRA is cancelled on a PPLV where a return fee was assessed' do
      return_invoice = FactoryGirl.create(:return_invoice, new_product_fee: 100)
      cancellation = described_class.new(return_invoice: return_invoice, reason: 'a valid reason', user: cancelling_user)

      expect(ReturnInvoiceMailer).to receive(:cancellation_notification_to_accountant).with(return_invoice)
      cancellation.process
    end

    it 'does not sends an email if VRA is cancelled on a PPLV where a return fee was not assessed' do
      expect(ReturnInvoiceMailer).not_to receive(:cancellation_notification_to_accountant).with(return_invoice)
      cancellation.process
    end
  end

  describe '#reinstate' do
    it 'reinstates a return' do
      cancellation.process
      expect(return_invoice.reload.cancelling_user).to eq(cancellation.user)
      expect(return_invoice.reload).to be_cancelled
      cancellation.reinstate
      expect(return_invoice.reload.cancelling_user).to be_nil
      expect(return_invoice.reload).not_to be_cancelled
    end

    it 'enqueues a job to create Autoims drop notice' do
      expect(CreateDropNoticeJob).to receive(:enqueue)
      cancellation.reinstate
    end

    it 'enqueues a job to add internet note' do
      Settings.salvage_internet_notes_enabled = true
      expect(AddInternetNoteJob).to receive(:enqueue)
      cancellation.reinstate
    end
  end

  describe 'expiration date for return is updated to 3 business days in the future' do
    before do
      allow_any_instance_of(ReturnInvoice).to receive(:vehicle_must_be_eligible).and_return(true)
    end

    it 'when today is a thursday, expiration is updated to the following tuesday' do
      thursday = Date.new(2018, 4, 2)
      Timecop.freeze(thursday) do
        return_invoice.expiration_date = Time.current
        tuesday = Time.current.next_week(:tuesday).to_date
        cancellation.reinstate
        expect(return_invoice.reload.expiration_date).to eq(tuesday)
      end
    end

    it 'when today is a friday, expiration is updated to the following wednesday' do
      friday = Date.new(2018, 4, 10)
      Timecop.freeze(friday) do
        wednesday = Time.current.next_week(:wednesday).to_date
        return_invoice.expiration_date = Time.current
        cancellation.reinstate
        expect(return_invoice.reload.expiration_date).to eq(wednesday)
      end
    end

    it 'when today is a monday, expiration is updated to the following thursday' do
      monday = Date.new(2018, 4, 13)
      Timecop.freeze(monday) do
        thursday = monday + 3.days
        return_invoice.expiration_date = Time.current
        cancellation.reinstate
        expect(return_invoice.reload.expiration_date).to eq(thursday)
      end
    end

    it 'expires normally since default date is ahead of calculated expiration date' do
      default_expiration_date = 6.days.from_now.to_date
      return_invoice.expiration_date = default_expiration_date
      cancellation.reinstate
      expect(return_invoice.reload.expiration_date).to eq(default_expiration_date)
    end
  end
end
