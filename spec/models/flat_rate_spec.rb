# encoding: utf-8
require 'spec_helper'

describe FlatRate do
  describe '#eligible?' do
    subject(:flat_rate) { FlatRate.new }

    it 'is eligible when ineligiblity condition is missing' do
      flat_rate.ineligibility_condition = nil

      expect(flat_rate).to be_eligible
    end

    it 'is not eligible when there is an ineligibility condition' do
      flat_rate.ineligibility_condition = 1

      expect(flat_rate).to_not be_eligible
    end
  end
end
