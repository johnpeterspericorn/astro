# encoding: utf-8
require 'spec_helper'

describe Failover do
  subject { described_class }
  describe '.current' do
    context 'when failover is active' do
      before do
        allow(subject).to receive(:active?) { true }
      end

      it 'chooses the record with latest `began_at`' do
        [1.day.ago, Time.current, 2.days.ago].each do |time|
          subject.create!(began_at: time)
        end

        expect(subject.current.began_at).to be_within(1.second).of Time.current
      end
    end

    context 'when failover is not active' do
      before do
        allow(subject).to receive(:active?) { false }
      end

      it 'is nil' do
        expect(subject.current).to be_nil
      end
    end
  end
end
