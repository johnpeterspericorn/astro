# encoding: utf-8
require 'spec_helper'

describe PromisePurchase do
  let(:sold_vehicle) { FactoryGirl.create(:sold_vehicle) }
  let(:miles_selected)  { 500 }
  let(:additional_days) { 14 }
  let(:promise_offer) { FactoryGirl.build(:promise_offer, left_lot: true) }

  subject { described_class.from_options(sold_vehicle.vnum, miles_selected, additional_days) }
  before do
    relation = double
    allow_any_instance_of(EligiblePromiseOfferQuery).to receive(:by_user) { relation }
    allow(relation).to receive(:order) { [promise_offer] }
    PromiseOffer.stub_chain(:by_vnum).and_return(promise_offer)
  end

  describe 'scopes' do
    let (:unbundled_promise_offer)    { PromiseOffer.create! }
    let!(:unbundled_promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: unbundled_promise_offer) }

    let(:bundled_promise_offer) { PromiseOffer.create! }
    let!(:inspection_purchase)        { bundled_promise_offer.create_inspection_purchase! }
    let!(:bundled_promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: bundled_promise_offer) }

    describe '.unbundled_for_transaction_id' do
      before { Settings.inspections_enabled = true }
      specify { expect(described_class.unbundled.count).to eq(1) }
      specify { expect(described_class.unbundled.first.id).to eq(unbundled_promise_purchase.id) }
    end

    describe '.bundled_for_transaction_id' do
      before { Settings.inspections_enabled = true }
      specify { expect(described_class.bundled.count).to eq(1) }
      specify { expect(described_class.bundled.first.id).to eq(bundled_promise_purchase.id) }

      context 'when inspections are disabled' do
        before { Settings.inspections_enabled = false }
        specify { expect(described_class.bundled.count).to eq(0) }
      end
    end
  end

  describe '.from_options' do
    context 'without a persisted promise purchase' do
      describe '#miles_selected' do
        subject { super().miles_selected }
        it { is_expected.to eql miles_selected }
      end

      describe '#additional_days' do
        subject { super().additional_days }
        it { is_expected.to eql additional_days }
      end
    end

    context 'with a persisted promise purchase' do
      let!(:saved_purchase) { FactoryGirl.create(:promise_purchase, miles_selected: 250, additional_days: 7) }

      it 'should return the persisted promise purchase' do
        promise_offer.save
        saved_purchase.update_attributes(promise_offer: promise_offer)
        expect(subject).to eql saved_purchase
      end

      describe '#miles_selected' do
        subject { super().miles_selected }
        it { is_expected.to eql miles_selected }
      end

      describe '#additional_days' do
        subject { super().additional_days }
        it { is_expected.to eql additional_days }
      end
    end
  end

  describe '#promise_kind' do
    let(:offer) { FactoryGirl.build(:promise_offer) }
    let(:purchase) { FactoryGirl.build(:promise_purchase, promise_offer: offer) }

    it 'is a PurchaseProtectKind' do
      expect(purchase.promise_kind).to be_a(PurchaseProtectKind)
    end

    context 'when it has limited_volume' do
      let(:offer) { FactoryGirl.build(:promise_offer, limited_volume: NewProductKind::LEVEL_3) }

      it 'is a NewProductKind' do
        expect(purchase.promise_kind).to be_a(NewProductKind)
      end
    end

    context 'when it has percent_coverage' do
      let(:offer) { FactoryGirl.build(:promise_offer, percent_coverage: 95) }

      it 'is a NewProductKind' do
        expect(purchase.promise_kind).to be_a(NewProductKind)
      end
    end
  end

  describe '#eligible_for_return?' do
    it 'returns false if a return has been initiated' do
      allow(subject).to receive(:return_invoice).and_return(double(:return_invoice, cancelled?: false, persisted?: false))
      allow(subject).to receive(:promise_kind).and_return(double(:promise_kind, within_window?: false))
      expect(subject).to_not be_eligible_for_return
    end

    it 'returns false if it was purchased today' do
      subject.promised_at = Time.current
      expect(subject).to_not be_eligible_for_return
    end

    it 'returns false if the purchase has expired' do
      subject.promised_at = 100.days.ago
      expect(subject).to_not be_eligible_for_return
    end

    it 'returns true if return has not been initiated and its not expired and the vehicle has been paid for' do
      subject.promised_at = 2.days.ago
      expect(subject).to be_eligible_for_return
    end
  end

  describe '#next_day_return_eligible?' do
    it 'will be true if vehicle can be returned atleast by the next day after purchase' do
      subject.promised_at = 1.day.ago
      subject.kind = 'market_protect'
      expect(subject.next_day_return_eligible?).to be(false)
    end

    it 'will be false if vehicle cannot be returned by the next day after purchase' do
      subject.promised_at = 1.day.ago
      expect(subject.next_day_return_eligible?).to be(true)
    end
  end

  describe '#priority' do
    before do
      Settings.query_ods = true
    end

    it 'is 1 if eligible for return' do
      subject.promised_at = 2.days.ago
      expect(subject.priority).to eq(1)
    end

    it 'is 2 if not eligible because it has been returned' do
      allow(subject).to receive(:return_invoice).and_return(double(:return_invoice, cancelled?: false, persisted?: true))
      allow(subject).to receive(:promise_kind).and_return(double(:promise_kind, within_window?: true))
      expect(subject.priority).to eq(2)
    end

    it 'is 3 otherwise' do
      subject.promised_at = 100.days.ago
      expect(subject.priority).to eq(3)
    end
  end

  describe '#updated_before_save?' do
    it 'should be false without a promise offer' do
      expect(described_class.new).not_to be_updated_before_save
    end

    context 'with a offer' do
      it 'should be false if the model has changed but the offer is not promised' do
        promise_purchase = described_class.new(promise_offer: promise_offer)
        allow(promise_purchase).to receive(:changed?).and_return(true)

        expect(promise_purchase).not_to be_updated_before_save
      end

      it 'should be false if the model has not changed and the offer is promised' do
        promise_offer = PromiseOffer.new(promised_at: 5.minutes.ago)
        promise_purchase = described_class.new(promise_offer: promise_offer)
        allow(promise_purchase).to receive(:changed?).and_return(false)

        expect(promise_purchase).not_to be_updated_before_save
      end

      it 'should be true if the model has changed and the offer is promised' do
        promise_offer = PromiseOffer.new(promised_at: 5.minutes.ago)
        promise_purchase = described_class.new(promise_offer: promise_offer)
        allow(promise_purchase).to receive(:changed?).and_return(true)

        expect(promise_purchase).to be_updated_before_save
      end
    end
  end

  describe '#updated_after_save?' do
    it 'should be true if the offer is updated' do
      promise_offer = PromiseOffer.new(promised_at: 5.minutes.ago, updated_at: Time.current)
      promise_purchase = described_class.new(promise_offer: promise_offer)
      expect(promise_purchase).to be_updated_after_save
    end
  end

  describe 'paid/unpaid' do
    let(:offer) { PromiseOffer.new }

    let(:purchase) { described_class.new(promise_offer: offer) }

    describe '#paid?' do
      before do
        allow(offer).to receive(:seller_paid?).and_return(false)
      end

      it 'is paid when the offer is paid' do
        allow(offer).to receive(:seller_paid?).and_return(true)

        expect(purchase).to be_paid
      end

      it 'is paid when there is a payment' do
        payment = FactoryGirl.create(:payment)
        purchase.payments = [payment]

        expect(purchase).to be_paid
      end
    end

    describe '#unpaid?' do
      it 'is unpaid when the offer is not paid and there is no payment' do
        allow(offer).to receive(:seller_paid?).and_return(false)
        purchase.payments = []

        expect(purchase).to be_unpaid
      end
    end
  end

  describe '#send_confirmation' do
    let(:mail) { double }

    subject(:purchase) { described_class.new }

    before do
      allow(mail).to receive(:deliver_later)
    end

    context 'kind is purchase protect' do
      it 'sends a purchase confirmation message' do
        purchase.kind = PromisePurchase::PURCHASE_PROTECT

        expect(PromisePurchaseMailer).to receive(:purchase_confirmation) { mail }

        purchase.send_confirmation(nil)
      end
    end

    context 'kind is total protect' do
      it 'sends a purchase confirmation message' do
        purchase.kind = PromisePurchase::TOTAL_PROTECT

        expect(PromisePurchaseMailer).to receive(:purchase_confirmation) { mail }

        purchase.send_confirmation(nil)
      end
    end

    context 'kind is extended protect' do
      it 'sends a purchase confirmation message' do
        purchase.kind = PromisePurchase::EXTENDED_PROTECT

        expect(PromisePurchaseMailer).to receive(:purchase_confirmation) { mail }

        purchase.send_confirmation(nil)
      end
    end

    context 'kind is market protect' do
      it 'sends a promise refresh confirmation message' do
        purchase.kind = PromisePurchase::MARKET_PROTECT

        expect(PromisePurchaseRefreshMailer).to receive(:refresh_confirmation) { mail }

        purchase.send_confirmation(nil)
      end
    end
  end

  describe 'promised_at callback' do
    let(:promise_offer) { FactoryGirl.create(:promise_offer, purchased_at: purchased_at) }
    let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }

    context 'offer has a purchase date' do
      let(:purchased_at) { 3.days.ago }
      it 'defaults to the promise_offer purchased_at date' do
        expect(promise_purchase.promised_at).to eq(promise_offer.purchased_at)
      end
    end

    context 'offer does not have a purchase date' do
      let(:purchased_at) { nil }

      it 'defaults to the current time' do
        now = Time.zone.local(2018, 1, 1, 12, 0, 0)
        Timecop.freeze(now) do
          expect(promise_purchase.promised_at).to eq(now)
        end
      end
    end
  end

  describe '#receive_by_date' do
    let(:offer) { PromiseOffer.new }
    let(:additional_days) { 0 }

    subject(:purchase) { described_class.new(promise_offer: offer, additional_days: additional_days) }

    before do
      Timecop.freeze
      purchase.promised_at = Time.current
      purchase.promise_offer.automatic_purchase = true
      purchase.promise_offer.seller_paid = false
    end

    after do
      Timecop.return
    end

    context 'promise offer has days selection' do
      let(:days_selected) { 14 }

      before do
        offer.days_selected = days_selected
      end

      it 'is one day after offer days selection has passed' do
        receive_by_date = days_selected.days.from_now.to_date

        expect(purchase.receive_by_date.to_date).to eq(receive_by_date.to_date)
      end
    end

    it 'is one day after base duration and additional days has passed' do
      receive_by_date = (Promise::BASE_DURATION + additional_days).days.from_now.to_date

      expect(purchase.receive_by_date.to_date).to eq(receive_by_date.to_date)
    end
  end
end
