# encoding: utf-8
require 'spec_helper'

describe CancelledReturnReport do
  let(:cancelled_return1) { double('ReturnInvoice', id: 123, cancelled_at: 2.days.ago) }
  let(:cancelled_return2) { double('ReturnInvoice', id: 456, cancelled_at: 3.days.ago) }
  let(:cancelled_returns) { [cancelled_return1, cancelled_return2] }

  it 'accepts a list of cancelled returns' do
    expect { CancelledReturnReport.new(cancelled_returns) }.not_to raise_error
  end

  describe 'instance methods' do
    subject { CancelledReturnReport.new(cancelled_returns) }
    let(:csv_file) { StringIO.new }
    let(:local_file) { '/tmp/abcd.csv' }

    before do
      allow(subject).to receive(:local_file_path).and_return(local_file)
      allow(File).to receive(:open).with(local_file, 'w', anything).and_return(csv_file)
      allow_any_instance_of(S3File).to receive(:upload).with(local_file)
      allow_any_instance_of(S3File).to receive(:presigned_url)
    end

    it 'creates and uploads a csv file with the cancelled returns' do
      expect_any_instance_of(S3File).to receive(:upload).with(local_file)
      subject.create_and_upload
      expect(csv_file.string).to include(cancelled_return1.id.to_s, cancelled_return2.id.to_s)
    end

    it 'provides a link to the presigned_url for the s3 resource' do
      expect_any_instance_of(S3File).to receive(:presigned_url)

      subject.create_and_upload
      subject.presigned_url
    end
  end
end
