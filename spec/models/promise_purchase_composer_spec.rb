# encoding: utf-8
require 'spec_helper'
require_relative '../requests_support/data_helper.rb'

describe PromisePurchaseComposer do
  subject(:composer) { described_class.new(params) }
  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let!(:promise_only_vehicle) { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no) }
  let!(:inspection_only_vehicle) { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no) }
  let!(:bundled_vehicle) { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no) }
  let!(:another_bundled_vehicle) { FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no) }
  let(:params) do
    {
      purchase_queries: [
        PurchaseQuery.new(inspection_only_vehicle.vnum, promise_options: 'inspection_only', inspection: 'true'),
        PurchaseQuery.new(promise_only_vehicle.vnum, promise_options: '250_14'),
        PurchaseQuery.new(bundled_vehicle.vnum, promise_options: '250_14', inspection: 'true'),
        PurchaseQuery.new(another_bundled_vehicle.vnum, promise_options: '250_14', inspection: 'true')
      ],
      pawn_access_number: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
    }.with_indifferent_access
  end

  before do
    create_location_data
    display_vehicle_for_offer SoldVehicle.where(pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
  end

  describe '#promise_purchases' do
    specify { expect(composer.promise_purchases.length).to eq(3) }
    specify { expect(composer.promise_purchases.map(&:promise_offer).map(&:vnum)).to match_array([promise_only_vehicle.vnum, bundled_vehicle.vnum, another_bundled_vehicle.vnum]) }
  end

  describe '#inspection_purchases' do
    specify { expect(composer.inspection_purchases.length).to eq(3) }
    specify { expect(composer.inspection_purchases.map(&:promise_offer).map(&:vnum)).to match_array([inspection_only_vehicle.vnum, bundled_vehicle.vnum, another_bundled_vehicle.vnum]) }
  end

  describe '#bundled_purchases' do
    before do
      Settings.inspections_enabled = true
    end

    subject(:composer) { described_class.new(params) }
    specify { expect(composer.bundled_purchases.length).to eq(2) }
    specify { expect(composer.bundled_purchases.map(&:promise_purchase).map(&:promise_offer).map(&:vnum)).to match_array([bundled_vehicle.vnum, another_bundled_vehicle.vnum]) }
  end
end
