# encoding: utf-8
require 'spec_helper'

describe PresaleVehicle do
  describe '.at_pawn_location' do
    let! (:pawn_location)       { PawnLocation.create!(initials: 'ABCD', name: 'Chicago Auto') }
    let! (:other_pawn_location) { PawnLocation.create!(initials: 'XXXX', name: 'Other Auto') }
    let! (:presale_vehicle)        { described_class.create!(location_initials: pawn_location.initials) }
    let! (:other_presale_vehicle)  { described_class.create!(location_initials: other_pawn_location.initials) }

    it "returns rows with the given location's initials" do
      result_ids = described_class.at_pawn_location(pawn_location.initials).pluck(:id)
      expect(result_ids).to include(presale_vehicle.id)
      expect(result_ids).not_to include(other_presale_vehicle.id)
    end
  end

  describe '.lanes_for_pawn' do
    let  (:pawn_initials) { 'ABCD' }
    let! (:pawn_location)       { PawnLocation.create!(initials: pawn_initials, name: 'Chicago Auto') }
    let! (:presale_vehicle)        { described_class.create!(location_initials: pawn_location.initials, lane_no: 1) }
    let! (:other_presale_vehicle)  { described_class.create!(location_initials: pawn_location.initials, lane_no: 1) }
    let! (:a_third_presale_vehicle) { described_class.create!(location_initials: pawn_location.initials, lane_no: 2) }

    it 'returns a list of unique lanes for a given pawn' do
      expect(described_class.lanes_for_pawn(pawn_initials)).to eq(%w(1 2))
    end
  end

  describe '.pawn_ordered' do
    it 'orders vehicles by pawn location name then lane number then run number' do
      location = FactoryGirl.create(:pawn_location)
      v1 = FactoryGirl.create(:presale_vehicle, location_initials: location.initials, vnum: '123', lane_no: '3', run_no: '1')
      v2 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '2', run_no: '10')
      v3 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '2', run_no: '1')
      v4 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '1', run_no: '2')
      v5 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '11', run_no: '2')
      v6 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '10', run_no: '2')
      v7 = FactoryGirl.create(:presale_vehicle, vnum: '123', lane_no: '11', run_no: '11')

      vehicles = PresaleVehicle.pawn_ordered

      expect(vehicles).to eq([
                               v1,
                               v4,
                               v3,
                               v2,
                               v6,
                               v5,
                               v7
                             ])
    end
  end
end
