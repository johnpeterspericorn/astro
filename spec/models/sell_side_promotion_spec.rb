# encoding: utf-8
require 'spec_helper'

describe SellSidePromotion do
  let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, left_lot: true, seller_paid: true, group_code: 'digital') }
  let(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer) }
  let!(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }
  subject { described_class.new(promise_purchase) }

  describe '#valid?' do
    it 'checks if number of returns exceeds allowed returns' do
      expect(subject.valid?).to be_truthy
    end
  end

  describe '#return_count' do
    it 'returns the count of vehicle returns done for agreement_no and group_code' do
      expect(subject.return_count).to eq(1)
    end
  end

  describe '#purchase_count' do
    it 'returns the count of purchases done for agreement_no and group_code' do
      expect(subject.purchase_count).to eq(1)
    end
  end

  describe '#allowed_returns' do
    it 'allows 3 returns under promo for 10 purchases' do
      expect(subject.allowed_returns).to eq(3)
    end

    it 'allows 3 returns for every 10 purchases' do
      allow_any_instance_of(SellSidePromotion).to receive(:purchase_count) { 21 }
      promo = described_class.new(promise_purchase)
      expect(promo.allowed_returns).to eq(9)
    end

    it 'allows only a maximum of 10 returns' do
      allow_any_instance_of(SellSidePromotion).to receive(:purchase_count) { 31 }
      promo = described_class.new(promise_purchase)
      expect(promo.allowed_returns).to eq(10)
    end
  end
end
