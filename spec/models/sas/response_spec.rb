# encoding: utf-8
require 'spec_helper'

describe Sas::Response do
  describe 'initialization' do
    it 'allows initialization with arbitrary attributes', regression: true do
      expect { described_class.new(arbi: :trary) }.not_to raise_error
    end
  end
end
