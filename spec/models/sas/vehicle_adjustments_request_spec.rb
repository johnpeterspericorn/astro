# encoding: utf-8
require 'spec_helper'

describe Sas::VehicleAdjustmentsRequest do
  let(:vnum) { 'VNUM12345' }
  let(:agreement_number) { '5000000' }
  let(:client) { double :api_client }

  subject(:request) { described_class.new(vnum, agreement_number, client) }

  before do
    allow(Sas::VehicleAdjustmentsResponse).to receive(:new)
  end

  describe '#adjustments' do
    let(:response) { double :response }

    it 'is nil before adjustments are loaded' do
      expect(request.adjustments).to be_nil
    end

    it 'loads adjustments from MAS api' do
      allow(client).to receive(:post).and_return(response)

      expect(Sas::VehicleAdjustmentsResponse).to receive(:new).with(response)

      request.load
    end
  end

  describe '#load' do
    it 'posts itself as parameters to the api client' do
      expect(client)
        .to receive(:post)
        .with(request)
        .and_return({})

      request.load
    end
  end
end
