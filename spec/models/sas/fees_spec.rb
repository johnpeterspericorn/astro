# encoding: utf-8
require 'spec_helper'

describe Sas::Fees do
  describe '#new' do
    it 'may be initialized with a single fee' do
      fees = described_class.new(double)

      expect(fees.count).to eq(1)
    end

    it 'may be initialized with a collection of fees' do
      fees = described_class.new([double, double])

      expect(fees.count).to eq(2)
    end

    it 'may be initialized with no arguments' do
      fees = described_class.new

      expect(fees.count).to eq(0)
    end
  end

  let(:reimbursable_fee) { double(reimbursable?: true) }
  let(:nonreimbursable_fee) { double(reimbursable?: false) }

  describe '#reimbursable_fees' do
    it 'returns Sas::Fees' do
      fees = described_class.new([])

      expect(fees.reimbursable_fees).to be_a(described_class)
    end

    it 'returns fees which are reimbursable' do
      fees = described_class.new([reimbursable_fee, nonreimbursable_fee])

      expect(fees.reimbursable_fees.count).to eq(1)
      expect(fees.reimbursable_fees.first).to eq(reimbursable_fee)
    end

    it 'returns an empty collection when no fees are reimbursable' do
      fees = described_class.new(nonreimbursable_fee)

      expect(fees.reimbursable_fees.count).to eq(0)
    end
  end

  describe '#nonreimbursable_fees' do
    it 'returns Sas::Fees' do
      fees = described_class.new([])

      expect(fees.nonreimbursable_fees).to be_a(described_class)
    end

    it 'returns fees which are non-reimbursable' do
      fees = described_class.new([reimbursable_fee, nonreimbursable_fee])

      expect(fees.nonreimbursable_fees.count).to eq(1)
      expect(fees.nonreimbursable_fees.first).to eq(nonreimbursable_fee)
    end

    it 'returns an empty collection when all fees are reimbursable' do
      fees = described_class.new(reimbursable_fee)

      expect(fees.nonreimbursable_fees.count).to eq(0)
    end
  end

  describe '#total' do
    it 'is the sum of all fee amounts' do
      fee1 = double(amount: 1)
      fee2 = double(amount: 2)
      fees = described_class.new([fee1, fee2])

      expect(fees.total).to eq(3)
    end

    it 'is zero when there are no fees' do
      fees = described_class.new([])

      expect(fees.total).to eq(0)
    end
  end
end
