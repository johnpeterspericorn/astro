# encoding: utf-8
require 'spec_helper'

describe Sas::VehicleAdjustmentsApiClient do
  subject(:client) { described_class.new }

  describe '#post' do
    it 'returns response hash for successful request', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
      request_params = double :request, vnum: '1FAHP3F21CL278150', agreement_number: 5_133_859
      response = client.post(request_params)

      expect(response).to include(
        'OVESuccessFee',
        'OVEFacilitationFee',
        'BuyNet',
        'VehiclePrice',
        'BuyFee',
        'HowPaid',
        'description',
        'CertifiedFee',
        'FloorAgency',
        'TotalAdjustments',
        'msg'
      )
    end

    it 'raises error when vehicle cannot be found' do
      request_params = double :request, vnum: 'VNUM1234', agreement_number: 5_000_000

      VCR.use_cassette('vehicle_adjustments_api/missing_vehicle_request') do
        expect do
          client.post(request_params)
        end.to raise_error(Sas::VehicleAdjustmentsApiClient::VehicleNotFound)
      end
    end

    it 'raises error when api error occurs' do
      request_params = double :request, vnum: 'VNUM1234', agreement_number: 5_000_000

      VCR.use_cassette('vehicle_adjustments_api/failed_request') do
        expect do
          client.post(request_params)
        end.to raise_error(Sas::ApiError)
      end
    end
  end
end
