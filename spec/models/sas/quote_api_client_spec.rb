# encoding: utf-8
require 'spec_helper'

describe Sas::QuoteApiClient do
  subject(:client) { described_class.new(request) }

  describe '#post' do
    let(:vnum) { '1B3HB48B78D754223' }
    let(:seller_no)  { 5_011_377 }
    let(:buyer_no)   { 5_006_132 }
    let(:sale_price)   { 1900 }
    let(:location_initials) { 'SLAA' }
    let(:request) { Hashie::Mash.new(vnum: vnum, seller_5m: seller_no, buyer_5m: buyer_no, sale_price: sale_price, location_initials: location_initials) }

    it 'returns response hash for successful request', vcr: { cassette_name: 'quote_api/successful_request' } do
      # The values here correspond to the Astroheim provided successful request
      response = client.post

      # The values below correspond to the recorded successful response
      expect(response).to include(
        'EligCode' => '0',
        'Message' => 'Eligible',
        'OfferToken' => '-1',
        'Result1' => '100',
        'Result2' => '175',
        'Result3' => '2.85',
        'Result4' => '130',
        'ResultType' => 'Q',
        'rc' => '0'
      )
    end

    context 'when an API error occurs' do
      let(:request) { Hashie::Mash.new }

      it 'raises error when api error occurs' do
        VCR.use_cassette('quote_api/failed_request') do
          expect do
            client.post
          end.to raise_error(Sas::ApiError)
        end
      end
    end
  end
end
