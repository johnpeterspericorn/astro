# encoding: utf-8
require 'spec_helper'

describe Sas::Fee do
  describe '.build' do
    it 'builds a single fee' do
      attributes = { 'Code' => 'ABC' }
      fees = described_class.build(attributes)

      expect(fees.first.code).to eq('ABC')
    end

    it 'builds a collection of fees' do
      attributes = [{ 'Code' => 'ABC' }]
      fees = described_class.build(attributes)

      expect(fees.first.code).to eq('ABC')
    end
  end

  describe '.new' do
    describe 'code' do
      it 'is initialized with a capitalized "Code"' do
        fee = described_class.new('Code' => 'abc')
        expect(fee.code).to eq('abc')
      end
    end

    describe 'amount' do
      it 'is initialized with a capitalized "Amount"' do
        fee = described_class.new('Amount' => '123')
        expect(fee.amount).to eq(123)
      end
    end

    describe 'fee name' do
      it 'is initialized with a capitalized "Fee"' do
        fee = described_class.new('Fee' => 'sauce')
        expect(fee.name).to eq('sauce')
      end
    end

    describe 'bucket' do
      it 'is initialized with a capitalized "Bucket"' do
        fee = described_class.new('Bucket' => 'KFC')
        expect(fee.bucket).to eq('KFC')
      end
    end
  end

  describe 'reimbursable?' do
    it 'is reimbursable when bucket is' do
      fee = described_class.new('Bucket' => 'Reimbursable')
      expect(fee.reimbursable?).to eq(true)
    end

    it 'is not when bucket is not' do
      fee = described_class.new('Bucket' => 'NonReimbursable')
      expect(fee.reimbursable?).to eq(false)
    end
  end
end
