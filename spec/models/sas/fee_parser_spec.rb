# encoding: utf-8
require 'spec_helper'

describe Sas::FeeParser do
  describe '.parse' do
    it 'parses a set of comma delimited fee attributes' do
      fee_string = 'Code: AR-DS'

      fees = described_class.parse(fee_string)

      expect(fees.length).to eq(1)
      expect(fees.first.fetch('Code')).to eq('AR-DS')
    end

    it 'parses multiple fees attributes delimited by periods' do
      fee_string = 'Code: AR-DS. Code: DEALA. Code: DEALB. Code: DEALC. Code: PSI01, Amount: 123.'

      fees = described_class.parse(fee_string)

      expect(fees.length).to eq(5)
      expect(fees.second.fetch('Code')).to eq('DEALA')
      expect(fees.last.fetch('Amount')).to eq('123')
    end

    it 'strings trailing digits off "Fee123" keys' do
      fee_string = 'Fee456: THINGS'

      fees = described_class.parse(fee_string)

      expect(fees.first.fetch('Fee')).to eq('THINGS')
    end
  end
end
