# encoding: utf-8
require 'spec_helper'

describe Sas::QuoteResponse do
  describe '.build' do
    it 'raises an error when building unknown type' do
      raw_response = { 'ResultType' => nil }

      expect { described_class.build(raw_response) }.to raise_error(described_class::InvalidQuoteType)
    end
  end

  describe '#price' do
    it 'calculates prices for days and miles' do
      base_price_250 = '100'
      additional_day_price = '1'
      raw_response = { 'Result1' => base_price_250, 'Result3' => additional_day_price }

      response = described_class.new(raw_response)

      # The base price is for 7 days, so the price calulated by adding the
      # "additional day price" * "number of days past the base"
      expect(response.price(250, 14)).to eq(107)
    end
  end
end
