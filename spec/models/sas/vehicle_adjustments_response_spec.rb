# encoding: utf-8
require 'spec_helper'

describe Sas::VehicleAdjustmentsResponse do
  let(:floor_plan_company_code) { 'FP1' }

  let(:raw_response) do
    {
      FloorAgency: floor_plan_company_code,

      OVESuccessFee: 'foobar',
      OVEFacilitationFee: 'foobar',
      BuyNet: 'foobar',
      VehiclePrice: 'foobar',
      BuyFee: 'foobar',
      HowPaid: 'foobar',
      PaymentStatus: 'foobar',
      Description: 'foobar',
      CertifiedFee: 'foobar',
      TotalAdjustments: 'foobar',
      Msg: 'foobar'
    }
  end

  describe '#floor_plan_company' do
    subject(:response) { described_class.new(raw_response) }

    it 'finds the associated record' do
      model = double 'FloorPlanCompany'

      expect(model).to receive(:find_by_company_code).with(floor_plan_company_code)

      response.floor_plan_company(model)
    end
  end

  describe '#paid_by_buyer?' do
    it 'returns true when probably buyer paid' do
      subject = described_class.new(raw_response.merge(PaymentStatus: 'PROBABLY BUYER PAID'))
      expect(subject.paid_by_buyer?).to be_truthy
    end

    it 'returns true when buyer paid' do
      subject = described_class.new(raw_response.merge(PaymentStatus: 'BUYER PAID'))
      expect(subject.paid_by_buyer?).to be_truthy
    end
  end
end
