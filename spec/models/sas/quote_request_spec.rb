# encoding: utf-8
require 'spec_helper'

describe Sas::QuoteRequest do
  let(:vnum)           { 'VNUM12345' }
  let(:seller_number) { '5000000' }
  let(:buyer_number)  { '5000001' }
  let(:price)         { 1234 }
  let(:client)        { double :client }

  let(:response) { double :response }

  subject(:request) do
    described_class.new(
      vnum: vnum,
      seller_5m: seller_number,
      buyer_5m: buyer_number,
      sale_price: price,
      location_initials: 'SVAA'
    )
  end

  before do
    allow(Sas::QuoteResponse).to receive(:build).and_return(response)
    allow(Sas::QuoteApiClient).to receive(:new).and_return(client)
  end

  describe '#quotes' do
    it 'is nil before quotes are loaded' do
      expect(request.quotes).to be_nil
    end

    it 'loads quotes from MAS api' do
      allow(client).to receive(:post)

      request.load

      expect(request.quotes).to be_present
    end
  end
end
