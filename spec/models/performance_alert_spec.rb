# encoding: utf-8
require 'spec_helper'

describe PerformanceAlert do
  describe 'follow_up_date_input' do
    before do
      FactoryGirl.create(:agreement_information, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER)
    end

    it 'calculates if date is current date' do
      perf_alert = FactoryGirl.create(:performance_alert, follow_up_date: Time.zone.now + 60)
      expect(perf_alert.valid?).to be_truthy
    end

    it 'calculates if date is yesterday' do
      perf_alert = FactoryGirl.build(:performance_alert, follow_up_date: Time.zone.now - 1.day)
      expect(perf_alert.valid?).to be_falsey
    end

    it 'calculates if date is on 30th day' do
      perf_alert = FactoryGirl.create(:performance_alert, follow_up_date: Time.zone.now + 30.days)
      expect(perf_alert.valid?).to be_truthy
    end
  end
end
