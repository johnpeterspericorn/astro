# encoding: utf-8
require 'spec_helper'

describe PendingConditions do
  MetCondition = Struct.new(:target) do
    def met?
      true
    end

    def code
      self.class.code
    end

    def self.code
      'CODEZ!'
    end
  end

  UnmetCondition = Struct.new(:target) do
    def met?
      false
    end
  end

  let(:target) { double :target }

  describe '#met?' do
    it 'is false if no composed condition is met' do
      pending_condition = described_class.new(target, conditions: UnmetCondition)

      expect(pending_condition).to_not be_met
    end

    it 'is true if any composed condition is satisfied' do
      pending_condition = described_class.new(target, conditions: MetCondition)

      expect(pending_condition).to be_met
    end

    it 'should track vehicle price over limit condition' do
      allow_any_instance_of(PromiseOffer).to receive(:vehicle_purchase_price).and_return(51_000)
      promise_offer = FactoryGirl.create(:promise_offer)
      pending_condition = PendingConditions::VehiclePriceOverLimitCondition.new(promise_offer)
      expect(pending_condition).to be_met
    end
  end

  describe '#code' do
    let(:code) { double :code }

    it 'is nil when no condition has been met' do
      pending_condition = described_class.new(target, conditions: UnmetCondition)

      expect(pending_condition.code).to be_nil
    end

    it "is the met condition's code when a condition has been met" do
      pending_condition = described_class.new(target, conditions: MetCondition)

      expect(pending_condition.code).to eq(MetCondition.code)
    end
  end

  describe PendingConditions::BlacklistedCondition do
    let(:vnum) { 'ABCD888' }
    let(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: 5123450) }
    let(:vehicle_information) { FactoryGirl.create(:vehicle_information, vnum: vnum) }
    let(:offer) { FactoryGirl.create(:promise_offer, vehicle_information: vehicle_information, pawn_information: pawn_information) }

    let!(:vnum_blacklist) { FactoryGirl.create(:vnum_blacklist, vnum: vnum) }
    let!(:agreementship_blacklist) { FactoryGirl.create(:agreementship_blacklist, agreement_no: offer.agreement_no) }

    it 'assigns vnum blacklisted code' do
      pending_condition = PendingConditions::BlacklistedCondition.new(offer)
      expect(pending_condition.code).to eq(PendingConditions::VEHICLE_BLACKLISTED)
    end

  end
end
