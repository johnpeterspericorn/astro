# encoding: utf-8
require 'spec_helper'

describe PawnLocation do
  let(:first_email) { 'test1@example.com' }
  let(:second_email) { 'test2@example.com' }
  let(:email_string) { "#{first_email}, #{second_email}" }
  let(:email_array) { [first_email, second_email] }

  context 'scopes' do
    describe '.accepts_returns' do
      before do
        described_class.create(name: 'Statesville', initials: 'SVAA', accepts_returns: false)
        described_class.create(name: 'Drive', initials: 'DRIV')
      end

      it 'only returns those locations that accept returns' do
        expect(described_class.accepts_returns.count).to eq(1)
      end
    end
  end

  describe 'emails' do
    it 'allows emails to be set as comma separated string' do
      subject.return_emails = email_string
      expect(subject.return_emails).to eq(email_array)
    end

    it 'allows emails to be set as an array' do
      subject.return_emails = email_array
      expect(subject.return_emails).to eq(email_array)
    end
  end
end
