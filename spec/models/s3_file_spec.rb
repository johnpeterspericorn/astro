# encoding: utf-8
require 'spec_helper'

describe S3File do
  it 'configures AWS on initialize' do
    expect(Aws.config).to receive(:update)
    expect(Aws::Credentials).to receive(:new)

    S3File.new('abcd')
  end

  describe 'file upload' do
    subject { S3File.new('abcd') }

    it 'can upload a file to AWS S3' do
      file_to_upload = Rails.root.join('spec/fixtures/cancelled_returns.csv').to_s

      expect_any_instance_of(Aws::S3::Object).to receive(:upload_file).with(file_to_upload)
      subject.upload(file_to_upload)
    end

    it 'gives the presigned url of the uploaded file' do
      file_to_upload = Rails.root.join('spec/fixtures/cancelled_returns.csv').to_s
      expect_any_instance_of(Aws::S3::Object).to receive(:presigned_url).with(:get, expires_in: 1.week)
      expect_any_instance_of(Aws::S3::Object).to receive(:upload_file).with(file_to_upload)

      subject.upload(file_to_upload)
      subject.presigned_url
    end
  end
end
