# encoding: utf-8
require 'spec_helper'

describe User do
  let(:user) { FactoryGirl.create(:user, super_user: true) }

  describe '#roles' do
    it 'returns a collection of the roles the user has' do
      user = described_class.new(super_user: true, invoiceable: true)

      expect(user.roles).to match_array(%w(super_user invoiceable agreement_transaction_viewer agreement_score_viewer))
    end

    it 'user is invoiceable by default' do
      user = FactoryGirl.create(:user, super_user: true)
      expect(user.roles).to match_array(%w(super_user invoiceable agreement_transaction_viewer))
    end

    it 'user is agreement_transaction_viewer and agreement_score_viewer by default' do
      user = described_class.new(super_user: true)
      expect(user.roles).to match_array(%w(super_user agreement_transaction_viewer agreement_score_viewer))
    end
  end

  context 'buyer user' do
    subject { FactoryGirl.create(:buyer_user) }
    let(:agreement_number) { '5110000' }
    let(:other_agreement_number) { '5220000' }
    let(:promise_offer) { double(agreement_no: agreement_number) }

    describe '#owns_offer?' do
      context 'when the offer parameter has an AA# of 0' do
        before do
          allow(promise_offer).to receive(:pawn_access_no) { '0' }
        end

        it "returns false if the user's agreement #s don't include the offer's" do
          allow(subject).to receive(:agreement_numbers) { [other_agreement_number] }
          expect(subject.owns_offer?(promise_offer)).to be_falsey
        end

        it 'returns true otherwise' do
          allow(subject).to receive(:agreement_numbers) { [agreement_number, other_agreement_number] }
          expect(subject.owns_offer?(promise_offer)).to be_truthy
        end
      end

      context 'when the offer parameter has a non-zero AA#' do
        let(:pawn_access_number) { '100000000' }
        let(:other_pawn_access_number) { '100000001' }

        before do
          allow(promise_offer).to receive(:pawn_access_no) { pawn_access_number }
        end

        it "returns true if the user's pawn access # matches the offer's" do
          allow(subject).to receive(:pawn_access_number) { pawn_access_number }
          expect(subject.owns_offer?(promise_offer)).to be_truthy
        end
      end
    end
  end

  describe '.with_roles' do
    it 'returns users who have a combination of the roles selected' do
      admin_user = FactoryGirl.create(:admin_user, buyer_user: true)
      FactoryGirl.create(:buyer_user)

      users = User.with_roles(%w(admin buyer_user))

      expect(users.count).to eq(1)
      expect(users).to eq([admin_user])
    end

    it 'returns all users if no role is given' do
      FactoryGirl.create(:api_user)

      users = User.with_roles([])

      expect(users.count).to eq(1)
    end
  end

  describe 'Versioning', versioning: true do
    context 'when new user created' do
      it { expect(user).to be_versioned }
    end

    context 'when new product fee updated' do
      before do
        user.update_attributes!(vehicle_creator: true)
      end

      it { expect(user.versions.last.reify.vehicle_creator).to be false }
      it { expect(user.versions.last.changeset.first[0]).to eq 'vehicle_creator' }
      it { expect(user.versions.last.changeset.first[1][0]).to eq false }
      it { expect(user.versions.last.changeset.first[1][1]).to eq true }
    end
  end
end
