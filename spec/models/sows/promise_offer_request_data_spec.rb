# encoding: utf-8
require 'spec_helper'

describe Sows::PromiseOfferRequestData do
  subject { described_class.new(promise_offer) }
  let(:promise_offer) do
    FactoryGirl.create :promise_offer,
                       days_selected: 7,
                       miles_selected: 250,
                       promise_price: 50.0000000000000001,
                       pawn_information: pawn_information,
                       seller_paid: true,
                       automatic_purchase: true,
                       bypass_charge: false,
                       limited_volume: false,
                       percent_coverage: nil
  end
  let(:pawn_information) do
    FactoryGirl.create :pawn_information,
                       work_order_number: 1_234_567
  end

  context 'when feature flag is off' do
    it 'returns a RequestData instance' do
      expect(subject.product_type).to eq('7 Days, 250 Miles')
      expect(subject.description).to eq('agreementshield 7D 250M')
      expect(subject.amount).to eq(50)
      expect(subject.location_initials).to eq('AAAA')
      expect(subject.sblu).to eq(123)
      expect(subject.work_order_number).to eq('1234567')
      expect(subject).to be_seller_paid
      expect(subject).to_not be_bypass_charge
      expect(subject.vnum).to_not be_nil
      expect(subject.vnum).to eq(promise_offer.vehicle_information.vnum)
      expect(subject.charge_code).to be_nil
      expect(subject.call_id).to be_nil
    end
  end

  context 'when feature flag is on' do
    around(:each) do |example|
      Settings.product_mapping_enabled = true
      example.run
      Settings.product_mapping_enabled = false
    end

    it 'returns a RequestData instance' do
      expect(subject.product_type).to eq('7 Days, 250 Miles')
      expect(subject.description).to eq('agreementshield 7D 250M')
      expect(subject.amount).to eq(50)
      expect(subject.location_initials).to eq('AAAA')
      expect(subject.sblu).to eq(123)
      expect(subject.work_order_number).to eq('1234567')
      expect(subject).to be_seller_paid
      expect(subject).to_not be_bypass_charge
      expect(subject.vnum).to_not be_nil
      expect(subject.vnum).to eq(promise_offer.vehicle_information.vnum)
      expect(subject.charge_code).to eq(963)
      expect(subject.call_id).to eq('DEALS')
    end
  end
end
