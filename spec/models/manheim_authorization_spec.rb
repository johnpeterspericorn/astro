# encoding: utf-8
require 'spec_helper'

describe AstroheimAuthorization do
  describe '#get_agreement_numbers', vcr: { record: :new_episodes } do
    let(:pawn_access_number) { '100700225' }
    subject { described_class.new(pawn_access_number: pawn_access_number).get_agreement_numbers }

    it { expect(subject).to be_kind_of Enumerable  }
    it { expect(subject).to be_present             }
    it { expect(subject.first).to match(/^5\d{6}$/) }
  end
end
