# encoding: utf-8
require 'spec_helper'

describe Cancellation do
  let(:vehicle_information) { FactoryGirl.create(:vehicle_information, vnum: vnum) }
  let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, days_selected: 7, miles_selected: 250, vehicle_information: vehicle_information) }
  let(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: '5241420', universal_no: 'ABC12345', promise_offer_id: promise_offer.id) }
  let!(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer) }
  let(:promise_purchase_for_email) { PromisePurchaseForEmail.new(promise_purchase).as_json }
  let(:inspection_purchase) { FactoryGirl.create(:inspection_purchase, promise_offer: promise_offer) }
  let(:promise_refund) { double 'PromiseRefund' }
  let(:reason)             { "I don't like the car" }
  let(:mail)               { double(:mail).as_null_object }
  let(:vnum)                { '2G1WC5E39C1222359' } # from randomvnum.com

  subject(:cancellation) { described_class.new(reason: reason) }

  before do
    Settings.charges_enabled = true
  end

  describe '#process' do
    before do
      allow(Ods::Client).to receive(:new) { double(request_vehicle_info: { has_agreementshield_charge: true }) }
      allow_any_instance_of(PromiseOffer).to receive(:charge_present_in_ods?) { true }
    end

    subject { described_class.new(reason: reason, promise_purchase_id: promise_purchase.id, ineligible_for_repurchase: ineligible_for_repurchase) }
    let(:ineligible_for_repurchase) { '0' }

    context 'when required attributes are missing' do
      it 'returns false if cancellation is invalid' do
        cancellation = described_class.new
        expect(cancellation.process).to be_falsey
      end
    end

    it 'returns true if successful' do
      expect(PromiseChargeJob)
        .to receive(:enqueue)
      expect(subject.process).to be_truthy
    end

    it 'sends an email if successful and vehicle is eligible for repurchase' do
      expect(CancellationMailer).to receive(:cancellation_confirmation_eligible_for_repurchase).with(promise_offer, promise_purchase_for_email, reason).and_return(mail)
      expect(PromiseChargeJob).to receive(:enqueue)
      expect(subject.process).to be_truthy
    end

    context 'when vehicle is ineligble for repurchase' do
      let(:ineligible_for_repurchase) { '1' }
      it 'sends an email' do
        expect(CancellationMailer).to receive(:cancellation_confirmation_ineligible_for_repurchase).with(promise_offer, promise_purchase_for_email, reason).and_return(mail)
        expect(PromiseChargeJob).to receive(:enqueue)
        expect(subject.process).to be_truthy
      end
    end

    it 'does not send an email if the updating steps fail' do
      allow(PromisePurchase).to receive(:find).with(promise_purchase.id).and_return(promise_purchase)
      expect(promise_purchase).to receive(:destroy).and_raise(ActiveRecord::Rollback)
      expect(PromiseChargeJob).to receive(:enqueue)
      expect(CancellationMailer).not_to receive(:cancellation_confirmation_ineligible_for_repurchase)
      expect(subject.process).to be_truthy
    end

    it 'creates a cancelled purchase' do
      expect(PromiseChargeJob).to receive(:enqueue)
      expect { subject.process }.to change { CancelledPurchase.count }.by(1)
    end

    it 'destroys the promise purchase' do
      expect(PromiseChargeJob).to receive(:enqueue)
      expect { subject.process }.to change { PromisePurchase.count }.by(-1)
    end

    context 'when inspection purchase exists' do
      it 'destroys the inspection purchase' do
        expect(PromiseChargeJob).to receive(:enqueue)
        inspection_purchase
        expect { subject.process }.to change { InspectionPurchase.count }.by(-1)
      end
    end

    it 'nilifies promise offer attributes' do
      expect(PromiseChargeJob).to receive(:enqueue)
      expect(promise_offer.miles_selected).to_not be_nil
      expect(promise_offer.days_selected).to_not be_nil
      subject.process
      promise_offer.reload
      expect(promise_offer.miles_selected).to be_nil
      expect(promise_offer.days_selected).to be_nil
    end

    it 'refunds promise amount' do
      expect(PromiseRefund)
        .to receive(:new)
        .and_return(promise_refund)
      expect(PromiseChargeJob)
        .to receive(:enqueue)
        .with(promise_refund)

      subject.process
    end

    it 'skip refund if skip_applying_credit is flagged' do
      cancellation = described_class.new(reason: reason, skip_applying_credit: '1') # "1" is form for true
      expect(CancellationJobBuilder)
        .not_to receive(:enqueue_job)

      cancellation.process
    end

    it 'skip cancellation confirmation mailer if silent is flagged' do
      cancellation = described_class.new(reason: reason, silent: '1') # "1" is form for true
      expect(CancellationMailer)
        .not_to receive(:cancellation_confirmation_eligible_for_repurchase)

      cancellation.process
    end

    context 'ineligible for re-purchase' do
      let(:ineligibility_condition) { IneligibilityCondition::CANCELLED_PURCHASE }
      subject(:cancellation) do
        described_class.new(
          ineligibility_condition:   ineligibility_condition,
          ineligible_for_repurchase: true,
          promise_purchase_id: promise_purchase.id,
          reason:                    reason)
      end
      before do
        expect(PromiseChargeJob).to receive(:enqueue)
      end

      it 'updates the offer with the ineligibility condition' do
        expect(promise_offer.ineligibility_condition).to_not eq(ineligibility_condition)
        expect(subject.process).to be_truthy
        promise_offer.reload
        expect(promise_offer.ineligibility_condition).to eq(ineligibility_condition)
      end

      it 'blacklists the vnum' do
        expect(VinBlacklist).to receive(:create!).with(vnum: vnum)
        subject.process
      end
    end
  end

  describe 'issue refund' do
    subject { described_class.new(reason: reason, promise_purchase_id: promise_purchase.id) }

    before do
      Settings.query_ods = true
      Settings.sows_enabled = true
      promise_purchase.promise_offer.pawn_information.update(sblu: '123', agreement_no: '5241420', universal_no: 'ABC12345')
    end

    it 'should apply credit only if charged in ods' do
      allow_any_instance_of(Ods::Client).to receive(:request_vehicle_info) { { has_agreementshield_charge: true } }
      expect(SowsCancellationJob).to receive(:enqueue)
      subject.process
    end

    it 'doesnt apply credit if not charged' do
      allow_any_instance_of(Ods::Client).to receive(:request_vehicle_info) { { has_agreementshield_charge: false } }
      expect(SowsCancellationJob).not_to receive(:enqueue)
      subject.process
    end
  end
end
