# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe PriceFetcher do
  describe '.pricing_information' do
    before do
      stub_buyer_authorization
    end

    let(:price) { 100 }
    let(:inspection_information) { InspectionInformation.create!(inspection_only_price: price) }
    let(:promise_offer) { FactoryGirl.create(:promise_offer, inspection_information: inspection_information) }

    subject(:fetcher) { described_class.new(promise_offer) }

    context 'successful query' do
      it 'should include inspection information if it is present' do
        expect(fetcher.pricing_information[:inspection_only_price]).to eq(price)
      end

      it 'should not include inspection information if it is not included' do
        inspection_information.update_attributes!(inspection_only_price: nil)
        expect(fetcher.pricing_information[:inspection_only_price]).not_to be_present
      end
    end
  end
end
