# encoding: utf-8
require 'spec_helper'

describe InternetNoteMailer do
  describe '.notify_last_failure' do
    let(:vnum) { 'ABC123' }
    let(:location_initials) { 'ABC' }
    let(:sblu) { 'DEF' }
    let(:work_order_number) { '1234' }
    let(:note) { 'WOWOW' }
    let(:failure_message) { ':boom:' }

    let(:attributes) do
      {
        vnum: vnum,
        pawn_location_initials: location_initials,
        sblu: sblu,
        work_order: work_order_number,
        notes: note
      }
    end

    subject(:mail) do
      described_class.notify_last_failure(attributes, failure_message)
    end

    it 'includes the notes attributes' do
      expect(mail.body).to include(vnum)
      expect(mail.body).to include(location_initials)
      expect(mail.body).to include(sblu)
      expect(mail.body).to include(work_order_number)
      expect(mail.body).to include(note)
    end

    it 'includes the failure message' do
      expect(mail.body).to include(failure_message)
    end
  end
end
