# encoding: utf-8
require 'requests_helper'

describe ReturnInvoiceMailer do
  let(:return_location_name) { 'ABC' }
  let(:return_location_emails) { ['return_pawn@astroheim.com', 'another_return@astroheim.com'] }

  describe '#return_confirmation' do
    let(:return_location) { PawnLocation.new(name: return_location_name, return_emails: return_location_emails) }

    let(:buyer_email) { 'userexample.com' }
    let(:vnum) { 'A123B' }

    let(:invoice) { double :return_invoice, buyer_email: buyer_email, pawn_location: return_location, id: -1, email: 'foo@bar.com', slug: 'af910101b3d6b3aae42ae7aa6c02fe8d' }
    let(:decorated_invoice) { double :return_invoice, odometer_reading_with_units: '12345', distance_units: 'Miles', slug: 'af910101b3d6b3aae42ae7aa6c02fe8d' }
    let(:certificate) { OpenStruct.new vnum: vnum, return_invoice: invoice, receive_by_date: Time.current, initiation_date: Time.current }

    subject(:mail) { described_class.return_confirmation(invoice) }

    before do
      allow(ReturnCertificate).to receive(:new).and_return(certificate)
      allow(invoice).to receive(:canadian_return?).and_return(false)
      allow(invoice).to receive(:decorate).and_return(decorated_invoice)
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match(/Return: #{return_location_name}/) }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match(/VNUM: #{vnum}/) }
    end

    describe '#bcc' do
      subject { super().bcc }
      it { is_expected.to include(*return_location_emails) }
    end

    describe 'body content' do
      subject { mail.body.encoded }
      it { is_expected.to include('AgreementShield returns are subject to audit for compliance') }
    end
  end

  vcr_options = { cassette_name: 'successful_easypost_shipment' }

  describe '#pawn_location_return_notification', vcr: vcr_options do
    let(:agreementship_addr) { OpenStruct.new }
    let(:agreement_information) { FactoryGirl.build(:agreement_information) }
    let(:agreementship_addr_street) { agreement_information.address_street }
    let(:agreementship_addr_city) { agreement_information.address_city }
    let(:vnum) { 'A123B' }

    let(:shipment) { double :shipment, label_url: 'foo.com' }
    let(:invoice)    { double :return_invoice, shipment: shipment, floor_plan_branch: nil, id: -1, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d' }
    let(:certificate) { OpenStruct.new vnum: vnum, agreementship_addr: agreementship_addr, agreementship_addr_street: agreementship_addr_street, agreementship_addr_city: agreementship_addr_city }

    subject(:mail) { described_class.pawn_location_return_notification(invoice) }

    before do
      allow(ReturnCertificate).to receive(:new).and_return(certificate)
      allow(invoice).to receive(:canadian_return?).and_return(false)
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match(/Disbursement Form, VNUM: #{vnum}/) }
    end

    describe 'body content' do
      subject { mail.parts.first.body }

      it { is_expected.to include vnum }
      it { is_expected.to include certificate.initiation_date }
      it { is_expected.to include agreementship_addr_street }
      it { is_expected.to include agreementship_addr_city }
      it { is_expected.to include I18n.t('return_invoice.disbursement.unpaid') }
      it { is_expected.to include shipment.label_url }

      context 'floor plan paid promise' do
        let(:floor_plan_company_name) { 'Nice-n-Level' }
        let(:floor_plan_branch_street) { '123 Main St' }

        before do
          allow(certificate).to receive(:floor_plan_company_name).and_return(floor_plan_company_name)
        end

        let(:floor_plan_company) { FactoryGirl.create(:floor_plan_company, name: floor_plan_company_name) }
        let(:floor_plan_branch) { FactoryGirl.create(:floor_plan_branch, address_street: floor_plan_branch_street, floor_plan_company: floor_plan_company) }

        before do
          allow(invoice).to receive(:floor_plan_branch).and_return(floor_plan_branch)
        end

        it { is_expected.to include floor_plan_company_name }
        it { is_expected.to include floor_plan_branch_street }
      end
    end
  end

  describe '#return_cancellation for return eligible' do
    let(:user_email) { 'pinky@test.com' }
    let(:invoice)    { FactoryGirl.create(:return_invoice, cancelled_at: Time.current, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d') }
    subject(:mail) { described_class.cancelled_and_eligible_for_return(invoice, user_email) }

    it 'renders the html body' do
      expect(mail.body.encoded).to include('ASTRSHIELD RETURN CANCELLED')
      expect(mail.body.encoded).to include('Agreement Information')
      expect(mail.body.encoded).to include("VRA#: #{invoice.id}")
      expect(mail.body.encoded).to include("Return Cancelled: #{invoice.cancelled_at.strftime('%m/%d/%Y')}")
    end

    it 'renders the text body' do
      expect(mail.text_part.body.encoded).to include('ASTRSHIELD RETURN CANCELLED')
      expect(mail.text_part.body.encoded).to include('Agreement Information')
    end
  end

  describe '#return_cancellation for return ineligible' do
    let(:user_email) { 'pinky@test.com' }
    let(:invoice)    { FactoryGirl.create(:return_invoice, cancelled_at: Time.current, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d') }
    subject(:mail) { described_class.cancelled_and_ineligible_for_return(invoice, user_email) }

    it 'renders the html body' do
      expect(mail.body.encoded).not_to include('Buyback Promise Terms')
    end

    it 'renders the text body' do
      expect(mail.text_part.body.encoded).not_to include('Buyback Promise Terms')
    end
  end

  describe '#return_reinstatement_notification' do
    let(:user) { FactoryGirl.create(:admin_user) }
    let(:return_location) { PawnLocation.new(initials: 'ABCDE', name: 'ABC') }
    let(:invoice) { FactoryGirl.create(:return_invoice, cancelled_at: Time.current, pawn_location: return_location, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d') }

    subject(:mail) { described_class.return_reinstatement_notification(invoice, user) }

    it { should include_in_each_part("VNUM: #{invoice.vnum}") }
    it { expect(mail.text_part.body.encoded).to include obfuscated_return_certificate_url(invoice.slug) }
    it { should include_in_each_part("Return Location: #{return_location.initials}") }
    it { should include_in_each_part("New Expiration Date: #{invoice.expiration_date}") }
    it { should include_in_each_part("VRA Reinstated by: #{user.username}") }
  end

  describe '#cancellation_notification_to_accountant' do
    let(:invoice) { FactoryGirl.create(:return_invoice, new_product_fee: 100, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d').decorate }

    subject(:mail) { described_class.cancellation_notification_to_accountant(invoice) }

    describe '#subject' do
      subject { super().subject }
      it { should match(/PPLV Cancelled VRA# #{invoice.id}/) }
    end

    describe 'body content' do
      subject { mail.parts.first.body }
      it { expect(subject).to include invoice.vnum }
      it { expect(subject).to include invoice.return_fee }
    end
  end

  describe '#pawn_location_left_lot_notification' do
    context 'with pawn_location emails' do
      let(:return_location) { FactoryGirl.create(:pawn_location, name: return_location_name, return_emails: return_location_emails) }
      let(:invoice) { FactoryGirl.create(:return_invoice, pawn_location: return_location, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d').decorate }
      let(:locale) { 'en' }

      subject(:mail) { described_class.pawn_location_left_lot_notification(invoice, locale) }

      describe '#subject' do
        subject { super().subject }
        it { should match(%r{AgreementShield VRA Initiatated \/ Vehicle has not left lot VRA #{invoice.vra} VNUM #{invoice.vnum}}) }
      end

      describe 'body content' do
        subject { mail.parts.first.body }

        it { expect(subject).to include 'This is the AgreementShield return, it was never removed from lot. Please request for this vehicle to be restocked into the astrdian drop record.' }
        it { expect(subject).to include 'Vehicle Has Not Left the Lot' }
      end

      describe 'sender address' do
        it { expect(subject.to).to include 'return_pawn@astroheim.com', 'another_return@astroheim.com' }
      end
    end

    context 'without action location emails' do
      let(:invoice) { FactoryGirl.create(:return_invoice, slug: 'af910101b3d6b3aae42ae7aa6c02fe8d').decorate }
      subject(:mail) { described_class.pawn_location_left_lot_notification(invoice) }

      describe 'sender address' do
        it { expect(subject.to).not_to include 'return_pawn@astroheim.com', 'another_return@astroheim.com' }
        it { expect(subject.to).to include 'technology@agreementshield.com' }
      end
    end
  end
end
