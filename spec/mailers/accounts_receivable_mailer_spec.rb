# encoding: utf-8
require 'spec_helper'

describe AccountsReceivableMailer do
  describe '.ledger_report' do
    subject(:mail) { described_class.ledger_report }

    let!(:promise_offer) { AccountsReceivableOfferDecorator.decorate(FactoryGirl.create(:promise_offer)) }

    let!(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
    let!(:cancelled_purchase) { CancelledPurchase.create_from_purchase!('test', promise_purchase, nil) }
    let!(:decorated_cancelled_purchase) { AccountsReceivableCancelledPurchaseDecorator.decorate(cancelled_purchase) }

    before do
      Settings.accounts_receivable_report_emails = ['ar@astroheim.com', 'ar2@astroheim.com']
    end

    it 'is sent to each AR report email address' do
      Settings.accounts_receivable_report_emails.each do |email|
        expect(mail.to).to include(email)
      end
    end

    it 'attaches a CSV report' do
      expect(mail.attachments.first.content_type).to include 'text/csv'
    end
  end
end
