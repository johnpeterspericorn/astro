# encoding: utf-8
require 'spec_helper'

describe SalesMailer do
  describe '#report' do
    let(:date) { Date.yesterday }
    let(:email) { 'test@astroheim.com' }
    let!(:purchase) { FactoryGirl.create(:returnable_promise_purchase) }
    let!(:offer) { purchase.promise_offer }

    subject(:mail) { described_class.report }

    it 'is sent to sales report email' do
      Settings.sales_report_email = email
      expect(mail.to).to include(email)
    end

    it 'sends previous days sales report' do
      expect(mail.body).to include(offer.pawn_location.name)
    end

    it 'displays seller information' do
      expect(mail.body).to include('% Sell Side')
      expect(mail.body).to include('Sell Side Agreements')
    end

    it 'displays disclaimer' do
      expect(mail.body).to include('The following numbers are a preliminary estimate of promise sales from yesterday')
    end
  end
end
