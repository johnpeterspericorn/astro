# encoding: utf-8
require 'spec_helper'

describe ChargesMailer do
  describe '.notify_last_failure' do
    let(:run_no) { '12345' }
    let(:charge) { double 'PromiseCharge' }
    let(:error) { 'Erthing Broked' }
    let(:universal_no) { '9999' }
    let (:pawn_information) { double 'PawnInformation' }

    before do
      allow(charge).to receive(:attributes).and_return([])
      allow(charge).to receive(:run_no).and_return(run_no)
      allow(charge).to receive(:location_initials)
      allow(charge).to receive(:lane_no)
      allow(charge).to receive(:vnum)
      allow(charge).to receive(:pawn_information).and_return(pawn_information)
      allow(pawn_information).to receive(:universal_no).and_return(universal_no)
    end

    subject(:mail) { described_class.notify_last_failure(charge, error) }

    it 'includes run number in subject' do
      expect(mail.subject).to include(run_no)
    end

    it 'includes universal_no in body content' do
      expect(mail.body).to include(universal_no)
    end
  end
end
