# encoding: utf-8
require 'spec_helper'

describe HelpPanelMailer do
  describe '.send_guides' do
    let(:address) { 'foo@bar.baz' }
    let(:guide_names) { ['blank.pdf'] }

    subject(:mail) { described_class.send_guides(address, guide_names) }

    it 'sends to the provided address' do
      expect(mail.to).to include(address)
    end

    it 'attaches the specified guides' do
      expect(mail.attachments.length).to eql guide_names.length

      filenames = mail.attachments.map(&:filename)

      guide_names.each do |guide_name|
        expect(filenames).to include(guide_name)
      end
    end
  end

  describe 'Call Me' do
    let(:user_info) do
      {
        first_name: 'Harry',
        last_name: 'Potter',
        phone: '999',
        email: 'harry_potter@hogwarts.com',
        problem_type: 'Some Topic'
      }
    end

    subject(:mail) { described_class.contact(user_info) }

    it 'sends to the provided address' do
      expect(mail.from).to include('info@agreementshield.com')
    end
  end
end
