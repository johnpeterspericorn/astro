# encoding: utf-8
require 'spec_helper'

describe DropNoticeMailer do
  describe '.notify_last_failure' do
    let(:vnum)            { 'ABC123' }
    let(:seller_number)  { '123456' }
    let(:promise_offer) { double 'PromiseOffer', vnum: vnum, seller_no: seller_number }

    let(:return_location) { 'ABC' }
    let(:location)       { double 'PawnLocation', initials: return_location }

    let(:returned_at)    { 1.day.ago }
    let(:return_id)      { 123       }

    let(:return_invoice) { double 'ReturnInvoice' }
    let(:error_message) { 'It bumsploded.' }

    before do
      allow(return_invoice).to receive(:id).and_return(return_id)
      allow(return_invoice).to receive(:created_at).and_return(returned_at)
      allow(return_invoice).to receive(:pawn_location).and_return(location)
      allow(return_invoice).to receive(:promise_offer).and_return(promise_offer)
    end

    subject(:mail) { described_class.notify_last_failure(return_invoice, error_message) }

    it 'is sent to requests email' do
      expect(mail.to).to include(Astro::REQUEST_EMAIL)
    end

    def includes_in_body(content)
      expect(mail.body).to include(content)
    end

    it { includes_in_body(error_message) }
    it { includes_in_body(vnum)           }
    it { includes_in_body(returned_at) }
    it { includes_in_body(seller_number) }
    it { includes_in_body(return_id) }
    it { includes_in_body(return_location) }
  end
end
