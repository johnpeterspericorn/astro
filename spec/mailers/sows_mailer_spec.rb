# encoding: utf-8
require 'spec_helper'

describe SowsMailer do
  describe '.notify_last_failure' do
    let(:offer) { double(:promise_offer, vnum: vnum).as_null_object }
    let(:vnum) { 'ABC123' }
    let(:error) { 'boom' }
    let(:job) { double :job, name: nil }

    subject(:mail) { described_class.notify_last_failure(offer, error, job) }

    it 'includes vnum in body' do
      expect(mail.body).to include(vnum)
    end

    it 'includes error in body' do
      expect(mail.body).to include(error)
    end
  end
end
