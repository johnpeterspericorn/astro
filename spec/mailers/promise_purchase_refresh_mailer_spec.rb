# encoding: utf-8
require 'spec_helper'

describe PromisePurchaseRefreshMailer do
  let(:purchase) { FactoryGirl.create(:promise_purchase) }
  let(:offer) { purchase.promise_offer }

  describe '#refresh_confirmation' do
    subject(:mail) { described_class.refresh_confirmation(offer) }

    it 'includes vnum# in subject' do
      expect(mail.subject).to include(offer.vnum)
    end

    describe 'the mail body' do
      it { is_expected.to include_in_each_part offer.vnum }
    end
  end
end
