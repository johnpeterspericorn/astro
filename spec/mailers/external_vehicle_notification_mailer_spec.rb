# encoding: utf-8
require 'requests_helper'

describe ExternalVehicleNotificationMailer do
  describe '#vehicle_rejection' do
    let(:external_vehicle) { FactoryGirl.create(:external_vehicles_load) }

    subject(:mail) { described_class.vehicle_rejection(external_vehicle) }

    describe 'body content' do
      subject { mail.body.encoded }
      it { is_expected.to include(external_vehicles_approver_url(external_vehicle.external_vehicles_batch, external_vehicle_id: external_vehicle.id)) }
    end
  end
end
