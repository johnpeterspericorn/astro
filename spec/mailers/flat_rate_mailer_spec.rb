# encoding: utf-8
require 'spec_helper'

describe FlatRateMailer do
  describe '.offer_confirmation' do
    let(:user_email) { 'err@,say@wah.huh' }
    let(:buyer_name) { 'David Worth' }
    let(:buyer_email) { 'one@test.com, two@test.com, three@test.com' }
    let(:sales_rep_email) { 'sales_rep@test.com' }
    let(:buyer_information) { double 'BuyerInformation', name: buyer_name, emails: buyer_email }
    let(:flat_rate_offer_batch) { double 'FlatRateOfferBatch' }

    before do
      allow(flat_rate_offer_batch).to receive(:buyer_information).and_return(buyer_information)
      allow(flat_rate_offer_batch).to receive(:user_email).and_return(user_email)
      allow(flat_rate_offer_batch).to receive(:sales_rep_email) { sales_rep_email }
    end

    subject(:mail) { described_class.offer_confirmation(flat_rate_offer_batch) }

    it 'is sent to buyer emails' do
      expect(mail.to).to include('one@test.com')
      expect(mail.to).to include('two@test.com')
      expect(mail.to).to include('three@test.com')
    end

    it 'is sent to sales rep email' do
      expect(mail.cc).to include(sales_rep_email)
    end

    it 'is blind copied to user' do
      expect(mail.bcc).to include(user_email.split(',').last)
    end

    it 'avoid bad email addresses and send mail to others' do
      expect(mail.bcc).not_to include(user_email.split(',').first)
    end

    it "is blind copied to #{Astro::AUTOMATIC_EMAIL}" do
      expect(mail.bcc).to include(Astro::AUTOMATIC_EMAIL)
    end

    it 'addresses buyer by name' do
      expect(mail).to include_in_each_part buyer_name
    end

    it 'links to flat rate offer batch' do
      batch_url = flat_rate_offer_batch_approval_url(flat_rate_offer_batch)

      expect(mail).to include_in_each_part batch_url
    end
  end

  describe '.offer_accepted' do
    let(:buyer_email) { 'buyer@astro.com' }
    let(:user_email) { 'admin@astro.com' }
    let(:id) { 1 }
    let(:buyer_information) { double 'BuyerInformation', emails: buyer_email }
    let(:flat_rate_offer_batch) { double 'FlatRateOfferBatch' }
    let(:sales_rep_email) { 'sales_rep@test.com' }
    let(:flat_rate_offers) { [] }
    let(:accepting_user) { double username: 'foo' }
    let(:accepted_at) { Time.current.to_s }
    let(:pawn_access_number) { '11111' }
    let(:effective_date) { Date.current.to_s }

    before do
      allow(buyer_information).to receive(:name)
      allow(buyer_information).to receive(:cell_phone)
      allow(flat_rate_offers).to receive(:decorate).and_return(flat_rate_offers)

      allow(flat_rate_offer_batch).to receive(:user_email).and_return(user_email)
      allow(flat_rate_offer_batch).to receive(:buyer_information).and_return(buyer_information)
      allow(flat_rate_offer_batch).to receive(:flat_rate_offers).and_return(flat_rate_offers)
      allow(flat_rate_offer_batch).to receive(:belongs_to_canada?).and_return(false)

      allow(accepting_user).to receive(:pawn_access_number).and_return(pawn_access_number)
      allow(flat_rate_offer_batch).to receive(:sales_rep_email) { sales_rep_email }
      allow(flat_rate_offer_batch).to receive(:id).and_return(id)
      allow_any_instance_of(FlatRateMailer).to receive(:generate_offer_pdf).and_yield("#{Rails.root}/public/flat_rate_offer_doc_1.pdf")
      FileUtils.touch('public/flat_rate_offer_doc_1.pdf')
    end

    subject(:mail) { described_class.offer_accepted(flat_rate_offer_batch, accepting_user, accepted_at, effective_date) }

    it 'is sent to agreementshield' do
      expect(mail.to).to include(Astro::AUTOMATIC_EMAIL)
    end

    it 'is NOT sent to buyer' do
      expect(mail.to).to_not include(buyer_email)
    end

    it 'is blind copied to user' do
      expect(mail.bcc).to include(user_email)
    end

    it 'links to batch' do
      batch_url = flat_rate_offer_batch_url(flat_rate_offer_batch)

      expect(mail).to include_in_each_part batch_url
    end

    it 'links to ACH page' do
      ach_url = page_url('ach')

      expect(mail).to include_in_each_part ach_url
    end

    it "includes accepting user's pawn_access_number in mail body" do
      expect(mail).to include_in_each_part pawn_access_number
    end

    it 'attach flat_rate_offer_doc' do
      expect(mail.attachments.count).to eq 3
      expect(mail.attachments[2].filename).to include 'flat_rate_offer_doc.pdf'
    end
  end

  describe '.offer_reset' do
    let(:buyer_information) { FactoryGirl.build(:buyer_information) }
    let(:sales_rep_email) { 'sales_rep@test.com' }
    let(:flat_rate_offer_batch) { double 'FlatRateOfferBatch', buyer_information: buyer_information, sales_rep_email: sales_rep_email, flat_rate_offers: [] }

    subject(:mail) { described_class.offer_reset(flat_rate_offer_batch) }

    it 'includes a link to update the batch information' do
      expect(mail).to include_in_each_part edit_flat_rate_offer_batch_url(flat_rate_offer_batch)
    end
  end
end
