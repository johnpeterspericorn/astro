# encoding: utf-8
require 'spec_helper'

describe PaymentsMailer do
  let(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
  let!(:payment_response) { FactoryGirl.create(:payment_response, payment: payment, promise_purchase: promise_purchase) }
  let!(:payment) { FactoryGirl.create(:payment, created_at: 1.day.ago) }

  describe '.report' do
    subject(:mail) { described_class.report(payments) }
    let(:payments) { Payment.for_yesterday }
    let(:email) { 'test@astroheim.com' }

    it 'is sent to payments report email' do
      Settings.payments_report_email = email
      expect(mail.to).to include(email)
    end
  end

  describe '.receipt', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
    subject(:mail) { described_class.receipt(recipient, payment) }
    let(:recipient) { 'foo@bar.com' }
    let(:order_id)  { payment.order_id }
    let!(:usa_currency_code) { '$' }

    it '#to' do
      expect(mail.to).to include(recipient)
    end

    it 'includes the payment attributes' do
      expect(mail.body).to include_in_each_part(payment.address_zip)
      expect(mail.body).to include_in_each_part(order_id)
      expect(mail.body).to include_in_each_part('60007')
    end

    it 'includes nonreimbursable-fees' do
      expect(mail.body).to include_in_each_part('D M')
      expect(mail.body).to include_in_each_part("#{usa_currency_code} 105")
      expect(mail.body).to_not include_in_each_part('AR-DS')
      expect(mail.body).to_not include_in_each_part('21D 250M')
    end

    it 'DOES NOT include total nonreimbursable-fees' do
      expect(mail.body).to_not include_in_each_part("#{usa_currency_code} 245")
    end

    it 'includes vehicle information' do
      expect(mail.body).to include_in_each_part(promise_purchase.vnum)
      expect(mail.body).to include_in_each_part(promise_purchase.make)
    end

    it 'has correct from and bcc addresses' do
      expect(mail.from.join).to eq(Astro::NO_REPLY_EMAIL)
      expect(mail.bcc).to include(Astro::ACCOUNTING_EMAIL)
      expect(mail.bcc).to include(Astro::SUPPORT_EMAIL)
    end
  end
end
