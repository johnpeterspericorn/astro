# encoding: utf-8
require 'requests_helper'

describe AuditMailer do
  let(:vnum)           { 'A123B' }
  let(:email)         { 'foo@bar.com, bar@foo.com' }
  let(:agreement_no)     { '11111' }
  let(:reason)        { double(description: 'ABC') }
  let(:reasons)       { [reason] }
  let(:invoice)       { double :return_invoice }
  let(:certificate)   { OpenStruct.new vnum: vnum, agreement_no: agreement_no }

  describe '#notify_opening_audit' do
    subject(:mail) { described_class.notify_opening_audit(invoice) }

    before do
      allow(invoice).to receive(:return_certificate).and_return(certificate)
      allow(invoice).to receive(:audit_reasons).and_return(reasons)
      allow(invoice).to receive(:audit_emails).and_return(email)
    end

    it '#subject' do
      expect(mail.subject).to include(vnum)
    end

    it '#to' do
      expect(mail.to).to include('foo@bar.com')
      expect(mail.to).to include('bar@foo.com')
    end

    describe 'the mail body' do
      it { is_expected.to include_in_each_part(vnum) }
      it { is_expected.to include_in_each_part(agreement_no) }
      it { is_expected.to include_in_each_part(reason.description) }
    end
  end

  describe '#notify_clearing_audit' do
    subject(:mail) { described_class.notify_return_audit_cleared(invoice) }

    before do
      allow(invoice).to receive(:return_certificate).and_return(certificate)
      allow(invoice).to receive(:audit_emails).and_return(email)
    end

    it '#subject' do
      expect(mail.subject).to include(vnum)
    end

    it '#to' do
      expect(mail.to).to include('foo@bar.com')
      expect(mail.to).to include('bar@foo.com')
    end

    describe 'the mail body' do
      it { is_expected.to include_in_each_part(vnum) }
      it { is_expected.to include_in_each_part(agreement_no) }
    end
  end

  describe '#notify_return_audit_flagged' do
    subject(:mail) { described_class.notify_return_audit_flagged(invoice) }

    before do
      allow(invoice).to receive(:return_certificate).and_return(certificate)
      allow(invoice).to receive(:audit_reasons).and_return([])
      allow(invoice).to receive(:audit_emails).and_return(email)
    end

    it '#subject' do
      expect(mail.subject).to include(vnum)
    end

    it '#to' do
      expect(mail.to).to include('foo@bar.com')
      expect(mail.to).to include('bar@foo.com')
    end

    describe 'the mail body' do
      it { is_expected.to include_in_each_part(vnum) }
      it { is_expected.to include_in_each_part(agreement_no) }

      it 'displays reasons if present' do
        allow(invoice).to receive(:audit_reasons).and_return(reasons)
        expect(mail.body).to include_in_each_part(reason.description)
      end
    end
  end
end
