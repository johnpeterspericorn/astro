# encoding: utf-8
require 'requests_helper'

describe PromisePurchaseMailer do
  let(:email) { 'foo@bar.com' }
  let(:another_email) { 'baz@bar.com' }
  let (:agreement_info) do
    {
      pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER,
      badge_no:          SasDataHelper::DEFAULT_BADGE_NUMBER,
      agreement_no:         SasDataHelper::DEFAULT_DEALER_NUMBER,
      email:             [email, another_email].join(',')
    }
  end

  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA') }
  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA') }
  before do
    purchase_promise_for_vehicle acura
    display_vehicle_for_offer honda
  end
  let(:sold_vehicle) { PromiseOffer.by_make('ACURA').first.decorate }

  let(:promise_purchase)  { PromisePurchase.first.decorate }
  let(:promise_purchases) { [promise_purchase] }

  describe '#notify_pending' do
    let(:promise_offer) { promise_purchase.promise_offer }
    subject(:mail) { described_class.notify_pending(promise_purchase) }

    describe 'the mail body' do
      it { is_expected.to include_in_each_part promise_offer.agreement_no }
      it { is_expected.to include_in_each_part promise_offer.pawn_access_no }
      it { is_expected.to include_in_each_part promise_offer.badge_no }
    end
  end

  describe 'purchase_confirmation for an offer purchase' do
    subject(:mailer) do
      described_class.purchase_confirmation(
        promise_purchases:  promise_purchases,
        user_email:           agreement_info[:email]
      )
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to include acura.vnum }
    end
  end

  context 'purchase_confirmation for total protect' do
    let(:purchase) { PromisePurchase.first.decorate }

    let!(:mailer) do
      described_class.purchase_confirmation(
        promise_purchases:  [purchase],
        user_email:           agreement_info[:email]
      )
    end

    subject(:mail) { mailer.html_part.body.to_s }

    before do
      purchase.update_attributes(kind: PromisePurchase::TOTAL_PROTECT)
    end

    it { expect(mail).to have_content(purchase.first_window_min_return_time) }
    it { expect(mail).to have_content(purchase.first_window_max_return_time) }
    it { expect(mail).to have_content(purchase.second_window_min_return_time) }
    it { expect(mail).to have_content(purchase.second_window_max_return_time) }
  end

  describe 'purchase_confirmation for multiple purchases' do
    let(:inspection_purchase)    { sold_vehicle.inspection_purchase }
    let(:inspection_purchases)   { InspectionPurchase.all }
    let(:acura_inspection_price) { 123 }
    let(:honda_inspection_price) { acura_inspection_price + 10 }

    subject(:mailer) do
      described_class.purchase_confirmation(
        promise_purchases:  promise_purchases,
        inspection_purchases: inspection_purchases,
        user_email:           agreement_info[:email]
      )
    end

    before do
      purchase_inspection_for_offer acura.promise_offers.first, acura_inspection_price
      purchase_inspection_for_offer honda.promise_offers.first, honda_inspection_price
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include Astro::PROTECTED_EMAIL }
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include email }
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include another_email }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to include I18n.t('promise_purchase.multiple_purchase_confirmation.subject') }
    end

    describe 'the mail body' do
      let(:promise_offer) { acura.promise_offers.first }
      let(:promise_purchase) { promise_offer.promise_purchase }
      let!(:pawn_location) { FactoryGirl.create(:svaa_location) }

      subject { mailer }

      it { is_expected.to include_in_each_part agreement_info[:badge_no] }
      it { is_expected.to include_in_each_part agreement_info[:badge_no] }
      it { is_expected.to include_in_each_part agreement_info[:agreement_no] }
      it { is_expected.to include_in_each_part agreement_info[:pawn_access_no] }

      it { is_expected.to include_in_each_part pawn_location.name }
      it { is_expected.to include_in_each_part promise_purchase.max_return_time.to_s(:slashed) }

      it { is_expected.to include_in_each_part sold_vehicle.full_description }
      it { is_expected.to include_in_each_part sold_vehicle.vnum }
      it { is_expected.to include_in_each_part sold_vehicle.odometer_reading }
      it { is_expected.to include_in_each_part sold_vehicle.lane_no }
      it { is_expected.to include_in_each_part sold_vehicle.run_no }
      it { is_expected.to include_in_each_part sold_vehicle.purchase_price }

      it { is_expected.to include_in_each_part acura_inspection_price }
      it { is_expected.to include_in_each_part honda_inspection_price }
      it { is_expected.to include_in_each_part 'To initiate a return for this vehicle' }
      it { expect { promise_purchase.first_window_min_return_time }.to raise_error }
      it { expect { promise_purchase.first_window_max_return_time }.to raise_error }
      it { expect { promise_purchase.second_window_min_return_time }.to raise_error }
      it { expect { promise_purchase.second_window_max_return_time }.to raise_error }
    end
  end

  describe '#pending_confirmation' do
    let(:user_email) { 'test@test' }
    let(:pending_bundled) { double(PromisePurchase, bundled_purchase: promise_purchase) }

    subject(:mail) { described_class.pending_confirmation([pending_bundled], [], user_email) }

    it 'is sent to the user email' do
      expect(mail.to).to include(user_email)
    end
  end

  describe '#notify_api_error' do
    let(:vnum) { 'AAA000' }
    let(:request) { double }
    let(:return_code) { 12 }

    subject(:mail) do
      described_class.notify_api_error(
        request,
        return_code
      )
    end

    before do
      allow(request).to receive(:[]).with('vnum') { vnum }
    end

    it 'includes vnum in subject' do
      expect(mail.subject).to include(vnum)
    end

    it 'is sent to MAS' do
      expect(mail.to).to include(Astro::MAS_EMAIL)
    end

    it 'includes return code in body content' do
      expect(mail.body).to include(I18n.t('purchase_api.return_code')[return_code])
    end
  end
end
