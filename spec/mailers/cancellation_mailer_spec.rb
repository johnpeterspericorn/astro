# encoding: utf-8
require 'spec_helper'

describe CancellationMailer do
  let(:vnum) { 'JTJGF10U710110628' }
  # let(:full_description)    { 'a full description' }
  let(:buyer_email)         { 'buyer@caragreement.com' }
  let(:location_initials)   { 'ALAA' }
  let(:pawn_access_no)   { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:pawn_location) { FactoryGirl.create(:pawn_location, initials: location_initials) }
  let(:vehicle_information) { FactoryGirl.create(:vehicle_information, vnum: vnum, odometer_reading: 1234) }
  let(:pawn_information) { FactoryGirl.create(:pawn_information, lane_no: '1', run_no: '2', pawn_access_no: pawn_access_no, badge_no: '1', agreement_no: '500001', pawn_location: pawn_location) }
  let(:promise_offer)     { FactoryGirl.create(:promise_offer, pawn_information: pawn_information, vehicle_information: vehicle_information, emails: [buyer_email]) }
  let(:promise_purchase)  { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:reason) { 'A very good reason' }

  describe '#cancellation_confirmation_eligible_for_repurchase' do
    subject (:mailer) do
      described_class.cancellation_confirmation_eligible_for_repurchase(promise_offer, promise_purchase, reason)
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include Astro::PROTECTED_EMAIL }
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include buyer_email }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match location_initials }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match pawn_access_no }
    end

    # it { is_expected.to include_in_each_part promise_offer.full_description }
    it { is_expected.to include_in_each_part vnum }
  end

  describe '#cancellation_confirmation_ineligible_for_repurchase' do
    subject (:mailer) do
      described_class.cancellation_confirmation_ineligible_for_repurchase(promise_offer, promise_purchase, reason)
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include Astro::PROTECTED_EMAIL }
    end

    describe '#to' do
      subject { super().to }
      it { is_expected.to include buyer_email }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match location_initials }
    end

    describe '#subject' do
      subject { super().subject }
      it { is_expected.to match pawn_access_no }
    end

    # it { is_expected.to include_in_each_part promise_offer.full_description }
    it { is_expected.to include_in_each_part vnum }
    it { is_expected.to include_in_each_part reason }
  end
end
