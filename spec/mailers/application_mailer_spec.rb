# encoding: utf-8
require 'spec_helper'

describe ApplicationMailer do
  class TestMailer < ApplicationMailer
    def test_timezone
      mail(subject: 'Testing Timezone', body: Time.zone.name.to_s)
    end
  end

  describe 'mailer timezone' do
    it 'should be Eastern Time' do
      expect(TestMailer.test_timezone.body.to_s).to eq('Eastern Time (US & Canada)')
    end
  end
end
