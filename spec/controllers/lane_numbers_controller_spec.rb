# encoding: utf-8
require 'requests_helper'

describe LaneNumbersController do
  describe '#index' do
    let(:initials) { 'SVAA' }

    before do
      stub_buyer_authorization
      sign_in
    end

    context 'with a valid location' do
      let(:pawn_location) { PawnLocation.new }
      let(:lane_number) { '42' }

      before do
        allow(PresaleVehicleDecorator).to receive(:lanes_at_location).with('SVAA').and_return([lane_number])

        get :index, location_initials: initials
      end

      it 'succeeds with a valid pawn location' do
        expect(response).to be_success
      end

      it 'includes the provided lanes in the response' do
        expect(response.body).to include lane_number
      end
    end

    context 'when no location is found' do
      it 'returns not found' do
        get :index, location_initials: ''

        expect(response).to be_not_found
      end
    end
  end
end
