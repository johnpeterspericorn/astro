# encoding: utf-8
require 'requests_helper'

describe TransactionsController do
  let(:user) { FactoryGirl.create(:buyer_user, agreement_transaction_viewer: true) }
  let(:json) { JSON.parse(response.body) }
  let(:agreement_number) { '5000000' }
  let(:transactions) { [{ foo: 123 }] }
  let(:attributes) { { agreement_number: agreement_number, returned: nil, days: 90, offset: 0, limit: 10, active: true, location_initial: nil, days_left: nil } }

  let(:attributes1) { { agreement_number: '100000001', returned: nil, days: 90, offset: 0, limit: 10, active: true, location_initial: nil, days_left: nil } }
  describe 'GET #index' do
    before :each do
      sign_in(user)
      allow(Transaction).to receive(:by_agreement_number).with(attributes).and_return(transactions)
      allow(Transaction).to receive(:by_agreement_number).with(attributes1).and_return([])
    end

    it 'returns an array of transactions associated with agreement_number' do
      get :index, format: :json, agreement_number: agreement_number
      expect(json.size).to eq(1)
      record = json.first
      expect(record['foo']).to eq(123)

      get :index, format: :json, agreement_number: '100000001'
      json = JSON.parse(response.body)
      expect(json.size).to eq(0)
    end
  end
end
