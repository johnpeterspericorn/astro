# encoding: utf-8
require 'spec_helper'

describe ApplicationController, type: :controller do
  controller do
    skip_filter :authenticate_user_with_token!, :authenticate_user!, :set_alerts
    def index
      render text: 'foo'
    end

    def timezone_test
      render text: Time.zone.to_s
    end
  end

  describe 'set_locale' do
    it 'updates the locale' do
      expect(I18n.locale).to eq(:en)
      get :index, locale: 'fr-CA'
      expect(response).to be_success
      expect(response.body).to eq('foo')
      expect(I18n.locale).to eq(:'fr-CA')
    end
  end

  describe 'set_time_zone' do
    it 'updates timezone based on timezone offset' do
      routes.draw { get 'timezone_test' => 'anonymous#timezone_test' }
      ist_tz_offset_minutes = 330
      allow_any_instance_of(ApplicationController).to receive(:js_timezone_offset).and_return(ist_tz_offset_minutes)

      get :timezone_test
      expect(response.body).to have_content('GMT+05:30')
    end
  end
end
