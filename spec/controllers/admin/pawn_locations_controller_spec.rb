# encoding: utf-8
require 'spec_helper'

describe Admin::PawnLocationsController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user) }

  before do
    sign_in(user)
  end

  describe 'GET /' do
    describe 'CSV output' do
      let (:location_name)     { 'Drive' }
      let (:location_initials) { 'DRIV' }
      let (:return_emails) { ['return1@astroheim.com', 'return2@astroheim.com'] }
      let!(:pawn_location) { PawnLocation.create!(name: location_name, initials: location_initials, return_emails: return_emails) }

      specify do
        get :index, format: :html
        expect(response).to be_success
        expect(response.body).to have_content location_name
        expect(response.body).to have_content location_initials
      end

      specify do
        get :index, format: :csv
        expect(response).to be_success
        expect(response.body).to have_content 'id'
        expect(response.body).to have_content 'name'
        expect(response.body).to have_content 'initials'
        expect(response.body).to have_content 'inspections_enabled'
        expect(response.body).to have_content 'return_emails'
        expect(response.body).to have_content location_name
        expect(response.body).to have_content location_initials
        expect(response.body).to have_content return_emails.join(',')
      end
    end
  end
end
