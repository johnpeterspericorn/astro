# encoding: utf-8
require 'spec_helper'

describe Admin::MarketIndicesController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user) }
  before do
    sign_in(user)
  end

  describe '#show' do
    describe 'existing' do
      let!(:market_index) { FactoryGirl.create(:market_index) }

      it 'shows the available market_index' do
        get :show
        expect(response.body).to include(market_index.notes)
      end
    end

    describe 'not existing' do
      it 'creates new market_index resource if not found' do
        expect(MarketIndex).to receive(:create)

        get :show
      end
    end
  end

  describe '#update' do
    let!(:market_index) { FactoryGirl.create(:market_index) }

    it 'updates the value of market_index' do
      patch :update, market_index: { value: 99 }
      market_index.reload

      expect(assigns(:market_index)).to eq(market_index)
      expect(market_index.value).to eq(99)
    end
  end
end
