# encoding: utf-8
require 'spec_helper'
describe Admin::FloorPlanCompaniesController do
  render_views

  let!(:floor_plan_company) { FactoryGirl.create(:floor_plan_company) }
  let(:user) { FactoryGirl.build(:admin_user, finance_admin_viewer: true) }

  before do
    sign_in(user)
  end

  it 'succeeds' do
    get :index
    expect(response).to be_success
  end
end
