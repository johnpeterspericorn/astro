# encoding: utf-8
require 'spec_helper'

describe Admin::FeatureFlagsController do
  let(:user) { FactoryGirl.build(:admin_user, feature_flag_editor: true) }

  before do
    sign_in(user)
  end

  describe 'POST /' do
    it 'should toggle feature flags' do
      Settings.feature = false
      post :create, settings: { feature: '1' }
      expect(response).to be_success
      expect(Settings['feature']).to be_truthy
      post :create, settings: { feature: '0' }
      expect(Settings['feature']).to be_falsey
    end
  end
end
