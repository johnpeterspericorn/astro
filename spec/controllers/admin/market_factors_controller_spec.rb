# encoding: utf-8
require 'spec_helper'

describe Admin::MarketFactorsController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user) }
  before do
    sign_in(user)
  end

  describe '#create' do
    let(:market_factor) { FactoryGirl.build(:market_factor, title: 'This is the title') }

    it 'creates a new market_factor' do
      put :create, market_factor: market_factor.attributes

      expect(MarketFactor.last.title).to eq('This is the title')
    end
  end

  describe '#update' do
    let!(:market_factor) { FactoryGirl.create(:market_factor) }

    it 'updates the value of market_factor' do
      patch :update, market_factor: { title: 'New Title' }, id: market_factor.id
      market_factor.reload

      expect(assigns(:market_factor)).to eq(market_factor)
      expect(market_factor.title).to eq('New Title')
    end
  end
end
