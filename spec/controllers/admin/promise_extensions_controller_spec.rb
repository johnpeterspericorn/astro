# encoding: utf-8
require 'spec_helper'

describe Admin::PromiseExtensionsController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user, promise_admin_editor: true) }

  before do
    sign_in(user)
  end

  describe '#index' do
    let!(:promise_extension) { FactoryGirl.create(:promise_extension) }

    it 'should have all promise extensions in the list' do
      get :index

      expect(response).to be_success
      expect(response.body).to have_content promise_extension.vnum
    end
  end

  describe '#import' do
    it 'should import promise extensions from csv' do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/promise_extensions.csv'), 'text/csv')

      params = {}
      params['extensions_file'] = file
      post :import, import: params

      expect(response).to redirect_to(admin_promise_extensions_path)
      expect(PromiseExtension.count).to eql(2)
    end
  end
end
