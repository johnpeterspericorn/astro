# encoding: utf-8
require 'spec_helper'

describe Admin::ReturnInvoicesController do
  let(:user) { FactoryGirl.create(:admin_user) }
  let(:return_invoice) { FactoryGirl.create(:return_invoice) }
  let(:mailer_stub) { double }

  describe '#update' do
    context 'a kiosk user changing the pawn location' do
      before do
        sign_in user
      end

      it 'should not process drop notice if pawn location did not change' do
        expect(mailer_stub).to_not receive(:deliver_later)
        expect_any_instance_of(ReturnInvoice).not_to receive(:update_drop_notice)
        return_invoice.promise_purchase = FactoryGirl.create(:returnable_promise_purchase)
        put :update, id: return_invoice.id, return_invoice: return_invoice.attributes
      end

      it 'should process drop notice if pawn location did change' do
        expect(mailer_stub).to receive(:deliver_later)
        expect(ReturnInvoiceMailer).to receive(:return_confirmation).with(anything).and_return(mailer_stub)
        expect_any_instance_of(ReturnInvoice).to receive(:update_drop_notice)
        return_invoice.pawn_location = FactoryGirl.create(:oklahoma_location)
        put :update, id: return_invoice.id, return_invoice: return_invoice.attributes
      end
    end
  end
end
