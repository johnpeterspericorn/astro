# encoding: utf-8
require 'spec_helper'

describe Admin::PendingOffersController do
  let(:user) { FactoryGirl.build(:admin_user) }

  before do
    sign_in(user)
  end

  describe '#index' do
    context 'CSV format' do
      it 'succeeds' do
        expect(response).to be_success
      end

      it 'gets pending offer CSV data' do
        expect_any_instance_of(PendingOfferCsv).to receive(:to_s)
        get :index, format: :csv
      end
    end
  end
end
