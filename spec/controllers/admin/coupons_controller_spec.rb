# encoding: utf-8
require 'spec_helper'

describe Admin::CouponsController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user) }
  let!(:coupon) { FactoryGirl.create(:coupon).decorate }

  before do
    sign_in(user)
  end

  describe 'GET /' do
    describe 'CSV output' do
      specify do
        get :index, format: :html
        expect(response).to be_success
        expect(response.body).to have_content coupon.code
        expect(response.body).to have_content coupon.expiration_date
      end

      specify do
        get :index, format: :csv
        expect(response).to be_success
        expect(response.body).to have_content coupon.code
        expect(response.body).to have_content coupon.id
        expect(response.body).to have_content coupon.expiration_date
        expect(response.body).to have_content coupon.amount
        expect(response.body).to have_content coupon.name
      end
    end
  end
end
