# encoding: utf-8
require 'spec_helper'

describe Admin::UsersController do
  render_views

  let(:user) { FactoryGirl.build(:admin_user, user_admin_editor: true) }

  before do
    sign_in(user)
  end

  describe 'GET /' do
    describe 'CSV output' do
      let(:pawn_location) { PawnLocation.create(name: 'Drive', initials: 'DRIV') }
      let(:superuser_name) { 'astroDRIV' }
      let(:superuser_email) { 'super@astroheim.local' }
      let(:kioskuser_name) { 'swipeDRIV' }
      before do
        User.create!(super_user: true, username: superuser_name, location_initials: pawn_location.initials, email: superuser_email)
        User.create!(kiosk_user: true, username: kioskuser_name, location_initials: pawn_location.initials)
      end

      specify do
        get :index, format: :html, '/missioncontrol/users' => { 'search' => 'DRIV' }
        expect(response).to be_success
        expect(response.body).to have_content superuser_name.downcase
        expect(response.body).to have_content superuser_email
        expect(response.body).to have_content kioskuser_name.downcase
      end

      specify do
        get :index, format: :csv, '/missioncontrol/users' => { 'search' => 'DRIV' }
        expect(response).to be_success
        expect(response.body).to have_content 'username'
        expect(response.body).to have_content 'location_initials'
        expect(response.body).to have_content 'active'
        expect(response.body).to have_content 'email'
        expect(response.body).to have_content superuser_name.downcase
        expect(response.body).to have_content superuser_email
        expect(response.body).to have_content kioskuser_name.downcase
      end
    end
  end
end
