# encoding: utf-8
require 'spec_helper'

describe FlatRateOfferBatches::ApprovalsController do
  let(:id) { 42 }
  let(:flat_rate_offer_batch) { double(approve: nil, reject: nil, pawn_access_no: nil) }

  before do
    allow(FlatRateOfferBatch).to receive(:find) { flat_rate_offer_batch }
    allow(flat_rate_offer_batch).to receive(:valid_offers?) { true }

    user = double(:user).as_null_object
    allow(user).to receive(:pawn_access_number)

    sign_in(user)
  end

  describe '#show' do
    let(:flat_rate_offer_batch) { FactoryGirl.create(:flat_rate_offer_batch_with_offer) }

    before do
      user = double(:user).as_null_object
      sign_in(user)
    end

    it 'show offers even if the batch is expired' do
      agreement_numbers = flat_rate_offer_batch.offered_agreement_numbers
      allow(flat_rate_offer_batch).to receive(:eligible_agreement_numbers).and_return(agreement_numbers)
      flat_rate_offer_batch.update_attributes!(expiration_date: 1.day.ago)

      get :show, flat_rate_offer_batch_id: flat_rate_offer_batch.id
      expect(response.status).to eq(200)
    end

    it 'shows error message if batch has no offers' do
      allow(flat_rate_offer_batch).to receive(:flat_rate_offers).and_return([])

      get :show, flat_rate_offer_batch_id: flat_rate_offer_batch.id
      expect(response).to redirect_to(root_path)
    end
  end

  describe '#create' do
    let(:today) { Date.current }
    let(:day)   { today.day    }
    let(:month) { today.month  }
    let(:year)  { today.year   }

    context 'when the batch is approved' do
      before do
        allow_any_instance_of(FlatRateOfferBatchApproval).to receive(:approve).and_return(true)
        allow_any_instance_of(FlatRateOfferBatches::ApprovalsController).to receive(:generate_offer_pdf).and_return(true)
      end

      it 'redirects after success' do
        post :create, flat_rate_offer_batch_id: id, offer_effective_date: { 'date(1i)' => year.to_s, 'date(2i)' => month.to_s, 'date(3i)' => day.to_s }
        expect(response).to be_redirect
      end

      it 'renders a success message' do
        post :create, flat_rate_offer_batch_id: id, offer_effective_date: { 'date(1i)' => year.to_s, 'date(2i)' => month.to_s, 'date(3i)' => day.to_s }
        expect(flash[:success]).to be_present
      end
    end

    context 'when acceptance fails' do
      before do
        allow_any_instance_of(FlatRateOfferBatchApproval).to receive(:approve).and_return(false)
        allow_any_instance_of(FlatRateOfferBatches::ApprovalsController).to receive(:generate_offer_pdf).and_return(true)
      end

      it 'renders an error' do
        post :create, flat_rate_offer_batch_id: id, offer_effective_date: { 'date(1i)' => year.to_s, 'date(2i)' => month.to_s, 'date(3i)' => day.to_s }
        expect(flash[:error]).to be_present
      end
    end
  end

  describe '#destroy' do
    context 'when the batch is rejected' do
      before do
        allow_any_instance_of(FlatRateOfferBatchApproval).to receive(:reject).and_return(true)
      end

      it 'redirects after success' do
        delete :destroy, flat_rate_offer_batch_id: id
        expect(response).to be_redirect
      end

      it 'renders a success message' do
        delete :destroy, flat_rate_offer_batch_id: id
        expect(flash[:success]).to be_present
      end
    end

    context 'when rejection fails' do
      before do
        allow_any_instance_of(FlatRateOfferBatchApproval).to receive(:reject).and_return(false)
      end

      it 'renders an error' do
        delete :destroy, flat_rate_offer_batch_id: id
        expect(flash[:error]).to be_present
      end
    end
  end
end
