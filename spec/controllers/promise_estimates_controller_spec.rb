# encoding: utf-8
require 'requests_helper'

describe Quotes::PromiseEstimatesController do
  let(:current_user) { double('user', buyer_user?: false) }
  before do
    request.env['HTTP_REFERER'] = 'back'
    sign_in current_user
  end

  describe 'GET index' do
    context 'when missing required params' do
      context 'missing VNUM' do
        before do
          get :index, vnum_suffix: nil, format: :json
        end

        it 'redirects to the research path with an error' do
          expect(flash[:error]).to eql I18n.t('promise_estimates.missing_info')
          expect(response).to be_redirect
        end
      end

      context 'missing all params' do
        before do
          get :index, format: :json
        end

        it 'redirects to the research path with an error' do
          expect(flash[:error]).to eql I18n.t('promise_estimates.missing_info')
          expect(response).to be_redirect
        end
      end
    end

    context 'JSON' do
      let!(:ineligibility_condition) { IneligibilityCondition.create!(condition_id: 1, notice: 'foobar') }
      let!(:presale_vehicle) { FactoryGirl.create(:presale_vehicle) }
      let(:vnum_suffix) { presale_vehicle.vnum[-6..-1] }

      subject (:parsed_json) { JSON.parse(response.body) }

      context 'VNUM suffix searching' do
        before do
          get :index, vnum_suffix: vnum_suffix, format: :json
        end

        it 'should be successful' do
          expect(response).to be_success
        end

        it 'should include the vehicle make' do
          expect(parsed_json['vehicle_data'].first['make']).to eq(presale_vehicle.make)
        end

        it 'should include an array of ineligibility conditions' do
          expect(parsed_json['ineligibility_conditions']).to be_present
        end
      end

      context 'full VNUM searching' do
        before do
          get :index, vnum: presale_vehicle.vnum, format: :json
        end

        subject (:parsed_json) { JSON.parse(response.body) }

        it 'should be successful' do
          expect(response).to be_success
        end

        it 'should include the vehicle make' do
          expect(parsed_json['vehicle_data'].first['make']).to eq(presale_vehicle.make)
        end

        it 'should include an array of ineligibility conditions' do
          expect(parsed_json['ineligibility_conditions']).to be_present
        end
      end
    end
  end

  describe '.print_page_count' do
    let(:presale_vehicle_filter) { double }

    before do
      allow(controller).to receive(:presale_vehicle_filter) { presale_vehicle_filter }
    end

    it 'should return one page for fewer presale vehicles than configured per page' do
      allow(presale_vehicle_filter).to receive(:filtered).and_return(double(:ar_proxy, count: Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PRINT_PAGE - 1))
      expect(subject.send(:print_page_count)).to eq(1)
    end

    it 'should return two pages for more presale vehicles than configured per page' do
      allow(presale_vehicle_filter).to receive(:filtered).and_return(double(:ar_proxy, count: Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PRINT_PAGE + 1))
      expect(subject.send(:print_page_count)).to eq(2)
    end
  end
end
