# encoding: utf-8
require 'requests_helper'

describe AgreementsController do
  before do
    stub_buyer_authorization
    sign_in
  end

  let(:user) { FactoryGirl.create(:api_user) }
  let(:json) { JSON.parse(response.body) }
  let(:pawn_access_number) { '100037609' }
  let(:agreement_no) { '5241420' }

  describe 'GET #index' do
    it 'returns an array of agreements associated with pawn_access_number' do
      get :index, format: :json, pawn_access_number: pawn_access_number
      expect(json.size).to eq(1)
      record = json.first
      expect(record['agreementship_name']).to eq('A Great Agreement')
      expect(record['agreement_no']).to eq(agreement_no)
    end
  end
end
