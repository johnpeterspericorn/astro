# encoding: utf-8
require 'requests_helper'

describe MarketIndicesController do
  let!(:market_index) { FactoryGirl.create(:market_index) }
  let!(:market_factor1) { FactoryGirl.create(:market_factor, title: 'ABC', market_index: market_index) }
  let!(:market_factor2) { FactoryGirl.create(:market_factor, title: 'LMN', market_index: market_index) }

  let(:json) { JSON.parse(response.body) }

  before do
    sign_in
  end

  describe '#show' do
    it 'should show market index details and factors' do
      get :show, format: :json

      factor1 = json['market_factors'].first
      factor2 = json['market_factors'].last

      expect(json['notes']).to eq(market_index.notes)
      expect(factor1['title']).to eq('ABC')
      expect(factor2['title']).to eq('LMN')
    end
  end
end
