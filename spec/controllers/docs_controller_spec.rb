# encoding: utf-8
require 'requests_helper'

describe DocsController do
  describe 'GET #show' do
    let(:user) { FactoryGirl.build(:super_user) }

    it 'returns api doc pages' do
      sign_in(user)
      get :show, path: 'index.html'

      expect(response).to be_success
    end

    context 'with unauthorized users' do
      it 'fails' do
        sign_in(user)
        expect_any_instance_of(DocPolicy).to receive(:show?) { false }

        get :show, path: 'index.html'

        expect(response).to_not be_success
      end
    end
  end
end
