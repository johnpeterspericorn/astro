# encoding: utf-8
require 'requests_helper'

describe ReturnInvoicesController do
  let(:kiosk_user)  { User.new(kiosk_user: true, username: 'kiosk') }
  let(:buyer_user)  { User.new(buyer_user: true, username: 'buyer') }
  let(:super_user)  { User.new(super_user: true, username: 'super', email: 'super@astro.local') }
  let(:not_allowed) { I18n.t('not_allowed') }

  let(:admin_user) { FactoryGirl.create(:admin_user) }
  let(:return_invoice) { FactoryGirl.create(:return_invoice) }
  let(:mailer_stub) { double }

  describe 'GET #edit' do
    context 'kiosk user' do
      let(:user) { kiosk_user }

      before do
        sign_in user
        get :edit, id: return_invoice.id
      end

      it 'redirects to root' do
        expect(response).to redirect_to(root_path)
      end

      it "says you're not allowed" do
        expect(flash[:alert]).to eq(not_allowed)
      end
    end

    context 'buyer user' do
      let(:user) { buyer_user }

      before do
        sign_in user
        get :edit, id: return_invoice.id
      end

      it 'redirects to root' do
        expect(response).to redirect_to(root_path)
      end

      it "says you're not allowed" do
        expect(flash[:alert]).to eq(not_allowed)
      end
    end
  end

  describe 'PUT #update' do
    context 'kiosk user' do
      let(:user) { kiosk_user }

      before do
        sign_in user
        put :update, id: return_invoice.id
      end

      it 'redirects to root' do
        expect(response).to redirect_to(root_path)
      end

      it "says you're not allowed" do
        expect(flash[:alert]).to eq(not_allowed)
      end
    end

    context 'buyer user' do
      let(:user) { buyer_user }

      before do
        sign_in user
        put :update, id: return_invoice.id
      end

      it 'redirects to root' do
        expect(response).to redirect_to(root_path)
      end

      it "says you're not allowed" do
        expect(flash[:alert]).to eq(not_allowed)
      end
    end

    context 'astro user changing pawn location' do
      before do
        sign_in super_user
      end

      it 'should process drop notice if pawn location did change' do
        expect(mailer_stub).to receive(:deliver_later)
        expect(ReturnInvoiceMailer).to receive(:return_confirmation).with(anything).and_return(mailer_stub)
        expect_any_instance_of(ReturnInvoice).to receive(:update_drop_notice)
        return_invoice.pawn_location = FactoryGirl.create(:oklahoma_location)
        put :update, id: return_invoice.id, return_invoice: return_invoice.attributes
      end
    end
  end
end
