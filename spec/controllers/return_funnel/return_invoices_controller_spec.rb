# encoding: utf-8
require 'requests_helper'

describe ReturnFunnel::ReturnInvoicesController do
  before do
    allow_any_instance_of(Autoims::CLIENT).to receive(:notify_of_return).and_return(true)
  end

  describe '#create' do
    let(:buyer_user)         { User.new(buyer_user: true, username: 'Testy') }
    let(:pawn_access_no)  { '100123123' }
    let(:agreement_no)          { '5000001'   }

    context 'Floor Plan Company with holds_title true' do
      let(:floor_company) { FactoryGirl.create(:floor_plan_company, holds_title: true) }
      let(:branch) { FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_company) }

      let(:purchase) { FactoryGirl.create(:returnable_promise_purchase) }
      let(:return_params) {
        {
          floor_plan_branch_id: branch.id,
          buyer_and_agreement_information_form: { emails: "abc@example.com" },
          return_invoice: {
            mechanical_issues: "1",
            cosmetic_issues: "0",
            unable_to_sell: "0",
            changed_mind: "0",
            other: "0",
            no_answer: "0",
            additional_information: "",
            estimated_cost: "0.0",
            odometer_on_return: "1020",
            location_initials: "AAAA",
            title_status: "2",
            left_lot: ""
          },
          promise_purchase_id: purchase.id
        }
      }

      before do
        sign_in buyer_user
        allow_any_instance_of(ReturnInvoicePolicy).to receive(:create?) { true }
      end

      it 'will create return invoice with title received' do
        post :create, return_params
        return_invoice = assigns(:return_invoice)
        expect(return_invoice.title_received).to be_truthy
      end
    end
  end

  describe 'finalize_return step' do
    context 'ineligibility' do
      let(:sold_vehicle) { SoldVehicle.new }
      let(:promise_purchase) { PromisePurchase.new }
      let(:super_user) { FactoryGirl.create(:super_user) }
      let(:promise_offer) { double(decorate: double, pawn_access_no: 4321, agreement_no: 1234) }

      before do
        request.env['HTTP_REFERER'] = new_return_funnel_return_invoice_path
        sign_in super_user
      end

      it 'fails if the promise is ineligible', vcr: { cassette_name: 'easypost/verified_address' } do
        allow(promise_purchase).to receive(:promise_offer) { promise_offer }
        allow(promise_purchase).to receive(:eligible_for_return?) { false }
        allow(PromisePurchase).to receive(:find).and_return(promise_purchase)
        allow(promise_purchase).to receive(:country).and_return(SupportedCountry::DEFAULT)
        allow(promise_offer).to receive(:pplv?).and_return(false)
        allow(promise_offer).to receive(:seller_paid).and_return(true)
        allow_any_instance_of(ReturnInvoice).to receive(:promise_offer).and_return(promise_offer)
        expect do
          post :create, id: :finalize_return, return_invoice: { odometer_on_return: 0 }, buyer_and_agreement_information_form: { some: :param }
        end.to_not change(ReturnInvoice, :count)
      end
    end

    context 'Promise purchase authorization' do
      let(:buyer_user)         { User.new(buyer_user: true, username: 'Testy') }
      let(:pawn_access_no)  { '100123123' }
      let(:agreement_no)          { '5000001'   }

      let(:pawn_information) { PawnInformation.new(pawn_access_no: pawn_access_no, agreement_no: agreement_no) }

      let(:promise_offer)    { PromiseOffer.new(pawn_information: pawn_information) }
      let(:promise_purchase) { PromisePurchase.new(promise_offer: promise_offer) }

      before do
        sign_in buyer_user
        ReturnInvoiceMailer.stub_chain(:return_confirmation, :deliver_now)
        ReturnInvoiceMailer.stub_chain(:pawn_location_return_notification, :deliver_now)
        allow(PromisePurchase).to receive(:find).and_return(promise_purchase)
        allow_any_instance_of(ReturnInvoice).to receive(:finalize)
        allow(promise_purchase).to receive(:paid?) { true }
        allow_any_instance_of(BuyerAndAgreementInformationForm).to receive(:valid?).and_return(true)
      end

      context 'when the user is not authorized to return the promise' do
        before do
          expect_any_instance_of(ReturnInvoicePolicy).to receive(:create?) { false }
        end

        it 'does not complete the return' do
          allow(promise_offer).to receive(:pplv?).and_return(false)
          allow_any_instance_of(ReturnInvoice).to receive(:promise_offer).and_return(promise_offer)
          post :create, id: :finalize_return, return_invoice: { some: :param }, buyer_and_agreement_information_form: { some: :param }
          expect(flash[:alert]).to be_present
        end
      end

      context 'when the user is authorized to return' do
        before do
          expect_any_instance_of(ReturnInvoicePolicy).to receive(:create?) { true }
        end

        # it 'completes the return', vcr: {cassette_name: 'easypost/successful_shipment'} do
        #   expect_any_instance_of(ReturnInvoice).to receive(:save){ true }
        #   expect_any_instance_of(ReturnInvoice).to receive(:country).at_least(:once){ "United States of America" }
        #   post :create, id: :finalize_return, return_invoice: {some: :param}, buyer_and_agreement_information_form: {some: :param}
        #   expect(flash.now[:success]).to be_present
        # end
      end
    end
  end
end
