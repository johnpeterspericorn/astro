# encoding: utf-8
require 'requests_helper'

describe ReturnFunnel::PaymentNotificationsController do
  let(:super_user) { FactoryGirl.create(:super_user) }
  let(:purchase) { FactoryGirl.create(:promise_purchase) }
  let(:request_params) { { promise_purchase_id: purchase.id } }
  subject { get :new, promise_purchase_id: purchase.id }

  before do
    sign_in super_user
  end

  describe '#new' do
    before do
      allow_any_instance_of(PromisePurchase).to receive(:paid_by_buyer?).and_return(false)
      allow_any_instance_of(PromiseOffer).to receive(:adjustments_paid_by_floor_plan?).and_return(false)
    end

    describe 'pplv' do
      before do
        allow_any_instance_of(PromisePurchase).to receive(:pplv?).and_return(true)
      end

      it 'redirects to return fee notification if paid' do
        allow_any_instance_of(PromisePurchase).to receive(:paid?).and_return(true)

        return_fee_url = new_return_funnel_return_fee_notification_path(request_params)
        expect(subject).to redirect_to(return_fee_url)
      end

      it 'shows payment notification screen if not paid' do
        allow_any_instance_of(PromisePurchase).to receive(:paid?).and_return(false)
        expect(subject).to render_template(:new)
      end
    end

    describe 'non pplv' do
      before do
        allow_any_instance_of(PromisePurchase).to receive(:pplv?).and_return(false)
        allow_any_instance_of(PromisePurchase).to receive(:paid?).and_return(true)
      end

      it 'proceeds with the return if promise is paid' do
        returns_url = new_return_funnel_verification_path(request_params)
        expect(subject).to redirect_to(returns_url)
      end
    end
  end
end
