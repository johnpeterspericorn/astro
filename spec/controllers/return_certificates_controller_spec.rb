# encoding: utf-8
require 'requests_helper'
require_relative '../requests/steps/return_invoice_steps'

describe ReturnCertificatesController do
  include_context 'return invoice steps'
  before do
    allow_any_instance_of(PromisePurchase).to receive(:paid?).and_return(true)
    stub_buyer_authorization
  end

  describe 'GET show' do
    describe 'JSON' do
      let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000) }
      let(:agreement_number) { acura.agreement_no }
      let(:return_invoice) { ReturnInvoice.first }

      context 'logged in as buyer user' do
        subject (:json) { JSON.parse(response.body) }
        let(:return_certificate) { json['return_certificate'] }
        let(:buyer_user) { User.create!(buyer_user: true, username: 'Testy') }

        before do
          allow_any_instance_of(ReturnInvoicePolicy).to receive(:show?) { true }
          promise_and_return_vehicle(acura, buyer_user)
          sign_in buyer_user
          get :show, return_invoice_id: return_invoice.id, format: :json
        end

        it 'should be successful' do
          expect(response).to be_success
        end

        it 'should include a return invoice' do
          expect(return_certificate['return_invoice']).to be_present
        end

        it 'should include a promise purchase' do
          expect(return_certificate['promise_purchase']).to be_present
        end

        it 'should include a promise offer' do
          expect(return_certificate['promise_offer']).to be_present
        end
      end

      context 'logged in as super user' do
        let(:super_user) { User.create!(super_user: true, username: 'Testy', email: 'testy@astro.local') }

        before do
          promise_and_return_vehicle(acura, super_user)
          allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([])
          sign_in super_user
          get :show, return_invoice_id: return_invoice.id, format: :json
        end

        it 'should be authorized' do
          expect(response).to be_success
        end
      end
    end
  end
end
