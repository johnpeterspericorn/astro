# encoding: utf-8
require 'spec_helper'

describe Api::PawnLocationsController do
  let(:user) { FactoryGirl.create(:buyer_user) }
  let(:token) { user.authentication_token }
  let(:json) { JSON.parse(response.body) }

  describe 'GET #index' do
    it 'responds with purchase enabled pawn locations' do
      location = FactoryGirl.create(:pawn_location)
      FactoryGirl.create(:presale_vehicle, location_initials: location.initials)
      FactoryGirl.create(:pawn_location, purchases_enabled: false)

      get :index, auth_token: token, format: :json

      expect(json.length).to eq(1)
      expect(json.first.fetch('initials')).to eq(location.initials)
    end

    it 'orders lanes as numbers rather than strings' do
      lane_1 = '2'
      lane_2 = '10'
      location = FactoryGirl.create(:pawn_location)
      FactoryGirl.create(:presale_vehicle, location_initials: location.initials, lane_no: lane_1)
      FactoryGirl.create(:presale_vehicle, location_initials: location.initials, lane_no: lane_2)

      get :index, auth_token: token, format: :json

      expect(json.first.fetch('presale_lane_numbers')).to eq([lane_1, lane_2])
    end
  end
end
