# encoding: utf-8
require 'spec_helper'

describe Api::PromisePurchaseRefreshesController do
  before do
    allow(subject).to receive(:authenticate_api_user!)
  end

  let(:universal_no) { 'GCAA2018080301862585156' }
  let(:offer) { double(PromiseOffer, promise_purchase: purchase) }
  let(:purchase) { double(PromisePurchase, touch: nil) }

  let(:valid_params) do
    {
      auth_token: FactoryGirl.create(:api_user).authentication_token,
      universal_no: universal_no,
      format: :json
    }
  end

  describe '#update' do
    before do
      allow(PromisePurchaseRefreshMailer).to receive(:refresh_confirmation) { double.as_null_object }
    end

    context 'when the given universal number exists' do
      before do
        allow(PromiseOffer).to receive(:find_by_universal_no).with(universal_no) { offer }
      end

      it 'refreshes the timestamp' do
        expect(purchase).to receive(:touch).with(:promised_at)
        put :update, valid_params
      end

      it 'sends a confirmation to the user' do
        expect(PromisePurchaseRefreshMailer).to receive(:refresh_confirmation)
        put :update, valid_params
      end

      context 'when the `silent` parameter is true' do
        it 'does not send an email' do
          expect(PromisePurchaseRefreshMailer).not_to receive(:refresh_confirmation)
          put :update, valid_params.merge(silent: true)
        end
      end
    end

    context "when a purchase with the given universal number doesn't exist" do
      it 'returns Not Found' do
        put :update, valid_params.merge(universal_no: 'foobar')
        expect(response).to be_not_found
      end
    end
  end
end
