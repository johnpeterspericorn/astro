# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe Api::BuyerPromisePurchasesController do
  let!(:user) { FactoryGirl.create(:buyer_user) }

  let(:token) { user.authentication_token }
  let(:json) { JSON.parse(response.body) }

  describe '#create' do
    let(:days) { 7 }
    let(:miles) { 500 }
    let(:pawn_access_no) { '10000000' }
    let(:email) { 'tweetie@birds.nest' }

    let(:base_params) do
      { auth_token: token, format: :json }
    end

    let!(:offer) do
      FactoryGirl.create(
        :promise_offer,
        pawn_access_no: pawn_access_no
      )
    end

    let(:offer_params) do
      {
        additional_days: days,
        miles: miles,
        id: offer.id
      }
    end

    before do
      Settings.disable_purchase_api_price_match = true
      user.reset_authentication_token!
      allow_any_instance_of(User).to receive(:pawn_access_number) { pawn_access_no }
    end

    context 'when the vehicle exists and belongs to the user' do
      it 'purchases a promise for the given user' do
        expect do
          post :create, base_params.merge(offers: [offer_params])
        end.to change { PromisePurchase.count }.by(1)
      end

      it 'sets the offer email' do
        post :create, base_params.merge(email_address: email, offers: [offer_params])

        expect(offer.reload.emails).to eq(email)
      end

      it 'responds with a true success field for successful purchases' do
        post :create, base_params.merge(offers: [offer_params])
        json['purchases'].each { |purchase_response| expect(purchase_response['success']).to be_truthy }
      end

      it 'responds with a false success field for expired offers' do
        offer.update_attributes!(must_be_promised_at: 1.day.ago)
        post :create, base_params.merge(offers: [offer_params])

        expiration_response = json['purchases'].find { |offer_response| offer_response['id'] == offer.id }
        expect(expiration_response['success']).to be_falsey
      end

      it 'responds with the ineligibility condition description when the offer has an ineligibility condition' do
        ineligibility_condition = FactoryGirl.create(:ineligibility_condition, condition_id: 1, notice: 'ineligible as promise cutoff date expired')
        offer.update_attributes(ineligibility_condition: ineligibility_condition.id)

        post :create, base_params.merge(offers: [offer_params])

        expiration_response = json['purchases'].first
        expect(expiration_response['ineligibility_notice']).to eq('ineligible as promise cutoff date expired')
      end
    end

    describe 'Error handling' do
      context 'when the provided VNUM is not present in the system' do
        let(:bad_offer_params) do
          offer_params.merge(additional_days: days, miles: miles, id: -1)
        end

        it 'yields Not Found' do
          post :create, base_params.merge(offers: [bad_offer_params])

          expect(response).to be_not_found
        end
      end

      context 'when days or miles are not provided' do
        it 'yields Bad Request' do
          offer_params.delete(:miles)
          post :create, base_params.merge(offers: [offer_params])
          expect(response.status).to eql 400
        end
      end

      context 'when days are invalid' do
        before do
          offer_params[:additional_days] = -100
        end

        it 'responds with unprocessible entity' do
          params = base_params.merge(offers: [offer_params])

          post :create, params

          expect(response).to be_unprocessable
        end
      end

      context 'when miles are invalid' do
        before do
          offer_params[:miles] = -1000
        end

        it 'responds with unprocessible entity' do
          params = base_params.merge(offers: [offer_params])

          post :create, params

          expect(response).to be_unprocessable
        end
      end

      context 'when email format is invalid' do
        it 'responds with unprocessible entity' do
          params = base_params.merge(email_address: 'not-an-email')

          post :create, params

          expect(response).to be_unprocessable
        end
      end
    end
  end
end
