# encoding: utf-8
require 'spec_helper'

describe Api::PurchasablePromiseOffersController do
  let(:buyer_user) { FactoryGirl.create(:buyer_user) }
  let(:token) { buyer_user.authentication_token }
  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }

  before do
    allow_any_instance_of(User).to receive(:pawn_access_number) { pawn_access_number }
  end

  describe 'GET #index' do
    let(:json) { JSON.parse(response.body) }

    let!(:offer) { FactoryGirl.create(:day_old_promise_offer, pawn_access_no: pawn_access_number) }

    it 'responds with all offers for the user' do
      FactoryGirl.create(:promise_offer, pawn_information: FactoryGirl.create(:pawn_information, pawn_access_no: '000'))

      get :index, auth_token: token, format: :json

      expect(json.length).to eq(1)
    end

    it 'filters results by vnum' do
      new_offer = FactoryGirl.create(:promise_offer, pawn_access_no: pawn_access_number)

      get :index, auth_token: token, format: :json, filter: { vnum: new_offer.vnum }

      expect(json.length).to eq(1)
      expect(json.first.fetch('vnum')).to eq(new_offer.vnum)
    end

    it 'filters results by 100m' do
      FactoryGirl.create(:promise_offer, pawn_access_no: '123')

      get :index, auth_token: token, format: :json

      expect(json.length).to eq(1)
    end
  end
end
