# encoding: utf-8
require 'spec_helper'

describe Api::PendingOffersController do
  let!(:api_user) { FactoryGirl.create(:api_user) }
  let!(:promise_offer) { FactoryGirl.create(:promise_offer) }

  it 'finalizes the offer' do
    expect_any_instance_of(PromiseOffer).to receive(:finalize_pending)

    put :update, format: :json, promise_offer_id: promise_offer.id, auth_token: api_user.authentication_token

    expect(response).to be_success
  end
end
