# encoding: utf-8
require 'spec_helper'
require_relative '../../requests_support/country_helper.rb'

describe Api::PromisePurchasesController do
  let(:agreement_number) { '5000000' }
  let(:vnum) { SecureRandom.hex }
  let(:days) { 7 }
  let(:miles) { 250 }
  let(:api_purchaser) { double('ApiPurchaser') }
  let!(:api_user) { FactoryGirl.create(:api_user) }
  let(:token) { api_user.authentication_token }
  let(:email) { 'adama@caprica.gov' }
  let(:json) { JSON.parse(response.body) }
  let(:price) { 100 }

  let(:valid_params) do
    {
      auth_token: token,
      email_address: email,
      buyer_agreement_number: agreement_number,
      vnum: vnum,
      days: days,
      miles: miles,
      agreementshield_quoted_price: price.to_s,
      format: :json
    }
  end

  before do
    Settings.disable_purchase_api_price_match = true
    Delayed::Worker.delay_jobs = true
    allow(subject).to receive(:authenticate_api_user!)
    allow_any_instance_of(SoldVehicle).to receive(:available_for_purchase?) { true }
    allow_any_instance_of(Promise).to receive(:price) { price }
    allow_any_instance_of(PromiseOffer).to receive(:country) { america }
  end

  after do
    Delayed::Worker.delay_jobs = false
  end

  it 'returns service unavailable when the feature flag for the API is disabled' do
    Settings.purchase_api_enabled = false
    post :create, valid_params
    expect(response.status).to eq(503)
  end

  context 'when the feature flag is enabled' do
    before do
      Settings.purchase_api_enabled = true
    end

    describe 'happy path full-stack test' do
      before do
        FactoryGirl.create(:sold_vehicle, agreement_no: agreement_number, vnum: vnum)
      end

      it 'succeeds' do
        post :create, valid_params
        expect(response.status).to eq(200)
      end

      it 'provides a return_code of 0' do
        post :create, valid_params
        expect(json['return_code']).to eql 0
      end

      it 'creates a promise purchase' do
        expect do
          post :create, valid_params
        end.to change(PromisePurchase, :count).by(1)
      end

      it 'gives a transaction ID' do
        post :create, valid_params
        expect(json['transaction_id']).to eq(PromisePurchase.last.transaction_id)
      end

      it 'responds with the offer id' do
        post :create, valid_params
        expect(json['promise_offer_id']).to eq(PromisePurchase.last.promise_offer_id)
      end

      it 'responds with the promise purchase id' do
        post :create, valid_params
        expect(json['promise_purchase_id']).to eq(PromisePurchase.last.id)
      end
    end

    context 'when the promise options provided are not as expected' do
      let(:bad_params) do
        FactoryGirl.create(:sold_vehicle, agreement_no: agreement_number, vnum: vnum)
        valid_params.merge(days: 365)
      end

      it 'gives a response code of 4' do
        post :create, bad_params
        expect(json['return_code']).to eql 4
      end

      it 'returns a Bad Request response' do
        post :create, bad_params
        expect(response).to be_bad_request
      end
    end

    context 'when the purchaser raises an ArgumentError' do
      before do
        allow(ApiPurchaser).to receive(:new) { api_purchaser }
        allow(api_purchaser).to receive(:valid?) { true }
        allow(api_purchaser).to receive(:purchase_vehicle) { raise ArgumentError }
      end

      it 'returns as a bad request' do
        post :create, valid_params
        expect(response).to be_bad_request
        expect(json['return_code']).to eql 3
      end
    end

    context 'when the purchaser shows as invalid' do
      before do
        allow(api_purchaser).to receive(:valid?) { false }
      end

      it 'returns as bad request' do
        post :create, valid_params
        expect(response).to be_bad_request
      end

      it 'does not attempt a purchase' do
        expect(api_purchaser).not_to receive(:purchase_vehicle)
        post :create, valid_params
      end
    end

    context 'the purchase is pending' do
      before do
        FactoryGirl.create(:sold_vehicle, agreement_no: agreement_number, vnum: vnum)
        VinBlacklist.create!(vnum: vnum)
      end

      it 'responds with success and return code 1' do
        post :create, valid_params

        expect(response).to be_success
        expect(json['return_code']).to eq(1)
      end

      it 'responds to subsequent request with 400 and return code 11' do
        post :create, valid_params
        post :create, valid_params

        expect(response).to be_bad_request
        expect(json['return_code']).to eq(11)
      end
    end

    context 'price match is disabled' do
      let(:requested_price) { 101.0 }

      describe 'when requested price does not match offer price' do
        let(:params) do
          valid_params.merge(agreementshield_quoted_price: requested_price)
        end

        before do
          FactoryGirl.create(:sold_vehicle, agreement_no: agreement_number, vnum: vnum)
        end

        it 'is successful' do
          post :create, params
          expect(response.status).to eq(200)
        end

        it 'creates a promise purchase with agreementshield_quoted_price' do
          post :create, params
          expect(PromisePurchase.last.promise_price).to eql(requested_price)
        end
      end
    end
  end
end
