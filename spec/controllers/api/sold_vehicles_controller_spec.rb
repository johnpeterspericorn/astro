# encoding: utf-8
require 'spec_helper'

describe Api::SoldVehiclesController do
  let(:api_user) { FactoryGirl.create(:api_user) }
  let(:auth_token) { api_user.authentication_token }

  describe 'POST #create' do
    describe 'success' do
      let!(:pawn_location) { FactoryGirl.create(:svaa_location) }
      let(:sold_vehicle_attributes) { FactoryGirl.attributes_for(:sold_vehicle) }

      it 'responds with success' do
        post :create, format: :json, sold_vehicle: sold_vehicle_attributes, auth_token: auth_token

        expect(response).to be_success
      end

      it 'creates a SoldVehicle' do
        expect do
          post :create, format: :json, sold_vehicle: sold_vehicle_attributes, auth_token: auth_token
        end.to change { SoldVehicle.count }.by(1)
      end
    end

    describe 'failure' do
      it 'responds as unprocessible' do
        post :create, format: :json, auth_token: auth_token, sold_vehicle: { some: :param }

        expect(response).to be_unprocessable
      end
    end
  end
end
