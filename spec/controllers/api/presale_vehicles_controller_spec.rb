# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe Api::PresaleVehiclesController do
  let(:user) { FactoryGirl.create(:buyer_user) }
  let(:token) { user.authentication_token }

  describe '#index' do
    let(:json) { JSON.parse(response.body) }

    let(:base_params) do
      { auth_token: token, format: :json }
    end

    context 'no filters given' do
      it 'responds with bad request' do
        get :index, base_params

        expect(response).to be_bad_request
      end
    end

    context 'blank filters given' do
      it 'responds with bad request' do
        get :index, base_params.merge(filter: {})

        expect(response).to be_bad_request
      end
    end

    context 'location search' do
      let(:location) { 'ABC' }
      let(:lane) { '123' }
      let!(:location_vehicle) { FactoryGirl.create(:presale_vehicle, location_initials: location, lane_no: lane, ineligibility_condition: 2) }
      let!(:non_location_vehicle) { FactoryGirl.create(:presale_vehicle, location_initials: 'WRONG') }
      let!(:non_lane_vehicle) { FactoryGirl.create(:presale_vehicle, location_initials: location_vehicle.location_initials, lane_no: 999) }

      it 'allows a client to search by pawn location and lane #' do
        get :index, base_params.merge(filter: {
                                        location_initials: location,
                                        lane_no: lane
                                      })

        expect(response).to be_success

        expect(json['vehicles'].length).to eq(1)
        expect(json['vehicles'].first['vnum']).to eq(location_vehicle.vnum)
      end

      it 'displays the mobile ineligibility notice' do
        get :index, base_params.merge(filter: {
                                        location_initials: location,
                                        lane_no: lane
                                      })

        expect(response).to be_success
        expect(json['vehicles'].first['ineligibility_notice']).to eq(I18n.t('ineligibility_conditions.mobile')[2])
      end
    end

    context 'OVE/make/model search' do
      let!(:vehicle) { FactoryGirl.create(:presale_vehicle, ove: true) }
      let!(:wrong_vehicle) { FactoryGirl.create(:presale_vehicle, ove: true, make: 'Wrongmake') }

      it 'allows a client to search by make/model/OVE status' do
        get :index, base_params.merge(filter: {
                                        ove: true,
                                        make: vehicle.make,
                                        model: vehicle.model
                                      })

        expect(response).to be_success

        expect(json['vehicles'].length).to eq(1)
        expect(json['vehicles'].first['vnum']).to eq(vehicle.vnum)
      end
    end

    context 'vnum suffix search' do
      let!(:vehicle) { FactoryGirl.create(:presale_vehicle) }

      before do
        # create another vehicle so count is meaningful
        FactoryGirl.create(:presale_vehicle)
      end

      it 'allows a client to search by a vnum suffix' do
        get :index, base_params.merge(filter: { vnum_suffix: vehicle.vnum[-6..-1] })

        expect(json['vehicles'].length).to eq(1)
        expect(json['vehicles'].first['vnum']).to eq(vehicle.vnum)
      end
    end
  end
end
