# encoding: utf-8
require 'spec_helper'

describe Api::QuotesController do
  let(:buyer_user) { FactoryGirl.create(:buyer_user) }
  let(:base_params) { { auth_token: buyer_user.authentication_token, format: :json } }
  let(:json) { JSON.parse(response.body) }
  let(:quotes_json) { json.fetch('quotes') }
  let!(:pawn_location) { FactoryGirl.create(:svaa_location) }
  let(:quote_request) { double Sas::QuoteRequest }
  let(:quotes) { double(:quotes, miles: [250, 500], days: [7, 14, 21]) }

  describe '"swipe" access' do
    let!(:vehicle) { FactoryGirl.create(:presale_vehicle, vnum: vnum, average_valuation: price, seller_no: seller, location_initials: pawn_location.initials) }
    let(:vnum) { '19UUA66206A072671' }
    let(:seller) { '4919311' }
    let(:buyer) { '5318201' }
    let(:price) { 10_900 }
    let(:price_250_by_7) { 100 }
    let(:price_500_by_7) { 175 }

    it 'responds with success and rates', vcr: { cassette_name: 'quote_api/successful_request' } do
      pawn_access_number = '1234'
      auth = double(:buyer_authorization)
      auth.stub(get_agreement_numbers: [buyer])
      allow(Astro::BUYER_AUTHORIZATION_CLASS)
        .to receive(:new)
        .with(pawn_access_number: pawn_access_number)
        .and_return(auth)

      request.env['rack.session'][:pawn_access_number] = pawn_access_number
      get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

      expect(response).to be_success
      expect(quotes_json.fetch('250').fetch('7')).to eq(price_250_by_7)
      expect(quotes_json.fetch('500').fetch('7')).to eq(price_500_by_7)
    end
  end

  describe 'GET #show' do
    let!(:vehicle) { FactoryGirl.create(:presale_vehicle, vnum: vnum, average_valuation: price, seller_no: seller, location_initials: pawn_location.initials, ineligibility_condition: 2) }

    before do
      allow(quote_request).to receive(:load) { quote_request }

      allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([buyer])
      expect(Sas::QuoteRequest).to receive(:new).with(
        hash_including(vnum: vnum,
                       seller_5m: seller,
                       buyer_5m: buyer.to_i,
                       sale_price: price,
                       location_initials: pawn_location.initials)
      ).and_return(quote_request)

      allow(quote_request).to receive(:quotes).and_return(quotes)
    end

    describe 'html response' do
      render_views

      let(:vnum) { '19UUA66206A072671' }
      let(:seller) { '4919311' }
      let(:buyer) { '5318201' }
      let(:price) { 10_900 }

      before do
        allow(quotes).to receive(:price)
      end

      it 'responds with success' do
        params = {
          auth_token: buyer_user.authentication_token,
          presale_vehicle_id: vehicle.id,
          buyer_number: buyer
        }

        get :show, params

        expect(response).to be_success
      end
    end

    context 'as an API user' do
      let(:api_user) { FactoryGirl.create(:api_user) }
      let(:base_params) { { auth_token: api_user.authentication_token, format: :json } }

      let(:vnum) { '19UUA66206A072671' }
      let(:seller) { '4919311' }
      let(:buyer) { '5318201' }
      let(:price) { 10_900 }
      let(:price_250_by_7) { 240 }
      let(:price_500_by_7) { 375 }
      let(:quotes) { double(:quotes, miles: [250, 500], days: [7]) }

      before do
        allow(quotes).to receive(:price).with(250, 7) { price_250_by_7 }
        allow(quotes).to receive(:price).with(500, 7) { price_500_by_7 }
        allow(quotes).to receive(:flat_rate).and_return(false)
      end

      it 'responds with rates' do
        get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

        expect(response).to be_success
        expect(quotes_json.fetch('250').fetch('7')).to eq(price_250_by_7)
        expect(quotes_json.fetch('500').fetch('7')).to eq(price_500_by_7)
      end

      it 'responds with mobile ineligibility_condition' do
        get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

        expect(response).to be_success
        expect(json.fetch('ineligibility_notice')).to eq(I18n.t('ineligibility_conditions.mobile')[2])
      end
    end

    context 'with quoted rate' do
      # The values here correspond to the Astroheim quoted rate
      let(:vnum)   { '19UUA66206A072671' }
      let(:seller) { '4919311' }
      let(:buyer) { '5318201' }
      let(:price) { 10_900 }
      let(:price_250_by_7) { 240 }
      let(:price_500_by_7) { 375 }
      let(:quotes) { double(:quotes, miles: [250, 500], days: [7]) }

      before do
        allow(quotes).to receive(:price).with(250, 7) { price_250_by_7 }
        allow(quotes).to receive(:price).with(500, 7) { price_500_by_7 }
        allow(quotes).to receive(:flat_rate).and_return(false)
      end

      it 'responds with rates' do
        get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

        expect(response).to be_success
        expect(quotes_json.fetch('250').fetch('7')).to eq(price_250_by_7)
        expect(quotes_json.fetch('500').fetch('7')).to eq(price_500_by_7)
      end
    end

    context 'with flat rate' do
      # The values here correspond to the Astroheim flat rate
      let(:vnum)   { '19UUA66295A070254' }
      let(:seller) { '5010977' }
      let(:buyer) { '5006132' }
      let(:price) { 10_500 }
      let(:miles) { 250 }
      let(:days)  { 21 }
      let(:quote_price) { 150 }

      before do
        allow(quotes).to receive(:price) { quote_price }
        allow(quotes).to receive(:flat_rate).and_return(true)
      end

      it 'responds with rate and boolean' do
        get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

        quotes_json = json.fetch('quotes')

        expect(response).to be_success
        expect(json.fetch('flat_rate')).to be_truthy
        expect(quotes_json.fetch(miles.to_s).fetch(days.to_s)).to eq(quote_price)
      end
    end

    context 'with no result' do
      # The values here correspond to no result from api
      let(:vnum)   { '19UUA56673A038252' }
      let(:seller) { '5269621' }
      let(:buyer) { '5006132' }
      let(:price) { 1500 }
      let(:quotes) { Naught.build.new }

      it 'responds with empty quotes' do
        get :show, base_params.merge(presale_vehicle_id: vehicle.id, buyer_number: buyer, price: price)

        expect(response).to be_success
        expect(json.fetch('quotes')).to be_empty
      end
    end
  end

  describe 'GET #show' do
    context 'request for missing vnum' do
      it 'responds not found' do
        get :show, base_params.merge(vnum: 'abc123')

        expect(response).to be_not_found
      end
    end
  end
end
