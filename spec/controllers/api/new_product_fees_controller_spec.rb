# encoding: utf-8
require 'spec_helper'

describe Api::NewProductFeesController do
  let(:api_user) { FactoryGirl.create(:api_user) }
  let(:auth_token) { api_user.authentication_token }
  let(:json) { JSON.parse(response.body) }

  describe 'GET #index' do
    it 'list all new product fees' do
      new_product_fee = FactoryGirl.create(:new_product_fee_365_1)
      get :index, auth_token: auth_token, format: :json
      expect(json.length).to eq(1)
      expect(json['new_product_fees'].first['timeframe']).to eq(new_product_fee.timeframe)
    end
  end

  describe 'GET #show' do
    it 'list details of a new product fee' do
      new_product_fee = FactoryGirl.create(:new_product_fee_365_1)
      get :show, id: new_product_fee.id, auth_token: auth_token, format: :json

      expect(json.length).to eq(1)
      expect(json['new_product_fee']['timeframe']).to eq(new_product_fee.timeframe)
    end
  end

  describe 'POST #create' do
    describe 'success' do
      let(:new_product_fee_attributes) { FactoryGirl.attributes_for(:new_product_fee_365_1) }

      it 'responds with success' do
        post :create, format: :json, new_product_fee: new_product_fee_attributes, auth_token: auth_token

        expect(response).to be_success
      end

      it 'creates a New Product Fee' do
        expect do
          post :create, format: :json, new_product_fee: new_product_fee_attributes, auth_token: auth_token
        end.to change { NewProductFee.count }.by(1)
      end
    end

    describe 'failure' do
      it 'responds as bad_request' do
        post :create, format: :json, auth_token: auth_token

        expect(response.status).to eq 400
      end
    end
  end

  describe 'PUT #update' do
    let(:new_product_fee) { FactoryGirl.create(:new_product_fee_365_1) }

    describe 'success' do
      it 'responds with success' do
        put :update, format: :json, id: new_product_fee.id, auth_token: auth_token, new_product_fee: FactoryGirl.attributes_for(:new_product_fee, fee: 215)
        expect(response).to be_success
      end

      it 'update New Product Fee' do
        put :update, format: :json, id: new_product_fee.id, auth_token: auth_token, new_product_fee: FactoryGirl.attributes_for(:new_product_fee, fee: 215)
        new_product_fee.reload
        expect(new_product_fee.fee).to eq(215)
      end
    end

    describe 'failure' do
      it 'responds as bad_request' do
        put :update, format: :json, auth_token: auth_token, id: new_product_fee.id

        expect(response.status).to eq 400
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:new_product_fee) { FactoryGirl.create(:new_product_fee_365_1) }

    it 'delete a New Product Fee' do
      expect do
        delete :destroy, format: :json, id: new_product_fee.id, auth_token: auth_token
      end.to change { NewProductFee.count }.by(-1)
    end
  end
end
