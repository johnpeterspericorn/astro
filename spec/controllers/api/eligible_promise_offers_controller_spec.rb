# encoding: utf-8
require 'spec_helper'

describe Api::EligiblePromiseOffersController do
  let(:buyer_user) { FactoryGirl.create(:buyer_user) }
  let(:base_params) { { format: :json, auth_token: buyer_user.authentication_token } }
  let(:json) { JSON.parse(response.body) }
  let(:agreement_no) { '500000' }
  let(:pawn_access_no) { '100000000' }

  describe 'GET #index' do
    include ActionView::Helpers::NumberHelper
    let(:model) { 'Focus' }
    let(:vehicle_information)  { FactoryGirl.create(:vehicle_information, model: model) }
    let!(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: agreement_no, pawn_access_no: pawn_access_no) }
    let!(:eligible_offer)      { FactoryGirl.create(:promise_offer, pawn_information: pawn_information, vehicle_information: vehicle_information, pawn_access_no: pawn_access_no) }

    before do
      allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_pawn_access_number).and_return(pawn_access_no)
      allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([agreement_no])
    end

    it 'responds with offers that are eligible for purchase' do
      get :index, base_params

      expect(json.first.fetch('id')).to eq(eligible_offer.id)
    end

    it 'responds with offers that are not purchased' do
      eligible_offer.update_attributes!(purchased_at: nil)
      get :index, base_params

      expect(json.first.fetch('id')).to eq(eligible_offer.id)
    end

    it 'includes the ineligibility reason with offers in response' do
      eligible_offer.update_attributes!(ineligibility_condition: -1)

      get :index, base_params
      expect(json.first.fetch('mobile_ineligibility_notice')).to eq(I18n.t('ineligibility_conditions.mobile')[eligible_offer.ineligibility_condition])
    end

    it 'provides backwards compatibility by aliasing mobile ineligibility notice' do
      notice = I18n.t('ineligibility_conditions.mobile')[-1]

      get :index, base_params
      expect(json.first.fetch('ineligibility_notice')).to eq(notice)
    end

    it 'sends the default message if the ineligibility_condition has none' do
      eligible_offer.update_attributes!(ineligibility_condition: 35)

      get :index, base_params
      expect(json.first.fetch('mobile_ineligibility_notice')).to eq(I18n.t('ineligibility_conditions.mobile')[-1])
    end

    it 'does not send a message if the ineligibility_condition is nil' do
      eligible_offer.update_attributes!(ineligibility_condition: nil)

      get :index, base_params
      expect(json.first.fetch('mobile_ineligibility_notice')).to_not eq(I18n.t('ineligibility_conditions.mobile')[-1])
    end

    it 'does not respond with offers that are past cutoff' do
      eligible_offer.update_attributes!(must_be_promised_at: 1.day.ago)

      get :index, base_params

      expect(json.length).to eq(0)
    end

    it 'does not respond with offers that are owned by other users' do
      other_pawn_information = FactoryGirl.create(:pawn_information, agreement_no: agreement_no, pawn_access_no: '2000000')
      FactoryGirl.create(:promise_offer, pawn_information: other_pawn_information)

      get :index, base_params

      expect(json.length).to eql 1
    end

    it 'records offers for sold vehicles' do
      vehicle = FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no)

      get :index, base_params

      expect(json.map { |row| row['vnum'] }).to include(vehicle.vnum)
    end

    # Regression - bugs like these are caused due to overloading of the `model`
    # method.
    it 'uses the `model` column for the `model ` key in the response' do
      get :index, base_params

      expect(json[0]['model']).to eql model
    end

    it 'uses an integer value for vehicle_purchase_price' do
      get :index, base_params

      expect(json[0]['vehicle_purchase_price']).to eql number_to_currency(eligible_offer.vehicle_purchase_price, format: '%u %n', precision: 0, separator: '.', delimiter: ',')
    end
  end
end
