# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe Api::PromiseEligibilitiesController do
  let(:token) { user.authentication_token }
  let(:auth) { double(:astroheim_authorization) }
  let!(:user) { FactoryGirl.create(:api_user) }

  let(:json) { JSON.parse(response.body) }

  let(:params) do
    {
      auth_token: token,
      format: :json
    }
  end

  let(:notice) { 'foobarbaz' }
  let(:condition) { 3 }

  let!(:vehicles) do
    [
      FactoryGirl.create(:presale_vehicle, vnum: '1D4HD38K86F167472'),
      FactoryGirl.create(:presale_vehicle, vnum: '2FAFP74W4WX191614')
    ]
  end

  let(:vehicle_params) do
    vehicles.map { |vehicle| { vnum: vehicle.vnum, seller_no: vehicle.seller_no } }
  end

  let(:seller_no) { '5151515' }

  describe '#show' do
    context 'happy path' do
      before do
        IneligibilityCondition.create!(condition_id: condition, notice: notice)
        vehicle = vehicles.first
        vehicle.ineligibility_condition = condition
        vehicle.save!

        get :show, params.merge(vehicles: vehicle_params)
        expect(response).to be_success
      end

      it 'responds with a field indicating the input VNUM' do
        vnums = json.map { |j| j['vnum'] }
        expect(vnums).to match_array(vehicles.map(&:vnum))
      end

      it 'responds with a field reflecting promise eligibility' do
        expect(json.last['promise_eligible']).to be_falsey
      end

      it 'responds with an ineligibility message field' do
        expect(json.last['ineligibility_reason']).to eq(notice)
      end
    end

    context 'when a VNUM and seller # provided do not line up' do
      let(:vehicle) { FactoryGirl.create(:presale_vehicle) }
      let(:mismatched_vehicle_params) do
        [{ vnum: vehicle.vnum, seller_no: 'WRONG' }]
      end

      let(:notice) { "Foobar'd" }
      let!(:mismatch_condition) do
        IneligibilityCondition.create!(
          condition_id: VehicleEligibility::MISMATCHED_SELLER_NUMBER_INELIGIBILITY_CONDITION,
          notice: notice
        )
      end

      it 'returns a custom ineligibility condition' do
        get :show, params.merge(vehicles: mismatched_vehicle_params)

        expect(json.first['ineligibility_reason']).to eq(notice)
      end
    end

    context 'when a required param is missing' do
      it 'responds with "bad request"' do
        get :show, params.merge(seller_no: seller_no)
        expect(response).to be_bad_request
      end
    end

    context 'when none of the VNUMs corresond to a vehicle in the DB' do
      let(:nonexistent_vehicle_params) { [{ vnum: 'FOO', seller_no: '5000000' }] }

      it 'responds with "not found"' do
        get :show, params.merge(vehicles: nonexistent_vehicle_params)
        expect(response).to be_not_found
      end
    end
  end

  describe '#create' do
    describe 'success' do
      it 'responds with an ineligibility flag' do
        post :create, params.merge(vehicles: vehicle_params)
        expect(json.first['promise_eligible']).to be_truthy
      end
    end
  end
end
