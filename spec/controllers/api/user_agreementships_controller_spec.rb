# encoding: utf-8
require 'spec_helper'
require 'requests_support/login_helper'

describe Api::UserAgreementshipsController do
  describe '#index' do
    let(:token) { user.authentication_token }
    let(:auth) { double(:astroheim_authorization) }
    let(:json) { JSON.parse(response.body) }
    let!(:user) { FactoryGirl.create(:buyer_user) }

    let(:agreement_no1) { '500000' }
    let(:agreement_name1) { 'Agreement 1' }

    let(:agreement_no2) { '500001' }
    let(:agreement_name2) { 'Agreement 2' }
    let(:numbers_to_names) do
      {
        agreement_no1 => agreement_name1,
        agreement_no2 => agreement_name2
      }
    end

    before do
      allow(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:new).with(username: user.username) { auth }
      allow(auth).to receive(:get_agreement_numbers_to_names).and_return(numbers_to_names)
    end

    it 'returns a list of associated agreementships' do
      get :index, auth_token: token, format: :json
      expect(json['agreementships']).to eql [
        { 'agreementNumber' => agreement_no1, 'agreementName' => agreement_name1 },
        { 'agreementNumber' => agreement_no2, 'agreementName' => agreement_name2 }
      ]
    end
  end
end
