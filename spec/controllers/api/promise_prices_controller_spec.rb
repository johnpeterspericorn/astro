# encoding: utf-8
require 'spec_helper'

describe Api::PromisePricesController do
  let!(:api_user) { FactoryGirl.create(:api_user) }

  let(:pricing_stub) { Hash.new }

  let(:offer) { FactoryGirl.build(:promise_offer) }

  before do
    allow(PromiseOffer).to receive(:find_by_vnum) { offer }
    Settings.pricing_api_enabled = true
    allow_any_instance_of(PriceFetcher).to receive(:pricing_information) { pricing_stub }
  end

  it 'succeeds with auth by API token' do
    get :show, format: :json, auth_token: api_user.authentication_token, vnum: offer.vnum, pawn_access_number: offer.pawn_access_no
    expect(response).to be_success
  end
end
