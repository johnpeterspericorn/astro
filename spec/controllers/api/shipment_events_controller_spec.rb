# encoding: utf-8
require 'spec_helper'

describe Api::ShipmentEventsController do
  let(:api_user)   { FactoryGirl.create(:api_user) }
  let(:auth_token) { api_user.authentication_token }
  let(:auth_params) { { auth_token: auth_token } }

  describe 'POST #create' do
    context 'tracker.updated event' do
      let(:easypost_shipment_id) { 'shp_ABC123' }
      let!(:shipment) { FactoryGirl.create(:shipment, easypost_shipment_id: easypost_shipment_id) }

      let(:event_status) { 'delivered' }
      # based loosely on easypost's example https://www.easypost.com/docs/webhooks#example
      let(:event_payload) do
        {
          description: 'tracker.updated',
          result: {
            status:      event_status,
            shipment_id: easypost_shipment_id
          }
        }
      end

      before do
        allow_any_instance_of(Shipment).to receive(:buyer_email)
      end

      def post_tracker_update
        request.env['RAW_POST_DATA'] = event_payload.to_json
        post :create, auth_params
      end

      it 'updates the shipment status' do
        expect do
          post_tracker_update
        end.to change { shipment.reload.status }.to(event_status)
      end

      describe 'tracking update notification' do
        let(:buyer_email) { 'test@astroheim.com' }

        before do
          Settings.tracking_update_notification_enabled = true
          allow_any_instance_of(Shipment).to receive(:buyer_email).and_return(buyer_email)
          Delayed::Worker.delay_jobs = false
        end

        it 'notifies user of shipment update' do
          post_tracker_update

          mail = last_delivered_mail

          expect(mail.to).to include(buyer_email)
          expect(mail.body).to include(event_status)
        end
      end
    end

    context 'missing parameters' do
      it 'is unprocessable without a description' do
        request.env['RAW_POST_DATA'] = { result: {} }.to_json
        post :create, auth_params

        expect(response).to be_unprocessable
      end

      it 'is unprocessable without a result' do
        request.env['RAW_POST_DATA'] = { description: '' }.to_json
        post :create, auth_params

        expect(response).to be_unprocessable
      end
    end
  end
end
