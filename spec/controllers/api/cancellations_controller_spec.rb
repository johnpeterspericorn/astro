# encoding: utf-8
require 'spec_helper'

describe Api::CancellationsController do
  describe '#create' do
    let(:api_user) { FactoryGirl.create(:api_user) }
    let(:pawn_information) { FactoryGirl.create(:pawn_information, universal_no: 'FAAO2018280104153102896') }
    let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, pawn_information: pawn_information) }
    let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
    let(:reason) { 'MAS provided reason' }

    let(:cancellation) { double('Cancellation') }

    before do
      api_user.reset_authentication_token!
      allow(Cancellation).to receive(:new).and_return(cancellation)
    end

    context 'with param universal_no' do
      let(:params) do
        {
          format: :json,
          auth_token: api_user.authentication_token,
          universal_no: promise_offer.universal_no,
          reason: reason
        }
      end

      context 'when a vehicle with the universal # is not found' do
        let(:not_found_params) { params.merge(universal_no: 'bogus') }

        it 'returns not found' do
          post :create, not_found_params
          expect(response).to be_not_found
        end
      end

      it 'succeeds with valid parameters' do
        expect(cancellation).to receive(:process).and_return(true)

        post :create, params

        expect(response).to be_success
      end

      context 'ineligible for repurchase' do
        before do
          allow(cancellation).to receive(:process).and_return(true)
        end

        it 'initializes the cancellation with given ineligibility condition' do
          condition = 123
          expect(Cancellation).to receive(:new).with(hash_including(ineligibility_condition: condition))

          post :create, params.merge(ineligibility_condition: condition)
        end

        it 'flags cancellation as ineligible for repurchase' do
          expect(Cancellation).to receive(:new).with(hash_including(ineligible_for_repurchase: true))

          post :create, params.merge(ineligible_for_repurchase: true)
        end
      end

      let(:errors) { ['Bad return reason'] }
      let(:json_response) { JSON.parse(response.body) }
      context 'when cancellation is invalid' do
        before do
          expect(cancellation).to receive(:process).and_return(false)
        end

        it 'returns Unprocessable Entity with invalid parameters' do
          allow(cancellation).to receive(:errors) { errors }

          post :create, params

          expect(response.status).to eq(422)
          expect(json_response['errors']).to eq(errors)
        end
      end
    end

    context 'with purchace_id' do
      let(:params) do
        {
          format: :json,
          auth_token: api_user.authentication_token,
          purchase_id: promise_purchase.id,
          reason: reason
        }
      end

      it 'succeeds with valid parameters' do
        expect(cancellation).to receive(:process).and_return(true)

        post :create, params

        expect(response).to be_success
      end
    end

    context 'with vra exists' do
      let!(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }

      let(:params) do
        {
          format: :json,
          auth_token: api_user.authentication_token,
          purchase_id: promise_purchase.id,
          reason: reason

        }
      end

      it 'returns Unprocessable Entity' do
        expect(cancellation).to_not receive(:process)

        post :create, params

        expect(response).to be_unprocessable
        expect(response.status).to eq(422)
      end
    end
  end
end
