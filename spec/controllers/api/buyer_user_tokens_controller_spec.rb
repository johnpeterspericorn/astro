# encoding: utf-8
require 'requests_helper'

describe Api::BuyerUserTokensController do
  let(:user_klass) { User }
  let!(:buyer_user) { FactoryGirl.create :buyer_user }

  subject(:json_response) { OpenStruct.new JSON.parse(response.body) }

  describe 'POST #create' do
    context 'non-existent buyer user' do
      let(:username) { 'lolwut' }

      def post_create_with_username
        allow_any_instance_of(user_klass).to receive(:pawn_access_number).and_return('100000000')
        post :create, format: :json, buyer_user: { username: username }
      end

      context 'with valid buyer credentials' do
        let(:created_buyer_user) { user_klass.find_by_username(username) }

        before do
          allow_any_instance_of(user_klass).to receive(:valid_astroheim_authentication?).and_return(true)
        end

        it 'creates the User' do
          expect do
            post_create_with_username
          end.to change { user_klass.count }.by(1)
        end

        it 'responds with the authentication token' do
          post_create_with_username
          expect(json_response.auth_token).to eq(created_buyer_user.authentication_token)
        end
      end

      context 'with invalid buyer credentials' do
        it 'is not found' do
          allow_any_instance_of(user_klass).to receive(:valid_astroheim_authentication?).and_return(false)
          post_create_with_username
          expect(response).to be_not_found
        end
      end
    end

    context 'existing buyer user' do
      context 'invalid password' do
        before do
          allow_any_instance_of(user_klass).to receive(:valid_astroheim_authentication?).and_return(false)
          post :create, format: :json, buyer_user: { username: buyer_user.username }
        end

        it 'is not found' do
          expect(response).to be_not_found
        end
      end

      context 'valid password' do
        before do
          allow_any_instance_of(user_klass).to receive(:valid_astroheim_authentication?).and_return(true)
        end

        context 'user has an pawn access number' do
          before do
            allow_any_instance_of(user_klass).to receive(:pawn_access_number).and_return('100000000')
            post :create, format: :json, buyer_user: { username: buyer_user.username, password: 'lolsecret' }
          end

          it 'is successful' do
            expect(response).to be_success
            expect(json_response.success).to be_truthy
          end

          it 'responds with the authentication token' do
            buyer_user.reload # this request updates the user's token
            expect(json_response.auth_token).to eq(buyer_user.authentication_token)
          end
        end

        context 'user has no pawn access number' do
          before do
            allow_any_instance_of(user_klass).to receive(:pawn_access_number).and_return(nil)
            post :create, format: :json, buyer_user: { username: buyer_user.username, password: 'lolsecret' }
          end

          it 'responds that token auth did not succeed' do
            expect(json_response.success).to be_falsey
          end
          it 'does not provide an auth token' do
            expect(json_response.auth_token).to be_blank
          end
        end
      end
    end
  end
end
