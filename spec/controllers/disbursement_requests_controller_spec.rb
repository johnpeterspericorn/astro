# encoding: utf-8
require 'spec_helper'

describe DisbursementRequestsController do
  describe '#show' do
    let(:return_invoice) { double(id: 1) }
    let(:disbursement_request_policy) { double('policy') }

    before do
      allow(DisbursementRequestPolicy).to receive(:new) { disbursement_request_policy }
      allow(subject).to receive(:return_invoice) { return_invoice }
      sign_in
    end

    context 'when a user is not authorized to view the page' do
      before do
        allow(disbursement_request_policy).to receive(:show?) { false }
      end

      it 'forbids access' do
        get :show, return_invoice_id: return_invoice.id
        expect(response).to be_redirect
        expect(flash[:alert]).to be_present
      end
    end

    context 'when a user is authorized to view the page' do
      before do
        allow(disbursement_request_policy).to receive(:show?) { true }
      end

      it 'allows access otherwise' do
        get :show, return_invoice_id: return_invoice.id
        expect(response).to be_success
      end
    end
  end
end
