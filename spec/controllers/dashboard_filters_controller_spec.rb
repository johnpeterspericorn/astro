# encoding: utf-8
require 'requests_helper'

describe DashboardFiltersController do
  let(:buyer_user) { double('user', buyer_user?: true, super_user?: false, pawn_access_number: '100686866') }
  let(:super_user) { double('user', buyer_user?: false, super_user?: true) }
  let(:kiosk_user) { double('user', buyer_user?: false, super_user?: false, kiosk_user?: true) }

  describe '#show' do
    before do
      request.env['HTTP_REFERER'] = 'back'
      sign_in buyer_user
    end

    context 'for a buyer user' do
      it 'redirects to the agreement dashboard on param with buyer\'s AA#' do
        get :show, next: 'agreement_dashboard', pawn_access_number: buyer_user.pawn_access_number
        path = agreement_dashboard_path + "#/agreement_dashboard?pawn_access_number=#{buyer_user.pawn_access_number}"
        expect(response).to redirect_to(path)
      end
    end

    context 'for a super user' do
      it 'shows filters page' do
        sign_in super_user

        get :show, next: 'agreement_dashboard'
        expect(response).to render_template(:show)
      end
    end

    context 'for a kiosk user' do
      it 'shows filters page' do
        sign_in kiosk_user

        get :show, next: 'agreement_dashboard'
        expect(response).to render_template(:show)
      end
    end

    describe 'market index config' do
      it 'has market_index param if market index is enabled' do
        Settings.market_index_enabled = true
        get :show, next: 'agreement_dashboard', pawn_access_number: buyer_user.pawn_access_number
        path = agreement_dashboard_path + "#/agreement_dashboard?pawn_access_number=#{buyer_user.pawn_access_number}&market_index=1"
        expect(response).to redirect_to(path)
      end
    end
  end

  describe '#update' do
    before do
      request.env['HTTP_REFERER'] = 'back'
      sign_in super_user
    end

    it 'passes agreement number to the dashboard if chosen' do
      put :update, agreement_number: '1234', next: 'agreement_dashboard'
      path = agreement_dashboard_path + '#/agreement_dashboard?agreement_number=1234'
      expect(response).to redirect_to(path)
    end
  end
end
