# encoding: utf-8
require 'requests_helper'

describe DashboardScoresController do
  let(:user) { FactoryGirl.create(:buyer_user, agreement_score_viewer: true) }
  let(:record) { JSON.parse(response.body) }
  let(:agreement_number) { 5_000_000 }

  describe 'GET #index' do
    before :each do
      sign_in(user)
      FactoryGirl.create(:dashboard_score, buyer_num: agreement_number)
    end

    it 'returns an array of transactions associated with agreement_number' do
      get :index, format: :json, agreement_number: agreement_number
      expect(record['buyer_num']).to eq(5_000_000)
      expect(record['purchase_quality']).to eq(162)
      expect(record['return_score']).to eq(122)
      expect(record['margin_score']).to eq(129)

      get :index, format: :json, agreement_number: '100000001'
      record = JSON.parse(response.body)
      expect(record['buyer_num']).to be_nil
    end
  end
end
