# encoding: utf-8
require 'spec_helper'

describe CancellationsController do
  let(:kiosk_user) { FactoryGirl.create(:kiosk_user) }
  let(:super_user) { FactoryGirl.create(:super_user) }

  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promised_at: 3.days.ago) }

  describe '#new' do
    context 'vehicle returned' do
      before do
        sign_in super_user
        get :new, promise_purchase_id: promise_purchase.id
      end

      it 'succeeds' do
        expect(response).to be_success
      end
    end
  end

  describe '#create' do
    before do
      sign_in super_user
    end

    it 'processes a cancellation' do
      expect_any_instance_of(Cancellation).to receive(:process) { true }
      post :create, promise_purchase_id: promise_purchase.id, cancellation: { reason: 'foobar' }
    end
  end
end
