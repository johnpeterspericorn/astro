# encoding: utf-8
require 'requests_helper'

describe PromiseOffersController do
  before do
    stub_buyer_authorization
    sign_in
  end

  context 'when the user is authorized' do
    before do
      controller.stub_chain(:current_user, :kiosk_user?) { true }
      controller.stub_chain(:current_user, :super_user?) { false }
    end

    describe 'GET /index' do
      context 'HTML' do
        context 'when a vehicle is past cutoff' do
          let(:sold_vehicle) { FactoryGirl.create(:sold_vehicle, must_be_promised_at: 1.minute.ago) }
          let!(:pawn_location) { FactoryGirl.create(:pawn_location, initials: sold_vehicle.location_initials, purchases_enabled: true) }

          it 'marks past-cutoff offers as ineligible' do
            expect do
              get :index, badge_number: sold_vehicle.badge_no
            end.to change(PromiseOffer, :count).by(1)
            expect(PromiseOffer.last).to_not be_eligible
          end
        end
      end

      context 'JSON' do
        let(:makes) { %w(ACURA HONDA) }
        before do
          allow_any_instance_of(ErrorCorrectingPromiseOfferQuery).to receive(:by_filter_query_data) { PromiseOffer }

          makes.each { |make| FactoryGirl.create(:sold_vehicle, make: make) }
          get :index, badge_number: SasDataHelper::DEFAULT_BADGE_NUMBER, format: :json
          @parsed_response = JSON.parse(response.body)
        end

        it 'should be successful' do
          expect(response).to be_success
        end
        it 'should respond with vehicles' do
          expect(@parsed_response).not_to be_empty
        end

        it 'should include both a Honda and an Acura' do
          expect(@parsed_response.map { |vehicle| vehicle['model']['make'] }).to match_array(makes)
        end
      end
    end
  end

  context 'when the user is not authorized to use the given AA#', :vcr do
    let(:user) { User.new(buyer_user: true, username: 'cooley') }

    before do
      allow(controller).to receive(:current_user).and_return(user)
    end

    it 'fails' do
      get :index, pawn_access_number: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
      expect(response).to be_forbidden
      expect(response.body).to be_blank
    end
  end

  context 'missing filter data regression' do
    before do
      request.env['HTTP_REFERER'] = edit_vehicle_filter_path
    end

    it 'should redirect to filter page for missing AA#' do
      get :index, pawn_access_number: ''
      expect(response).to redirect_to edit_vehicle_filter_path(filter_type: :pawn_access_number)
    end
    it 'should redirect to filter page for missing badge #' do # only applies to Kiosk Users
      get :index, badge_number: ''
      expect(response).to redirect_to edit_vehicle_filter_path(filter_type: :pawn_access_number)
    end

    it 'should not record any vehicles' do
      expect(PromiseOfferRecorder).not_to receive(:record_offers)
      get :index, pawn_access_number: ''
    end
  end
end
