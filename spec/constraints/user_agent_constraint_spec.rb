# encoding: utf-8
require 'spec_helper'

describe UserAgentConstraint do
  describe '#matches?' do
    let(:expression) { %r{Totes Fab.*/1.0} }
    let(:request) { double env: {} }

    subject(:constraint) { described_class.new(expression) }

    it 'matches a user agent matching its expression' do
      request.env['HTTP_USER_AGENT'] = 'Totes Fab Browser/1.0'

      expect(constraint.matches?(request)).to be_truthy
    end

    it 'does not match user agent not matching its expression' do
      request.env['HTTP_USER_AGENT'] = 'Totes Fab Browser/1.2'

      expect(constraint.matches?(request)).to be_falsey
    end

    it 'does not match request with no user agent' do
      expect(constraint.matches?(request)).to be_falsey
    end
  end
end
