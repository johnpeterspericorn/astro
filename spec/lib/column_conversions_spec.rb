# encoding: utf-8
require 'column_conversions'
require 'spec_helper'

describe ColumnConversions do
  it 'converts value to boolean' do
    expect(described_class.value_to_boolean('true')).to be_truthy
    expect(described_class.value_to_boolean('1')).to be_truthy
    expect(described_class.value_to_boolean('false')).to be_falsey
    expect(described_class.value_to_boolean('0')).to be_falsey
  end
end
