# encoding: utf-8
require 'spec_helper'
require 'bsc_charges/client'
require 'bsc_charges/request'
require 'bsc_charges/purchase_request'
require 'bsc_charges/cancellation_request'

describe BscCharges::Client do
  let(:promise_offer) { FactoryGirl.create(:promise_offer, promise_price: 100) }

  subject { described_class }

  describe '#purchase' do
    it 'logs an attempt to charge BSC webservice', vcr: { cassette_name: 'bsc_api/successful_purchase_call' } do
      expect_any_instance_of(BscCharges::PurchaseRequest).to receive(:log_attempt)
      subject.purchase(promise_offer)
    end

    context 'when the call fails', vcr: { cassette_name: 'bsc_api/failed_purchase_call' } do
      it 'returns false and logs failure' do
        expect_any_instance_of(BscCharges::PurchaseRequest).to receive(:log_failure)
        expect(subject.purchase(promise_offer)).to be_falsey
      end
    end

    context 'when the call succeeds', vcr: { cassette_name: 'bsc_api/successful_purchase_call' } do
      it 'returns true' do
        expect(subject.purchase(promise_offer)).to be_truthy
      end
    end
  end

  describe '#cancel' do
    it 'logs an attempt to charge BSC webservice', vcr: { cassette_name: 'bsc_api/successful_cancellation_call' } do
      expect_any_instance_of(BscCharges::CancellationRequest).to receive(:log_attempt)
      subject.cancel(promise_offer)
    end

    context 'when the call fails', vcr: { cassette_name: 'bsc_api/failed_cancellation_call' } do
      it 'returns false and logs failure' do
        expect_any_instance_of(BscCharges::CancellationRequest).to receive(:log_failure)
        expect(subject.cancel(promise_offer)).to be_falsey
      end
    end

    context 'when the call succeeds', vcr: { cassette_name: 'bsc_api/successful_cancellation_call' } do
      it 'returns true' do
        expect(subject.cancel(promise_offer)).to be_truthy
      end
    end
  end

  describe '#invoke' do
    subject { described_class.new(BscCharges::Request.new(promise_offer)) }

    before do
      allow_any_instance_of(Typhoeus::Request).to receive(:run).and_return(true)
      allow_any_instance_of(BscCharges::Request).to receive(:purchase_amount).and_return(123)
    end

    describe 'on successful request' do
      it 'returns true if json has result code zero' do
        allow_any_instance_of(Typhoeus::Request).to receive(:response)
          .and_return(double(body: '{"outputParameters":{"rc": 0}}'))
        expect(subject.invoke).to eq(true)
      end
    end

    describe 'on failed request' do
      it 'returns false if json has result code other than zero' do
        allow_any_instance_of(Typhoeus::Request).to receive(:response)
          .and_return(double(body: '{"outputParameters":{"rc": 3}}'))
        expect(subject.invoke).to eq(false)
      end

      it 'returns false if the response is not JSON' do
        allow_any_instance_of(Typhoeus::Request).to receive(:response)
          .and_return(double(body: 'ServerException'))
        expect(subject.invoke).to eq(false)
      end
    end
  end
end
