# encoding: utf-8
require 'spec_helper'
require 'ods/client'
require 'ods/configuration'

describe Ods::Client do
  let(:access_token) { 'qcyq4h57xhazcfy82shd8kjx' }
  let(:endpoint) { Ods::Configuration.endpoint }
  let(:username) { Ods::Configuration.username }
  let(:password) { Ods::Configuration.password }
  let(:token) { Token.new }

  subject { described_class.new(agreement_no: '5383156', location_initials: 'PLM1', universal_no: '2018', sale_no: '58', lane_no: '15', run_no: '250') }

  context 'left_lot' do
    it 'will make a succesful api request and identify a vehicle as havnumg left the lot' do
      VCR.use_cassette 'ods/successful_token_request' do
        VCR.use_cassette 'ods/successful_api_request', allow_playback_repeats: true do
          VCR.use_cassette 'ods/successful_api_invoice_request', allow_playback_repeats: true do
            expect(subject.request_vehicle_info[:left_lot]).to eq true
          end
        end
      end
    end
  end

  context 'vehicle_payment' do
    it 'will make a succesful api request and identify vehicle_paid_by_floor_plan' do
      VCR.use_cassette 'ods/successful_api_request', allow_playback_repeats: true do
        VCR.use_cassette 'ods/successful_api_invoice_request', allow_playback_repeats: true do
          expect(subject.request_vehicle_info[:left_lot]).to eq true
          expect(subject.request_vehicle_info[:paid_by_floor_plan]).to eq true
          expect(subject.request_vehicle_info[:payment_type]).to eq Ods::Configuration::VEHICLE_FLOORED
        end
      end
    end

    it 'will make a succesful api request and identify vehicle payment type' do
      VCR.use_cassette 'ods/successful_api_request', allow_playback_repeats: true do
        VCR.use_cassette 'ods/successful_api_invoice_request', allow_playback_repeats: true do
          expect(subject.request_vehicle_info[:left_lot]).to eq true
          expect(subject.request_vehicle_info[:payment_type]).to eq Ods::Configuration::VEHICLE_FLOORED
        end
      end
    end
  end

  context 'agreementshield charge' do
    it 'will make a succesful api request and identify if agreementshield charged' do
      VCR.use_cassette 'ods/successful_api_request', allow_playback_repeats: true do
        VCR.use_cassette 'ods/successful_api_invoice_request', allow_playback_repeats: true do
          expect(subject.request_vehicle_info[:has_agreementshield_charge]).to eq true
        end
      end
    end

    it 'will make a successful api call and give false if no charge present' do
      VCR.use_cassette 'ods/successful_api_request', allow_playback_repeats: true do
        VCR.use_cassette 'ods/invoice_request_without_charge', allow_playback_repeats: true do
          expect(subject.request_vehicle_info[:has_agreementshield_charge]).to eq false
        end
      end
    end
  end
end
