# encoding: utf-8
require 'spec_helper'

describe PaymentService::Customer do
  let(:user) { FactoryGirl.create(:user_without_payment_profile) }
  let(:user_with_payment_profile) { FactoryGirl.create(:user, buyer_user: true) }

  describe '.create' do
    it 'creates a payment customer on Stripe', vcr: { cassette_name: 'payment/customer/create' } do
      expect(user.payment_customer_id).to be_nil
      described_class.create(user)
      user.reload
      expect(user.payment_customer_id).to_not be_nil
    end

    it 'creates a payment customer on Stripe despite being called twice', vcr: { cassette_name: 'payment/customer/create_twice' } do
      expect(user.payment_customer_id).to be_nil
      described_class.create(user)
      user.reload
      first_payment_customer_id = user.payment_customer_id
      expect(user.payment_customer_id).to_not be_nil
      described_class.create(user)
      user.reload
      second_payment_customer_id = user.payment_customer_id
      expect(user.payment_customer_id).to_not be_nil
      expect(first_payment_customer_id).to eq second_payment_customer_id
    end

    it 'should not create profile if one exists' do
      expect(user_with_payment_profile.payment_customer_id).to_not be_nil
      original_payment_customer_id = user_with_payment_profile.payment_customer_id
      described_class.create(user_with_payment_profile)
      user_with_payment_profile.reload
      expect(user_with_payment_profile.payment_customer_id).to eq(original_payment_customer_id)
    end
  end

  describe '#add_card' do
    let(:card_attrs) do
      {
        object: 'card',
        exp_month: '1',
        exp_year: Time.current.year + 2,
        number: '4242424242424242',
        currency: 'usd',
        cvc: 123
      }
    end

    it 'adds a card to customer', vcr: { cassette_name: 'payment/customer/add_card' } do
      described_class.create(user)
      user.reload
      customer = described_class.new(user)
      response = customer.add_card(card_attrs)
      cards = response.sources.data
      expect(cards.count).to eq(1)
      card = cards.first
      expect(card.cvc_check).to eq('pass')
      expect(card.last4).to eq('4242')
      expect(card.customer).to eq(user.payment_customer_id)
      expect(customer.card?).to be_truthy
    end
  end
end
