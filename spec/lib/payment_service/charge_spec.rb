# encoding: utf-8
require 'spec_helper'

describe PaymentService::Charge do
  let(:user) { FactoryGirl.create(:user_without_payment_profile) }
  let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:promise_offer) { FactoryGirl.build(:promise_offer, promise_250: 250) }

  describe '.promise_purchase' do
    let(:card_attrs) do
      {
        object: 'card',
        exp_month: '1',
        exp_year: Time.current.year + 2,
        number: '4242424242424242',
        currency: 'usd',
        cvc: 123
      }
    end

    def create_customer_and_add_credit_card
      PaymentService::Customer.create(user)
      user.reload
      PaymentService::Customer.new(user).add_card(card_attrs)
    end

    it 'creates a charge on Stripe in spite of missing a payment_customer_id', vcr: { cassette_name: 'payment/charge/promise_purchase_without_payment_customer_id' } do
      expect(user.payment_customer_id).to be_nil
      charge = described_class.promise_purchase(user, promise_purchase, 25_000, order_id: 'order_id_foo')
      expect { charge.run }.to raise_exception(PaymentService::Charge::Error)
      expect(user.payment_customer_id).to_not be_nil
    end

    it 'creates a charge on Stripe', vcr: { cassette_name: 'payment/charge/promise_purchase' } do
      create_customer_and_add_credit_card
      charge = described_class.promise_purchase(user, promise_purchase, 25_000, order_id: 'order_id_foo')
      charge.run
      response = charge.response
      expect(response.object).to eq('charge')
      expect(response.status).to eq('succeeded')
      expect(response.paid).to be_truthy
      expect(response.amount).to eq(25_000)
    end

    it 'creates one charge on Stripe in spite of being charged twice', vcr: { cassette_name: 'payment/charge/promise_purchase_twice' } do
      create_customer_and_add_credit_card
      first_response = described_class.promise_purchase(user, promise_purchase, 25_000, order_id: 'order_id_foo')
      first_response.run
      response = described_class.promise_purchase(user, promise_purchase, 25_000, order_id: 'order_id_foo')
      response.run
      expect(first_response.response.id).to eq(response.response.id)
    end

    context 'failures' do
      let(:promise_offer) { FactoryGirl.build(:promise_offer, promise_250: 0) }
      it 'creates a failure for a zero amount', vcr: { cassette_name: 'payment/charge/failure' } do
        create_customer_and_add_credit_card
        expect { described_class.promise_purchase(user, promise_purchase, 0, order_id: 'order_id_foo').run }.to raise_exception(PaymentService::Charge::Error)
      end
    end
  end
end
