# encoding: utf-8
require 'yaml'
require_relative '../../lib/localizer'

describe Localizer do
  let(:test_locales) { File.dirname(__FILE__) + '/../fixtures/locales' }

  after(:each) do
    FileUtils.rm_f(test_locales + '/dash.yml')
    FileUtils.rm_f(test_locales + '/one/dash.yml')
    FileUtils.rm_f(test_locales + '/two/dash.yml')
    FileUtils.rm_f(test_locales + '/two/three/dash.yml')
  end

  describe '::for_each_directory' do
    it 'iterates through locales directory' do
      dirs = []
      Localizer.for_each_directory(test_locales) do |dir|
        dirs << dir
      end
      expect(dirs.count).to eql 3
    end
  end

  describe '::dasherize' do
    let(:top_level_yaml) { { 'dash' => { 'top_directory' => { 'this' => '-----' } } } }
    let(:bottom_level_yaml) { { 'dash' => { 'three' => { 'this' => '-----' } } } }

    before do
      Localizer.dasherize(test_locales)
    end

    it 'creates a top level dash.yml file' do
      expect(YAML.load_file(test_locales + '/dash.yml')).to eql(top_level_yaml)
    end

    it 'creates a bottom level dash.yml file' do
      expect(YAML.load_file(test_locales + '/two/three/dash.yml')).to eql(bottom_level_yaml)
    end
  end
end
