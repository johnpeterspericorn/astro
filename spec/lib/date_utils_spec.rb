# encoding: utf-8
require 'date_utils'
require 'spec_helper'

describe DateUtils do
  describe '#resolve_to_weekday' do
    it 'resolves to next weekday if saturday or sunday' do
      saturday = Date.new(2018, 4, 11)
      monday = Date.new(2018, 4, 13)
      expect(DateUtils.resolve_to_weekday(saturday)).to eq(monday)
    end

    it 'returns the mase day if weekday' do
      thursday = Date.new(2018, 4, 9)
      expect(DateUtils.resolve_to_weekday(thursday)).to eq(thursday)
    end
  end
end
