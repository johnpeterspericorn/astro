# encoding: utf-8
require 'spec_helper'
require 'bsc_charges/client'
require 'bsc_charges/request'
require 'bsc_charges/purchase_request'
require 'bsc_charges/cancellation_request'

describe BscCharges::Request do
  let(:promise_offer) do
    FactoryGirl.create(:promise_offer, miles_selected: 250, days_selected: 14, promise_price: 123.45)
  end

  subject { described_class.new(promise_offer) }

  describe '#message' do
    before do
      allow_any_instance_of(described_class).to receive(:purchase_amount).and_return(123)
    end

    it 'returns a query string' do
      expect(subject.message).to be_a(String)
      expect(subject.message).to include(promise_offer.vnum)
    end

    it 'contains url encoded charge description' do
      expect(subject.message).to include('ASTRSHIELD+14D+250M')
    end
  end

  describe 'PurchaseRequest' do
    subject { BscCharges::PurchaseRequest.new(promise_offer) }

    it 'shows positive promise price for the offer' do
      expect(subject.purchase_amount).to eq 123
    end
  end

  describe 'CancellationRequest' do
    subject { BscCharges::CancellationRequest.new(promise_offer) }

    it 'shows negative promise price for the offer' do
      expect(subject.purchase_amount).to eq(-123)
    end
  end
end
