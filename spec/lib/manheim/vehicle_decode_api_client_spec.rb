# encoding: utf-8
require 'astroheim/vehicle_decode_api'
require 'spec_helper'

describe Astroheim::VehicleDecodeApi::Client do
  describe 'successful response' do
    let(:valid_vnum) { '1N4AA5AP5BC818485' }
    subject { described_class.new(valid_vnum) }

    it 'should have make, model, year in response', vcr: { cassette_name: 'vehicle_deocder_api/success' } do
      response = subject.decode_vnum

      expect(response[:make]).not_to be_blank
      expect(response[:model]).not_to be_blank
      expect(response[:year]).not_to be_blank

      expect(response[:errors]).to be_blank
    end
  end

  describe 'error response' do
    let(:invalid_vnum) { '123456789' }
    subject { described_class.new(invalid_vnum) }

    it 'should have errors component in the json', vcr: { cassette_name: 'vehicle_deocder_api/failure' } do
      response = subject.decode_vnum

      expect(response[:errors]).not_to be_empty
    end
  end
end
