# encoding: utf-8
require 'astroheim/salvage_api/add_note_message_data'

describe Astroheim::SalvageApi::AddNoteMessageData do
  let(:pawn_initials) { 'ABC' }
  let(:sblu)            { 'abc123' }
  let(:work_order)      { 123 }

  let(:note_data_class) { double('InternetNoteMessageData').as_null_object }

  let(:attributes) do
    {
      pawn_location_initials: pawn_initials,
      sblu:       sblu,
      work_order: work_order
    }
  end

  subject(:data) { described_class.new(attributes, note_data_class) }

  describe 'attributes' do
    it 'requires pawn location initials' do
      attrs = attributes.dup
      attrs.delete(:pawn_location_initials)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end

    it 'requires sblu number' do
      attrs = attributes.dup
      attrs.delete(:sblu)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end

    it 'requires work order number' do
      attrs = attributes.dup
      attrs.delete(:work_order)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end

    it 'accepts attributes for one note' do
      note_attrs = { some: :thing }
      attrs = attributes.merge(notes: note_attrs)

      expect(note_data_class).to receive(:new).once.with(note_attrs)

      described_class.new(attrs, note_data_class)
    end

    it 'accepts attributes for multiple notes' do
      notes_attrs = [{ first: :thing }, { second: :thing }]
      attrs = attributes.merge(notes: notes_attrs)

      expect(note_data_class).to receive(:new).once.with(notes_attrs.first)
      expect(note_data_class).to receive(:new).once.with(notes_attrs.last)

      described_class.new(attrs, note_data_class)
    end
  end

  describe '#to_h' do
    let(:request) { data.to_h.fetch(:request) }

    it 'includes pawn initials' do
      expect(request.fetch(:pawn_id)).to eq(pawn_initials)
    end

    it 'includes sblu number' do
      expect(request.fetch(:sblu)).to eq(sblu)
    end

    it 'includes work order number' do
      expect(request.fetch(:work_order)).to eq(work_order)
    end

    it 'includes internet notes' do
      expect(request).to include(:internet_notes)
    end
  end
end
