# encoding: utf-8
require 'astroheim/salvage_api/internet_note_message_data'
require 'date'

describe Astroheim::SalvageApi::InternetNoteMessageData do
  let(:added_by) { 'Jay H.' }
  let(:added_date) { Date.parse('2018-04-01') }
  let(:note_text) { 'Good news, everyone' }
  let(:note_type) { 'news' }

  let(:attributes) do
    {
      added_by:   added_by,
      added_date: added_date,
      text:  note_text,
      type:  note_type
    }
  end

  subject(:note_data) { described_class.new(attributes) }

  describe 'required attributes' do
    it 'requires added_by' do
      attrs = attributes.dup
      attrs.delete(:added_by)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end

    it 'requires note text' do
      attrs = attributes.dup
      attrs.delete(:text)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end

    it 'requires note type' do
      attrs = attributes.dup
      attrs.delete(:type)
      expect { described_class.new(attrs) }.to raise_error(ArgumentError)
    end
  end

  describe '#to_h' do
    let(:note_item_hash) { note_data.to_h.fetch(:item) }

    it 'includes added_by' do
      expect(note_item_hash).to include(added_by: added_by)
    end

    it 'includes added_date' do
      expect(note_item_hash).to include(added_date: '20180401')
    end

    it 'includes note_text' do
      expect(note_item_hash).to include(note_text: note_text)
    end

    it 'includes note_type' do
      expect(note_item_hash).to include(note_type: note_type)
    end
  end
end
