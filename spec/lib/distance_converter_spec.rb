# encoding: utf-8
require 'distance_converter'
require 'spec_helper'

describe DistanceConverter do
  let(:value) { 42 }
  subject { DistanceConverter.new(value) }

  it 'shows value in miles by default' do
    expect(subject.convert).to eq('42 Miles')
  end

  it 'can override units by specifying country code' do
    distance_converter = DistanceConverter.new(value, 'CA')
    allow(I18n).to receive(:translate).with('distance_units.CA') { { unit: 'Km' } }
    expect(distance_converter.convert).to eq('42 Km')
  end
end
