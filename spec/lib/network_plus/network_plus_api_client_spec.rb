# encoding: utf-8
require 'spec_helper'
require 'network_plus/network_plus_api_client'

describe NetworkPlus::NetworkPlusApiClient do
  let!(:external_vehicles_load) { FactoryGirl.create(:external_vehicles_load) }
  describe '#post' do
    describe 'on successful request' do
      it 'will return 200 response after successful_request' do
        VCR.use_cassette 'network_plus/successful_request' do
          expect(NetworkPlus::NetworkPlusApiClient.new(external_vehicles_load).post).to eq 200
        end
      end

      it 'will logs informations' do
        VCR.use_cassette 'network_plus/successful_request' do
          expect(NetworkPlusLogger).to receive(:info).at_least(3).times
          NetworkPlus::NetworkPlusApiClient.new(external_vehicles_load).post
        end
      end
    end

    describe 'on failed request' do
      it 'returns false if json has result code other than 200' do
        VCR.use_cassette 'network_plus/failed_request' do
          external_vehicles_load.update_attributes(buy_fee: nil)
          expect(NetworkPlus::NetworkPlusApiClient.new(external_vehicles_load).post).to eq 500
        end
      end
    end
  end
end
