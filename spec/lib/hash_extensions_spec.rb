# encoding: utf-8
require_relative '../../lib/hash_extensions'

describe HashExtensions do
  describe '#map_keys' do
    it 'transforms has keys by the provided map' do
      hash = { 'abc' => 123 }.extend(described_class)

      mapped_hash = hash.map_keys(&:upcase)

      expect(mapped_hash).to eq('ABC' => 123)
    end
  end
end
