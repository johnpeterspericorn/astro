# encoding: utf-8
require 'spec_helper'

describe G2gwsChargesJob do
  let(:promise_offer) { double 'PromiseOffer' }
  let(:promise_purchase) { double 'PromisePurchase', id: 1 }

  subject(:job) { described_class.new(promise_offer) }

  describe 'errors' do
    let(:delayed_job) { double 'Delayed::Job' }
    let(:exception_message) { double :message }
    let(:exception)        { double :exception, message: exception_message }
    let(:mail)             { double :mail }

    it 'notifies us of failure for the last failed attempt' do
      allow(promise_offer).to receive(:promise_purchase).and_return(promise_purchase)

      expect(G2gwsChargesMailer)
        .to receive(:notify_last_failure)
        .with(promise_offer, exception_message)
        .and_return(mail)
      expect(mail).to receive(:deliver_now)

      job.last_failure(delayed_job, exception)
    end
  end
end
