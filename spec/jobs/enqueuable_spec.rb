# encoding: utf-8
require 'spec_helper'

describe Enqueuable do
  class Job
    extend Enqueuable
  end

  describe '.enqueue' do
    it 'creates and job and sends it to delayed jobs' do
      parameter = double(:paramter)
      job       = double(:job)

      expect(Job)
        .to receive(:new)
        .with(parameter)
        .and_return(job)
      expect(Delayed::Job)
        .to receive(:enqueue)
        .with(job)

      Job.enqueue(parameter)
    end
  end
end
