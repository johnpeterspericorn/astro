# encoding: utf-8
require 'spec_helper'

describe DeleteDropNoticeJob do
  let(:return_invoice) { double 'ReturnInvoice' }

  subject(:job) { described_class.new(return_invoice) }

  describe '#perform' do
    it 'deletes the drop notice' do
      expect(Autoims::CLIENT).to receive(:delete_drop_notice).with(return_invoice)

      job.perform
    end
  end
end
