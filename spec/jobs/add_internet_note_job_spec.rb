# encoding: utf-8
require 'spec_helper'

describe AddInternetNoteJob do
  describe '#perform' do
    let(:vnum) { double :vnum }
    let(:initials) { double :location_initials }
    let(:sblu) { double :sblu }
    let(:work_order) { double :work_order }
    let(:note) { double :note }

    let(:job_attributes) do
      {
        vnum: vnum,
        pawn_location_initials: initials,
        sblu: sblu,
        work_order: work_order,
        notes: note
      }
    end

    it 'creates an internet note' do
      job = described_class.new(job_attributes)

      expect(InternetNote)
        .to receive(:create)
        .with(job_attributes)

      job.perform
    end

    it 'does not create internet note for excluded location' do
      excluded_location = described_class::EXCLUDED_LOCATIONS.first
      job_attributes[:pawn_location_initials] = excluded_location
      job = described_class.new(job_attributes)

      expect(InternetNote).not_to receive(:create)

      job.perform
    end
  end
end
