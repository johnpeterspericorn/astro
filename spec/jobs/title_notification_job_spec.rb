# encoding: utf-8
require 'spec_helper'

describe TitleNotificationJob do
  let(:return_invoice) { double 'ReturnInvoice' }
  subject(:job) { described_class.new }

  describe '#perform' do
    let(:mailer)           { double :mailer          }
    let(:delayed_job)      { double 'Delayed::Job'   }
    let(:mail)             { double :mail            }

    before do
      Settings.title_notifications_enabled = true
      allow(ReturnInvoice).to receive(:where) { [return_invoice] }
      allow(ReturnInvoiceMailer).to receive(:title_expiration_notification) { mail }
      allow(mail).to receive(:deliver_later) { true }
      allow(return_invoice).to receive(:update) { true }
    end

    it 'send notifications for return invoices where title not received' do
      expect(ReturnInvoice)
        .to receive(:where)
        .with(["vehicle_received = ? AND title_received = ? AND title_notification_sent = ? AND expiration_date > ?", true, false, false, Date.current])
      expect(ReturnInvoiceMailer)
        .to receive(:title_expiration_notification)
        .with(return_invoice)
      expect(mail).to receive(:deliver_later)
      expect(return_invoice).to receive(:update).with({title_notification_sent: true})

      job.perform
    end
  end
end
