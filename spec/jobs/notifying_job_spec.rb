# encoding: utf-8
require 'spec_helper'

describe NotifyingJob do
  class SomeJob < NotifyingJob
    def max_attempts
      2
    end
  end

  subject(:job) { SomeJob.new }

  describe '#error' do
    let(:delayed_job) { double Delayed::Job }
    let(:exception) { double }

    describe 'first failure' do
      it 'sends #first_failure to self with job and exception' do
        allow(delayed_job).to receive(:attempts).and_return(0)

        expect(job).to receive(:first_failure).with(delayed_job, exception)

        job.error(delayed_job, exception)
      end
    end

    describe 'last failure' do
      it 'sends #last_failure to self with job and exception' do
        allow(delayed_job).to receive(:attempts).and_return(1)

        expect(job).to receive(:last_failure).with(delayed_job, exception)

        job.error(delayed_job, exception)
      end
    end
  end
end
