# encoding: utf-8
require 'spec_helper'

describe G2gwsOrdersApiPurchaseJob do
  let(:offer) { double 'PromiseOffer', agreement_no: '5432100', sblu: 123, seller_paid: true, seller_paid?: true, pawn_information: { location_initials: 'ABCD' } }
  let(:consignment_id) { 123_45 }

  subject(:job) { described_class.new(offer) }

  describe '#perform' do
    before do
      allow(G2gws::InvoicesApi::Client).to receive(:get_consignment) { consignment_id }
      allow(G2gws::OrdersApi::Client).to receive(:purchase).and_return(true)
      allow(offer).to receive(:promise_purchase).and_return(double(id: 1))
    end

    it 'updates consignment_id in offer' do
      expect(offer).to receive(:update_attributes)

      job.perform
    end

    describe 'errors' do
      before do
        allow(G2gws::InvoicesApi::Client).to receive(:get_consignment).and_raise(G2gws::NonRetryableError)
      end

      it 'logs errors to Rollbar' do
        expect(Rollbar).to receive(:error)

        job.perform
      end
    end
  end
end
