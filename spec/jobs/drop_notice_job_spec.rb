# encoding: utf-8
require 'spec_helper'

describe DropNoticeJob do
  let(:return_invoice) { double 'ReturnInvoice' }

  subject(:job) { described_class.new(return_invoice) }

  describe 'errors' do
    let(:mailer)           { double :mailer          }
    let(:delayed_job)      { double 'Delayed::Job'   }
    let(:mail)             { double :mail            }
    let(:exception_message) { double :message }
    let(:exception) { double :exception, message: exception_message }

    it 'notifies of failure on the last attempt' do
      expect(mailer)
        .to receive(:notify_last_failure)
        .with(return_invoice, exception_message)
        .and_return(mail)
      expect(mail).to receive(:deliver_now)

      job.last_failure(delayed_job, exception, mailer)
    end
  end
end
