# encoding: utf-8
require 'spec_helper'

describe SowsJob do
  let(:offer) { double :promise_offer, promise_purchase: purchase }
  let(:purchase) { double :promise_purchase, id: purchase_id }
  let(:purchase_id) { double }

  subject(:job) { described_class.new(offer) }

  describe '#name' do
    it 'is its class name' do
      expect(job.name).to eq(described_class.name)
    end
  end

  describe '#last_failure' do
    it 'sends last failure message' do
      mailer = double
      error_message = double
      error = double message: error_message
      mail = double

      expect(mailer).to receive(:notify_last_failure)
        .with(offer, error_message, job)
        .and_return(mail)
      expect(mail).to receive(:deliver_now)

      job.last_failure(nil, error, mailer)
    end
  end
end
