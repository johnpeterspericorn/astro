# encoding: utf-8
require 'spec_helper'

describe PromiseChargeJob do
  let(:promise_charge) { double 'PromiseCharge' }

  subject(:job) { described_class.new(promise_charge) }

  specify '#perform' do
    expect(promise_charge).to receive(:submit)
    job.perform
  end

  describe 'errors' do
    let(:delayed_job) { double 'Delayed::Job' }
    let(:exception_message) { double :message }
    let(:exception)        { double :exception, message: exception_message }
    let(:mail)             { double :mail }

    it 'notifies us of failure for first failed attempt' do
      expect(ChargesMailer)
        .to receive(:notify_first_failure)
        .with(promise_charge, exception_message)
        .and_return(mail)
      expect(mail).to receive(:deliver_now)

      job.first_failure(delayed_job, exception)
    end

    it 'notifies us of failure for the last failed attempt' do
      expect(ChargesMailer)
        .to receive(:notify_last_failure)
        .with(promise_charge, exception_message)
        .and_return(mail)
      expect(mail).to receive(:deliver_now)

      job.last_failure(delayed_job, exception)
    end
  end
end
