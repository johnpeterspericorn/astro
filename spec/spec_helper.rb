# encoding: utf-8
if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start 'rails'
  SimpleCov.minimum_coverage 85
end

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require 'capybara/rails'
require 'mas_data_helper'
require 'paper_trail/frameworks/rspec'
require 'webmock/rspec'
require 'ods/client'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.

  config.include Capybara::DSL

  config.before type: :request do
    DatabaseCleaner.strategy = :truncation, { except: %w(charge_code_mappings) }
  end

  config.use_transactional_fixtures = false

  config.before :suite do
    DatabaseCleaner.clean
  end

  config.before do
    DatabaseCleaner.start
  end

  config.after do
    DatabaseCleaner.clean
  end

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = 'random'

  puts "Running with seed #{config.seed}" if config.seed

  config.before(:all, :draper_with_helpers) do
    c = ApplicationController.new
    c.request = ActionDispatch::TestRequest.new
    c.set_current_view_context
  end

  config.before(:each) { stub_pusher }
  config.before(:each) { stub_ods_client }
  # This stubs an HTTP request in the devise_astroheim_auth gem
  config.before(:each) { allow(Devise::AstroheimAdapter).to receive(:valid_credentials?) }
  config.before(:each) { Settings.sows_enabled = true }
  config.before(:each) { I18n.locale = :en }

  config.after(:each) { ActionMailer::Base.deliveries = [] }

  config.after(:each) do
    ActiveJob::Base.queue_adapter.enqueued_jobs.clear
    ActiveJob::Base.queue_adapter.performed_jobs.clear
  end

  config.filter_run_excluding broken: true

  config.before(:each, js: true) do
    Capybara.page.current_window.resize_to(1600, 1200)
  end
end

def enqueued_jobs
  ActiveJob::Base.queue_adapter.enqueued_jobs
end
alias enqueued_deliveries enqueued_jobs

def last_enqueued_delivery
  enqueued_deliveries.last
end

def mail_deliveries
  ActionMailer::Base.deliveries
end

def last_delivered_mail
  mail_deliveries.last
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/vcr'
  config.hook_into :webmock
  config.configure_rspec_metadata!
  config.allow_http_connections_when_no_cassette = false
  config.ignore_localhost = true # ignore local API calls for SoA stuff
end

module ControllerHelpers
  def sign_in(user = double('user'))
    if user.nil?
      allow(request.env['warden']).to receive(:authenticate!)
        .and_throw(:warden, scope: :user)
      allow(controller).to receive(:current_user).and_return(nil)
    else
      allow(request.env['warden']).to receive(:authenticate!).and_return(user)
      allow(controller).to receive(:current_user).and_return(user)
    end
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, type: :controller
  config.include ControllerHelpers,   type: :controller
  config.include UserRequestHelper,   type: :request
end

def vehicle_attributes(options = {})
  {
    agreement_no:         SasDataHelper::DEFAULT_DEALER_NUMBER,
    pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER,
    badge_no:          SasDataHelper::DEFAULT_BADGE_NUMBER,
    universal_no: 'svaa1234567890',
    vnum: 'VNUM12345678901234',
    make: 'HONDA',
    model: 'Civic EX',
    odometer_reading: 123_456,
    lane_no: '123',
    run_no: '567',
    promise_250: 100.0,
    promise_500: 200.0,
    additional_day: 5.0,
    location_initials: 'SVAA',
    sblu: '12231627',
    inspection_only_price: 125.0,
    inspection_bundle_price: 53.0,
    purchased_at: 1.day.ago
  }.merge(options)
end

def stub_pusher
  allow_any_instance_of(SelectionPusher).to receive(:push_to_dashboard) unless described_class == SelectionPusher
end

def stub_ods_client
  return if described_class == Ods::Client
  allow_any_instance_of(Ods::Client).to receive(:left_the_lot?).and_return(false)
  allow_any_instance_of(Ods::Client).to receive(:paid_by_floor_plan?).and_return(false)
  allow_any_instance_of(Ods::Client).to receive(:payment_type)
end

def each_part(mail)
  mail.parts.select { |p| p.content_type =~ /multipart/ }.flat_map(&:parts).each
end

RSpec::Matchers.define :include_in_each_part do |expected|
  match do |mail|
    each_part(mail).any? && each_part(mail).all? do |part|
      part.body.include? expected
    end
  end
end

Capybara.javascript_driver = :webkit

Capybara::Webkit.configure do |config|
  config.allow_url('js.pusher.com')
end

Capybara.configure do |config|
  config.match = :prefer_exact
  config.ignore_hidden_elements = false
end
