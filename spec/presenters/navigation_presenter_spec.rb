# encoding: utf-8
require 'spec_helper'

describe NavigationPresenter do
  let(:user) { double 'User' }

  subject(:presenter) { NavigationPresenter.new(user) }

  describe '#show_badge_button?' do
    it 'defaults to the badge ui setting' do
      allow(Settings).to receive(:show_badge_ui).and_return(true)

      expect(presenter.show_badge_button?).to be_truthy
    end

    it 'always shows button if user is super' do
      allow(Settings).to receive(:show_badge_ui).and_return(false)
      allow(user).to receive(:super_user?).and_return(true)

      expect(presenter.show_badge_button?).to be_truthy
    end

    it 'does not show button otherwise' do
      allow(Settings).to receive(:show_badge_ui).and_return(false)
      allow(user).to receive(:super_user?).and_return(false)

      expect(presenter.show_badge_button?).to be_falsey
    end
  end

  describe '#show_agreement_filter?' do
    it 'is true if the setting is enabled and the user is super' do
      allow(Settings).to receive(:show_agreement_filter_ui).and_return(true)
      allow(user).to receive(:super_user?).and_return(true)

      expect(presenter.show_agreement_filter?).to be_truthy
    end

    it 'is false if setting is false' do
      allow(Settings).to receive(:show_agreement_filter_ui).and_return(false)
      allow(user).to receive(:super_user?).and_return(true)

      expect(presenter.show_agreement_filter?).to be_falsey
    end

    it 'is false if user is not super' do
      allow(Settings).to receive(:show_agreement_filter_ui).and_return(true)
      allow(user).to receive(:super_user?).and_return(false)

      expect(presenter.show_agreement_filter?).to be_falsey
    end
  end
end
