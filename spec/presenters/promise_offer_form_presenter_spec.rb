# encoding: utf-8
require 'spec_helper'
require 'ostruct'

describe PromiseOfferFormPresenter do
  describe '#inspection_disabled_css_class' do
    let(:offer) { double 'PromiseOffer' }

    subject(:decorator) { described_class.new offer }

    it 'is disabled if offer is not eligible for inspection' do
      allow(offer).to receive(:eligible_for_inspection?).and_return(false)
      expect(decorator.inspection_disabled_css_class).to eq(:disabled)
    end

    it 'is nil otherwise' do
      allow(offer).to receive(:eligible_for_inspection?).and_return(true)
      expect(decorator.inspection_disabled_css_class).to be_nil
    end
  end

  describe '#offer_vehicle_classes' do
    specify { expect(described_class.new(OpenStruct.new(left_lot?: true)).offer_vehicle_classes).to include 'disabled_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(left_lot?: false)).offer_vehicle_classes).not_to include 'disabled_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(seller_paid?: true)).offer_vehicle_classes).to include 'seller_paid_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(seller_paid?: false)).offer_vehicle_classes).not_to include 'seller_paid_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(promise_purchase: double)).offer_vehicle_classes).to include 'promise_purchased_for_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(promise_purchase: nil)).offer_vehicle_classes).not_to include 'promise_purchased_for_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(inspection_purchase: double)).offer_vehicle_classes).to include 'inspection_purchased_for_vehicle' }
    specify { expect(described_class.new(OpenStruct.new(inspection_purchase: nil)).offer_vehicle_classes).not_to include 'inspection_purchased_for_vehicle' }
    specify do
      presenter = described_class.new(OpenStruct.new)
      allow(presenter).to receive(:vehicle_has_inspection_ui?).and_return(true)
      expect(presenter.offer_vehicle_classes).to include 'vehicle_with_inspection_ui'
    end
    specify do
      presenter = described_class.new(OpenStruct.new)
      allow(presenter).to receive(:vehicle_has_inspection_ui?).and_return(false)
      expect(presenter.offer_vehicle_classes).to include 'vehicle_without_inspection_ui'
    end
    specify do
      presenter = described_class.new(OpenStruct.new(past_cutoff?: true), User.new(super_user: true))
      expect(presenter.offer_vehicle_classes).not_to include 'past_cutoff_vehicle'
    end
    specify do
      presenter = described_class.new(OpenStruct.new(past_cutoff?: true), User.new(buyer_user: true))
      expect(presenter.offer_vehicle_classes).to include 'past_cutoff_vehicle'
    end
  end

  describe '#can_be_selected?' do
    let(:current_user) { double(:current_user) }
    let(:promise_offer) { PromiseOffer.new(must_be_promised_at: 1.day.from_now, left_lot: false) }
    subject(:presenter) { described_class.new(promise_offer.decorate, current_user) }
    before do
      allow(current_user).to receive(:super_user?).and_return(false)
    end

    it 'should be false for promised vehicles' do
      allow(presenter).to receive(:not_yet_promised?).and_return(false)
      expect(presenter.can_be_selected?).to be_falsey
    end

    it 'should false for vehicles with inspections' do
      allow(promise_offer).to receive(:has_inspection?).and_return(true)
      expect(presenter.can_be_selected?).to be_falsey
    end

    it 'should be false for left lot vehicles' do
      allow(promise_offer).to receive(:left_lot?).and_return(true)
      expect(presenter.can_be_selected?).to be_falsey
    end

    it 'cannot be selected when the promise offer is past cutoff' do
      allow(promise_offer).to receive(:must_be_promised_at).and_return(1.day.ago)
      expect(presenter.can_be_selected?).to be_falsey
    end

    it 'should be false for vehicles that are past time and non-super-users' do
      allow(promise_offer).to receive(:must_be_promised_at).and_return(1.day.ago)
      expect(presenter.can_be_selected?).to be_falsey
    end

    it 'should be true for vehicles that are past time and super-users' do
      allow(current_user).to receive(:super_user?).and_return(true)
      allow(promise_offer).to receive(:must_be_promised_at).and_return(1.day.ago)
      expect(presenter.can_be_selected?).to be_truthy
    end

    it 'should be true for un-promised vehicles without inspections which are neither past-time or checked-out' do
      allow(promise_offer).to receive(:not_yet_promised?).and_return(true)
      expect(presenter.can_be_selected?).to be_truthy
    end
  end

  describe '#vehicle_has_inspection_ui?' do
    subject { described_class.new(PromiseOffer.new.decorate) }

    specify 'it should be false if a location does not have inspections' do
      allow(subject).to receive(:purchase_location_has_inspections?).and_return(false)
      expect(subject.vehicle_has_inspection_ui?).to be_falsey
    end

    specify 'it should be false if the offer lacks inspection pricing information' do
      allow(subject).to receive(:purchase_location_has_inspections?).and_return(true)
      allow(subject).to receive(:has_inspection_pricing?).and_return(false)
      expect(subject.vehicle_has_inspection_ui?).to be_falsey
    end

    specify 'it should be true if the offer location has inspections and the offer has inspection pricing information' do
      allow(subject).to receive(:purchase_location_has_inspections?).and_return(true)
      allow(subject).to receive(:has_inspection_pricing?).and_return(true)
      expect(subject.vehicle_has_inspection_ui?).to be_truthy
    end
  end
end
