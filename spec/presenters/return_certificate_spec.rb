# encoding: utf-8
require 'spec_helper'

describe ReturnCertificate do
  let(:additional_information) { 'The User added some information' }
  let(:invoice)               { double :return_invoice, user_email: email, additional_information: additional_information }
  let(:purchase)              { double :promise_purchase }
  let(:offer)                 { double :promise_offer    }
  let(:pawn_info)          { double :pawn_information }
  let(:email)                 { 'test@example.com' }
  let(:changed_mind_reason)   { I18n.t('return_invoice.return_reasons.changed_mind') }
  let(:other_reason)          { I18n.t('return_invoice.return_reasons.other')        }
  let(:reasons)               { "#{changed_mind_reason}, #{other_reason}"            }
  let!(:usa_currency_code) { '$' }

  subject(:certificate) { described_class.new invoice }

  before do
    allow(offer).to receive(:decorate).and_return(offer)
    allow(purchase).to receive(:decorate).and_return(purchase)
    allow(purchase).to receive(:promise_offer).and_return(offer)
    allow(invoice).to receive(:promise_purchase).and_return(purchase)
    allow(offer).to receive(:country).and_return('United States of America')

    ReturnInvoice::RETURN_REASONS.each do |reason|
      allow(invoice).to receive(reason).and_return(false)
    end
    allow(invoice).to receive(:changed_mind).and_return(true)
    allow(invoice).to receive(:other).and_return(true)
  end

  specify '#initiation_date' do
    expect(invoice).to receive :created_at
    certificate.initiation_date
  end

  specify '#vehicle_purchase_date' do
    allow(offer).to receive(:purchased_at) { Time.new(2018, 2, 4) }
    expect(certificate.vehicle_purchase_date).to eql '02/04/12'
  end

  specify '#odometer_limit' do
    odometer_reading = 1000
    miles_selected = 100
    allow(offer).to receive(:odometer_reading).and_return(odometer_reading)
    allow(purchase).to receive(:miles_selected).and_return(miles_selected)

    expect(certificate.odometer_limit).to eq(1100)
  end

  specify '#return_location' do
    return_location = 'Return Location'
    allow(invoice).to receive(:pawn_location).and_return(double(name: return_location))

    expect(certificate.return_location).to eq(return_location)
  end

  specify '#return_reasons' do
    expect(certificate.return_reasons).to match reasons
    expect(certificate.return_reasons).to match additional_information
  end

  describe '#vehicle_purchase_price' do
    it 'delegates to promise offer' do
      expect(offer).to receive :vehicle_purchase_price
      certificate.vehicle_purchase_price
    end

    it 'defaults to zero' do
      allow(pawn_info).to receive(:vehicle_purchase_price).and_return(0)
      expect(offer).to receive(:vehicle_purchase_price).and_return('$ 0')
      expect(certificate.vehicle_purchase_price).to eql "#{usa_currency_code} 0"
    end
  end

  describe '#buy_fee' do
    it 'delegates to promise offer' do
      expect(offer).to receive :adjustments_buy_fee
      certificate.adjustments_buy_fee
    end

    it 'defaults to zero' do
      allow(offer).to receive(:adjustments_buy_fee).and_return(nil)
      expect(certificate.adjustments_buy_fee).to eql "#{usa_currency_code} 0"
    end
  end

  specify '#total_refund' do
    allow(offer).to receive(:source).and_return(double('offer', vehicle_purchase_price: 1000))
    allow(offer).to receive(:adjustments_buy_fee).and_return(100)
    expect(certificate.total_refund).to eql "#{usa_currency_code} 1,100"
  end

  specify '#purchase_location' do
    purchase_location = 'Purchase Location'
    allow(offer).to receive(:purchase_location).and_return(double(name: purchase_location))

    expect(certificate.purchase_location).to eq(purchase_location)
  end

  context 'authorization service provided information' do
    before do
      allow(offer).to receive :pawn_access_no
    end

    specify '#agreementship_addr' do
      agreementship_addr = double :address

      expect_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS)
        .to receive(:get_agreementship_addr)
        .and_return(agreementship_addr)

      expect(certificate.agreementship_addr).to eq(agreementship_addr)
    end
  end
end
