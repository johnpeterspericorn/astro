# encoding: utf-8
FactoryGirl.define do
  factory :flat_rate do
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER

    target_price_360 456.0

    target_price_360_psi 222.0

    target_price_500 666.0

    target_price_500_psi 950.0

    inspection_adjustment_price 10.0
    days_adjustment_price       20.0
    miles_adjustment_price      30.0
    country 'United States of America'
  end

  factory :flat_rate_canada, class: FlatRate do
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER

    target_price_360 4560.0

    target_price_360_psi 2220.0

    target_price_500 6660.0

    target_price_500_psi 9500.0

    inspection_adjustment_price 10.0
    days_adjustment_price       20.0
    miles_adjustment_price      30.0
    country 'Canada'
  end
end
