# encoding: utf-8
FactoryGirl.define do
  factory :floor_plan_branch do
    floor_plan_company
    name 'Limb'
    sequence(:site_no, &:to_s)
    sequence(:address_street) { |n| "#{n} St." }
    sequence(:address_suite, &:to_s)
    address_city 'Chicago'
    address_state 'GA'
    sequence(:address_zipcode) { |n| n.to_s.rjust(5, '0') }
  end
end
