FactoryGirl.define do
  factory :market_index do
    notes 'We understand it\'s getting harder to get the right vehicles at the right price. Now\'s a good time to familiarize yourself with how your performance can impact your rate.'
    value 84
  end
end
