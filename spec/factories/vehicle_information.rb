# encoding: utf-8
FactoryGirl.define do
  factory :vehicle_information do
    make    SasDataHelper::DEFAULT_VEHICLE_MAKE
    vnum     { SecureRandom.hex[0..16].upcase }
    odometer_reading 1000
  end
end
