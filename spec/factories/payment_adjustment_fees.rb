# encoding: utf-8
FactoryGirl.define do
  factory :adj_fees1, class: PaymentAdjustmentFee do
    payment
    item_description 'ABC123'
    fee_amount 100
    adjustment_code '123ABC'
  end

  factory :adj_fees2, class: PaymentAdjustmentFee do
    payment
    item_description 'ABC456'
    fee_amount 200
    adjustment_code '456ABC'
  end

  factory :adj_fees3, class: PaymentAdjustmentFee do
    payment
    item_description 'ABC789'
    fee_amount 300
    adjustment_code '789ABC'
  end
end
