# encoding: utf-8
FactoryGirl.define do
  factory :flat_rate_inquiry do
    pawn_access_no '100000'
    agreement_no         '55555'
    miles_selected    '360'
    user { |b| b.association(:super_user) }
  end
end
