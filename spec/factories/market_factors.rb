FactoryGirl.define do
  factory :market_factor do
    title "Factor ABC"
    description "Factor ABC is due to XYZ, and is calculated with the data from data.gov"
    color "#EE0000"
    market_index
  end
end
