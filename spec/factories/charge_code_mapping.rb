# encoding: utf-8
FactoryGirl.define do
  factory :charge_code_mapping do
    seller_paid false
    automatic_purchase true
    product 'purchase_protect'
    limited_volume false
    percent_coverage false
    charge_code 962
    call_id 'DEALB'
  end
end
