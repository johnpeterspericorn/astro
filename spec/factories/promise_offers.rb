# encoding: utf-8
FactoryGirl.define do
  factory :promise_offer do
    preselected_promise_kind 'purchase_protect'
    transient do
      pawn_access_no nil
    end

    promise_250  100.0
    promise_500  150.0
    additional_day 5.0
    ineligibility_condition 0
    vehicle_information

    pawn_information do
      build(:pawn_information, pawn_access_no: pawn_access_no)
    end

    purchased_at { Time.current }

    factory :ineligible_promise_offer do
      ineligibility_condition 1
    end

    factory :recently_created_promise_offer do
      purchased_at { 5.minutes.ago }
    end

    factory :day_old_promise_offer do
      purchased_at { 1.day.ago }
    end
  end
end
