# encoding: utf-8
FactoryGirl.define do
  factory :payment_response do
    payment
    promise_purchase
    raw_response "{:new_order_response=>{:return=>{:industry_type=>\"EC\", :trans_type=>\"AC\", :bin=>\"000001\", :merchant_id=>\"211707\", :terminal_id=>\"001\", :card_brand=>\"AX\", :order_id=>\"192566_ed1652af1b\", :tx_ref_num=>\"55E3D001A2FA419DD919B2B3ACD1236FB38C537B\", :tx_ref_idx=>\"1\", :resp_date_time=>\"20180830235442\", :proc_status=
    >\"0\", :approval_status=>\"1\", :resp_code=>\"00\", :avs_resp_code=>\"A \", :cvv_resp_code=>\"M\", :authorization_code=>\"235734\", :mc_recurring_adv_code=>{:\"@xsi:type\"=>\"xsd:string\"}, :visa_vb_v_resp_code=>{:\"@xsi:type\"=>\"xsd:string\"}, :proc_status_message=>\"Approved\", :resp_code_message=>{:\"@xsi:type\"=>\"xsd:string\"}, :host_resp_code=>\"100\", :
    host_avs_resp_code=>\"I2\", :host_cvv_resp_code=>\"M\", :retry_trace=>{:\"@xsi:type\"=>\"xsd:string\"}, :retry_attemp_count=>{:\"@xsi:type\"=>\"xsd:string\"}, :last_retry_date=>{:\"@xsi:type\"=>\"xsd:string\"}, :customer_ref_num=>{:\"@xsi:type\"=>\"xsd:string\"}, :customer_name=>{:\"@xsi:type\"=>\"xsd:string\"}, :profile_proc_status=>{:\"@xsi:type\"=>\"xsd:strin
    g\"}, :profile_proc_status_msg=>{:\"@xsi:type\"=>\"xsd:string\"}, :\"@xsi:type\"=>\"ns:NewOrderResponseElement\"}, :@xmlns=>\"urn:ws.paymentech.net/PaymentechGateway\", :\"@xsi:type\"=>\"ns:NewOrderResponse\"}, :@id=>\"_0\"}"
  end

  factory :payment_response_with_error, class: PaymentResponse do
    promise_purchase
    raw_response        '{:fault=>{:faultcode=>"SOAP-ENV:Server", :faultstring=>"  841 Error validating card/account number range"}, :@id=>"_0"}'
    error_message       '(SOAP-ENV:Server)   841 Error validating card/account number range'
  end
end
