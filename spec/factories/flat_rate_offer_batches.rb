# encoding: utf-8
FactoryGirl.define do
  factory :flat_rate_offer_batch do
    sequence(:pawn_access_no, &:to_s)

    factory :flat_rate_offer_batch_with_offer do
      after(:create) do |batch|
        batch.flat_rate_offers = [create(:flat_rate_offer, flat_rate_offer_batch: batch)]
      end
    end
  end
end
