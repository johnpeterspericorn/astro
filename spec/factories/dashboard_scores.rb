# encoding: utf-8
FactoryGirl.define do
  factory :dashboard_score do
    buyer_num         5_000_000
    payment_kind      'DS360'
    avg_loss          758.923
    return_rate       5.808
    volume            24.89
    margin            70.62
    earnings          2636.769
    return_score      122.0
    purchase_quality  162
    margin_score      129
    volume_score      155
    earnings_score    167
    date              Time.zone.today
  end
end
