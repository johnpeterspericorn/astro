# encoding: utf-8
FactoryGirl.define do
  factory :shipment do
    easypost_shipment_id 'shp_ABC123'
    tracking_code '1ZABC123'
    carrier 'UPS'
  end
end
