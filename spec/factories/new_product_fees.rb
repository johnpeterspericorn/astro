# encoding: utf-8
FactoryGirl.define do
  factory :new_product_fee do
    agreement_no ''
    seller_no ''
    seller_paid false
    timeframe 365
    count 3
    fee 0.0
  end

  factory :new_product_fee_365_1, class: NewProductFee do
    agreement_no ''
    seller_no ''
    seller_paid false
    timeframe 365
    count 6
    fee 145.0
  end

  factory :new_product_fee_365_999, class: NewProductFee do
    agreement_no ''
    seller_no ''
    seller_paid false
    timeframe 365
    count 99_999
    fee 254.0
  end
end
