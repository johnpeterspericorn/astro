# encoding: utf-8
FactoryGirl.define do
  factory :promise_extension do
    vnum { SecureRandom.hex[0..16].upcase }
    additional_days 2
  end
end
