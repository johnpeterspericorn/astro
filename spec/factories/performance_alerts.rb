# encoding: utf-8
FactoryGirl.define do
  factory :performance_alert do
    current_user { |b| b.association(:super_user) }
    relationship_manager { |b| b.association(:relationship_manager_1) }
    emails 'sfdfdf-fghfh@sdasds.com'
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER
    return_rate true
    lpc true
    follow_up_date 5.days.from_now
    letter_sent 5.days.ago
  end
end
