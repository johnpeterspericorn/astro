# encoding: utf-8
FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "user#{n}" }
    sequence(:email) { |n| "superuser#{n}@astroheim.com" }
    sequence(:payment_customer_id) { |n| "cust_#{n}" }

    factory :super_user do
      super_user true
    end

    factory :admin_user do
      super_user true
      admin true
    end

    factory :api_user do
      api_user true
    end

    factory :buyer_user do
      buyer_user true
      authentication_token { SecureRandom.hex }
    end

    agreement_score_viewer false

    trait :no_payment_profile do
      buyer_user true
      payment_customer_id nil
    end

    factory :user_without_payment_profile, traits: [:no_payment_profile]
  end
end
