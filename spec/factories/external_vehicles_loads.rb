# encoding: utf-8
require_relative '../mas_data_helper'

FactoryGirl.define do
  factory :external_vehicles_load do
    additional_day 10
    badge_no SasDataHelper::DEFAULT_BADGE_NUMBER
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER
    promise_250 100
    promise_500 200
    inspection_bundle_price 250
    inspection_only_price 120
    lane_no SasDataHelper::DEFAULT_LANE_NUMBER
    location_initials 'SVAA'
    make 'GENUINE'
    model 'FORD'
    odometer_reading 100
    buy_fee 11
    vehicle_total 10_011
    purchased_at { 1.day.ago }
    run_no  SasDataHelper::DEFAULT_RUN_NUMBER
    sale_no 1
    sequence(:sblu) { |n| n }
    vehicle_purchase_price SasDataHelper::DEFAULT_VEHICLE_PRICE
    vnum { SecureRandom.hex[0..16].upcase }
    year 2018
    batch_id 1
    automatic_purchase_email SasDataHelper::DEFAULT_PURCHASE_EMAIL
    rejecting_user { |b| b.association(:admin_user) }
    user { |b| b.association(:super_user) }
    external_vehicles_batch { |b| b.association(:external_vehicles_batch) }
  end
end
