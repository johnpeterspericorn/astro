# encoding: utf-8
FactoryGirl.define do
  factory :rate_change do
    rate_changing_user { |b| b.association(:buyer_user) }
    link_clicked_user { |b| b.association(:buyer_user) }
    emails 'sfdfdf-fghfh@sdasds.com'
    relationship_manager { |b| b.association(:relationship_manager_1) }
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER
    return_rate 10.0
    effective_date 6.days.ago
    letter_sent 5.days.ago
    link_clicked_at 3.days.ago
  end
end
