# encoding: utf-8
FactoryGirl.define do
  factory :payment do
    order_id 'ABC123'
    authorization_code 'DEF456'
    amount 123.4
    promise_price 123.4
    return_fee nil
    created_at Date.today
    credit_card_number'1234567890'
    credit_card_type'VI'
    address_zip 60_007
    address_street '729 Cutter lane'
    address_city 'New York City'
    address_state 'New York'
  end
end
