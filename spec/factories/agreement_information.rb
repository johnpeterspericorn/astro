# encoding: utf-8
FactoryGirl.define do
  factory :agreement_information do
    agreementship_name 'NMH'
    address_street  '1989 meso Ave.'
    address_city    'Chicago'
    address_state   'GA'
    address_zipcode '30317'
    agreement_no       '5000000'
  end
end
