# encoding: utf-8
FactoryGirl.define do
  factory :vnum_blacklist do
    vnum { SecureRandom.hex }
  end
end
