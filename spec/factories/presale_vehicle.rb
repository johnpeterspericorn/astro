# encoding: utf-8
FactoryGirl.define do
  factory :presale_vehicle do
    make    'GENUINE'
    vnum     { SecureRandom.hex[0..16].upcase }
  end
end
