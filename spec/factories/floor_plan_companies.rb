# encoding: utf-8
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :floor_plan_company do
    sequence(:name) { |n| "Company#{n}" }
    sequence(:company_code) { |n| "NMH#{n}" }
  end

  factory :company1, class: FloorPlanCompany do
    name 'company1'
    company_code 'co1'
  end

  factory :company2, class: FloorPlanCompany do
    name 'company2'
    company_code 'co2'
  end
end
