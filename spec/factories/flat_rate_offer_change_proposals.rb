# encoding: utf-8
FactoryGirl.define do
  factory :flat_rate_offer_change_proposal do
    flat_rate_offer
    price 123.0
  end
end
