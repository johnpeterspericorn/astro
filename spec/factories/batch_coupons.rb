# encoding: utf-8
FactoryGirl.define do
  factory :batch_coupon do
    association :coupon, :consumed
    association :coupon_batch
  end

  factory :batch_1, class: BatchCoupon do
    association :coupon, factory: :unconsumed
    association :coupon_batch, factory: :agreementshield_batch
  end
end
