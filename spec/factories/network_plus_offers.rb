# encoding: utf-8
FactoryGirl.define do
  factory :network_plus_offer do
    offering_user { |b| b.association(:super_user) }
    accepting_user { |b| b.association(:super_user) }
    emails 'sfdfdf-fghfh@sdasds.com'
    state 'confirmed'
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER
    existing_network_rate 22
    network_plus_surcharge 100
    expiration_date 5.days.from_now
    letter_sent 5.days.ago
    accepted 7.days.from_now
    accepted_at 7.days.from_now
  end
end
