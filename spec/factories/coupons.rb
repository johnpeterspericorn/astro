# encoding: utf-8
FactoryGirl.define do
  factory :coupon do
    code 'ASD123'
    amount 12.0
    expiration_date 1.day.from_now
    country 'United States of America'

    trait :consumed do
      promise_purchase
    end

    trait :expired do
      expiration_date 1.day.ago
    end
  end

  factory :unconsumed, class: Coupon do
    code 'abc123'
    amount 14.0
    expiration_date 1.day.ago
    country 'Canada'
  end
end
