# encoding: utf-8
FactoryGirl.define do
  factory :buyer_information do
    pawn_access_no { SecureRandom.hex }
    name 'Foo Bar'
    emails 'buyer@yahoo.com'
    cell_phone '555-5555'
    office_phone '555-3232'
  end
end
