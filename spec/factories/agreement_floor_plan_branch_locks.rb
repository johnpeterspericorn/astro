# encoding: utf-8
FactoryGirl.define do
  factory :agreement_floor_plan_branch_lock do
    agreement_no 'MyString'
    company_code 'MyString'
    site_no 'MyString'
  end
end
