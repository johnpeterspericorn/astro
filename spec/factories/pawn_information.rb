# encoding: utf-8
require_relative '../mas_data_helper'

FactoryGirl.define do
  factory :pawn_information do
    lane_no           SasDataHelper::DEFAULT_LANE_NUMBER
    run_no            SasDataHelper::DEFAULT_RUN_NUMBER
    sale_no           1
    vehicle_purchase_price SasDataHelper::DEFAULT_VEHICLE_PRICE
    sblu '123'
    pawn_location

    factory :pawn_information_with_offer do
      promise_offer
    end
  end
end
