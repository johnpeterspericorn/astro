# encoding: utf-8
FactoryGirl.define do
  factory :floor_plan_branch_selection do
    agreement_no '5432100'
    company_code 'ABCD'
    site_no '1234'
  end
end
