# encoding: utf-8
FactoryGirl.define do
  factory :promise_purchase do
    kind 'purchase_protect'
    transient do
      pawn_access_no nil
    end

    promise_offer do
      build(:promise_offer, pawn_access_no: pawn_access_no)
    end
    additional_days 0
    miles_selected 250
    channel PromiseChannel::ASTRO

    factory :returnable_promise_purchase do
      promise_offer do
        build(:day_old_promise_offer)
      end
    end
  end
end
