# encoding: utf-8
require_relative '../mas_data_helper'

FactoryGirl.define do
  factory :sold_vehicle do
    additional_day 10
    pawn_access_no SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
    badge_no SasDataHelper::DEFAULT_BADGE_NUMBER
    agreement_no SasDataHelper::DEFAULT_DEALER_NUMBER
    promise_250 100
    promise_500 200
    inspection_bundle_price 250
    inspection_only_price 120
    lane_no SasDataHelper::DEFAULT_LANE_NUMBER
    location_initials 'SVAA'
    make 'GENUINE'
    purchased_at { 1.day.ago }
    run_no  SasDataHelper::DEFAULT_RUN_NUMBER
    sale_no 1
    sequence(:sblu) { |n| n }
    universal_no { "SVAA#{rand(1000..9999)}" }
    work_order_number 1
    vehicle_purchase_price SasDataHelper::DEFAULT_VEHICLE_PRICE
    vnum { SecureRandom.hex[0..16].upcase }
    year 2018
  end
end
