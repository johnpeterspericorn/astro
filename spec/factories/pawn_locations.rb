# encoding: utf-8
FactoryGirl.define do
  factory :pawn_location do
    initials 'AAAA'
    name 'AAA'
    country 'United States of America'
    partner_network 'US-Astroheim'
  end

  factory :oklahoma_location, class: PawnLocation do
    initials 'OKHM'
    name 'Oklahoma'
    partner_network 'US-NonAstroheim'
  end

  factory :svaa_location, class: PawnLocation do
    initials 'SVAA'
    name 'Sva'
    country 'United States of America'
    partner_network 'US-Astroheim'
  end

  factory :ca_location, class: PawnLocation do
    initials 'MTRL'
    name 'Astroheim Montreal'
    country 'Canada'
    partner_network 'CA-Astroheim'
    tax_rate 10
  end

  factory :canadian_location, class: PawnLocation do
    initials 'CVAA'
    name 'Statesville'
    country 'Canada'
    partner_network 'CA-Astroheim'
    tax_rate 15
  end
end
