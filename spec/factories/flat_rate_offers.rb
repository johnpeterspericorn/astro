# encoding: utf-8
FactoryGirl.define do
  factory :flat_rate_offer do
    flat_rate_offer_batch
    flat_rate
    price 321.0
    miles_selected 360
    offering_user { |b| b.association(:super_user) }
  end
end
