# encoding: utf-8
FactoryGirl.define do
  factory :return_invoice do
    association :promise_purchase, factory: :returnable_promise_purchase
    odometer_on_return 1000
    pawn_location
    mechanical_issues true
    returning_user { FactoryGirl.create(:super_user) }
    title_status 'X'
    shipment
  end

  factory :ca_return_invoice, class: ReturnInvoice do
    association :promise_purchase, factory: :returnable_promise_purchase
    odometer_on_return 1000
    pawn_location { FactoryGirl.create(:ca_location) }
    mechanical_issues true
    returning_user { FactoryGirl.create(:super_user) }
    title_status 'X'
    shipment
  end
end
