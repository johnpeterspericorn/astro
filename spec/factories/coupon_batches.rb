# encoding: utf-8
FactoryGirl.define do
  factory :coupon_batch do
    name 'Astroheim Royalty'
  end
  factory :agreementshield_batch, class: CouponBatch do
    name 'AgreementShield Royalty'
  end
end
