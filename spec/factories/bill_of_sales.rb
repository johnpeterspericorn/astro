# encoding: utf-8
# Read about factories at https//github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bill_of_sale do
    vnum 'ASSFKJKLJ12334KLH'
    make 'Chevy Opra'
    year 2018
    price 200
    odometer_reading 100
    seller_agreement_name 'Radhika'
    seller_address '234 ASDF'
    seller_postal_code '60007 ON'
    seller_phone 1_234_567
    seller_5mil 5_241_420
    promise_purchase do
      build(:promise_purchase)
    end
  end
end
