# encoding: utf-8
module SasDataHelper
  DEFAULT_AUCTION_ACCESS_NUMBER = '100686866'.freeze  # Felicia Rawles AA#
  DEFAULT_DEALER_NUMBER         = '5241420'.freeze    # Felicia Rawles 5m#
  DEFAULT_BADGE_NUMBER          = '1010'.freeze
  DEFAULT_LANE_NUMBER           = '12'.freeze
  DEFAULT_RUN_NUMBER            = '34'.freeze
  DEFAULT_RISK_FACTOR           = 0.0
  DEFAULT_RANGE_LOWER_LIMIT     = 10.2
  DEFAULT_RANGE_UPPER_LIMIT     = 20.3
  DEFAULT_VEHICLE_CODE          = 1
  DEFAULT_VEHICLE_PRICE         = 10_000.0
  DEFAULT_VEHICLE_MAKE          = 'FACTROID'.freeze
  DEFAULT_PURCHASE_EMAIL        = 'test_buyer@astroheim.com'.freeze
end
