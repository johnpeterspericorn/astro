# encoding: utf-8
require 'spec_helper'

describe Cancellation do
  let(:price) { 42 }
  let(:promise_offer) { FactoryGirl.create(:promise_offer, promise_price: price, days_selected: 7, miles_selected: 250) }
  let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:reason) { 'foobar' }

  subject(:cancellation) do
    described_class.new(
      promise_purchase_id: promise_purchase.id,
      reason: reason
    )
  end

  before do
    Delayed::Worker.delay_jobs = true
  end

  after do
    Delayed::Worker.delay_jobs = false
  end

  context 'SOWS integration' do
    before do
      allow_any_instance_of(PromiseOffer).to receive(:charge_present_in_ods?) { true }
      Settings.sows_enabled = true
    end

    it "sends offer's days and miles as they are before cancellation" do
      promise_offer.pawn_information.update_attributes!(sblu: '123')

      expect(Astroheim::Sows::Client).to receive(:new) do |request|
        request_offer = request.request_data.promise_offer

        expect(request_offer.days_selected).to eql promise_offer.days_selected
        expect(request_offer.miles_selected).to eql promise_offer.miles_selected

        double(invoke_client: true)
      end

      cancellation.process
      Delayed::Job.last.invoke_job
    end
  end
end
