# encoding: utf-8
require 'spec_helper'
require 'requests_support/data_helper'
require 'requests_support/login_helper'
require 'requests_support/kiosk_helper'
require 'requests_support/navigation_helper'

def promise_label(vehicle, options = {})
  if options[:miles] && options[:days]
    ['vehicle', vehicle.vnum, 'promise_options', options[:miles], options[:days]].join('_')
  else
    ['vehicle', vehicle.vnum, 'promise_options', options[:other] || 'none'].join('_')
  end
end

def select_date(date, options = {})
  field = options.fetch(:from)
  select date.strftime('%Y'), from: "#{field}_1i" # year
  select date.strftime('%B'), from: "#{field}_2i" # month
  select date.strftime('%-d'), from: "#{field}_3i" # day
end

def select_date_and_time(date, options = {})
  field = options[:from]
  select_date date, from: field
  select date.strftime('%H'), from: "#{field}_4i" # hour
  select date.strftime('%M'), from: "#{field}_5i" # minute
end

def inspection_label(vehicle)
  "vehicle_#{vehicle.vnum}_inspection"
end

def page_should_have_vehicle_makes(*vehicles)
  vehicles.each do |vehicle|
    expect(page).to have_content vehicle.make.titleize
  end
end

def page_should_not_have_vehicle_makes(*vehicles)
  vehicles.each do |vehicle|
    expect(page).not_to have_content vehicle.make.titleize
  end
end

def page_should_be_root_page
  expect(page.current_path).to eql root_path
end
