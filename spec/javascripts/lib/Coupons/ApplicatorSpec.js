describe('Applicator', function(){
  describe('#apply', function(){
    var applicator, display, promisePrice, promise, registry;
    var ajax = function() {
      return promise;
    };

    beforeEach(function(){
      display = jasmine.createSpy();
      promisePrice = {};
      registry = { codes: ['xyz'] } ;
      promisePrice.applyCoupon = function(){};
      promise = {};
      promise.done = function() { return promise };
      promise.fail = function() { return promise };

      applicator = new Coupons.Applicator(promisePrice, registry, display);
    });

    it('returns promise', function(){
      expect(applicator.apply('Canada', 'abc', ajax)).toEqual(promise);
    });

    describe('success', function(){
      var couponData;

      beforeEach(function(){
        couponData = { coupon: { country: 'Canada', code: 'abc', amount: 123 } };

        promise.done = function(c){
          c(couponData);
          return promise;
        };
      });

      it('applies coupon to promise price', function(){
        var coupon = new Coupons.Coupon(couponData.coupon);

        spyOn(promisePrice, 'applyCoupon');

        applicator.apply('Canada', coupon.code, ajax);

        expect(promisePrice.applyCoupon).toHaveBeenCalledWith(coupon);
      });

      it('displays success message', function(){
        applicator.apply('Canada', 'abc', ajax);

        expect(display).toHaveBeenCalledWith('The coupon "abc" has been applied');
      });
    });

    describe('failure', function(){
      it('displays parsed error message', function(){
        var xhr = {};
        xhr.responseText = '{"errors":":boom:"}';

        promise.fail = function(c){
          c(xhr);
          return promise;
        };

        applicator.apply('Canada', 'abc', ajax);

        expect(display).toHaveBeenCalledWith('The coupon failed with the error: :boom:');
      });
    });
  });
});
