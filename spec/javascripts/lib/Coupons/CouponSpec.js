describe("Coupon", function(){
  var data = { amount: 123, code: 'abc123' }

  it("is initialized with an amount", function(){
    coupon = new Coupons.Coupon(data);

    expect(coupon.amount).toEqual(123);
  });

  it("is initialized with a code", function(){
    coupon = new Coupons.Coupon(data);

    expect(coupon.code).toEqual('abc123');
  });
});
