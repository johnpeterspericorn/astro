describe('Coupons.Registry', function(){
  describe('#initialization', function(){
    it('may be initialized with pre-registered codes', function(){
      var registry = new Coupons.Registry(['abc123']);

      expect(registry.codes).toEqual(['abc123']);
    });
  });

  describe('#register', function(){
    it('adds registered code to registry', function(){
      var registry = new Coupons.Registry();
      registry.register('abc123');

      expect(registry.codes).toEqual(['abc123']);
    });
  });

  describe('#deregister', function(){
    it('removes registered code from registry', function(){
      var registry = new Coupons.Registry();
      registry.register('abc123');
      registry.deregister('abc123');

      expect(registry.codes).toEqual([]);
    });
  });

  describe('#isRegistered', function(){
    var registry = new Coupons.Registry();

    it('is false if code has not been registered', function(){
      expect(registry.isRegistered('abc123')).toBeFalsy();
    });

    it('is true if code has been registered', function(){
      registry.register('abc123');

      expect(registry.isRegistered('abc123')).toBeTruthy();
    });
  });
});
