describe('PromisePrice', function(){
  var price = 100;
  var element, registry, coupon, promisePrice;

  beforeEach(function(){
    element = {}
    registry = {}
    coupon = {}

    element.data = function(){ return price };
    element.text = function(){}

    promisePrice = new Coupons.PromisePrice(element, registry);
  });

  describe('#applyCoupon', function(){
    describe('when coupon has been registered', function(){
      beforeEach(function(){
        registry.isRegistered = function() { return true };
      });

      it('refreshes the view', function(){
        spyOn(element, 'text');

        promisePrice.applyCoupon(coupon);

        expect(element.text).not.toHaveBeenCalled();
      });
    });

    describe('when coupon has not been registered', function(){
      beforeEach(function(){
        registry.isRegistered = function() { return false };
        registry.register = function(){};
        spyOn(registry, 'register');
      });

      it('registers the coupon', function(){
        coupon.code = 123;

        promisePrice.applyCoupon(coupon);

        expect(registry.register).toHaveBeenCalledWith(coupon.code)
      });

      it('refeshes the view with rounded price', function(){
        coupon.amount = 10.5;

        spyOn(element, 'text');

        promisePrice.applyCoupon(coupon);

        expect(element.text).toHaveBeenCalledWith('$ 90')
      });
    });
  });

  describe('#resetCoupon', function(){
    beforeEach(function(){
      registry.isRegistered = function() { return false };
      registry.register = function(){};
      registry.deregister = function(){};
    });

    it('refreshes the view with the original price', function(){
      coupon.amount = 10.5;

      spyOn(element, 'text');

      promisePrice.applyCoupon(coupon);

      expect(element.text).toHaveBeenCalledWith('$ 90')

      promisePrice.resetCoupon()

      expect(element.text).toHaveBeenCalledWith('$ 100')
    });

    it('deregisters the coupon', function(){
      spyOn(registry, 'deregister');

      promisePrice.applyCoupon(coupon);
      promisePrice.resetCoupon();

      expect(registry.deregister).toHaveBeenCalledWith(coupon.code);
    });
  });
});
