# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Sold Vehicles Admin' do
  include_context 'admin steps'

  let(:aa_no)          { 100_000_001 }
  let(:other_aa_no)    { 100_000_002 }
  let(:agreement_no)      { 5_000_001 }
  let(:other_agreement_no) { 5_000_002 }
  let(:location)       { 'AAA' }
  let(:other_location) { 'BBB' }

  let!(:sold_vehicle) do
    FactoryGirl.create :sold_vehicle,
                       pawn_access_no: aa_no,
                       agreement_no:         agreement_no,
                       location_initials: location
  end
  let!(:other_vehicle) do
    FactoryGirl.create :sold_vehicle,
                       pawn_access_no: other_aa_no,
                       agreement_no:         other_agreement_no,
                       location_initials: other_location
  end

  before do
    login_to_admin_space
    click_on 'Sold Vehicles'
  end

  it 'displays sold vehicles' do
    expect(page).to have_content(sold_vehicle.vnum)
  end

  context 'VNUM filter' do
    it 'should display sold vehicle given an existing VNUM' do
      fill_in 'VNUM', with: sold_vehicle.vnum
      click_on I18n.t('forms.filter')
      expect(page).to have_field('VNUM', with: sold_vehicle.vnum)
      expect(page).to have_content(sold_vehicle.vnum)
    end

    it 'should not display sold vehicles given a non existing VNUM' do
      sold_vehicle_vnum = '12345'
      fill_in 'VNUM', with: sold_vehicle_vnum
      click_on I18n.t('forms.filter')
      expect(page).to have_field('VNUM', with: sold_vehicle_vnum)
      expect(page).to_not have_content(sold_vehicle_vnum)
    end
  end

  context 'Agreement# filter' do
    it 'should display sold vehicles given an existing Agreement#' do
      fill_in 'Agreement#', with: sold_vehicle.agreement_no
      click_on I18n.t('forms.filter')
      expect(page).to have_field('Agreement#', with: sold_vehicle.agreement_no.to_s)
      expect(page).to have_content(sold_vehicle.agreement_no)
    end

    it 'should not display sold vehicles given a non-existing Agreement# ' do
      sold_vehicle_agreement_no = '5555'
      fill_in 'Agreement#:', with: sold_vehicle_agreement_no
      click_on I18n.t('forms.filter')
      expect(page).to have_field('Agreement#', with: sold_vehicle_agreement_no)
      expect(page).to_not have_content(sold_vehicle_agreement_no)
    end
  end
end
