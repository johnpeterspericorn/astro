# encoding: utf-8
require 'requests_helper'
require_relative './steps/purchase_cancellation_steps'
require_relative './steps/return_invoice_steps'
require_relative './steps/promise_purchase_steps'

describe 'Re-Purchase workflow' do
  include_context 'purchase cancellation steps'
  include_context 'return invoice steps'
  include_context 'promise purchase steps'

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000) }
  let!(:usa_currency_code) { '$' }

  before do
    stub_buyer_authorization
    stub_ods_for_refund
  end

  context 'super user logged in' do
    before do
      log_in_super_user
      purchase_promise
      cancel_purchase
    end

    it 'can be re-purchased with the right amount' do
      view_vehicles_by_pawn_access_number
      select_acura_for_250_miles_and_14_additional_days
      click_next

      expect(page).to have_content("#{usa_currency_code} 240")
    end
  end
end
