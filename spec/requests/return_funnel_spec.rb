# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'
require_relative './steps/pawn_access_number_swipe_steps'
require_relative './steps/navigation_steps'

describe 'Return workflow' do
  include_context 'navigation steps'
  include_context 'return invoice steps'
  include_context 'pawn access number swipe steps'

  let!(:acura)  { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, seller_paid: true) }
  let!(:benz)   { FactoryGirl.create(:sold_vehicle, make: 'BENZ', odometer_reading: 10_000, seller_paid: true) }
  let!(:honda)  { FactoryGirl.create(:sold_vehicle, make: 'HONDA', odometer_reading: 10_000, agreement_no: SecureRandom.hex, seller_paid: true) }
  let!(:mitsu)  { FactoryGirl.create(:sold_vehicle, make: 'MITSUBISHI', odometer_reading: 10_000, seller_paid: true) }
  let!(:toyota) { FactoryGirl.create(:sold_vehicle, make: 'TOYOTA', odometer_reading: 10_000, seller_paid: true) }
  let!(:volvo)  { FactoryGirl.create(:sold_vehicle, make: 'VOLVO', odometer_reading: 10_000, seller_paid: true) }

  before do
    Settings.salvage_internet_notes_enabled = true
    stub_buyer_authorization
  end

  vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

  context 'navigation', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'as a Buyer user' do
      before do
        log_in_buyer_user
      end

      it 'should have a back button on the enter user info page' do
        visit_returns_user_info_page
        click_on I18n.t('forms.back')
        expect(current_path).to eq(root_path)
      end
    end

    context 'as kiosk user' do
      before do
        log_in_kiosk_user
      end

      it 'should have a back button on the enter user info page' do
        visit_returns_user_info_page
        click_on I18n.t('forms.back')
        expect(current_path).to eq(root_path)
      end

      it 'should have a back button on the select vehicle page' do
        view_returns_page
        click_on I18n.t('forms.back')
        expect(current_path).to eq(return_funnel_filter_path(:pawn_access_number_filter))
      end

      it 'should have a cancel button on the verify return page' do
        purchase_promise_for_vehicle(acura)
        initiate_return
        click_on I18n.t('forms.cancel')
        page_should_be_root_page
      end

      it 'should have a back button on the verify return page' do
        purchase_promise_for_vehicle(acura)
        initiate_return
        click_on I18n.t('forms.back')

        expect(current_path).to eq(return_funnel_filter_path(:pawn_access_number_filter))
      end

      describe 'Kiosk Integration' do
        before do
          visit_returns_user_info_page
        end

        it 'should prompt the user for input when pawn access is selected' do
          page_should_be_pawn_access_number_input_page
        end

        it 'should parse track data correctly', js: true do
          purchase_promise_for_vehicle(honda)
          log_in_kiosk_user # this is weird.  it's like the capybara thread doesn't know about the login in the main spec!
          visit_returns_user_info_page

          submit_pawn_access_number_form_for_kiosk_user

          expect(page).to have_content honda.vnum
        end

        it 'should handle bad track data with an error message', js: true do
          enter_bad_pawn_swipe
          expect(page).to have_content 'Please swipe your Pawn Access card again.'
        end
      end
    end

    context 'as a Buyer User' do
      before do
        log_in_buyer_user
      end

      it 'should bypass the pawn access number filter' do
        purchase_promise_for_vehicle(acura)
        visit_returns_user_info_page
        expect(page).to have_content PromiseOffer.by_vnum(acura.vnum).first.decorate.full_description
        expect(page).not_to have_content I18n.t('return_invoice.not_available.no_eligible_vehicles')
        expect(page).to have_content acura.vnum
      end

      it 'should redirect to the returns page on one click' do
        # this is a tiny regression after I introduced a bug.  --@daveworth
        purchase_promise_for_vehicle(acura)
        visit_returns_user_info_page
        click_on 'Return'
        expect(page).to have_content I18n.t('return_invoice.return_reasons.mechanical_issues')
      end
    end

    context 'as a super user' do
      let(:pawn_access_number_filter_button) { I18n.t('forms.pawn_access.input_label') }
      let(:agreement_number_filter_button)        { I18n.t('forms.agreement_number.input_label')  }
      let(:vnum_number_filter_button)           { I18n.t('forms.vnum_number.input_label')     }
      let(:mileage) { acura.odometer_reading + max_mileage - 1 }

      before do
        log_in_super_user
        purchase_promise_for_vehicle(acura)
        purchase_promise_for_vehicle(honda)
        purchase_promise_for_vehicle(mitsu)

        PromiseOffer.by_vnum(mitsu.vnum).first.pawn_information.update_attributes(pawn_access_no: '1')
        PromiseOffer.by_vnum(honda.vnum).first.pawn_information.update_attributes(pawn_access_no: '1')
      end

      it 'shows filter options' do
        visit_returns_user_info_page

        expect(page).to have_content(pawn_access_number_filter_button)
        expect(page).to have_content(agreement_number_filter_button)
        expect(page).to have_content(vnum_number_filter_button)
      end

      it 'handles empty AA# filters' do
        visit_returns_user_info_page
        click_on pawn_access_number_filter_button
        click_on I18n.t('forms.enter')
        expect(page).to have_content 'Pawn Access #'
        expect(page).to have_content I18n.t('return_invoice.missing_filter_error', filter_type: 'Pawn access number')
      end

      it 'handles 0 AA# filters' do
        visit_returns_user_info_page
        click_on pawn_access_number_filter_button
        select_pawn_access_number_for_return(-1)
        expect(page).to have_content 'Pawn Access #'
        expect(page).to have_content I18n.t('return_invoice.pawn_access_number_filter_error.invalid_min_range')
      end

      it 'handles empty VNUM # filters' do
        visit_returns_user_info_page
        click_on vnum_number_filter_button
        click_on I18n.t('forms.enter')
        expect(page).to have_content I18n.t('return_funnel.filters.vnum_number.input_label')
        expect(page).to have_content I18n.t('return_invoice.missing_filter_error', filter_type: 'Vin number')
      end

      it 'handles empty Agreement # filters' do
        visit_returns_user_info_page
        click_on agreement_number_filter_button
        click_on I18n.t('forms.enter')
        expect(page).to have_content I18n.t('return_funnel.filters.agreement_number.input_label')
        expect(page).to have_content I18n.t('return_invoice.missing_filter_error', filter_type: 'Agreement number')
      end

      it 'displays vehicles by agreement #' do
        visit_returns_user_info_page
        click_on agreement_number_filter_button
        select_agreement_number_for_return(mitsu.agreement_no)

        expect(page).to have_content(mitsu.vnum)
        expect(page).not_to have_content(honda.vnum)
      end

      it 'displays vehicles by VNUM #' do
        visit_returns_user_info_page
        click_on vnum_number_filter_button
        select_vnum_number_for_return(honda.vnum)

        expect(page).to have_content(honda.vnum)
        expect(page).not_to have_content(mitsu.vnum)
      end

      context 'regressions' do
        it 'clears the AA# from the session in the filter selection step' do
          felicias_pawn_access_number = SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
          felicias_name = 'Felicia Rawles'
          empty_hello = 'Hello ,'
          allow(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:new).with(hash_including(pawn_access_number: felicias_pawn_access_number)).and_return(double(get_agreement_name: felicias_name, get_agreement_numbers: []))
          PromisePurchase.destroy_all # this triggers showing the name based on the AA#

          visit_returns_user_info_page
          click_on pawn_access_number_filter_button
          select_pawn_access_number_for_return(felicias_pawn_access_number)
          expect(page).to have_content felicias_name

          visit_returns_user_info_page
          click_on agreement_number_filter_button
          select_agreement_number_for_return(mitsu.agreement_no)

          expect(page).not_to have_content felicias_name
          expect(page).to have_content empty_hello
        end
      end
    end
  end

  context 'vehicle eligibility', vcr: vcr_options do
    let(:eligible)               { acura }
    let(:returned)               { mitsu }
    let(:recently_ineligible)    { honda }
    let(:old_ineligible)         { benz }
    let(:purchased_too_recently) { toyota }
    let(:not_yet_paid_for)       { volvo }

    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      purchased_too_recently.update_attributes(pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
      not_yet_paid_for.update_attributes(pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context "super users' super powers" do
      it 'vehicles purchased recently can be returned by super users' do
        log_in_super_user
        purchase_promise_for_vehicle(not_yet_paid_for)
        purchase_promise_for_vehicle(purchased_too_recently)
        visit_returns_user_info_page
        click_on I18n.t('forms.pawn_access.input_label')
        select_access_number_for_return(acura.pawn_access_no)

        expect_to_see_number_of_vehicles(2)
        expect(page).to have_content purchased_too_recently.vnum
        expect(page).to have_content not_yet_paid_for.vnum
        expect(page).to_not have_css('[rel~=return_purchase]', text: 'Ineligible')
      end
    end

    context 'display' do
      let(:no_vehicles_error) { I18n.t('return_invoice.not_available.no_eligible_vehicles') }

      before do
        log_in_kiosk_user
      end

      it 'displays vehicles purchased today as ineligible' do
        acura.update_attributes!(purchased_at: Time.current)
        purchase_promise_for_vehicle(acura)
        view_returns_page

        within page.find('.vehicle_description', text: acura.vnum) do
          expect(page).to have_content I18n.t('return_invoice.ineligible.before_min_return_time')
        end
      end

      it 'displays purchase expired by admin as ineligible' do
        purchase_promise_for_vehicle(acura)
        purchase =  acura.promise_offers.first.promise_purchase
        purchase.update_attributes(expire_promise: true)
        view_returns_page

        within page.find('.vehicle_description', text: acura.vnum) do
          expect(page).to have_content I18n.t('return_invoice.ineligible.after_max_return_time')
        end
      end

      it 'displays market protect return eligibility for market protect purchases' do
        purchase_promise_for_vehicle(acura)
        purchase =  acura.promise_offers.first.promise_purchase
        purchase.update_attributes(kind: PromisePurchase::MARKET_PROTECT)
        view_returns_page

        within page.find('.vehicle_description', text: acura.vnum) do
          expect(page).to have_content I18n.t('return_invoice.ineligible.market_protect')
        end
      end

      it 'does not display "old" ineligible vehicles' do
        purchase_promise_for_vehicle(acura)
        make_promise_old(acura)
        view_returns_page

        expect(page).to_not have_content(acura.vnum)
      end

      it 'tells users by name that they do not have any returnable vehicles' do
        agreement_name = 'Somebody'
        stub_buyer_authorization(name: agreement_name)
        purchase_promise_for_vehicle(returned)
        initiate_return
        enter_return_details(returned)
        verify_return
        update_buyer_information
        view_returns_page

        expect(page).to have_content(agreement_name)
        expect(page).to have_content(no_vehicles_error)
      end

      it 'shows return button for eligible vehicles' do
        purchase_promise_for_vehicle(eligible)
        view_returns_page
        expect(page).to have_css('[rel~=return_purchase]')
      end

      context 'returned vehicles' do
        before do
          return_vehicle(eligible)
          view_returns_page
        end

        it 'shows return date for returned vehicles' do
          expect(page).to have_content("Returned: #{Date.current}")
        end

        it 'displays a message for returned vehicles' do
          expect(page).to have_content I18n.t('return_invoice.return_initiated')
        end

        it 'displays a link to the return certificate' do
          invoice_link_copy = I18n.t('return_invoice.link_copy')
          expect(page).to have_content invoice_link_copy
          click_on invoice_link_copy
          return_invoice = PromiseOffer.by_vnum(eligible.vnum).first.promise_purchase.return_invoice
          expect(current_path).to eql obfuscated_return_certificate_path(slug: return_invoice.slug)
        end
      end

      it 'shows ineligible button for recently ineligible vehicles' do
        purchase_promise_for_vehicle(eligible)
        make_promise_ineligible_after_max_time(eligible)
        view_returns_page
        expect(page).to have_css('[rel~=ineligible]')
      end
    end

    describe 'ods attribute update' do
      before do
        log_in_kiosk_user
      end

      it 'updates attributes to make purchase eligible for return' do
        purchase_promise_for_vehicle(eligible)
        view_returns_page

        expect(page).to have_css('[rel~=return_purchase]')
      end
    end
  end

  context 'with valid odometer measurement', vcr: vcr_options do
    let(:mileage) { acura.odometer_reading + max_mileage - 1 }
    let(:email)   { 'nerdbot@bignerdranch.com' }
    let(:unable_to_sell_reason) { I18n.t('return_invoice.return_reasons.unable_to_sell') }
    let(:kiosk_location) { 'DRIV' }
    let(:st_louis) { 'St. Louis' }
    let(:chicago) { 'Chicago' }
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      PawnLocation.create!(name: st_louis, initials: 'SLAA', accepts_returns: false)
      PawnLocation.create!(name: chicago, initials: 'AAA', purchases_enabled: false)
      PawnLocation.create!(name: 'Drive', initials: kiosk_location)
      log_in_kiosk_user kiosk_location
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'with valid return location initials' do
      it "should pre-select the vehicle's purchase location" do
        purchase_promise_for_vehicle(acura)
        initiate_return
        expect(find('select#return_invoice_location_initials option[selected]').value).to eql acura.location_initials
      end

      it 'should prepopluate the provided email' do
        purchase_promise_for_vehicle(acura)
        # setup email without cleaning up the offer in #initiate_return
        PromiseOffer.by_vnum(acura.vnum).first.update_attributes(emails: SasDataHelper::DEFAULT_PURCHASE_EMAIL)
        initiate_return(acura)
        enter_return_details(acura)
        verify_return

        expect(find('input#buyer_and_agreement_information_form_emails').value).to eql SasDataHelper::DEFAULT_PURCHASE_EMAIL
      end

      it 'should only display those locations with returns enabled' do
        PawnLocation.create!(name: 'TORONTO', initials: 'TO', purchases_enabled: false, country: 'Canada')
        purchase_promise_for_vehicle(acura)
        initiate_return

        expect(find('select#return_invoice_location_initials')).to have_content 'Drive'
        expect(find('select#return_invoice_location_initials')).to_not have_content 'TORONTO'
        expect(find('select#return_invoice_location_initials')).to have_content chicago
        expect(find('select#return_invoice_location_initials')).not_to have_content st_louis
      end

      it 'allows a user to return a vehicle' do
        expect do
          return_vehicle(acura)
        end.to change(ReturnInvoice, :count).by(1)

        expect(page).to have_content 'Your return has been initiated.'
      end

      it 'allows a user to specify multiple return reasons' do
        additional_information = 'A good reason from the user'
        purchase_promise_for_vehicle(acura)
        initiate_return
        enter_odometer_reading(mileage)
        enter_location(pawn_location.name)
        select_title_status
        select_return_reason('mechanical_issues')
        select_return_reason('unable_to_sell')
        fill_in 'return_invoice_additional_information', with: additional_information
        verify_return
        update_buyer_information

        return_invoice = ReturnInvoice.last
        expect(return_invoice.mechanical_issues).to be_truthy
        expect(return_invoice.unable_to_sell).to be_truthy
        expect(return_invoice.changed_mind).to be_falsey
        expect(return_invoice.additional_information).to eql additional_information
      end

      it 'displays a success message' do
        return_vehicle(acura)

        expect(page).to have_content I18n.t('return_invoice.finalize_return.success_message')
        click_done
        expect(page).not_to have_content I18n.t('return_invoice.finalize_return.success_message')
      end

      it 'mails a confirmation after return is finalized' do
        mailer_stub = double(:mailer_stub)
        expect(mailer_stub).to receive(:deliver_later)
        expect(ReturnInvoiceMailer).to receive(:return_confirmation).with(any_args).and_return(mailer_stub)

        return_vehicle_with_email(acura, email)
      end

      context 'when no reasons are given' do
        it 'fails and redirects back to the form' do
          expect do
            attempt_return_without_reason(acura)
          end.to_not change(ReturnInvoice, :count)

          expect(page).to have_content I18n.t('return_invoice.return_reasons.none_specified_error')
        end
      end
    end
  end

  context 'vehicle adjustment' do
    describe 'api error' do
      before do
        VCR.use_cassette('vehicle_adjustments_api/failed_request') do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
        end
      end

      it 'displays how vehicle was paid page ' do
        expect(page).to have_content I18n.t('vehicles.how_paid.display_text')
      end
    end

    describe 'vehicle not found' do
      before do
        VCR.use_cassette('vehicle_adjustments_api/missing_vehicle_request') do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
        end
      end

      it 'displays how vehicle was paid page' do
        expect(page).to have_content I18n.t('vehicles.how_paid.how_paid_text')
      end
    end

    describe 'api success' do
      before do
        VCR.use_cassette('vehicle_adjustments_api/successful_request') do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
        end
      end

      it 'displays reasons for return page' do
        expect(page).to have_content 'Reasons for Return'
        expect(page).to_not have_content 'unable to process'
      end
    end
  end

  context 'title status' do
    let(:new_email) { 'new_email@some_buyer.com' }
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      log_in_kiosk_user
      purchase_promise_for_vehicle(acura)
      initiate_return
      enter_location(pawn_location.name)
      select_return_reason('unable_to_sell')
      enter_odometer_reading(acura.odometer_reading + 1)
      verify_return
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'should present an error without a title status' do
      expect(page).to have_content I18n.t('return_invoice.invalid_title_status')
    end
  end

  context 'with invalid odometer measurement', vcr: vcr_options do
    let(:over_max) { acura.odometer_reading + max_mileage + 1 }
    let(:under_min) { acura.odometer_reading - 1 }

    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      log_in_kiosk_user
      purchase_promise_for_vehicle(acura)
      initiate_return
      enter_location(pawn_location.name)
      select_return_reason('unable_to_sell')
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'fails when odometer is above range' do
      enter_odometer_reading(over_max)

      expect do
        verify_return
      end.to_not change(ReturnInvoice, :count)

      expect(page).to have_content I18n.t('return_invoice.odometer.too_high_error')
    end

    it 'should allow re-submitting after an error' do
      enter_odometer_reading(under_min)
      select_title_status
      verify_return

      enter_odometer_reading(acura.odometer_reading)
      select_title_status
      expect do
        verify_return
        update_buyer_information
      end.to change(ReturnInvoice, :count).by(1)
    end

    it 'should not blow up when clicking back after multiple failures' do
      enter_odometer_reading(under_min)
      verify_return
      verify_return
      click_on I18n.t('forms.back')
      expect(current_path).to eq(return_funnel_filter_path(:pawn_access_number_filter))
    end

    context 'when the odometer is below range' do
      before do
        enter_odometer_reading(under_min)
      end

      it 'fails to return the vehicle' do
        expect do
          verify_return
        end.to_not change(ReturnInvoice, :count)
      end

      it 'displays the error to the user' do
        verify_return
        expect(page).to have_content I18n.t('return_invoice.odometer.too_low_error')
      end
    end

    it 'preserves reason for return' do
      enter_odometer_reading(0)
      select_return_reason('unable_to_sell')
      verify_return
      expect(page).to have_checked_field('return_invoice_unable_to_sell')
    end
  end

  describe 'buyer information confirmation' do
    let(:email) { 'care@bear.com' }
    let(:vehicle) { acura }

    before do
      log_in_super_user
      purchase_promise_for_vehicle(vehicle)
      purchase_promise_for_vehicle(benz)
    end

    def view_returns_by_agreement_number(vehicle)
      visit_returns_user_info_page
      click_on I18n.t('forms.agreement_number.input_label')
      fill_in 'agreement_number', with: vehicle.agreement_no
      click_on I18n.t('forms.enter')
    end

    def initiate_return_by_agreement_number(vehicle)
      view_returns_by_agreement_number(vehicle)
      first(:link, 'Return').click
    end

    def initiate_and_verify_return(vehicle)
      initiate_return_by_agreement_number(vehicle)
      enter_return_details(vehicle)
      verify_return
    end

    context 'when the vehicle was paid by floor plan', vcr: vcr_options do
      let(:branch_name) { 'Branch1' }
      let(:other_branch_name) { 'Branch2' }
      let(:other_street) { '123 A Road' }
      let(:company) { FactoryGirl.create(:floor_plan_company) }
      let!(:branch) { FactoryGirl.create(:floor_plan_branch, name: branch_name, company_code: company.company_code) }
      let!(:other_branch) { FactoryGirl.create(:floor_plan_branch, name: other_branch_name, company_code: company.company_code, address_street: other_street) }
      let(:agreement_information) { AgreementInformation.new(agreement_no: '5000000') }
      let(:offer) { vehicle.promise_offers.first }

      before do
        Settings.salvage_internet_notes_enabled = false
        allow_any_instance_of(PromiseOffer).to receive(:floor_plan_company) { company }
        allow(AgreementInformation).to receive(:build_with_defaults) { agreement_information }
      end

      it 'notifies pawn location with selected branch address' do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.insert_cassette('salvage_api/add_note')

          initiate_and_verify_return(vehicle)
          select other_branch_name, from: 'floor_plan_branch_id'
          expect do
            click_on 'Finish'
          end.to change { enqueued_deliveries.count }.by(2)

          VCR.eject_cassette
        end
      end

      it 'allows the user to set an address using the branch dropdown' do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.insert_cassette('salvage_api/add_note')

          initiate_and_verify_return(vehicle)

          select branch_name, from: 'floor_plan_branch_id'
          click_on 'Finish'

          expect(offer.promise_purchase.return_invoice).to be_present

          VCR.eject_cassette
        end
      end

      it 'lock floor plan branch in dropdown' do
        FactoryGirl.create(:agreement_floor_plan_branch_lock, company_code: company.company_code, site_no: branch.site_no, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER)
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.insert_cassette('salvage_api/add_note')

          initiate_and_verify_return(vehicle)
          expect(find_field('floor_plan_branch_id').find('option[selected]').text).to eq branch.name

          VCR.eject_cassette
        end
      end

      it 'allows the user to set multiple emails' do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.insert_cassette('salvage_api/add_note')

          email1 = 'starbuck@galactica.gov'
          email2 = 'jim@galaxynine.plt'
          email = [email1, email2].join(',')

          initiate_and_verify_return(vehicle)
          select branch_name, from: 'floor_plan_branch_id'
          fill_in 'buyer_and_agreement_information_form_emails', with: email

          expect do
            click_on 'Finish'
          end.to change { enqueued_deliveries.count }.by(2)

          VCR.eject_cassette
        end
      end

      it "updates to display the currently-selected branch's address", js: true do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          initiate_and_verify_return(vehicle)
          select branch_name, from: 'floor_plan_branch_id'
          select other_branch_name, from: 'floor_plan_branch_id'

          expect(page.find(".floor_plan_address[data-branch-id='#{other_branch.id}']")).to be_visible
          expect(page.find(".floor_plan_address[data-branch-id='#{branch.id}']")).to_not be_visible
        end
      end

      it 'auto-selects the branch previously selected by agreement' do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          initiate_and_verify_return(vehicle)
          select other_branch_name, from: 'floor_plan_branch_id'
          click_on 'Finish'
          initiate_and_verify_return(benz)

          expect(page).to have_select('floor_plan_branch_id', selected: other_branch_name)
        end
      end

      let(:notes) { 'Foo Bar Baz Quux' }

      context 'when the rescue option "other" is provided' do
        around do |example|
          VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
            example.run
          end
        end

        before do
          initiate_and_verify_return(vehicle)
          page.select branch_name, from: 'floor_plan_branch_id'
          page.select 'Other', from: 'floor_plan_branch_id'
          fill_in 'other_branch_notes', with: notes
        end

        it 'uses the "notes" field in the agreement information' do
          expect do
            click_on 'Finish'
          end.to change(ReturnInvoice, :count).by(1)
        end

        it 'sends an alert to accounting/support' do
          expect(ReturnInvoiceMailer).to receive(:other_branch_selected) { double(deliver_later: nil) }
          click_on 'Finish'
        end
      end
    end

    context 'left lot in ods', vcr: vcr_options do
      let(:branch_name) { 'Branch1' }
      let(:other_branch_name) { 'Branch2' }
      let(:other_street) { '123 A Road' }
      let(:company) { FactoryGirl.create(:floor_plan_company) }
      let!(:branch) { FactoryGirl.create(:floor_plan_branch, name: branch_name, company_code: company.company_code) }
      let!(:other_branch) { FactoryGirl.create(:floor_plan_branch, name: other_branch_name, company_code: company.company_code, address_street: other_street) }
      let(:agreement_information) { AgreementInformation.new(agreement_no: '5000000') }
      let(:offer) { vehicle.promise_offers.first }

      before do
        Settings.salvage_internet_notes_enabled = false
        allow_any_instance_of(PromiseOffer).to receive(:floor_plan_company) { company }
        allow(AgreementInformation).to receive(:build_with_defaults) { agreement_information }
      end

      it 'expect left_lot in return invoice to be updated as false' do
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.use_cassette('salvage_api/add_note') do
            initiate_and_verify_return(vehicle)
            select other_branch_name, from: 'floor_plan_branch_id'
            click_on 'Finish'
            return_invoice = ReturnInvoice.last
            expect(return_invoice.left_lot).to be_falsey
          end
        end
      end

      it 'expect left_lot in return invoice to be updated as true' do
        allow_any_instance_of(ReturnInvoice).to receive(:left_lot_in_ods?).and_return(true)
        VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
          VCR.use_cassette('salvage_api/add_note') do
            initiate_and_verify_return(vehicle)
            select other_branch_name, from: 'floor_plan_branch_id'
            click_on 'Finish'
            return_invoice = ReturnInvoice.last
            expect(return_invoice.left_lot).to be_truthy
          end
        end
      end
    end

    it 'filters by VNUM suffix' do
      view_returns_by_agreement_number(vehicle)

      fill_in 'vnum_suffix', with: vehicle.vnum[-4..-1]
      click_on 'Submit'
      expect(page).to have_content vehicle.vnum
      fill_in 'vnum_suffix', with: 'BADSUFFIX'
      click_on 'Submit'
      expect(page).to_not have_content vehicle.vnum
    end

    it 'requires an email', vcr: vcr_options do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      initiate_and_verify_return(vehicle)
      update_buyer_information(email: '')

      expect(page).to have_content('Please provide email')

      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'supports multiple emails', vcr: vcr_options do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      initiate_and_verify_return(vehicle)
      update_buyer_information(email: 'jay@nmh.com, kylie@nmh.com')
      expect(enqueued_deliveries.count).to eq(2)
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'requires a valid address', vcr: { cassette_name: 'easypost/unverified_address' } do
      VCR.use_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true) do
        VCR.insert_cassette('salvage_api/add_note')

        Settings.address_verification_enabled = true
        invalid_address_error = I18n.t('return_invoice.address.invalid')

        initiate_and_verify_return(vehicle)
        update_buyer_information(address_street: 'asdf')

        expect(page).to have_content(invalid_address_error)

        VCR.eject_cassette
      end
    end

    it "doesn't blow up with verifiable but invalid zipcode", vcr: { cassette_name: 'easypost/invalid_zipcode' } do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      six_digit_zip = '303017'

      initiate_and_verify_return(vehicle)
      update_buyer_information(address_zipcode: six_digit_zip)

      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'is updated', vcr: vcr_options do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      initiate_and_verify_return(vehicle)
      update_buyer_information(email: email)

      expect(BuyerInformation.first.emails).to eq(email)

      VCR.eject_cassette
      VCR.eject_cassette
    end
  end

  context 'vehicle return status', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      log_in_super_user
      return_vehicle(acura)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'should be displayed when the feature is enabled' do
      view_returns_page_with_return_status
      expect_return_statuses_to_be_displayed
    end

    it 'should have status set based on return invoice fields' do
      update_return_status(acura)
      view_returns_page_with_return_status
      returned_vehicle_should_have_return_statuses_set
    end

    it 'should not allow Edit Return if returned vehicle received' do
      update_return_status(acura)
      view_returns_page_with_return_status
      expect(page).to_not have_content(I18n.t('forms.returns.edit'))
    end

    it 'should not allow Cancel Return when returned vehicle received' do
      update_return_status(acura)
      view_returns_page_with_return_status
      expect(page).to_not have_content(I18n.t('return_invoice.cancellation.link_copy'))
    end
  end

  context 'audit alert message', vcr: vcr_options do
    let(:alert_message) { I18n.t('return_invoice.audit_alert', domain: "promise.#{Astro::DOMAIN}").to_s }

    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      log_in_super_user
      return_vehicle(acura)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'should be displayed when there is an audit in progress' do
      return_invoice = acura.promise_offers.last.promise_purchase.return_invoice
      return_invoice.create_audit!(resolved_by: Date.current, emails: 'xyz@abc.com')
      view_returns_page

      expect(page).to have_content(alert_message)
    end
  end
end
