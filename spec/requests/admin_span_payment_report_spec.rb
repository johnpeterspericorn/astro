# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Span Payment Report' do
  include_context 'admin steps'

  let(:purchase_1) { FactoryGirl.create(:promise_purchase, id: 1) }
  let(:purchase_2) { FactoryGirl.create(:promise_purchase, id: 2) }
  let(:purchase_3) { FactoryGirl.create(:promise_purchase, id: 3) }

  let(:payment_1) { FactoryGirl.create(:payment) }
  let(:payment_2) { FactoryGirl.create(:payment, authorization_code: 'ASD123') }
  let(:payment_3) { FactoryGirl.create(:payment, authorization_code: 'ADS3123') }

  let!(:payment_reponse_1) { FactoryGirl.create(:payment_response, payment: payment_1, raw_response: 'asds', promise_purchase: purchase_1) }
  let!(:payment_reponse_2) { FactoryGirl.create(:payment_response, payment: payment_2, raw_response: 'asfdf', promise_purchase: purchase_2) }
  let!(:payment_reponse_3) { FactoryGirl.create(:payment_response, payment: payment_3, raw_response: 'fghfg', promise_purchase: purchase_3) }

  before do
    login_as_finance_admin_viewer
    click_on I18n.t('admin.navigation.span_payment_report')
  end

  it 'shows date from and date to' do
    expect(page).to have_content I18n.t('span_payment_report.admin.email.email_address_label')
    expect(page).to have_content I18n.t('span_payment_report.admin.date_from')
    expect(page).to have_content I18n.t('span_payment_report.admin.date_to')
  end

  it 'should send an email provided a good email address is given' do
    fill_in 'emails', with: 'sdsad@dasd.com'
    expect { click_on I18n.t('span_payment_report.admin.email.send_label') }.to change { enqueued_deliveries.count }.by(1)
  end

  it 'should send an email provided a good email address is given and date range specified' do
    fill_in 'emails', with: 'sdsad@dasd.com'
    fill_in 'Date From', with: 180.days.ago
    fill_in 'Date To', with: 1.day.ago
    expect { click_on I18n.t('span_payment_report.admin.email.send_label') }.to change { enqueued_deliveries.count }.by(1)
  end
end
