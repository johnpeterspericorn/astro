# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Promise Purchase Sales Dashboard' do
  include_context 'admin steps'

  %w(a b c d).each do |location_initial|
    let(:"location_#{location_initial}") do
      FactoryGirl.create(:pawn_location, initials: location_initial * 4, name: location_initial * 5)
    end

    let(:"pawn_information_#{location_initial}") do
      pawn_location = send("location_#{location_initial}")
      FactoryGirl.create(:pawn_information, pawn_location: pawn_location)
    end
  end

  def create_promise_purchases
    [promise_offer, location_b_offer, seller_paid_offer, auto_purchase_offer, promise_offer_a].each do |offer|
      FactoryGirl.create(:promise_purchase, promise_offer: offer)
    end
  end

  let(:promise_offer)         { FactoryGirl.create(:recently_created_promise_offer, pawn_information: pawn_information_a, promise_price: 200) }
  let(:location_b_offer)        { FactoryGirl.create(:promise_offer, pawn_information: pawn_information_b, purchased_at: 1.day.ago) }
  let(:auto_purchase_offer)     { FactoryGirl.create(:recently_created_promise_offer, automatic_purchase: true, pawn_information: pawn_information_c, promise_price: 100) }
  let(:seller_paid_offer)       { FactoryGirl.create(:recently_created_promise_offer, seller_paid: true, pawn_information: pawn_information_d, promise_price: 300) }
  let(:pawn_information_e)   { FactoryGirl.create(:pawn_information, location_initials: pawn_information_a.location_initials) }
  let(:promise_offer_a)       { FactoryGirl.create(:recently_created_promise_offer, pawn_information: pawn_information_e, promise_price: 500) }
  let(:avg_ds_price)            { (promise_offer_a.promise_price + promise_offer.promise_price) / 2 }
  let(:total_avg_ds_price)      { (promise_offer.promise_price + promise_offer_a.promise_price + seller_paid_offer.promise_price + auto_purchase_offer.promise_price) / 4 }

  before do
    create_promise_purchases
    login_to_admin_space
    click_on I18n.t('admin.navigation.sales_dashboard')
  end

  context 'Location based sales data' do
    it 'shows sales data of all locations by default' do
      expect(data_table).to have_content location_a.name
    end

    it 'displays numeric average of agreementshield price corresponding to each location' do
      expect(data_table).to have_content avg_ds_price
    end
  end

  context 'Total Sales related data' do
    it 'displays average total sales data of all locations for current day by default' do
      expect(data_table).to have_content total_avg_ds_price
    end
  end

  context 'Seller Paid related data' do
    it 'displays seller paid ratio' do
      expect(data_table).to have_content '25.000%'
    end

    it 'displays agreements who received seller paid promises' do
      expect(data_table).to have_content '1'
    end
  end

  context 'Search Filter' do
    it 'defaults to today\'s date till current time as date range' do
      Timecop.freeze do
        visit admin_sales_dashboard_index_path
        date_from_field_value = DateTime.parse(find_field('filters_date_from').value)
        date_to_field_value = DateTime.parse(find_field('filters_date_to').value)
        expect(date_from_field_value).to eq(DateTime.current.beginning_of_day)
        expect(date_to_field_value.to_s).to eq(DateTime.current.to_s)
      end
    end

    it 'filters by a date range' do
      yesterday = Date.current - 1.day
      fill_in 'Date From', with: yesterday.beginning_of_day
      fill_in 'Date To', with: yesterday.end_of_day
      click_on I18n.t('forms.generic_submission')

      expect(data_table).not_to have_content location_a.name
      expect(data_table).to have_content location_b.name
    end

    it 'filters by Pawn Location' do
      select(location_c.name, from: 'Location')
      click_on I18n.t('forms.generic_submission')

      expect(data_table).not_to have_content location_a.name
      expect(data_table).to have_content location_c.name
    end

    it 'filters by Automatic Purchase flag' do
      select('Yes', from: 'Auto Purchased')
      click_on I18n.t('forms.generic_submission')

      expect(data_table).not_to have_content location_a.name
      expect(data_table).to have_content location_c.name
    end

    it 'filters by Seller Paid flag' do
      select('Yes', from: 'Seller Paid')
      click_on I18n.t('forms.generic_submission')

      expect(data_table).not_to have_content location_a.name
      expect(data_table).to have_content location_d.name
    end
  end
end
