# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe Admin::NetworkPlusOffers::ApprovalsController do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:agreement_info) { FactoryGirl.create(:agreement_information, agreement_no: '5241420') }
  let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }
  let!(:network_plus_offer) { FactoryGirl.create(:network_plus_offer) }
  let(:emails) { 'sfdfdf.fghfh@sdasds.com' }
  let(:existing_network_rate) { '12' }
  let(:network_plus_surcharge) { '100' }
  let(:expiration_date) { 5.days.from_now }

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
  end

  describe 'show page' do
    it 'show terms and conditions page' do
      visit admin_network_plus_offer_approval_url(network_plus_offer)
      expect(page).to have_content('Network+@AgreementShield.com')
      expect(page).to have_content('Accept')
      expect(page).to have_content('Reject')
      expect(page).to have_content('250,000 miles')
    end

    it 'will display accepted message' do
      visit admin_network_plus_offer_approval_url(network_plus_offer)
      click_on 'Accept'
      expect(NetworkPlusOffer.last.state).to eq('approved')
      expect(page).to have_content(I18n.t('admin.network_plus_offer.approval.accepted'))
    end

    it 'will display rejected message' do
      visit admin_network_plus_offer_approval_url(network_plus_offer)
      click_on 'Reject'
      expect(NetworkPlusOffer.last.state).to eq('rejected')
      expect(page).to have_content(I18n.t('admin.network_plus_offer.approval.rejected'))
    end
  end
end
