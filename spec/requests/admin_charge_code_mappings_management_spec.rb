# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - Charge Code Mapping Management' do
  include_context 'admin steps'
  describe 'charge code mappings management workflow' do
    let!(:charge_code_mapping) { FactoryGirl.create(:charge_code_mapping) }
    before do
      login_as_finance_admin_viewer
      click_on I18n.t('charge_code_mappings.admin.navigation')
    end

    describe 'Charge Code Mappings Index' do
      it 'lists Charge Codes' do
        expect(page).to have_content charge_code_mapping.automatic_purchase
      end
    end

    describe 'Charge Code Edit' do
      it 'updates Charge Code attributes' do
        click_on I18n.t('forms.edit')
        product = 'product1'
        fill_in 'Product', with: product
        click_on I18n.t('forms.update')
        expect(page).to have_content(product)
      end
    end

    describe 'Add New Product Fees' do
      before do
        click_on 'New Charge Code Mapping'
      end

      it 'creates a New Product Fees' do
        product = 'product1'
        fill_in 'Product', with: product
        check 'Automatic Purchase'
        check 'Seller Paid'
        check 'Percent Coverage'
        fill_in 'Charge Code', with: 962
        click_on 'Create Charge code mapping'
        expect(page).to have_content(product)
      end
    end
  end
end
