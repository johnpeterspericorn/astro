# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/promise_purchase_steps'

describe 'Admin agreement information update' do
  include_context 'admin steps'

  let(:agreement_information) { FactoryGirl.create(:agreement_information) }
  let(:new_city) { SecureRandom.hex }
  let(:pawn_access_no) { SecureRandom.hex }

  before do
    login_to_admin_space
    visit edit_admin_agreement_information_path(pawn_access_no: pawn_access_no, agreement_no: agreement_information.agreement_no)
    fill_in 'agreement_information[address_city]', with: new_city
  end

  it 'updates necessary fields' do
    expect do
      click_on I18n.t('forms.update')
    end.to change { agreement_information.reload.address_city }.to(new_city)
  end

  it "doesn't update unchanged fields" do
    expect do
      click_on I18n.t('forms.update')
    end.to_not change { agreement_information.reload.address_street }
  end
end
