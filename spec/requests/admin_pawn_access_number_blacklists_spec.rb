# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Pawn Access Number Blacklists View' do
  include_context 'navigation steps'
  include_context 'admin steps'
  let!(:pawn_access_no_blacklist) { FactoryGirl.create(:pawn_access_number_blacklist) }
  let(:random_aa_number) { 123_456 }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.pawn_access_number_blacklists')
  end

  describe 'PawnAccessNumberBlacklist Index' do
    it 'should list Pawn Access Number blacklists' do
      expect(page).to have_content(pawn_access_no_blacklist.pawn_access_no)
    end

    context 'Pawn Access # filter' do
      it 'should display pawn access # blacklists given an existing aa#' do
        fill_in 'filters_pawn_access_no', with: pawn_access_no_blacklist.pawn_access_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content pawn_access_no_blacklist.pawn_access_no
      end

      it 'should not display pawn access # blacklists given a non existing aa#' do
        fill_in 'filters_pawn_access_no', with: random_aa_number
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content random_aa_number
      end
    end
  end

  describe 'Add new Pawn Access Number Blacklist' do
    before do
      click_on I18n.t('admin.pawn_access_number_blacklists.create')
    end

    it 'allows pawn access number blacklists to be added' do
      pawn_access_no = '123456'
      fill_in 'Pawn Access#', with: pawn_access_no
      click_on I18n.t('forms.finish')
      expect(page).to have_content pawn_access_no
    end
  end

  describe 'Delete Pawn Access Number Blacklist' do
    it 'allows Pawn Access Number Blacklist to be deleted' do
      click_link I18n.t('forms.delete')
      expect(page).to_not have_content(pawn_access_no_blacklist.pawn_access_no)
    end
  end

  describe 'Edit Pawn Access Number Blacklist' do
    let(:new_expiration_date) { Date.tomorrow }

    before do
      click_edit
    end

    it 'allows Pawn Access Number Blacklist to be edited' do
      fill_in 'Expiration Date', with: new_expiration_date
      click_on I18n.t('forms.update')
      expect(page).to have_content(new_expiration_date)
      expect(page).to have_content(I18n.t('pawn_access_no_blacklist.status.active'))
    end
  end
end
