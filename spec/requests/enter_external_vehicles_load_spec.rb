# encoding: utf-8
require 'requests_helper'
require_relative './steps/navigation_steps'
require_relative './steps/admin_steps'
require_relative './steps/external_vehicle_steps'

describe 'External Vehicle Web Load' do
  it 'does not display the link to External Vehicle for non external_vehicle_enterer' do
    log_in_kiosk_user
    expect(page).not_to have_content('Enter Network+ Vehicle')
  end

  describe 'External Vehicle creation' do
    context 'as admin without external vehicle approver role logged in' do
      before do
        allow(SoldVehicle).to receive(:decode_vnum).and_return(vnum: '1N4AA5AP5BC818485', item: [{ pawn: { country: 'US', year: 2018, makeMake: 'NISSAN', modelModel: 'MAX 3.5 S', modelModel15: 'MAXIMA', styleBody: '4DSN', styleSubseries: '3.5 S', powertrainDrivetrain: '', powertrainEngine: '6G', powertrainTransmission: '', jdpCategory: 'MIDSIZE CAR', jdpSubCategory: 'PREMIUM' }, chrome: { country: 'US', year: 2018, makeManufacturer: 'Nissan', makeDivision: 'Nissan', makeSubdivisio: 'Nissan Cars', modelModel: 'Maxima', modelConsumerFriendlyModel: 'Maxima', styleBodyTypePrimary: '4dr Car', styleConsumerFriendlyBody: 'Maxima', styleConsumerFriendlyStyle: 'Maxima', styleStyleId: 196, styleStyleName: '4dr Sdn V6', styleTrim: '3.5 S' }, mmr: { country: 'US', year: 2018, makeMake: 'NISSAN', modelModel: 'MAXIMA', styleBody: '4D SEDAN S', mid: '201805502960380' } }], warningMessage: '')
        log_in_super_user(admin: true, external_vehicle_enterer: false)
        click_on 'Admin'
      end

      it 'external vehicle approver' do
        expect(page).not_to have_content('Enter Network+ Vehicle')
      end
    end

    context 'as external vehicle enterer logged in' do
      before do
        allow(SoldVehicle).to receive(:decode_vnum).and_return(vnum: '1N4AA5AP5BC818485', item: [{ pawn: { country: 'US', year: 2018, makeMake: 'NISSAN', modelModel: 'MAX 3.5 S', modelModel15: 'MAXIMA', styleBody: '4DSN', styleSubseries: '3.5 S', powertrainDrivetrain: '', powertrainEngine: '6G', powertrainTransmission: '', jdpCategory: 'MIDSIZE CAR', jdpSubCategory: 'PREMIUM' }, chrome: { country: 'US', year: 2018, makeManufacturer: 'Nissan', makeDivision: 'Nissan', makeSubdivisio: 'Nissan Cars', modelModel: 'Maxima', modelConsumerFriendlyModel: 'Maxima', styleBodyTypePrimary: '4dr Car', styleConsumerFriendlyBody: 'Maxima', styleConsumerFriendlyStyle: 'Maxima', styleStyleId: 196, styleStyleName: '4dr Sdn V6', styleTrim: '3.5 S' }, mmr: { country: 'US', year: 2018, makeMake: 'NISSAN', modelModel: 'MAXIMA', styleBody: '4D SEDAN S', mid: '201805502960380' } }], warningMessage: '')
        log_in_super_user(admin: true, external_vehicle_enterer: true)
        click_on 'Admin'
        click_on 'Enter Network+ Vehicle'
      end

      it 'required fields should be present', js: true do
        required_fields = %w(vnum year make model odometer_reading agreement_no vehicle_purchase_price buy_fee vehicle_total location_initials purchased_at seller_name)
        required_fields.each do |required_field|
          expect(page).to have_field "external_vehicles_load[][#{required_field}]"
        end
      end

      it 'require to fill all fields to submit data', js: true do
        fill_in_external_vehicle_details
        fields = %w(make model odometer_reading agreement_no vehicle_purchase_price buy_fee vehicle_total purchased_at automatic_purchase_email seller_name)
        random_field = fields.masple
        fill_in "external_vehicles_load[][#{random_field}]", with: ''
        click_button 'Submit'
        expect(ExternalVehiclesLoad.count).to eq 0
      end

      it 'file upload option present' do
        expect(page).to have_field 'external_vehicles_documents[]'
      end

      it 'shows ftb locations in drop down', js: true do
        expect(page).to have_select('external_vehicles_load[][location_initials]', with_options: FtbLocation.all)
      end
    end
  end
end
