# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Vin Blacklists View' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:vnum_blacklist) { FactoryGirl.create(:vnum_blacklist, expiration_date: Date.current) }
  let(:rand_vnum) { 'ABCDEF' }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.vnum_blacklists')
  end

  describe 'VinBlacklist Index' do
    it 'should list vnum blacklists' do
      expect(page).to have_content(vnum_blacklist.vnum)
    end

    context 'VNUM filter' do
      it 'should display vnum blacklists given an existing VNUM' do
        fill_in 'VNUM', with: vnum_blacklist.vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_content vnum_blacklist.vnum
      end

      it 'should not display vnum blacklists given a non existing VNUM' do
        fill_in 'VNUM', with: rand_vnum
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content rand_vnum
      end
    end
  end

  describe 'Edit VinBlacklist' do
    let(:new_expiration_date) { Date.tomorrow }

    before do
      click_edit
    end

    it 'allows VinBlacklist to be edited' do
      fill_in 'Expiration Date', with: new_expiration_date
      click_on I18n.t('forms.update')
      expect(page).to have_content(new_expiration_date)
    end
  end

  describe 'Delete VinBlacklist' do
    it 'allows VinBlacklist to be deleted' do
      click_link I18n.t('forms.delete')
      expect(page).to_not have_content(vnum_blacklist.vnum)
    end
  end

  describe 'Add VinBlacklist' do
    let(:vnum) { '12345' }

    before do
      click_on I18n.t('admin.vnum_blacklists.create')
    end

    it 'allows vnum blacklists to be added' do
      fill_in 'Vin', with: vnum
      click_on I18n.t('forms.finish')
      expect(page).to have_content vnum
    end
  end
end
