# encoding: utf-8
require 'requests_helper'
require_relative './steps/purchase_cancellation_steps'
require_relative './steps/return_invoice_steps'

describe 'Cancellation workflow' do
  include_context 'purchase cancellation steps'
  include_context 'return invoice steps'

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000) }

  before do
    stub_buyer_authorization
    stub_ods_for_refund
  end

  context 'super user logged in' do
    let(:reason) { I18n.t('cancellation.cancellation_reasons')[1] }

    before do
      log_in_super_user
      purchase_promise
    end

    it 'can cancel purchase' do
      purchase_can_be_cancelled
    end

    it 'has a working back button' do
      super_user_view_returns_page
      find(purchase_cancellation_button).click
      click_on I18n.t('forms.back')

      expect(current_path).to eql return_funnel_filter_path(:pawn_access_number_filter)
    end

    it 'can be re-purchased' do
      super_user_view_returns_page
      find(purchase_cancellation_button).click
      select reason, from: 'Reason'
      click_on 'Submit'

      promise_can_be_purchased
    end

    context 'with delayed jobs enabled' do
      before do
        allow_any_instance_of(Astroheim::Charge).to receive(:submit!)
        Settings.charges_enabled = true
      end

      it 'successfully deserializes refund jobs' do
        super_user_view_returns_page
        find(purchase_cancellation_button).click
        select reason, from: 'Reason'
        click_on 'Submit'
        job = Delayed::Job.second
        expect { job.invoke_job }.not_to raise_error
      end

      context 'when the associated offer is pending' do
        let(:promise_offer) { PromiseOffer.first }

        before do
          expect(PromiseOffer.count).to eq(1)
          promise_offer.update_attributes!(promise_status: :pending)
        end

        it 'does not issue a refund' do
          expect_any_instance_of(PromiseChargeJob).not_to receive(:enqueue)

          super_user_view_returns_page
          find(purchase_cancellation_button).click
          select reason, from: 'Reason'
          click_on 'Submit'
        end
      end
    end
  end

  context 'other user logged in' do
    before do
      log_in_kiosk_user
    end

    it 'cannot cancel a promise purchase' do
      purchase_promise
      purchase_cannot_be_cancelled
    end

    context 'cancellation ineligible for re-purchase' do
      it 'cannot be re-purchased' do
        purchase = FactoryGirl.create(:promise_purchase)
        Cancellation.new(
          promise_purchase_id: purchase.id,
          ineligible_for_repurchase: true
        ).process

        view_returns_page
        expect(page).to_not have_css('.promise_options')
      end
    end
  end
end
