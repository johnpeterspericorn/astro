# encoding: utf-8
require 'requests_helper'

describe 'Pawn Locations' do
  let!(:location) { FactoryGirl.create(:pawn_location, purchases_enabled: true) }

  before do
    visit pawn_locations_path
  end

  it 'lists all purchases_enabled locations' do
    expect(page).to have_content(location.name)
  end
end
