# encoding: utf-8
require 'requests_helper'
require_relative './steps/promise_purchase_steps'
require_relative './steps/promise_selection_vehicle_steps'
require_relative './steps/navigation_steps'

describe 'Promise Purchase Workflow Regression Tests' do
  include_context 'navigation steps'
  include_context 'promise purchase steps'
  include_context 'promise selection vehicle steps'

  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let!(:pawn_location) { FactoryGirl.create(:svaa_location) }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA') }
  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA') }
  before do
    stub_buyer_authorization
    log_in_kiosk_user
  end

  it 'should not blow up if vehicles for previously purchased promises no longer exist' do
    view_vehicles_by_pawn_access_number_as_kiosk_user
    select_honda_for_500_miles_and_14_additional_days
    checkout
    click_done

    honda.destroy

    # Note... if you do this before the deletion, badness.
    view_vehicles_by_pawn_access_number_as_kiosk_user
    select_acura_for_250_miles_and_7_additional_days
    checkout
    click_done

    page_should_be_root_page
  end

  it 'should not blow up if no vehicles are selected during the checkout path' do
    view_vehicles_by_pawn_access_number_as_kiosk_user
    click_next
    expect(current_path).to eql promise_offers_path
  end

  it "should not display another user's promise offers for a previously unselected (but presented vehicle) if no new vehicles are present" do
    stub_buyer_authorization

    # create an old, unselected, promise offer for the vehicle
    PromiseOfferRecorder.record_offers [honda]
    PromiseOffer.first.update_attributes(created_at: 1.week.ago)

    # create a new promise offer, with a different work order number and sblu
    honda.update_attributes!(universal_no: 'new_universal', work_order_number: 989, sblu: 333, pawn_access_no: 'newAA', agreement_no: 'new5M')
    PromiseOfferRecorder.record_offers [honda]

    expect(PromiseOffer.by_vnum(honda.vnum).count).to eq(2)

    # the vehicle should not be presented to original user
    view_vehicles_by_pawn_access_number_as_kiosk_user
    expect(page).not_to have_content 'Honda'
  end

  it 'should not show duplicate cars for a single badge # promise offer and two Sold Vehicles' do
    FactoryGirl.create(:pawn_location, initials: honda.location_initials)
    PromiseOfferRecorder.record_offers [honda]

    # Sold vehicles is a direct DB insert by MAS, so no validations are checked
    honda.dup.save(validate: false)

    view_vehicles_by_badge_number

    page_should_only_have_one_honda_civic
  end

  #  Note, this is the regression drivnumg a refactor to decouple everything
  #  from SoldVehicles
  it 'should allow a user to purchase a promise when the old sold vehicle refers to a different vehicle than the offer' do
    SoldVehicle.where(make: 'ACURA').first
    honda = SoldVehicle.where(make: 'HONDA').first

    purchase_promise_for_vehicle honda
    expect(PromisePurchase.count).to eq(1)
    # purchase = PromisePurchase.first
    # purchase.sold_vehicle = acura
    # purchase.save

    # show that promises on the acura _can_ be purchased
    view_vehicles_by_pawn_access_number_as_kiosk_user
    select_acura_for_250_miles_and_14_additional_days
    click_next

    expect(page).to have_content 'Acura'

    # but for some reason you cannot for 7 days
    view_vehicles_by_pawn_access_number_as_kiosk_user
    select_acura_for_250_miles_and_7_additional_days
    click_next

    expect(page).to have_content 'Acura'
  end

  it 'should not change prices from the selection page into the confirmation page if sold vehicle prices change under us' do
    original_offer_price = honda.promise_250.to_i

    # User visits kiosk and does not select a promise on a Honda but we record
    # the offer with a slightly higher price
    honda.update_attributes(promise_250: original_offer_price + 1)
    view_vehicles_by_pawn_access_number_as_kiosk_user

    # User returns to kiosk and decides to purchase a promise on the Honda.
    # In the mean time the cost of the promise has been reduced based on
    # changes in the database.  The user is presented with a new lower cost
    honda.update_attributes(promise_250: original_offer_price)
    view_vehicles_by_pawn_access_number_as_kiosk_user
    honda_selected_price_at_250_miles_and_0_additional_days = honda_information.first('label').text.strip
    select_honda_for_250_miles_and_0_additional_days
    click_next

    # on checkout user is presented with the previously offered price but should
    # be presented with the promise offer price
    honda_offer_price_at_250_miles_and_0_additional_days = page.find('.price').text.strip
    expect(honda_offer_price_at_250_miles_and_0_additional_days).to eql honda_selected_price_at_250_miles_and_0_additional_days
  end

  it 'should update the correct promise offer when an older purchase exists' do
    old_timestamps = { created_at: 1.week.ago, updated_at: 1.week.ago }

    vehicle_attributes = { 'model' => 'ELANTRA', 'odometer_reading' => 121_518, 'year' => 2006, 'make' => 'HYUNDAI', 'vnum' => 'KMHDN46D86U306610', 'lane_no' => '14', 'run_no' => '217', 'universal_no' => 'SVAA2018161402171423385', 'pawn_access_no' => '100467186', 'badge_no' => '1725', 'agreement_no' => '5251381', 'promise_250' => 100.0, 'additional_day' => 2.85, 'promise_500' => 175.0, 'vehicle_purchase_price' => 3900, 'ineligibility_condition' => 0, 'if_bid' => false, 'work_order_number' => '1423385', 'location_initials' => 'SVAA', 'ove' => nil, 'presale_inspection' => nil, 'buy_fee' => 220.0, 'promise_prepaid' => nil, 'inspection_only_price' => nil, 'inspection_bundle_price' => nil, 'automatic_purchase' => nil, 'preselected_promise_days' => nil, 'preselected_promise_miles' => nil, 'automatic_purchase_email' => nil, 'automatic_purchase_processed' => false, 'sblu' => '12231627' }
    vehicle = FactoryGirl.create(:sold_vehicle, vehicle_attributes)

    old_offer_attributes = { 'days_selected' => 7, 'miles_selected' => 250, 'promise_price' => 50.0, 'promise_status' => :promised, 'emails' => 'jbcampbell_1@hotmail.com', 'promise_250' => 50.0, 'promise_500' => 100.0, 'additional_day' => 2.85, 'ineligibility_condition' => 0, 'if_bid' => false, 'left_lot' => false, 'automatic_purchase' => nil }
    old_offer = PromiseOffer.create!(old_offer_attributes.merge(old_timestamps).merge(promised_at: 1.week.ago))

    old_purchase_attributes = { 'id' => 626, 'miles_selected' => 250, 'additional_days' => 0, 'transaction_id' => '0_1365521692_c97daa74f7a2' }
    old_offer.create_promise_purchase(old_purchase_attributes.merge(old_timestamps))

    old_pawn_info_attributes = { 'pawn_access_no' => '0', 'badge_no' => '647', 'buy_fee' => 150.0, 'agreement_no' => '9269444', 'lane_no' => '4', 'run_no' => '60', 'sale_no' => '1', 'location_initials' => 'SVAA', 'universal_no' => 'SVAA2018150400601421644', 'vehicle_purchase_price' => 2900.0, 'work_order_number' => '1421644', 'sblu' => '12231627' }
    old_offer.create_pawn_information(old_pawn_info_attributes.merge(old_timestamps))

    old_vehicle_info_attributes = { 'make' => 'HYUNDAI', 'model' => 'ELANTRA', 'odometer_reading' => 121_505, 'ove' => nil, 'vnum' => 'KMHDN46D86U306610', 'year' => 2006 }
    old_offer.create_vehicle_information(old_vehicle_info_attributes.merge(old_timestamps))

    view_vehicles_by_pawn_access_number_as_kiosk_user vehicle.pawn_access_no
    select_promise vehicle: vehicle, miles: 250, days: 0
    click_next
    expect(page).to have_content 'Hyundai'
  end
end
