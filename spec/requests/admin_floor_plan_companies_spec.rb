# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Floor Plan Company Admin' do
  include_context 'admin steps'

  let!(:floor_plan_company) { FactoryGirl.create(:floor_plan_company) }

  before do
    login_as_finance_admin_viewer
    click_on I18n.t('admin.navigation.floor_plan_companies')
  end

  describe 'filtering floor plan companies' do
    it 'filters by company code' do
      code = 'XYZ'
      FactoryGirl.create(:floor_plan_company, company_code: code)

      fill_in 'Company code', with: code
      click_on 'Save Filters'

      expect(page).to have_content(code)
      expect(page).to_not have_content(floor_plan_company.company_code)
    end
  end

  describe 'Editing a floor plan company' do
    let(:new_code) { 'NMH' }

    it "lets an admin update the company's data" do
      click_on floor_plan_company.name
      fill_in 'Code', with: new_code
      page.check('Floor Plan Does Not Pay DS Fee')
      click_on 'Submit'

      expect(page).to have_content new_code
      expect(page).to have_content true
    end
  end

  describe 'Creating a new floor plan company' do
    let(:name) { 'My New Company' }
    let(:company_code) { 'NMH' }

    it 'lets an admin create a new company row' do
      click_on I18n.t('admin.floor_plan_companies.create')

      %w(name company_code).each do |field|
        fill_in "floor_plan_company_#{field}", with: send(field)
      end

      expect do
        click_on 'Submit'
      end.to change(FloorPlanCompany, :count).by(1)

      expect(page).to have_content name
    end
  end
end
