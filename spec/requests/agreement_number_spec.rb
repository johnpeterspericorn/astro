# encoding: utf-8
require 'requests_helper'
require_relative './steps/agreement_number_steps'
require_relative './steps/pawn_location_steps'

describe 'Agreement Number Workflow' do
  include_context 'agreement number steps'
  include_context 'pawn location steps'

  before do
    Settings.show_agreement_filter_ui = true
  end

  context 'as a kiosk user' do
    before do
      stub_buyer_authorization
      log_in_kiosk_user
    end

    it 'does not show agreement filter' do
      expect(page).not_to have_content(agreement_input_label)
    end
  end

  context 'as a super user' do
    before do
      create_location_data
      log_in_super_user
    end

    let(:agreement_number) { '5234382' }
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', agreement_no: agreement_number) }
    let!(:volvo) { FactoryGirl.create(:sold_vehicle, make: 'VOLVO') }

    it 'should show only the users associated with a given agreement #' do
      visit_filter_page
      click_agreement_number
      enter_agreement_number agreement_number

      page_should_have_vehicle_makes(acura)
      page_should_not_have_vehicle_makes(volvo)
    end
  end
end
