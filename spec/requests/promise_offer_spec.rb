# encoding: utf-8
require 'requests_helper'
require_relative './steps/promise_selection_steps'
require_relative './steps/promise_selection_vehicle_steps'

describe 'Promise Offer Selection View' do
  include_context 'promise selection'
  include_context 'promise selection vehicle steps' # for vehicle information helpers

  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:agreement_no)         { SasDataHelper::DEFAULT_DEALER_NUMBER         }

  let!(:sva_location) { FactoryGirl.create(:svaa_location) }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', pawn_access_no: pawn_access_no) }

  before do
    Settings.show_badge_ui = true
  end

  context 'vehicle display' do
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', pawn_access_no: pawn_access_no) }
    let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', pawn_access_no: pawn_access_no) }
    let!(:volvo) { FactoryGirl.create(:sold_vehicle, make: 'VOLVO', pawn_access_no: pawn_access_no) }
    let!(:mitsubishi) { FactoryGirl.create(:sold_vehicle, make: 'MITSUBISHI', pawn_access_no: pawn_access_no) }

    let(:acura_label_id) { "##{promise_label(acura, miles: 250, days: 0)}" }

    before do
      stub_buyer_authorization
      log_in_kiosk_user
    end

    context 'user with 0 pawn access number' do
      it 'does not allow user to proceed' do
        view_vehicles_by_pawn_access_number_as_kiosk_user '0'

        expect(page).to have_content('Pawn Access Number must be greater than 0')
      end
    end

    context 'users with vehicles' do
      it 'should show only the vehicles associated with a given pawn access number' do
        mitsubishi.update_attributes(pawn_access_no: '100', agreement_no: agreement_no)
        view_vehicles_by_pawn_access_number_as_kiosk_user
        page_should_have_vehicles_by_agreement_number_and_pawn_access_number %w(ACURA HONDA)
        page_should_not_have_vehicles_for_their_agreement_numbers_with_different_pawn_access_numbers
      end

      it 'should have full information for a given vehicle' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        page_should_have_vehicle_info
      end

      it 'should have the promise matrix for a given vehicle' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        page_should_have_promise_matrix
      end

      it 'should indicate an if-bid on a vehicle' do
        acura.update_attributes(if_bid: true)

        view_vehicles_by_pawn_access_number_as_kiosk_user

        acura_should_be_an_if_bid
      end

      it 'should show eligible for purchase, then purchased, then ineligible vehicles' do
        acura.update_attributes(ineligibility_condition: 1)
        view_vehicles_by_pawn_access_number_as_kiosk_user

        # mitsubishi should be eligible for purchase
        # honda should have been purchased
        purchase_promise_for_vehicle honda
        # acura should be ineligible

        view_vehicles_by_pawn_access_number_as_kiosk_user

        within('.current_vehicles') do
          expect(page.body).to match(/Mitsubishi.*Honda.*Acura/m)
        end
      end

      it 'should update offers if the sold vehicle changes' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        expect(acura_information).to have_content acura.promise_250.to_i

        acura.update_attributes(promise_250: 666, promise_500: 999)
        volvo.update_attributes(pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(acura_information).to have_content acura.promise_250.to_i
        expect(acura_information).to have_content acura.promise_500.to_i

        expect(vehicle_information('volvo')).to be_present
      end

      it 'should show non-inspection UI for vehicles without inspection information' do
        Settings.inspections_enabled = true
        acura.update_attributes(inspection_only_price: nil, inspection_bundle_price: nil)
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(acura_information).to have_content '$ 0'
        expect(acura_information).not_to have_content I18n.t('promise_offers.inspection_ui.comprehensive_check')
      end

      context 'vehicles ineligible for purchase' do
        it "should not have 'none' preselected for left lot vehicles" do
          checkout_vehicle acura
          view_vehicles_by_pawn_access_number_as_kiosk_user
          expect(acura_information).to have_content I18n.t('promise_offers.vehicles.left_lot_copy')
          expect(acura_information).not_to have_css('td.selected')
        end

        it 'should not allow a purchase of an ineligible vehicle', js: true, broken: true do
          checkout_vehicle acura
          view_vehicles_by_pawn_access_number_as_kiosk_user
          within acura_information do
            find(acura_label_id).click
          end
          next_button_should_not_be_enabled
        end

        it 'should not display prices for a past-cutoff vehicle', js: true do
          set_vehicle_past_cutoff acura
          view_vehicles_by_pawn_access_number_as_kiosk_user
          expect(acura_information).not_to have_css('.promise_options')
        end

        it "should not have 'none' preselected for past-time vehicles" do
          set_vehicle_past_cutoff acura
          view_vehicles_by_pawn_access_number_as_kiosk_user
          expect(acura_information).to have_content I18n.t('promise_offers.vehicles.past_cutoff_copy')
          expect(acura_information).not_to have_css('td.selected')
        end

        it 'should not allow a purchase of a seller-paid vehicle', js: true do
          make_vehicle_seller_paid acura

          purchase_promise_for_vehicle(acura, 7, 500)
          view_vehicles_by_pawn_access_number_as_kiosk_user
          seller_paid_vehicle_should_have_copy_and_blank_grid
        end

        # Bug #53841825
        it 'should not show duplicate vehicles when made ineligible by ods attributes' do
          allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(true)
          view_vehicles_by_pawn_access_number_as_kiosk_user
          expect(page).to have_css('h3', text: 'Acura', count: 1)
        end
      end
    end

    context 'users without vehicles' do
      before do
        allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([])
        view_vehicles_by_pawn_access_number_as_kiosk_user '100000000'
      end

      it "should show 'no vehicles' copy" do
        page_should_display_copy_for_users_with_no_vehicles
      end

      it 'should have a home button' do
        click_on I18n.t('forms.home')
        expect(page.current_path).to eql root_path
      end
    end

    context 'bad AA#s' do
      it 'should return to filters page' do
        allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:has_contact_details?).and_return(false)
        view_vehicles_by_pawn_access_number_as_kiosk_user '10003186'
        expect(page.current_path).to eql edit_vehicle_filter_path
      end
    end
  end
end
