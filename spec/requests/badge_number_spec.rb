# encoding: utf-8
require 'requests_helper'
require_relative './steps/badge_number_steps'
require_relative './steps/pawn_location_steps'

describe 'Badge Number Workflow' do
  include_context 'badge number steps'
  include_context 'pawn location steps'

  before do
    Settings.show_badge_ui = true
  end

  context 'as a kiosk user' do
    let(:badge_number) { 1234 }

    let!(:pawn) { FactoryGirl.create(:svaa_location) }

    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', badge_no: badge_number, location_initials: pawn.initials) }
    let!(:acura_location) { FactoryGirl.create(:pawn_location, initials: acura.location_initials, purchases_enabled: true) }

    let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', location_initials: pawn.initials) }
    let!(:honda_location) { FactoryGirl.create(:pawn_location, initials: honda.location_initials, purchases_enabled: true) }

    before do
      stub_buyer_authorization
      log_in_kiosk_user
    end

    it 'should show only the users associated with a given badge #' do
      visit_filter_page
      click_badge_number
      enter_badge_number badge_number

      page_should_have_vehicle_makes(acura)
      page_should_not_have_vehicle_makes(honda)
    end
  end

  context 'as a super user' do
    let!(:disabled_location) { FactoryGirl.create(:pawn_location, purchases_enabled: false) }

    let(:location_initials) { 'DRIV' }
    let!(:pawn_location) { FactoryGirl.create(:svaa_location, initials: location_initials) }
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA') }
    let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', location_initials: location_initials) }

    before do
      Settings.show_badge_ui = true
      log_in_super_user
      create_location_data
    end

    it 'should not show disabled locations' do
      visit_filter_page
      click_badge_number

      expect(page).not_to have_css('option', text: disabled_location.name)
    end

    context 'user has no location initials' do
      before do
        visit_filter_page
        click_badge_number
      end

      it 'should filter by selected dropdown' do
        fill_in 'badge_number', with: SasDataHelper::DEFAULT_BADGE_NUMBER
        click_on I18n.t('forms.enter')

        page_should_have_vehicle_makes(acura)
        page_should_not_have_vehicle_makes(honda)
      end
    end

    context 'user has location initials' do
      before do
        User.where(super_user: true).first.update_attributes(location_initials: location_initials)
        visit_filter_page
        click_badge_number
      end

      it "the hidden location dropdown should have the user's location selected", js: true do
        expect(page.find('.super_user_location_selector')).not_to be_visible
        click_on I18n.t('forms.super_user.pawn_location_show_hide_link')
        expect(page.find('.super_user_location_selector')).to be_visible
        expect(page.find('#location_initials').value).to eql location_initials
      end

      it 'should filter by selected dropdown' do
        fill_in 'badge_number', with: SasDataHelper::DEFAULT_BADGE_NUMBER
        click_on I18n.t('forms.enter')

        page_should_not_have_vehicle_makes(acura)
        page_should_have_vehicle_makes(honda)
      end
    end
  end
end
