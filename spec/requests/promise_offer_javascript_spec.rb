# encoding: utf-8
require 'requests_helper'
require_relative './steps/promise_selection_steps'
require_relative './steps/promise_selection_vehicle_steps'

describe 'Promise Offers Selection View - javascript exercises', js: true do
  include_context 'promise selection'
  include_context 'promise selection vehicle steps'

  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:agreement_no)         { SasDataHelper::DEFAULT_DEALER_NUMBER         }
  let!(:sva_location) { FactoryGirl.create(:svaa_location) }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', pawn_access_no: pawn_access_no) }
  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', pawn_access_no: pawn_access_no) }

  let(:acura_label_id) { "##{promise_label(acura, miles: 250, days: 0)}" }

  before do
    stub_buyer_authorization
    log_in_kiosk_user
    view_vehicles_by_pawn_access_number_as_kiosk_user
  end

  context "price factors'" do
    it 'should toggle rating info' do
      within acura_information do
        expect(find('.price_factors')).not_to be_visible
        click_on I18n.t('vehicles.ratings.link')
        expect(find('.price_factors')).to be_visible
      end
    end

    it 'should preserve selected promise option' do
      within acura_information do
        page.execute_script "$('#{acura_label_id}').click()" # find(acura_label_id).click

        expect(find(acura_label_id)).to be_checked
        click_on I18n.t('vehicles.ratings.link')
        expect(find(acura_label_id)).to be_checked
      end
    end
  end

  context 'agreement filters' do
    let(:honda_agreement_name) { 'Agreement for Honda' }
    let(:acura_agreement_name) { 'Agreement for Acura' }

    context 'user has multiple agreementships' do
      before do
        honda.agreement_no = '6000000'
        honda.save!

        allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers_to_names) do
          {
            honda.agreement_no => honda_agreement_name,
            acura.agreement_no => acura_agreement_name
          }
        end

        view_vehicles_by_pawn_access_number_as_kiosk_user
      end

      it 'should let a user filter by single agreementships' do
        select acura_agreement_name, from: 'agreementship'
        click_on I18n.t('forms.filter')

        expect(listing_for(acura)).to be_visible
        expect(listing_for(honda)).not_to be_visible

        select honda_agreement_name, from: 'agreementship'
        click_on I18n.t('forms.filter')

        expect(listing_for(acura)).not_to be_visible
        expect(listing_for(honda)).to be_visible
      end
    end

    context 'when user only has one agreement number' do
      it "shouldn't show the filter partial" do
        expect(page).not_to have_content 'Agreement #s'

        expect(listing_for(acura)).to be_visible
        expect(listing_for(honda)).to be_visible
      end
    end
  end
end
