# encoding: utf-8
require 'requests_helper'
require_relative './steps/payment_steps'
require_relative './steps/return_invoice_steps'

describe 'Return Payments Workflow' do
  include_context 'payment steps'
  include_context 'return invoice steps'
  include_context 'new_product_data'

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000) }
  let!(:palv_vehicle) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', odometer_reading: 10_000, limited_volume: 3, seller_paid: false) }
  let!(:palv_canadian_vehicle) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', odometer_reading: 10_000, limited_volume: 3, seller_paid: false) }
  let!(:floor_plan_company) { FactoryGirl.create(:floor_plan_company, company_code: 'FMC2') }

  let(:vehicle_on_lot) { acura }
  let(:promise_offer)      { vehicle_on_lot.promise_offers.first }
  let(:promise_purchase)   { promise_offer.promise_purchase }
  let(:fees) { double('Sas::Fees') }
  let(:return_button_css) { '[rel~=return_purchase]' }
  let(:return_fee_return_button) { '[rel~=return_fee_return_purchase]' }
  let(:pay_for_purchase_button) { '[rel~=pay_for_purchase]' }

  before do
    allow_any_instance_of(PromiseOffer).to receive(:left_lot_in_ods?).and_return(false)
    stub_buyer_authorization
  end

  context 'requiring payment' do
    context 'kiosk user' do
      before do
        log_in_kiosk_user
      end

      context 'user purchases promise for on lot vehicle' do
        before do
          VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
          purchase_promise_for_vehicle(vehicle_on_lot)
        end

        after do
          VCR.eject_cassette
        end

        it 'directs users to pay for promise when vehicle is not paid for' do
          view_returns_page
          click_on I18n.t('forms.return')

          expect(page).to have_content(I18n.t('forms.pay_for_promise'))
        end

        it 'does not return if user attempts to bypass verify step' do
          allow_any_instance_of(BuyerAndAgreementInformationForm).to receive(:valid?).and_return(true)
          expect do
            page.driver.post return_funnel_return_invoices_path(promise_purchase_id: promise_purchase.id, return_invoice: { location_initials: nil }, buyer_and_agreement_information_form: { some: :param })
          end.to_not change(ReturnInvoice, :count)
        end
      end

      context 'user purchases promise for seller paid vehicle' do
        before do
          vehicle_on_lot.update_attributes!(seller_paid: true)
          purchase_promise_for_vehicle(vehicle_on_lot)
        end

        it 'does not require payment from user' do
          view_returns_page

          expect(page).to_not have_content(I18n.t('forms.pay_for_promise'))
        end
      end
    end

    context 'super user' do
      before do
        log_in_super_user
        VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
        purchase_promise_for_vehicle(vehicle_on_lot)
      end

      after do
        VCR.eject_cassette
      end

      it 'allows payment to be skipped' do
        view_returns_page
        find(return_button_css).click
        find(return_button_css).click

        expect(page).to have_content('Reasons for Return')
      end
    end

    context 'informational message' do
      before do
        VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

        log_in_super_user
        purchase_promise_for_vehicle(vehicle_on_lot)
      end

      after do
        VCR.eject_cassette
      end

      context 'paid with floorplan' do
        it 'show informational message with floor plan company name after clicking on payment' do
          view_returns_page
          find(return_button_css).click
          expect(page).to have_content(I18n.t('return_invoice.floor_plan_informal_message', floor_plan_company: floor_plan_company.name))
        end

        it 'should not show cc payment link if skip_ds_payment true' do
          floor_plan_company.update_attributes(skip_ds_payment: true)
          view_returns_page
          find(return_button_css).click
          expect(page).to_not have_link 'Pay for Promise'
        end
      end

      context 'unpaid' do
        it 'show informational message of unpaid after clicking on payment' do
          allow_any_instance_of(PromiseOffer).to receive(:adjustments_paid_by_floor_plan?).and_return(false)
          view_returns_page
          find(return_button_css).click

          expect(page).to have_content(I18n.t('return_invoice.unpaid_informal_message'))
        end
      end
    end
  end

  context 'as super user with no invoiceable rights' do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('payment/customer/charge', allow_playback_repeats: true)
      log_in_super_user(invoiceable: false)
      purchase_promise_for_vehicle(vehicle_on_lot)
      stub_fees_to_allow_payments
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'promise unpaid' do
      it 'shows the pay for promise BUT NO return button' do
        view_returns_page
        find(return_button_css).click

        expect(page).to have_link 'Pay for Promise'
        expect(page).to have_no_link 'Return'
      end
    end
  end

  context 'as super user with invoiceable rights' do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      log_in_super_user
      purchase_promise_for_vehicle(vehicle_on_lot)
      stub_fees_to_allow_payments
    end

    after do
      VCR.eject_cassette
    end

    context 'promise unpaid' do
      it 'shows the pay for promise and return button' do
        view_returns_page
        find(return_button_css).click

        expect(page).to have_link 'Pay for Promise'
        expect(page).to have_link 'Return'
      end
    end
  end

  context 'as kiosk user with invoiceable rights' do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      log_in_kiosk_user
      purchase_promise_for_vehicle(vehicle_on_lot)
      stub_fees_to_allow_payments
    end

    after do
      VCR.eject_cassette
    end

    context 'promise unpaid' do
      it 'shows the pay for promise' do
        view_returns_page
        find(return_button_css).click

        expect(page).to have_link 'Pay for Promise'
        expect(page).to have_no_link 'Return'
      end
    end
  end

  context 'seller_paid' do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      log_in_super_user
      vehicle_on_lot.update_attributes!(seller_paid: true)
      purchase_promise_for_vehicle(vehicle_on_lot)
      stub_fees_to_allow_payments
    end

    after do
      VCR.eject_cassette
    end

    context 'promise unpaid' do
      it 'shows the pay for promise continue return process' do
        view_returns_page
        expect(page).to have_link 'Return'

        find(return_button_css).click
        expect(page).to have_content I18n.t('return_funnel.verifications.form.label.additional_information')
        expect(page).to have_content I18n.t('return_funnel.verifications.form.label.title_status')
      end
    end
  end

  context 'new product fee' do
    let(:payment_button_css) { '[rel~=pay_for_purchase]' }
    let(:pawn_access_number_filter_button) { I18n.t('forms.pawn_access.input_label') }
    let(:agreement_number_filter_button)        { I18n.t('forms.agreement_number.input_label')  }
    let(:vnum_number_filter_button)           { I18n.t('forms.vnum_number.input_label')     }
    let(:mileage) { acura.odometer_reading + max_mileage - 1 }

    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('vehicle_deocder_api/successful_easypost_shipment', allow_playback_repeats: true)
      log_in_super_user
      purchase_promise_for_vehicle(vehicle_on_lot)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'new product fee payment' do
      before do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(5)
        allow_any_instance_of(PromisePurchase).to receive(:limited_volume).and_return(3)
        create_product_fees
        stub_fees_to_allow_payments
      end

      it 'show informational message after clicking on payment' do
        view_returns_page
        find(return_button_css).click

        expect(page).to have_content(I18n.t('return_invoice.floor_plan_informal_message', floor_plan_company: floor_plan_company.name))
      end

      it 'skipping pplv return fee payment should NOT take user to cc payment form' do
        initiate_pplv_return(acura, false)
        find(return_fee_return_button).click
        click_on I18n.t('new_product_fee.payment.skip_payment')
        enter_odometer_reading(mileage)
        enter_location(pawn_location.name)
        select_title_status
        select_return_reason('mechanical_issues')
        select_return_reason('unable_to_sell')
        fill_in 'return_invoice_additional_information', with: additional_information
        verify_return
        update_buyer_information
        expect(page).to have_content('Your return has been initiated.')
      end
    end
  end
end
