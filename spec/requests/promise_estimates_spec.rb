# encoding: utf-8
require 'requests_helper'

describe 'Promise Estimation Workflow' do
  let!(:pawn_location) { FactoryGirl.create(:pawn_location) }
  let!(:malibu) { FactoryGirl.create(:presale_vehicle, model: 'MALIBU', lane_no: 1, run_no: 1, location_initials: pawn_location.initials) }

  context 'as a user who does not have agreementships' do
    let(:kiosk_user) { User.find_by_username('kiosk') }

    before do
      visit root_path
      log_in_kiosk_user
    end

    it 'prompts user for pawn access number entry' do
      visit quotes_research_path
      submit_pawn_access_number_form_for_kiosk_user('100686866')

      expect(page).to have_css('select#buyer_number')
    end
  end

  context 'as a user havnumg agreementships and a default pawn location' do
    let(:kiosk_user) { User.find_by_username('kiosk') }

    before do
      visit root_path
      stub_buyer_authorization
      log_in_kiosk_user(pawn_location.initials)
    end

    it 'should present pawn filter page with default pawn location selected' do
      visit quotes_research_path
      submit_pawn_access_number_form_for_kiosk_user('100686866')
      select first('#buyer_number option').text, from: 'buyer_number'
      click_on I18n.t('promise_estimates.filters.pawn')

      expect(page).to have_select('location_initials', selected: pawn_location.name)
    end
  end

  context 'as a buyer user' do
    before do
      log_in_buyer_user
    end

    it 'should display a page without vehicles when no params are passed' do
      visit quotes_promise_estimates_path(filter_type: 'vnum')

      expect(page).to have_content 'Input the last 8 digits of the Vehicle Identification Number (VNUM).'
    end

    it 'should present vehicle filter page' do
      navigate_to_research_page
      expect(page.current_url).to eql quotes_research_url
    end

    context 'when low/high valuations are missing' do
      let!(:pawn) { FactoryGirl.create(:svaa_location) }
      let(:average_valuation) { 123 }
      let!(:presale_vehicle) { FactoryGirl.create(:presale_vehicle, average_valuation: average_valuation, location_initials: pawn.initials) }

      it 'shows average valuation instead' do
        create_location_data
        navigate_to_research_page
        click_on I18n.t('promise_estimates.filters.vnum')
        fill_in 'vnum_suffix', with: presale_vehicle.vnum
        click_on 'Search'

        quote_details = page.find('.quote-l-detail-box').text

        expect(quote_details.scan(average_valuation.to_s).length).to eql 3
      end
    end

    context 'Odometer Units' do
      let!(:pawn) { FactoryGirl.create(:svaa_location, country: 'Canada') }
      let!(:presale_vehicle) { FactoryGirl.create(:presale_vehicle, average_valuation: 12_345.0, location_initials: pawn.initials) }
      it 'shows units based on country of location' do
        navigate_to_research_page

        click_on I18n.t('promise_estimates.filters.vnum')
        fill_in 'vnum_suffix', with: presale_vehicle.vnum
        click_on 'Search'

        distance_units = I18n.translate('distance_units.CA.unit')
        vehicle_quotes = page.find('.quote-l-quote-box').text
        expect(vehicle_quotes).to include(distance_units)
      end
    end
  end
end
