# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Buyer Information View' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let(:agreement_number) { '55555' }
  let!(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: agreement_number, pawn_access_no: '123') }
  let!(:buyer_information) { FactoryGirl.create(:buyer_information, pawn_access_no: pawn_information.pawn_access_no) }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.buyer_information')
  end

  describe 'Index view' do
    it 'displays all buyer information' do
      expect(page).to have_content buyer_information.emails
    end

    context 'Pawn Access# filter' do
      it 'filters buyer information given an associated Pawn Access#' do
        fill_in 'Pawn Access#', with: buyer_information.pawn_access_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content(buyer_information.emails)
      end

      it 'doesnot list buyer information given a non existing Pawn Access#' do
        pawn_access_no = '100686899'
        fill_in 'Pawn Access#', with: pawn_access_no
        click_on I18n.t('forms.filter')
        expect(page).to_not have_content(buyer_information.emails)
      end
    end

    context 'Agreement# filter' do
      it 'filters buyer information given an associated Agreement#' do
        fill_in 'Agreement#', with: agreement_number
        click_on I18n.t('forms.filter')
        expect(page).to have_content(buyer_information.emails)
      end

      it 'doesnot list buyer information given a non-existing Agreement# ' do
        agreement_no = '100555'
        fill_in 'Agreement#:', with: agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to_not have_content(buyer_information.emails)
      end
    end
  end

  describe 'Add a new Buyer Information' do
    let(:new_name) { 'FooBar' }
    let(:new_pawn_access_no) { '1009999' }

    it 'allows admin to add a new buyer information' do
      click_on I18n.t('admin.buyer_information.create')
      fill_in 'Name', with: new_name
      fill_in 'Pawn Access #', with: new_pawn_access_no
      click_on I18n.t('forms.finish')
      expect(page).to have_content new_name
    end

    it 'allows admin to add a new buyer information with multiple emails' do
      emails = 'old@email.com, new@email.com'
      click_on I18n.t('admin.buyer_information.create')
      fill_in 'Email', with: emails
      fill_in 'Pawn Access #', with: new_pawn_access_no
      click_on I18n.t('forms.finish')
      expect(page).to have_content emails
    end
  end

  describe 'Edit Buyer Information' do
    let(:name) { 'Fubar' }

    it 'allows admin to edit a buyer information' do
      click_edit
      fill_in 'Name', with: name
      click_on I18n.t('forms.finish')
      expect(page).to have_content name
    end
  end

  describe 'Delete Buyer Information' do
    it 'allows BuyerInformation to be deleted' do
      click_link I18n.t('forms.delete')
      expect(page).to_not have_content(buyer_information.emails)
    end
  end
end
