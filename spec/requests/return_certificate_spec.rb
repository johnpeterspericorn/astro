# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'

describe 'Return Certificates' do
  include_context 'return invoice steps'

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, seller_paid: true) }

  vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

  context 'logged in as Buyer User', vcr: vcr_options do
    let(:city) { 'NEW YORK' }

    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

      stub_buyer_authorization
      log_in_buyer_user

      return_options = {
        buyer_information: { address_city: city }
      }
      return_vehicle(acura, return_options)

      promise_offer = acura.promise_offers.last
      return_invoice = promise_offer.promise_purchase.return_invoice
      return_invoice.update_attribute(:additional_information, additional_information)

      visit return_certificate_path(return_invoice_id: return_invoice.id)
    end

    after do
      VCR.eject_cassette
    end

    it 'displays return certificate' do
      returned_vehicle_certificate_should_have_return_mileage(acura)
      returned_vehicle_certificate_should_have_return_by_date
      returned_vehicle_certificate_should_have_title_status
    end

    it 'does not double decorate numeric values' do
      expect(page).not_to have_content('$$')
    end

    it 'has a navigation buttons' do
      page_should_have_done_button
      page_should_have_print_button
    end
  end

  context 'logged in as kiosk user', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

      stub_buyer_authorization
      allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([])
      log_in_kiosk_user

      return_vehicle(acura)
    end

    after do
      VCR.eject_cassette
    end

    it 'allows the return certificate to be viewed by the owner of the vehicle' do
      visit root_path
      click_on I18n.t('navigation.return')

      submit_pawn_access_number_form_for_kiosk_user
      click_on I18n.t('return_invoice.link_copy')

      returned_vehicle_certificate_should_have_return_mileage(acura)
      returned_vehicle_certificate_should_have_return_by_date
      returned_vehicle_certificate_should_have_title_status
      returned_vehicle_certificate_should_have_return_instructions
      returned_vehicle_certificate_should_have_return_terms
    end
  end

  context 'logged in as a super user', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

      stub_buyer_authorization
      log_in_super_user
    end

    after do
      VCR.eject_cassette
    end

    it 'has a link back to all filtered vehicles for return as a super user' do
      return_vehicle(acura)
      click_on I18n.t('return_certificate.more_returns_link')
      expect(current_path).to eql return_funnel_filtered_purchases_path
      expect(page).to have_content 'Acura'
    end
  end
end
