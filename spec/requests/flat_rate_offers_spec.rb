# encoding: utf-8
require 'requests_helper'
require_relative './steps/flat_rate_steps'

describe 'Flat Rate Offer Selection' do
  include_context 'flat rate steps'

  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:flat_rate) { FactoryGirl.create(:flat_rate, target_price_360: target_price, country: 'Germany', lv_tier: 3, target_price_lv: 10) }
  let(:buyer_name) { 'Some Body' }
  let(:buyer_info) { BuyerInformation.first }

  let(:agreement_name) { 'Eddie M.' }
  let(:agreementship_name) { "Malfunctioning Eddie's" }
  let(:agreement_email) { 'ed@io-void.com' }
  let(:agreement_cell) { '404-555-5555' }
  let(:notification_email) { 'manager@astroheim.com' }

  let(:target_price) { 234 }
  let(:valid_price) { target_price }
  let(:germany) { Country.find_country_by_name('Germany') }

  before do
    stub_buyer_authorization(
      name: agreement_name,
      email: agreement_email,
      phone: agreement_cell,
      agreementship_name: agreementship_name
    )
    stub_const('SupportedCountry::NAMES', ['United States of America', 'Canada', 'Germany'])
    stub_const('SupportedCountry::DEFAULT', 'United States of America')
    stub_const('SupportedCountry::CA', 'Canada')
  end

  describe 'Confirmation page' do
    context 'super user logged in' do
      context 'palv' do
        before do
          log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
          view_flat_rates_for_palv(pawn_access_number, flat_rate.agreement_no, 'Germany')
        end

        subject { page }

        it { is_expected.to have_content(agreementship_name) }

        it 'should show currency of flat rate country' do
          expect(page).to have_content germany.currency['symbol']
        end
      end

      before do
        log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
        view_flat_rates_for_country(pawn_access_number, flat_rate.agreement_no, 'Germany')
      end

      it 'should show currency of flat rate country' do
        expect(page).to have_content germany.currency['symbol']
      end

      context 'no price is entered' do
        before do
          click_on I18n.t('forms.next')
        end

        subject { page }

        it { is_expected.to have_content(I18n.t('flat_rates.errors.confirm_selection')) }
      end

      context 'when a valid price is entered', js: true do
        before do
          check('include_agreementship')
          click_on I18n.t('forms.next')
        end

        subject { page }

        it { is_expected.to have_content(flat_rate.agreement_no) }
        it { is_expected.to have_content(currency(valid_price, germany)) }
        it { is_expected.to have_content(agreementship_name) }

        context 'when offer is confirmed' do
          let(:user_email) { 'marvnum@mars.gov' }
          let(:batch) { FlatRateOfferBatch.last }

          before do
            fill_in 'user_email', with: user_email
            fill_in 'flat_rate_offer_batch_sales_rep_email', with: user_email

            click_on I18n.t('forms.finish')
          end

          it 'confirms the batch' do
            expect(page).to have_content('AgreementShield 360 Offer confirmed.')

            expect(batch).to be_confirmed
          end

          it 'creates a confirmation email' do
            expect(enqueued_deliveries.count).to eq(1)
          end
        end
      end

      context 'buyer information update', js: true do
        let(:buyer_name) { 'Donald Duck' }
        let(:buyer_information) { BuyerInformation.last }

        before do
          check('include_agreementship')
          click_on I18n.t('forms.next')
        end

        it 'updates the buyers name' do
          fill_in 'Agreement Name', with: buyer_name
          fill_in 'flat_rate_offer_batch_sales_rep_email', with: 'sithu@g.com'
          click_on I18n.t('forms.finish')
          expect(page).to have_content('Admin')
          expect(buyer_information.reload.name).to eq(buyer_name)
        end
      end
    end
  end

  describe 'Re-confirmation' do
    let(:flat_rate_offer_batch) { FactoryGirl.create(:flat_rate_offer_batch) }
    let(:buyer_information) { FactoryGirl.create(:buyer_information, pawn_access_no: flat_rate_offer_batch.pawn_access_no) }
    let(:new_email) { 'newemail@example.com' }

    before do
      log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
      visit edit_flat_rate_offer_batch_path(flat_rate_offer_batch)
    end

    it 'lets the user update the agreement email', js: true do
      fill_in 'buyer_information_emails', with: new_email
      fill_in 'flat_rate_offer_batch_sales_rep_email', with: new_email
      click_on I18n.t('forms.finish')

      page.document.synchronize(1) { buyer_information.reload.emails == new_email }
    end

    it 'does not update with invalid sales rep email', js: true do
      fill_in 'buyer_information_emails', with: new_email
      fill_in 'flat_rate_offer_batch_sales_rep_email', with: 'invalid_email'

      expect do
        click_on I18n.t('forms.finish')
      end.not_to change { buyer_information.reload.emails }
    end
  end

  context 'agreement has already been offered a flat rate', js: true do
    let(:batch) { FlatRateOfferBatch.find_by_pawn_access_no(pawn_access_number) }

    before do
      Timecop.freeze(Time.local(2018, 3, 3, 18))
      log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
      view_flat_rates_for_country(pawn_access_number, flat_rate.agreement_no, 'Germany')
      check('include_agreementship')
      click_on I18n.t('forms.next')
      click_on I18n.t('forms.finish')
    end

    it 'does not allow multiple offers to the mase buyer and mase country' do
      view_flat_rates_for_country(pawn_access_number, flat_rate.agreement_no, 'Germany')

      expect(page).to have_content(I18n.t('flat_rates.errors.already_exists'))
    end

    it 'does not allow multiple offers to be created via a saved link to the flat rate page' do
      visit flat_rates_path(pawn_access_number: pawn_access_number, flat_rates: { agreement_numbers: [flat_rate.agreement_no], country: 'Germany', miles: '360' })

      expect(page).to have_content(I18n.t('flat_rates.errors.already_exists'))
    end
  end

  context 'default expiration date', js: true do
    before do
      log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
      view_flat_rates_for_country(pawn_access_number, flat_rate.agreement_no, 'Germany')
      check('include_agreementship')
      click_on I18n.t('forms.next')
      click_on I18n.t('forms.finish')
    end

    it 'populates default expiration date as 14 days from date of creation' do
      offer_batch = FlatRateOfferBatch.find_by_pawn_access_no(pawn_access_number)
      expect(offer_batch.expiration_date).to eq(14.days.from_now.utc.to_date)
    end
  end
end
