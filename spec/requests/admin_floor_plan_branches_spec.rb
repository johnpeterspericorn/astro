# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Floor Plan Branch Admin' do
  include_context 'admin steps'

  let!(:floor_plan_branch) { FactoryGirl.create(:floor_plan_branch) }

  before do
    login_to_admin_space
    visit admin_floor_plan_company_floor_plan_branches_path(floor_plan_branch.floor_plan_company)
  end

  describe 'Creating a new floor plan branch' do
    let(:name) { 'My New Branch' }
    let(:shipment_name) { 'The things' }
    let(:site_no) { 'NMH123' }

    before do
      Settings.address_verification_enabled = true
    end

    it 'lets admin create a new branch row if address is valid', vcr: { cassette_name: 'easypost/verified_address' } do
      click_on I18n.t('admin.floor_plan_branches.create')

      fill_in 'Name', with: name
      fill_in 'Shipment Name', with: shipment_name
      fill_in 'Site Number', with: site_no

      expect do
        click_on 'Submit'
      end.to change(FloorPlanBranch, :count).by(1)

      expect(page).to have_content(name)
    end

    it 'does not create a new branch row if address is invalid', vcr: { cassette_name: 'easypost/unverified_address' } do
      click_on I18n.t('admin.floor_plan_branches.create')

      fill_in 'Name', with: name
      fill_in 'Shipment Name', with: shipment_name
      fill_in 'Site Number', with: site_no

      expect { click_on 'Submit' }.not_to change { FloorPlanBranch.count }
      expect(page).to have_content I18n.t('admin.floor_plan_branches.address.invalid')
    end
  end

  describe 'Editing a floor plan branch' do
    let(:site_no) { 'NMH123' }
    let(:phone_number) { '1234567890' }

    it 'lets admin update the branches\'s data' do
      click_on floor_plan_branch.name
      fill_in 'Site Number', with: site_no
      find('#floor_plan_branch_active').set(false)
      click_on 'Submit'

      expect(page).to have_content(site_no)
      expect(page).to have_content(false)
    end

    it 'lets admin update the branches\'s phone number' do
      click_on floor_plan_branch.name
      fill_in 'Phone Number', with: phone_number
      click_on 'Submit'

      expect(floor_plan_branch.reload.phone_number).to eq(phone_number)
    end
  end
end
