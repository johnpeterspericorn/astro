# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Floor Plan Branches Selection for Returns' do
  include_context 'admin steps'
  let(:floor_plan_company) { FactoryGirl.create(:floor_plan_company) }
  let(:company_code) { floor_plan_company.company_code }

  before do
    login_to_admin_space
  end

  it 'lists last floor plan branch for agreement' do
    branch_a = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch A')
    branch_b = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch B')
    FactoryGirl.create(:floor_plan_branch_selection, company_code: company_code, site_no: branch_a.site_no)
    FactoryGirl.create(:floor_plan_branch_selection, company_code: company_code, site_no: branch_b.site_no)

    visit admin_floor_plan_branch_selections_path

    expect(page).to have_content branch_b.name
    expect(page).not_to have_content branch_a.name
  end
end
