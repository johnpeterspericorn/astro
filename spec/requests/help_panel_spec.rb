# encoding: utf-8
require 'requests_helper'

describe 'Help Panel' do
  before do
    Settings.learn_about_agreementshield_enabled = true
    log_in_super_user
    visit root_path
  end

  describe 'Email Me section' do
    let(:address) { 'foo@bar.baz' }

    it 'Emails guides to an address' do
      check 'guide2'
      check 'guide3'
      fill_in 'guide_email', with: address

      expect do
        click_on 'Email me'
      end.to change { enqueued_deliveries.count }.by(1)
    end
  end

  describe 'Call Me' do
    it 'sends out contact info to agreementshield' do
      fill_in 'First Name', with: 'Harry'
      fill_in 'Last Name', with: 'Potter'
      fill_in 'Phone', with: '999'
      fill_in 'Email', with: 'harry_potter@hogwarts.com'
      select 'App Download', from: 'problem_type'

      expect do
        click_on 'Call me'
      end.to change { enqueued_deliveries.count }.by(1)
    end
  end
end
