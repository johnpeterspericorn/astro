# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Dashboard Scores view' do
  include_context 'navigation steps'
  include_context 'admin steps'

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
    visit admin_index_path
  end

  describe 'Dashboard Scores Index' do
    let!(:dashboard_score) { FactoryGirl.create(:dashboard_score) }

    before do
      click_on I18n.t('admin.navigation.dashboard_scores')
    end

    it 'lists dashboard scores' do
      expect(page).to have_content dashboard_score.buyer_num
    end

    it 'shows scores with 2 precision' do
      expect(page).to have_content dashboard_score.avg_loss.round(2)
    end

    it 'trim trailing zeros after decimal point' do
      expect(page).to have_content format('%g', dashboard_score.return_score.round(2))
    end

    context 'Agreement# filter' do
      it 'should display dashboard scores given an existing agreement_no' do
        fill_in 'Agreement#', with: dashboard_score.buyer_num
        click_on I18n.t('forms.filter')
        expect(page).to have_content dashboard_score.buyer_num
      end

      it 'should not display dashboard scores given a non existing agreement_no' do
        agreement_no = 44_444
        fill_in 'Agreement#', with: agreement_no
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content agreement_no
      end
    end
  end
end
