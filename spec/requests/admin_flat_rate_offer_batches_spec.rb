# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin flat rate batches view' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:flat_rate_offer_batch) { FactoryGirl.create(:flat_rate_offer_batch, state: 'confirmed', sales_rep_email: 'sales@test.com') }

  before do
    log_in_super_user(admin: true, flat_rate_editor: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    click_on I18n.t('admin.navigation.flat_rate_offer_batches')
  end

  it "shows a batch's offering user email" do
    expect(page).to have_content flat_rate_offer_batch.user_email
  end

  it "shows a batch's approval state" do
    expect(page).to have_content flat_rate_offer_batch.approved?
  end

  describe 'Resetting a batch' do
    it 'sets the batch state to unconfirmed' do
      expect do
        click_on I18n.t('admin.flat_rate_offer_batches.reset')
      end.to change { flat_rate_offer_batch.reload.state }.from('confirmed').to('unconfirmed')
    end

    it 'sends a reset message to the offering user' do
      expect(FlatRateMailer).to receive(:offer_reset) { double(deliver_later: nil) }
      click_on I18n.t('admin.flat_rate_offer_batches.reset')
    end
  end

  describe 'confirming a batch' do
    it 'transitions the batch from unconfirmed to confirmed' do
      click_on I18n.t('admin.flat_rate_offer_batches.reset')
      click_on I18n.t('admin.flat_rate_offer_batches.confirm')
      visit admin_index_path
      click_on I18n.t('admin.navigation.flat_rate_offer_batches')

      expect(page).to have_content('confirmed')
    end
  end

  describe 'adding offers to a batch' do
    let(:new_price) { 4242 }
    let(:new_agreement_no) { '504040' }
    let(:buyer_auth) { double(get_agreement_numbers: [new_agreement_no]).as_null_object }

    before do
      allow(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:new).with(pawn_access_number: flat_rate_offer_batch.pawn_access_no) { buyer_auth }
      FactoryGirl.create(:flat_rate,
                         agreement_no: new_agreement_no
                        )
    end

    it 'adds a new offer with the given agreement # to the batch' do
      click_on I18n.t('admin.flat_rate_offer_batches.add_offer')
      fill_in 'flat_rate_offer_price', with: new_price
      select new_agreement_no, from: 'flat_rate_offer_flat_rate_id'

      expect do
        click_on I18n.t('forms.finish')
      end.to change { flat_rate_offer_batch.flat_rate_offers.count }.by(1)

      visit admin_flat_rate_offer_batches_path

      expect(page).to have_content new_agreement_no
    end
  end

  it 'lets an admin delete a batch' do
    click_on I18n.t('forms.delete')

    expect(page).to_not have_css('tr.flat_rate_offer_batch')
  end

  describe 'editing a batch' do
    it 'updates batch attributes' do
      pawn_access_number = '12345'

      click_edit
      fill_in 'Pawn access no', with: pawn_access_number
      click_on I18n.t('forms.update')

      expect(page).to have_content(pawn_access_number)
    end

    it 'updates expiration date' do
      new_date = Date.current
      click_edit
      select_date(new_date, from: :flat_rate_offer_batch_expiration_date)
      click_on I18n.t('forms.update')
      flat_rate_offer_batch.reload
      expect(flat_rate_offer_batch.expiration_date).to eq(new_date)
    end

    it 'updates buyer information attributes' do
      email = 'jay@bignerdranch.com, foo@bar.com'

      click_edit
      fill_in 'buyer_information_emails', with: email
      click_on I18n.t('forms.update')
      click_edit

      expect(page).to have_css("#buyer_information_emails[value='#{email}']")
    end
  end

  describe 'search' do
    context 'search by pawn_access_no' do
      it 'shows no results if AA# is not present' do
        random_aa = rand(123_123_123)
        fill_in 'filters_pawn_access_no', with: random_aa
        click_on I18n.t('forms.filter')

        expect(page).not_to have_content(flat_rate_offer_batch.pawn_access_no)
      end

      it 'shows results for a AA#' do
        fill_in 'filters_pawn_access_no', with: flat_rate_offer_batch.pawn_access_no
        click_on I18n.t('forms.filter')

        expect(page).to have_content(flat_rate_offer_batch.pawn_access_no)
      end
    end

    context 'search by agreement_no' do
      let(:agreement_no) { '5123456' }
      let(:flat_rate_offer) { FactoryGirl.create(:flat_rate_offer, agreement_no: agreement_no) }
      let(:batch_with_offers) { FactoryGirl.create(:flat_rate_offer_batch_with_offer, flat_rate_offers: [flat_rate_offer]) }

      it 'shows no results if Agreement# is not present' do
        random_agreement = rand(5_342_410)
        fill_in 'filters_agreement_no', with: random_agreement
        click_on I18n.t('forms.filter')

        expect(page).not_to have_content(flat_rate_offer.agreement_no)
      end

      it 'shows results for a Agreement#' do
        fill_in 'filters_agreement_no', with: flat_rate_offer.agreement_no
        click_on I18n.t('forms.filter')

        expect(page).to have_content(flat_rate_offer.agreement_no)
      end
    end
  end
end
