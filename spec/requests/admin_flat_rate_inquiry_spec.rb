# encoding: utf-8
require 'requests_helper'
require_relative './steps/flat_rate_steps'
require_relative './steps/admin_steps'

describe 'Admin Flat Rate Inquiry - Workflow' do
  include_context 'flat rate steps'
  include_context 'admin steps'

  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:flat_rate) { FactoryGirl.create(:flat_rate) }

  before do
    stub_buyer_authorization
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
    visit admin_index_path
    view_flat_rate_agreement_numbers(pawn_access_number)
    find("input[type=checkbox][value='#{flat_rate.agreement_no}']").set(true)
  end

  it 'creates a new FlatRateInquiry' do
    expect do
      click_on I18n.t('forms.flat_rates.view')
    end.to change { FlatRateInquiry.count }.by(1)
  end

  it 'allows admin to view flat rate inquiry' do
    click_on I18n.t('forms.flat_rates.view')

    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
    visit admin_index_path

    click_on I18n.t('admin.navigation.flat_rate_inquiries')
    expect(page).to have_content pawn_access_number
  end

  describe 'filtering flat rate inquiries' do
    let!(:flat_rate_inquiry) { FactoryGirl.create(:flat_rate_inquiry) }

    before do
      log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
      visit admin_index_path
      click_on I18n.t('admin.navigation.flat_rate_inquiries')
    end

    context 'Agreement# filter' do
      it 'displays flat rate inquiries given an existing Agreement#' do
        fill_in 'Agreement#', with: flat_rate_inquiry.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content flat_rate_inquiry.agreement_no
      end

      it 'does not display flat rate inquiries given a non existing Agreement#' do
        fill_in 'Agreement#', with: 1111
        click_on I18n.t('forms.filter')
        expect(page).to_not have_content flat_rate_inquiry.agreement_no
      end
    end

    context 'Pawn Access# filter' do
      it 'displays flat rate inquiries given an existing Pawn Access#' do
        fill_in 'Pawn Access#', with: flat_rate_inquiry.pawn_access_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content flat_rate_inquiry.pawn_access_no
      end

      it 'does not display flat rate inquiries given a non existing Pawn Access#' do
        fill_in 'Pawn Access#', with: 12_345
        click_on I18n.t('forms.filter')
        expect(page).to_not have_content(flat_rate_inquiry.pawn_access_no)
      end
    end

    context 'Date filter' do
      it 'filters flat rate inquiries for a given date range' do
        fill_in 'Date From', with: Time.current.beginning_of_day
        fill_in 'Date To', with: Time.current.end_of_day
        click_on I18n.t('forms.filter')
        expect(page).to have_content flat_rate_inquiry.pawn_access_no
      end

      it 'does not list flat rate inquiries outside date range' do
        yesterday = 1.day.ago
        fill_in 'Date From', with: yesterday.beginning_of_day
        fill_in 'Date To', with: yesterday.end_of_day
        click_on I18n.t('forms.filter')
        expect(page).to_not have_content flat_rate_inquiry.pawn_access_no
      end
    end
  end
end
