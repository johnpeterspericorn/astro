# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - New Product Fee Management' do
  include_context 'admin steps'
  describe 'pawn location management workflow' do
    let!(:new_product_fee) { FactoryGirl.create(:new_product_fee) }
    before do
      visit_as_feature_flag_editor
      click_on I18n.t('new_product_fee.admin.navigation')
    end

    describe 'New Product Fees Index' do
      it 'lists New Product Fees' do
        expect(page).to have_content new_product_fee.agreement_no
      end
    end

    describe 'New Product Fees Edit' do
      it 'updates New Product Fees attributes' do
        click_on I18n.t('forms.edit')
        new_agreement_no = '2222'
        fill_in 'Agreement No', with: new_agreement_no
        click_on I18n.t('forms.update')
        expect(page).to have_content(new_agreement_no)
      end
    end

    describe 'Add New Product Fees' do
      before do
        click_on 'New Product Fee'
      end

      it 'creates a New Product Fees' do
        new_agreement_no = 55_555
        fill_in 'Agreement No', with: new_agreement_no
        fill_in 'Seller No', with: 100
        fill_in 'Timeframe', with: 100
        fill_in 'Count', with: 10
        fill_in 'Fee', with: 200
        click_on 'Create New product fee'
        expect(page).to have_content(new_agreement_no)
      end
    end

    describe 'New Product Fees Delete' do
      it 'delete New Product Fee' do
        click_on I18n.t('forms.delete')
        expect(page).not_to have_content(new_product_fee.timeframe)
      end
    end
  end
end
