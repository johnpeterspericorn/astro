# encoding: utf-8
require 'spec_helper'

describe '/coupons' do
  before do
    sign_in_with_user
  end

  describe 'GET #show' do
    let(:coupon) { FactoryGirl.create(:coupon) }

    it 'identifies coupons by code' do
      get "/coupons/#{coupon.code}.json"

      expect(response).to be_success
    end

    it 'includes amount and code in response body' do
      amount = 12.3
      coupon.update_attributes!(amount: amount)

      get "/coupons/#{coupon.code}.json"

      json = JSON.parse(response.body)
      coupon_json = json.fetch('coupon')

      expect(coupon_json.fetch('amount')).to eq(amount)
      expect(coupon_json.fetch('code')).to eq(coupon.code)
    end

    it 'responds 404 when coupon code cannot be found' do
      get '/coupons/nonope.json'

      expect(response).to be_not_found
    end

    it 'responds 410 when coupon has been used' do
      coupon.update_attributes!(promise_purchase_id: 1)

      get "/coupons/#{coupon.code}.json"

      expect(response.status).to eq(410)
    end
  end
end
