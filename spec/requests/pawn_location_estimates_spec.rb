# encoding: utf-8
require 'requests_helper'
require_relative './steps/pawn_location_steps'
require_relative './steps/navigation_steps'

describe 'Pawn Location Estimates Workflow' do
  include_context 'navigation steps'
  include_context 'pawn location steps'

  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let!(:malibu) { FactoryGirl.create(:presale_vehicle, model: 'MALIBU', lane_no: 1, run_no: 1, location_initials: pawn.initials) }
  let!(:pilot)  { FactoryGirl.create(:presale_vehicle, model: 'PILOT', lane_no: 2, run_no: 2, location_initials: 'SVAA') }
  let(:agreement_name) { 'Fooquux' }
  let(:agreements) do
    {
      '500000' => agreement_name
    }
  end

  before do
    allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers_to_names) { agreements }

    create_location_data
    log_in_buyer_user
    navigate_to_research_page

    select_agreement(agreement_name)
    browse_at_pawn

    select_svaa_location
    select_lane 1
    submit_lane_form
  end

  it 'should filter on location and lane #s' do
    within('#promise_estimates_view') do
      expect(page).to     have_content malibu.model.titleize
      expect(page).not_to have_content pilot.model.titleize
    end
  end

  it 'should show a printer view for filtering on location and lane #s', js: true do
    within('#promise_estimates_view') do
      expect(page).to have_content 'Malibu'
    end
  end

  context 'ineligible vehicles' do
    let!(:ineligible_vehicle) { FactoryGirl.create(:presale_vehicle, lane_no: 1, location_initials: 'SVAA', ineligibility_condition: ineligibility_condition) }
    let(:vnum) { ineligible_vehicle.vnum }
    let(:ineligibility_condition) { 5 }

    before do
      navigate_to_research_page

      select_agreement(agreement_name)
      browse_at_pawn

      select_svaa_location
      select_lane ineligible_vehicle.lane_no
      submit_lane_form
    end

    it 'should be displayed' do
      expect(page).to have_content vnum
    end

    it 'should display ineligibilty reason' do
      ineligible_reason = I18n.t('presale_vehicles.ineligibility_conditions')[ineligibility_condition]
      expect(page).to have_content ineligible_reason
    end
  end

  it 'should allow a user to return to the landing page' do
    click_done

    expect(page.current_path).to eql root_path
  end
end
