# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'

describe 'Disbursement Request' do
  include_context 'return invoice steps'

  let(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000) }

  let(:promise_purchase) { acura.promise_offers.last.promise_purchase }
  let(:return_invoice) { promise_purchase.return_invoice }

  before do
    VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

    stub_buyer_authorization
    log_in_super_user
    return_vehicle(acura)
  end

  after do
    VCR.eject_cassette
  end

  describe 'disbursement form', vcr: { cassette_name: 'successful_easypost_shipment' } do
    it 'warns that vehicle is unpaid' do
      visit return_invoice_disbursement_request_path(return_invoice)

      expect(page).to have_content(I18n.t('return_invoice.disbursement.unpaid'))
    end
  end
end
