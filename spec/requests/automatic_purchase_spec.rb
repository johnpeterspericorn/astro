# encoding: utf-8
require 'requests_helper'
# This class is executed by a rake task on a schedule. We still want to make
# sure it plays nice with a "full-stack". This is that.
describe 'Automated Promise Purchases' do
  let(:days)    { 7 }
  let(:miles)   { 250 }
  let(:email)   { 'test@example.com' }
  let(:base_attributes) do
    { preselected_promise_days:  days,
      preselected_promise_miles: miles,
      automatic_purchase_email:    email }
  end
  let(:automatic_purchase_attributes) do
    base_attributes.merge(automatic_purchase: true)
  end
  let(:seller_paid_attributes) do
    base_attributes.merge(seller_paid: true)
  end
  let(:ineligible_automatic_purchase_attributes) do
    automatic_purchase_attributes.merge(ineligibility_condition: 1)
  end

  let(:offer) { PromiseOffer.first }
  let(:purchase) { PromisePurchase.first }
  let(:vehicle) { SoldVehicle.first }
  let!(:pawn) { FactoryGirl.create(:svaa_location) }

  before do
    FactoryGirl.create(:sold_vehicle)
  end

  context 'automatic purchase' do
    before do
      vehicle.update_attributes(automatic_purchase_attributes)
    end

    context 'when a flat rate offer for the agreement exists' do
      let!(:flat_rate_offer_batch) { FactoryGirl.create(:flat_rate_offer_batch_with_offer) }
      let(:flat_rate_offer) { flat_rate_offer_batch.flat_rate_offers.first }

      before do
        flat_rate_offer.update_attributes!(agreement_no: vehicle.agreement_no)
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
      end

      it 'overrides the flat rate price' do
        expect(offer.promise_price).to_not eq(flat_rate_offer.price)
      end
    end

    specify 'no offers exist before automatic purchases are processed' do
      expect(offer).to be_nil
    end

    specify 'no purchases exist before automatic purchases are processed' do
      expect(purchase).to be_nil
    end

    specify 'no queued jobs exist before automatic purchase processing' do
      expect(Delayed::Job.count).to eq(0)
    end

    specify 'vehicle is not processed' do
      expect(vehicle.automatic_purchase_processed).to be_falsey
    end

    context 'automatic purchase processed' do
      before do
        Settings.charges_enabled = true
        allow_any_instance_of(Astroheim::Charge).to receive(:submit!)
        allow_any_instance_of(Astroheim::Sows::Client).to receive(:purchase)
      end

      it 'records an offer for the vehicle' do
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase

        expect(PromiseOffer.count).to eq(1)
        expect(offer.days_selected).to eq(days)
        expect(offer.miles_selected).to eq(miles)
        expect(offer.vehicle_information.vnum).to eq(vehicle.vnum)
      end

      it 'records a purchase for the vehicle' do
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase

        expect(PromisePurchase.count).to eq(1)
        expect(purchase.miles_selected).to eq(miles)
        expect(purchase.transaction_id).to_not be_nil
      end

      it 'queues a job for the charge' do
        expect(PromiseChargeJob).to receive(:enqueue)

        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
      end

      it 'marks the vehicle as processed' do
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase

        expect(vehicle.reload.automatic_purchase_processed).to be_truthy
      end

      it 'sends a confirmation' do
        expect do
          AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
        end.to change { enqueued_deliveries.count }.by(1)
      end

      it 'sets offer channel as automatic' do
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase

        expect(PromisePurchase.first.channel).to eq(PromiseChannel::AUTOMATIC)
      end

      it 'does not blow up when previous, unpurchased offer exists for vehicle' do
        # This test covers a longstanding bug https://www.pivotaltracker.com/story/show/90545434
        # At some point in the future, this test can be removed. Iagreemently we
        # take the time to stand up proper unit tests against all these
        # objects to cover such edge cases.
        PromiseOfferRecorder.record_offer(vehicle)
        vehicle.update_attributes!(updated_at: 1.minute.from_now)
        AutomatedPromisePurchaser.record_purchase(vehicle)
      end

      context 'in the future another automatic purchase for the mase vehicle' do
        let(:new_run_no) { 13 }
        let(:new_lane_no) { 27 }
        let(:new_work_order_number) { 66_757_675 }
        let(:new_email) { 'somenew@email.com' }
        let(:second_automatic_purchase_attributes) do
          { automatic_purchase:           true,
            automatic_purchase_processed: false,
            preselected_promise_days:   days,
            preselected_promise_miles:  miles,
            automatic_purchase_email:     new_email,
            run_no:     new_run_no,
            lane_no:    new_lane_no,
            universal_no: "svaa201850#{new_lane_no}00#{new_run_no}",
            work_order_number: new_work_order_number
          }
        end
        it 'creates a new promise offer for a vehicle re-sold at a new pawn' do
          # We are yet to figure out the case of re-selling the vehicle at a new pawn
          # For the time being we assume that the resold vehicle will have a different work_order-number
          # Refer story #132961311
          Timecop.travel(2.days.from_now) do
            new_vehicle = SoldVehicle.where(vnum: vehicle.vnum).first.dup
            new_vehicle.update_attributes(second_automatic_purchase_attributes)
            expect(new_vehicle.reload.automatic_purchase_processed).to be_falsey

            expect do
              AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
            end.to change { enqueued_deliveries.count }.by(2)

            expect(PromiseOffer.count).to eq(2)
            new_offer = PromiseOffer.by_vnum(vehicle.vnum).order('promise_offers.created_at DESC').first
            expect(new_offer.days_selected).to eq(days)
            expect(new_offer.miles_selected).to eq(miles)
            expect(new_offer.vehicle_information.vnum).to eq(vehicle.vnum)

            expect(PromisePurchase.count).to eq(2)
            new_purchase = new_offer.promise_purchase
            expect(new_purchase.miles_selected).to eq(miles)
            expect(new_purchase.transaction_id).to_not be_nil

            expect(new_vehicle.reload.automatic_purchase_processed).to be_truthy
          end
        end
      end
    end
  end

  context 'seller paid' do
    before do
      Settings.charges_enabled = true
      vehicle.update_attributes(seller_paid_attributes)
    end

    specify 'no offers exist before automatic purchases are processed' do
      expect(offer).to be_nil
    end

    specify 'no purchases exist before automatic purchases are processed' do
      expect(purchase).to be_nil
    end

    specify 'vehicle is not processed' do
      expect(vehicle.automatic_purchase_processed).to be_falsey
    end

    context 'automatic purchase processed' do
      before do
        Delayed::Worker.delay_jobs = false
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
      end

      it 'records an offer for the vehicle' do
        expect(PromiseOffer.count).to eq(1)
        expect(offer.days_selected).to eq(days)
        expect(offer.miles_selected).to eq(miles)
        expect(offer.vehicle_information.vnum).to eq(vehicle.vnum)
      end

      it 'records a purchase for the vehicle' do
        expect(PromisePurchase.count).to eq(1)
        expect(purchase.miles_selected).to eq(miles)
        expect(purchase.transaction_id).to_not be_nil
      end

      it 'does not queue job for charge' do
        expect(Delayed::Job.count).to eq(0)
      end

      it 'marks the vehicle as processed' do
        expect(vehicle.reload.automatic_purchase_processed).to be_truthy
      end
    end
  end

  context 'when the offer requires approval' do
    before do
      Settings.charges_enabled = true
      vehicle.update_attributes!(automatic_purchase_attributes.merge(pending_reason_code: PendingConditions::APPROVAL_REQUIRED))
    end

    it 'does not queue job for charge' do
      expect(PromiseChargeJob).not_to receive(:enqueue)

      AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
    end
  end

  context 'when the vehicle has already had a promise purchased' do
    before do
      vehicle.update_attributes(automatic_purchase_attributes)
      allow_any_instance_of(SoldVehicle).to receive(:purchase_exists?) { true }
    end

    it 'does not record any offers' do
      expect do
        AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
      end.to_not change(PromiseOffer, :count)
    end
  end

  context 'ineligible automatic purchase' do
    before do
      vehicle.update_attributes(ineligible_automatic_purchase_attributes)
      AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
    end

    it 'does not record any offers' do
      expect(PromiseOffer.count).to be_zero
    end

    it 'does not record any purchases' do
      expect(PromisePurchase.count).to be_zero
    end

    it 'notifies offer email that purchase is ineligible' do
      sold_vehicle = SoldVehicle.last
      expect(enqueued_deliveries.count).to eq(1)
      mailer, method, _, active_job_object = last_enqueued_delivery[:args]
      expect(mailer).to eq('PromisePurchaseMailer')
      expect(method).to eq('notify_ineligible')
      expect(active_job_object).to eq('_aj_globalid' => sold_vehicle.to_gid.to_s)
    end

    it 'does mark the vehicle as processed' do
      expect(vehicle.reload.automatic_purchase_processed).to eq(true)
    end
  end

  context 'invalid sold vehicle which is ineligible automatic purchase' do
    before do
      vehicle.assign_attributes({
        automatic_purchase: true,
        preselected_promise_miles: nil # to make the record invalid
      }.merge(ineligible_automatic_purchase_attributes))
      vehicle.save(validate: false)
    end

    it 'does mark the vehicle as processed' do
      AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
      expect(vehicle.reload.automatic_purchase_processed).to eq(true)
    end
  end
end
