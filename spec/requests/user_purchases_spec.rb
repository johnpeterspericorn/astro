# encoding: utf-8
require 'requests_helper'

describe 'User Purchases' do
  describe 'index view' do
    let!(:user) { log_in_buyer_user }

    context 'a purchase exists for a user' do
      it 'displays the purchase' do
        purchase = FactoryGirl.create(:promise_purchase, pawn_access_no: user.pawn_access_number)

        visit user_purchases_path

        expect(page).to have_content(purchase.vnum)
      end
    end

    context 'a purchase exists for a different user' do
      it 'does not display the purchase' do
        purchase = FactoryGirl.create(:promise_purchase, pawn_access_no: '1234')

        visit user_purchases_path

        expect(page).not_to have_content(purchase.vnum)
      end
    end
  end
end
