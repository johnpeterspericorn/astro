# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Pending Purchases View' do
  include_context 'admin steps'

  before do
    login_to_admin_space
  end

  context 'when not in failover mode' do
    before do
      click_on I18n.t('admin.navigation.failover')
    end

    describe 'enabling failover' do
      it 'flips the Settings value' do
        expect do
          click_on I18n.t('admin.failovers.enable')
        end.to change(Failover, :active?).to(true)
      end

      it 'creates a new Failover record' do
        expect do
          click_on I18n.t('admin.failovers.enable')
        end.to change(Failover, :count).by(1)
        expect(Failover.last.began_at).to be_within(1.second).of Time.current
      end
    end
  end

  context 'when in failover mode' do
    before do
      click_on I18n.t('admin.navigation.failover')
      click_on I18n.t('admin.failovers.enable')
    end

    describe 'disabling failover' do
      it 'flips the Settings value' do
        expect do
          click_on I18n.t('admin.failovers.disable')
        end.to change(Failover, :active?).to(false)
        expect(Failover.last.ended_at).to be_within(1.second).of Time.current
      end
    end
  end
end
