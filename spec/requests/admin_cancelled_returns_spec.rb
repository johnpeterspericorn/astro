# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Cancelled Returns' do
  include_context 'admin steps'

  let!(:cancelling_user) { FactoryGirl.create(:super_user) }
  let!(:cancelled_return) do
    FactoryGirl.create(:return_invoice, cancelled_at: 1.minute.ago, cancelling_user: cancelling_user)
  end

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.cancelled_returns')
  end

  describe 'Index view' do
    let(:nonexistent_vnum) { 'ABCDEF123XYZ' }
    let(:nonexistent_vra) { 98_765_432 }

    it 'displays cancelled returns' do
      expect(page).to have_content cancelled_return.id
    end

    context 'VNUM filter' do
      it 'should display cancelled returns given an existing VNUM' do
        fill_in 'VNUM', with: cancelled_return.vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_content cancelled_return.vnum
      end

      it 'should not display cancelled returns given a non existing VNUM' do
        expect(page).to have_content(cancelled_return.vnum)
        fill_in 'VNUM', with: nonexistent_vnum
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content(cancelled_return.vnum)
      end
    end

    context 'VRA# filter' do
      it 'should display cancelled returns given an existing VRA#' do
        fill_in 'VRA', with: cancelled_return.id
        click_on I18n.t('forms.filter')
        expect(page).to have_content cancelled_return.id
      end

      it 'should not display cancelled returns given a non-existing VRA# ' do
        expect(page).to have_content cancelled_return.id
        fill_in 'VRA', with: nonexistent_vra
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content cancelled_return.id
      end
    end

    context 'when offer is deleted' do
      it 'displays cancelled return without link to offer' do
        cancelled_return.promise_offer.destroy
        allow_any_instance_of(ReturnInvoice).to receive(:expiration_date).and_return(20.days.from_now)

        visit admin_cancelled_returns_path

        expect(page).to have_content(cancelled_return.id)
        expect(page).not_to have_css('[rel~=edit_promise_offer]')
      end
    end

    context 'when promise purchase does not exist' do
      it 'renders page with no errors' do
        cancelled_return.promise_purchase.destroy

        visit admin_cancelled_returns_path
        expect(page).to have_content('Reinstate Return')
      end
    end
  end
end
