# encoding: utf-8
require 'requests_helper'
require_relative './steps/presale_vehicle_steps'

describe 'Presale Vehicle Estimates Workflow' do
  include_context 'presale vehicle steps'

  before do
    log_in_buyer_user
    navigate_to_research_page
  end

  describe 'VNUM filter' do
    let!(:pawn) { FactoryGirl.create(:svaa_location) }
    let!(:presale_vehicle) { FactoryGirl.create(:presale_vehicle, location_initials: pawn.initials) }
    let(:suffix) { presale_vehicle.vnum[-8..-1] }

    before do
      Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PAGE.times { FactoryGirl.create(:presale_vehicle, vnum: (SecureRandom.hex + suffix), location_initials: pawn.initials) }
      browse_presale_vehicles_by_vnum
      fill_in 'vnum_suffix', with: suffix
      submit_presale_form
    end

    it 'should have the correct vehicle present' do
      expect(page).to have_content presale_vehicle.vnum
    end
  end
end
