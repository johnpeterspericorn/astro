# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin flat rate offers view' do
  include_context 'admin steps'

  let!(:flat_rate_offer) { FactoryGirl.create(:flat_rate_offer) }

  before do
    log_in_super_user(admin: true, flat_rate_editor: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    click_on I18n.t('admin.navigation.flat_rate_offers')
  end

  it 'lets an admin delete an offer' do
    click_on I18n.t('forms.delete')
    expect(page).to_not have_css('tr.flat-rate-offer')
  end
end
