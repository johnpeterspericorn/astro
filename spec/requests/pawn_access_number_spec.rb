# encoding: utf-8
require 'requests_helper'
require_relative './steps/pawn_access_number_steps'
require_relative './steps/pawn_access_number_swipe_steps'

describe 'Pawn Access Number Workflow' do
  include_context 'pawn access number steps'
  include_context 'pawn access number swipe steps'

  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let (:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let (:agreement_name) { 'A Great Agreement' }

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA') }
  let!(:volvo) { FactoryGirl.create(:sold_vehicle, make: 'VOLVO', pawn_access_no: '0000000') }

  before do
    stub_buyer_authorization
  end

  context 'as a kiosk user' do
    before do
      log_in_kiosk_user
      visit_filter_page
      click_pawn_access
    end

    it 'should show vehicles for the given AA#' do
      view_vehicles_by_pawn_access_number_as_kiosk_user

      page_should_have_agreement_name agreement_name

      page_should_have_vehicle_makes(acura)
      page_should_not_have_vehicle_makes(volvo)
    end

    it 'should handle bad track data with an error message', js: true do
      enter_bad_pawn_swipe
      expect(page).to have_text 'Please swipe your Pawn Access card again.'
    end
  end

  context 'as a buyer user' do
    before do
      log_in_buyer_user
    end

    it 'should skip the filter page and display vehicles' do
      visit_filter_page

      page_should_have_vehicle_makes(acura)
      page_should_not_have_vehicle_makes(volvo)
    end
  end
end
