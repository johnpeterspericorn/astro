# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Pending Purchases View' do
  include_context 'admin steps'

  let!(:offer) { FactoryGirl.create(:promise_offer, promise_status: 'charge_failed') }

  before do
    login_as_finance_admin_viewer
    click_on I18n.t('admin.navigation.accounts_receivable_report_offers')
  end

  it 'shows offers that will appear on the AR report after the interval' do
    expect(page).to have_content offer.vnum
  end
end
