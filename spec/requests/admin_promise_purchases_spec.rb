# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Promise Purchases View' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:pawn_information) { FactoryGirl.create(:pawn_information) }
  let(:ca_pawn_location) { FactoryGirl.create(:ca_location) }
  let(:ca_pawn_information) { FactoryGirl.create(:pawn_information, pawn_location: ca_pawn_location) }
  let!(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information, emails: 'foo@bar.com') }
  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.promise_purchases')
  end

  describe 'Index view' do
    it 'displays purchased promises' do
      expect(page).to have_content promise_purchase.vnum
    end

    it 'displays distance selected in units based on country' do
      promise_offer.update_attributes(pawn_information: ca_pawn_information)
      visit admin_promise_purchases_path
      expect(page).to have_content 'Km'
      expect(page).not_to have_content 'Miles'
    end
  end

  describe 'Expire Promise' do
    context 'login as promise_offer_as_editor' do
      before do
        visit_promise_offer_as_editor
        click_on I18n.t('admin.navigation.promise_purchases')
      end

      it 'show expire promise link' do
        expect(page).to have_link I18n.t('promise_purchases.expire')
      end

      it 'expire promise after clicking in expire promise link' do
        first(:link, text: /\AExpire Promise\z/).click
        expect(page).to have_content I18n.t('promise_purchases.expired')
      end
    end

    context 'login as admin user without promise_offer_as_editor' do
      it 'show expire promise link' do
        expect(page).to have_link I18n.t('promise_purchases.expire')
      end

      it 'will not expire promise after clicking in expire promise link' do
        first(:link, text: /\AExpire Promise\z/).click
        visit admin_promise_purchases_path
        expect(page).to have_link I18n.t('promise_purchases.expire')
      end
    end
  end

  describe 'UnExpire Promise' do
    context 'login as promise_offer_as_editor' do
      before do
        promise_purchase.update_attributes(expire_promise: true)
        visit_promise_offer_as_editor
        click_on I18n.t('admin.navigation.promise_purchases')
      end

      it 'show unexpire link' do
        expect(page).to have_link I18n.t('promise_purchases.unexpire')
      end

      it 'unexpire promise after clicking in unexpire promise link' do
        first(:link, text: /\A#{I18n.t('promise_purchases.unexpire')}\z/).click
        expect(page).not_to have_content I18n.t('promise_purchases.expired').to_s
      end
    end

    context 'login as admin user without promise_offer_as_editor' do
      before do
        promise_purchase.update_attributes(expire_promise: true)
        visit admin_promise_purchases_path
      end

      it 'show unexpire link' do
        expect(page).to have_link I18n.t('promise_purchases.unexpire')
      end

      it 'will not unexpire promise after clicking in unexpire promise link' do
        first(:link, text: /\A#{I18n.t('promise_purchases.unexpire')}\z/).click
        visit admin_promise_purchases_path
        expect(page).to have_link I18n.t('promise_purchases.unexpire')
      end
    end
  end

  describe 'editing a purchase' do
    it 'updates the promised at time' do
      kind = PromisePurchase::MARKET_PROTECT
      last_week = 1.week.ago

      click_edit
      fill_in 'purchased_promise_form_promised_at', with: last_week
      select kind, from: 'purchased_promise_form_kind'
      click_on 'Submit'

      expect(page).to have_content(last_week)
      expect(page).to have_content(kind)
    end

    it 'updates the vehicle information' do
      make = 'Acura'
      model = 'Volvo'
      year = 2010
      odometer_reading = 500

      click_edit
      fill_in 'purchased_promise_form_make', with: make
      fill_in 'purchased_promise_form_year', with: year
      fill_in 'purchased_promise_form_model', with: model
      fill_in 'purchased_promise_form_odometer_reading', with: odometer_reading
      click_on 'Submit'

      expect(page).to have_content(make)
      expect(page).to have_content(year)
      expect(page).to have_content(model)
      expect(page).to have_content(odometer_reading)
    end

    it 'update partner field' do
      partner = 'mas-partner'

      click_on I18n.t('forms.edit')
      fill_in 'purchased_promise_form_partner', with: partner
      click_on 'Submit'

      promise_purchase.reload
      expect(promise_purchase.partner).to eq(partner)
    end

    it 'updates pawn information' do
      buy_fee = 20.0
      vehicle_purchase_price = 200
      pawn_access_no = '1111111'
      agreement_no = '55555'

      click_edit
      fill_in 'purchased_promise_form_buy_fee', with: buy_fee
      fill_in 'purchased_promise_form_vehicle_purchase_price', with: vehicle_purchase_price
      fill_in 'purchased_promise_form_pawn_access_no', with: pawn_access_no
      fill_in 'purchased_promise_form_agreement_no', with: agreement_no
      click_on 'Submit'

      pawn_information.reload

      expect(page).to have_content(vehicle_purchase_price)
      expect(pawn_information.buy_fee).to eq(buy_fee)
      expect(pawn_information.pawn_access_no).to eq(pawn_access_no)
      expect(pawn_information.agreement_no).to eq(agreement_no)
    end

    it 'updates promise offer' do
      email = 'abc@xyz.com, foo@bar.com'
      transport_reimbursement = 40
      days_selected = 14
      miles_selected = 500

      click_edit
      fill_in 'purchased_promise_form_emails', with: email
      fill_in 'purchased_promise_form_transport_reimbursement', with: transport_reimbursement
      select days_selected.to_s, from: 'purchased_promise_form_days_selected'
      select miles_selected.to_s, from: 'purchased_promise_form_miles_selected'
      click_on 'Submit'

      promise_offer.reload

      expect(promise_offer.emails).to eq(email)
      expect(promise_offer.days_selected).to eq(days_selected)
      expect(promise_offer.miles_selected).to eq(miles_selected)
      expect(promise_offer.transport_reimbursement).to eq(transport_reimbursement)
    end

    context 'a purchase has been returned' do
      before do
        promise_purchase.update_attributes(promised_at: 1.day.ago)
        FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase)
      end

      it 'doesn\'t blow up', regression: true do
        expect { click_edit }.to_not raise_error
      end
    end

    context 'Successful purchase update' do
      before do
        click_edit
        click_on 'Submit'
      end

      it 'doesn\'t list any errors' do
        expect(page).to_not have_content('Error updating Promise purchase')
      end
    end

    context 'Failed purchase update' do
      before do
        click_edit
        fill_in 'purchased_promise_form_make', with: nil
        click_on 'Submit'
      end

      it 'lists validation errors' do
        expect(page).to have_content("Error updating Promise purchase: Make can't be blank")
      end
    end
  end

  describe 'updating a MARKET_PROTECT promise_purchase' do
    it 'sends a promise refresh confirmation message when the purchase is updated' do
      email = 'cats@dogs.com'
      promise_purchase.update_attributes(kind: PromisePurchase::MARKET_PROTECT)

      click_edit
      fill_in 'purchased_promise_form_emails', with: email
      click_on 'Submit'

      expect(last_enqueued_delivery[:job]).to eq(ActionMailer::DeliveryJob)
    end
  end

  describe 'version history', versioning: true do
    it 'shows audit link' do
      expect(page).to have_content('Audit')
    end

    it 'shows version history header' do
      click_on 'Audit'
      expect(page).to have_content(I18n.t('admin.promise_purchase_versions.index.header_copy'))
    end

    it 'shows version history of promise_purchase' do
      promise_purchase.update_attributes(additional_days: 1)
      click_on 'Audit'
      expect(page).to have_content('set additional_days from 0 to 1')
    end

    it 'shows version history of vehicle information' do
      vehicle_information = promise_purchase.promise_offer.vehicle_information
      vehicle_information.update_attributes(make: 'AUDI')
      click_on 'Audit'
      expect(page).to have_content('set make from FACTROID to AUDI')
    end

    it 'shows version history of pawn information' do
      pawn_information = promise_purchase.promise_offer.pawn_information
      pawn_information.update_attributes(vehicle_purchase_price: 100)
      click_on 'Audit'
      expect(page).to have_content('set vehicle_purchase_price from 10000.0 to 100.0')
    end

    it 'shows version history of promise offer' do
      promise_offer = promise_purchase.promise_offer
      promise_offer.update_attributes(emails: 'a@example.com')
      click_on 'Audit'
      expect(page).to have_content('set emails from foo@bar.com to a@example.com')
    end
  end
end
