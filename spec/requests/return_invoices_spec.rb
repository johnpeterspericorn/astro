# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'

describe 'Return Invoice Management' do
  include_context 'return invoice steps'

  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, seller_paid: true) }

  let!(:other_location) do
    PawnLocation.create!(initials: 'ABC', name: 'A Big City')
  end

  context 'editing of return invoice' do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      log_in_super_user
      return_vehicle(acura)
    end

    before do
      VCR.eject_cassette
    end

    it 'can be edited', vcr: { cassette_name: 'successful_easypost_shipment_and_retrieval' } do
      view_returns_page
      click_on I18n.t('forms.returns.edit')
      page.find('select#return_invoice_pawn_location_id').select(other_location.name)
      click_on I18n.t('forms.update')

      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      return_invoice = acura.promise_offers.last.promise_purchase.return_invoice
      visit return_certificate_path(return_invoice_id: return_invoice.id)
      VCR.eject_cassette
      returned_vehicle_certificate_should_have_return_terms
      expect(page).to have_content(other_location.name)
    end
  end

  context 'sending email after editing vra' do
    let!(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: '510000', pawn_access_no: '100686866') }
    let!(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, pawn_information: pawn_information) }
    let!(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer) }
    let!(:return_location) { FactoryGirl.create(:pawn_location, name: 'BBB', initials: 'BBBB') }
    let!(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }

    context 'super user logged in' do
      before do
        log_in_super_user
      end

      specify 'email gets sent when return location changes' do
        view_returns_page
        click_on I18n.t('forms.returns.edit')
        page.find('select#return_invoice_pawn_location_id').select(other_location.name)
        expect(page.find('#send_return_confirmation')).to be_truthy
        expect do
          click_on I18n.t('forms.update')
        end.to change { enqueued_deliveries.count }.by(1)
      end
    end
  end
end
