# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin flat rate offers view' do
  include_context 'navigation steps'
  include_context 'admin steps'

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true, flat_rate_editor: true)
    visit admin_index_path
  end

  describe 'Flat Rates Index' do
    let!(:flat_rate) { FactoryGirl.create(:flat_rate) }

    before do
      click_on I18n.t('admin.navigation.flat_rates')
    end

    it 'lists flat rates' do
      expect(page).to have_content flat_rate.agreement_no
    end

    it 'shows the country applicable for flat rate' do
      expect(page).to have_content flat_rate.country
    end

    context 'Agreement# filter' do
      it 'should display flat rates given an existing agreement_no' do
        fill_in 'Agreement#', with: flat_rate.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content flat_rate.agreement_no
      end

      it 'should not display flat rates given a non existing agreement_no' do
        agreement_no = 44_444
        fill_in 'Agreement#', with: agreement_no
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content agreement_no
      end
    end

    describe 'Flat Rate Edit' do
      before do
        click_edit
      end

      it 'updates flat rate attributes' do
        new_agreement_no = '2222'
        fill_in 'Agreement no', with: new_agreement_no
        click_on I18n.t('forms.update')
        expect(page).to have_content(new_agreement_no)
      end
    end
  end

  describe 'Add FlatRate' do
    before do
      click_on I18n.t('admin.navigation.flat_rates')
      click_on I18n.t('admin.flat_rates.create')
    end

    it 'creates a new flat rate' do
      new_agreement_no = 55_555
      fill_in 'Agreement no', with: new_agreement_no
      fill_in 'Inspection adjustment price', with: 100
      fill_in 'Days adjustment price', with: 100
      fill_in 'Miles adjustment price', with: 100

      fill_in 'Target price 360', with: 200

      fill_in 'Target price 360 psi', with: 200

      fill_in 'Target price 500', with: 200

      fill_in 'Target price 500 psi', with: 200

      click_on 'Create Flat rate'
      expect(page).to have_content(new_agreement_no)
    end
  end
end
