# encoding: utf-8
require 'spec_helper'

describe '/api/promise_purchases/{:id}/promise_offer' do
  let(:user) { FactoryGirl.create(:api_user) }
  let(:base_params) do
    { auth_token: user.authentication_token, format: :json }
  end

  describe 'PUT #update' do
    let(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
    let(:promise_offer) { promise_purchase.promise_offer }
    let(:buyer_number) { '12345' }
    let(:seller_number) { '54321' }
    let(:pawn_access_no) { '87654321' }
    let(:new_vehicle_price) { 12_345.5 }
    let(:buy_fee) { 123.4 }
    let(:days_selected) { 75 }
    let(:miles_selected) { 250 }
    let(:update_params) do
      {
        promise_offer_id: promise_offer.id,
        vnum: promise_offer.vnum,
        agreement_no: buyer_number,
        pawn_access_no: pawn_access_no,
        seller_no: seller_number,
        vehicle_purchase_price: new_vehicle_price,
        buy_fee: buy_fee,
        miles_selected: miles_selected,
        days_selected: days_selected
      }
    end

    it 'updates pawn information' do
      params = base_params.merge(update_params)
      pawn_information = promise_offer.pawn_information

      put "/api/promise_purchases/#{promise_purchase.id}/promise_offer", params

      promise_offer.reload
      pawn_information.reload
      expect(response).to be_success
      expect(pawn_information.agreement_no).to eq(buyer_number)
      expect(pawn_information.seller_no).to eq(seller_number)
      expect(pawn_information.vehicle_purchase_price).to eq(new_vehicle_price)
      expect(pawn_information.buy_fee).to eq(buy_fee)
      expect(promise_offer.days_selected).to eq(days_selected)
      expect(promise_offer.miles_selected).to eq(miles_selected)
    end
  end
end
