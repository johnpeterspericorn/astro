# encoding: utf-8
require 'spec_helper'

describe '/api/buyer_promise_purchase' do
  let(:user) { FactoryGirl.create(:buyer_user) }

  let(:base_params) do
    { auth_token: user.authentication_token, format: :json }
  end

  describe 'POST #create' do
    let(:pawn_access_number) { '123' }
    let(:offer) { FactoryGirl.create(:promise_offer, pawn_access_no: pawn_access_number) }

    before do
      allow_any_instance_of(User).to receive(:pawn_access_number) { pawn_access_number }
    end

    describe 'legacy iOS app v1.15 support' do
      let(:headers) do
        { 'HTTP_USER_AGENT' => 'AgreementShield Testing/1.15' }
      end

      it 'adjusts "additional days" as if it\'s provided as "total days"' do
        total_days = '21' # this is "total" days, rather than "additional days" to the base days
        adjusted_days = total_days.to_i - Promise::BASE_DURATION

        offer_params = { additional_days: total_days, miles: 500, id: offer.id }
        params = base_params.merge(offers: [offer_params])

        post '/api/buyer_promise_purchase', params, headers

        expect(response).to be_success
        expect(PromisePurchase.last.additional_days).to eq(adjusted_days)
      end
    end
  end
end
