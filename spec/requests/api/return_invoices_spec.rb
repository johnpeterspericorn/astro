# encoding: utf-8
require 'spec_helper'

describe '/api/return_invoices' do
  let(:user) { FactoryGirl.create(:api_user) }
  let(:base_params) do
    { auth_token: user.authentication_token, format: :json }
  end

  describe 'PUT #update' do
    let(:vehicle_received) { true }
    let(:title_received) { false }
    let(:refund_processed) { true }
    let(:pawn_location) { FactoryGirl.create(:pawn_location, initials: 'AAAA', name: 'Aaaaa') }
    let(:promise_offer) { FactoryGirl.create(:day_old_promise_offer) }
    let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer, promised_at: Time.current - 1.day) }
    let(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase, pawn_location: pawn_location) }

    it 'updates return invoice' do
      update_params = {
        vehicle_received: vehicle_received,
        title_received: title_received,
        refund_processed: refund_processed,
        pawn_location_id: pawn_location.id
      }
      params = base_params.merge(update_params)
      put "/api/return_invoices/#{return_invoice.id}", params
      return_invoice.reload
      expect(response).to be_success
      expect(return_invoice.vehicle_received).to be_truthy
    end
  end
end
