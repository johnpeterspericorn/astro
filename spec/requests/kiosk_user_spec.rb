# encoding: utf-8
require 'requests_helper'

describe 'KioskUser login' do
  context 'when a kiosk user is logged in' do
    before do
      log_in_kiosk_user
    end

    it 'should never be logged out from timeout' do
      visit root_path
      expect(page).not_to have_content 'Sign in'

      Timecop.travel(300.days.from_now) do
        visit root_path
        expect(page).not_to have_content 'Sign in'
      end
    end
  end
end
