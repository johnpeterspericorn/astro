# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Return Invoice View' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:pawn_information) { FactoryGirl.create(:pawn_information, agreement_no: '510000') }
  let!(:promise_offer) { FactoryGirl.create(:day_old_promise_offer, pawn_information: pawn_information) }
  let!(:promise_purchase) { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer) }
  let!(:return_location) { FactoryGirl.create(:pawn_location, name: 'BBB', initials: 'BBBB') }
  let!(:return_invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.return_invoices')
  end

  describe 'Index view' do
    let(:rand_vnum) { SecureRandom.hex[0..16].upcase }
    let(:rand_vra) { rand(1_000_000) }

    it 'displays return invoices' do
      expect(page).to have_content return_invoice.id
    end

    it 'displays obfuscated vra Link' do
      expect(page).to have_link(nil, href: "/links/#{return_invoice.slug}")
    end

    context 'VNUM filter' do
      it 'should display return invoices given an existing VNUM' do
        fill_in 'VNUM', with: return_invoice.promise_purchase.vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_content return_invoice.id
        expect(page).to have_xpath("//table[@class='data-table']/tbody/tr")
      end

      it 'should not display return invoices given a non existing VNUM' do
        fill_in 'VNUM', with: rand_vnum
        click_on I18n.t('forms.filter')
        expect(page).not_to have_xpath("//table[@class='data-table']/tbody/tr")
      end
    end

    context 'VRA# filter' do
      it 'should display return invoices given an existing VRA#' do
        fill_in 'VRA', with: return_invoice.id
        click_on I18n.t('forms.filter')
        expect(page).to have_content return_invoice.id
        expect(page).to have_xpath("//table[@class='data-table']/tbody/tr")
      end

      it 'should not display return invoices given a non-existing VRA# ' do
        fill_in 'VRA', with: rand_vra
        click_on I18n.t('forms.filter')
        expect(page).not_to have_xpath("//table[@class='data-table']/tbody/tr")
      end
    end

    context 'Agreement# filter' do
      it 'should display return invoices given an existing Agreement#' do
        fill_in 'Agreement#', with: return_invoice.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content return_invoice.vnum
      end

      it 'should not display return invoices given a non-existing Agreement#' do
        agreement_number = '1234'
        fill_in 'Agreement#', with: agreement_number
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content return_invoice.vnum
      end
    end
  end

  describe 'Edit view' do
    let(:new_odometer_reading) { 1020 }

    before do
      click_edit
    end

    it 'allows return location to be edited' do
      select(return_location.name, from: 'Return Location')
      click_on I18n.t('forms.update')
      expect(return_invoice.reload.pawn_location).to eq(return_location)
    end

    it 'will confirm email and send when return location is changed' do
      select(return_location.name, from: 'Return Location')
      expect(page.find('#send_return_confirmation')).to be_truthy
      expect do
        click_on I18n.t('forms.update')
      end.to change { enqueued_deliveries.count }.by(2)
    end

    it 'allows return status flags to be edited' do
      check I18n.t('return_status.vehicle_received')
      check I18n.t('return_status.refund_processed')
      click_on I18n.t('forms.update')
      expect(return_invoice.reload.vehicle_received).to be_truthy
      expect(return_invoice.reload.refund_processed).to be_truthy
    end

    it 'allows odometer reading to be edited' do
      fill_in 'Odometer Reading', with: new_odometer_reading
      click_on I18n.t('forms.update')
      expect(return_invoice.reload.odometer_on_return).to eq(new_odometer_reading)
    end

    it 'notifies astrdian on location change' do
      select(return_location.name, from: 'Return Location')
      expect do
        click_on I18n.t('forms.update')
      end.to change { enqueued_deliveries.count }.to(2)
      expect(return_invoice.reload.pawn_location).to eq(return_location)
    end

    it 'does not allow Cancel Return if vehicle received is set true' do
      check I18n.t('return_status.vehicle_received')
      click_on I18n.t('forms.update')
      expect(page).to have_content(I18n.t('admin.returns.vehicle_received'))
    end

    it 'allows floor plan branch to be changed' do
      flooring_company = FactoryGirl.create(:floor_plan_company)
      branch_a = FactoryGirl.create(:floor_plan_branch, name: 'Branch A', floor_plan_company: flooring_company)
      branch_b = FactoryGirl.create(:floor_plan_branch, name: 'Branch B', floor_plan_company: flooring_company)
      FactoryGirl.create(:floor_plan_branch_selection,
                         company_code: flooring_company.company_code,
                         site_no: branch_a.site_no,
                         return_invoice_id: return_invoice.id)
      visit edit_admin_return_invoice_path(return_invoice)

      select branch_b.name, from: 'floor_plan_branch_id'
      click_on I18n.t('forms.update')
      expect(return_invoice.reload.floor_plan_branch).to eq(branch_b)
    end

    it 'allows floor plan company to be changed', js: true do
      flooring_company_a = FactoryGirl.create(:floor_plan_company, name: 'Company A')
      flooring_company_b = FactoryGirl.create(:floor_plan_company, name: 'Company B')
      branch_a = FactoryGirl.create(:floor_plan_branch, name: 'Branch A', floor_plan_company: flooring_company_a)
      branch_b = FactoryGirl.create(:floor_plan_branch, name: 'Branch B', floor_plan_company: flooring_company_b)
      FactoryGirl.create(:floor_plan_branch_selection,
                         company_code: flooring_company_a.company_code,
                         site_no: branch_a.site_no,
                         return_invoice_id: return_invoice.id)
      visit edit_admin_return_invoice_path(return_invoice)

      select flooring_company_b.name, from: 'floor_plan_company_id'
      wait_for_ajax
      select branch_b.name, from: 'floor_plan_branch_id'
      click_on I18n.t('forms.update')

      expect(page).to have_content('Successfully updated return')
      return_invoice.reload
      expect(return_invoice.floor_plan_branch_selection.company_code).to eq(flooring_company_b.company_code)
      expect(return_invoice.floor_plan_branch_selection.site_no).to eq(branch_b.site_no)
    end

    it 'allows bill of sale to be edited' do
      seller_phone = '99999'
      ca_return_invoice = FactoryGirl.create(:ca_return_invoice)
      bill_of_sale = FactoryGirl.create(:bill_of_sale, return_invoice: ca_return_invoice)

      visit edit_admin_return_invoice_path(ca_return_invoice)

      fill_in 'return_invoice[bill_of_sale_attributes][seller_phone]', with: seller_phone
      click_on I18n.t('forms.update')

      expect(page).to have_content('Successfully updated return')
      bill_of_sale.reload
      expect(bill_of_sale.seller_phone).to eq(seller_phone)
    end

    context 'pplv' do
      it 'allows editing new product fee if pplv' do
        pawn_information = FactoryGirl.create(:pawn_information, agreement_no: '510000')
        promise_offer = FactoryGirl.create(:promise_offer, pawn_information: pawn_information, limited_volume: NewProductKind::LEVEL_3, promise_status: 'promised', automatic_purchase: true, days_selected: 21)
        promise_purchase = FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer)
        FactoryGirl.create(:pawn_location, name: 'BBB', initials: 'BBBB')

        Timecop.freeze(Date.today + 10.days) do
          return_invoice = FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase)
          login_to_admin_space

          click_on I18n.t('admin.navigation.return_invoices')
          click_on I18n.t('forms.edit')
          fill_in 'Return Fee', with: 100
          click_on I18n.t('forms.update')

          expect(return_invoice.reload.new_product_fee).to eq(100)
        end
      end
    end
  end

  describe 'Send Expiry Notifications' do
    let(:mailer_stub) { double(deliver_later: nil) }

    before do
      allow(return_invoice).to receive(:title_received).and_return(false)
      allow(ReturnInvoiceMailer).to receive(:title_expiration_notification).and_return(mailer_stub)
      allow(ReturnInvoiceMailer).to receive(:return_expiration_notification).and_return(mailer_stub)
      click_edit
    end

    it 'sends notification expiry emails' do
      expect(mailer_stub).to receive(:deliver_later)
      expect(ReturnInvoiceMailer).to receive(:title_expiration_notification).with(return_invoice).and_return(mailer_stub)
      click_on I18n.t('return_invoice.notify_title_expiry')
    end

    it 'sends return expiry notification reminder email' do
      expect(mailer_stub).to receive(:deliver_later)
      expect(ReturnInvoiceMailer).to receive(:return_expiration_notification).with(return_invoice).and_return(mailer_stub)
      click_on I18n.t('return_invoice.notify_return_expiry')
    end
  end
end
