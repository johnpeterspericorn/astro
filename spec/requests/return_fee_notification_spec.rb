# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'
require_relative './steps/pawn_access_number_swipe_steps'
require_relative './steps/navigation_steps'

describe 'Return Fee Notification' do
  include_context 'navigation steps'
  include_context 'return invoice steps'
  include_context 'pawn access number swipe steps'
  include_context 'new_product_data'

  let!(:honda) { FactoryGirl.create(:sold_vehicle, limited_volume: NewProductKind::LEVEL_3, make: 'HONDA', odometer_reading: 10_000, agreement_no: SecureRandom.hex, seller_paid: true) }

  before do
    Settings.salvage_internet_notes_enabled = true
    stub_buyer_authorization
  end

  vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

  context 'navigation', vcr: vcr_options do
    before do
      create_product_fees
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'as a super user' do
      let(:pawn_access_number_filter_button) { I18n.t('forms.pawn_access.input_label') }
      let(:agreement_number_filter_button)        { I18n.t('forms.agreement_number.input_label')  }
      let(:vnum_number_filter_button)           { I18n.t('forms.vnum_number.input_label')     }
      let(:mileage) { honda.odometer_reading + max_mileage - 1 }

      before do
        log_in_super_user
        purchase_promise_for_vehicle(honda)

        PromiseOffer.by_vnum(honda.vnum).first.pawn_information.update_attributes(pawn_access_no: '1')
        allow_any_instance_of(PromisePurchase).to receive(:eligible_for_return?).and_return(true)
      end

      it 'does not notify return fee if pplv and seller_paid' do
        initiate_pplv_return(honda, true)

        expect(page).to_not have_content('return fee')
        expect(page).to have_content('Reasons for Return')
      end

      it 'does not notify return fee if NOT pplv irrespective of seller_paid' do
        visit_returns_user_info_page
        click_on vnum_number_filter_button
        select_vnum_number_for_return(honda.vnum)
        click_on I18n.t('forms.return')

        expect(page).to_not have_content('return fee')
        expect(page).to have_content('Reasons for Return')
      end

      it 'show informational message after clicking on payment' do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(4)
        allow_any_instance_of(PromisePurchase).to receive(:paid?).and_return(false)
        initiate_pplv_return(honda, false)

        expect(page).to have_content(I18n.t('return_invoice.unpaid_informal_message'))
      end

      it 'displays return fee and additional fee notification if pplv and buyer paid for 4th return' do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(4)
        initiate_pplv_return(honda, false)

        expect(page).to have_content('This is your 4th return performed within 365 days and your return fee will be $ 150.')
        expect(page).to have_content('You may perform 2 additional return(s) before an additional $ 350 be imposed.')
        expect(page).to have_content('CC Payment')
        expect(page).to have_content('Skip Payment')
      end

      it 'notifies user of how many more free returns until user gets charged' do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(1)
        initiate_pplv_return(honda, false)

        expect(page).to have_content('This is your 1st return performed within 365 days and your return fee will be $ 0.')
        expect(page).to have_content('You may perform 2 additional return(s) before an additional $ 150 be imposed.')
      end

      it 'does not show skip payment link if return fee is zero' do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(1)
        initiate_pplv_return(honda, false)

        expect(page).to have_content('Continue')
        expect(page).to_not have_content('Skip Payment')
      end

      it 'displays NO additional fee notification if pplv and buyer paid for threshold return' do
        allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).and_return(7)
        initiate_pplv_return(honda, false)

        expect(page).to have_content('This is your 7th return performed within 365 days and your return fee will be $ 500.')
        expect(page).to_not have_content('additional')
        expect(page).to have_content('Skip Payment')
      end
    end
  end
end
