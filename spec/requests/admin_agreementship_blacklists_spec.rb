# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Agreementship Blacklists View' do
  include_context 'navigation steps'
  include_context 'admin steps'
  let!(:agreementship_blacklist) { FactoryGirl.create(:agreementship_blacklist) }
  let(:random_agreement_no) { 54_321 }

  before do
    login_to_admin_space
    click_on I18n.t('admin.navigation.agreementship_blacklists')
  end

  describe 'AgreementshipBlacklist Index' do
    it 'should list agreementship blacklists' do
      expect(page).to have_content(agreementship_blacklist.agreement_no)
    end

    context 'Agreement# filter' do
      it 'should display agreementship blacklists given an existing agreement#' do
        fill_in 'filters_agreement_no', with: agreementship_blacklist.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content agreementship_blacklist.agreement_no
      end

      it 'should not display agreementship blacklists given a non existing agreement#' do
        fill_in 'filters_agreement_no', with: random_agreement_no
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content random_agreement_no
      end
    end
  end

  describe 'Edit AgreementshipBlacklist' do
    let(:new_expiration_date) { Date.tomorrow }

    before do
      click_edit
    end

    it 'allows AgreementshipBlacklist to be edited' do
      fill_in 'Expiration Date', with: new_expiration_date
      click_on I18n.t('forms.update')
      expect(page).to have_content(new_expiration_date)
      expect(page).to have_content(I18n.t('agreementship_blacklist.status.active'))
    end
  end

  describe 'Delete AgreementshipBlacklist' do
    it 'allows AgreementshipBlacklist to be deleted' do
      click_link I18n.t('forms.delete')
      expect(page).to_not have_content(agreementship_blacklist.agreement_no)
    end
  end

  describe 'Add a new AgreementshipBlacklist' do
    let(:agreement_no) { '6666' }

    before do
      click_on I18n.t('admin.agreementship_blacklists.create')
    end

    it 'allows agreementship blacklists to be added' do
      fill_in 'Agreement#', with: agreement_no
      click_on I18n.t('forms.finish')
      expect(page).to have_content agreement_no
    end
  end
end
