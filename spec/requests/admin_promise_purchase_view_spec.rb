# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - Navigation' do
  include_context 'admin steps'

  let!(:sva_location) { FactoryGirl.create(:svaa_location) }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA') }

  before do
    PromiseOfferRecorder.record_offers [acura]
  end

  context 'All promise selection viewer' do
    before do
      visit_promise_offer_viewer
    end

    it 'should show the vehicle as unpromised' do
      expect(page).to have_content 'Acura'
      expect(page).to have_content 'unselected'
    end
  end
end
