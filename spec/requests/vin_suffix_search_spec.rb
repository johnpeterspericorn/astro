# encoding: utf-8
require 'requests_helper'
require_relative './steps/vnum_suffix_search_steps'

describe 'VNUM Suffix Filter Workflow' do
  include_context 'vnum suffix search steps'

  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let!(:malibu) { FactoryGirl.create(:presale_vehicle, make: 'CHEVROLET', model: 'MALIBU', lane_no: 1, run_no: 1, location_initials: 'SVAA') }
  let(:vnum_suffix) { malibu.vnum[-6..-1] }

  before do
    log_in_buyer_user
  end

  it 'should filter on VNUM-suffix' do
    navigate_to_research_page
    search_by_vnum

    fill_in 'vnum_suffix', with: vnum_suffix
    submit_vnum_suffix_form

    expect(page).to have_content malibu.vnum
  end
end
