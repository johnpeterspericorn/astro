# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Manage Dashboard action' do
  include_context 'navigation steps'
  include_context 'admin steps'
  let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }

  before do
    FactoryGirl.create(:flat_rate_offer)
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    click_on I18n.t('admin.navigation.dashboard_scores')
  end

  describe 'Manage Action' do
    it 'agreement info and action form' do
      FactoryGirl.create(:agreement_information, agreement_no: '5241420')
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content dashboard_score.buyer_num
      expect(page).to have_content 'Network Plus Addition'
    end
  end

  describe 'Network Plus Action Type' do
    it 'displays network plus offer form' do
      agreement_info = FactoryGirl.create(:agreement_information, agreement_no: '5241420')
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content agreement_info.agreementship_name
      fill_in 'emails', with: 'sfdfdf.fghfh@sdasds.com'
      choose('action_type_network_plus_addition')
      click_on 'Submit'
      expect(page).to have_content 'Existing (Network) Rate :'
    end

    it 'throws an error if agreement_information not there' do
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content 'Please enter the agreement information for this agreement'
    end
  end
end
