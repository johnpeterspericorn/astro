# encoding: utf-8
require 'requests_helper'
require_relative './steps/flat_rate_steps'
require_relative '../requests_support/country_helper.rb'

describe 'Flat Rates Page' do
  include_context 'flat rate steps'

  let(:pawn_access_number) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let!(:flat_rate) { FactoryGirl.create(:flat_rate) }
  let!(:currency) { currency_symbol(flat_rate.country) }

  before do
    stub_buyer_authorization(agreement_numbers: flat_rate.agreement_no)
  end

  context 'when the policy allows building an offer' do
    context 'user is a flat_rate_inquiry_viewer' do
      before do
        log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
        allow_any_instance_of(FlatRatePolicy).to receive(:build_offer?) { true }
      end

      it 'gives user the ability to offer a flat rate target price', js: true do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)
        check('include_agreementship')
        click_on I18n.t('forms.next')

        visit admin_flat_rate_offers_path
        expect(page).to have_content('456')
      end

      it 'does not give user the ability to offer a flat rate target price' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)
        click_on I18n.t('forms.next')

        visit admin_flat_rate_offers_path
        expect(page).to_not have_content('456')
      end

      it 'displays only target price' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to_not have_content("#{currency} 123")
        expect(page).to have_content("#{currency} 456")
        expect(page).to_not have_content("#{currency} 789")
      end
    end

    context 'user is a flat_rate_editor' do
      before do
        log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
        allow_any_instance_of(FlatRatePolicy).to receive(:build_offer?) { true }
      end

      it 'displays 250 miles flat rate by default' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content("#{currency} 456")
      end

      it 'displays 250 miles with PSI flat rate when selected' do
        view_360_miles_with_psi_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content("#{currency} 222")
      end

      it 'displays 500 miles when selected' do
        view_500_miles_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content("#{currency} 666")
      end

      it 'displays 500 miles with PSI flat rate when selected' do
        view_500_miles_with_psi_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content("#{currency} 950")
      end

      context 'offers for country' do
        let!(:flat_rate_canada) { FactoryGirl.create(:flat_rate_canada) }

        it 'displays 250 miles flat rate for selected country' do
          country = 'Canada'
          view_flat_rates_for_country(pawn_access_number, flat_rate.agreement_no, country)

          expect(page).to have_content("#{currency} 4,560")
          expect(page).to have_content(country)
        end
      end

      context 'no agreement number selected' do
        before do
          view_flat_rate_agreement_numbers(pawn_access_number)
          click_on I18n.t('forms.flat_rates.view')
        end

        it 'tells user to select agreement number(s)' do
          expect(page).to have_content(I18n.t('flat_rates.errors.select_agreement_numbers'))
        end
      end
    end
  end

  context 'when the policy does not allow building an offer' do
    let!(:flat_rate) { FactoryGirl.create(:flat_rate, ineligibility_condition: 1) }
    let(:reason) { I18n.t('flat_rates.ineligibility_conditions')[1] }

    context 'as a super user' do
      before do
        log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
      end

      it 'displays an entry with the reason for flat rate ineligibility' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content(flat_rate.agreement_no)
        expect(page).to have_content(reason)
      end

      it 'displays the entry, but with a warning' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)

        expect(page).to have_content("#{currency} 456")
        expect(page).to have_content(reason)
      end
    end
  end

  context 'flat rate offer already exists for agreement' do
    let(:flat_rate_offer) { FactoryGirl.create(:flat_rate_offer) }
    let(:flat_rate) { flat_rate_offer.flat_rate }
    let(:pawn_access_number) { flat_rate_offer_batch.pawn_access_no }
    let(:flat_rate_offer_batch) { flat_rate_offer.flat_rate_offer_batch }

    before do
      log_in_super_user(flat_rate_inquiry_viewer: true, flat_rate_editor: true)
    end

    context 'for mase country' do
      it 'directs user to delete existing batch before proceeding' do
        view_flat_rates(pawn_access_number, flat_rate.agreement_no)
        find('[rel~=delete-batch]').click
        view_flat_rates(pawn_access_number, flat_rate_offer.agreement_no)

        expect(page).to have_content('Automatic AgreementShield Offer Generator')
      end
    end

    context 'for a different country' do
      it 'allows user to create new batch' do
        view_flat_rates_for_country(flat_rate_offer_batch.pawn_access_no, flat_rate_offer.agreement_no, 'Canada')

        expect(page).to have_content('Automatic AgreementShield Offer Generator')
      end
    end
  end
end
