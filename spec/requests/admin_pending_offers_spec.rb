# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/promise_purchase_steps'

describe 'Admin Pending Purchases View' do
  include_context 'admin steps'

  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let!(:pawn) { FactoryGirl.create(:svaa_location) }
  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', pending_reason_code: PendingConditions::APPROVAL_REQUIRED) }
  let!(:promise_offer) do
    PromiseOfferRecorder.record_offer(honda)
    offer = honda.promise_offers.first
    offer.tap do |go|
      go.promise_status = :pending
      go.save!
    end
  end
  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }

  before { visit_pending_promises_viewer }

  it 'displays pending offers' do
    expect(page).to have_content(honda.vnum)
  end

  describe 'Network+ Vehicle' do
    let(:external_vehicles_batch) { FactoryGirl.create(:external_vehicles_batch) }
    let!(:external_vehicles_load) { FactoryGirl.create(:external_vehicles_load, vnum: promise_offer.vnum, external_vehicles_batch: external_vehicles_batch) }

    it 'links to network plus batch' do
      click_on 'Details'
      expect(page).to have_content 'belongs to the Network+ Batch'
    end
  end

  context 'VNUM filter' do
    it 'should display pending offer given an existing VNUM' do
      fill_in 'VNUM', with: honda.vnum
      click_on I18n.t('forms.filter')
      expect(page).to have_content honda.vnum
    end

    it 'should not display pending offer given a non existing VNUM' do
      rand_vnum = 'ABC123'
      fill_in 'VNUM', with: rand_vnum
      click_on I18n.t('forms.filter')
      expect(page).not_to have_content rand_vnum
    end
  end

  context 'Agreement filter' do
    it 'should display pending offers given an existing agreement' do
      fill_in 'Agreement#', with: honda.agreement_no
      click_on I18n.t('forms.filter')
      expect(page).to have_content honda.agreement_no
    end

    it 'should not display pending offer given a non existing agreement' do
      rand_agreement_no = '555555'
      fill_in 'Agreement#', with: rand_agreement_no
      click_on I18n.t('forms.filter')
      expect(page).not_to have_content rand_agreement_no
    end
  end

  context 'Location Initial filter' do
    it 'should display pending offers given an existing location initial' do
      fill_in 'Location Initials', with: honda.location_initials
      click_on I18n.t('forms.filter')
      expect(page).to have_content honda.location_initials
    end

    it 'should not display pending offers given a non existing location Initial' do
      rand_location_initials = 'ZZZZ'
      fill_in 'Location Initial', with: rand_location_initials
      click_on I18n.t('forms.filter')
      expect(page).not_to have_content rand_location_initials
    end
  end

  describe 'Individual pending purchase view' do
    before do
      visit_pending_promises_viewer
      click_on 'Details'
    end

    describe 'Invalidation' do
      let(:promise_purchase_id) { promise_offer.promise_purchase.id }

      it 'directs the user to the cancellation funnel' do
        click_on 'Invalidate'

        expect(current_path).to eql new_promise_purchase_cancellation_path(promise_purchase_id: promise_purchase_id)
      end
    end

    describe 'Allowing a purchase' do
      it 'submits a charge for the purchase' do
        Settings.charges_enabled = true
        expect(PromiseChargeJob).to receive(:enqueue)
        click_on 'Allow'
      end

      it 'sends confirmation mail' do
        expect { click_on 'Allow' }.to change { enqueued_jobs.count }.by(1)
      end

      it 'changes status to promised' do
        expect do
          click_on 'Allow'
        end.to change { promise_offer.reload.pending? }.from(true).to(false)
      end

      context 'confirmation email based on promise kind' do
        before do
          promise_offer.update_attributes!(emails: 'test@test.com')
        end

        it 'sends purchase confirmation if kind is PURCHASE_PROTECT' do
          promise_purchase.update_attributes!(kind: PromisePurchase::PURCHASE_PROTECT)
          click_on 'Allow'
          mailer, method, _, active_job_object = last_enqueued_delivery[:args]
          expect(mailer).to eq('PromisePurchaseMailer')
          expect(method).to eq('purchase_confirmation')
          expect(active_job_object).to eq('promise_purchases' => [
                                            { '_aj_globalid' => promise_purchase.to_gid.to_s }
                                          ],
                                          'user_email' => 'test@test.com',
                                          '_aj_symbol_keys' => %w(promise_purchases user_email))
        end

        it 'sends refresh confirmation if kind is MARKET_PROTECT' do
          promise_purchase.update_attributes!(kind: PromisePurchase::MARKET_PROTECT)
          click_on 'Allow'
          mailer, method, _, active_job_object = last_enqueued_delivery[:args]
          expect(mailer).to eq('PromisePurchaseRefreshMailer')
          expect(method).to eq('refresh_confirmation')
          expect(active_job_object).to eq('_aj_globalid' => promise_offer.to_gid.to_s)
        end
      end

      it "updates the offer's approved_at timestamp" do
        expect do
          click_on 'Allow'
        end.to change { promise_offer.reload.approved_at }
      end

      context 'offer is paid by floor plan' do
        before do
          promise_offer.update_attributes!(paid_by_floor_plan: true)
        end

        it 'does not charge for the purchase' do
          Settings.charges_enabled = true
          expect(PromiseChargeJob).not_to receive(:enqueue)
          click_on 'Allow'
        end
      end

      context 'offer charge is bypassed' do
        before do
          promise_offer.update_attributes!(bypass_charge: true)
        end

        it 'does not charge for the pruchase' do
          Settings.charges_enabled = true
          expect(PromiseChargeJob).not_to receive(:enqueue)
          click_on 'Allow'
        end
      end
    end
  end
end
