# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'
require_relative './steps/manage_dashboard_action_steps'

describe Admin::RateChangesController do
  include_context 'navigation steps'
  include_context 'admin steps'
  include_context 'manage dashboard actions steps'

  let!(:agreement_info) { FactoryGirl.create(:agreement_information, agreement_no: '5241420') }
  let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }
  let(:emails) { 'sfdfdf.fghfh@sdasds.com' }
  let!(:rel_manager) { FactoryGirl.create(:relationship_manager_1) }
  let(:effective_date) { 5.days.from_now }

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    click_on I18n.t('admin.navigation.dashboard_scores')
    click_on I18n.t('admin.navigation.dashboard_score_management.manage')
  end

  describe 'Rate Change Creation' do
    it 'successfully processes rate changes' do
      process_rate_change(emails, 10.0, effective_date, rel_manager.name)
      expect(page).to have_content('Rate Change Email sent')
    end

    it 'successfully sends out a rate change email' do
      process_rate_change(emails, 12.0, effective_date, rel_manager.name)

      mail = last_delivered_mail
      expect(mail.to).to include(emails)
      expect(mail.subject).to eq(I18n.t('rate_change_mailer.return_rate_version.subject'))
      expect(mail.body.encoded).to include('your rate needs to be')
    end
  end

  describe 'Manage Rate Change' do
    it 'shows 2 rate changes in the listing' do
      FactoryGirl.create(:rate_change, return_rate: 10.0, effective_date: effective_date)
      process_rate_change(emails, 12.0, effective_date, rel_manager.name)
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content('RateChange', count: 2)
      expect(page).to have_content('new rate: 10.0')
    end

    it 'should not track viewed_user or rate change link_clicks if super user' do
      process_rate_change(emails, 12.0, effective_date, rel_manager.name)
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).not_to have_content('viewed_user')
    end
  end
end
