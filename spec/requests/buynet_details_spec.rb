# encoding: utf-8
require 'requests_helper'

describe 'Buynet details page' do
  let(:promise_purchase) { FactoryGirl.create :promise_purchase }
  before do
    log_in_super_user
  end

  it 'displays adjustments fees', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
    visit promise_purchase_buynet_path(promise_purchase)

    within('.reimbursable-fees') do
      expect(page).to have_content('21D 250M')
      expect(page).to have_content('DEALA')
      expect(page).to have_content('$ 105')
    end

    within('.nonreimbursable-fees') do
      expect(page).to have_content('D M')
      expect(page).to have_content('DEALB')
      expect(page).to have_content('-$ 105')
    end
  end

  context 'CC payment link' do
    describe 'US Partner Network' do
      let(:pawn_location) { FactoryGirl.create(:pawn_location) }
      it 'displays link', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
        visit promise_purchase_buynet_path(promise_purchase)
        expect(page).to have_link(I18n.t('buynets.show.cc_payment'))
      end
    end

    describe 'Non-US Partner Network' do
      let(:pawn_location) { FactoryGirl.create(:ca_location) }
      let(:pawn_information) { FactoryGirl.create(:pawn_information, pawn_location: pawn_location) }
      let(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information) }
      let(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
      it 'does not display link', vcr: { cassette_name: 'vehicle_adjustments_api/successful_request' } do
        visit promise_purchase_buynet_path(promise_purchase)
        expect(page).to_not have_link(I18n.t('buynets.show.cc_payment'))
      end
    end
  end
end
