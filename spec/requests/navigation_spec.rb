# encoding: utf-8
require 'requests_helper'
require_relative './steps/navigation_steps'

describe 'Site Navigation' do
  include_context 'navigation steps'

  context 'Kiosk Users' do
    before do
      log_in_kiosk_user
    end

    it 'should have a back button on the vehicle filter page' do
      Settings.show_badge_ui = true
      visit_filter_page
      click_on I18n.t('forms.back')
      page_should_be_root_page
    end

    it 'should skip the filter method selection if badge number is disabled' do
      Settings.show_badge_ui = false
      visit_filter_page
      page_should_be_pawn_access_number_filter_page
    end

    it 'should not have a logout button' do
      expect(page).not_to have_content I18n.t('forms.logout')
    end
  end

  context 'Buyer users' do
    before do
      stub_buyer_authorization
      log_in_buyer_user
    end

    context "when a 'msg' parameter is set" do
      it 'shows a message to the user' do
        visit root_path(msg: 'thank_you_message')
        expect(page).to have_content(I18n.t('help.thank_you_message'))
      end
    end

    it 'should have a back button on the terms and conditions page' do
      click_on I18n.t('navigation.layout.static_links.terms_and_conditions')
      click_on I18n.t('forms.home')

      page_should_be_root_page
    end

    it 'should have a back button on the FAQ page' do
      click_on I18n.t('navigation.layout.static_links.faq')
      click_on I18n.t('forms.back')
      page_should_be_root_page
    end

    it 'should have a logout button' do
      click_on I18n.t('forms.logout')

      page_should_be_login_page
    end

    it 'should not show purchase and return workflows' do
      disable_purchase_authorization_for_user
      visit root_path

      expect(page).not_to have_content I18n.t('navigation.purchase')
      expect(page).not_to have_content I18n.t('navigation.return')
    end
  end

  context 'Buyer users deep linking' do
    it 'should redirect to linked page after login' do
      visit quotes_research_path
      page_should_be_login_page

      log_in_buyer_user

      page_should_be_quotes_research_page
    end
  end

  context 'Super Users' do
    it 'should have a logout button' do
      log_in_super_user
      click_on I18n.t('forms.logout')

      page_should_be_login_page
    end
  end
end
