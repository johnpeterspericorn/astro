# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Payments View' do
  include_context 'admin steps'

  let!(:payment_response) { FactoryGirl.create(:payment_response, error_message: SecureRandom.hex) }

  before do
    login_as_finance_admin_viewer
    visit admin_payments_path
    VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
  end

  after do
    VCR.eject_cassette
  end

  it 'shows error messages' do
    expect(page).to have_content payment_response.error_message
  end

  describe 'Index view' do
    let(:payment_vnum) { '8070df75579591623' }
    let(:payment_agreement_number) { '6275270' }

    it 'should display all payments' do
      expect(page).to have_content payment_response.id
    end

    it 'should display return fee' do
      FactoryGirl.create(:payment, return_fee: 150)
      visit admin_payments_path
      expect(page).to have_content 'ReturnFee'
      expect(page).to have_content '$ 150'
    end

    it 'should send email receipt given email address' do
      click_link I18n.t('payments.email.resend_label')
      fill_in 'emails', with: 'sdsad@dasd.com'
      expect { click_on I18n.t('payments.email.send_label') }.to change { enqueued_deliveries.count }.by(1)
    end

    it 'should not send an email when invalid email address is provided' do
      click_link I18n.t('payments.email.resend_label')
      fill_in 'emails', with: 'sdsaddasd.com'
      expect { click_on I18n.t('payments.email.send_label') }.not_to change { enqueued_deliveries.count }
    end

    context 'VNUM filter' do
      it 'should display payments given an existing VNUM' do
        fill_in 'VNUM', with: payment_response.promise_purchase.vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_field('VNUM', with: payment_response.promise_purchase.vnum)
        expect(page).to have_content(payment_response.payment.order_id)
      end

      it 'should not display return payments given a non existing VNUM' do
        fill_in 'VNUM', with: payment_vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_field('VNUM', with: payment_vnum)
        expect(page).to_not have_content(payment_vnum)
      end
    end

    context 'Agreement# filter' do
      it 'should display payments given an existing Agreement#' do
        fill_in 'Agreement#', with: payment_response.promise_purchase.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_field('Agreement#', with: payment_response.promise_purchase.agreement_no)
        expect(page).to have_content(payment_response.payment.order_id)
      end

      it 'should not display payments given a non-existing Agreement# ' do
        fill_in 'Agreement#:', with: payment_agreement_number
        click_on I18n.t('forms.filter')
        expect(page).to have_field('Agreement#', with: payment_agreement_number)
        expect(page).to_not have_content(payment_agreement_number)
      end
    end
  end
end
