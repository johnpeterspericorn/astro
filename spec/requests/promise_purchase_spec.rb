# encoding: utf-8
require 'requests_helper'
require_relative './steps/navigation_steps'
require_relative './steps/promise_purchase_steps'
require_relative './steps/promise_selection_vehicle_steps'

describe 'Promise Purchase Workflow' do
  include_context 'navigation steps'
  include_context 'promise purchase steps'

  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', pawn_access_no: pawn_access_no) }
  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', pawn_access_no: pawn_access_no) }

  before do
    stub_buyer_authorization
    Settings.inspections_enabled = true
    Settings.show_badge_ui = true
    Settings.show_agreement_filter_ui = true
    create_location_data
  end

  context 'as a kiosk user' do
    before do
      log_in_kiosk_user
    end

    it 'should prefill the email field with the last used email' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_email('new@email.com')

      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_acura_for_250_miles_and_7_additional_days
      click_next

      page_should_have_prefilled_user_email('new@email.com')

      expect(BuyerInformation.count).to eql 1
    end

    # Protect against regression https://www.pivotaltracker.com/story/show/81697560
    it 'allows user to override prefilled email' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_acura_for_250_miles_and_7_additional_days
      checkout_with_email('saved@test.com')

      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_email('new@test.com')

      expect(PromiseOffer.last.emails).to eq('new@test.com')
    end

    it 'persists email on offer' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout

      expect(PromiseOffer.last.emails).to be_present
    end

    it 'allow user to enter multiple emails' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_email(' new@old.com , saved@test.com ')

      expect(PromiseOffer.last.emails).to include('new@old.com')
      expect(PromiseOffer.last.emails).to include('saved@test.com')
    end

    it 'should show only the users associated with a given agreement number' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      click_next

      checkout_page_should_have_information_for(honda)
    end

    it 'should display purchase confirmation if the blacklisted vnum entry has expired' do
      VinBlacklist.create!(vnum: honda.vnum, expiration_date: Date.yesterday)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout
      expect(page).to have_content('Purchase Confirmation')
    end

    it 'should display purchase confirmation if the blacklisted agreementship entry has expired' do
      AgreementshipBlacklist.create!(agreement_no: honda.agreement_no, expiration_date: Date.yesterday)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout
      expect(page).to have_content('Purchase Confirmation')
    end

    it 'should display purchase confirmation if the blacklisted agreement entry has expired' do
      PawnAccessNumberBlacklist.create!(pawn_access_no: honda.pawn_access_no, expiration_date: Date.yesterday)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout
      expect(page).to have_content('Purchase Confirmation')
    end

    it 'should prefill email field with agreement email address' do
      email = 'jimmybobby@joe.com'
      stub_buyer_authorization(email: email)

      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      click_next

      page_should_have_prefilled_user_email(email)
    end

    it 'should reject invalid email' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_invalid_email

      page_should_have_invalid_email_alert
    end

    it 'flags buyer for updates' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days

      click_next
      find('.return-email').set('valid@example.com')
      check 'send_updates'

      expect do
        click_on I18n.t('forms.next')
      end.to change(BuyerInformation, :count).by(1)

      expect(BuyerInformation.first.send_updates).to be_truthy
    end

    it 'should accept valid email' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_valid_email

      page_should_not_have_invalid_email_alert
    end

    it 'should reject missing email' do
      blank_email_alert = I18n.t('activemodel.errors.models.email_validation.attributes.email_addresses.blank')
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout_with_email('')

      expect(page).to have_content(blank_email_alert)
    end

    describe 'inspections', js: true do
      let(:inspection_bundle_price) { honda.inspection_bundle_price }
      let(:inspection_only_price)   { honda.inspection_only_price }

      include_context 'promise selection vehicle steps'

      before do
        view_vehicles_by_pawn_access_number_as_kiosk_user
      end

      context 'when a promise is also chosen' do
        before do
          within honda_information do
            select_honda_for_500_miles_and_14_additional_days_with_inspection
          end
        end

        it 'should allow users to add an inspection', broken: true do
          checkout

          page.document.synchronize(3) do
            offer = PromiseOffer.by_vnum(honda.vnum).first
            offer.present? && offer.has_inspection? &&
              offer.inspection_purchase.price == inspection_bundle_price &&
              offer.inspection_purchase.transaction_id.present?
          end

          honda_should_have_vehicle_inspection
        end

        it 'shows bundled data on the checkout page', broken: true do
          click_next

          expect(page).to have_content 'comprehensive checks'
          page_should_show_honda_with_bundled_purchase
        end
      end
    end

    it 'should take a user to the offers page from the checkout page when clicking back' do
      view_vehicles_by_pawn_access_number_as_kiosk_user

      select_honda_for_500_miles_and_14_additional_days

      click_next

      click_on I18n.t('forms.back')

      page_should_be_promise_offers_view
    end

    it 'should indicate if a vehicle is on if bid' do
      honda.update_attributes(if_bid: true)

      view_vehicles_by_pawn_access_number_as_kiosk_user

      select_honda_for_500_miles_and_14_additional_days

      click_next

      checkout_page_should_indicate_honda_is_an_if_bid
    end

    it 'should allow users to print' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      expect(page).to have_content I18n.t('forms.print')
    end

    it 'should allow a user to select none, and have a vehicle not listed in the next screen' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_none_for_honda
      select_acura_for_250_miles_and_7_additional_days
      click_next
      expect(page).not_to have_content 'Honda'
    end

    it 'should allow a customer to cancel during checkout step' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      click_next
      cancel_checkout_process

      page_should_be_root_page
    end

    it 'should handle bidder badge # filter and back button in checkout funnel' do
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      click_next
      click_on I18n.t('forms.back') # returns to offer page
      click_on I18n.t('forms.cancel') # returns to landing page

      view_vehicles_by_badge_number
      select_honda_for_250_miles_and_7_additional_days
      click_next
      click_on I18n.t('forms.back')

      expect(page.current_url).to eql vehicle_filter_url
    end

    it 'should send an email after checkout' do
      mailer_stub = double
      expect(mailer_stub).to receive(:deliver_later).exactly(2).times
      expect(PromisePurchaseMailer).to receive(:purchase_confirmation).and_return(mailer_stub)
      expect(PromisePurchaseMailer).to receive(:pending_confirmation).and_return(mailer_stub)

      view_vehicles_by_pawn_access_number_as_kiosk_user

      select_honda_for_500_miles_and_14_additional_days
      select_acura_for_250_miles_and_7_additional_days

      checkout
    end

    it 'shows if bids on the confirmation page' do
      honda.update_attributes(if_bid: true)

      view_vehicles_by_pawn_access_number_as_kiosk_user

      select_honda_for_500_miles_and_14_additional_days
      select_acura_for_250_miles_and_7_additional_days

      checkout

      confirmation_page_should_indicate_honda_is_an_if_bid
    end

    context 'charging a purchase' do
      before do
        Settings.charges_enabled = true
      end

      it 'queues charge jobs if charges are enabled' do
        expect(PromiseChargeJob).to receive(:enqueue).exactly(2).times

        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        select_acura_for_250_miles_and_7_additional_days
        checkout
      end

      it 'does not queue jobs for floor plan paid vehicles' do
        Settings.query_ods = true
        allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(true)
        allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(true)

        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        select_acura_for_250_miles_and_7_additional_days

        # Warning: This spec is tightly coupled to VCR. The error we're hoping
        # to avoid here is VCR's UnhandledHTTPRequestError. This error is raised
        # when a charge is attempted (which posts to an external web service).
        expect { checkout }.to_not raise_error
      end

      it 'does not charge for vehicles requiring approval' do
        honda.update_attributes(pending_reason_code: PendingConditions::APPROVAL_REQUIRED)

        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days

        # Warning: This spec is tightly coupled to VCR. The error we're hoping
        # to avoid here is VCR's UnhandledHTTPRequestError. This error is raised
        # when a charge is attempted (which posts to an external web service).
        expect { checkout }.to_not raise_error
      end

      it 'does not charge for vehicles with bypass enabled' do
        honda.update_attributes(bypass_charge: true)

        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days

        # Warning: This spec is tightly coupled to VCR. The error we're hoping
        # to avoid here is VCR's UnhandledHTTPRequestError. This error is raised
        # when a charge is attempted (which posts to an external web service).
        expect { checkout }.to_not raise_error
      end
    end

    context 'finishing a transaction' do
      before do
        view_vehicles_by_pawn_access_number_as_kiosk_user

        select_honda_for_500_miles_and_14_additional_days
        select_acura_for_250_miles_and_7_additional_days

        checkout
      end

      it 'should have a print option' do
        expect(page).to have_content I18n.t('forms.print')
      end

      it 'should go back to the root' do
        click_done

        page_should_be_root_page
      end

      it 'should have a correct transaction id on purchases' do
        promise_purchases_should_have_transaction_numbers_for_kiosk_user
      end
    end

    context 'cycling back through the transaction workflow' do
      before do
        view_vehicles_by_pawn_access_number_as_kiosk_user

        select_honda_for_500_miles_and_14_additional_days

        checkout
        click_done
      end

      context 'without a new vehicle' do
        before do
          view_vehicles_by_pawn_access_number_as_kiosk_user
        end

        it 'should still select previous promises' do
          expect(page.find('.current_vehicles')).to have_content 'Honda'
          expect(page.first("##{promise_label(honda, miles: 500, days: 14)}")).to be_checked
        end

        it 'should have vehicle descriptions highlighted' do
          expect(page).to have_css '.promised_vehicle_description.selected'
        end

        it 'should show unselected promises before selected' do
          acura_should_appear_before_honda
        end

        it 'should not allow promises to be updated' do
          select_acura_for_250_miles_and_7_additional_days # checkout another vehicle to actually check the next page...
          select_honda_for_250_miles_and_7_additional_days
          click_next
          expect(page).to have_content 'Acura'
          expect(page).not_to have_content 'Honda'
          expect(page).not_to have_content 'Updated'
        end

        it 'should show new promises checkout page' do
          select_acura_for_250_miles_and_7_additional_days
          click_next
          expect(page).not_to have_content 'Honda'
          expect(page).not_to have_content 'Updated'
          expect(page).to have_content 'Acura'
        end

        it 'should only show new promises in the checkout page if they exist' do
          select_honda_for_250_miles_and_7_additional_days
          select_acura_for_250_miles_and_7_additional_days
          click_next
          expect(page).to have_content 'Acura'
        end
      end

      context 'with a new vehicle' do
        it 'should show new vehicles and hide old ones', js: true do
          new_vehicle = FactoryGirl.create(:sold_vehicle, pawn_access_no: pawn_access_no)
          expect(SoldVehicle.where(pawn_access_no: pawn_access_no).count).to eq(3)
          expect(PromiseOffer.joins(:pawn_information).where(pawn_information: { pawn_access_no: pawn_access_no }).count).to eq(2)
          view_vehicles_by_pawn_access_number_as_kiosk_user

          expect(page.find('.current_vehicles')).to have_content new_vehicle.vnum
          expect(page.find('.current_vehicles')).to have_content 'Honda'
        end
      end

      context 'with a now missing vehicle' do
        before do
          SoldVehicle.find(acura.id).destroy
        end

        # regression test from bug encountered during an pawn.  Vehicles may not longer exist!
        it 'should not blow up' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          expect(page).not_to have_content 'Acura'
          expect(page).to have_content 'Honda'
        end
      end
    end

    context 'with ineligible vehicles' do
      let!(:ineligible_vehicle)          { FactoryGirl.create(:sold_vehicle, ineligibility_condition: 1, pawn_access_no: '8675309') }
      let(:ineligible_vehicle_agreement_no) { ineligible_vehicle.agreement_no }

      let!(:invalid_vehicle) { FactoryGirl.create(:sold_vehicle, promise_250: nil) }
      let(:invalid_vehicle_offer) { invalid_vehicle.promise_offers.first }
      let(:invalid_vehicle_agreement_no) { invalid_vehicle.agreement_no }

      it 'should not blow up when visiting the promise offer page with ineligible cars' do
        allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([ineligible_vehicle_agreement_no])
        view_vehicles_by_pawn_access_number_as_kiosk_user ineligible_vehicle.pawn_access_no

        expect(page).to have_content ineligible_vehicle.make.titleize
        expect(page).not_to have_content I18n.t('vehicles.ratings.link')
      end

      it 'should show invalid vehicles as ineligible for purchase' do
        allow_any_instance_of(Astro::BUYER_AUTHORIZATION_CLASS).to receive(:get_agreement_numbers).and_return([invalid_vehicle_agreement_no])
        ineligibility_condition = IneligibilityCondition.create!(condition_id: IneligibilityCondition::INVALID_PRICING, notice: 'Foobarbaz')
        view_vehicles_by_pawn_access_number_as_kiosk_user invalid_vehicle.pawn_access_no

        expect(page).to have_content invalid_vehicle.vnum
        expect(page).to have_content ineligibility_condition.notice
      end
    end

    context 'with a left lot vehicle' do
      before do
        Settings.query_ods = true
        allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(true)
        allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(true)
      end

      it 'should include copy for left lot vehicles' do
        allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_paid_by_floor_plan?).and_return(false)
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(page).to have_content 'has already left the pawn lot'
      end

      it 'should not include copy for left vehicle paid by floor plan' do
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(page).not_to have_content 'has already left the pawn lot'
      end

      it 'should include copy for floor plan when paid by floor plan' do
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(page).to have_css('.badge', text: I18n.t('vehicles.floor_plan'))
      end

      it 'should allow purchase of checked out vehicle paid by floor plan' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_acura_for_250_miles_and_7_additional_days
        click_next
        expect(page).to have_content 'Acura'
      end
    end

    context 'with a past time vehicle' do
      it 'should include copy for checked out vehicles' do
        honda.update_attributes(must_be_promised_at: 1.day.ago)
        view_vehicles_by_pawn_access_number_as_kiosk_user

        expect(page).to have_content I18n.t('promise_offers.vehicles.past_cutoff_copy')
      end
    end

    describe 'coupons' do
      let(:amount) { 10.0 }
      let(:coupon) { FactoryGirl.create(:coupon, amount: amount) }
      let(:price) { 100.0 }

      before do
        Settings.coupons_enabled = true
        honda.update_attributes!(promise_250: price)
      end

      it 'updates promise price UI when coupon is applied', js: true do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_250_miles_and_0_additional_days
        click_next

        find('[rel~=coupon-code]').set(coupon.code)
        find('[rel~=apply-coupon]').click

        expect(page).to have_content('$ 90')
      end

      it 'updates promise price UI when coupon is applied irrespective of coupon-code case', js: true, selenium: true do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_250_miles_and_0_additional_days
        click_next

        find('[rel~=coupon-code]').set(coupon.code.downcase)
        find('[rel~=apply-coupon]').click

        expect(page).to have_content('$ 90')
      end

      it 'prevents the UI from reflecting prices less that $ 0', js: true do
        honda.update_attributes!(promise_250: 10.0)
        coupon.update_attributes!(amount: 11.0)

        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_250_miles_and_0_additional_days
        click_next

        find('[rel~=coupon-code]').set(coupon.code)
        find('[rel~=apply-coupon]').click

        expect(page).to have_content('$ 0')
      end

      context 'a coupon is applied', js: true do
        before do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_0_additional_days
          click_next

          find('[rel~=coupon-code]').set(coupon.code)
          find('[rel~=apply-coupon]').click
        end

        it 'resets the view, removnumg the coupon' do
          find('[rel~=reset-coupon]').click

          expect(page).to have_content('$ 100')
        end

        it 'applies the coupon again that has been reset' do
          find('[rel~=reset-coupon]').click
          find('[rel~=coupon-code]').set(coupon.code)
          find('[rel~=apply-coupon]').click

          expect(page).to have_content('$ 90')
        end
      end

      context 'an unused coupon' do
        let(:coupon) { FactoryGirl.create(:coupon) }

        it 'is consumed when purchase is completed' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_acura_for_250_miles_and_7_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code)
          click_next

          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_7_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code)
          click_next

          error_message = I18n.t('coupons.consumed_error', code: coupon.code)
          expect(page).to have_content(error_message)
        end

        it 'reduces the price by the coupon amount' do
          honda.update_attributes!(promise_250: 100)
          coupon.update_attributes!(amount: 10)

          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_0_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code)
          click_next

          expect(page).to have_content('$ 90')
        end

        it 'does not allow the price to be less than 0' do
          honda.update_attributes!(promise_250: 100)
          coupon.update_attributes!(amount: 110)

          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_0_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code)
          click_next

          expect(page).to have_content('$ 0')
        end
      end

      context 'an expired coupon' do
        let(:coupon) { FactoryGirl.create(:coupon, :expired) }

        it 'is rejected when attemtped to use' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_7_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code)
          click_next

          error_message = I18n.t('coupons.expired_error', code: coupon.code)
          expect(page).to have_content(error_message)
        end
      end

      context 'a non-existent coupon' do
        it 'is rejected when attemtped to use' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_7_additional_days
          click_next
          find('[rel~=coupon-code]').set('AAAAA')
          click_next

          error_message = I18n.t('coupons.missing_error', code: 'AAAAA')
          expect(page).to have_content(error_message)
        end
      end

      context 'no coupon is entered' do
        it 'does not blow up' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_7_additional_days
          click_next

          click_next

          error_message = I18n.t('coupons.missing_error', code: '')
          expect(page).not_to have_content(error_message)
        end
      end

      context 'ignore coupon code case' do
        let(:coupon) { FactoryGirl.create(:coupon) }

        it 'updates promise price when coupon is applied irrespective of coupon-code case' do
          view_vehicles_by_pawn_access_number_as_kiosk_user
          select_honda_for_250_miles_and_0_additional_days
          click_next
          find('[rel~=coupon-code]').set(coupon.code.downcase)
          click_next

          expect(page).to have_content('$ 88')
        end
      end
    end
  end

  context 'as a super user' do
    before do
      honda.update_attributes(must_be_promised_at: 1.day.ago)
      log_in_super_user
      click_on I18n.t('navigation.purchase')
      click_on I18n.t('forms.pawn_access.input_label')
      fill_in 'pawn_access_number', with: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER
      click_on I18n.t('forms.enter')
    end

    it 'should allow super users to buy promises on past-time vehicles' do
      expect(page).not_to have_content I18n.t('promise_offers.vehicles.past_cutoff_copy')
      select_honda_for_500_miles_and_14_additional_days
      click_next
      expect(page).to have_content 'Honda'
    end
  end
end
