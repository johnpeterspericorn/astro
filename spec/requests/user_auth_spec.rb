# encoding: utf-8
require 'requests_helper'

describe 'User Authentication' do
  describe 'token authentication' do
    let!(:buyer_user) { FactoryGirl.create :buyer_user }
    let(:invalid_auth_token) { '123lol' }

    before do
      buyer_user.reset_authentication_token!
    end

    it 'authenticates successfully with valid token' do
      visit root_path(auth_token: buyer_user.authentication_token)

      expect(page).to have_content I18n.t('forms.logout')
      expect(current_path).to eq(root_path)
    end

    it 'fails to authenticate with invalid token' do
      visit root_path(auth_token: invalid_auth_token)

      expect(current_path).to eq(new_user_session_path)
    end
  end
end
