# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Return Invoices Dashboard' do
  include_context 'admin steps'

  describe '# returns dashboard data' do
    include_context 'returns_dashboard_data'
    let(:today) { DateTime.current }

    before do
      login_to_admin_space
      click_on I18n.t('admin.navigation.returns_dashboard')
    end

    context 'Search Filter' do
      it 'displays default data for the reports' do
        number_of_returns = 2
        Timecop.freeze(today) do
          expect(find_field(:filters_date_from).value).to eq(today.beginning_of_day.to_s)
          expect(find_field(:filters_date_to).value).to eq(today.to_s)
          expect(page).to have_content('5241412')
          expect(page).to have_content('5241413')
          expect(page).to have_content SasDataHelper::DEFAULT_VEHICLE_MAKE
          expect(page).to have_content 'Aaaaa'
          expect(page).to have_content number_of_returns
        end
      end

      it 'shows returns by customer' do
        fill_in 'Date From', with: Date.current
        fill_in 'Date To', with: today
        click_on I18n.t('forms.generic_submission')
        expect(page).to have_content('Agreementship Name')
        expect(page).to have_content('Number of Returns Initiated')
      end

      it 'shows returns by location' do
        fill_in 'Date From', with: Date.current
        fill_in 'Date To', with: today
        click_on I18n.t('forms.generic_submission')
        expect(page).to have_content('Number of Returns Initiated')
        expect(page).to have_content('Average Vehicle Price')
      end

      it 'shows high dollar returns' do
        fill_in 'Date From', with: Date.current
        fill_in 'Date To', with: today
        click_on I18n.t('forms.generic_submission')
        expect(page).to have_content 'VNUM'
        expect(page).to have_content 'Year, Make, Model'
      end
    end
  end
end
