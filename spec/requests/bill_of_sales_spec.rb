# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'

describe 'Bill of Sales' do
  include_context 'return invoice steps'

  describe 'Return Workflow for Canadian returns' do
    let!(:agreement) { FactoryGirl.create(:agreement_information) }
    let(:canadian_location) { FactoryGirl.create(:canadian_location) }
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, agreement_no: 5_000_000, seller_paid: true, location_initials: canadian_location.initials, promise_250: 400, promise_500: 800) }

    vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

    context 'logged in as a super user', vcr: vcr_options do
      before do
        VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

        stub_buyer_authorization
        allow_any_instance_of(Promise).to receive(:validate_distance_and_length).and_return(true)
        allow_any_instance_of(PromiseOffer).to receive(:check_pending_conditions).and_return(true)
        log_in_super_user
      end

      after do
        VCR.eject_cassette
      end

      it 'sees the bill of sale form' do
        return_canadian_vehicle(acura)
        expect(page).to have_content 'Bill Of Sale'
      end

      it 'throws validation error on verification page if incomplete' do
        initiate_return_incomplete_verification(acura)
        expect(page).to have_content "Odometer on return can't be blank"
        expect(page).to have_content 'Please select a title status'
      end

      it 'throws validation error on incomplete bill of sale form' do
        return_canadian_vehicle(acura)
        complete_bill_of_sale
        expect(page).to have_content 'Please fill all the required fields marked with an *'
      end

      it 'should continue on to the return invoice form page when the bill of sale info is corrected' do
        return_canadian_vehicle(acura)
        complete_bill_of_sale
        enter_bill_of_sale_details
        complete_bill_of_sale
        expect(page).to have_content 'Confirm Return Information'
      end

      it 'moves on to return invoice form on proper submission of bill of sale' do
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        complete_bill_of_sale
        expect(page).to have_content 'Confirm Return Information'
      end

      it 'displays bill of sale data on the VRA page' do
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        complete_bill_of_sale
        click_on I18n.t('forms.finish')
        expect(page).to have_content 'Bill Of Sale'
        expect(page).to have_content I18n.t('return_funnel.bill_of_sale.terms_and_conditions')
      end

      it 'does not error out on 500 character size limit for bill of sales data' do
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        val = 'a'
        500.times { val << 'a' }
        fill_in 'bill_of_sale[selling_sales_person_name]', with: val
        complete_bill_of_sale
        click_on I18n.t('forms.finish')
        expect(page).to have_content 'Bill Of Sale'
        expect(page).to have_content I18n.t('return_funnel.bill_of_sale.terms_and_conditions')
      end

      it 'does not error out on 1000 character size limit for bill of sales data' do
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        val = 'a'
        1000.times { val << 'a' }
        fill_in 'bill_of_sale[selling_sales_person_name]', with: val
        complete_bill_of_sale
        click_on I18n.t('forms.finish')
        expect(page).to have_content 'Bill Of Sale'
        expect(page).to have_content I18n.t('return_funnel.bill_of_sale.terms_and_conditions')
      end

      it 'displays correct tax and price with fee for the bill of sale data on the VRA page for default tax rate 15%' do
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        complete_bill_of_sale
        click_on I18n.t('forms.finish')
        expect(page).to have_content '11,905.00'
        expect(page).to have_content '1,785.75'
      end
    end
  end

  describe 'Canadian taxes' do
    let!(:agreement) { FactoryGirl.create(:agreement_information) }
    let(:canadian_location) { FactoryGirl.create(:canadian_location, tax_rate: 20) }
    let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, agreement_no: 5_000_000, seller_paid: true, location_initials: canadian_location.initials, promise_250: 400, promise_500: 800) }

    vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

    context 'logged in as a super user', vcr: vcr_options do
      before do
        VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)

        stub_buyer_authorization
        allow_any_instance_of(Promise).to receive(:validate_distance_and_length).and_return(true)
        allow_any_instance_of(PromiseOffer).to receive(:check_pending_conditions).and_return(true)
        log_in_super_user
      end

      after do
        VCR.eject_cassette
      end

      it 'displays correct tax and price with fee for the bill of sale data on the VRA page for tax rate 20%' do
        canadian_location = FactoryGirl.create(:canadian_location, tax_rate: 20)
        acura = FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, agreement_no: 5_000_000, seller_paid: true, location_initials: canadian_location.initials, promise_250: 400, promise_500: 800)
        return_canadian_vehicle(acura)
        enter_bill_of_sale_details
        complete_bill_of_sale
        click_on I18n.t('forms.finish')
        expect(page).to have_content '11,905.00'
        expect(page).to have_content '2,381.00'
      end
    end
  end
end
