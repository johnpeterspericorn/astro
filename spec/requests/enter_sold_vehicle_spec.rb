# encoding: utf-8
require 'requests_helper'
require_relative './steps/navigation_steps'
require_relative './steps/admin_steps'

describe 'Sold Vehicle Web Load' do
  let(:sold_vehicle) { FactoryGirl.build(:sold_vehicle) }
  let!(:astroheim_pawn_location) { FactoryGirl.create(:svaa_location) }
  let!(:non_astroheim_location) { FactoryGirl.create(:oklahoma_location) }

  it 'does not display the link to sold vehicle for non vehicle creators' do
    log_in_kiosk_user
    expect(page).not_to have_content('Enter New Sold Vehicle')
  end

  describe 'Sold Vehicle creation ', js: true do
    context 'as vehicle creator logged in' do
      before do
        log_in_super_user(admin: true, vehicle_creator: true)
        click_on 'Enter New Sold Vehicle'

        fill_in 'purchased_at', with: sold_vehicle.purchased_at
        fill_in 'sold_vehicle[vehicle_purchase_price]', with: sold_vehicle.vehicle_purchase_price

        select sold_vehicle.year.to_s, from: 'sold_vehicle[year]'
        fill_in 'sold_vehicle[vnum]', with: sold_vehicle.vnum
        fill_in 'sold_vehicle[lane_no]', with: sold_vehicle.lane_no
        fill_in 'sold_vehicle[run_no]', with: sold_vehicle.run_no
        page.uncheck('sold_vehicle[automatic_purchase]')
        fill_in 'sold_vehicle[make]', with: sold_vehicle.make
        fill_in 'sold_vehicle[sale_no]', with: sold_vehicle.sale_no
      end

      it 'creates a sold vehicle with required parameters - sblu, universal_no and work_order_no for astroheim pawn locations' do
        select astroheim_pawn_location.initials, from: 'sold_vehicle[location_initials]'
        fill_in 'sold_vehicle[universal_no]', with: sold_vehicle.universal_no
        fill_in 'sold_vehicle[work_order_number]', with: sold_vehicle.work_order_number
        fill_in 'sold_vehicle[sblu]', with: sold_vehicle.sblu

        click_button 'Create Sold vehicle'
        wait_for_ajax
        visit admin_sold_vehicles_path

        expect(page).to have_content(sold_vehicle.make)
        expect(page).to have_content(sold_vehicle.vnum)
      end

      it 'lets create a sold vehicle with sblu, universal_no and work_order_number for non-astroheim pawn locations' do
        select non_astroheim_location.initials, from: 'sold_vehicle[location_initials]'
        fill_in 'sold_vehicle[universal_no]', with: sold_vehicle.universal_no
        fill_in 'sold_vehicle[work_order_number]', with: sold_vehicle.work_order_number
        fill_in 'sold_vehicle[sblu]', with: sold_vehicle.sblu

        click_button 'Create Sold vehicle'
        wait_for_ajax
        visit admin_sold_vehicles_path

        expect(page).to have_content(sold_vehicle.make)
        expect(page).to have_content(sold_vehicle.vnum)
      end
    end
  end
end
