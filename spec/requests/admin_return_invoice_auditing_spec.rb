# encoding: utf-8
require 'requests_helper'

describe 'Auditing return invoices' do
  context 'an unaudited return invoice' do
    describe 'opening an audit' do
      let!(:return_invoice) { FactoryGirl.create(:return_invoice) }
      let(:audit_reason) { I18n.t('return_invoice.audit.reasons')[1] }
      let(:email) { 'abc@xyz.com, harry_potter@hogwarts.com' }

      it 'accepts input for reason description' do
        log_in_super_user(admin: true)
        visit edit_admin_return_invoice_path(return_invoice)
        click_on 'New Audit'

        fill_in 'audit_emails', with: email
        fill_in 'audit_reasons_attributes_0_description', with: audit_reason
        click_on 'Open Audit'

        expect(AuditReason.last.description).to eq(audit_reason)
      end

      it 'sets the resolution date to 2 business days from today' do
        friday  = Date.parse('2018-04-18')
        tuesday = Date.parse('2018-04-22')

        Timecop.freeze(friday) do
          log_in_super_user(admin: true)
          visit edit_admin_return_invoice_path(return_invoice)

          click_on 'New Audit'
          fill_in :audit_emails, with: email
          select I18n.t('return_invoice.audit.reasons')[1], from: 'description'
          click_on 'Open Audit'

          return_invoice.reload
          expect(return_invoice.audit.resolved_by).to eq(tuesday)
          expect(return_invoice.audit.emails).to eq(email)
        end
      end
    end
  end

  context 'an audited return invoice' do
    let!(:return_invoice) { FactoryGirl.create(:return_invoice) }
    let(:email) { 'abc@xyz.com' }

    before do
      log_in_super_user(admin: true)
      return_invoice.create_audit!(resolved_by: Date.current, emails: email)
      visit admin_audited_returns_path
    end

    it 'allows admin to update resolution date' do
      new_email = 'xyz@abc.com'
      new_date = 10.days.from_now.to_date

      click_on 'Edit Audit'
      fill_in :audit_emails, with: new_email
      select_date(new_date, from: :audit_resolved_by)
      click_on I18n.t('forms.update')

      expect(page).to have_content(new_date)
      expect(return_invoice.reload.audit_emails).to eq(new_email)
    end

    describe 'closing an audit' do
      it 'may be cleared as no problem' do
        visit admin_audited_returns_path
        click_on 'Clear Audit'

        expect(page).to have_content('Cleared')
      end

      it 'may be flagged as problematic' do
        visit admin_audited_returns_path
        click_on 'Flag Audit'
        click_on 'Close Audit With Problem'

        expect(page).to have_content('Flagged')
      end
    end

    describe 'closing audit with problem' do
      let(:emails) { 'john@example.com,jane@example.com' }

      before do
        visit admin_audited_returns_path
        click_on 'Flag Audit'
      end

      it 'should update emails provided for confirmation email' do
        fill_in :audit_emails, with: emails
        click_on 'Close Audit With Problem'
        audit = return_invoice.audit.reload
        expect(audit.emails).to eq(emails)
      end
    end
  end
end
