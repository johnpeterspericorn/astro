# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'
require_relative './steps/manage_dashboard_action_steps'

describe Admin::NetworkPlusOffersController do
  include_context 'navigation steps'
  include_context 'admin steps'
  include_context 'manage dashboard actions steps'

  let!(:agreement_info) { FactoryGirl.create(:agreement_information, agreement_no: '5241420') }
  let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }
  let(:emails) { 'sfdfdf.fghfh@sdasds.com' }
  let(:existing_network_rate) { '12' }
  let(:network_plus_surcharge) { '100' }
  let(:expiration_date) { 5.days.from_now }

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    click_on I18n.t('admin.navigation.dashboard_scores')
    click_on I18n.t('admin.navigation.dashboard_score_management.manage')
  end

  describe 'Network Plus Offer Statuses' do
    let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }

    it 'successfully creates offer if valid offer does not exist' do
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      expect(NetworkPlusOffer.last.state).to eq('confirmed')
      expect(page).to have_content('Network Plus offer created!')
    end

    it 'successfully creates offer if there are existing rejected offers' do
      FactoryGirl.create(:network_plus_offer, state: 'rejected')
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      expect(page).to have_content('Network Plus offer created!')
    end

    it 'displays error if valid offer exists' do
      FactoryGirl.create(:network_plus_offer, state: 'confirmed')
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      expect(page).to have_content('The agreement already has an offer')
    end

    it 'displays error if approved offer exists' do
      FactoryGirl.create(:network_plus_offer, state: 'approved')
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      expect(page).to have_content('The agreement already has an offer')
    end
  end

  describe 'Manage Network Plus Offers' do
    it 'shows a network plus offer in the listing' do
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content('NetworkPlusOffer', count: 1)
      expect(page).to have_content('confirmed')
      expect(page).to have_content("existing_network_rate : #{existing_network_rate}")
    end

    it 'shows 2 network plus offers in the listing' do
      FactoryGirl.create(:network_plus_offer, state: 'rejected')
      get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
      visit admin_index_path
      click_on I18n.t('admin.navigation.dashboard_scores')
      click_on I18n.t('admin.navigation.dashboard_score_management.manage')
      expect(page).to have_content('NetworkPlusOffer', count: 2)
      expect(page).to have_content('confirmed')
      expect(page).to have_content('rejected')
    end
  end
end
