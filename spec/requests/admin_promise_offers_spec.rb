# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin Promise Offers View' do
  include_context 'navigation steps'
  include_context 'admin steps'

  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase) }
  let(:promise_offer) { promise_purchase.promise_offer }

  before do
    visit_promise_offer_as_editor
  end

  describe 'List view of offers' do
    context 'VNUM filter' do
      let(:unknown_vnum) { SecureRandom.hex[0..16].upcase }

      before do
        click_on I18n.t('admin.navigation.all_promise_offers')
      end

      it 'should list offers for an existing VNUM' do
        fill_in 'filters_vnum', with: promise_offer.vnum
        click_on I18n.t('forms.filter')
        expect(page).to have_content promise_offer.vnum
      end

      it 'should not list offers for a non existing VNUM' do
        expect(page).to have_content promise_offer.vnum

        fill_in 'VNUM', with: unknown_vnum
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content promise_offer.vnum
      end

      it 'filters offers for an existing agreement#' do
        fill_in 'Agreement#', with: promise_offer.agreement_no
        click_on I18n.t('forms.filter')
        expect(page).to have_content promise_offer.agreement_no
      end

      it 'does not list offers for a non existing agreement#' do
        unknown_agreement_no = 12_345

        fill_in 'Agreement#', with: unknown_agreement_no
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content unknown_agreement_no
      end

      it 'filters offers for a given date range' do
        fill_in 'Date From', with: Time.current.beginning_of_day
        fill_in 'Date To', with: Time.current.end_of_day
        click_on I18n.t('forms.filter')
        expect(page).to have_content promise_offer.vnum
      end

      it 'does not list offers outside date range' do
        fill_in 'Date From', with: Date.tomorrow.beginning_of_day
        fill_in 'Date To', with: Date.tomorrow.end_of_day
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content promise_offer.vnum
      end
    end
  end

  describe 'Editing unpurchased offers' do
    let!(:ineligibility_condition_1) { FactoryGirl.create(:ineligibility_condition, condition_id: 1, notice: 'ineligible as promise cutoff date expired') }
    let!(:ineligibility_condition_2) { FactoryGirl.create(:ineligibility_condition, condition_id: 2, notice: 'ineligible due to some reason') }
    let!(:unpurchased_offer) { FactoryGirl.create(:promise_offer, ineligibility_condition: ineligibility_condition_1.condition_id, promised_at: Time.current, must_be_promised_at: Time.current + 6.days, left_lot: true) }

    before do
      click_on I18n.t('admin.navigation.all_promise_offers')
    end

    it 'updates agreement information' do
      agreementship = '5421217'
      agreement = '100686867'
      click_edit
      fill_in 'promise_offer[pawn_information_attributes][agreement_no]', with: agreementship
      fill_in 'promise_offer[pawn_information_attributes][pawn_access_no]', with: agreement
      click_on 'Apply Updates'

      unpurchased_offer.reload
      expect(unpurchased_offer.agreement_no).to eq(agreementship)
      expect(unpurchased_offer.pawn_access_no).to eq(agreement)
    end

    it 'updates ineligibility information' do
      click_edit
      select(ineligibility_condition_2.notice, from: 'promise_offer_ineligibility_condition')
      click_on 'Apply Updates'

      unpurchased_offer.reload
      expect(unpurchased_offer.ineligibility_condition).to eq(ineligibility_condition_2.condition_id)
    end

    it 'updates days information' do
      click_edit
      fill_in 'promise_offer_promise_250', with: 250
      fill_in 'promise_offer_additional_day', with: 6
      click_on 'Apply Updates'

      unpurchased_offer.reload
      expect(unpurchased_offer.promise_250).to eq(250.0)
      expect(unpurchased_offer.additional_day).to eq(6)
    end

    it 'updates discount' do
      amount = 12.3
      description = ':moneybag:'

      unpurchased_offer.create_discount!
      click_edit
      fill_in 'promise_offer[discount_attributes][amount]', with: amount
      fill_in 'promise_offer[discount_attributes][description]', with: description
      click_on 'Apply Updates'

      unpurchased_offer.reload

      expect(unpurchased_offer.discount.amount).to eq(amount)
      expect(unpurchased_offer.discount.description).to eq(description)
    end

    # This protects against regression identified during feature acceptance
    it 'preserves existing discount' do
      unpurchased_offer.create_discount!(amount: 12.3)
      click_edit
      click_on 'Apply Updates'

      unpurchased_offer.reload
      expect(unpurchased_offer.discount.amount).to eq(12.3)
    end
  end

  describe 'Editing promises' do
    let(:new_days) { 21 }
    let(:new_miles) { 500 }
    let(:new_price) { 10_101 }
    let(:new_email) { 'foo1990@example.com' }
    let(:new_odometer_reading) { 9999 }

    before do
      click_on I18n.t('admin.navigation.promise_purchases')
    end

    it 'updates promise values' do
      click_on I18n.t('forms.promise_offers.edit')
      page.find('#promise_offer_days_selected').select(new_days.to_s)
      page.find('#promise_offer_miles_selected').select(new_miles.to_s)
      fill_in 'Email', with: new_email

      click_on 'Apply Updates'

      promise_offer.reload
      expect(promise_offer.days_selected).to eq(new_days)
      expect(promise_offer.miles_selected).to eq(new_miles)
      expect(promise_offer.emails).to eq(new_email)
    end

    it 'updates odometer reading' do
      click_on I18n.t('forms.promise_offers.edit')
      fill_in 'Odometer', with: new_odometer_reading
      click_on 'Apply Updates'

      promise_offer.reload
      expect(promise_offer.odometer_reading).to eq(new_odometer_reading)
    end

    it 'updates vehicle purchase price' do
      new_price = 200
      click_on I18n.t('forms.promise_offers.edit')
      fill_in 'Vehicle Purchase Price', with: new_price
      click_on 'Apply Updates'

      promise_offer.reload
      expect(promise_offer.vehicle_purchase_price).to eq(new_price)
    end

    it 'updates new_must_be_promised_at reading' do
      new_time = DateTime.current.beginning_of_day

      click_on I18n.t('forms.promise_offers.edit')
      select_date_and_time(new_time, from: :promise_offer_must_be_promised_at)
      click_on 'Apply Updates'

      promise_offer.reload
      expect(promise_offer.must_be_promised_at).to eq(new_time)
    end

    it 'updates transportation coverage' do
      coverage_value = 1234.5
      click_on I18n.t('forms.promise_offers.edit')
      fill_in I18n.t('promise_offers.transportation_coverage'), with: coverage_value
      click_on 'Apply Updates'

      promise_offer.reload
      expect(promise_offer.transport_reimbursement).to eq(coverage_value)
    end

    context 'price is updated' do
      let(:charge) { double 'Astroheim::Charge' }
      let(:old_price) { 99_999 }

      before do
        allow(Astroheim::Charge).to receive(:new).and_return(charge)
        allow(charge).to receive(:submit!)

        promise_offer.update_attributes!(promise_price: old_price)

        click_on I18n.t('forms.promise_offers.edit')
        fill_in 'Price', with: new_price
        Delayed::Worker.delay_jobs = false
      end

      it 'updates the promise price' do
        click_on 'Apply Updates'

        promise_offer.reload
        expect(promise_offer.promise_price).to eq(new_price)
      end

      context 'when AS/400 is selected as a charge option' do
        let(:adjustment) { new_price - old_price }
        it 'creates an adjustment charge for the price change' do
          expect(Astroheim::Charge)
            .to receive(:new)
            .with(hash_including(amount: adjustment))

          choose 'charge_as400'
          click_on 'Apply Updates'
        end
      end

      context 'when manual charging is selected' do
        it 'does not send a charge' do
          expect(Astroheim::Charge)
            .not_to receive(:new)

          choose 'charge_'
          click_on 'Apply Updates'
        end
      end
    end

    context 'when confirmation should be updated' do
      it 're-sends the confirmation email' do
        click_on I18n.t('forms.promise_offers.edit')
        fill_in 'Email', with: new_email
        check 'Update Confirmation'
        expect do
          click_on 'Apply Updates'
        end.to change { enqueued_deliveries.count }.by(1)
      end
    end

    context 'when the vehicle has left lot' do
      before do
        promise_offer.update_attributes!(left_lot: true)
      end

      it 'disables the AS/400 option' do
        click_on I18n.t('forms.promise_offers.edit')
        expect(page).to have_css('#charge_as400[disabled]')
      end
    end

    context 'when the offer is pending' do
      before do
        promise_offer.update_attributes!(promise_status: 'pending')
      end

      it 'disables the AS/400 option' do
        click_on I18n.t('forms.promise_offers.edit')
        expect(page).to have_css('#charge_as400[disabled]')
      end
    end
  end
end
