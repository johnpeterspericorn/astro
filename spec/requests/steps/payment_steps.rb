# encoding: utf-8
shared_context 'payment steps' do
  let(:credit_card_number) { '4242424242424242' }
  let(:cvc)                { 123 }
  let(:email)              { "fake_#{Time.current.to_i}@agreementshield.com" }
  let(:expiration_year)    { Date.current.year + 1 }
  let(:expiration_date)    { "01/#{expiration_year}" }
  let(:zipcode)            { '30319' }
  let(:country)            { 'United States' }
  let(:price) { promise_purchase.credit_card_payment_price }

  def submit_payment
    click_on 'Pay with Card'
    expect(page).to have_selector 'iframe'
    fill_in 'Email',            with: email
    fill_in 'Name',             with: 'Fake Name'
    fill_in 'Street',           with: '2002 Summit Blvd #1001'
    fill_in 'City',             with: 'Chicago'
    fill_in 'ZIP Code',         with: zipcode
    # fill_in 'Country',          with: country
    click_on 'Payment Info'
    expect(page).to have_selector 'iframe'
    fill_in 'Card number',      with: credit_card_number
    fill_in 'Expiry',           with: expiration_date
    fill_in 'CVC',              with: cvc
    click_on "Pay $#{price}.00"
  end

  def stub_fees_to_allow_payments
    allow(promise_purchase).to receive(:fees).and_return(fees)
    allow(fees).to receive(:nonreimbursable_fees).and_return({})
  end
end
