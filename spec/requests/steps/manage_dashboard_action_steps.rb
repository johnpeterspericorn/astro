# encoding: utf-8
shared_context 'manage dashboard actions steps' do
  # network plus action

  def submit_network_plus_action(emails)
    fill_in 'emails', with: emails
    choose('action_type_network_plus_addition')
    submit_form
  end

  def submit_network_plus_form(existing_network_rate, network_plus_surcharge, expiration_date)
    fill_in 'network_plus_offer[existing_network_rate]', with: existing_network_rate
    fill_in 'network_plus_offer[network_plus_surcharge]', with: network_plus_surcharge
    fill_in 'network_plus_offer[expiration_date]', with: expiration_date
    submit_form
  end

  def get_network_plus_offer_approval_page(emails, existing_network_rate, network_plus_surcharge, expiration_date)
    submit_network_plus_action(emails)
    submit_network_plus_form(existing_network_rate, network_plus_surcharge, expiration_date)
  end

  # performance alert action

  def submit_performance_alert_action_type(emails)
    fill_in 'emails', with: emails
    choose('action_type_performance_alert')
    submit_form
  end

  def submit_performance_alert_form(return_rate, lpc, follow_up_date, rel_manager)
    find(:css, '#performance_alert_return_rate').set(return_rate)
    find(:css, '#performance_alert_lpc').set(lpc)
    select rel_manager, from: 'performance_alert_relationship_manager_id'
    fill_in 'performance_alert[follow_up_date]', with: follow_up_date
    submit_form
  end

  def process_performance_alert(emails, return_rate, lpc, follow_up_date, rel_manager)
    submit_performance_alert_action_type(emails)
    submit_performance_alert_form(return_rate, lpc, follow_up_date, rel_manager)
  end

  # rate change action

  def submit_rate_change_action_type(emails)
    fill_in 'emails', with: emails
    choose('action_type_rate_change')
    submit_form
  end

  def submit_rate_change_form(return_rate, effective_date, rel_manager)
    find(:css, '#rate_change_return_rate').set(return_rate)
    select rel_manager, from: 'rate_change_relationship_manager_id'
    fill_in 'rate_change[effective_date]', with: effective_date
    submit_form
  end

  def process_rate_change(emails, return_rate, effective_date, rel_manager)
    submit_rate_change_action_type(emails)
    submit_rate_change_form(return_rate, effective_date, rel_manager)
  end

  # common

  def submit_form
    click_on 'Submit'
  end

  def manage_agreement_performance
    click_on I18n.t('admin.navigation.dashboard_scores')
    click_on I18n.t('admin.navigation.dashboard_score_management.manage')
  end
end
