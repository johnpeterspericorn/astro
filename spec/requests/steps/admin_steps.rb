# encoding: utf-8
shared_context 'admin steps' do
  def login_to_admin_space
    log_in_super_user(admin: true)
    visit admin_index_path
  end

  def visit_as_feature_flag_editor
    log_in_super_user(admin: true, feature_flag_editor: true)
    visit admin_index_path
  end

  def visit_promise_offer_as_editor
    log_in_super_user(admin: true, promise_admin_editor: true)
    visit admin_index_path
  end

  def login_as_user_admin_editor
    log_in_super_user(admin: true, user_admin_editor: true)
    visit admin_index_path
  end

  def login_as_finance_admin_viewer
    log_in_super_user(admin: true, finance_admin_viewer: true)
    visit admin_index_path
  end

  def login_as_flat_rate_inquiry_viewer
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
  end

  def login_as_network_plus_approver
    log_in_super_user(admin: true, external_vehicle_approver: true)
    visit admin_index_path
  end

  def visit_promise_offer_viewer
    login_to_admin_space
    click_on I18n.t('admin.navigation.all_promise_offers')
  end

  def visit_pending_promises_viewer
    login_to_admin_space
    click_on I18n.t('admin.navigation.pending_offers')
  end

  def visit_payments_viewer
    login_to_admin_space
    click_on I18n.t('admin.navigation.payments')
  end

  def add_new_pawn_location_email(email)
    current_emails = find_field('Emails').value
    fill_in 'Emails', with: "#{current_emails}, #{email}"
    click_on 'Update'
    click_on name
  end

  def add_new_accounting_email(email)
    fill_in 'Accounting Email', with: email.to_s
    click_on 'Update'
    click_on name
  end

  def expect_email_field_to_be(email)
    expect(page.find_field('Return Emails').value).to eq(email)
  end

  def expect_accounting_email_to_be(email)
    expect(page.find_field('Accounting Email').value).to eq(email)
  end

  def navigate_to_user_index
    visit admin_index_path
    click_on I18n.t('admin.navigation.users')
  end

  def edit_user_by_username(username)
    navigate_to_user_index
    click_on username
  end

  def page_should_be_user_index
    expect(current_path).to eql admin_users_path
  end

  def page_should_be_admin_index_page
    expect(current_path).to eql admin_index_path
  end

  def navigate_to_pawn_location_index
    visit admin_index_path
    click_on I18n.t('admin.navigation.pawn_locations')
  end

  def page_should_be_pawn_location_index
    expect(current_path).to eql admin_pawn_locations_path
  end

  def use_filters
    click_on I18n.t('admin.sold_vehicles.filter_copy')
  end

  def data_table
    find('.data-table')
  end
end
