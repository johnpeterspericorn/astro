# encoding: utf-8
shared_context 'pawn access number steps' do
  def click_pawn_access
    click_on I18n.t('forms.pawn_access.input_label') if Settings.show_badge_ui
  end

  def page_should_be_promise_offers_page
    expect(page).to have_css('#vehicles_index_view')
  end

  def page_should_have_agreement_name(agreement_name)
    expect(page).to have_content agreement_name
  end
end
