# encoding: utf-8
shared_context 'presale vehicle steps' do
  def browse_ove_vehicles
    click_on I18n.t('promise_estimates.filters.ove')
  end

  def browse_presale_vehicles_by_vnum
    click_on I18n.t('promise_estimates.filters.vnum')
  end

  def submit_presale_form
    click_on 'Search'
  end
end
