# encoding: utf-8
shared_context 'badge number steps' do
  def click_badge_number
    click_on I18n.t('forms.badge_number.input_label')
  end

  def enter_badge_number(number)
    fill_in 'badge_number', with: number
    click_button I18n.t('forms.enter')
  end

  def page_should_be_badge_number_input_page
    expect(page).to have_selector(:css, '#badge_number')
  end

  def page_should_have_badge_number(badge_number)
    expect(page).to have_content I18n.t('promise_offers.greetings.badge_number', badge_number: badge_number)
  end
end
