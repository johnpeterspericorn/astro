# encoding: utf-8
shared_context 'agreement number steps' do
  def click_agreement_number
    click_on agreement_input_label
  end

  def enter_agreement_number(number)
    fill_in 'agreement_number', with: number
    click_button I18n.t('forms.enter')
  end

  def page_should_be_agreement_number_input_page
    expect(page).to have_selector(:css, '#agreement_number')
  end

  def page_should_have_agreement_number(agreement_number)
    expect(page).to have_content agreement_greeting(agreement_number)
  end

  def agreement_input_label
    I18n.t('forms.agreement_number.input_label')
  end

  def agreement_greeting(agreement_number)
    I18n.t('promise_offers.greetings.agreement_number', agreement_number: agreement_number)
  end
end
