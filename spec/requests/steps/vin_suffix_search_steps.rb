# encoding: utf-8
shared_context 'vnum suffix search steps' do
  def search_by_vnum
    click_on I18n.t('promise_estimates.filters.vnum')
  end

  def submit_vnum_suffix_form
    click_on 'Search'
  end
end
