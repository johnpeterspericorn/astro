# encoding: utf-8
shared_context 'return invoice steps' do
  let(:pawn_access_no) { SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER }
  let(:max_mileage) { 250 }
  let(:days_selected) { 7 + Promise::BASE_DURATION - 1 } # -1 is to account for the Timecop interaction with purchasing promises a day ago by default
  let(:vehicle_css) { 'ul.vehicle_list.return > li' }
  let(:unable_to_sell_reason) { I18n.t('return_invoice.return_reasons.unable_to_sell') }
  let(:no_answer_reason) { I18n.t('return_invoice.return_reasons.no_answer') }
  let(:default_title_status_reason_id) { 2 }
  let(:additional_information) { 'Some additional information' }

  let!(:pawn_location) do
    PawnLocation.create!(initials: Astro::DEFAULT_AUCTION_LOCATION, name: 'Statesville', country: 'United States of America', return_instructions: 'This location is G2G location')
  end

  def return_vehicle(vehicle, options = {})
    return_detail_options = options.fetch(:return_details, {})
    buyer_info_options    = options.fetch(:buyer_information, {})

    stub_buyer_authorization
    purchase_promise_for_vehicle(vehicle)
    initiate_return
    enter_return_details(vehicle, return_detail_options)
    verify_return
    update_buyer_information(buyer_info_options)
  end

  def return_canadian_vehicle(vehicle, options = {})
    return_detail_options = options.fetch(:return_details, {})
    # stub_buyer_authorization
    purchase_promise_for_vehicle(vehicle)
    initiate_return
    enter_return_details(vehicle, return_detail_options)
    verify_return
  end

  def initiate_return_incomplete_verification(vehicle, options = {})
    options.fetch(:return_details, {})
    options.fetch(:buyer_information, {})

    stub_buyer_authorization
    purchase_promise_for_vehicle(vehicle)
    initiate_return
    verify_return
  end

  def initiate_pplv_return(vehicle, seller_paid)
    allow_any_instance_of(PromisePurchase).to receive(:pplv?).and_return(true)
    allow_any_instance_of(PromisePurchase).to receive(:seller_paid).and_return(seller_paid)
    visit_returns_user_info_page
    click_on vnum_number_filter_button
    select_vnum_number_for_return(vehicle.vnum)
    click_on I18n.t('forms.return')
  end

  def enter_bill_of_sale_details
    enter_seller_information
  end

  def enter_seller_information
    fill_in 'bill_of_sale[seller_phone]', with: '123456'
  end

  def enter_return_details(vehicle, options = {})
    reason = options.fetch(:reason) { 'unable_to_sell' }

    enter_odometer_reading(valid_mileage(vehicle))
    enter_location(pawn_location.name)
    select_return_reason(reason)
    select_title_status
  end

  def attempt_return_without_reason(vehicle)
    purchase_promise_for_vehicle(vehicle)
    initiate_return
    enter_odometer_reading(valid_mileage(vehicle))
    enter_location(pawn_location.name)
    select_title_status
    verify_return
  end

  def return_vehicle_with_email(_vehicle, email)
    purchase_promise_for_vehicle(acura)
    initiate_return
    enter_odometer_reading(mileage)
    enter_location(pawn_location.name)
    select_title_status
    select_return_reason('unable_to_sell')
    verify_return
    update_buyer_information(email: email)
  end

  def valid_mileage(vehicle)
    vehicle.odometer_reading + max_mileage - 1
  end

  def nth_vehicle_css(index)
    "#{vehicle_css}:nth-child(#{index})"
  end

  def expect_to_see_number_of_vehicles(count)
    expect(page).to have_css(vehicle_css, count: count)
  end

  def expect_nth_vehicle_to_have_vnum(index, vnum)
    expect(page).to have_css(nth_vehicle_css(index), text: vnum)
  end

  def visit_returns_user_info_page
    visit root_path
    click_on I18n.t('navigation.return')
  end

  def view_returns_page(vehicle = acura)
    visit_returns_user_info_page
    click_on I18n.t('forms.pawn_access.input_label') if page.has_css?('a', text: I18n.t('forms.pawn_access.input_label')) # handle the super user case
    select_access_number_for_return(vehicle.pawn_access_no) if page.has_css?('#pawn_access_number_filter')
  end

  def view_returns_page_with_return_status(vehicle = acura)
    Settings.return_status_enabled = true
    view_returns_page(vehicle)
  end

  def select_access_number_for_return(pawn_access_number = acura.pawn_access_no)
    fill_in 'pawn_access_number', with: pawn_access_number
    submit_pawn_access_number_form_for_kiosk_user pawn_access_number
  end

  def select_agreement_number_for_return(agreement_number)
    fill_in 'agreement_number', with: agreement_number
    click_on 'Enter'
  end

  def select_vnum_number_for_return(vnum_number)
    fill_in 'vnum_number', with: vnum_number
    click_on 'Enter'
  end

  def select_pawn_access_number_for_return(pawn_access_number)
    fill_in 'pawn_access_number', with: pawn_access_number
    click_on 'Enter'
  end

  def initiate_return(vehicle = acura)
    allow_any_instance_of(PromiseOffer).to receive(:seller_paid?) { true }
    view_returns_page(vehicle)
    click_on 'Return'
  end

  def update_buyer_information(options = {})
    email = options[:email]
    address_street = options[:address_street]
    address_city = options[:address_city]
    address_zip  = options[:address_zipcode]

    if email
      page.find('form#new_buyer_and_agreement_information_form').fill_in 'Email', with: email
    end

    if address_street
      page.find('form#new_buyer_and_agreement_information_form').fill_in 'Street Address', with: address_street
    end

    if address_city
      page.find('form#new_buyer_and_agreement_information_form').fill_in 'City', with: address_city
    end

    if address_zip
      page.find('form#new_buyer_and_agreement_information_form').fill_in 'Zip', with: address_zip
    end

    click_on I18n.t('forms.finish')
  end

  def enter_odometer_reading(mileage)
    fill_in 'return_invoice_odometer_on_return', with: mileage
  end

  def enter_location(location_name)
    page.find('select#return_invoice_location_initials').select(location_name)
  end

  def select_title_status(title_status_reason_id = default_title_status_reason_id)
    select I18n.t('return_invoice.title_statuses')[title_status_reason_id], from: 'Title Status'
  end

  def complete_bill_of_sale
    click_on 'Create Bill of sale'
  end

  def verify_return
    click_on I18n.t('forms.next')
  end

  def make_promise_ineligible_after_max_time(vehicle)
    promise_purchase = vehicle.promise_offers.last.promise_purchase
    promise_purchase.promised_at = (days_selected + 2).days.ago # we need to push it a little further back thanks to havnumg to use Timecop to create the promise yesterday
    promise_purchase.save!
  end

  def make_promise_old(vehicle)
    promise_purchase = vehicle.promise_offers.last.promise_purchase
    promise_purchase.promised_at = 100.days.ago
    promise_purchase.save!
  end

  def honda_should_be_ineligible_for_return
    expect(page).not_to have_css('.return_btn input[type="submit"]')
  end

  def page_should_display_honda
    expect(page).to have_content(honda.vnum)
  end

  def page_should_not_display_honda
    expect(page).not_to have_content(honda.vnum)
  end

  def select_return_reason(reason)
    check "return_invoice_#{reason}"
  end

  def page_should_have_done_button
    expect(page).to have_css('.btn', text: I18n.t('forms.done'))
  end

  def page_should_have_print_button
    expect(page).to have_css('.btn', text: I18n.t('forms.print'))
  end

  def returned_vehicle_certificate_should_have_return_mileage(vehicle)
    expect(page).to have_content(vehicle.odometer_reading + max_mileage)
  end

  def returned_vehicle_certificate_should_have_return_by_date
    expect(page).to have_content(days_selected.days.from_now.to_date)
  end

  def returned_vehicle_certificate_should_have_address_information(city = nil)
    city ||= 'Chicago' # city stubbed in astroheim authorization stub
    expect(page).to have_content(city)
  end

  def returned_vehicle_certificate_should_have_title_status(title_status_reason_id = default_title_status_reason_id)
    expect(page).to have_content I18n.t('return_invoice.title_statuses')[title_status_reason_id]
  end

  def returned_vehicle_certificate_should_have_return_instructions
    expect(page).to have_content 'This location is G2G location'
  end

  def returned_vehicle_certificate_should_have_return_terms
    expect(page).to have_content "returns are subject to audit for compliance with AgreementShield's terms and conditions"
  end

  def update_return_status(vehicle)
    return_invoice = vehicle.promise_offers.last.promise_purchase.return_invoice
    return_invoice.vehicle_received = true
    return_invoice.title_received = true
    return_invoice.refund_processed = true
    return_invoice.save!
  end

  def expect_return_statuses_to_be_displayed
    vehicle_received_checkbox = find('#vehicle_received')
    title_received_checkbox = find('#title_received')
    refund_processed_checkbox = find('#refund_processed')

    expect(vehicle_received_checkbox).to be_present
    expect(title_received_checkbox).to be_present
    expect(refund_processed_checkbox).to be_present
  end

  def returned_vehicle_should_have_return_statuses_set
    vehicle_received_checkbox = find('#vehicle_received')
    title_received_checkbox = find('#title_received')
    refund_processed_checkbox = find('#refund_processed')

    expect(vehicle_received_checkbox).to be_checked
    expect(title_received_checkbox).to be_checked
    expect(refund_processed_checkbox).to be_checked
  end
end
