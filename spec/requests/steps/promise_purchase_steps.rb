# encoding: utf-8
shared_context 'promise purchase steps' do
  def checkout_div
    page.find('.checkout')
  end

  def select_promise(options = {})
    label = ".vnum#{options[:vehicle][:vnum]}miles#{options[:miles]}days#{options[:days]}"
    if Capybara.current_driver == Capybara.javascript_driver
      page.execute_script "$('#{label}').click()"
    else
      find(label).set('checked')
    end
  end

  def select_acura_for_250_miles_and_7_additional_days
    select_promise vehicle: acura, miles: 250, days: 7
  end

  def select_acura_for_250_miles_and_14_additional_days
    select_promise vehicle: acura, miles: 250, days: 14
  end

  def select_honda_for_250_miles_and_0_additional_days
    select_promise vehicle: honda, miles: 250, days: 0
  end

  def select_honda_for_250_miles_and_7_additional_days
    select_promise vehicle: honda, miles: 250, days: 7
  end

  def select_honda_for_500_miles_and_14_additional_days
    select_promise vehicle: honda, miles: 500, days: 14
  end

  def select_honda_for_500_miles_and_14_additional_days_with_inspection
    choose('500 Miles')
    within '.distance_500_row.bundled_inspections' do
      select_honda_for_500_miles_and_14_additional_days
    end
  end

  def select_none_for_honda
    select_promise vehicle: honda
  end

  def acura_should_appear_before_honda
    expect(page.body).to match(/Acura.*Honda/m)
  end

  def purchase_inspection_only_for_honda
    within honda_information do
      page.find('label', text: I18n.t('promise_offers.inspection_ui.inspection_only_copy')).click
    end
  end

  def checkout
    click_next
    click_next

    return unless Capybara.current_driver == Capybara.javascript_driver
    check 'allow_continue'
    first(:button, I18n.t('forms.checkout')).click
  end

  def click_next
    first('.next_btn', I18n.t('forms.next')).click
  end

  def checkout_with_email(email)
    click_next
    page.find('.wrapper').fill_in 'Email', with: email
    click_next
  end

  def checkout_with_invalid_email
    checkout_with_email('invalid@example')
  end

  def checkout_with_valid_email
    checkout_with_email('test@example.com')
  end

  def honda_should_have_vehicle_inspection
    promise_purchase = PromisePurchase.last
    expect(promise_purchase).to be_present
    promise_offer = promise_purchase.promise_offer

    expect(promise_offer.inspection_purchase).to be_present
  end

  def page_should_show_honda_with_bundled_purchase
    expect(page.find('.bundled_inspections')).to have_content honda.vnum
  end

  def page_should_show_honda_inspection
    expect(page.find('.inspections')).to have_content honda.vnum
  end

  def page_should_show_bundled_purchase_price_total
    page.document.synchronize(1.0) do
      offer = PromiseOffer.by_vnum(honda.vnum).first
      # ugly hack since inspection purchase is being created in another thread
      next unless offer.inspection_purchase.present? && offer.promise_price

      promise_price = offer.promise_price
      inspection_price = offer.inspection_purchase.price
      total = promise_price + inspection_price

      expect(page).to have_content "Total Price: $ #{total.round}"
    end
  end

  def checkout_page_should_have_information_for(vehicle)
    offer = vehicle.promise_offers.last.decorate

    expect(page).to have_content offer.full_description
    expect(page).to have_content "#{offer.days_selected} days"
    expect(page).to have_content "Lane #: #{offer.lane_no}"
    expect(page).to have_content "Run #: #{offer.run_no}"
    expect(page).to have_content "Vehicle Price: #{offer.purchase_price}"
  end

  def page_should_have_prefilled_user_email(email)
    expect(page).to have_css("input[value='#{email}']")
  end

  def checkout_page_should_indicate_honda_is_an_if_bid
    expect(page).to have_content I18n.t('vehicles.if_bid')
  end

  RSpec::Matchers.define :be_in_pending_section do
    match do |vehicle|
      page.has_css?('#pending') &&
        page.find('#pending').has_content?(vehicle.vnum)
    end
  end

  def confirmation_page_should_have_selected_vehicle_information(*offers)
    offers.map(&:decorate).each do |offer|
      expect(checkout_div).to have_content "Lane #: #{offer.lane_no}"
      expect(checkout_div).to have_content "Run #: #{offer.run_no}"
      expect(checkout_div).to have_content "Vehicle Price: #{offer.purchase_price}"
    end

    expect(checkout_div).to have_content 'Total Price: $ 510'
  end

  def confirmation_page_should_indicate_honda_is_an_if_bid
    expect(checkout_div).to have_content I18n.t('vehicles.if_bid')
  end

  def cancel_checkout_process
    click_on I18n.t('forms.cancel')
  end

  def page_should_be_promise_offers_view
    expect(page).to have_css('#vehicles_index_view')
  end

  def page_filter_element
    page.find('.filter')
  end

  def page_should_only_have_one_honda_civic
    expect(page.all('li.vehicle .vehicle_description').map(&:text).select { |text| text =~ /Honda/i }.count).to eq(1)
  end

  def promise_purchases_should_have_transaction_numbers_for_kiosk_user
    PromisePurchase.all.each do |purchase|
      expect(purchase.transaction_id).to match(/^#{SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER}/)
    end
  end

  def page_should_have_invalid_email_alert
    expect(page).to have_content(invalid_email_alert)
  end

  def page_should_not_have_invalid_email_alert
    expect(page).not_to have_content(invalid_email_alert)
  end

  def invalid_email_alert
    I18n.t('email_validation.invalid')
  end
end
