# encoding: utf-8
def fill_in_external_vehicle_details
  fill_in 'external_vehicles_load[][vnum]', with: '1N4AA5AP5BC818485'
  select 2018, from: 'external_vehicles_load[][year]'
  fill_in 'external_vehicles_load[][make]', with: 'NISSAN'
  fill_in 'external_vehicles_load[][model]', with: '999'
  fill_in 'external_vehicles_load[][odometer_reading]', with: 999
  fill_in 'external_vehicles_load[][agreement_no]', with: '524244'
  fill_in 'external_vehicles_load[][vehicle_purchase_price]', with: 111
  fill_in 'external_vehicles_load[][buy_fee]', with: 10
  fill_in 'external_vehicles_load[][vehicle_total]', with: 121
  select FtbLocation.all.first, from: 'external_vehicles_load[][location_initials]'
  fill_in 'external_vehicles_load[][automatic_purchase_email]', with: 'a@b.com'
  fill_in 'external_vehicles_load[][purchased_at]', with: Time.zone.today
end
