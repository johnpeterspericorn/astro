# encoding: utf-8
shared_context 'flat rate steps' do
  def view_flat_rate_agreement_numbers(pawn_access_number)
    visit flat_rates_agreement_number_list_path
    submit_pawn_access_number_form_for_kiosk_user(pawn_access_number)
  end

  def view_flat_rates(pawn_access_number, agreement_number)
    view_flat_rate_agreement_numbers(pawn_access_number)
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    click_on I18n.t('forms.flat_rates.view')
  end

  def view_500_miles_flat_rates(pawn_access_number, agreement_number)
    view_flat_rate_agreement_numbers(pawn_access_number)
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    page.choose('500')
    click_on I18n.t('forms.flat_rates.view')
  end

  def view_360_miles_with_psi_flat_rates(pawn_access_number, agreement_number)
    view_flat_rate_agreement_numbers(pawn_access_number)
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    page.check('PSI eligible')
    click_on I18n.t('forms.flat_rates.view')
  end

  def view_500_miles_with_psi_flat_rates(pawn_access_number, agreement_number)
    view_flat_rate_agreement_numbers(pawn_access_number)
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    page.choose('500')
    page.check('PSI eligible')
    click_on I18n.t('forms.flat_rates.view')
  end

  def view_flat_rates_for_country(pawn_access_number, agreement_number, country = 'United States of America')
    view_flat_rate_agreement_numbers(pawn_access_number)
    select country, from: 'flat_rates[country]'
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    click_on I18n.t('forms.flat_rates.view')
  end

  def view_flat_rates_for_palv(pawn_access_number, agreement_number, country = 'United States of America')
    view_flat_rate_agreement_numbers(pawn_access_number)
    select country, from: 'flat_rates[country]'
    find("input[type=checkbox][value='#{agreement_number}']").set(true)
    page.check('Lv')
    select 3, from: 'flat_rates[limited_volume]'
    click_on I18n.t('forms.flat_rates.view')
  end

  def currency(amount, country)
    "#{country.currency['symbol']} #{amount}"
  end
end
