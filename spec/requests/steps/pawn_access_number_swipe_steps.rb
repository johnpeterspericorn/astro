# encoding: utf-8
shared_context 'pawn access number swipe steps' do
  def page_should_be_pawn_access_number_input_page
    expect(page).to have_selector(:css, '#pawn_access_number')
  end

  def enter_bad_pawn_swipe
    track_data = "'clearly bad data'"
    page.execute_script "parseTrackData(#{track_data});"
  end
end
