# encoding: utf-8
require_relative './purchase_cancellation_steps'

shared_context 'return cancellation steps' do
  include_context 'purchase cancellation steps'

  def return_purchase
    super_user_view_returns_page
    click_on 'Return'
    enter_return_details(acura)
    verify_return
    click_on I18n.t('forms.finish')
  end

  def cancel_return
    super_user_view_returns_page
    click_on(return_cancellation_link)
  end

  def specify_return_reason_by_super_user
    fill_in 'return_invoice_cancellation_reason', with: 'test reason'
    select 'At your request', from: 'cancellation_reasons'
    find('#return_invoice_cancellation_send_email').set(true)
    click_on('Submit')
  end

  def specify_return_reason
    select I18n.t('return_invoice.cancellation.cancellation_reasons')[1], from: 'return_invoice_cancellation_reason'
    click_on('Submit')
  end

  def return_cancellation_link
    I18n.t('return_invoice.cancellation.link_copy')
  end
end
