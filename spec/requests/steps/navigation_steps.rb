# encoding: utf-8
shared_context 'navigation steps' do
  def page_should_be_login_page
    expect(page).to have_css "form[action='#{user_session_path}']" # check for the default Devise login path
  end

  def page_should_be_quotes_research_page
    expect(page.current_url).to eql quotes_research_url
  end

  def page_should_be_pawn_access_number_filter_page
    expect(page).to have_css '#pawn_access_number_filter'
  end

  def disable_purchase_authorization_for_user
    allow_any_instance_of(User).to receive(:authorized_to_purchase?).and_return(false)
  end

  def click_done
    # click_on I18n.t('forms.done')
    first(:link, I18n.t('forms.done')).click
  end

  def click_edit
    first(:link, text: /\AEdit\z/).click
  end
end
