# encoding: utf-8
shared_context 'pawn location steps' do
  def select_agreement(agreement_name)
    select agreement_name, from: 'buyer_number'
  end

  def browse_at_pawn
    click_on I18n.t('promise_estimates.filters.pawn')
  end

  def search_by_vnum
    click_on I18n.t('promise_estimates.filters.vnum')
  end

  def select_lane(lane_number)
    select lane_number.to_s, from: 'lane_no'
  end

  def submit_lane_form
    click_on 'Search'
  end

  def select_drive_location
    page.find('select#location_initials').select('Drive')
  end

  def select_svaa_location
    page.find('select#location_initials').select('Statesville')
  end

  def drive_location
    PawnLocation.find_by_name('Drive')
  end

  def lane_select_should_have_drive_number(lane_number)
    page.find('select#lane_no').has_xpath?("option[text() = '#{lane_number}']")
  end

  def svaa_location_should_be_selected
    expect(find('select#location_initials option[selected]').text).to eql PawnLocation.where(initials: 'SVAA').first.name
  end

  def lane_should_be_selected?(lane_number)
    expect(find('select#lane_no option[selected]').text).to eql lane_number.to_s
  end
end
