# encoding: utf-8
shared_context 'promise selection' do
  def listing_for(sold_vehicle)
    page.first("li.vehicle[data-agreement-no='#{sold_vehicle.agreement_no}']")
  end

  def page_should_have_vehicle_info
    acura_offer = PromiseOffer.by_vnum(acura.vnum).first
    page.document.synchronize(1.0) do
      acura_offer = PromiseOffer.by_vnum(acura.vnum).first
      acura_offer.present?
    end

    acura_offer = acura_offer.decorate

    expect(acura_information).to have_content acura_offer.full_description
    expect(acura_information).to have_content acura_offer.vnum
    expect(acura_information).to have_content acura_offer.odometer_reading
    expect(acura_information).to have_content acura_offer.lane_no
    expect(acura_information).to have_content acura_offer.run_no
    expect(acura_information).to have_content acura_offer.purchase_price
  end

  def page_should_have_promise_matrix
    allow(acura).to receive(:flat_rate_price)
    acura_offer = PromiseOffer.by_vnum(acura.vnum).first
    page.document.synchronize(1.0) do
      acura_offer = PromiseOffer.by_vnum(acura.vnum).first
      acura_offer.present?
    end

    promise = Promise.new(acura_offer).decorate
    expect(acura_information).to have_content promise.price(250, 0)
    expect(acura_information).to have_content promise.price(250, 7)
    expect(acura_information).to have_content promise.price(500, 14)
  end

  def acura_should_be_an_if_bid
    expect(acura_information).to have_content I18n.t('vehicles.if_bid')
  end

  def page_should_have_vehicles_by_agreement_number_and_pawn_access_number(makes)
    makes.each do |make|
      expect(vehicle_information(make).text).not_to be_empty
    end
  end

  def page_should_not_have_vehicles_for_their_agreement_numbers_with_different_pawn_access_numbers
    %w(MITSUBISHI).each do |make|
      expect(vehicle_information(make)).to be_nil
    end
  end

  def page_should_display_copy_for_users_with_no_vehicles
    expect(page).to have_css '.no_vehicles_copy'
  end

  def next_button_should_not_be_enabled
    expect(page).to have_css "input[value='Next'][disabled='disabled']"
  end
end
