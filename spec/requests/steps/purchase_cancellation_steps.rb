# encoding: utf-8
require_relative './promise_selection_vehicle_steps'

shared_context 'purchase cancellation steps' do
  include_context 'promise selection vehicle steps'

  def purchase_promise
    purchase_promise_for_vehicle(acura)
  end

  def super_user_view_returns_page
    visit_returns_user_info_page
    click_on 'Pawn Access #'
    select_access_number_for_return
  end

  def purchase_can_be_cancelled
    super_user_view_returns_page
    expect(page).to have_css(purchase_cancellation_button)
  end

  def purchase_cannot_be_cancelled
    view_returns_page
    expect(page).to_not have_css(purchase_cancellation_button)
  end

  def promise_can_be_purchased
    view_vehicles_by_pawn_access_number
    within acura_information do
      expect(page).to have_css('.promise_options')
    end
  end

  def promise_cannot_be_purchased
    view_vehicles_by_pawn_access_number
    within acura_information do
      expect(page).to_not have_css('.promise_options')
    end
  end

  def purchase_cancellation_button
    '[rel~=cancel_purchase]'
  end

  def cancel_purchase
    super_user_view_returns_page
    find(purchase_cancellation_button).click
    select I18n.t('cancellation.cancellation_reasons')[1], from: 'Reason'
    click_on 'Submit'
  end

  def stub_ods_for_refund
    allow_any_instance_of(PromiseOffer).to receive(:charge_present_in_ods?).and_return(true)
  end
end
