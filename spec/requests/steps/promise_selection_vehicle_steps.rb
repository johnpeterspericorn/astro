# encoding: utf-8
shared_context 'promise selection vehicle steps' do
  def acura_information
    vehicle_information 'acura'
  end

  def honda_information
    vehicle_information 'honda'
  end

  def benz_information
    vehicle_information 'benz'
  end

  def vehicle_information(make)
    page.all('li.vehicle').select { |node| node.text =~ /#{make}/i }.first
  end

  def checkout_vehicle(vehicle)
    page.document.synchronize(1.0) do
      offer = PromiseOffer.by_vnum(vehicle.vnum).first
      offer = display_vehicle_for_offer(vehicle).first unless offer
      offer && offer.update_attributes(left_lot: true) && offer.left_lot == true
    end
  end

  def set_vehicle_past_cutoff(vehicle)
    page.document.synchronize(1.0) do
      offer = PromiseOffer.by_vnum(vehicle.vnum).first
      offer = display_vehicle_for_offer(vehicle).first unless offer
      offer && offer.update_attributes(must_be_promised_at: 1.day.ago)
    end
  end

  def make_vehicle_seller_paid(vehicle)
    seller_paid_attributes = {
      preselected_promise_miles: 500,
      preselected_promise_days:  14,
      automatic_purchase_email:    'foo@bar.com',
      seller_paid: true
    }
    acura.update_attributes(seller_paid_attributes)
    page.document.synchronize(1.0) do
      offer = PromiseOffer.by_vnum(vehicle.vnum).first
      offer = display_vehicle_for_offer(vehicle).first unless offer
      offer && offer.update_attributes(seller_paid: true) && offer.seller_paid == true
    end
  end

  def seller_paid_vehicle_should_have_copy_and_blank_grid
    expect(acura_information).to have_content I18n.t('promise_offers.vehicles.seller_paid_copy').split(/\./)[0] # lame hack to cope with the double space in the I18n copy breaking things :-/
    expect(acura_information).to have_css("td.selected input[value='500_7']")
    acura_information.all('label').each do |label_element|
      expect(label_element).not_to have_content '$'
    end
  end
end
