# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Ods Attribute Update' do
  include_context 'admin steps'

  let!(:offer) { FactoryGirl.create(:promise_offer) }

  before do
    login_to_admin_space
  end

  it 'updates ods attributes for offer' do
    allow_any_instance_of(OdsQueryInterface).to receive(:vehicle_has_left_lot?).and_return(true)
    visit admin_promise_offers_path

    expect(offer).to_not be_left_lot

    find('[rel~=update_ods]').click

    expect(offer.reload).to be_left_lot
  end
end
