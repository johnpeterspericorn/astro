# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - User Management' do
  include_context 'admin steps'

  describe 'user creation' do
    before do
      login_as_user_admin_editor
      visit admin_users_path
    end

    it 'creates a new user' do
      click_on I18n.t('admin.users.create.link_copy')
      fill_in 'user_username', with: 'poohbear'
      find('#user_buyer_user').set(true)
      click_on 'Create User'
      expect(page).to have_content('poohbear')
    end

    it 'creates a new user with deafult roles agreement_transaction_viewer and agreement_score_viewer' do
      click_on I18n.t('admin.users.create.link_copy')
      fill_in 'user_username', with: 'test'
      find('#user_buyer_user').set(true)
      click_on 'Create User'
      expect(page).to have_content('agreement_transaction_viewer, agreement_score_viewer')
    end

    it 'sends new super user password reset instructions' do
      click_on I18n.t('admin.users.create.link_copy')
      fill_in 'user_username', with: 'superpooh'
      fill_in 'user_email', with: 'pooh@test'
      find('#user_super_user').set(true)

      expect { click_on 'Create User' }.to change { mail_deliveries.count }.by(1)
      expect(last_delivered_mail.subject).to include('Reset')
    end

    it 'does not allow a user to be created without a role' do
      click_on I18n.t('admin.users.create.link_copy')
      fill_in 'user_username', with: 'invalid_user'
      find('#user_buyer_user').set(false)
      find('#user_buyer_user').set(false)
      find('#user_agreement_transaction_viewer').set(false)
      find('#user_agreement_score_viewer').set(false)
      click_on 'Create User'

      expect(page).to have_content('user must have at least one role (api, buyer, kiosk, super)')
      expect { click_on 'Create User' }.to_not change(User, :count)
    end

    it 'does not allow duplicate username' do
      username = 'pinocchio'
      FactoryGirl.create(:buyer_user, username: username)
      click_on I18n.t('admin.users.create.link_copy')
      fill_in 'user_username', with: username

      expect { click_on 'Create User' }.to_not change(User, :count)
    end
  end

  describe 'user editting' do
    before do
      login_as_user_admin_editor
      @super_user = User.find_by(username: 'super')
    end

    it 'updates user attributes' do
      user = FactoryGirl.create(:buyer_user)
      visit admin_user_path(user)
      click_on I18n.t('admin.users.show.edit')
      fill_in 'user_username', with: 'kramer'
      click_on 'Update User'

      expect(page).to have_content('kramer')
    end

    it 'displays promise admin section for a super admin with promise admin edit rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      fill_in 'user_username', with: 'kramer'
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_promise_admin_viewer').set(true)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to have_content('Promise Administration')
    end

    it 'does not display promise admin section for a super admin without promise admin edit rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_promise_admin_viewer').set(false)
      find('#user_promise_admin_editor').set(false)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to_not have_content('Promise Administration')
    end

    it 'displays loss prevention section for a super admin with loss prevention viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_loss_prevention_viewer').set(true)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to have_content(I18n.t('admin.section_headers.loss_prevention_admin'))
    end

    it 'does not display loss prevention section for a super admin without loss prevention viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_loss_prevention_viewer').set(false)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to_not have_content(I18n.t('admin.section_headers.loss_prevention_admin'))
    end

    it 'displays finance section for a super admin with finance viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_finance_admin_viewer').set(true)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to have_content(I18n.t('admin.section_headers.finance_admin'))
    end

    it 'does not display finance section for a super admin without finance viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_finance_admin_viewer').set(false)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to_not have_content(I18n.t('admin.section_headers.finance_admin'))
    end

    it 'displays AgreementShield 360 section for a super admin with flat rate inquiry viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_flat_rate_inquiry_viewer').set(true)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to have_content(I18n.t('admin.section_headers.agreementshield_admin'))
    end

    it 'does not display AgreementShield 360 section for a super admin without flat rate inquiry viewer rights' do
      visit admin_user_path(@super_user)
      click_on I18n.t('admin.users.show.edit')
      find('#user_super_user').set(true)
      find('#user_admin').set(true)
      find('#user_flat_rate_inquiry_viewer').set(false)
      click_on 'Update User'
      visit admin_index_path

      expect(page).to_not have_content(I18n.t('admin.section_headers.agreementshield_admin'))
    end

    it 'allows token to be reset' do
      user = FactoryGirl.create(:api_user)

      visit admin_user_path(user)
      click_on I18n.t('admin.users.show.edit')
      click_on I18n.t('admin.users.edit.reset_token')

      expect(page).to have_content(user.reload.authentication_token)
    end
  end

  describe 'vehicle creation role' do
    before do
      login_as_user_admin_editor
    end

    it 'is available for super users' do
      user = FactoryGirl.create(:super_user, email: 'test@test.com')

      visit admin_user_path user
      click_on I18n.t('admin.users.show.edit')
      expect(page).to have_content('vehicle_creator')
    end

    it 'is not available for non-super users' do
      user = FactoryGirl.create(:buyer_user, username: 'kennyloggins')

      visit admin_user_path user
      click_on I18n.t('admin.users.show.edit')

      expect(page).not_to have_content('Vehicle Creator?')
    end
  end

  describe 'password reset link' do
    let(:reset_link_copy) { I18n.t('devise.passwords.reset_link_copy') }

    before do
      login_as_user_admin_editor
    end

    it 'is visible for super users' do
      user = FactoryGirl.create(:super_user, email: 'test@test')

      visit admin_user_path(user)
      click_on I18n.t('admin.users.show.edit')

      expect(page).to have_content(reset_link_copy)
    end

    it 'is not visible for other non-super users' do
      user = FactoryGirl.create(:buyer_user)
      visit admin_user_path(user)
      expect(page).not_to have_content(reset_link_copy)
    end
  end

  describe 'version history', versioning: true do
    before do
      login_as_user_admin_editor
      @super_user = User.find_by(username: 'super')
    end

    it 'shows version history' do
      user = FactoryGirl.create(:api_user)
      visit admin_user_versions_path
      expect(page).to have_content(I18n.t('admin.user_versions.index.header_copy'))
      expect(page).to have_content("super created the user #{user.username}")
    end
  end
end
