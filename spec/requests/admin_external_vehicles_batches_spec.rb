# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'

describe 'Admin External Vehicles Batch Approver' do
  include_context 'navigation steps'
  include_context 'admin steps'
  let(:admin_user) { FactoryGirl.create(:admin_user) }
  let(:external_vehicles_batch) { FactoryGirl.create(:external_vehicles_batch, user_id: admin_user.id) }
  let!(:external_vehicles_load) { FactoryGirl.create(:external_vehicles_load, external_vehicles_batch: external_vehicles_batch) }
  let(:random_agreement_no) { 54_321 }

  before do
    login_as_network_plus_approver
    click_on I18n.t('admin.navigation.external_vehicle_batches')
  end

  describe 'Vehicles Batch Index' do
    context 'Search' do
      it 'should allow to search by Agreement#, VNUM' do
        fill_in 'vehicle_batch_filters_agreement_no', with: external_vehicles_load.agreement_no
        fill_in 'vehicle_batch_filters_vnum', with: external_vehicles_load.vnum
        click_on I18n.t('forms.search')
        expect(page).to have_content external_vehicles_batch.id
        expect(page).to have_content admin_user.username
        expect(page).to have_content SasDataHelper::DEFAULT_DEALER_NUMBER
      end

      it 'doesnt show any results for a random Agreement#' do
        fill_in 'vehicle_batch_filters_agreement_no', with: external_vehicles_load.agreement_no
        click_on I18n.t('forms.search')
        expect(page).to_not have_content external_vehicles_load.vnum
      end
    end
  end

  describe 'Vehicles Batch view' do
    it 'should show editable fields for approver' do
      visit admin_external_vehicles_batch_path(external_vehicles_batch)
      expect(find('input#external_vehicles_load_vnum').value).to eq(external_vehicles_load.vnum)
    end

    it 'should show a readonly view for other users' do
      login_to_admin_space
      visit admin_external_vehicles_batch_path(external_vehicles_batch)
      expect(page).not_to have_field('external_vehicles_load_vnum')
    end
  end
end
