# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - Pawn Location Management' do
  include_context 'admin steps'

  describe 'pawn location management workflow' do
    let(:initials) { 'ABC' }
    let(:name) { 'A Big City' }
    let(:email) { 'test@example.com' }
    let(:new_email) { 'another@example.com' }
    let(:emails) { [email] }
    let(:comma_separated_emails) { "#{email}, #{new_email}" }
    let!(:location) { PawnLocation.create! initials: initials, name: name, return_emails: emails }

    before do
      login_to_admin_space
      click_on I18n.t('admin.navigation.pawn_locations')
    end

    it 'lists locations' do
      expect(page).to have_content(name)
    end

    it 'shows a particular location with email addresses' do
      click_on name
      expect_email_field_to_be(email)
    end

    it 'allows emails to be added' do
      click_on name
      add_new_pawn_location_email(new_email)

      expect_email_field_to_be(comma_separated_emails)
    end

    it 'allows accounting email to be added' do
      click_on name
      add_new_accounting_email(new_email)

      expect_accounting_email_to_be(new_email)
    end

    it 'allows purchases flag to be toggled' do
      click_on name
      uncheck 'Purchases Enabled?'
      click_on 'Update'
      click_on name
      expect(page.find('#pawn_location_purchases_enabled')).not_to be_checked
    end

    it 'allows to edit g2g converted flag' do
      click_on name
      check 'G2G Converted?'
      click_on 'Update'
      click_on name
      expect(page.find('#pawn_location_g2g_converted')).to be_checked
    end

    it 'allows to update bsc_location flag' do
      click_on name
      check 'BSC Location?'
      click_on 'Update'
      click_on name
      expect(page.find('#pawn_location_bsc_location')).to be_checked
    end

    it 'allows to update country' do
      click_on name
      select 'Canada', from: 'pawn_location_country'
      click_on 'Update'
      click_on name
      expect(page).to have_select(:pawn_location_country, selected: 'Canada')
    end

    it 'allows to update partner network' do
      click_on name
      select 'CA-Astroheim', from: 'pawn_location_partner_network'
      click_on 'Update'
      click_on name
      expect(page).to have_select(:pawn_location_partner_network, selected: 'CA-Astroheim')
    end
  end
end
