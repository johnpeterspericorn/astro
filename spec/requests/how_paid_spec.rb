# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_invoice_steps'
require_relative './steps/pawn_access_number_swipe_steps'

describe 'Return workflow on VehicleNotFound error' do
  include_context 'return invoice steps'
  include_context 'pawn access number swipe steps'

  let!(:acura)  { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, seller_paid: true) }
  let!(:benz)   { FactoryGirl.create(:sold_vehicle, make: 'BENZ', odometer_reading: 10_000, seller_paid: true) }
  let!(:honda)  { FactoryGirl.create(:sold_vehicle, make: 'HONDA', odometer_reading: 10_000, agreement_no: SecureRandom.hex, seller_paid: true) }
  let!(:mitsu)  { FactoryGirl.create(:sold_vehicle, make: 'MITSUBISHI', odometer_reading: 10_000, seller_paid: true) }
  let!(:toyota) { FactoryGirl.create(:sold_vehicle, make: 'TOYOTA', odometer_reading: 10_000, seller_paid: true) }
  let!(:volvo)  { FactoryGirl.create(:sold_vehicle, make: 'VOLVO', odometer_reading: 10_000, seller_paid: true) }
  let!(:company1)  { FactoryGirl.create(:company1) }
  let!(:company2)  { FactoryGirl.create(:company2) }

  before do
    Settings.salvage_internet_notes_enabled = true
    stub_buyer_authorization
  end

  context 'how paid information collection' do
    describe 'vehicle adjustment api error' do
      before do
        VCR.use_cassette('vehicle_adjustments_api/failed_request') do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
        end
      end

      after do
        VCR.eject_cassette
      end

      it 'displays how vehicle was paid page ' do
        expect(page).to have_content I18n.t('vehicles.how_paid.display_text')
      end

      it 'displays error if no selection is made' do
        verify_return
        expect(page).to have_content I18n.t('vehicles.how_paid.none_selected')
      end

      it 'displays floor plan company drop down if floor_plan_paid selected' do
        select 'My floor plan paid', from: 'how_paid_information_payment_type'
        expect(page).to have_content I18n.t('vehicles.how_paid.floor_plan_company_text')
      end

      it 'saves how paid information' do
        select 'My floor plan paid', from: 'how_paid_information_payment_type'
        select 'company1', from: 'floor_plan_company'
        expect { click_on I18n.t('forms.next') }.to change(HowPaidInformation, :count).by(1)
      end
    end
  end

  context 'vehicle adjustment' do
    describe 'api error' do
      it 'shows floor branch form when floor plan paid selected' do
        VCR.use_cassette('vehicle_adjustments_api/failed_request', allow_playback_repeats: true) do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
          select 'My floor plan paid', from: 'how_paid_information_payment_type'
          select 'company1', from: 'floor_plan_company'
          verify_return
          enter_return_details(acura)
          verify_return
          expect(page).to have_content('Select a branch')
        end
      end

      it 'shows buyer address form when buyer paid selected' do
        VCR.use_cassette('vehicle_adjustments_api/failed_request', allow_playback_repeats: true) do
          log_in_kiosk_user
          purchase_promise_for_vehicle(acura)
          initiate_return
          select 'I paid the pawn (cash, check, etc)', from: 'how_paid_information_payment_type'
          verify_return
          enter_return_details(acura)
          verify_return
          expect(page).to have_content('Street Address *')
        end
      end
    end
  end
end
