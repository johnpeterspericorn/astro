# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/promise_purchase_steps'

describe 'Admin Pending Purchases View' do
  include_context 'admin steps'

  let!(:return_invoice) { FactoryGirl.create(:return_invoice, shipment: nil) }
  let(:promise_offer) { return_invoice.promise_offer }

  before do
    login_to_admin_space
    visit admin_incomplete_returns_path
    allow_any_instance_of(PromiseOffer).to receive(:payment_type) { nil }
  end

  it 'shows incomplete returns' do
    expect(page).to have_content promise_offer.vnum
  end

  describe 'Retrying a return' do
    context 'when the retry succeeds' do
      it 'displays a success message' do
        expect_any_instance_of(ReturnInvoice).to receive(:finalize)
        click_on 'Retry'
        expect(page).to have_content I18n.t('admin.incomplete_returns.success')
      end
    end
  end

  describe 'Skipping label generation' do
    it 'displays a success message' do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request')

      click_on 'Complete and skip label'
      expect(page).to have_content I18n.t('admin.incomplete_returns.success')

      VCR.eject_cassette
    end
  end
end
