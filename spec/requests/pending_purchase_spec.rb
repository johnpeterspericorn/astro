# encoding: utf-8
require 'requests_helper'
require_relative './steps/promise_purchase_steps'
require_relative './steps/return_invoice_steps'

describe 'Pending Promise Purchase Workflow' do
  include_context 'promise purchase steps'
  include_context 'return invoice steps'

  let!(:honda) { FactoryGirl.create(:sold_vehicle, make: 'HONDA', pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER) }

  before do
    stub_buyer_authorization
    create_location_data
    log_in_kiosk_user
  end

  context 'IF bid vehicles' do
    before do
      honda.update_attributes!(if_bid: true)
    end

    context 'when the IF bid flag is enabled' do
      before do
        Settings.if_bid_pending_enabled = true
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        checkout
      end

      it 'marks the promise pending' do
        expect(honda).to be_in_pending_section
      end
    end

    context 'when the IF bid flag is disabled' do
      before do
        Settings.if_bid_pending_enabled = false
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        checkout
      end

      it 'does not mark the promise pending' do
        expect(honda).not_to be_in_pending_section
      end
    end
  end

  context 'when a vehicle is blacklisted' do
    before do
      VinBlacklist.create!(vnum: honda.vnum)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
    end

    it 'marks the offer pending' do
      checkout
      expect(honda.promise_offers.last).to be_in_pending_section
    end
  end

  context 'when an individual is blacklisted' do
    before do
      PawnAccessNumberBlacklist.create!(pawn_access_no: honda.pawn_access_no)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
    end

    it 'marks the offer pending' do
      checkout
      expect(page).to have_content('Purchase Pending')
    end
  end

  context 'when a seller is blacklisted' do
    before do
      AgreementshipBlacklist.create!(agreement_no: honda.seller_no)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
    end

    it 'marks the offer pending' do
      checkout
      expect(page).to have_content('Purchase Pending')
    end
  end

  context 'when a blacklisted vehicle remains nonexpired' do
    before do
      VinBlacklist.create!(vnum: honda.vnum, expiration_date: Date.tomorrow)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
    end

    it 'should display purchase pending' do
      checkout
      expect(page).to have_content('Purchase Pending')
    end
  end

  context 'when a agreement is marked as blacklisted for purchases' do
    before do
      AgreementshipBlacklist.create!(agreement_no: honda.agreement_no)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout
    end

    describe 'the vehicle with the pending promise' do
      it 'displays pending status as the reason for ineligibility' do
        visit root_path
        log_in_kiosk_user
        view_returns_page(honda)
        expect(page).to have_content I18n.t('return_invoice.ineligible.offer_pending')
      end

      it 'is not available for return' do
        Timecop.travel(2.days.from_now) do
          visit root_path
          log_in_kiosk_user
          view_returns_page(honda)
        end

        expect(page).to have_content I18n.t('return_invoice.not_available.no_eligible_vehicles')
      end
    end

    it 'displays the purchase as pending after purchase' do
      expect(page.find('#pending')).to have_content honda.vnum
    end

    it 'sends a confirmation mail' do
      expect(enqueued_deliveries.count).to eq(2)
    end
  end

  context 'when a blacklisted agreementship remains non-expired' do
    before do
      AgreementshipBlacklist.create!(agreement_no: honda.agreement_no, expiration_date: Date.tomorrow)
      view_vehicles_by_pawn_access_number_as_kiosk_user
      select_honda_for_500_miles_and_14_additional_days
      checkout
    end

    it 'displays the purchase as pending' do
      expect(page.find('#pending')).to have_content honda.vnum
    end
  end

  describe 'approval conditions' do
    context "when the offer will cost more than $#{PendingConditions::PromisePriceOverLimitCondition::GUARANTEE_PRICE_LIMIT_FOR_APPROVAL}" do
      before do
        honda.promise_500 = PendingConditions::PromisePriceOverLimitCondition::GUARANTEE_PRICE_LIMIT_FOR_APPROVAL + 1
        honda.save!
      end

      it 'displays the purchase as pending after purchase' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        checkout

        expect(honda).to be_in_pending_section
      end
    end

    context "when the vehicle was purchased for more than $#{PendingConditions::VehiclePriceOverLimitCondition::VEHICLE_PRICE_LIMIT_FOR_APPROVAL}" do
      before do
        honda.vehicle_purchase_price = PendingConditions::VehiclePriceOverLimitCondition::VEHICLE_PRICE_LIMIT_FOR_APPROVAL + 1
        honda.save!
      end

      it 'displays the purchase as pending after purchase' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        checkout

        expect(honda).to be_in_pending_section
      end
    end

    context 'when the vehicle has a pending reason set by Astroheim' do
      before do
        honda.pending_reason_code = PendingConditions::APPROVAL_REQUIRED
        honda.save!
      end

      it 'displays the purchase as pending after purchase' do
        view_vehicles_by_pawn_access_number_as_kiosk_user
        select_honda_for_500_miles_and_14_additional_days
        checkout

        expect(honda).to be_in_pending_section
      end
    end
  end
end
