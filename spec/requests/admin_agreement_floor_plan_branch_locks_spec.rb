# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Agreement Floorplan Locks' do
  include_context 'admin steps'
  let!(:floor_plan_company) { FactoryGirl.create(:floor_plan_company) }
  let(:company_code) { floor_plan_company.company_code }

  before do
    login_as_finance_admin_viewer
  end

  context 'Index' do
    it 'shows link in admin page' do
      expect(page).to have_link I18n.t('admin.agreement_floor_plan_branch_locks.index.header_copy')
    end

    it 'lists last floor plan branch for agreement' do
      branch_a = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch A')
      FactoryGirl.create(:agreement_floor_plan_branch_lock, company_code: company_code, site_no: branch_a.site_no, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER)

      click_on I18n.t('admin.agreement_floor_plan_branch_locks.index.header_copy')
      fill_in 'agreement_number', with: SasDataHelper::DEFAULT_DEALER_NUMBER
      click_on 'Search'
      expect(page).to have_content SasDataHelper::DEFAULT_DEALER_NUMBER
      expect(page).to have_content branch_a.name
      expect(page).to have_content branch_a.site_no
      expect(page).to have_content floor_plan_company.name
      expect(page).to have_content floor_plan_company.company_code
    end
  end

  context 'Create', js: true do
    it 'create agreement floorplan lock', js: true do
      branch_a = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch A')

      click_on I18n.t('admin.agreement_floor_plan_branch_locks.index.header_copy')
      click_on I18n.t('admin.agreement_floor_plan_branch_locks.create.link_copy')
      fill_in 'agreement_floor_plan_branch_lock_agreement_no', with: SasDataHelper::DEFAULT_DEALER_NUMBER
      select "#{floor_plan_company.company_code}-#{floor_plan_company.name}", from: 'agreement_floor_plan_branch_lock_company_code'
      select "#{branch_a.site_no}-#{branch_a.name}", from: 'agreement_floor_plan_branch_lock_site_no'
      click_on 'Submit'
      expect(page).to have_content SasDataHelper::DEFAULT_DEALER_NUMBER
      expect(page).to have_content branch_a.name
      expect(page).to have_content branch_a.site_no
      expect(page).to have_content floor_plan_company.name
      expect(page).to have_content floor_plan_company.company_code
    end
  end

  context 'Update' do
    it 'update agreement floor plan lock', js: true do
      branch_a = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch A')
      branch_b = FactoryGirl.create(:floor_plan_branch, floor_plan_company: floor_plan_company, name: 'Branch B')
      FactoryGirl.create(:agreement_floor_plan_branch_lock, company_code: company_code, site_no: branch_a.site_no, agreement_no: SasDataHelper::DEFAULT_DEALER_NUMBER)

      click_on I18n.t('admin.agreement_floor_plan_branch_locks.index.header_copy')
      fill_in 'agreement_number', with: SasDataHelper::DEFAULT_DEALER_NUMBER
      click_on 'Search'
      click_on I18n.t('admin.agreement_floor_plan_branch_locks.edit.link_copy')
      select "#{branch_b.site_no}-#{branch_b.name}", from: 'agreement_floor_plan_branch_lock_site_no'
      click_on 'Submit'
      expect(page).to have_content branch_b.name
    end
  end
end
