# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Coupon Batches' do
  include_context 'admin steps'
  let(:count) { 2 }
  let(:expiration) { Time.current }
  let(:amount) { 123 }
  let(:batch_name) { 'Astroheim Royalty' }
  let!(:canada) { 'Canada' }
  let!(:america) { 'United States of America' }

  describe 'batch creation' do
    before do
      login_to_admin_space
    end

    it 'creates multiple coupons' do
      visit new_admin_coupons_batch_path
      fill_in 'count', with: count
      fill_in 'coupon_expiration_date', with: expiration
      fill_in 'coupon_amount', with: amount
      select america, from: 'coupon_country'

      expect { click_on 'Save Batch' }.to change(Coupon, :count).by(count)
    end

    it 'viewing the coupons index, I see batch_name' do
      visit new_admin_coupons_batch_path
      fill_in 'count', with: count
      fill_in 'batch_name', with: batch_name
      fill_in 'coupon_expiration_date', with: expiration
      fill_in 'coupon_amount', with: amount
      select canada, from: 'coupon[country]'

      click_on 'Save Batch'
      expect(page).to have_content(batch_name)
      expect(page).to have_content(canada)
    end
  end
end
