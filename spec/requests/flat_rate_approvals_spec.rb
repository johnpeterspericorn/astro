# encoding: utf-8
require 'requests_helper'

describe 'Flat Rate Offer Approvals' do
  let!(:batch) { FactoryGirl.create(:flat_rate_offer_batch_with_offer, state: :confirmed) }

  let(:password) { SecureRandom.hex }
  let!(:buyer_user) { FactoryGirl.create(:buyer_user) }
  let(:agreementship_name) { 'Awesome Sauce' }
  let(:max_model_year) { (Time.zone.today - 19.years).year }

  before do
    stub_buyer_authorization(agreementship_name: agreementship_name)
    log_in_buyer_user(username: buyer_user.username, password: password, flat_rate_editor: true)
  end

  describe 'Offer Acceptance Page' do
    before do
      visit flat_rate_offer_batch_approval_path(batch)
    end

    it 'shows protection terms and limits applied to each vehicle' do
      offer = FlatRateOffer.last
      expect(page).to have_content(agreementship_name)
      expect(page).to have_content(offer.agreement_no)
      expect(page).to have_content(offer.miles_selected)
      expect(page).to have_content('Agreement Information')
    end

    it 'displays agreementshield customer service details' do
      expect(page).to have_content(I18n.t('flat_rate_offer_batches.approvals.show.agreementshield_customer_service'))
      expect(page).to have_content(I18n.t('flat_rate_offer_batches.approvals.show.hours', hours: I18n.t('help.support_hours')))
    end

    it 'shows max model year as 20 year from today' do
      expect(page).to have_content(max_model_year)
    end
  end

  describe 'Offer Acceptance Page' do
    it 'displays offer acceptance terms and conditions with no error' do
      offer = FlatRateOffer.last
      offer.flat_rate.update_attributes(id: 3)
      visit flat_rate_offer_batch_approval_path(batch)
      expect(page).to have_content(I18n.t('flat_rate_offer_batches.approvals.show.agreementshield_customer_service'))
    end
  end

  describe 'Acceptance' do
    before do
      visit flat_rate_offer_batch_approval_path(batch)
      click_on 'Accept'
    end

    it 'sets the batch acceptor' do
      batch.reload.flat_rate_offers.each do |offer|
        expect(offer.accepting_user).to eq(buyer_user)
      end
    end

    it 'sets accepted_at' do
      batch.reload.flat_rate_offers.each do |offer|
        expect(offer.accepted_at).to be_present
      end
    end
  end

  describe 'notifications' do
    before do
      visit flat_rate_offer_batch_approval_path(batch)
    end

    it 'sends a rejection email' do
      expect do
        click_on 'Reject'
      end.to change { enqueued_deliveries.count }.by(1)
    end

    it 'sends the rejection email' do
      expect do
        click_on 'Reject'
      end.to change { enqueued_deliveries.count }.by(1)
    end

    it 'sends acceptance email' do
      expect do
        click_on 'Accept'
      end.to change { enqueued_deliveries.count }.by(1)
    end
  end
end
