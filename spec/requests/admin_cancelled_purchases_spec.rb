# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/purchase_cancellation_steps'
require_relative './steps/return_invoice_steps'

describe 'Cancelled Purchases' do
  include_context 'admin steps'
  include_context 'purchase cancellation steps'
  include_context 'return invoice steps'

  let(:vnum)    { '12345' }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, vnum: vnum) }

  before do
    stub_buyer_authorization

    log_in_super_user
    stub_ods_for_refund

    reason = I18n.t('cancellation.cancellation_reasons')[1]
    purchase_promise_for_vehicle(acura)
    super_user_view_returns_page
    find(purchase_cancellation_button).click
    select reason, from: 'Reason'
    click_on 'Submit'
  end

  it 'displays all cancelled purchases for a vnum' do
    visit_promise_offer_viewer
    click_on 'Cancelled Purchases'

    expect(page).to have_content(vnum)
  end

  context 'VNUM filter' do
    let(:rand_vnum) { SecureRandom.hex[0..16].upcase }

    before do
      visit_promise_offer_viewer
      click_on 'Cancelled Purchases'
    end

    it 'should list cancelled purchases for an existing VNUM' do
      fill_in 'filters_vnum', with: acura.vnum
      click_on I18n.t('forms.filter')
      expect(page).to have_content acura.vnum
    end

    it 'should not list cancelled purchases for a non existing VNUM' do
      expect(page).to have_content acura.vnum

      fill_in 'VNUM', with: rand_vnum
      click_on I18n.t('forms.filter')
      expect(page).not_to have_content acura.vnum
    end
  end
end
