# encoding: utf-8
require 'requests_helper'
require_relative './steps/return_cancellation_steps'
require_relative './steps/return_invoice_steps'

describe 'Return Cancellation workflow' do
  include_context 'return cancellation steps'
  include_context 'return invoice steps'

  let(:return_link) { '[rel~=return_purchase]' }
  let!(:acura) { FactoryGirl.create(:sold_vehicle, make: 'ACURA', odometer_reading: 10_000, seller_paid: true) }
  let(:reason_text) { '[rel~=admin_reason]' }

  before do
    Settings.salvage_internet_notes_enabled = true
    stub_buyer_authorization
  end

  vcr_options = { cassette_name: 'successful_easypost_shipment_and_retrieval' }

  context 'super user logged in', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note', allow_playback_repeats: true)

      log_in_super_user
      purchase_promise
      return_purchase
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    specify 'return can be cancelled' do
      super_user_view_returns_page
      expect(page).to have_content(return_cancellation_link)
    end

    specify 'return cannot be cancelled twice' do
      cancel_return
      view_returns_page
      expect(page).not_to have_content I18n.t('cancellation.link_copy')
    end

    specify 'cancelled return with reason specified is not eligible for return again' do
      cancel_return
      expect(page).to_not have_css(reason_text)
      specify_return_reason
      super_user_view_returns_page
      expect(page).not_to have_css(return_link)
    end

    specify 'cancelled return submit button does not freeze when no reason specified' do
      cancel_return
      expect(page.find_button('Submit')[:disabled]).to be_falsey
    end

    describe 'cancellation emails' do
      let(:mail) { double(:mail).as_null_object }

      it 'sends email indicating that purchase cannot be returned again', broken: true do
        Timecop.freeze(1.day.from_now) do
          expect(ReturnInvoiceMailer).to receive(:cancelled_and_ineligible_for_return).and_return(mail)
          log_in_super_user(admin: true) # the existing login has expired by now, re-login
          view_returns_page acura
          click_on(return_cancellation_link)
          specify_return_reason_by_super_user
        end
      end

      it 'sends email indicating that purchase cannot be returned again when it has expired', broken: true do
        Timecop.freeze(1.day.ago) do
          expect(ReturnInvoiceMailer).to receive(:cancelled_and_ineligible_for_return).and_return(mail)
          log_in_super_user(admin: true)
          view_returns_page
          click_on(return_cancellation_link)
          specify_return_reason_by_super_user
        end
      end
    end
  end

  context 'pawn user logged in', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note', allow_playback_repeats: true)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    context 'cancelling return they initiated' do
      before do
        log_in_super_user
        return_vehicle(acura)
      end

      it 'allows cancellation' do
        view_returns_page
        expect(page).to have_content(return_cancellation_link)
      end

      specify 'cancelled return with reason specified is not eligible for return again' do
        view_returns_page
        click_on(return_cancellation_link)
        specify_return_reason
        view_returns_page
        expect(page).not_to have_css(return_link)
      end
    end

    context 'cancellation return someone else initiated' do
      before do
        log_in_super_user
        return_vehicle(acura)
        visit root_path
        click_on 'Logout'
        log_in_super_user
      end

      it 'does not allow cancellation' do
        view_returns_page
        expect(page).to_not have_css(return_cancellation_link)
      end
    end
  end

  context 'other user logged in', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note')

      log_in_kiosk_user
      return_vehicle(acura)
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    specify 'return cannot be cancelled' do
      view_returns_page
      expect(page).to_not have_css(return_cancellation_link)
    end
  end

  context 'admin user logged in', vcr: vcr_options do
    before do
      VCR.insert_cassette('vehicle_adjustments_api/successful_request', allow_playback_repeats: true)
      VCR.insert_cassette('salvage_api/add_note', allow_playback_repeats: true)

      log_in_super_user(admin: true)
      purchase_promise
      return_purchase
    end

    after do
      VCR.eject_cassette
      VCR.eject_cassette
    end

    it 'presents user with free form text to add reason based on eligibility' do
      super_user_view_returns_page
      cancel_return
      expect(page).to have_css(reason_text)
    end

    it 'prevents cancellation if cancellation reason prompt is selected', js: true do
      cancel_return
      select I18n.t('return_invoice.cancellation.cancellation_reason_prompt'), from: 'cancellation_reasons'
      expect(page).to have_button('Submit', disabled: true)
    end

    it 'sends an email to support team only if user does not check the email checkbox', js: true do
      user_email = 'pinky@test.com'
      cancel_return
      select I18n.t('return_invoice.cancellation.cancellation_reasons')[1], from: 'cancellation_reasons'
      fill_in 'return_invoice_cancellation_user_email', with: user_email
      find_field('return_invoice_cancellation_send_email').set(false)
      expect do
        click_on 'Submit'
      end.to_not change { enqueued_deliveries.count }
    end

    it 'sends an email to udpated email and support team if user checks the email checkbox', js: true do
      user_email = 'pinky@test.com'
      cancel_return
      select I18n.t('return_invoice.cancellation.cancellation_reasons')[1], from: 'cancellation_reasons'
      fill_in 'return_invoice_cancellation_user_email', with: user_email
      click_on 'Submit'
      expect(page).to have_content 'It ain\'t production' # bogus line @ https://github.com/Astroheim/astro/commit/f7eab0bce8a748e6a631cddb8ef56603f73184ec#commitcomment-13660130
      expect(enqueued_deliveries.count).to eq(3)
    end

    it 'allows return cancellation with reason', js: true do
      cancel_return
      select I18n.t('return_invoice.cancellation.cancellation_reasons')[1], from: 'cancellation_reasons'
      click_on 'Submit'
      view_returns_page
      expect(page).not_to have_content I18n.t('cancellation.link_copy')
    end

    it 'resets cancellation reason and note if cancellation reason prompt is selected', js: true do
      cancel_return
      select I18n.t('return_invoice.cancellation.cancellation_reasons')[1], from: 'cancellation_reasons'
      select I18n.t('return_invoice.cancellation.cancellation_reason_prompt'), from: 'cancellation_reasons'
      expect(page).to have_selector('#return_invoice_cancellation_reason:empty')
      expect(page).to have_field('return_invoice_cancellation_reason', with: '')
    end
  end
end
