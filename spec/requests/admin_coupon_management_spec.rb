# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'

describe 'Admin Tools - Coupon Management' do
  include_context 'admin steps'

  let!(:coupon) { FactoryGirl.create(:coupon, :consumed) }
  let!(:batch_coupon) { FactoryGirl.create(:batch_coupon) }
  let!(:batch_1) { FactoryGirl.create(:batch_1) }
  let(:consumed_coupon_code) { 'ASD123' }
  let(:unconsumed_coupon_code) { 'ABC123' }

  describe 'user creation' do
    before do
      login_to_admin_space
    end

    it 'creates a new coupon' do
      visit new_admin_coupon_path
      fill_in 'coupon_amount', with: 12.0
      fill_in 'coupon_expiration_date', with: 15.days.from_now
      click_on 'Create Coupon'

      expect(page).to have_content('12.0')
    end
  end

  describe 'coupon updating' do
    before do
      login_to_admin_space
    end

    it 'updates coupon attributes' do
      coupon = FactoryGirl.create(:coupon)

      visit edit_admin_coupon_path(coupon)
      fill_in 'coupon_amount', with: 130
      click_on 'Update Coupon'

      expect(page).to have_content('130')
    end
  end

  describe 'coupon history' do
    before do
      login_to_admin_space
      PaperTrail.enabled = true
    end

    it 'updates coupon attributes' do
      coupon = FactoryGirl.create(:coupon)

      visit edit_admin_coupon_path(coupon)
      fill_in 'coupon_amount', with: 130
      click_on 'Update Coupon'

      visit edit_admin_coupon_path(coupon.id)
      fill_in 'coupon_amount', with: 170
      click_on 'Update Coupon'

      expect(page).to have_content('Coupon History')
      expect(page).to have_content('130')
      expect(page).to have_content('170')
    end
  end

  describe 'coupon filter' do
    before do
      login_to_admin_space
      visit admin_coupons_path
    end

    context 'Batch Filter' do
      it 'should display coupons given an existing batch name' do
        select 'AgreementShield Royalty', from: 'filters_name'
        click_on I18n.t('forms.filter')
        expect(page).to have_content 'AgreementShield Royalty'
      end
    end

    context 'Status Filter' do
      it 'should display consumed coupons when selected' do
        select 'consumed', from: 'filters_status'
        click_on I18n.t('forms.filter')
        expect(page).to have_content consumed_coupon_code
        expect(page).not_to have_content unconsumed_coupon_code
      end

      it 'should display unconsumed copouns when selected' do
        select 'unconsumed', from: 'filters_status'
        click_on I18n.t('forms.filter')
        expect(page).to have_content unconsumed_coupon_code
        expect(page).not_to have_content consumed_coupon_code
      end
    end

    context 'Status and Batch Filter' do
      it 'should display coupons given an existing batch name and consumed status' do
        select 'consumed', from: 'filters_status'
        select 'Astroheim Royalty', from: 'filters_name'
        click_on I18n.t('forms.filter')
        expect(page).to have_content consumed_coupon_code
        expect(page).not_to have_content unconsumed_coupon_code
      end

      it 'should not display coupons given an existing batch name and unconsumed status' do
        select 'unconsumed', from: 'filters_status'
        select 'Astroheim Royalty', from: 'filters_name'
        click_on I18n.t('forms.filter')
        expect(page).not_to have_content consumed_coupon_code
        expect(page).not_to have_content unconsumed_coupon_code
      end

      it 'retains selected values upon search' do
        status =  'consumed'
        batch_name = 'Astroheim Royalty'
        select status, from: 'filters_status'
        select batch_name, from: 'filters_name'
        expect(page).to have_select('filters_name', selected: batch_name)
        expect(page).to have_select('filters_status', selected: status)
      end
    end
  end
end
