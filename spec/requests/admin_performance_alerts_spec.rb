# encoding: utf-8
require 'requests_helper'
require_relative './steps/admin_steps'
require_relative './steps/navigation_steps'
require_relative './steps/manage_dashboard_action_steps'

describe Admin::PerformanceAlertsController do
  include_context 'navigation steps'
  include_context 'admin steps'
  include_context 'manage dashboard actions steps'

  let!(:agreement_info) { FactoryGirl.create(:agreement_information, agreement_no: '5241420') }
  let!(:dashboard_score) { FactoryGirl.create(:dashboard_score, buyer_num: '5241420') }
  let(:emails) { 'sfdfdf.fghfh@sdasds.com' }
  let!(:perf_alert) { FactoryGirl.create(:performance_alert) }
  let!(:rel_manager) { FactoryGirl.create(:relationship_manager_1) }
  let(:follow_up_date) { 5.days.from_now }
  let(:mailer) { double :mailer }
  let(:mail) { double :mail }

  before do
    log_in_super_user(admin: true, flat_rate_inquiry_viewer: true)
    visit admin_index_path
    manage_agreement_performance
  end

  describe 'Performance Alert Creation' do
    it 'successfully processes performance alerts' do
      process_performance_alert(emails, true, false, follow_up_date, rel_manager.name)
      expect(page).to have_content('Performance Alert Email sent')
    end

    it 'successfully sends out a performance warning email' do
      expect(PerformanceAlertMailer)
        .to receive(:send_email)
      process_performance_alert(emails, true, false, follow_up_date, rel_manager.name)
    end
  end
end
