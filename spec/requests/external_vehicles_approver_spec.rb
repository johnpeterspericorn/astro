# encoding: utf-8
require 'requests_helper'
require_relative './steps/navigation_steps'
require_relative './steps/admin_steps'

describe 'External Vehicle Approver' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:external_vehicles_batch) { FactoryGirl.create(:external_vehicles_batch, user_id: user.id) }
  let!(:external_vehicles_batch_1) { FactoryGirl.create(:external_vehicles_batch, user_id: user.id, status: 'posted') }
  let!(:external_vehicles_load) { FactoryGirl.create(:external_vehicles_load, external_vehicles_batch_id: external_vehicles_batch.id, seller_name: 'David John') }

  it 'does not display the link to External Vehicle for non external_vehicle_enterer' do
    log_in_kiosk_user
    expect(page).not_to have_content('Network+ Batches')
  end

  describe 'External Vehicle approval', js: true do
    context 'as admin without external vehicle approver role logged in' do
      before do
        log_in_super_user(admin: true, external_vehicle_approver: false)
        click_on 'Admin'
      end

      it 'list batches for approval' do
        expect(page).not_to have_content('Network+ Batches')
      end
    end

    context 'as external vehicle approver logged in' do
      before do
        log_in_super_user(admin: true, external_vehicle_approver: true)
        click_on 'Admin'
        click_on 'Network+ Batches'
      end

      it 'lists batch details' do
        expect(page).to have_content user.username
        expect(page).to have_content external_vehicles_batch.id
        expect(page).to have_content external_vehicles_batch.status
      end

      it 'list batches for approvals' do
        expect(page).to have_link 'List Vehicles'
        expect(page).to have_content external_vehicles_load.batch_id
      end

      it 'list details to approve external vehicles data' do
        click_on 'List Vehicles'
        expect(page).to have_button 'Approve'
        expect(page).to have_button 'Reject'
        expect(page).to have_button 'Save'
        expect(page.body).to include(external_vehicles_load.vnum)
      end

      it 'shows seller name when entered' do
        click_on 'List Vehicles'
        expect(page.body).to include(external_vehicles_load.seller_name)
      end

      it 'will not show posted batches' do
        expect(page.all('#new_product_fees_table tr').count).to eq 2
      end

      it 'file upload option present' do
        click_on 'List Vehicles'
        expect(page).to have_field 'external_vehicles_document[document]'
      end

      it 'shows ftb locations in drop down', js: true do
        click_on 'List Vehicles'
        have_select('#cars', options: %w(Volvo Saab Mercedes Audi))
        expect(page.body).to include(FtbLocation.all.first)
        expect(page.body).to include(FtbLocation.all.last)
      end
    end
  end

  describe 'External Vehicle Updates after Rejection', js: true do
    context 'as admin with external vehicle approver role logged in' do
      let!(:external_vehicle) { FactoryGirl.create(:external_vehicles_load, external_vehicles_batch_id: external_vehicles_batch.id, seller_name: 'David John') }

      before do
        log_in_super_user(admin: true, external_vehicle_approver: true)
        external_vehicle.reject!
        visit external_vehicles_approver_path(external_vehicle.external_vehicles_batch, external_vehicle_id: external_vehicle.id)
      end

      it 'list batches for approval' do
        expect(page).to have_content('Rejected')
      end
    end
  end
end
