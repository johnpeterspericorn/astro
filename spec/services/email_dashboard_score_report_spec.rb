# encoding: utf-8
require 'spec_helper'

describe EmailDashboardScoreReport do
  let(:params) do
    {
      dashboard_score: { agreement_no: 524_142_0, emails: 'amas@aas.com' }
    }
  end

  subject { described_class.new(params) }

  context 'when email ineligible' do
    it 'raises EmailInvalidError' do
      params[:dashboard_score][:emails] = 'sadsddsa'
      expect { subject.send_email }.to raise_error(EmailDashboardScoreReport::EmailInvalidError)
    end
  end

  context 'when email is eligible' do
    it 'sends email' do
      expect { subject.send_email }.to change { enqueued_deliveries.count }.by(1)
    end
  end
end
