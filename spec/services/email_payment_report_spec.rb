# encoding: utf-8
require 'spec_helper'

describe EmailPaymentReport do
  let(:params) { { date_from: 60.days.ago.to_s, date_to: Time.zone.now.to_s, emails: 'amas@aas.com' } }
  subject { described_class.new(params) }

  context 'when email ineligible' do
    it 'raises EmailInvalidError' do
      params[:emails] = 'sadsddsa'
      expect { subject.send_email }.to raise_error(EmailPaymentReport::EmailInvalidError)
    end
  end

  context 'when email is eligible' do
    it 'sends email' do
      expect { subject.send_email }.to change { enqueued_deliveries.count }.by(1)
    end
  end
end
