# encoding: utf-8
require 'spec_helper'

describe ReturnFeeNotificationForPromisePurchase do
  let(:agreement_no) { SasDataHelper::DEFAULT_DEALER_NUMBER }
  let!(:ai) { FactoryGirl.create(:pawn_information, agreement_no: agreement_no) }
  let!(:promise_offer)     { FactoryGirl.create(:day_old_promise_offer, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false, pawn_information: ai) }
  let!(:promise_purchase)  { FactoryGirl.create(:returnable_promise_purchase, promise_offer: promise_offer, miles_selected: 250, additional_days: 7) }
  let!(:invoice) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase) }
  subject { described_class.new(promise_purchase.id) }

  context 'when return fee is ineligible' do
    describe 'return fee is captured' do
      before do
        payment = FactoryGirl.create(:payment, return_fee: 500)
        FactoryGirl.create(:payment_response, payment: payment, promise_purchase: promise_purchase)
      end

      it 'raises ReturnFeeIneligibleError' do
        expect { subject }.to raise_error(ReturnFeeNotificationForPromisePurchase::ReturnFeeIneligibleError)
      end
    end
  end

  context 'when return fee is eligible' do
    before do
      FactoryGirl.create(:new_product_fee, seller_paid: false, agreement_no: nil, timeframe: 365, count: 3, fee: 10.0)
      FactoryGirl.create(:new_product_fee, seller_paid: false, agreement_no: nil, timeframe: 365, count: 6, fee: 150.0)
      FactoryGirl.create(:new_product_fee, seller_paid: false, agreement_no: nil, timeframe: 365, count: 999, fee: 500.0)
    end

    it 'should display the correct return fee information' do
      expect(subject.purchase).to eq(promise_purchase)
      expect(subject.number_of_returns).to eq(2)
      expect(subject.timeframe).to eq(365)
      expect(subject.fee).to eq(10)
      expect(subject.additional_returns).to eq(1)
      expect(subject.additional_fee).to eq(140.0)
    end

    it 'should display return fee information for seller paid and palv scenario' do
      aa = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
      dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: aa, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: false)
      rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
      FactoryGirl.create(:return_invoice, promise_purchase: rgp)
      expect(subject.number_of_returns).to eq(3)
    end

    it 'should NOT display return fee information for seller paid and palv scenario' do
      aa = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
      dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: aa, left_lot: true, limited_volume: NewProductKind::LEVEL_3, purchased_at: 8.days.ago, seller_paid: true)
      rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
      FactoryGirl.create(:return_invoice, promise_purchase: rgp)
      expect(subject.number_of_returns).to eq(2)
    end

    it 'should NOT display return fee information for buyer paid but NOT palv scenario' do
      aa = FactoryGirl.create(:pawn_information, agreement_no: agreement_no)
      dogo = FactoryGirl.create(:day_old_promise_offer, pawn_information: aa, left_lot: true, limited_volume: false, purchased_at: 8.days.ago, seller_paid: false)
      rgp = FactoryGirl.create(:returnable_promise_purchase, promise_offer: dogo, miles_selected: 250, additional_days: 7)
      FactoryGirl.create(:return_invoice, promise_purchase: rgp)
      expect(subject.number_of_returns).to eq(2)
    end
  end
end
