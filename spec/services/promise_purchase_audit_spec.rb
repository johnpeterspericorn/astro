# encoding: utf-8
require 'spec_helper'

describe PromisePurchaseAudit do
  let!(:pawn_information) { FactoryGirl.create(:pawn_information) }
  let!(:promise_offer) { FactoryGirl.create(:promise_offer, pawn_information: pawn_information, emails: 'foo@bar.com') }
  let!(:promise_purchase) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer) }
  let(:params) { ActionController::Parameters.new(filters: { date_from: 2.days.ago.to_s, date_to: (Time.current + 1.day).to_s, promise_purchase_id: promise_purchase.id }) }
  subject { described_class.new(params) }

  describe 'version history', versioning: true do
    it 'shows version history of promise_purchase' do
      promise_purchase.update_attributes(additional_days: 1)
      expect(subject.versions.first.changeset).to include('additional_days')
    end

    it 'shows version history of vehicle information' do
      vehicle_information = promise_purchase.promise_offer.vehicle_information
      vehicle_information.update_attributes(make: 'AUDI')
      expect(subject.versions.first.changeset).to include('make')
    end

    it 'shows version history of pawn information' do
      pawn_information = promise_purchase.promise_offer.pawn_information
      pawn_information.update_attributes(vehicle_purchase_price: 100)
      expect(subject.versions.first.changeset).to include('vehicle_purchase_price')
    end

    it 'shows version history of promise offer' do
      promise_offer = promise_purchase.promise_offer
      promise_offer.update_attributes(emails: 'a@example.com')
      expect(subject.versions.first.changeset).to include('emails')
    end
  end
end
