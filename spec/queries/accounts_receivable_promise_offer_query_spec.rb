# encoding: utf-8
require 'spec_helper'

describe AccountsReceivablePromiseOfferQuery do
  describe '.will_appear_on_accounts_receivable_report_at' do
    let!(:time) { Time.current }

    let(:offer_attrs) do
      {
        created_at: time
      }
    end

    context 'a failed charge' do
      let!(:offer) { FactoryGirl.create(:promise_offer, offer_attrs.merge(promise_status: 'charge_failed')) }

      it 'is included in the scope' do
        expect(described_class.new.will_appear_on_accounts_receivable_report_at(time))
          .to(include offer)
      end
    end

    context 'floor-plan-paid' do
      let!(:offer) { FactoryGirl.create(:promise_offer, offer_attrs.merge(promise_status: 'promised', paid_by_floor_plan: true)) }

      it 'is included in the scope' do
        expect(described_class.new.will_appear_on_accounts_receivable_report_at(time))
          .to(include offer)
      end
    end

    context 'approved pending offers' do
      let!(:offer) { FactoryGirl.create(:promise_offer, offer_attrs.merge(promise_status: 'promised', approved_at: Time.current, left_lot: true)) }

      it 'is included in the scope' do
        expect(described_class.new.will_appear_on_accounts_receivable_report_at(time))
          .to(include offer)
      end
    end

    context 'when the offer is too old to be on the report' do
      let!(:offer) { FactoryGirl.create(:promise_offer, created_at: 1.year.ago, promise_status: 'charge_failed') }

      it 'is not included in the scope, even when meeting a predicate' do
        expect(described_class.new.will_appear_on_accounts_receivable_report_at(time))
          .to_not(include offer)
      end
    end
  end
end
