# encoding: utf-8
require 'astroheim/sows/fake_client'

Astroheim::Sows.configure do |config|
  config.client = Astroheim::Sows::FakeClient
end
