# encoding: utf-8
shared_context 'new_product_data' do
  let(:agreement_no) { '5241420' }
  let(:seller_no) { '5241421' }
  let(:arbitrary_agreement_no) { '5241422' }
  let(:arbitrary_seller_no) { '5241423' }
  let(:timeframe_365) { 365 }
  let(:timeframe_180) { 180 }
  let(:timeframe_90) { 90 }
  let(:timeframe_30) { 30 }
  let(:count_1) { 1 }
  let(:count_2) { 2 }
  let(:count_3) { 3 }
  let(:count_4) { 4 }
  let(:count_5) { 5 }
  let(:count_6) { 6 }
  let(:count_7) { 7 }
  let(:count_8) { 8 }
  let(:count_10) { 10 }
  let(:count_11) { 11 }
  let(:count_14) { 14 }
  let(:count_15) { 15 }
  let(:buyer_paid) { false }
  let(:seller_paid) { true }
  let(:infinity) { 9999 }
  let(:the_fee) { 0.0 }

  def create_product_fees
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: '4534', timeframe: timeframe_180, count: count_4, fee: 0.0)
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: '4534', timeframe: timeframe_180, count: count_7, fee: 200.0)
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: '4534', timeframe: timeframe_180, count: infinity, fee: 600.0)
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: 123_45, timeframe: timeframe_365, count: count_3, fee: 0.0)
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: 123_45, timeframe: timeframe_365, count: count_6, fee: 150.0)
    FactoryGirl.create(:new_product_fee, seller_paid: buyer_paid, agreement_no: 123_45, timeframe: timeframe_365, count: infinity, fee: 500.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: seller_no, timeframe: timeframe_90, count: count_10, fee: 100.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: seller_no, timeframe: timeframe_90, count: count_15, fee: 200.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: seller_no, timeframe: timeframe_90, count: infinity, fee: 300.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: nil, timeframe: timeframe_30, count: count_5, fee: 50.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: nil, timeframe: timeframe_30, count: count_10, fee: 75.0)
    FactoryGirl.create(:new_product_fee, seller_paid: seller_paid, seller_no: nil, timeframe: timeframe_30, count: infinity, fee: 125.0)
  end

  def vendor_expectations(purchase, count, missing_vendor, seller_paid, timeframe)
    if missing_vendor && seller_paid
      allow(purchase).to receive(:seller_no).and_return(arbitrary_seller_no)
    elsif missing_vendor && !seller_paid
      allow(purchase).to receive(:agreement_no).and_return(arbitrary_agreement_no)
    elsif !missing_vendor && seller_paid
      allow(purchase).to receive(:seller_no).and_return(seller_no)
    else
      allow(purchase).to receive(:agreement_no).and_return(agreement_no)
    end
    allow(purchase).to receive(:seller_paid).and_return(seller_paid)
    allow_any_instance_of(NewProductFeeInformation).to receive(:get_number_of_returns_within_timeframe).with(timeframe).and_return(count)
  end

  def check_new_product_fee(the_fee, invoice, count, missing_vendor, seller_paid, timeframe)
    create_product_fees
    vendor_expectations(invoice.promise_purchase, count, missing_vendor, seller_paid, timeframe)
    new_product_info = NewProductFeeInformation.new(invoice.promise_purchase)
    expect(new_product_info.fee_details[:fee]).to eq(the_fee)
  end
end
