# encoding: utf-8
shared_context 'returns_dashboard_data' do
  let(:pawn_access_no) { '100123456' }

  let(:agreement_one) { '5241412' }
  let(:agreement_two) { '5241413' }

  let(:day_before_yesterday) { 2.days.ago }

  let!(:pawn_location_a) { FactoryGirl.create(:pawn_location, initials: 'AAAA', name: 'Aaaaa') }
  let!(:pawn_location_b) { FactoryGirl.create(:pawn_location, initials: 'BBBB', name: 'Bbbbb') }

  let!(:agreement_info_one) { FactoryGirl.create(:agreement_information, agreement_no: agreement_one) }
  let!(:agreement_info_two) { FactoryGirl.create(:agreement_information, agreement_no: agreement_two) }

  let!(:promise_offer_one) { FactoryGirl.create(:day_old_promise_offer) }
  let!(:promise_offer_two) { FactoryGirl.create(:day_old_promise_offer) }
  let!(:promise_offer_three) { FactoryGirl.create(:day_old_promise_offer) }

  let!(:pawn_info_one) { FactoryGirl.create(:pawn_information, pawn_access_no: pawn_access_no, pawn_location: pawn_location_a, agreement_no: agreement_one, vehicle_purchase_price: 15_000, promise_offer: promise_offer_one) }
  let!(:pawn_info_two) { FactoryGirl.create(:pawn_information, pawn_access_no: pawn_access_no, pawn_location: pawn_location_b, agreement_no: agreement_two, vehicle_purchase_price: 15_000, promise_offer: promise_offer_two) }
  let!(:pawn_info_three) { FactoryGirl.create(:pawn_information, pawn_access_no: pawn_access_no, pawn_location: pawn_location_a, agreement_no: agreement_one, vehicle_purchase_price: 15_000, promise_offer: promise_offer_three) }

  let!(:promise_purchase_one) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer_one, promised_at: day_before_yesterday) }
  let!(:promise_purchase_two) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer_two, promised_at: day_before_yesterday) }
  let!(:promise_purchase_three) { FactoryGirl.create(:promise_purchase, promise_offer: promise_offer_three, promised_at: day_before_yesterday) }

  let!(:return_invoice_one) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_one, pawn_location: pawn_location_a, created_at: 1.minute.ago) }
  let!(:return_invoice_two) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_two, pawn_location: pawn_location_b, created_at: 1.minute.ago) }
  let!(:return_invoice_three) { FactoryGirl.create(:return_invoice, promise_purchase: promise_purchase_three, pawn_location: pawn_location_a, created_at: 1.minute.ago) }

  def create_associated_vehicles
    FactoryGirl.create(:vehicle_information, promise_offer: promise_offer_one, vnum: '12345', year: '2005', make: 'chevy', odometer_reading: 500)
    FactoryGirl.create(:vehicle_information, promise_offer: promise_offer_two, vnum: '1111', year: '2006', make: 'toyota', odometer_reading: 600)
    FactoryGirl.create(:vehicle_information, promise_offer: promise_offer_three, vnum: '121212', year: '2006', make: 'nissan', odometer_reading: 700)
  end
end
