# encoding: utf-8
RSpec.configure do |config|
  config.before do
    Delayed::Worker.delay_jobs = true
  end
end
