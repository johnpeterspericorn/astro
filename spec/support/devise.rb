# encoding: utf-8
module UserRequestHelper
  def sign_in_with_user(user = FactoryGirl.create(:admin_user, password: 'password'))
    post user_session_path, user: { username: user.username, password: user.password }
  end
end
