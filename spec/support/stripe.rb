# encoding: utf-8
Capybara::Webkit.configure do |config|
  config.allow_url('stripecdn.com')
  config.allow_url('checkout.stripe.com')
  config.allow_url('api.mixpanel.com')
  config.allow_url('s3.amazonaws.com')
  config.allow_url('q.stripe.com')
  config.allow_url('js.stripe.com')
  config.allow_url('api.stripe.com')
end
