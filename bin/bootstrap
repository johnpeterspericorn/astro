#!/bin/sh

brew_install_if_needed() {
  package=$1

  brew info $package | grep "Not installed" > /dev/null 2>&1
  if [[ "$?" -eq "0" ]]; then
    brew install $package
  else
    echo "$package already in use"
  fi
}

setup_database() {
  success=0

  bundle exec rake db:setup
  let success=success+$?

  RAILS_ENV=test bundle exec rake db:create db:schema:load
  let success=$?+success

  if [[ "$success" -ne "0" ]]; then
    echo "I couldn't setup your database for you. Manual steps required."
    printf "\e[36m"
    echo ""
    echo "---------------------------------"
    echo "Some manual setup steps required:"
    echo ""
    echo "1) Bundle Install"
    echo "2) Initialize databases with:"
    echo "     bundle exec rake db:setup"
    echo "     RAILS_ENV=test bundle exec rake db:create db:schema:load"
    printf "\e[0m"

  else
    echo "Database successfully set up"
  fi
}

if [ ! -f .env ]; then
  cp -R -p config/env.masple .env
fi

# Brew packages
echo "Checking for QT..."
brew_install_if_needed qt
brew_install_if_needed npm

# Install dependencies
echo "Bundling..."
bundle install
echo "Checking for mailcatcher..."
gem install mailcatcher
echo "Checking for foreman..."
gem install foreman
echo "Installing node packages..."
npm install

# Set up the database and prep the test environment
echo "Setting up db..."
setup_database

echo
echo
echo "The project has been bootstrapped!"
echo "Please do the following to complete setup:"
echo
echo "* Edit the .env file to satisfy the needed variables"
echo
