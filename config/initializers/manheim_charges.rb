# encoding: utf-8
Astroheim::Charges.configure do |config|
  if Rails.env.production?
    config.api_username = 'agreementbwebservice'
    config.api_password = 'agreementbwebservice'
    config.qa_mode      = false
  end
  config.logger = ChargesLogger
end
