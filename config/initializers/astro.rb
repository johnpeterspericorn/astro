# encoding: utf-8
$stdout.sync = true if Rails.env.development?

module Astro
  ADMIN_USER     = 'MrWizard'.freeze
  ADMIN_PASSWORD = '4mbros!US'.freeze

  GUARANTEE_ESTIMATES_VEHICLES_PER_PAGE = 10
  GUARANTEE_ESTIMATES_VEHICLES_PER_PRINT_PAGE = 48

  DEFAULT_AUCTION_LOCATION = 'SVAA'.freeze

  TRAINS_OPEN_TIMEOUT = 1 # second
  TRAINS_READ_TIMEOUT = 1 # second

  BUYER_AUTHORIZATION_CLASS = EnvironmentConfig.fetch('BUYER_AUTHORIZATION_CLASS', 'BuyerAuthorization').constantize

  ACCOUNTING_EMAIL = "accounting@#{DOMAIN}".freeze
  APP_EMAIL        = "astro@#{DOMAIN}".freeze
  AUDIT_EMAIL      = "audit@#{DOMAIN}".freeze
  AUTOMATIC_EMAIL  = "automatic@#{DOMAIN}".freeze
  EMERGENCY_EMAIL  = "911@#{DOMAIN}".freeze
  NO_REPLY_EMAIL   = "no-reply@#{DOMAIN}".freeze
  PENDING_EMAIL    = "pending@#{DOMAIN}".freeze
  PROTECTED_EMAIL  = "protected@#{DOMAIN}".freeze
  MAS_EMAIL        = "masteam@#{DOMAIN}".freeze
  SUPPORT_EMAIL    = "support@#{DOMAIN}".freeze
  TEST_ENV_EMAIL   = "testenvemails@#{DOMAIN}".freeze
  QA_TEST_EMAIL    = 'test@sdasds.com'.freeze
  REQUEST_EMAIL    = "requests@#{DOMAIN}".freeze
  astrdian_INVENTORY_EMAIL = 'inventory@astromarketing.com'.freeze
  CANADIAN_ACCOUNTING_EMAIL = "caaccounting@#{DOMAIN}".freeze
  CANADIAN_ASTRSHIELD_EMAIL = 'agreementshieldcanada@Astroheim.com'.freeze
  LEFT_LOT_DEFAULT_EMAIL    = "technology@#{DOMAIN}".freeze
end
