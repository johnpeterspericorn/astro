# encoding: utf-8
require 'tagged_formatter'

logfile_path = Rails.root.join('log', "#{Rails.env}.log")
SHARED_LOGFILE = File.open(logfile_path, 'a')

Rails.logger = Logger.new(SHARED_LOGFILE)
Rails.logger.formatter = TaggedFormatter.new('MAIN', ::Logger::Formatter.new)

module Autoims
  Logger           = Logger.new(SHARED_LOGFILE)
  Logger.formatter = TaggedFormatter.new('AUTOIMS', ::Logger::Formatter.new)
end

BscLogger           = Logger.new(SHARED_LOGFILE)
BscLogger.formatter = TaggedFormatter.new('BSC', Logger::Formatter.new)

ChargesLogger           = Logger.new(SHARED_LOGFILE)
ChargesLogger.formatter = TaggedFormatter.new('CHARGES', Logger::Formatter.new)

DelayedJobLogger           = Logger.new(SHARED_LOGFILE)
DelayedJobLogger.formatter = TaggedFormatter.new('DELAYEDJOB', Logger::Formatter.new)

PaymentsLogger           = Logger.new(SHARED_LOGFILE)
PaymentsLogger.formatter = TaggedFormatter.new('PAYMENTS', Logger::Formatter.new)

PurchasesLogger           = Logger.new(SHARED_LOGFILE)
PurchasesLogger.formatter = TaggedFormatter.new('PURCHASES', Logger::Formatter.new)

QuotesLogger           = Logger.new(SHARED_LOGFILE)
QuotesLogger.formatter = TaggedFormatter.new('QUOTES', Logger::Formatter.new)

SalvageLogger           = Logger.new(SHARED_LOGFILE)
SalvageLogger.formatter = TaggedFormatter.new('SALVAGE', Logger::Formatter.new)

SowsLogger           = Logger.new(SHARED_LOGFILE)
SowsLogger.formatter = TaggedFormatter.new('SOWS', Logger::Formatter.new)

G2gwsLogger           = Logger.new(SHARED_LOGFILE)
G2gwsLogger.formatter = TaggedFormatter.new('G2GWS', Logger::Formatter.new)

NetworkPlusLogger           = Logger.new(SHARED_LOGFILE)
NetworkPlusLogger.formatter = TaggedFormatter.new('NETWORK_PLUS', Logger::Formatter.new)
