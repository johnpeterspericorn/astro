# encoding: utf-8
Stripe.api_key = ENV.fetch('STRIPE_API_KEY_PRIVATE')
module PaymentService
end
Dir[Rails.root.join('lib', 'payment_service', '*.rb')].each { |f| require f }
