# encoding: utf-8
unless Rails.env.production?
  require 'awesome_print'
  AwesomePrint.pry!
end

host = `hostname`.chomp
old_prompt = Pry.config.prompt
env_displayed_name = Rails.env.upcase[0..2]
env = case Rails.env
      when 'production'
        Pry::Helpers::Text.red(env_displayed_name)
      when 'qa'
        Pry::Helpers::Text.yellow(env_displayed_name)
      else
        Pry::Helpers::Text.green(env_displayed_name)
      end

Pry.config.prompt = [
  proc { |*a| "#{env} #{host} #{old_prompt.first.call(*a)}" },
  proc { |*a| "#{env} #{host} #{old_prompt.second.call(*a)}" }
]
