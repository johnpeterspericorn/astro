# encoding: utf-8
Time::DATE_FORMATS[:long_with_day] = "%A #{DateTime::DATE_FORMATS[:long]}"
Time::DATE_FORMATS[:slashed] = '%D'
Time::DATE_FORMATS[:dashed_with_tz] = '%Y-%m-%d %H:%M:%S %Z'
