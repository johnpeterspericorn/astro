# encoding: utf-8
ActiveSupport.on_load(:active_model_serializers) do
  ActiveModel::ArraySerializer.root = false
end
