# encoding: utf-8
ActiveSupport.on_load(:action_controller) do
  ApplicationController.log_warning_on_csrf_failure = false
end
