# encoding: utf-8
ActionController::Renderers.add(:csv) do |obj, options|
  filename    = options.fetch(:filename, 'data')
  disposition = "attachment; filename=#{filename}.csv"
  send_data(obj, type: Mime::CSV, disposition: disposition)
end
