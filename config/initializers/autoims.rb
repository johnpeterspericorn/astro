# encoding: utf-8
require 'tagged_formatter'
require Rails.root.join('app', 'models', 'autoims', 'remote_client')

module Autoims
  USERNAME = ENV.fetch('AUTOIMS_USER')
  PASSWORD = ENV.fetch('AUTOIMS_PASSWORD')
  AUTOIMS_URL = ENV.fetch('AUTOIMS_URL')
  XMLNS_URL   = ENV.fetch('AUTOIMS_XMLNS_URL')
  XSI_URL     = ENV.fetch('AUTOIMS_XSI_URL')

  CLIENT = if Rails.env.development? || Rails.env.test?
             Naught.build { |c| c.mimic(RemoteClient) }.tap do |klass|
               def klass.create_drop_notice(_); end

               def klass.delete_drop_notice(_); end
             end
           else
             RemoteClient
           end
end
