# encoding: utf-8
Astroheim::Sows.configure do |config|
  config.logger = SowsLogger
  config.api_username          = EnvironmentConfig.fetch('SOWS_USER')
  config.api_password          = EnvironmentConfig.fetch('SOWS_PASS')

  config.charges_enabled do
    Settings.sows_charges_notes
  end

  config.qa_mode = false if Rails.env.production?

  config.endpoint = ENV.fetch('SOWS_ENDPOINT')
end
