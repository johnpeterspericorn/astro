# encoding: utf-8
HealthCheck.setup do |config|
  # Text output upon success
  config.success = 'success'
  config.http_status_for_error_text = 500
  config.http_status_for_error_object = 500

  config.standard_checks = %w(database migrations custom)

  # max-age of response in seconds
  # cache-control is public when max_age > 1 and basic_auth_username is not set
  # You can force private without authentication for longer max_age by
  # setting basic_auth_username but not basic_auth_password
  config.max_age = 1
end
