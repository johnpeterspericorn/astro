# encoding: utf-8
if Rails.env.development?
  BetterErrors::Middleware.allow_ip!(ENV['TRUSTED_IP']) if ENV['TRUSTED_IP']
end
