# encoding: utf-8
if Object.const_defined? :Bullet
  Astro::Application.config.after_initialize do
    Bullet.enable = true
    Bullet.bullet_logger = true
    Bullet.console = true
    Bullet.rails_logger = true
    Bullet.rollbar = true
    Bullet.add_footer = true
  end
end
