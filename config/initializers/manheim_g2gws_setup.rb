# encoding: utf-8
G2gws.orders_api_configure do |config|
  config.logger = G2gwsLogger
  config.endpoint = ENV.fetch('G2G_API_ENDPOINT') + '/orders2'
  config.username = ENV.fetch('G2G_API_USERNAME')
  config.password = ENV.fetch('G2G_API_PASSWORD')
  config.api_version = ENV.fetch('G2G_ORDERS_API_VERSION')
end

G2gws.invoices_api_configure do |config|
  config.logger = G2gwsLogger
  config.endpoint = ENV.fetch('G2G_API_ENDPOINT') + '/invoices2'
  config.username = ENV.fetch('G2G_API_USERNAME')
  config.password = ENV.fetch('G2G_API_PASSWORD')
  config.api_version = ENV.fetch('G2G_INVOICES_API_VERSION')
end

G2gws.adjustments_api_configure do |config|
  config.logger = G2gwsLogger
  config.endpoint = ENV.fetch('G2G_API_ENDPOINT') + '/adjustments'
  config.username = ENV.fetch('G2G_API_USERNAME')
  config.password = ENV.fetch('G2G_API_PASSWORD')
  config.api_version = ENV.fetch('G2G_ADJUSTMENTS_API_VERSION')
end
