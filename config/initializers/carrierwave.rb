# encoding: utf-8
DEFAULT_REGION = 'us-west-2'.freeze
CarrierWave.configure do |config|
  config.storage    = :aws
  config.aws_bucket = "astro-#{Rails.env}"
  config.aws_acl    = 'public-read'

  config.aws_authenticated_url_expiration = 60 * 60 * 24 * 7

  config.aws_attributes = {
    expires: 1.week.from_now.httpdate,
    cache_control: 'max-age=604800'
  }

  config.aws_credentials = {
    access_key_id:     ENV['S3_KEY'],
    secret_access_key: ENV['S3_SECRET'],
    region:            DEFAULT_REGION
  }
end
