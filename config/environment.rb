# encoding: utf-8
# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application

begin
  Astro::Application.initialize!
rescue StandardError => e
  Rollbar.error(e)
  raise
end
