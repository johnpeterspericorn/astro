# encoding: utf-8
def number_of_processors
  if RUBY_PLATFORM =~ /linux/
    `cat /proc/cpuinfo | grep processor | wc -l`.to_i
  elsif RUBY_PLATFORM =~ /darwin/
    `sysctl -n hw.logicalcpu`.to_i
  else
    raise "can't determine 'number_of_processors' for '#{RUBY_PLATFORM}'"
  end
end

environment ENV['RAILS_ENV']

if ENV['RAILS_ENV'] == 'development'
  threads 2, 8
  port ENV['PORT']
else
  bind 'unix:///tmp/puma.sock'
  pidfile 'tmp/pids/puma.pid'
  preload_app!
  threads Integer(ENV['WEB_CONCURRENCY'] || 4), Integer(ENV['WEB_CONCURRENCY'] || 4) * 4
  worker_timeout Integer(ENV['WEB_TIMEOUT'] || 15)
  workers number_of_processors
  daemonize true
  state_path 'tmp/puma.state'
  stdout_redirect 'log/puma.log', 'log/puma.log', true
end

on_worker_boot do |worker_index|
  require 'active_record'
  config_path = File.expand_path('../database.yml', __FILE__)
  begin
    ActiveRecord::Base.connection.disconnect!
  rescue
    ActiveRecord::ConnectionNotEstablished
  end
  ActiveRecord::Base.establish_connection(ENV['DATABASE_URL'] || YAML.load_file(config_path)[ENV['RAILS_ENV']])
  File.open("tmp/pids/puma_worker_#{worker_index}.pid", 'w') { |f| f.puts Process.pid }
end

before_fork do
  ActiveRecord::Base.connection_pool.disconnect!
end

after_worker_fork do
  ActiveRecord::Base.establish_connection
end
