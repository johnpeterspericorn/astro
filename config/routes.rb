# encoding: utf-8
Astro::Application.routes.draw do
  get '/static/:action' => 'static'

  devise_for :users, controllers: {
    sessions: 'sessions'
  }

  resource :graphql, only: [:create]
  match '/graphql' => 'graphqls#options', via: :options

  unless Rails.env.production?
    mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'
  end

  resource :swipe, only: :new

  namespace :help_panel do
    resource :guide, only: :create
    resource :contact, only: :create
  end

  resources :coupons, only: :show
  resources :agreement_informations, only: :show, param: :agreement_no

  resources :pawn_locations, only: :index

  # sold vehicle routes
  resources :sold_vehicles, except: [:destroy, :index]
  resources :external_vehicles_approver, only: :show
  resources :external_vehicles_load, except: [:destroy, :index] do
    post 'approve'
    post 'reject'
  end

  resources :external_vehicles_documents

  resources :sold_vehicles, only: [], param: :vnum do
    member do
      get :decode
    end
  end

  # purchase routes
  resource  :vehicle_filter,   only: [:show, :edit, :update]
  resources :promise_offers, only: :index
  resources :promise_purchases, only: [:new, :create]
  resources :promise_purchases, only: [] do
    resources :cancellations, only: [:new, :create]
    resource  :payment,       only: [:new, :create]
    resource  :buynet,        only: :show
  end
  resources :transactions, only: [:index, :show]
  resources :dashboard_scores, only: [:index]
  resource :market_index, only: [:show]
  resources :lane_numbers, only: :index
  resources :user_purchases, only: :index

  namespace :quotes do
    resource :research, only: [] do
      get :index, :edit
    end
    resource :promise_estimates, only: [] do
      get :index
    end
  end

  resources :agreements, :user_details, only: [:show, :index]

  # pricing routes
  resources :flat_rate_offer_batches, except: [:index] do
    get :validate_sales_rep_email
    scope module: :flat_rate_offer_batches do
      resource :approval, only: [:show, :create, :destroy]
    end
  end

  resources :flat_rates, only: :index
  namespace :flat_rates do
    resource :agreement_number_list, only: :show
  end

  # returns routes
  namespace :return_funnel do
    resources :filters
    resources :filtered_purchases
    resources :verifications
    resources :return_invoices
    resource :payment_notification
    resource :return_fee_notification
    resource :bill_of_sale, only: [:new, :create]
  end

  resource :return_funnel, only: [:show, :create]
  resources :return_invoices, only: [:edit, :update, :destroy] do
    resource :disbursement_request, only: :show
    resource :return_cancellation do
      get :js_translation_feed
    end
  end
  resource :return_certificate, only: :show
  resources :floor_plan_companies, only: [:show, :index] do
    resources :floor_plan_branches, only: [:index]
  end

  resource :how_paid_information, only: [:new, :create]
  resource  :dashboard_filter, only: [:show, :edit, :update]

  resources :missioncontrol, only: [:index], controller: :admin, as: :admin
  namespace :admin, path: 'missioncontrol' do
    resources :users, except: [:destroy]
    devise_for :users, only: [:password], controllers: { passwords: 'admin/passwords' }
    resource :api_user_token, only: [:create]

    resources :external_vehicles_batches
    resource :accounts_receivable_report_emails
    resources :pawn_locations, except: :destroy
    resources :new_product_fees, :charge_code_mappings
    resources :user_versions, :promise_purchase_versions, only: :index
    resources :dashboard_scores do
      get :manage
    end
    resource :market_index, only: [:show, :edit, :update] do
      resources :market_factors, only: [:edit, :update, :new, :create, :destroy]
    end
    resources :promise_extensions, only: :index do
      collection do
        post 'import'
      end
    end
    resources :cancelled_purchases, only: :index
    resource :dashboard, only: :show
    resources :coupons do
      collection do
        resources :batches, only: [:new, :create], module: :coupons, as: :coupons_batch
      end
    end
    resource :agreement_information, only: [:edit, :update]
    resource :failover, only: [:show, :create, :update]
    resources :feature_flags, only: [:index, :create]
    resources :flat_rates, except: [:show, :destroy]
    resources :flat_rate_offer_batches, except: [:show, :new, :create] do
      resources :resets, only: :create
      resource :confirmation, only: :create, controller: 'flat_rate_offer_batches/confirmations'
      resources :flat_rate_offers, only: [:new, :create]
    end

    resources :flat_rate_offers, only: [:index, :destroy]

    resource :manage_dashboard_actions, only: [:new, :create]
    resources :network_plus_offers, only: [:new, :create] do
      scope module: :network_plus_offers do
        resource :approval
      end
    end
    resources :performance_alerts, only: [:new, :create]
    resources :rate_changes, only: [:new, :create] do
      scope module: :rate_changes do
        resource :client_communication
      end
    end

    resources :floor_plan_companies, except: [:show, :destroy] do
      resources :floor_plan_branches, except: [:show, :destroy]
    end
    resources :promise_offers, only: [:index, :edit, :update] do
      resource :ods_update, only: [:update]
    end
    resources :unpurchased_offers, only: [:edit, :update]
    resources :promise_purchases, only: [:index, :edit, :update] do
      post 'expire_promise'
      post 'unexpire_promise'
    end
    resources :incomplete_returns, only: [:index, :update]
    resources :payments, only: :index
    resources :span_payment_report
    resource :payments_report_email, only: [:edit, :update]
    resource :payment_receipt_email, only: [:new, :create]
    resource :sales_report_email, only: [:edit, :update]
    resources :pending_offers, only: [:index, :show, :update, :destroy]
    resources :sold_vehicles, only: [:index, :edit, :update]
    resources :accounts_receivable_report_offers, only: :index
    resources :return_invoices, only: [:index, :edit, :update, :destroy] do
      resource :audit, only: [:new, :create, :edit, :update] do
        post 'clear'
        resource :flag, only: [:new, :create], module: :audits
      end
      resource :expiring_title_notification, only: :create
      resource :expiring_return_notification, only: :create
    end
    resources :cancelled_returns, only: [:index]
    resources :audited_returns, only: [:index]
    resources :sales_dashboard, only: :index
    resources :returns_dashboard, only: :index
    resources :vnum_blacklists
    resources :agreement_blacklists
    resources :buyer_information
    resources :agreementship_blacklists
    resources :pawn_access_number_blacklists
    resources :flat_rate_inquiries, only: :index
    resources :floor_plan_branch_selections, only: :index
    resources :agreement_floor_plan_branch_locks
  end

  scope module: :legacy do
    namespace :api do
      resource :buyer_promise_purchase, only: :create, constraints: UserAgentConstraint.new(%r{AgreementShield.*/1.15})
    end
  end

  namespace :api do
    # End User APIs
    resources :pawn_locations, only: :index
    resource  :buyer_user_token, only: :create
    resource  :buyer_promise_purchase, only: :create
    resource  :cancellation, only: :create
    resource  :promise_price, only: :show
    resource  :promise_purchase, only: :create
    resource  :promise_purchase_refresh, only: :update
    resource  :pending_offer, only: :update
    resource  :quote, only: :show
    resources :purchasable_promise_offers, only: :index
    resource  :shipment_event, only: :create

    # MAS User APIs
    resource  :promise_eligibility, only: [:show, :create]
    resources :eligible_promise_offers, only: :index
    resources :presale_vehicles, only: :index
    resources :sold_vehicles, only: :create
    resources :promise_extensions, only: :create
    resources :user_agreementships, only: :index
    resources :promise_purchases, only: [] do
      resource :promise_offer, only: :update
    end
    resources :return_invoices, only: :update
    resources :new_product_fees
  end

  get '/return_invoice' => 'return_funnel/filters#index'
  get '/links/:slug' => 'return_certificates#show', as: :obfuscated_return_certificate
  health_check_routes('status')

  get '/doc'       => 'docs#index'
  get '/doc/*path' => 'docs#show'

  get '/agreement_dashboard' => 'pages#show', id: 'agreement_dashboard', format: false
  get '/performance' => 'pages#show', id: 'agreement_dashboard', performance: true, format: false
  get '/pages/:id' => 'pages#show', as: :page, format: false

  %w(vdp ove mypurchases).each do |path|
    get "/#{path}" => redirect('/')
  end

  root to: 'high_voltage/pages#show', id: 'landing_page'
end
