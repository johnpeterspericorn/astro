# encoding: utf-8
# More info at https://github.com/guard/guard#readme
guard 'bundler' do
  watch('Gemfile')
end

guard :rspec do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^lib/(.+)\.rb$}) { |m| "spec/lib/#{m[1]}_spec.rb" }
  watch('spec/spec_helper.rb') { 'spec' }
  watch('spec/worker_spec_helper.rb') { 'spec/workers' }

  # Rails example
  watch(%r{^spec/factories/(.+)_factory\.rb$})        { |m| "spec/models/#{m[1]}_spec.rb" }
  watch(%r{^app/(.+)\.rb$})                           { |m| "spec/#{m[1]}_spec.rb" }
  watch(%r{^app/(.*)(\.erb|\.haml)$})                 { |m| "spec/#{m[1]}#{m[2]}_spec.rb" }
  watch(%r{^workers/(.+)\.rb$})                       { |m| "spec/workers/#{m[1]}_spec.rb" }

  watch(%r{^spec/support/(.+)\.rb$})                  { 'spec' }
  watch('app/controllers/application_controller.rb')  { 'spec/controllers' }
  # Capybara request specs
  watch(%r{^app/views/(.+)/.*\.(erb|haml)$})          { |m| "spec/requests/#{m[1]}_spec.rb" }
end

guard 'livereload' do
  watch(%r{app/views/.+\.(erb|haml|slim)})
  watch(%r{app/helpers/.+\.rb})
  watch(%r{public/.+\.(css|js|html)})
  watch(%r{config/locales/.+\.yml})
  # Rails Assets Pipeline
  watch(%r{(app|vendor)(/assets/\w+/(.+\.(mass|css|js|html))).*}) { |m| "/assets/#{m[3]}" }
end
