# encoding: utf-8
source 'https://rubygems.org'
ruby '2.3.1'

# Infrastructure
gem 'rails', '4.2.6'

# Database
gem 'pg'
gem 'pg_search' # builds named scopes that take advantage of PostgreSQL's full text search

gem 'activerecord-session_store', git: 'git@github.com:Astroheim/activerecord-session_store.git'

# Web Servers
gem 'puma'

# Environment Controls
gem 'dotenv-rails'

# View Concerns
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'haml'
gem 'draper' # Decorate your models
gem 'health_check'
gem 'request_store'

# Style Frameworks
gem 'bourbon'
gem 'neat'
gem 'font-awesome-mass'

# Exception handling
gem 'rollbar'

# Logging
gem 'lograge'

# stripe payment system
gem 'stripe'

# graphql
gem 'graphql'

group :development do
  gem 'binding_of_caller' # support gem for better_errors
  gem 'better_errors'     # super sweet exception handling gem
  gem 'guard', require: false
  gem 'guard-livereload', require: false
  gem 'guard-rspec', require: false
  gem 'guard-shell', require: false
  gem 'guard-bundler', require: false
  gem 'guard-spring', require: false
  gem 'rack-livereload'
  gem 'growl', require: false
  gem 'rb-fsevent'
  gem 'clockwork', require: false
  gem 'quiet_assets'
  gem 'ruby-prof'
  # for VickiProxy
  gem 'sinatra'
  gem 'rest-client'
  gem 'overcommit', require: false
  gem 'letter_opener'
  gem 'web-console', '~> 2.0'
end

# Misc
gem 'aasm'                      # acts as state machine
gem 'active_model_serializers'  # use Rail's ActiveModel::Serializers for API response serialization
gem 'business_time'             # some.business_days.from_now
gem 'cocoon'                    # fancy nested form input
gem 'countries', require: 'countries/global' # ALL the countries
gem 'country_select'            # view helper for country selection
gem 'daemons'                   # allow delayed_job process to be daemonized
gem 'delayed_job'
gem 'delayed_job_active_record' # background jobs with few movnumg parts
gem 'delayed_job_recurring'
gem 'easypost'                  # generate shipping labels
gem 'has_barcode'               # make barcodes for certificate phase
gem 'hashie'                    # quick and easy hash initializers
gem 'high_voltage'              # good static pages with site layout
gem 'kaminari'                  # for pagination
gem 'naught'                    # null objects
gem 'paper_trail', '~>3.0'      # capture changes to records for auditibility
gem 'pusher'                    # For making calls to Pusher for auto-updating dashboards
gem 'glebtv-httpclient', '> 3.0' # http client
gem 'rake'
gem 'savon'                     # SOAP library for hitting Astroheim TRAINS webservice
gem 'setler'                    # for Feature Flags
gem 'typhoeus', '> 0.7.0'
gem 'wicked'                    # Wizard-ify your controllers
gem 'paranoia', '~> 2.0'        # Soft Delete of Models
gem 'jmespath', '~> 1.2'

gem 'devise'
gem 'devise_astroheim_auth',           git: 'ssh://git@github.com/Astroheim/devise_astroheim_auth.git', branch: 'after-auth-hook' # to access Astroheim's internal RESTful authN service
gem 'astroheim_authorization_service', git: 'ssh://git@github.com/Astroheim/astroheim_authorization_service.git'  # to access Astroheim's internal RESTful authZ service
gem 'vicki_proxy',                   git: 'ssh://git@github.com/Astroheim/vicki_proxy.git'                    # to access a proxy to Astroheim's Vicki auth service
gem 'astroheim-chargesclient',         git: 'ssh://git@github.com/Astroheim/astroheim-chargesclient.git'          # to access Astroheim's web service for charges
gem 'astroheim-sowsclient',            git: 'ssh://git@github.com/Astroheim/astroheim-sowsclient.git' # to access Astroheim's SOWS
gem 'g2gws',                         git: 'ssh://git@github.com/Astroheim/G2GWS.git', branch: 'api_updates_ver1.0' # to access Astroheim's Gavel To Gate web service

gem 'mass-rails'
gem 'coffee-rails'
gem 'uglifier', '>= 1.0.3'
# JavaScript Templates
gem 'handlebars_assets'
gem 'pry-rails'
gem 'newrelic_rpm'
gem 'awesome_print', require: false
gem 'pdfkit'
gem 'wkhtmltopdf-binary'
gem 'aws-sdk', '~> 2'
gem 'carrierwave'
gem 'carrierwave-aws'
gem 'jwt'

group :test, :development do
  gem 'rspec-rails'
  gem 'rspec', '> 3.0.0'
  gem 'rspec-activemodel-mocks'
  gem 'pry'
  gem 'pry-nav'
  gem 'hirb'          # nice styling of console output
  gem 'foreman'       # starting development environment

  # documentation
  gem 'redcarpet'
  gem 'yard'
  gem 'yard-rest'
  gem 'sdoc', require: false
end

group :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'capybara-screenshot'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'simplecov', require: false
  gem 'fuubar'
  gem 'vcr'        # replay API calls without hitting the network
  gem 'webmock'    # fake out VCR for requests
  gem 'timecop'    # manipulate time!
  gem 'zonebie'    # randomize timezone for each test run
  gem 'test-unit'
end

group :staging, :qa, :development do
  gem 'recipient_interceptor'
  gem 'rack-mini-profiler'
  gem 'bullet'
  gem 'bundler-audit', require: false
  gem 'brakeman', require: false
  gem 'rails_best_practices', require: false
  gem 'rubocop', require: false
  gem 'rubycritic', require: false
end

group :test, :staging, :qa, :development do
  gem 'vnumbot'
  gem 'factory_girl_rails'
  gem 'graphiql-rails'
end
