# encoding: utf-8
require 'bsc_charges/purchase_request'
require 'bsc_charges/cancellation_request'

module BscCharges
  class Client < Struct.new(:request)
    ENDPOINT_URL = EnvironmentConfig.fetch('BSC_CHARGES_ENDPOINT')

    def self.purchase(promise_offer)
      new(PurchaseRequest.new(promise_offer)).invoke
    end

    def self.cancel(promise_offer)
      new(CancellationRequest.new(promise_offer)).invoke
    end

    def invoke
      request.log_attempt
      api_request.run

      begin
        response = JSON.parse(api_request.response.body).fetch('outputParameters')
      rescue JSON::ParserError => e
        request.log_failure(e.message)
        return false
      end

      response_code = response.fetch('rc')
      if response_code == 0
        request.log_success
        return true
      else
        request.log_failure("Response Code : #{response_code}")
      end
      false
    end

    def api_request
      @api_request ||= Typhoeus::Request.new(
        ENDPOINT_URL,
        method: :post,
        body: request.message
      )
    end
  end
end
