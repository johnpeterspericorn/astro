# encoding: utf-8
module BscCharges
  class Request < Struct.new(:promise_offer)
    USER = EnvironmentConfig.fetch('BSC_CHARGES_USER')
    PASS = EnvironmentConfig.fetch('BSC_CHARGES_PASS')

    ASTRSHIELD_PRODUCT_TYPE = 'DSHD'.freeze

    def message
      body = {
        username: USER,
        password: PASS,
        vnum: promise_offer.vnum,
        location: promise_offer.location_initials,
        work_order_number: promise_offer.work_order_number,
        price: purchase_amount,
        description: description,
        producttype: ASTRSHIELD_PRODUCT_TYPE
      }

      body.to_query
    end

    def log_attempt
      BscLogger.info "BSC - Attempting charge for offer with VNUM #{promise_offer.vnum}."
    end

    def log_failure(message = '')
      BscLogger.error "BSC - Charge failed for offer with VNUM #{promise_offer.vnum}. Details - #{message}"
    end

    def log_success(message = '')
      BscLogger.info "BSC - Charge succeeded for offer with VNUM #{promise_offer.vnum}. Details - #{message}"
    end

    private

    def description
      "ASTRSHIELD #{promise_offer.days_selected}D #{promise_offer.miles_selected}M"
    end
  end
end
