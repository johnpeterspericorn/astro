# encoding: utf-8
require 'bsc_charges/request'

module BscCharges
  class PurchaseRequest < Request
    def purchase_amount
      promise_offer.rounded_promise_price
    end
  end
end
