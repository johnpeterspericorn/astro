# encoding: utf-8
require 'yaml'

module Localizer
  class Dasherizer
    TOP_KEY = 'dash'.freeze

    # can pass in a file to yaml, or some yaml data
    def initialize(yaml)
      @data = get_data(yaml)
    end

    def to_dash
      @data.tap do |data|
        dasherize data
        swap_out_language_name data
      end
    end

    private

    def swap_out_language_name(data)
      data[TOP_KEY] = data.delete(data.keys.first)
    end

    def get_data(yaml)
      if yaml.is_a?(String) && yaml.include?('.yml')
        path = Pathname.new(yaml)
        return YAML.load_file(path)
      elsif yaml.is_a?(Pathname)
        return YAML.load_file(yaml)
      else
        begin
          YAML.load(yaml)
        rescue e
          throw StardardError("Could not load yaml: #{yaml}")
        end
      end
    end

    def dasherize(data)
      data.each do |key, value|
        if value.is_a?(Hash)
          dasherize(value)
        elsif value.is_a?(Array)
          data[key] = value.map do |val|
            string_to_dash(val)
          end
        else
          data[key] = string_to_dash(value)
        end
      end
    end

    def string_to_dash(value)
      value = value.to_s
      value.split.map { |word| '-' * word.length }.join(' ')
    end
  end
end
