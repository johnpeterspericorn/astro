# encoding: utf-8
module ActiveRecordAttributeSanitizer
  def self.included(base)
    base.extend ClassMethods
  end

  def sanitized_attributes
    attributes.slice(*self.class.sanitized_attribute_names)
  end

  module ClassMethods
    RESERVED_ATTRIBUTE_NAMES = %w(id created_at updated_at).freeze

    def sanitized_attribute_names
      attribute_names - RESERVED_ATTRIBUTE_NAMES
    end
  end
end
