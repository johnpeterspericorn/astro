# encoding: utf-8
namespace :db do
  desc 'Update all user as invoiceable'
  task update_users_as_invoiceable: :environment do
    User.where(invoiceable: false).find_in_batches(batch_size: 100) do |group|
      group.each do |user|
        user.invoiceable = true
        user.save(validate: false)
      end
    end
  end
end
