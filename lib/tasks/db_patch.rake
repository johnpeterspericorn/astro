# encoding: utf-8
namespace :db_patch do
  desc 'Add Extension Days to existing purchases'
  task set_extension_days: :environment do
    search_params = {
      purchase_protect: 45, extended_protect: 60,
      market_protect: 100, total_protect: 100 }

    search_params.each do |key, value|
      date_from = value.days.ago.to_formatted_s(:db)
      sql = <<-SQL
        UPDATE promise_purchases AS gp
        SET extension_days = ge.additional_days,
        extension_reason = ge.reason
        FROM promise_extensions AS ge
        INNER JOIN vehicle_information vi ON vi.vnum = ge.vnum
        INNER JOIN promise_offers go ON vi.promise_offer_id = go.id
        INNER JOIN promise_purchases gp1 ON gp1.promise_offer_id = go.id
        WHERE gp.id = gp1.id
        AND gp.extension_days is NULL
        AND gp1.promised_at >= '#{date_from}'
        AND gp1.kind = '#{key}'
      SQL

      command = ActiveRecord::Base.connection.execute(sql)
      puts "Rows Updated(#{key}): #{command.cmd_status}"
    end
  end
end
