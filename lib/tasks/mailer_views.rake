# encoding: utf-8
namespace :mailer_views do
  namespace :flat_rates do
    def with_sandbox
      ActiveRecord::Base.connection.increment_open_transactions
      ActiveRecord::Base.connection.begin_db_transaction
      yield
    ensure
      ActiveRecord::Base.connection.rollback_db_transaction
      ActiveRecord::Base.connection.decrement_open_transactions
    end

    desc 'Send a flat rate offer confirmation email'
    task offer_confirmation: :environment do
      buyer_info = FactoryGirl.create(:buyer_information)
      batch = FactoryGirl.create(:flat_rate_offer_batch, pawn_access_no: buyer_info.pawn_access_no)
      FlatRateMailer.offer_confirmation(batch).deliver
    end

    task offer_accepted: :environment do
      with_sandbox do
        buyer_info = FactoryGirl.create(:buyer_information)
        accepting_user = FactoryGirl.create(:buyer_user)
        batch = FactoryGirl.create(:flat_rate_offer_batch,
                                   pawn_access_no: buyer_info.pawn_access_no)
        FlatRateMailer.offer_accepted(batch, accepting_user, Time.current).deliver
      end
    end

    task offer_rejected: :environment do
      with_sandbox do
        buyer_info = FactoryGirl.create(:buyer_information)

        batch = FactoryGirl.create(:flat_rate_offer_batch,
                                   pawn_access_no: buyer_info.pawn_access_no)

        flat_rate_offer = batch.flat_rate_offers.first
        FactoryGirl.create(:agreement_information, agreement_no: flat_rate_offer.agreement_no)

        FlatRateMailer.offer_rejection(batch).deliver
      end
    end
  end
end
