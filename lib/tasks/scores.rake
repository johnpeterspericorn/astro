# encoding: utf-8
namespace :scores do
  desc 'Seed DB with scores'
  task seed: :environment do
    raise "Can't do this in Production" if Rails.env.production?
    agreement_no = ENV.fetch('DEALER_NO') { '5241420' }
    FactoryGirl.create(:dashboard_score,
                       buyer_num: agreement_no,
                       avg_loss: rand(3000) + 1,
                       return_rate: rand(3000) / 100.to_f,
                       volume: rand(50),
                       margin: rand(6000) / 100.to_f + 10,
                       earnings: rand(1_000_000) / 100.to_f,
                       return_score: rand(200),
                       purchase_quality: rand(200),
                       margin_score: rand(190),
                       volume_score: rand(195),
                       earnings_score: rand(199))
  end
end
