# encoding: utf-8
desc 'Fetch and update all shipments without a label URL'
task update_shipments_label_urls: :environment do
  Shipment.where(label_url: nil).each do |shipment|
    begin
      shipment_label = ShipmentLabel.find(shipment.easypost_shipment_id)
      shipment.update_attributes!(label_url: shipment_label.url)
    rescue EasyPost::Error => error
      puts "Could not update label url for shipment #{shipment.id}: #{error.message}"
    end
  end
end
