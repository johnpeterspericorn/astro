# encoding: utf-8
namespace :jobs do
  desc 'Ensure recurring delayed jobs are scheduled'
  task schedule_recurring: :environment do
    AccountsReceivableReportJob.schedule
    AutomaticPurchasesJob.schedule
    PaymentReportJob.schedule
    SalesReportJob.schedule
    CancelledReturnReportJob.schedule
    OriginatingPurchasesReportJob.schedule
    TitleNotificationJob.schedule
  end
end
