# encoding: utf-8
desc 'Fetch and update all shipments without a carrier'
task update_shipments_carrier: :environment do
  shipments = Shipment.where('carrier IS NULL and created_at > ?', 3.months.ago)
  puts "**** Found #{shipments.count} shipment records 3 months ago with nil carrier information **** \n\n"
  shipments_count = shipments.count
  puts "**** Starting carrier information updates for all shipments ***** \n\n"
  shipments.each do |shipment|
    begin
      shipment_label = ShipmentLabel.find(shipment.easypost_shipment_id)
      shipment.update_attributes!(carrier: shipment_label.carrier)
      puts "**** Updated carrier of shipment id: #{shipment.id} with #{shipment_label.carrier} ***** \n\n"
    rescue EasyPost::Error => error
      puts "Could not update carrier for shipment #{shipment.id}: #{error.message}"
      shipments_count -= 1
    end
  end
  puts "**** Finished updating #{shipments_count} shipment records with carrier information **** \n\n"
end
