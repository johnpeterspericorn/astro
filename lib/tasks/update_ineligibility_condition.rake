# encoding: utf-8
namespace :db do
  desc 'Update Ineligibility Condition'
  task update_ineligibility_condition: :environment do
    begin
      notice = IneligibilityCondition.find_by_condition_id(3)
      notice.update_attributes(notice: 'We are unable to promise vehicles older than 20 model years.')
      puts "Updated ineligibility_condition : #{notice.condition_id}"
    rescue => e
      puts "Could not update ineligibility_condition : #{e.message}"
    end
  end
end
