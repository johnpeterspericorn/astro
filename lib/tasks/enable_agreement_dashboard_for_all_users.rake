# encoding: utf-8
namespace :db do
  desc 'Enable Agreement Dashboard For All users'
  task enable_agreement_dashboard_for_all_users: :environment do
    User.all.find_in_batches(batch_size: 100) do |group|
      group.each do |user|
        user.agreement_transaction_viewer = true
        user.agreement_score_viewer = true
        user.save(validate: false)
      end
    end
  end
end
