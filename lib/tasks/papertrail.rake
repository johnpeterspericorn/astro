# encoding: utf-8
desc 'remove excess version data collected by papertrail'
namespace :papertrail do
  task trim: :environment do
    time_ago = 2.months.ago
    PaperTrail::Version.delete_all ['created_at < ?', time_ago]
  end
end
