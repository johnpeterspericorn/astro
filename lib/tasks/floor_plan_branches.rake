# encoding: utf-8
require 'csv'

desc 'Dump fp branch address validity to csv'
task dump_floor_plan_branches: :environment do
  branches = FloorPlanBranch.all
  file_path = 'tmp/floor_plan_branches.csv'

  CSV.open(file_path, 'wb') do |csv|
    branches.each do |branch|
      address = branch.to_address
      csv << [branch.attributes, address, address.valid?]
    end
  end

  puts "CSV written to #{file_path}"
end
