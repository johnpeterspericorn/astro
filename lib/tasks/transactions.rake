# encoding: utf-8
namespace :transactions do
  desc 'Seed DB with transactions'
  task seed: :environment do
    raise "Can't do this in Production" if Rails.env.production?
    require 'vnumbot'
    pawn_access_no = ENV.fetch('AUCTION_ACCESS_NO') { '100686866' }
    agreement_no = ENV.fetch('DEALER_NO') { '5241420' }
    transactions_to_be_created = ENV.fetch('RECORD_COUNT') { 300 }
    pawn_locations = PawnLocation.all
    transactions_to_be_created.to_i.times do |_i|
      purchased_at = rand(90).days.ago
      vehicle = Vinbot::Vehicle.new
      vehicle_purchase_price = rand(25_000) + 10_000
      pawn_location = pawn_locations[rand(pawn_locations.count)]
      vehicle_information = FactoryGirl.create :vehicle_information,
                                               make: vehicle.make,
                                               model: vehicle.model,
                                               vnum: vehicle.vnum,
                                               year: vehicle.year,
                                               odometer_reading: rand(50_000) + 1
      pawn_information = FactoryGirl.create :pawn_information,
                                               pawn_access_no: pawn_access_no,
                                               agreement_no: agreement_no,
                                               pawn_location: pawn_location,
                                               vehicle_purchase_price: vehicle_purchase_price
      offer = FactoryGirl.create :day_old_promise_offer,
                                 pawn_access_no: pawn_access_no,
                                 pawn_information: pawn_information,
                                 purchased_at: purchased_at,
                                 days_selected: [7,14,21][rand(2)],
                                 vehicle_information: vehicle_information
      FactoryGirl.create :promise_purchase, pawn_access_no: pawn_access_no, promise_offer: offer
    end
  end
end
