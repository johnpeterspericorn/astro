# encoding: utf-8
namespace :db do
  desc 'Update all user as invoiceable'
  task floor_plan_branch_trim_white_spaces: :environment do
    FloorPlanBranch.all.find_in_batches(batch_size: 100) do |group|
      group.each do |branch|
        branch.site_no = branch.site_no.strip
        branch.save
      end
    end

    FloorPlanBranchSelection.all.find_in_batches(batch_size: 100) do |group|
      group.each do |branch|
        branch.site_no = branch.site_no.strip
        branch.save
      end
    end
  end
end
