# encoding: utf-8
require_relative Rails.root.join('spec', 'mas_data_helper')
desc 'Seeding processes outside of seeds.rb'

namespace :db do
  task seed: [
    'seed:create_pawn_locations',
    'seed:create_users',
    'seed:create_vehicles',
    'seed:create_pricing_schedule_information'
  ]
end

namespace :seed do
  desc 'Create all necessary users'
  task create_users: [:create_kioskuser, :create_superuser]
  task create_vehicles: [:create_sold_vehicles, :create_presale_vehicles]

  desc 'Create KioskUser'
  task create_kioskuser: :environment do
    user = User.find_by(kiosk_user: true, username: 'kiosk')
    unless user.present?
      puts 'Creating Kiosk User'
      User.create!(kiosk_user: true, username: 'kiosk', password: 'swipe')
    end
  end

  desc 'Create SuperUser'
  task create_superuser: :environment do
    user = User.find_by(super_user: true, username: 'astro')
    unless user.present?
      puts 'Creating Super User'
      User.create!(
        super_user: true,
        username: 'astro',
        password: 'simulcast',
        email: 'astro@astro.local',
        admin: true,
        user_admin_editor: true,
        invoiceable: true,
        loss_prevention_viewer: true,
        flat_rate_editor: true,
        feature_flag_editor: true,
        vehicle_creator: true,
        promise_admin_viewer: true,
        promise_admin_editor: true,
        agreement_transaction_viewer: true,
        agreement_score_viewer: true,
        finance_admin_viewer: true
      )
    end
  end

  desc 'Seed Sold Vehicle Data'
  task create_sold_vehicles: :environment do
    puts 'Seeding Sold Vehicle Data'
    6.times do
      FactoryGirl.create(:sold_vehicle, odometer_reading: 100, limited_volume: NewProductKind::LEVEL_3)
    end
    4.times do
      FactoryGirl.create(:sold_vehicle, odometer_reading: 100)
    end
  end

  desc 'Seed Presale Vehicle Data'
  task create_presale_vehicles: :environment do
    puts 'Seeding Presale Data'
    10.times do |i|
      FactoryGirl.create(
        :presale_vehicle,
        location_initials: PawnLocation.first.initials,
        lane_no: i,
        promise_250_low:    250 + 10 * i,
        low_valuation:        250 + 10 * i,
        promise_250_medium: 250 + 10 * i,
        average_valuation:    250 + 10 * i,
        promise_250_high:   250 + 10 * i,
        high_valuation:       250 + 10 * i,
        seller_no:            '5000000'
      )
    end
  end

  desc 'Seed Pawn Locations'
  task create_pawn_locations: :environment do
    puts 'Seeding Pawn Location Data'
    CSV.open(Rails.root.join('db', 'PawnLocations.csv')).each do |initials, name|
      PawnLocation.create!(initials: initials, name: name)
    end
  end

  desc 'Seed Pricing Schedule Information'
  task create_pricing_schedule_information: :environment do
    puts 'Seeding Pricing Schedule Information'

    risk_factor_modifier = SasDataHelper::DEFAULT_RISK_FACTOR
    agreement_number        = SasDataHelper::DEFAULT_DEALER_NUMBER
    range_lower_limit    = SasDataHelper::DEFAULT_RANGE_LOWER_LIMIT
    range_upper_limit    = SasDataHelper::DEFAULT_RANGE_UPPER_LIMIT
    vehicle_code         = SasDataHelper::DEFAULT_VEHICLE_CODE

    risk_factor = RiskFactor.create!(risk_modifier: risk_factor_modifier)
    AgreementRiskFactor.create!(agreement_no: agreement_number, risk_factor: risk_factor)
    PriceRange.create(risk_factor: risk_factor, vehicle_code: vehicle_code, lower_limit: range_lower_limit, upper_limit: range_upper_limit)
  end

  desc 'Seed Vehicle Categories'
  task create_vehicle_categories_and_pricing_distributions: :environment do
    VehicleCategory.destroy_all
    PricingDistribution.destroy_all

    # category,low,avg,high
    # All Vehicles,59,165,625
    # AS IS,75,160,375
    # COMPACT CAR,75,135,300
    # FULLSIZE CAR,75,155,275
    # LUXURY CAR,100,190,475
    # MIDSIZE CAR,75,155,325
    # PICKUP,100,190,525
    # SUV,100,180,425
    pricing_distributions = {
      'All Vehicles' => [59, 165, 625],
      'Compact'      => [75, 135, 300],
      'Mid-Size'     => [75, 155, 325],
      'Full-Size'    => [75, 155, 275],
      'Luxury'       => [100, 190, 475],
      'SUV'          => [100, 180, 425],
      'Truck'        => [100, 190, 525],
      'As-Is'        => [75, 160, 375]
    }
    pricing_distributions.each do |category_name, pricing|
      category = VehicleCategory.new(name: category_name)
      next unless category.save
      puts "Created Vehicle Category: '#{category_name}'"
      low_price, median_price, high_price = pricing
      distribution = PricingDistribution.new(vehicle_category_id: category.id, risk_factor_id: RiskFactor.default.id, low_price: low_price, median_price: median_price, high_price: high_price)
      if distribution.save
        puts "\tCreated pricing distribution for '#{category_name}' vehicles"
      end
    end
  end

  desc 'Freshen a users sold vehicles, or give them some'
  task :freshen, [:pawn_access_number, :count] => :environment do |_t, args|
    args.with_defaults(pawn_access_number: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER, count: 10)
    vehicles = SoldVehicle.where(pawn_access_no: args[:pawn_access_number])
    vehicles += SoldVehicle.where('pawn_access_no != ?', args[:pawn_access_number]).limit(args[:count] - vehicles.count) if vehicles.count < args[:count]
    vehicles.map { |sv| sv.update_attributes(created_at: 5.minutes.ago, pawn_access_no: args[:pawn_access_number]) }
    vehicles.each do |sv|
      offer = PromiseOffer.by_vnum(sv.vnum).first
      offer && offer.update_attributes(created_at: 5.minutes.ago)
    end
  end

  desc 'A custom task to keep staging in good shape'
  task cleanup_and_freshen_100686866: :environment do |_t, _args|
    vehicles = SoldVehicle.where(pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER)
    vehicles.map { |sv| sv.update_attributes(created_at: 5.minutes.ago, pawn_access_no: SasDataHelper::DEFAULT_AUCTION_ACCESS_NUMBER) }
    vehicles.each do |sv|
      offers = PromiseOffer.by_vnum(sv.vnum)
      purchase_ids = offers.flat_map(&:promise_purchase).compact.map(&:id)

      ReturnInvoice.unscoped.where(promise_purchase_id: purchase_ids).destroy_all
      offers.destroy_all
    end
  end

  desc 'Create default pending reasons'
  task create_pending_reasons: :environment do
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::APPROVAL_REQUIRED, 'This offer requires approval')
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::BLACKLISTED, 'This offers agreement has been blacklisted')
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::GUARANTEE_PRICE_EXCEEDED, 'The promise price exceeded the threshold requiring approval')
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::VEHICLE_PRICE_RATIO_EXCEEDED, 'Vehicle price ratio exceeded')
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::VEHICLE_PRICE_EXCEEDED, 'The vehicle purchase price exceeded the threshold requiring approval')
    PendingReason.find_or_create_by_code_and_notice(PendingConditions::IF_BID, 'This is an "if bid" offer')

    PendingReason.find_or_create_by_code_and_notice(7, 'Buyer-Seller Factors exceed threshold')
    PendingReason.find_or_create_by_code_and_notice(8, 'Crossover Representation')
    PendingReason.find_or_create_by_code_and_notice(9, 'Return rate exceeds threshold')
    PendingReason.find_or_create_by_code_and_notice(10, 'Unusual buyer purchase behavior')
  end
end
