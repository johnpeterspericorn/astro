# encoding: utf-8
namespace :db do
  task create_extranal_vehicle_branch_entry: :environment do
    batches = ExternalVehiclesLoad.select('distinct on (batch_id) *')
    batches.each do |batch|
      vehicles = ExternalVehiclesLoad.where(batch_id: batch.batch_id)
      external_vehicles_batch = ExternalVehiclesBatch.create(status: 'posted')
      vehicles.each do |vehicle|
        vehicle.external_vehicles_batch_id = external_vehicles_batch.id
        vehicle.save
        external_vehicles_batch.status = 'unposted' unless vehicle.status == 'posted'
        external_vehicles_batch.user_id = vehicle.user_id
      end
      external_vehicles_batch.save
    end
  end
end
