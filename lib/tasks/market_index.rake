# encoding: utf-8
namespace :market_index do
  desc 'Seed DB with Market Index'
  task seed: :environment do
    raise "Can't do this in Production" if Rails.env.production?
    market_factors = [
      {
        title: 'Consumer Confidence',
        description: 'Consumer confidence is xyz, We use ftc.gov/consumerIndex to measure consumer confidence',
        color: '#EE0000'
      },
      {
        title: 'Wholesale Volumes',
        description: "We use registration volumes from OVE. For more infomrmation see <a href='#'>ove.com/wholesale_volumes_index</a>",
        color: '#EE0000'
      },
      {
        title: 'Retail Volumes',
        description: 'It contains a text field followed by a button to trigger the addition of item in the Todo list. This component will hold the current item entered in the textfield and both of them are kept in sync using the onChange event. As soon as the submit button is pressed, the item is passed to its parent component',
        color: '#FFFFFF'
      },
      {
        title: 'Astroheim Index',
        description: 'Tracks the year over year changes in vehicle pricing at Astroheim pawns. Tracks the year over year changes in vehicle pricing at Astroheim pawns.',
        color: '#FFFFFF'
      }
    ]

    market_index = MarketIndex.create(value: 84, notes: 'We understand it\'s getting harder to get the right vehicles at the right price. Now\'s a good time to familiarize yourself with how your performance can impact your rate.')
    market_index.market_factors.create(market_factors)
  end
end
