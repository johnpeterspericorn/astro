# encoding: utf-8
desc 'Fetch the payment adjustment fees from adjustment api'
task create_payment_adjustments: :environment do
  Payment.all.each do |payment|
    next unless payment.promise_purchase.present?
    fees = payment.promise_purchase.fees
    fees.nonreimbursable_fees.each do |fee|
      PaymentAdjustmentFee.create(
        item_description: fee.name,
        fee_amount: fee.amount,
        payment_id: payment.id,
        adjustment_code: fee.code
      )
    end

    payment.update_attributes(amount: payment.decorate.promise_price)
  end
end
