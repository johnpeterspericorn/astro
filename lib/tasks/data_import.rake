# encoding: utf-8
require 'qa_csv'

desc 'Import data from production'

IMPORT_TABLES = %w(
  presale_vehicles
  sold_vehicles
  promise_offers
  pawn_information
  vehicle_information
  inspection_information
  promise_purchases
  inspection_purchases
  return_invoices
  payments
  payment_responses
).freeze

namespace :db do
  desc "Grab current staging schema and data using deploy's credentials (make sure her SSH key is loaded)"
  task import_all_from_staging: :environment do
    import_database from: staging_database_server, via: staging_web_server
  end

  desc "Grab current production schema and data using deploy's credentials (make sure her SSH key is loaded)"
  task import_all_from_prod: :environment do
    import_database from: production_database_server, via: production_web_server
  end

  desc 'Snapshot the current production database into a tarball in tmp/'
  task dump_all_from_prod: :environment do
    snapshot_database from: production_database_server, via: production_web_server
  end

  desc 'Snapshot the current staging database into a tarball in tmp/'
  task dump_all_from_staging: :environment do
    snapshot_database from: staging_database_server, via: staging_web_server
  end

  desc "Grab data only from prod using deploy's credentials"
  task import_data_from_prod: :environment do
    puts 'Truncating local tables...'
    truncate_tables
    import_tables from: production_database_server, via: production_web_server
  end

  desc 'Grab data only from prod and save as a dump'
  task dump_data_from_prod: :environment do
    import_tables from: production_database_server, via: production_web_server, dump: true
  end

  desc 'Pull data from prod and export the rows in $TABLE to spec/fixtures/$CSV'
  task export_prod_to_qa: :data_import_from_prod do
    model = ENV['TABLE'].constantize
    file = Rails.root.join('spec', 'fixtures', ENV['CSV'])

    QaCsv.export(model, file)
  end

  # Convenience task since we'll usually be pulling sold vehicles
  desc 'Pull data from prod and export the rows in SoldVehicle to spec/fixtures/$CSV'
  task export_prod_sold_vehicles_to_qa: :data_import_from_prod do
    file = Rails.root.join('spec', 'fixtures', ENV['CSV'])

    QaCsv.export(SoldVehicle, file)
  end

  desc 'Snapshot the current staging database into a tarball in staging web server /var/www/shared/dump'
  task remote_snapshot_staging: :environment do
    table_switches = IMPORT_TABLES.map { |table| "-t #{table}" }.join(' ')
    command = "ssh deploy@#{staging_web_server} 'pg_dump astro -h #{staging_database_server} -U astro --clean #{table_switches} | gzip > /var/www/shared/dump/astro.sql.gz'"
    puts `#{command}`
  end

  desc 'Restore staging database from most recent archive'
  task remote_restore_staging: :environment do
    command = "ssh deploy@#{staging_web_server} 'gunzip --stdout /var/www/shared/dump/astro.sql.gz | psql astro -h #{staging_database_server} -U astro'"
    puts `#{command}`
  end
end

def truncate_tables
  IMPORT_TABLES.each do |table|
    ActiveRecord::Base.connection.execute("TRUNCATE #{table}")
  end
end

def import_tables(options)
  database_server = options[:from]
  web_server      = options[:via]

  table_switches = IMPORT_TABLES.map { |table| "-t #{table}" }.join(' ')

  dump_command  = "pg_dump astro --data-only #{table_switches} -h #{database_server} -U astro -W"
  local_command = options[:dump] ? local_archive_command : local_postgres_command

  command = "ssh deploy@#{web_server} '#{dump_command}' | #{local_command}"
  puts "Executing `#{command}`"
  puts `#{command}`
end

def import_database(options)
  execute_database_command(options, local_postgres_command)
end

def config
  @config ||= Rails.configuration.database_configuration
end

def user_switch
  @user_switch ||= %(-U #{config[Rails.env]['username']}) if config[Rails.env]['username']
end

def host_switch
  @host_switch ||= %(-h #{config[Rails.env]['host']}) if config[Rails.env]['host']
end

def local_postgres_command
  "psql #{user_switch} #{host_switch} #{config[Rails.env]['database']}"
end

def archive_filename
  timestamp = DateTime.current.strftime('%Y_%m_%d-%T%:z')
  "tmp/astro_#{timestamp}.sql.gz"
end

def local_archive_command
  "gzip > #{archive_filename}"
end

def snapshot_database(options)
  execute_database_command(options, local_archive_command)
  puts "Archived current database into '#{archive_filename}'"
end

def execute_database_command(options, local_command)
  web_server = options[:via]
  raise RuntimeError('web server must be specified') unless web_server

  command = %(ssh deploy@#{web_server} '#{remote_dump_command(options)}' | #{local_command})
  puts "Executing `#{command}`"
  puts ` #{command} `
end

def remote_dump_command(options)
  database_server = options[:from]
  raise RuntimeError('database server must be specified') unless database_server

  "pg_dump astro --clean -h #{database_server} -U astro -W"
end

def staging_web_server
  'mdx-dc4qa2-web01.ove.local'
end

def staging_database_server
  'mdx-dc4qa2-db01.ove.local'
end

def production_web_server
  'mdx-dc3prod2-web01.ove.local'
end

def production_database_server
  'mdx-dc3prod2-db01.ove.local'
end
