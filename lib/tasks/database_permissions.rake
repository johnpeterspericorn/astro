# encoding: utf-8
MAS_WRITE_TABLES = %w(
  ineligibility_conditions
  sold_vehicles
  sold_vehicles_load
  presale_vehicles
  presale_vehicles_load
  pawn_access_number_blacklists
  agreementship_blacklists
  pending_reasons
  vnum_blacklists
  flat_rates
  floor_plan_companies
  floor_plan_branches
).freeze

ASTROHEIM_DEVS_READ_TABLES = %w(
  promise_offers
  sold_vehicles
  presale_vehicles
).freeze

namespace :db do
  desc 'Add necessary DB grants for the `mas` user'
  task grant_permissions: :environment do
    ActiveRecord::Base.connection.execute(permissions_sql)
  end

  def permissions_sql
    <<-SQL
      BEGIN;
        #{grant_select_sql}
        #{grant_write_sql}
      COMMIT;
    SQL
  end

  def grant_select_sql
    mas_grants = ActiveRecord::Base.connection.tables.map do |table|
      "GRANT SELECT ON #{table} TO mas;"
    end.join("\n")

    astroheim_devs_grants = ASTROHEIM_DEVS_READ_TABLES.map do |table|
      "GRANT SELECT ON #{table} TO astroheim_devs;"
    end.join("\n")

    [mas_grants, astroheim_devs_grants].join("\n")
  end

  def grant_write_sql
    MAS_WRITE_TABLES.map do |table|
      ["GRANT ALL PRIVILEGES ON #{table} TO mas;",
       "GRANT ALL PRIVILEGES ON #{table}_id_seq TO mas;"]
    end.join("\n")
  end
end
