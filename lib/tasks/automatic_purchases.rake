# encoding: utf-8
desc 'Process automatic promise purchases'
task process_outstanding_automatic_purchases: :environment do
  AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
end
