# encoding: utf-8
class DistanceConverter
  DEFAULTS = {
    distance_units: {
      format: '%n %u',
      unit: 'Miles'
    }
  }.freeze

  def initialize(number, country_code = nil)
    @country_code = country_code
    @number = number
  end

  def convert
    format = options.fetch(:format)
    unit = options.fetch(:unit)
    format.gsub('%n'.freeze, @number.to_s).gsub('%u'.freeze, unit)
  end

  private

  def options
    default_options.merge(i18n_options)
  end

  def default_options
    DEFAULTS[:distance_units]
  end

  def i18n_options
    return {} unless @country_code
    I18n.translate("distance_units.#{@country_code}")
  end
end
