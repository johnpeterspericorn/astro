# encoding: utf-8
require_relative './internet_note_message_data'

module Astroheim
  module SalvageApi
    class AddNoteMessageData
      attr_reader(
        :pawn_location_initials,
        :notes,
        :sblu,
        :work_order
      )

      def initialize(attributes, note_class = InternetNoteMessageData)
        @pawn_location_initials = attributes.fetch(:pawn_location_initials, &default_missing_attr)
        @sblu       = attributes.fetch(:sblu, &default_missing_attr)
        @work_order = attributes.fetch(:work_order, &default_missing_attr)

        note_attributes = [attributes.fetch(:notes, nil)].flatten.compact
        @notes = note_attributes.map { |attrs| note_class.new(attrs) }
      end

      def to_h
        {
          request: {
            pawn_id:     pawn_location_initials,
            internet_notes: notes.map(&:to_h),
            sblu:           sblu,
            work_order:     work_order
          }
        }
      end

      private

      def default_missing_attr
        ->(attr) { raise ArgumentError, "#{attr} is required" }
      end
    end
  end
end
