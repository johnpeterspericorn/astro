# encoding: utf-8
module Astroheim
  module SalvageApi
    class InternetNoteMessageData
      attr_reader(
        :added_by,
        :added_date,
        :text,
        :type
      )

      def initialize(attributes)
        @added_by   = attributes.fetch(:added_by,   &default_missing_attr)
        @added_date = attributes.fetch(:added_date, default_added_date)
        @text       = attributes.fetch(:text,       &default_missing_attr)
        @type       = attributes.fetch(:type,       &default_missing_attr)
      end

      def to_h
        {
          item: {
            added_by:   added_by,
            added_date: formatted_added_date,
            note_text:  text,
            note_type:  type
          }
        }
      end

      private

      def formatted_added_date
        added_date.strftime('%Y%m%d')
      end

      def default_added_date
        Date.today
      end

      def default_missing_attr
        ->(attr) { raise ArgumentError, "#{attr} is required" }
      end
    end
  end
end
