# encoding: utf-8
module Astroheim
  module SalvageApi
    WSDL = if Rails.env.production?
             Rails.root.join('lib', 'support', 'salvage_prod.wsdl')
           else
             Rails.root.join('lib', 'support', 'salvage_preprod.wsdl')
           end
    USER = if Rails.env.test?
             'test'
           else
             EnvironmentConfig.fetch('SALVAGE_API_USER')
           end
    PASS = if Rails.env.test?
             'test'
           else
             EnvironmentConfig.fetch('SALVAGE_API_PASS')
           end

    Client = Savon.client(
      wsdl:   WSDL,
      basic_auth: [USER, PASS],
      logger: SalvageLogger
    )
  end
end
