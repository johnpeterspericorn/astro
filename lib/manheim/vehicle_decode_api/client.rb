# encoding: utf-8
require 'typhoeus'
require 'cgi'

module Astroheim
  module VehicleDecodeApi
    class Client
      ENDPOINT = ENV.fetch('VEHICLE_DECODER_ENDPOINT')

      USERNAME = ENV.fetch('VEHICLE_DECODER_USERNAME')
      PASSWORD = ENV.fetch('VEHICLE_DECODER_PASSWORD')

      def initialize(vnum)
        @vnum = vnum
      end

      def decode_vnum
        logger.info "Attempting VNUM Decode Request with resource URL: #{resource_url}"
        request.run

        if successful_response?
          if response_body['item'] && response_body['item'].is_a?(Array)
            vehicle_trim = response_body['item'].first
            if vehicle_trim && vehicle_trim['mmr']
              mmr_decoded = vehicle_trim['mmr']

              make = mmr_decoded['makeMake']
              model = mmr_decoded['modelModel']
              year = mmr_decoded['year']
              mid = mmr_decoded['mid']

              logger.info "Successful Response: #{mmr_decoded.inspect}"
              return { make: make, model: model, year: year, mid: mid }
            end
          end
          logger.error 'Error: JSON response body is invalid'
          return { errors: 'JSON response body is invalid' }
        else
          logger.error "Error: #{response_body.inspect}"
          return response_body
        end
      end

      private

      def request
        @request ||= Typhoeus::Request.new(
          resource_url,
          method: :get,
          userpwd: username_password,
          headers: { 'Content-Type' => 'application/json' },
          verbose: true
        )
      end

      def logger
        Rails.logger
      end

      def resource_url
        "#{ENDPOINT}/#{@vnum}/?decoderType=mmr,pawn,chrome"
      end

      def username_password
        "#{USERNAME}:#{PASSWORD}"
      end

      def successful_response?
        response_body[:errors].blank?
      end

      def response_body
        @response_body ||= response
      end

      def response
        body = request.response.body
        content_type = request.response.headers['Content-Type']
        response_code = request.response.response_code
        logger.info "request.response: body => #{body} | content_type => #{content_type} "
        if /json/.match(content_type) && request.response.success?
          begin
            JSON.parse body
          rescue JSON::ParserError
            { errors: 'JSON Parse Error' }
          end
        else
          { errors: "Error response code[#{response_code}] or unknown content-type" }
        end
      end
    end
  end
end
