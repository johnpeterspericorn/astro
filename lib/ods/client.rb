# encoding: utf-8
require 'ods/configuration'
require 'ods/response'
require 'ods/request'

module Ods
  class Client
    class OdsIneligibleArgumentsError < ArgumentError; end

    attr_reader :agreement_no, :location_initials, :sale_year, :sale_number, :lane_number, :run_number

    def initialize(options = {})
      @agreement_no = options[:agreement_no]
      @location_initials = options[:location_initials]
      @sale_year = options[:universal_no][/\d{4}/] if options[:universal_no].present?
      @sale_number = options[:sale_no]
      @lane_number = options[:lane_no]
      @run_number = options[:run_no]

      raise OdsIneligibleArgumentsError if [@agreement_no, @location_initials, @sale_year, @sale_number, @lane_number, @run_number].any?(&:nil?)
    end

    def request_vehicle_info
      info = {}
      return false if vehicle_agreement_request.blank? || vehicle_agreement_request == Ods::Response::INVALID
      info[:left_lot] = (vehicle_agreement_request['vehicleStatus'] == Ods::Configuration::VEHICLE_STATUS)
      url_path = vehicle_agreement_request['invoice']['href']
      invoice_path = url_path.remove(request.endpoint)
      vehicle_invoice_request = invoice_request_response(invoice_path)

      payment_method = vehicle_invoice_request['payments'][0]['paymentMethod']
      info[:paid_by_floor_plan] = (payment_method == Ods::Configuration::VEHICLE_FLOORED)
      info[:payment_type] = payment_method
      info[:has_agreementshield_charge] = agreementshield_charge_present?(vehicle_invoice_request)
      info
    end

    def left_lot?
      return false if vehicle_agreement_request.blank?
      vehicle_agreement_request['vehicleStatus'] == Ods::Configuration::VEHICLE_STATUS
    end

    private

    def request
      Ods::Request.new
    end

    def api_output(response)
      Ods::Response.new(response, location_initials, sale_year, sale_number, lane_number, run_number)
    end

    def agreement_path
      "/orders/purchases/customer/#{agreement_no}?pawn=#{location_initials}&saleYear=#{sale_year}&saleNumber=#{sale_number}"
    end

    def agreement_request_response
      @agreement_request_response ||= request.invoke(agreement_path)
    end

    def invoice_request_response(invoice_path)
      @invoice_request_response ||= request.invoke(invoice_path)
    end

    def agreementshield_charge_present?(invoice_response)
      ds_jmes_expr =  "details[].itemDescriptions[?value == 'DEALA' || value == 'DEALB' || value == 'DEALS']"
      agreementshield_charges = JMESPath.search(ds_jmes_expr, invoice_response)
      agreementshield_charges.is_a?(Array) ? agreementshield_charges[0].present? : false
    end

    def vehicle_agreement_request
      response = agreement_request_response
      api_output(response).vehicle if response != Ods::Response::INVALID
    end
  end
end
