# encoding: utf-8
module Ods
  class Configuration
    SERVICE = 'ods'.freeze
    VEHICLE_STATUS = 'Left Lot'.freeze
    VEHICLE_FLOORED = 'Floored'.freeze

    def self.endpoint
      ENV.fetch('ODS_ENDPOINT')
    end

    def self.username
      ENV.fetch('ODS_USER')
    end

    def self.password
      ENV.fetch('ODS_PASS')
    end
  end
end
