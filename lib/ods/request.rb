# encoding: utf-8
module Ods
  class Request
    def api_request(access_token, path)
      endpoint_url = endpoint + path
      method = :get
      headers = { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{access_token}" }
      Typhoeus::Request.new(endpoint_url, method: method, headers: headers, verbose: true)
    end

    def invoke(path)
      access_token = token.generate_token(service_name, endpoint, username, password)
      return [] if access_token.blank?
      response = api_request(access_token, path).run
      auth_header = response.headers['WWW-Authenticate']
      if response.response_code != 200 || (auth_header.present? && auth_header.include?('invalid_token'))
        token.remove_expired_token(service_name)
        response = api_request(token.persist_token(service_name, endpoint, username, password), path).run
        return Ods::Response::INVALID if response.response_code != 200
      end
      JSON.parse(response.response_body)
    end

    def endpoint
      Ods::Configuration.endpoint
    end

    private

    def username
      Ods::Configuration.username
    end

    def password
      Ods::Configuration.password
    end

    def service_name
      Ods::Configuration::SERVICE
    end

    def token
      Token.new
    end
  end
end
