# encoding: utf-8
module Ods
  class Response
    INVALID = 'Invalid'.freeze
    attr_reader :vehicle

    def initialize(api_output, location_initials, sale_year, sale_number, lane_number, run_number)
      return [] if api_output['items'].blank?
      output = api_output['items'].select do |item|
        item['offering']['operatingLocation']['locationCode'] == location_initials &&
          item['offering']['saleKey']['saleYear'].to_s == sale_year &&
          item['offering']['saleKey']['saleNumber'].to_s == sale_number &&
          item['offering']['saleKey']['laneNumber'].to_s == lane_number &&
          item['offering']['saleKey']['runNumber'].to_s == run_number
      end
      @vehicle = output.first
    end
  end
end
