# encoding: utf-8
module ColumnConversions
  def value_to_boolean(value)
    ActiveRecord::Type::Boolean.new.type_cast_from_database value
  end

  def arrayify(value, delimiter = ',')
    if value
      value.split(delimiter).map(&:strip)
    else
      []
    end
  end

  module_function :value_to_boolean, :arrayify
end
