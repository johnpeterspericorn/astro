# encoding: utf-8
module EmailTracker
  class Rack
    def initialize(app)
      @app = app
    end

    def call(env)
      req = ::Rack::Request.new(env)
      if req.path_info =~ %r{^\/email\/track\/(.+).png}
        details = Base64.decode64(Regexp.last_match[1])
        unique_code = nil
        email = nil

        details.split('&').each do |kv|
          (key, value) = kv.split('=')
          case key
          when 'unique_code'
            unique_code = value
          when 'email'
            email = value
          end
        end
        if unique_code && email
          email_tracker = EmailTracking.find_by_unique_code(unique_code)
          email_tracker.update_attributes(
            ip_address: req.ip,
            letter_opened: DateUtils.now) unless email_tracker.letter_opened.present?
        end

        [200, { 'Content-Type' => 'image/png' }, [File.read(Rails.root.join('app', 'assets', 'images', 'email-signature.png'))]]
      else
        @app.call(env)
      end
    end
  end
end
