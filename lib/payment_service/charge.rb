# encoding: utf-8

module PaymentService
  class Charge
    class Error < StandardError; end
    class << self
      def promise_purchase(user, promise_purchase, price_in_cents, order_id:, idempotency_key: nil)
        if user.payment_customer_id.nil?
          PaymentService::Customer.create(user)
          user.reload
        end

        idempotency_key = promise_purchase.to_gid if idempotency_key.nil?

        promise_offer = promise_purchase.promise_offer
        model = promise_offer.vehicle_information
        vehicle_description = "#{model.year} #{model.make} #{model.model}".titleize
        description = "[ASTRSHIELD] #{vehicle_description} (#{model.vnum})"

        new(
          amount_in_cents: price_in_cents,
          currency: 'usd',
          description: description,
          customer: user.payment_customer_id,
          order_id: order_id,
          idempotency_key: idempotency_key
        )
      end
    end

    attr_reader :card, :response

    def initialize(amount_in_cents:, description:, customer:, order_id:, currency: 'usd', idempotency_key: SecureRandom.uuid)
      @amount_in_cents = amount_in_cents
      @description = description
      @customer = customer
      @currency = currency
      @order_id = order_id
      @success = false
      @run = false
      @idempotency_key = idempotency_key
    end

    def success?
      run unless @run
      @success
    end

    def run
      attrs = {
        amount: @amount_in_cents,
        currency: @currency,
        description: @description,
        customer: @customer,
        metadata: {
          order_id: @order_id
        }
      }

      @run = true
      begin
        @response = Stripe::Charge.create(attrs, idempotency_key: @idempotency_key)
        @success = (@response.status == 'succeeded')
        @card = @response.source if success?

      rescue Stripe::InvalidRequestError, Stripe::CardError => e
        raise Error, e.message
      end
      @response
    end

    def balance_transaction
      @response.balance_transaction
    end
  end
end
