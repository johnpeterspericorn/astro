# encoding: utf-8

module PaymentService
  class Customer
    class Error < StandardError; end
    class << self
      def create(user)
        return unless user.payment_customer_id.nil?
        attrs = {
          description: "Customer ##{user.id}",
          metadata: {
            id: user.id
          }
        }
        response = Stripe::Customer.create(attrs, idempotency_key: user.to_gid)

        raise response.inspect if response.id.nil?
        user.payment_customer_id = response.id
        user.save!
      end
    end

    attr_reader :user, :id, :error

    def initialize(user)
      @user = user
      @error = nil
      @record = Stripe::Customer.retrieve(user.payment_customer_id)
    end

    def add_card(token_or_card_attrs)
      @record.source = token_or_card_attrs # obtained with Stripe.js
      begin
        @record.save
      rescue Stripe::CardError => e
        @error = e.message
        raise Error, e.message
      end
      @record
    end

    def card?
      @record.sources.data.any? { |c| c.object == 'card' && c.cvc_check == 'pass' }
    end
  end
end
