# encoding: utf-8
module NetworkPlus
  class NetworkPlusApiClient < Struct.new(:request)
    ENDPOINT_URL = ENV.fetch('NETWORK_PLUS_API_ENDPOINT')

    def post
      NetworkPlusLogger.info "Posting NetworkPlus Vehicle #{request.vnum}"
      NetworkPlusLogger.info "Request: #{request_body}"
      api_request.run
      NetworkPlusLogger.info "Response: #{api_request.response.response_code}"
      processed_response
    end

    private

    def api_request
      @api_request ||= Typhoeus::Request.new(
        ENDPOINT_URL,
        method: :post,
        timeout: Integer(ENV.fetch('MAS_TIMEOUT')),
        body: request_body
      )
    end

    def request_body
      {
        username:   ENV.fetch('NETWORK_PLUS_API_USER'),
        password:   ENV.fetch('NETWORK_PLUS_API_PASSWORD'),
        vnum: request.vnum,
        make: request.make,
        model: request.model,
        year: request.year,
        odometer: request.odometer_reading,
        buyer: request.agreement_no,
        hundredmil: 0,
        vehiclesaleprice: request.vehicle_purchase_price,
        buyfee: request.buy_fee,
        location: 'FTB',
        seller: '0',
        lane: request.lane_no,
        run: request.run_no,
        sale: request.sale_no,
        purchasedate: request.purchased_at,
        work_order_number: '0'
      }.to_query
    end

    def processed_response
      api_request.response.response_code
    rescue JSON::ParserError => e
      NetworkPlusLogger.error "Failure for #{request.vnum}: #{e.message}"
    end
  end
end
