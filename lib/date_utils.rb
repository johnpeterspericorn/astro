# encoding: utf-8
module DateUtils
  def self.resolve_to_weekday(date)
    if date.saturday? || date.sunday?
      date.next_week
    else
      date
    end
  end

  def self.now
    Time.now.in_time_zone
  end

  def self.timestamp
    Time.now.to_i
  end

  def self.tomorrow
    Time.current.tomorrow.end_of_day
  end

  def self.is_date_before_latter(date1, date2)
    date1.to_date < date2.to_date
  end
end
