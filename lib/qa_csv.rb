# encoding: utf-8
require 'csv'

class QaCsv < Struct.new(:model, :filename, :forbidden_columns)
  def initialize(model, filename, forbidden_columns = %w(id created_at updated_at))
    super model, filename, forbidden_columns
  end

  def self.export(model, filename)
    new(model, filename).export
  end

  def self.import(model, filename)
    new(model, filename).import
  end

  def columns
    @columns ||= if appending?
                   csv.read
                   csv.headers
                 else
                   model.columns.map(&:name) - forbidden_columns.map(&:to_s)
                 end
  end

  def export
    csv << columns unless appending?

    model.find_each do |row|
      csv << columns.map { |column| row.send(column) }
    end
  end

  def import
    csv.each do |row|
      model.create!(row.to_hash)
    end
  end

  private

  def csv
    @csv ||= CSV.open(filename, 'a+', headers: true)
  end

  def appending?
    File.exist?(filename)
  end
end
