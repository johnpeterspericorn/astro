# encoding: utf-8
class TaggedFormatter < Logger::Formatter
  def initialize(*tags, formatter)
    @tags = tags
    @formatter = formatter
  end

  def call(*args, log_message)
    tagged_message = tag_message(log_message)
    formatter.call(*args, tagged_message)
  end

  private

  attr_reader :tags, :formatter

  def tag_body
    tags
      .collect { |t| "[#{t}]" }
      .join(' ')
  end

  def request_id
    return unless RequestStore.store[:request_id]
    "REQ-#{RequestStore.store[:request_id]}"
  end

  def tag_message(message)
    [tag_body, request_id, message].compact.join(' ')
  end
end
