# encoding: utf-8
module HashExtensions
  def map_keys
    return to_enum(:map_keys) unless block_given?
    each_with_object(self.class.new) { |(k, v), h| h[yield(k)] = v }
  end
end
