# encoding: utf-8
require_relative './localizer/dasherizer'
module Localizer
  class << self
    def dasherize(dir_path)
      write_dash(dir_path)
      for_each_directory(dir_path) do |directory|
        write_dash(directory)
      end
    end

    def write_dash(directory)
      directory = Pathname.new(directory)
      en_yaml_file   = directory.join('en.yml')
      dash_yaml_file = directory.join('dash.yml')
      dash_yaml = Localizer::Dasherizer.new(en_yaml_file).to_dash

      IO.write(dash_yaml_file, YAML.dump(dash_yaml))
    end

    def for_each_directory(root, &block)
      root = Pathname.new(root) if root.is_a? String
      Dir.glob("#{root}/*/") do |directory|
        new_root = root.join(directory)
        yield(new_root)
        for_each_directory(new_root, &block)
      end
    end
  end
end
