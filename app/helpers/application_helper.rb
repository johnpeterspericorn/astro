# encoding: utf-8
module ApplicationHelper
  def button_bar(options = {}, &block)
    content_tag :div, class: :checkout_bar do
      content_tag :div, class: 'container cf' do
        content = ''
        content << render_locales
        content << back_button(options[:back] || {})
        content << home_button(options[:home] || {})
        content << capture(&block) if block_given?
        content.html_safe
      end
    end
  end

  def current_locale
    I18n.locale
  end

  def default_currency_options(country_name, options)
    country = countrify(country_name)
    precision = options.fetch(:precision, 0)
    separator = options.fetch(:separator, country.currency['separator'])
    delimiter = options.fetch(:delimiter, country.currency['delimiter'])
    {
      unit: country.currency['symbol'],
      precision: precision,
      separator: separator,
      delimiter: delimiter,
      format: '%u %n'
    }
  end

  def currency(amount, country_name, options = {})
    number_to_currency amount, default_currency_options(country_name, options)
  end

  def render_locales
    render partial: 'layouts/locales'
  end

  def countrify(country)
    Country.find_country_by_name(country.present? ? country : 'United States of America')
  end

  private

  def back_button(options = {})
    return '' if options[:skip]
    label = options[:label] || t('forms.back')
    path  = options[:path]  || :back
    link_to label, path, class: 'btn cancel_btn'
  end

  def home_button(options = {})
    return '' if options[:skip]
    specific_path = (@_controller.controller_path =~ %r{^admin/} ? admin_index_path : root_path)
    link_path = options[:path].present? ? options[:path] : specific_path
    label = options[:label] || t('forms.home')
    link_to label, link_path, class: 'btn green'
  end
end
