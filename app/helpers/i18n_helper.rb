# encoding: utf-8
module I18nHelper
  def i18n_set?(key)
    I18n.t key, raise: true
  rescue I18n::MissingTranslationData
    false
  end
end
