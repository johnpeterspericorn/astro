# encoding: utf-8
module Admin
  module PromisePurchaseVersionsHelper
    def promise_purchase_version_heading(version)
      "<i>#{version.version_author}</i> #{t('admin.promise_purchase_versions.index.update_promise_purchase', item: version.item_type)}".html_safe
    end

    def promise_purchase_create_version_heading(version)
      "<i>#{version.version_author}</i> #{t('admin.promise_purchase_versions.index.create_promise_purchase', item: version.item_type)}".html_safe
    end
  end
end
