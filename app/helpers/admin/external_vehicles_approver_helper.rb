# encoding: utf-8
module Admin
  module ExternalVehiclesApproverHelper
    def pawn_locations
      PawnLocation.pluck(:initials).sort
    end

    def preselected_promise_kinds
      PromisePurchase::KINDS
    end

    def preselected_promise_days(vehicle = nil)
      vehicle&.preselected_promise_kind == 'purchase_protect' ? PromiseOffer::DAYS : %w(21)
    end

    def approve_label(vehicle = nil)
      %w(approved posted unposted).include?(vehicle.status) ? 'Approved' : 'Approve'
    end

    def reject_label(vehicle = nil)
      vehicle.status == 'rejected' ? 'Rejected' : 'Reject'
    end
  end
end
