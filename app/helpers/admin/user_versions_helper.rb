# encoding: utf-8
module Admin
  module UserVersionsHelper
    def version_heading(version)
      "<i>#{version.version_author}</i> #{t('admin.user_versions.index.update_user')} <i>#{version.version_user}</i>".html_safe
    end

    def create_version_heading(version)
      "<i>#{version.version_author}</i> #{t('admin.user_versions.index.create_user')} <i>#{version.version_user}</i>".html_safe
    end

    def change_set(change)
      "set <b><i>#{change[0]}</i></b> from <b>#{change[1][0]}</b> to <b>#{change[1][1]}</b>".html_safe
    end

    def time_stamp(version)
      "#{time_ago_in_words(version.created_at)} ago"
    end
  end
end
