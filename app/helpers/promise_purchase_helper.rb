# encoding: utf-8
module PromisePurchaseHelper
  def checkout_back_button(text, pawn_access_number)
    path = if pawn_access_number.present?
             promise_offers_path(pawn_access_number: pawn_access_number)
           else
             vehicle_filter_path
           end
    link_to text, path, class: 'btn cancel_btn'
  end
end
