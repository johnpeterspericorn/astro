# encoding: utf-8
module AdminHelper
  def last_deploy_time
    revision_file = Rails.root.join('REVISION')
    deploy_time = if File.exist?(revision_file)
                    File.mtime(revision_file)
                  else
                    'unknown'
                  end
    "Application last deployed at: #{deploy_time}"
  end
end
