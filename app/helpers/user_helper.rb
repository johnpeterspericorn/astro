# encoding: utf-8
module UserHelper
  def user_roles
    {
      User::API_USER => {},
      User::FLAT_RATE_INQUIRY_VIEWER => {},
      User::DEALER_TRANSACTION_VIEWER => {},
      User::DEALER_SCORE_VIEWER => {},
      User::BUYER_USER => {},
      User::INVOICEABLE => {},
      User::KIOSK_USER => {},
      User::SUPER_USER => {
        User::ADMIN => {
          User::FEATURE_FLAG_EDITOR => {},
          User::GUARANTEE_ADMIN_VIEWER => {},
          User::GUARANTEE_ADMIN_EDITOR => {},
          User::USER_ADMIN_EDITOR => {},
          User::LOSS_PREVENTION_VIEWER => {},
          User::FINANCE_ADMIN_VIEWER => {},
          User::FLAT_RATE_EDITOR => {}
        },
        User::VEHICLE_CREATOR => {},
        User::EXTERNAL_VEHICLE_ENTERER => {},
        User::EXTERNAL_VEHICLE_APPROVER => {}
      }
    }
  end
end
