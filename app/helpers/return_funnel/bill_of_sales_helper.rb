# encoding: utf-8
module ReturnFunnel
  module BillOfSalesHelper
    def full_address(purchase)
      suite = purchase.seller_info.address_suite.present? ? purchase.seller_info.address_suite : ''
      street = purchase.seller_info.address_street.present? ? purchase.seller_info.address_street : ''
      suite + street
    end
  end
end
