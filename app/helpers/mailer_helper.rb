# encoding: utf-8
module MailerHelper
  def track(code, email)
    EmailTracking.create!(unique_code: code, email: email, letter_sent: DateUtils.now)
    parameters = "unique_code=#{code}&email=#{email}"
    encoded_params = Base64.encode64(parameters).strip
    url = "#{root_url}email/track/#{encoded_params}.png"
    raw("\<img src=\"#{url}\" \>")
  end
end
