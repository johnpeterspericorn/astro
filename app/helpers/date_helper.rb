# encoding: utf-8
module DateHelper
  def offset_from_now(offset)
    Time.current + offset.days
  end
end
