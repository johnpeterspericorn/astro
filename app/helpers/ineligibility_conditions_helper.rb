# encoding: utf-8
module IneligibilityConditionsHelper
  def select_ineligibility_condition(form, object, options)
    ineligibility_conditions = IneligibilityCondition.all.map { |condition| [condition.notice, condition.condition_id] }

    form.select(object, ineligibility_conditions, options)
  end
end
