# encoding: utf-8
class AccountsReceivablePromiseOfferQuery
  def initialize(relation = PromiseOffer.all)
    @relation = relation
  end

  def will_appear_on_accounts_receivable_report_at(time)
    sql = <<-EOF
      promise_status = ? OR
      (paid_by_floor_plan = ? AND promise_status = ?) OR
      (left_lot = ? AND approved_at IS NOT NULL AND promise_status = ?)
    EOF

    within_ar_time_limit(time).where(
      sql,
      'charge_failed',
      true, 'promised',
      true, 'promised'
    )
  end

  private

  attr_reader :relation

  def within_ar_time_limit(time)
    relation.where('created_at > ?', time - AccountsReceivableMailer::INTERVAL)
  end
end
