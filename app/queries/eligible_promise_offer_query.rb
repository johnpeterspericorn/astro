# encoding: utf-8
class EligiblePromiseOfferQuery
  def initialize(relation = PromiseOffer.all)
    @relation = relation
  end

  def by_user(user)
    if user && user.super_user?
      without_ineligibility_condition(
        eligible_ods_attributes(relation)
      )
    else
      eligible_for_purchase(relation)
    end
  end

  private

  attr_reader :relation

  def without_ineligibility_condition(relation)
    relation.where('promise_offers.ineligibility_condition = 0 OR promise_offers.ineligibility_condition IS NULL')
  end

  def eligible_for_purchase(relation)
    without_ineligibility_condition(
      eligible_ods_attributes(
        relation.not_past_cutoff
      )
    )
  end

  def eligible_ods_attributes(relation)
    relation.left_lot_or_paid_by_floor_plan(false, true)
  end
end
