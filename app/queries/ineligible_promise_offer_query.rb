# encoding: utf-8
class IneligiblePromiseOfferQuery
  def initialize(relation = PromiseOffer.all)
    @relation = relation
  end

  def by_user(user)
    if user && user.super_user?
      relation.where(ineligibility_sql(left_lot: true))
    else
      ineligible_for_purchase(relation)
    end
  end

  private

  attr_reader :relation

  def ineligibility_sql(options = {})
    floor_plan_condition = <<-SQL
      AND
      promise_offers.paid_by_floor_plan = false
    SQL

    <<-SQL
      promise_offers.ineligibility_condition != 0
      OR
      (
        promise_offers.left_lot = #{options[:left_lot]}
        #{floor_plan_condition}
      )
    SQL
  end

  def ineligible_for_purchase(relation)
    relation.where <<-SQL, DateTime.current
      #{ineligibility_sql(left_lot: true)}
      OR
      promise_offers.must_be_promised_at < ?
    SQL
  end
end
