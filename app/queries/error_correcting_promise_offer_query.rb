# encoding: utf-8
class ErrorCorrectingPromiseOfferQuery
  def initialize(relation = PromiseOffer.all)
    @relation = relation
  end

  def by_filter_query_data(filter_query_data)
    case filter_query_data.filter_method
    when :pawn_access_number
      error_correcting_by_pawn_access_number(filter_query_data)
    when :badge_number
      error_correcting_by_badge_number(filter_query_data.badge_number)
    when :agreement_number
      error_correcting_by_agreement_number(filter_query_data.agreement_number)
    end
  end

  private

  attr_reader :relation

  def error_correcting_by_badge_number(badge_number)
    offers = relation
             .includes(:sold_vehicle, :pawn_information)
             .where(pawn_information: { badge_no:  badge_number })
             .where("sold_vehicles.badge_no = ? or sold_vehicles.badge_no = '0'", badge_number)
             .references(:sold_vehicles)
             .select(&:location_purchases_enabled?)

    relation.where(id: offers)
  end

  def error_correcting_by_pawn_access_number(filter_query_data)
    with_vehicle_for_current_buyer(filter_query_data)
      .includes(:pawn_information)
      .for_pawn_access_number(filter_query_data)
  end

  def error_correcting_by_agreement_number(agreement_number)
    relation
      .includes(:pawn_information, :sold_vehicle)
      .where(pawn_information: { agreement_no: agreement_number })
      .where("sold_vehicles.agreement_no = ? or sold_vehicles.agreement_no = '0'", agreement_number)
      .references(:sold_vehicles)
  end

  def with_vehicle_for_current_buyer(filter_query_data)
    relation
      .joins(:sold_vehicle)
      .merge(SoldVehicle.for_pawn_access_number(filter_query_data))
      .readonly(false)
  end
end
