# encoding: utf-8
class FloorPlanCompanyDecorator < ApplicationDecorator
  delegate_all

  def admin_link
    h.link_to name, h.edit_admin_floor_plan_company_path(source)
  end

  def branches_link
    h.link_to 'Branches', h.admin_floor_plan_company_floor_plan_branches_path(source)
  end
end
