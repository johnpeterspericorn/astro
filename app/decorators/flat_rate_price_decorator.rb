# encoding: utf-8
class FlatRatePriceDecorator < ApplicationDecorator
  delegate_all
  attr_reader :flat_rate_attribute

  def initialize(flat_rate, flat_rate_miles, psi_eligible, palv = false)
    super(flat_rate)
    @flat_rate_attribute = FlatRateAttribute.new(flat_rate: flat_rate, miles: flat_rate_miles, psi: psi_eligible, palv: palv)
  end

  def low_price
    currency(flat_rate_attribute.low_price)
  end

  def high_price
    currency(flat_rate_attribute.high_price)
  end

  def target_price
    currency(flat_rate_attribute.target_price)
  end

  def price_available?
    flat_rate_attribute.available?
  end

  def ineligibility_reason
    I18n.t('flat_rates.ineligibility_conditions')[ineligibility_condition]
  end
end
