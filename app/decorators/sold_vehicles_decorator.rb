# encoding: utf-8
class SoldVehiclesDecorator < PaginatingDecorator
  def fields
    {
      'Make'                   => :make,
      'Model'                  => :vehicle_model,
      'VNUM'                    => :vnum,
      'Agreement #'               => :agreement_no,
      'AA #'                   => :pawn_access_no,
      'Location'               => :location_initials,
      'Eligible?'              => :eligible?,
      'Available for Purchase' => :available_for_purchase?,
      'Updated'                => :updated_at,
      'Edit'                   => :edit_sold_vehicle_link
    }
  end
end
