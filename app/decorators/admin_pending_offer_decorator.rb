# encoding: utf-8
class AdminPendingOfferDecorator < PromiseOfferDecorator
  delegate_all

  def pending_reason_notice
    pending_reason.try(:notice)
  end

  def admin_pending_link
    h.link_to('Details', h.admin_pending_offer_path(source))
  end
end
