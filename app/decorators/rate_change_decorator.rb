# encoding: utf-8
class RateChangeDecorator < ApplicationDecorator
  delegate_all

  def return_rate
    currency(source.return_rate)
  end
end
