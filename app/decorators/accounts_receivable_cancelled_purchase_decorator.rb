# encoding: utf-8
class AccountsReceivableCancelledPurchaseDecorator < AccountsReceivableDecorator
  decorates :cancelled_purchase
  delegate_all

  def ar_amount
    -source.promise_price if source.promise_price.present?
  end

  def days_selected
    promise_days_selected
  end

  def miles_selected
    promise_miles_selected
  end
end
