# encoding: utf-8
class ExternalVehiclesLoadDecorator < ApplicationDecorator
  delegate_all

  def model
    source.model.to_s
  end

  def buy_fee
    format_price(source.buy_fee)
  end

  def vehicle_purchase_price
    format_price(source.vehicle_purchase_price)
  end

  def formatted_purchased_at
    source.purchased_at.to_formatted_s(:dashed_with_tz)
  end
end
