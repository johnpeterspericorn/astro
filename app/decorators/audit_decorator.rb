# encoding: utf-8
class AuditDecorator < ApplicationDecorator
  delegate_all

  def self.reason_options(selected)
    h.options_for_select(
      all_reasons.map { |k, v| [k, v] },
      all_reasons[selected]
    )
  end

  def self.all_reasons
    I18n.t('return_invoice.audit.reasons').invert
  end
end
