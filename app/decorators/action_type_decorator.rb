# encoding: utf-8
class ActionTypeDecorator < ApplicationDecorator
  delegate_all

  def email_sent_date
    date_sent.getlocal if date_sent?
  end

  def email_viewed_date
    email_viewed_at.getlocal if email_viewed_at?
  end
end
