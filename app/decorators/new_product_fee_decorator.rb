# encoding: utf-8
class NewProductFeeDecorator < ApplicationDecorator
  delegate_all

  def fee_in_currency
    currency(model.fee)
  end

  def country
    SupportedCountry::DEFAULT
  end
end
