# encoding: utf-8
class PawnAccessNumberBlacklistDecorator < ApplicationDecorator
  delegate_all

  def status
    expired? ? I18n.t('pawn_access_no_blacklist.status.inactive') : I18n.t('pawn_access_no_blacklist.status.active')
  end

  def delete_pawn_access_number_blacklist_link
    h.link_to(
      I18n.t('forms.delete'),
      h.admin_pawn_access_number_blacklist_path(source.id),
      rel: :delete_pawn_access_number_blacklist,
      method: :delete, data: { confirm: 'Are you sure?' }
    )
  end

  def edit_pawn_access_number_blacklist_link
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_pawn_access_number_blacklist_path(source.id),
      rel: :edit_pawn_access_number_blacklist
    )
  end
end
