# encoding: utf-8
class SoldVehicleDecorator < ApplicationDecorator
  include VehicleMixins::ViewConcerns # defines #full_description, #eligiblility_reason, #promise_price_threshold, #vehicle_code, #default_currency_options, #location_name
  delegate_all

  def vehicle_purchase_price
    currency(source.vehicle_purchase_price)
  end

  def odometer_reading
    h.number_with_delimiter(source.odometer_reading)
  end

  def vehicle_model
    source.model
  end

  def fields
    {
      'Make'                   => :make,
      'Model'                  => :vehicle_model,
      'VNUM'                    => :vnum,
      'Agreement #'               => :agreement_no,
      'AA #'                   => :pawn_access_no,
      'Location'               => :location_initials,
      'Eligible?'              => :eligible?,
      'Available for Purchase' => :available_for_purchase?,
      'Updated'                => :updated_at,
      'Processed'              => :automatic_purchase_processed,
      'Ineligibility'          => :ineligibility_condition
    }
  end

  def edit_sold_vehicle_link
    h.link_to(
      'Edit Sold Vehicle',
      h.edit_admin_sold_vehicle_path(source),
      rel: :edit_sold_vehicle
    )
  end
end
