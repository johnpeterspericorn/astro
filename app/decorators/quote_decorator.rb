# encoding: utf-8
class QuoteDecorator < ApplicationDecorator
  delegate_all

  def price(miles, days)
    currency(source.price(miles, days))
  end
end
