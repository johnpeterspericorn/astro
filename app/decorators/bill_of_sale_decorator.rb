# encoding: utf-8
class BillOfSaleDecorator < ApplicationDecorator
  delegate_all

  delegate :agreement_no, to: :promise_purchase

  def agreementship_name
    promise_purchase.seller_info.present? ? promise_purchase.seller_info.agreementship_name : ''
  end

  def full_address
    if promise_purchase.seller_info.present?
      suite = promise_purchase.seller_info.address_suite.present? ? promise_purchase.seller_info.address_suite : ''
      street = promise_purchase.seller_info.address_street.present? ? promise_purchase.seller_info.address_street : ''
      suite + street
    else
      ''
    end
  end

  def address_city
    promise_purchase.seller_info.present? ? promise_purchase.seller_info.address_city : ''
  end

  def address_zipcode
    promise_purchase.seller_info.present? ? promise_purchase.seller_info.address_zipcode : ''
  end

  delegate :vehicle_purchase_price, to: :promise_purchase

  delegate :odometer_reading, to: :promise_purchase

  def year
    promise_purchase.year.to_i
  end

  delegate :make, to: :promise_purchase

  delegate :model, to: :promise_purchase

  delegate :vnum, to: :promise_purchase
end
