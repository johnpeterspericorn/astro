# encoding: utf-8
class BundledPurchaseDecorator < ApplicationDecorator
  delegate_all

  delegate :expiration_date, :vnum, to: :promise_purchase

  def price
    promise_purchase.decorate.bundled_inspection_offer_price
  end

  def distance_selected_with_units
    promise_purchase.decorate.distance_selected_with_units
  end
end
