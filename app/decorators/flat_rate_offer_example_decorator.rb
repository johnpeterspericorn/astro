# encoding: utf-8
class FlatRateOfferExampleDecorator < ApplicationDecorator
  delegate_all

  def offer_price
    currency(source.offer_price)
  end

  def vehicle_amount_eligible_for_surcharge
    currency(vehicle_amount_in_excess_of_base)
  end

  def surcharge
    currency(source.surcharge)
  end

  def surcharge_rate
    h.number_to_percentage(FlatRateOfferExample::SURCHARGE_RATE * 100)
  end

  def psi_rate
    currency(FlatRateOfferExample::PSI_7_DAY_RATE)
  end

  def target_price_for_miles
    currency(source.target_price_for_miles)
  end

  def target_price
    currency(source.target_price)
  end

  def vehicle_base_price
    currency(FlatRateOfferExample::VEHICLE_BASE_PRICE)
  end

  def vehicle_price
    currency(source.vehicle_price)
  end

  def program_rate
    currency(source.program_rate)
  end

  def program_rate_with_psi
    currency(source.program_rate_with_psi)
  end

  def max_model_year
    (Time.zone.today - 19.years).year
  end
end
