# encoding: utf-8
class FlatRateOfferDecorator < ApplicationDecorator
  delegate_all

  GUARANTEE_DISTANCE1_US = 250
  GUARANTEE_DISTANCE1_CA = 600

  def price
    currency(source.price)
  end

  def target_price
    currency(source.target_price)
  end

  def target_price_for_miles
    currency(source.target_price_for_miles)
  end

  def product_selected
    [distance_selected_with_units, psi_text].join(' ')
  end

  def psi_text
    '& PSI' if psi_eligible?
  end

  def distance_selected_with_units
    selected_miles = if country_code == 'CA'
                       (source.miles_selected == GUARANTEE_DISTANCE1_US) ? GUARANTEE_DISTANCE1_CA : selected_miles
                     else
                       source.miles_selected
                     end
    distance(selected_miles, country_code) if source.miles_selected
  end

  def country_code
    Country.find_country_by_name(country).try(:alpha2)
  end

  def country
    source.country || SupportedCountry::DEFAULT
  end
end
