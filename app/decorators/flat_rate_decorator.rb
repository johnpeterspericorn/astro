# encoding: utf-8
class FlatRateDecorator < ApplicationDecorator
  delegate_all

  def target_price_250
    currency(source.target_price_250)
  end

  def target_price_250_psi
    currency(source.target_price_250_psi)
  end

  def target_price_360
    currency(source.target_price_360)
  end

  def target_price_360_psi
    currency(source.target_price_360_psi)
  end

  def target_price_500
    currency(source.target_price_500)
  end

  def target_price_500_psi
    currency(source.target_price_500_psi)
  end

  def target_price_lv
    currency(source.target_price_lv)
  end

  def edit_flat_rate_link
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_flat_rate_path(source.id),
      rel: :edit_flat_rate_link
    )
  end

  def all_ineligibility_conditions
    I18n.t('flat_rates.ineligibility_conditions')
  end

  def ineligibility_options
    h.options_for_select(
      all_ineligibility_conditions.map { |id, notice| [notice, id] },
      source.ineligibility_condition
    )
  end
end
