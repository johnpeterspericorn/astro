# encoding: utf-8
class PawnLocationDecorator < ApplicationDecorator
  delegate_all

  def self.selector(default_location = nil)
    h.select_tag :location_initials, select_options(default_location), class: 'form-l-select-btn-height', prompt: 'Select a location'
  end

  def self.select_options(default)
    default ||= Astro::DEFAULT_AUCTION_LOCATION
    pawn_locations = PawnLocation.with_purchases_enabled.order(:name)

    h.options_from_collection_for_select(
      pawn_locations,
      :initials,
      :name,
      default
    )
  end
end
