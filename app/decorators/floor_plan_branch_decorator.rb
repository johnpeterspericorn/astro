# encoding: utf-8
class FloorPlanBranchDecorator < ApplicationDecorator
  delegate :name, to: :country, prefix: true
  delegate_all

  def admin_link
    h.link_to name, h.edit_admin_floor_plan_company_floor_plan_branch_path(floor_plan_company, source)
  end

  private

  def country
    @country ||= Country.new(country_code)
  end
end
