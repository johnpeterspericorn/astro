# encoding: utf-8
class AccountsReceivableOfferDecorator < AccountsReceivableDecorator
  decorates :promise_offer
  delegate_all

  def ar_amount
    source.promise_price
  end
end
