# encoding: utf-8
class ReturnCertificateDecorator < ApplicationDecorator
  delegate_all

  def self.title_status_selector
    h.select_tag :title_status, select_options, prompt: 'Please Select'
  end

  def self.select_options
    h.options_for_select(
      I18n.t('return_invoice.title_statuses').map { |k, v| [v, k] }
    )
  end
end
