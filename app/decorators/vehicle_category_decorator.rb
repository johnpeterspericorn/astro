# encoding: utf-8
class VehicleCategoryDecorator < ApplicationDecorator
  delegate_all

  def low_price
    currency source.pricing_distribution.low_price
  end

  def median_price
    currency source.pricing_distribution.median_price
  end

  def high_price
    currency source.pricing_distribution.high_price
  end
end
