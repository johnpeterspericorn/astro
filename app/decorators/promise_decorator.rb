# encoding: utf-8
class PromiseDecorator < ApplicationDecorator
  delegate_all

  def flat_rate_price
    currency(promise_offer.flat_rate_price) if promise_offer.has_flat_rate_offer?
  end

  def price(distance, additional_days)
    price = model.price(distance, additional_days)
    currency(price)
  end

  def discounted_price(distance, additional_days)
    price = model.discounted_price(distance, additional_days)
    currency(price)
  end

  def price_label(distance, additional_days)
    "#{distance}_#{additional_days}"
  end
end
