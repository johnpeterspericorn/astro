# encoding: utf-8
class NetworkPlusOfferDecorator < ApplicationDecorator
  delegate_all

  def status_message
    prompt, style = if approved?
                      %w(accepted_care_prompt success)
                    elsif confirmed?
                      %w(unapproved_care_prompt error)
                    else rejected?
                      %w(rejected_care_prompt error)
                    end
    h.content_tag(:div, h.content_tag(:p, h.raw(I18n.t("admin.network_plus_offer.approvals.show.#{prompt}", email: Astro::AUTOMATIC_EMAIL).to_s)), class: ['offer-notice', style])
  end
end
