# encoding: utf-8
class ReturnInvoiceDecorator < ApplicationDecorator
  delegate_all

  def location_options
    pawn_locations.where(country: pawn_location.country).map { |l| [l.name, l.id] }
  end

  def title_status_name
    I18n.t('return_invoice.title_statuses')[source.title_status]
  end

  def return_location_initials
    source.pawn_location.initials
  end

  def estimated_correction_price
    currency(source.estimated_cost)
  end

  def returning_user_name
    source.returning_user.try(:username)
  end

  def cancel_return_link(options = {})
    if purchase_cancelled?
      I18n.t('admin.returns.purchase_cancelled')
    elsif vehicle_received?
      I18n.t('admin.returns.vehicle_received')
    else
      options[:rel]    ||= :cancel_return
      options[:data]   ||= { confirm: 'Are you sure?' }

      label = options.delete(:label) { I18n.t('return_invoice.cancellation.link_copy') }
      url   = h.new_return_invoice_return_cancellation_path(source)
      h.link_to(label, url, options)
    end
  end

  def edit_return_link(_options = {})
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_return_invoice_path(source),
      rel: :edit_return
    ) unless purchase_cancelled?
  end

  def edit_purchase_link(_options = {})
    if purchase_cancelled?
      I18n.t('admin.returns.purchase_cancelled')
    else
      h.link_to(
        'Edit Purchase',
        h.edit_admin_promise_purchase_path(source.promise_purchase),
        rel: :edit_purchase
      )
    end
  end

  def edit_audit_link
    h.link_to(
      'Edit Audit',
      h.edit_admin_return_invoice_audit_path(source),
      rel: :edit_audit
    )
  end

  def vra
    source.id
  end

  def vra_link(vra_listing_page = false, pawn_access_number = source.pawn_access_no)
    label_text = vra_listing_page ? h.t('return_invoice.link_copy') : vra
    options = vra_listing_page ? { class: 'invoice_link btn' } : {}
    if source.canadian_return?
      source.slug.present? ? (h.link_to label_text, h.obfuscated_return_certificate_path(slug: source.slug, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(vra), pawn_access_number: pawn_access_number), options) : (h.link_to label_text, h.return_certificate_path(return_invoice_id: vra, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(vra), pawn_access_number: pawn_access_number), options)
    else
      source.slug.present? ? (h.link_to label_text, h.obfuscated_return_certificate_path(slug: source.slug, pawn_access_number: pawn_access_number), options) : (h.link_to label_text, h.return_certificate_path(return_invoice_id: vra, pawn_access_number: pawn_access_number), options)
    end
  end

  def return_initiated_at
    source.created_at.to_date
  end

  def vehicle_price
    currency(source.vehicle_purchase_price)
  end

  def return_fee
    currency(source.new_product_fee)
  end

  def shipment_label_link
    h.link_to(
      'Shipping Label',
      source.shipment.label_url,
      rel: :shipment_label_url
    ) if source.shipment.present?
  end

  def edit_promise_offer_link
    h.link_to(
      'Edit Promise Offer',
      h.edit_admin_promise_offer_path(source.promise_offer),
      rel: :edit_promise_offer
    ) if promise_offer
  end

  def clear_audit_link
    h.link_to(
      'Clear Audit',
      h.clear_admin_return_invoice_audit_path(source),
      method: :post,
      rel: :clear_admin_return_invoice_audit
    ) if source.audit_opened?
  end

  def flag_audit_link
    h.link_to(
      'Flag Audit',
      h.new_admin_return_invoice_audit_flag_path(source),
      rel: :flag_admin_return_invoice_audit
    ) if source.audit_opened?
  end

  def reinstate_return_link
    h.link_to(
      'Reinstate Return',
      h.return_invoice_return_cancellation_path(source),
      method: :delete,
      rel: :reinstate_return
    ) if promise_offer
  end

  def cancelling_username
    cancelling_user.try(:username)
  end

  def last_update_by
    User.find_by_id(versions.last.try(:version_author)).try(:username) || 'Unknown'
  end

  def odometer_reading_with_units
    distance(source.odometer_reading, country_alpha2_code) if source.odometer_reading
  end

  def distance_units
    I18n.translate("distance_units.#{country_alpha2_code}.unit")
  end

  private

  def country_object
    Country.find_country_by_name(country || SupportedCountry::DEFAULT)
  end

  def country_alpha2_code
    country_object.try(:alpha2)
  end

  def pawn_locations
    PawnLocation.accepts_returns.order(:name)
  end
end
