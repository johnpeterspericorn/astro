# encoding: utf-8
class FlatRateOfferBatchDecorator < ApplicationDecorator
  decorates_association :flat_rate_offers
  delegate_all

  def offering_users_emails
    offering_users.map(&:email)
  end

  def status_message
    prompt, style = if approved?
                      %w(accepted_care_prompt success)
                    elsif rejected?
                      %w(rejected_care_prompt error)
                    else
                      confirmed? ? %w(confirmed_care_prompt error) : %w(unconfirmed_care_prompt error)
                    end
    h.content_tag(:div, h.content_tag(:p, h.raw(I18n.t("flat_rate_offer_batches.approvals.show.#{prompt}", email: Astro::AUTOMATIC_EMAIL).to_s)), class: ['offer-notice', style])
  end
end
