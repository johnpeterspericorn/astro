# encoding: utf-8
class PendingOfferDecorator < Draper::Decorator
  decorates :promise_offer
  delegate_all

  def current_status
    case
    when !promised?
      'Invalidated'
    when promise_purchase.try(:return_initiated?)
      'Allowed - Returned'
    when promise_purchase.try(:after_max_return_time?)
      'Allowed - Expired'
    else
      'Allowed - Active'
    end
  end

  NullUser = Naught.build { |c| c.mimic(User) }

  def changing_user
    @changing_user ||= source.changing_user || NullUser.new
  end

  def allowed_username
    changing_user.username if promised?
  end

  def invalidated_username
    changing_user.username unless promised?
  end

  def returned_at
    promise_purchase.return_invoice.created_at if promise_purchase.try(:return_initiated?)
  end

  def vehicle_purchased_at
    convert_datetime(purchased_at)
  end

  def buyer_agreement_number
    agreement_no
  end

  def seller_agreement_number
    seller_no
  end

  def ineligible
    convert_boolean(ineligible?)
  end

  def if_bid
    convert_boolean(if_bid?)
  end

  def repurchased
    convert_boolean(false)
  end

  def pending_reason
    promise_offer.pending_reason.try(:notice)
  end

  private

  def convert_boolean(bool)
    bool ? 1 : 0
  end

  def convert_datetime(datetime)
    datetime.to_date.to_s(:db) if datetime.present?
  end
end
