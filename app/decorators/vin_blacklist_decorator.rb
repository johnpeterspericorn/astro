# encoding: utf-8
class VinBlacklistDecorator < ApplicationDecorator
  delegate_all

  def edit_vnum_blacklist_link
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_vnum_blacklist_path(source.id),
      rel: :edit_vnum_blacklist
    )
  end

  def delete_vnum_blacklist_link
    h.link_to(
      I18n.t('forms.delete'),
      h.admin_vnum_blacklist_path(source.id),
      rel: :delete_vnum_blacklist,
      method: :delete, data: { confirm: 'Are you sure?' }
    )
  end

  def status
    expired? ? I18n.t('vnum.status.inactive') : I18n.t('vnum.status.active')
  end
end
