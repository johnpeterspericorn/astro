# encoding: utf-8
class AccountsReceivableDecorator < Draper::Decorator
  delegate_all

  def pawn_location
    source.location_initials
  end

  def agreementship_name
    AgreementInformation.build_with_defaults(agreement_no, pawn_access_no).agreementship_name
  end
end
