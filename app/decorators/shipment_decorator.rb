# encoding: utf-8
class ShipmentDecorator < ApplicationDecorator
  delegate_all
  DEFAULT_CARRIER = 'ups'.freeze

  def tracking_link(label = I18n.t('return_funnel.filtered_purchases.forms.track_shipment'))
    url = tracking_urls[carrier.try(:to_sym)]
    h.link_to label, url, class: 'btn' if url.present?
  end

  private

  def tracking_urls
    {
      ups:   "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=#{tracking_code}",
      fedex: "https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=#{tracking_code}&cntry_code=us"
    }
  end

  def carrier
    source.carrier.present? ? source.carrier.try(:downcase) : DEFAULT_CARRIER
  end
end
