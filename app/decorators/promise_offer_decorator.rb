# encoding: utf-8
class PromiseOfferDecorator < ApplicationDecorator
  include VehicleMixins::ViewConcerns # defines #full_description, #eligiblility_reason, #promise_price_threshold, #vehicle_code, #default_currency_options, #location_name
  alias vehicle_full_description full_description

  delegate :start_date, :return_eligible_date, :expiration_date, to: :promise_purchase

  decorates_association :promise
  decorates_association :inspection_purchase
  decorates_association :promise_purchase
  delegate_all

  NO_COVERAGE_PRICE = 0

  def offer_price(miles, additional_days)
    currency(source.promise.price(miles, additional_days))
  end

  def bundled_inspection_offer_price(miles, additional_days)
    currency(source.promise.price(miles, additional_days) + inspection_information.inspection_bundle_price)
  end

  def purchase_price
    currency(model.vehicle_purchase_price || 0.0)
  end

  def promised_at?(distance, days)
    (miles_selected == distance) && (days_selected == days + Promise::BASE_DURATION)
  end

  def not_yet_promised?
    miles_selected.nil? && days_selected.nil?
  end

  def distance_selected_with_units
    distance(source.miles_selected, country_code) if source.miles_selected
  end

  def distance_units
    I18n.translate("distance_units.#{country_code}.unit")
  end

  def inspection_only_price
    currency(source.inspection_only_price)
  end

  def inspection_bundle_price
    currency(source.inspection_bundle_price)
  end

  def emails
    model.emails.present? ? model.emails : I18n.t('promise_offers.missing_email')
  end

  def promise_price
    currency(model.promise_price)
  end

  def vehicle_purchase_price
    purchase_price
  end

  def no_coverage_price
    currency(NO_COVERAGE_PRICE)
  end

  def odometer_reading
    distance(source.odometer_reading, country_code) if source.odometer_reading
  end

  def country_code
    Country.find_country_by_name(country).try(:alpha2)
  end

  def country
    source.country || SupportedCountry::DEFAULT
  end

  def distance1_label
    I18n.t("offer_distances.#{country_code}.distance1").to_s + ' ' + I18n.t("distance_units.#{country_code}.unit")
  end

  def distance2_label
    I18n.t("offer_distances.#{country_code}.distance2").to_s + ' ' + I18n.t("distance_units.#{country_code}.unit")
  end

  def self.search_criteria(filter_query_data)
    case filter_query_data.filter_method
    when :badge_number
      I18n.t('promise_offers.greetings.badge_number', badge_number: filter_query_data.badge_number)
    when :pawn_access_number
      agreement_name = filter_query_data.buyer_authorization.get_agreement_name.titleize
      I18n.t('promise_offers.greetings.pawn_access_number', agreement_name: agreement_name)
    when :agreement_number
      I18n.t('promise_offers.greetings.agreement_number', agreement_number: filter_query_data.agreement_number)
    end
  end

  def pawn_code
    /^(?<pawn_code>[[:alpha:]]+)/ =~ model.universal_no
    pawn_code.upcase if pawn_code
  end

  def purchase_location
    /(?<initials>[a-z]+)/i =~ universal_no

    PawnLocation.find_by_initials(initials.upcase) if initials.present?
  end

  def selection_description
    "#{days_selected}D #{miles_selected}M"
  end

  def year_make_model(separator = ' ')
    [year, make, source.model].join(separator)
  end

  def has_transportation_coverage?
    source.transport_reimbursement.present? &&
      source.transport_reimbursement > 0
  end

  def vehicle_description
    [year_make_model,
     "#{odometer_reading} miles",
     "VNUM #{vnum}"
    ].join(', ')
  end

  def ods_update_link(options = {})
    options[:method] ||= :put
    options[:rel]    ||= :update_ods

    label = options.delete(:label) { 'Update ODS Attributes' }
    url   = h.admin_promise_offer_ods_update_path(source)

    h.link_to(label, url, options)
  end

  def edit_promise_link(options = {})
    options[:method] ||= :get
    options[:rel]    ||= :edit_admin_promise_link

    label = options.delete(:label) { 'Edit' }
    url   = promised? ? h.edit_admin_promise_offer_path(source) : h.edit_admin_unpurchased_offer_path(source)

    h.link_to(label, url, options)
  end

  def cancelled_purchases_link
    url = h.admin_cancelled_purchases_path(filters: { vnum: vnum })

    h.link_to('Cancelled Purchases', url)
  end

  def agreement_name
    agreement_information.agreementship_name
  end

  def transportation_coverage_value
    currency(source.transport_reimbursement)
  end

  def show_min_return_time?
    source.limited_quantity? || source.percent_coverage?
  end

  def value_protected
    currency(model.value_protected || 0.0)
  end

  private

  def agreement_information
    AgreementInformation.build_with_defaults(pawn_access_no, agreement_no)
  end
end
