# encoding: utf-8
class AdminPurchasedOfferDecorator < PromiseOfferDecorator
  delegate_all

  def edit_link
    h.link_to I18n.t('forms.promise_offers.edit'), h.edit_admin_promise_offer_path(source)
  end

  def promised_at
    source.promise_purchase.promised_at
  end

  def purchasing_username
    promise_purchase.purchasing_user.try(:username)
  end

  delegate :kind, to: :promise_purchase

  def purchase_partner
    promise_purchase.partner
  end

  def expire_promise_link
    if promise_purchase.expire_promise || promise_purchase.max_return_time.past?
      return ("#{I18n.t('promise_purchases.expired')} \n" + h.link_to(I18n.t('promise_purchases.unexpire').to_s, h.admin_promise_purchase_unexpire_promise_path(promise_purchase_id: promise_purchase.id), method: 'POST', data: { confirm: 'Are you sure?' }, rel: :expire_purchase)).html_safe unless promise_purchase.max_return_time.past?
      I18n.t('promise_purchases.expired')
    else
      h.link_to(I18n.t('promise_purchases.expire').to_s, h.admin_promise_purchase_expire_promise_path(promise_purchase_id: promise_purchase.id), method: 'POST', data: { confirm: 'Are you sure?' }, rel: :expire_purchase)
    end
  end
end
