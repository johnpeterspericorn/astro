# encoding: utf-8
class AgreementFloorPlanBranchLockDecorator < ApplicationDecorator
  delegate_all

  def agreement_floor_plan_branch_lock_edit_link
    h.link_to(
      I18n.t('admin.agreement_floor_plan_branch_locks.edit.link_copy'),
      h.edit_admin_agreement_floor_plan_branch_lock_path(source),
      rel: :edit_coupon
    )
  end
end
