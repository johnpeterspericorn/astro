# encoding: utf-8
class InspectionPurchaseDecorator < ApplicationDecorator
  delegate_all

  def promise_offer
    PromiseOfferFormPresenter.new(source.promise_offer.decorate)
  end

  def price
    currency(source.price)
  end
end
