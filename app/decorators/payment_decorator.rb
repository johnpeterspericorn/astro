# encoding: utf-8
class PaymentDecorator < ApplicationDecorator
  decorates_association :promise_purchase
  delegate_all

  delegate :agreement_no, :promise_price, :location_initials, :vnum, :universal_no, to: :purchase, allow_nil: true
  delegate :error_message, to: :payment_response, allow_nil: true

  delegate(
    :lane_no,
    :run_no,
    :days_selected,
    :country,
    :miles_selected,

    to:        :promise_offer,
    allow_nil: true
  )

  def item_description
    format_payment_field('item_description')
  end

  def sale_year
    universal_no[/\d{4}/] if universal_no.present?
  end

  def fee_amount
    format_payment_field('fee_amount')
  end

  def adjustment_code
    format_payment_field('adjustment_code')
  end

  def credit_card_number
    source.credit_card_number.to_s.rjust(4, '0') if source.credit_card_number.present?
  end

  def amount_charged
    currency(source.amount, precision: 2)
  end

  def send_email_receipt
    h.link_to I18n.t('payments.email.resend_label'), h.new_admin_payment_receipt_email_path(payment_id: source.id)
  end

  def promise_price
    currency(source.promise_price, precision: 2)
  end

  def return_fee
    currency(source.return_fee, precision: 2)
  end

  private

  def purchase
    promise_purchase || cancelled_purchase
  end

  def format_payment_field(field)
    values = payment_adjustment_fees.collect do |fee|
      value = fee.public_send(field)
      field == 'fee_amount' ? currency(value).to_s.delete(' ') : value
    end
    values.join(" \n")
  end
end
