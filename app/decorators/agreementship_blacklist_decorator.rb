# encoding: utf-8
class AgreementshipBlacklistDecorator < ApplicationDecorator
  delegate_all

  def edit_agreementship_blacklist_link
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_agreementship_blacklist_path(source.id),
      rel: :edit_agreementship_blacklist
    )
  end

  def delete_agreementship_blacklist_link
    h.link_to(
      I18n.t('forms.delete'),
      h.admin_agreementship_blacklist_path(source.id),
      rel: :delete_agreementship_blacklist,
      method: :delete, data: { confirm: 'Are you sure?' }
    )
  end

  def status
    expired? ? I18n.t('agreementship_blacklist.status.inactive') : I18n.t('agreementship_blacklist.status.active')
  end
end
