# encoding: utf-8
class CancelledPurchaseDecorator < ApplicationDecorator
  delegate_all

  def cancelling_username
    cancelling_user.try(:username)
  end
end
