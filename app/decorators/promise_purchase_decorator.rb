# encoding: utf-8
class PromisePurchaseDecorator < ApplicationDecorator
  delegate_all

  def promise_offer
    PromiseOfferFormPresenter.new(source.promise_offer.decorate, nil)
  end

  def price_in_cents(session = {})
    return_fee = session[:return_fee]
    fee_or_price = return_fee.present? ? Integer(return_fee) : promise_purchase.credit_card_payment_price
    Integer(fee_or_price * 100)
  end

  def price
    currency(model.price)
  end

  def credit_card_payment_price
    currency(model.credit_card_payment_price)
  end

  def duration
    model.days_selected
  end

  def return_ineligibility_reason
    case
    when !return_initiated? && exceeds_promo_returns?
      I18n.t('return_invoice.exceeds_allowed_promo_returns',
        return_count: sell_side_promo.return_count,
        purchase_count: sell_side_promo.purchase_count,
        promo_code: sell_side_promo.promo_code)
    when offer_pending?
      I18n.t('return_invoice.ineligible.offer_pending')
    when return_initiated?
      I18n.t('return_invoice.return_initiated')
    when after_max_return_time? || expire_promise
      I18n.t('return_invoice.ineligible.after_max_return_time')
    when before_min_return_time? && kind == PromisePurchase::MARKET_PROTECT
      I18n.t('return_invoice.ineligible.market_protect')
    when before_min_return_time?
      I18n.t('return_invoice.ineligible.before_min_return_time')
    end
  end

  def payment_links(current_user)
    links = []
    return links if Settings.buynet_during_return
    return_label = I18n.t('forms.return')
    if paid? || promise_offer.canadian_location? || paid_by_buyer? || (skip_ds_payment? && !current_user.invoiceable?)
      links << return_link_for_pplv_nonpplv(return_label)
    elsif (current_user.super_user? || current_user.buyer_user?) && current_user.invoiceable?
      links << pay_for_promise_link unless skip_ds_payment?
      links << return_link_for_pplv_nonpplv(return_label)
    else
      links << pay_for_promise_link unless skip_ds_payment?
    end
  end

  def buynet_link
    h.link_to I18n.t('forms.return'), h.promise_purchase_buynet_path(self), class: 'green btn', rel: :return_purchase
  end

  def cancellation_link(current_user)
    if eligible_to_cancel_purchase?(current_user)
      label = I18n.t('cancellation.cancel_link_copy')
      path = h.new_promise_purchase_cancellation_path(promise_purchase_id: self)
      h.link_to(label, path, rel: :cancel_purchase, data: { confirm: I18n.t('cancellation.confirmation') })
    elsif eligible_to_cancel_return?(current_user)
      label = I18n.t('return_invoice.cancellation.link_copy')
      path = h.new_return_invoice_return_cancellation_path(return_invoice_id: return_invoice)
      h.link_to(label, path, rel: :cancel_return)
    end
  end

  def initiate_return_link
    return if Settings.buynet_during_return
    args = { promise_purchase_id: self }
    rel = 'return_purchase'
    label = I18n.t('forms.return')
    if seller_paid
      path = h.new_return_funnel_verification_path(args)
      h.link_to(label, path, class: 'green btn', rel: rel)
    elsif paid? || promise_offer.canadian_location?
      return_label = I18n.t('forms.return')
      return_link_for_pplv_nonpplv(return_label)
    else
      path = h.new_return_funnel_payment_notification_path(args)
      h.link_to(label, path, class: 'green btn', rel: rel)
    end
  end

  def pay_for_promise_link
    pay_promise_label = I18n.t('forms.pay_for_promise')
    h.link_to(
      pay_promise_label,
      h.new_promise_purchase_payment_path(self, payment_type: 'promise'),
      class: 'green btn',
      rel: :pay_for_purchase
    )
  end

  def skip_ds_payment?
    promise_offer.adjustments_paid_by_floor_plan? && promise_offer.floor_plan_company && promise_offer.floor_plan_company.skip_ds_payment
  end

  def return_link_for_pplv_nonpplv(label)
    args = { promise_purchase_id: self }
    if pplv?
      rel = 'return_fee_return_purchase'
      path = h.new_return_funnel_return_fee_notification_path(args)
    else
      rel = 'return_purchase'
      path = h.new_return_funnel_verification_path(args)
    end
    h.link_to(label, path, class: 'green btn', rel: rel)
  end

  def start_date
    promised_at.to_s(:slashed)
  end

  def expiration_date
    max_return_time.to_s(:slashed)
  end

  def return_eligible_date
    min_return_time.to_s(:slashed) unless promised_at.nil?
  end

  def displayable_for_return?
    eligible_for_return? || expired_within_limit
  end

  def inspection_price
    if promise_offer.reload.has_inspection?
      promise_offer.inspection_purchase.price
    else
      'None'
    end
  end

  def bundled_inspection_offer_price
    currency(source.promise.price(miles_selected, additional_days) + promise_offer.inspection_information.inspection_bundle_price)
  end

  def return_funnel_priority
    # Enables sorting by eligibility, then purchase time

    [eligible_for_return? ? 0 : 1, created_at]
  end

  def return_under_audit?
    return_initiated? && return_invoice.audit && !return_invoice.audit.cleared?
  end

  def distance_selected_with_units
    distance(source.miles_selected, country_code) if source.miles_selected
  end

  def distance_units
    I18n.translate("distance_units.#{country_code}.unit")
  end

  def country_code
    Country.find_country_by_name(country).try(:alpha2)
  end

  def country
    source.country || SupportedCountry::DEFAULT
  end

  private

  def expired_within_limit
    7.days.ago < max_return_time
  end

  def eligible_to_cancel_return?(current_user)
    return_initiated? && ReturnInvoicePolicy.new(current_user, return_invoice).destroy?
  end

  def eligible_to_cancel_purchase?(current_user)
    CancellationPolicy.new(current_user, self).create?
  end
end
