# encoding: utf-8
class BuyerInformationDecorator < ApplicationDecorator
  delegate_all

  def edit_buyer_information_link
    h.link_to(
      I18n.t('forms.edit'),
      h.edit_admin_buyer_information_path(source.id),
      rel: :edit_buyer_information
    )
  end

  def delete_buyer_information_link
    h.link_to(
      I18n.t('forms.delete'),
      h.admin_buyer_information_path(source.id),
      rel: :delete_buyer_information,
      method: :delete, data: { confirm: 'Are you sure?' }
    )
  end
end
