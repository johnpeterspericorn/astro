# encoding: utf-8
class DashboardScoreDecorator < ApplicationDecorator
  delegate_all

  def avg_loss_with_precision
    round_and_trim(source.avg_loss)
  end

  def return_rate_with_precision
    round_and_trim(source.return_rate)
  end

  def volume_with_precision
    round_and_trim(source.volume)
  end

  def margin_with_precision
    round_and_trim(source.margin)
  end

  def earnings_with_precision
    round_and_trim(source.earnings)
  end

  def return_score_with_precision
    round_and_trim(source.return_score)
  end

  def margin_score_with_precision
    round_and_trim(source.margin_score)
  end

  def volume_score_with_precision
    round_and_trim(source.volume_score)
  end

  def earnings_score_with_precision
    round_and_trim(source.earnings_score)
  end

  def round_and_trim(score)
    format('%g', score.round(2))
  end

  def manage_link(options = {})
    return unless FlatRatePolicy.new(h.current_user).new?
    label = I18n.t('admin.navigation.dashboard_score_management.manage')
    options[:rel] ||= :manage_network_plus

    url = h.new_admin_manage_dashboard_actions_path(agreement_no: source.buyer_num)
    h.link_to(label, url, options)
  end
end
