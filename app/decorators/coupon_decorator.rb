# encoding: utf-8
class CouponDecorator < ApplicationDecorator
  delegate_all

  def edit_coupon_link
    h.link_to(
      I18n.t('admin.coupons.edit.link_copy'),
      h.edit_admin_coupon_path(source),
      rel: :edit_coupon
    )
  end

  def delete_coupon_link
    h.link_to(
      I18n.t('admin.coupons.destroy.link_copy'),
      h.admin_coupon_path(source.id),
      rel: :delete_coupon,
      method: :delete, data: { confirm: 'Are you sure?' }
    )
  end

  def show_coupon_link
    h.link_to(
      I18n.t('admin.coupons.show.link_copy'),
      h.admin_coupon_path(source.id),
      rel: :show_coupon
    )
  end

  def amount
    currency(source.amount, precision: 2)
  end

  def name
    source.coupon_batch_name
  end

  def status
    if source.consumed?
      'consumed'
    else
      'unconsumed'
    end
  end
end
