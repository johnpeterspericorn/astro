# encoding: utf-8
class QuickGuideDecorator < ApplicationDecorator
  delegate_all

  def self.quick_guides
    Dir[Rails.root.join('app', 'assets', 'pdfs', 'quick_guides', "*.#{I18n.locale}.pdf")].map do |filename|
      [File.basename(filename, ".#{I18n.locale}.pdf"), filename]
    end
  end
end
