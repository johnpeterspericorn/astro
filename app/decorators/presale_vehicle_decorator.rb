# encoding: utf-8
class PresaleVehicleDecorator < ApplicationDecorator
  include VehicleMixins::ViewConcerns # defines #full_description, #eligiblility_reason, #promise_price_threshold, #vehicle_code, #default_currency_options, #location_name
  delegate_all

  def low_valuation_value
    model.low_valuation || model.average_valuation
  end

  def high_valuation_value
    model.high_valuation || model.average_valuation
  end

  def average_valuation_value
    model.average_valuation
  end

  def low_valuation
    currency(model.low_valuation || model.average_valuation)
  end

  def high_valuation
    currency(model.high_valuation || model.average_valuation)
  end

  def average_valuation
    currency(model.average_valuation)
  end

  def price_at_100_percent_average_valuation
    currency(model.average_valuation)
  end

  def promise_low_price_at_100_percent_average_valuation_and_250_miles
    currency(model.promise_250_low)
  end

  def promise_medium_price_at_100_percent_average_valuation_and_250_miles
    currency(model.promise_250_medium)
  end

  def promise_high_price_at_100_percent_average_valuation_and_250_miles
    currency(model.promise_250_high)
  end

  def self.options_for_lane_select(pawn_location, lane_number = nil)
    pawn_location = Astro::DEFAULT_AUCTION_LOCATION if pawn_location.blank?
    h.options_for_select(lanes_at_location(pawn_location), lane_number)
  end

  def self.lanes_at_location(pawn_location)
    PresaleVehicle.lanes_for_pawn(pawn_location).map(&:to_i).sort
  end

  def currency_format
    location.currency['symbol']
  end
end
