# encoding: utf-8
class HowPaidInformationDecorator < ApplicationDecorator
  delegate_all

  def self.payment_type_options
    h.options_for_select(all_reasons)
  end

  def self.all_reasons
    I18n.t('vehicles.how_paid.payment_modes').flat_map(&:to_a).map(&:reverse)
  end
end
