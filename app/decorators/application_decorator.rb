# encoding: utf-8
require 'distance_converter'

class ApplicationDecorator < Draper::Decorator
  include ActionView::Helpers::NumberHelper

  private

  def default_currency_options(options)
    precision = options.fetch(:precision, 0)
    separator = options.fetch(:separator, '.')
    delimiter = options.fetch(:delimiter, ',')
    currency_unit = location ? location.currency['symbol'] : ''

    @default_currency_options ||= {
      unit: currency_unit,
      precision: precision,
      separator: separator,
      delimiter: delimiter,
      format: '%u %n'
    }
  end

  def currency(amount, options = {})
    h.number_to_currency amount, default_currency_options(options)
  end

  def location
    Country.find_country_by_name(country.present? ? country : 'United States of America')
  end

  def distance(value, country_code)
    DistanceConverter.new(value, country_code).convert
  end

  def format_price(number)
    number_with_precision(number, strip_insignificant_zeros: true)
  end
end
