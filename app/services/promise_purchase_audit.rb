# encoding: utf-8
class PromisePurchaseAudit
  delegate :promise_offer, :pawn_information, to: :promise_purchase
  delegate :vehicle_information, to: :promise_offer
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def versions
    versions  =  PromisePurchaseVersion.purchase_versions(promise_purchase, promise_offer, vehicle_information, pawn_information)
    versions  =  versions.initiated_at(date_from..date_to) if date_from.present? && date_to.present?
    versions.order('id desc').page(params[:page]).per(20)
  end

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find_by_id(promise_purchase_id)
  end

  def filter_params
    params.fetch(:filters, {})
          .permit(:date_from, :date_to, :promise_purchase_id)
  end

  def promise_purchase_id
    filter_params[:promise_purchase_id] || params[:promise_purchase_id]
  end

  def date_from
    filter_params[:date_from]
  end

  def date_to
    filter_params[:date_to]
  end
end
