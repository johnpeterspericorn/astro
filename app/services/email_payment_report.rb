# encoding: utf-8
require 'column_conversions'

class EmailPaymentReport
  include ColumnConversions
  class EmailInvalidError < StandardError; end
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def send_email
    raise EmailInvalidError unless EmailValidation.valid?(user_emails)
    payments = Payment.created_at(date_from..date_to)
    PaymentsMailer.report(payments.to_a, to: user_emails, subject: I18n.t('payments.email.subject.span_report')).deliver_later
  end

  private

  def user_emails
    arrayify(params[:emails])
  end

  def date_from_param
    params[:date_from]
  end

  def date_to_param
    params[:date_to]
  end

  def date_from
    parse_time(date_from_param, DateTime.current.beginning_of_day)
  end

  def date_to
    parse_time(date_to_param, DateTime.current)
  end

  def parse_time(time_string, default)
    return default unless time_string.present?
    Time.zone.parse(time_string)
  end
end
