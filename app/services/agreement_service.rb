# encoding: utf-8
class AgreementService
  attr_reader :agreement_info
  class AgreementInformationMissingError < StandardError; end

  def initialize(agreement_no)
    @agreement_info = AgreementInformation.for_agreement_no(agreement_no)
    raise AgreementInformationMissingError unless @agreement_info.present?
  end
end
