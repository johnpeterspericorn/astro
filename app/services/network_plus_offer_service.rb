# encoding: utf-8
class NetworkPlusOfferService
  attr_reader :network_plus_offer
  class NetworkPlusValidOfferExistsError < StandardError; end

  def initialize(agreement_no)
    @network_plus_offer = NetworkPlusOffer.for_agreement_no(agreement_no).active
  end

  def check_for_existing_offer
    raise NetworkPlusValidOfferExistsError if network_plus_offer.approved.present? || network_plus_offer.confirmed.present?
  end

  def valid_offer
    network_plus_offer.approved.present? ? network_plus_offer.approved : network_plus_offer.confirmed
  end
end
