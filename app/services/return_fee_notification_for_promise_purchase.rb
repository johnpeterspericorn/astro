# encoding: utf-8
class ReturnFeeNotificationForPromisePurchase
  class ReturnFeeIneligibleError < StandardError; end
  delegate :country, :canadian_location?, to: :@promise_purchase
  delegate :number_of_returns, :timeframe, :fee, :additional_returns, :additional_fee, to: :@fee_data

  def initialize(promise_purchase_id)
    @promise_purchase = PromisePurchase.find(promise_purchase_id)
    raise ReturnFeeIneligibleError if return_fee_ineligible?
    @fee_data = Hashie::Mash.new NewProductFeeInformation.new(@promise_purchase).fee_details
  end

  def purchase
    @promise_purchase
  end

  private

  def return_fee_ineligible?
    purchase.return_fee_captured? || purchase.seller_paid || !purchase.pplv?
  end
end
