# encoding: utf-8
require 'column_conversions'

class EmailDashboardScoreReport
  include ColumnConversions
  class EmailInvalidError < StandardError; end
  attr_reader :params

  def initialize(params)
    @params = params
  end

  def send_email
    raise EmailInvalidError unless EmailValidation.valid?(emails)
    DashboardScoreReportMailer.report(params, subject: I18n.t('dashboard_scores.email.subject')).deliver_later
  end

  def dashboard_scores_csv
    dashboard_scores = if agreement_number.present?
                         DashboardScore.where(buyer_num: agreement_number).order('id desc')
                       else
                         DashboardScore.all.order('id desc')
                       end
    column_names = %w(id buyer_num payment_kind avg_loss_with_precision return_rate_with_precision volume_with_precision margin_with_precision earnings_with_precision return_score_with_precision purchase_quality margin_score_with_precision volume_score_with_precision earnings_score_with_precision date)
    CSV.generate do |csv|
      csv << column_names
      dashboard_scores.decorate.each do |dashboard_score|
        csv << column_names.map { |attr| dashboard_score.public_send(attr) }
      end
    end
  end

  private

  def emails
    arrayify(params[:dashboard_score][:emails])
  end

  def agreement_number
    params[:dashboard_score][:agreement_no] if params[:dashboard_score].present?
  end
end
