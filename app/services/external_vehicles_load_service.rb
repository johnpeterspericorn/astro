# encoding: utf-8

class ExternalVehiclesLoadService
  attr_reader :params

  class DataInvalidationError < StandardError
    def initialize(msg)
      super(msg)
    end
  end

  def initialize(params, current_user = nil)
    @params = params
    @current_user = current_user
  end

  def save
    invalid_vnums = []
    batch_id = external_vehicle_batch.id
    params[:external_vehicles_load].each do |external_vehicles_load|
      external_vehicle = ExternalVehiclesLoad.new(external_vehicles_load.permit!)
      external_vehicle.purchased_at = DateTime.strptime("#{external_vehicles_load[:purchased_at]} #{Time.zone.now.zone}", '%m/%d/%y %H:%M %Z')
      external_vehicle.external_vehicles_batch_id = batch_id
      external_vehicle.user_id = current_user_id
      external_vehicle.automatic_purchase = true
      invalid_vnums << external_vehicle.vnum unless external_vehicle.save
    end
    params[:external_vehicles_documents].present? && params[:external_vehicles_documents].each do |doc|
      @external_vehicle_batch.external_vehicles_documents.create!(document: doc)
    end

    raise DataInvalidationError, "Vin: #{invalid_vnums.join(',')} not saved due to validation errors" if invalid_vnums.present?
    true
  end

  def external_vehicle_batch
    @external_vehicle_batch ||= ExternalVehiclesBatch.create(status: 'unapproved', user_id: @current_user.id)
  end

  private

  def current_user_id
    @current_user.id
  end
end
