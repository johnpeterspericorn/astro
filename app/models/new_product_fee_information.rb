# encoding: utf-8
class NewProductFeeInformation < Hashie::Dash
  attr_accessor :invoice_seller_paid, :invoice_seller_no, :invoice_agreement_no, :promise_purchase

  DEFAULT_FEES = {
    'no_fee'              => 0,
    'first_level_US_fee'  => 150,
    'second_level_US_fee' => 500,
    'first_level_CA_fee'  => 200,
    'second_level_CA_fee' => 650
  }.freeze

  def initialize(purchase)
    self.invoice_seller_paid = purchase.seller_paid
    self.invoice_seller_no = purchase.seller_no
    self.invoice_agreement_no = purchase.agreement_no
    self.promise_purchase = purchase
  end

  def fee_records
    @fee_records ||= NewProductFee.get_fee_records(invoice_seller_paid, invoice_seller_no, invoice_agreement_no)
  end

  def get_number_of_returns_within_timeframe(timeframe)
    @number_of_returns ||= if invoice_seller_paid
                             ReturnInvoice.for_seller_number(invoice_seller_no)
                           elsif promise_purchase.part_of_agreement_group?
                             ReturnInvoice.for_palv_group_code(promise_purchase.group_code, promise_purchase.country)
                           else
                             ReturnInvoice.for_agreement_number_with_palv_buyer_paid(invoice_agreement_no, promise_purchase.country)
                           end
    @number_of_returns = @number_of_returns.valid_and_initiated_at_within_timeframe(timeframe).size
    @number_of_returns + 1
  end

  def fee_info(number_of_invoices)
    fee_data = {}
    filtered_fee_records.each_with_index do |data, index|
      the_count = data[0]
      next unless number_of_invoices <= the_count
      fee_data[:count] = data[0]
      fee_data[:fee] = data[1]
      fee_data[:timeframe] = data[2]
      fee_data[:index] = index
      log_additional_fee_info(fee_data, number_of_invoices)
      return fee_data
    end
    fee_data
  end

  def fee_details
    return {} unless filtered_fee_records.present?
    timeframe = filtered_fee_records.first[2]
    number_of_returns = get_number_of_returns_within_timeframe(timeframe)
    fee_data = fee_info(number_of_returns)
    fee_data.merge(number_of_returns: number_of_returns)
  end

  def log_additional_fee_info(fee_data, number_of_returns)
    next_fee_index = fee_data[:index] + 1
    additional_data = filtered_fee_records[next_fee_index]
    return unless additional_data.present?
    fee_data[:additional_returns] = additional_returns(fee_data, number_of_returns)
    fee_data[:additional_fee] = additional_fee(fee_data, additional_data)
  end

  private

  def filtered_fee_records
    if fee_records.present?
      fee_records.pluck(:count, :fee, :timeframe, :id)
    elsif promise_purchase.canadian_location?
      default_fee_data(DEFAULT_FEES['first_level_CA_fee'], DEFAULT_FEES['second_level_CA_fee'])
    else
      default_fee_data(DEFAULT_FEES['first_level_US_fee'], DEFAULT_FEES['second_level_US_fee'])
    end
  end

  def default_fee_data(first_level_fee, second_level_fee)
    first_count = promise_purchase.limited_volume.to_i
    return [] unless first_count.present?
    second_count = NewProductKind::DEFAULT_VOLUME_TIERS[first_count]
    [
      [first_count, DEFAULT_FEES['no_fee'], NewProductKind::TIMEFRAME_365],
      [second_count, first_level_fee, NewProductKind::TIMEFRAME_365],
      [NewProductKind::DEFAULT_COUNT, second_level_fee, NewProductKind::TIMEFRAME_365]
    ]
  end

  def additional_returns(fee_data, number_of_returns)
    fee_data[:count] - number_of_returns
  end

  def additional_fee(fee_data, additional_data)
    additional_data[1] - fee_data[:fee]
  end
end
