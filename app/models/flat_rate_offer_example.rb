# encoding: utf-8
class FlatRateOfferExample
  VEHICLE_BASE_PRICE = 40_000 # dollars
  PSI_7_DAY_RATE     = 200    # dollars
  SURCHARGE_RATE     = 0.01   # 1%
  EXAMPLE_VEHICLE_PRICE = 48_000 # dollars

  delegate :target_price_for_miles, :target_price, to: :flat_rate_offer, allow_nil: true

  def initialize(flat_rate_offer, vehicle_price)
    @flat_rate_offer = flat_rate_offer
    @vehicle_price   = vehicle_price
  end

  delegate :country, to: :flat_rate_offer

  attr_reader :vehicle_price

  def offer_price
    flat_rate_offer.price
  end

  def vehicle_amount_in_excess_of_base
    vehicle_price - VEHICLE_BASE_PRICE
  end

  def surcharge
    vehicle_amount_in_excess_of_base * SURCHARGE_RATE
  end

  def program_rate
    target_price_for_miles + surcharge
  end

  def program_rate_with_psi
    program_rate + PSI_7_DAY_RATE
  end

  private

  attr_reader :flat_rate_offer
end
