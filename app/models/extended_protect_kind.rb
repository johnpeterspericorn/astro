# encoding: utf-8
class ExtendedProtectKind
  include ProtectKind
  THIRTY_DAYS = 30.days
  SEVEN_DAYS = 7.days
  EXTRA_DAY = 1.day

  def expiration_date
    date = [return_expiration_date, receive_by_date].max
    DateUtils.resolve_to_weekday(date)
  end

  def receive_by_date
    if DateUtils.is_date_before_latter((purchase.return_invoice.try(:created_at) || Time.current), promise_expiration_date)
      promise_expiration_date
    else
      promise_expiration_date + EXTRA_DAY
    end
  end

  def return_expiration_date
    date = promise_expiration_date
    date += SEVEN_DAYS  if purchase.program360?
    date
  end

  def promise_expiration_date
    time = (purchase_date || Time.current) + THIRTY_DAYS
    time.end_of_day + extended_days.days
  end

  alias final_window_expiration_date expiration_date
end
