# encoding: utf-8
class AstroheimAuthorization < BuyerAuthorization
  def initialize(options = {})
    @authorizations = {}
    @pawn_access_number = options[:pawn_access_number] || options[:universal_id]
    @username              = options[:username]

    get_pawn_access_number if @username.present? && @pawn_access_number.nil?
  end

  def get_pawn_access_number
    @pawn_access_number ||= user_authorization.pawn_access_numbers.try(:first)
    @pawn_access_number
  rescue SocketError, Errno::ECONNREFUSED
    super
  end

  def get_agreement_numbers
    get_agreement_numbers_to_names.keys
  end

  def get_agreement_numbers_to_names
    user_authorization.five_million_numbers_to_names
  rescue SocketError, Errno::ECONNREFUSED
    super
  end

  def get_agreementship_name_by_agreement_number(agreement_number)
    get_agreement_numbers_to_names[agreement_number]
  end

  def get_agreementship_addr
    user_authorization.agreementship_addr
  end

  def get_agreement_contact_info
    user_authorization.contact_information
  rescue SocketError, Errno::ECONNREFUSED
    super
  end

  def get_agreement_name
    get_agreement_contact_info.name
  end

  def get_agreement_phone
    get_agreement_contact_info.phone
  end

  def get_agreement_email
    get_agreement_contact_info.email
  end

  delegate :has_authorization_details?, to: :user_authorization

  def has_contact_details?
    user_authorization.has_contact_details?
  rescue SocketError, Errno::ECONNREFUSED
    super
  end

  private

  def user_authorization
    authorizations_key = @pawn_access_number || @username
    authorization_options = { universal_id: @pawn_access_number, username: @username }
    @authorizations[authorizations_key] ||=
      AstroheimAuthorizationService::UserAuthorization.new(
        authorization_service,
        authorization_options
      )
  end

  def authorization_service
    @authorization_service ||= AstroheimAuthorizationService::AuthorizationService.new(Astro::AUTHORIZATION_HOST, Astro::AUTHORIZATION_PORT, Astro::APP_ID)
  end
end
