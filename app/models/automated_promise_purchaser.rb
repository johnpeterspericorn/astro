# encoding: utf-8
class AutomatedPromisePurchaser
  attr_reader :purchase

  def initialize(vehicle, days = nil, miles = nil, channel = PromiseChannel::AUTOMATIC, purchaser_klass = PromisePurchaser)
    @vehicle = vehicle
    @channel = channel

    @additional_days = days ? days.to_i : vehicle.preselected_promise_days
    @additional_days -= Promise::BASE_DURATION if @additional_days.present?
    raise(ArgumentError, 'invalid number of days. must be 7, 14, or 21') unless Promise::LENGTHS.include?(@additional_days)

    @miles_selected = miles ? miles.to_i : vehicle.preselected_promise_miles
    raise(ArgumentError, 'invalid miles.  must be 250 or 360 or 500') unless Promise::DISTANCES.include?(@miles_selected)

    @purchaser_klass = purchaser_klass
  end

  def self.process_outstanding_vehicles_for_automatic_purchase
    SoldVehicle.outstanding_automatic_purchase.each do |vehicle|
      Rails.logger.info log_message(vehicle, 'processing automatic-purchase')
      record_purchase(vehicle)
    end

    SoldVehicle.outstanding_seller_paid.each do |vehicle|
      Rails.logger.info log_message(vehicle, 'processing Seller-Paid purchase')
      record_purchase(vehicle)
    end

    SoldVehicle.outstanding_ineligible.each do |vehicle|
      Rails.logger.info log_message(vehicle, 'ineligible for automatic purchase')
      PromisePurchaseMailer.notify_ineligible(vehicle).deliver_later
      vehicle.update_attribute(:automatic_purchase_processed, true)
    end
  end

  def self.record_purchase(vehicle, days = nil, miles = nil)
    new(vehicle, days, miles).record_purchase
  rescue ActiveRecord::StatementInvalid, ActiveRecord::RecordNotSaved, ActiveRecord::RecordInvalid, ArgumentError => e
    Rollbar.error(e)
    Rails.logger.error log_message(vehicle, 'purchase failed')
    Rails.logger.error "Error message: #{e.message}"
    InfrastructureMailer.automatic_purchase_failed(vehicle).deliver_later
    vehicle.automatic_purchase_processed = true if vehicle.automatic_purchase || vehicle.seller_paid
    vehicle.save!(validate: false)
  rescue => e
    Rollbar.error(e)
    Rails.logger.error log_message(vehicle, e.message)
  end

  def self.log_message(vehicle, message)
    vnum = vehicle.try(:vnum) || 'missing VNUM'
    pawn_access_number = vehicle.try(:pawn_access_no) || 'missing AA#'
    "[#{Time.current}] Automated Purchase: #{message} for Sold Vehicle with VNUM '#{vnum}' and AA# '#{pawn_access_number}'"
  end

  def record_purchase
    validate_vehicle
    record_offer

    @purchase = purchaser.record_purchase

    set_automatic_purchase_processed
  end

  private

  def purchaser
    @purchaser ||= @purchaser_klass.new(
      offer: @offer,
      email: confirmation_email,
      additional_days: @additional_days,
      miles: @miles_selected,
      channel: @channel,
      kind: promise_kind,
      partner: partner
    )
  end

  def confirmation_email
    @vehicle.automatic_purchase_email
  end

  def promise_kind
    @vehicle.preselected_promise_kind
  end

  def partner
    @vehicle.partner
  end

  def record_offer
    @offer = PromiseOfferRecorder.record_offer(@vehicle)
  end

  def set_automatic_purchase_processed
    @vehicle.update_attributes!(automatic_purchase_processed: true) if mark_as_automatic_purchase?
  end

  def mark_as_automatic_purchase?
    @vehicle.automatic_purchase || @vehicle.seller_paid
  end

  def vehicle_valid?
    @vehicle.valid_for_automated_purchase?
  end

  def validate_vehicle
    return if vehicle_valid?
    Rails.logger.error AutomatedPromisePurchaser.log_message(@vehicle, "validation failed. Validation Errors: [#{@vehicle.errors.full_messages.to_sentence}]")
    raise ActiveRecord::RecordInvalid, @vehicle
  end
end
