# encoding: utf-8
class FlatRateOfferBatch < ActiveRecord::Base
  include AASM

  has_many :flat_rate_offers, inverse_of: :flat_rate_offer_batch, dependent: :destroy

  has_paper_trail

  accepts_nested_attributes_for :flat_rate_offers, reject_if: ->(attrs) { attrs[:price].blank? }

  validates :pawn_access_no, presence: true
  validate  :buyer_represents_agreements

  before_create :set_expiration_date

  DEFAULT_EXPIRY_DAYS = 14

  def offered_agreement_numbers
    flat_rate_offers.map(&:agreement_no).compact
  end

  def eligible_for_acceptance?
    !expired? && unapproved? && !flat_rate_offers.blank?
  end

  def offers?
    !flat_rate_offers.blank?
  end

  def eligible_agreement_numbers
    authorization.get_agreement_numbers
  end

  def buyer_information
    BuyerInformation.build_with_defaults(pawn_access_no)
  end

  def offering_users
    flat_rate_offers.map(&:offering_user)
  end

  def approved?
    flat_rate_offers.all?(&:approved?)
  end

  def country
    first_offer = flat_rate_offers.first
    if first_offer.present? && first_offer.flat_rate && first_offer.country
      return first_offer.country
    end
    SupportedCountry::DEFAULT
  end

  def belongs_to_canada?
    country_code == 'CA'
  end

  def palv?
    flat_rate_offers.all?(&:palv)
  end

  def rejected?
    flat_rate_offers.all?(&:rejected?)
  end

  def expired?
    expiration_date && expiration_date.past?
  end

  def valid_offers?
    flat_rate_offers.all?(&:valid?)
  end

  def unapproved?
    flat_rate_offers.all?(&:unapproved?)
  end

  aasm column: :state do
    state :unconfirmed, initial: true
    state :confirmed

    event :reset do
      transitions to: :unconfirmed

      after do
        FlatRateMailer.offer_reset(self).deliver_later
      end
    end

    event :confirm do
      transitions from: :unconfirmed, to: :confirmed

      after do
        FlatRateMailer.offer_confirmation(self).deliver_later
      end
    end
  end

  def country_code
    country_object.try(:alpha2)
  end

  private

  def country_object
    Country.find_country_by_name(country || SupportedCountry::DEFAULT)
  end

  def set_expiration_date
    self.expiration_date = DEFAULT_EXPIRY_DAYS.days.from_now.utc.to_date
  end

  def authorization
    @authorization ||=
      Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_no)
  end

  def buyer_represents_agreements
    errors.add(:base, 'Buyer does not represent all agreements') unless offered_agreement_numbers.to_set.subset?(eligible_agreement_numbers.to_set)
  end
end
