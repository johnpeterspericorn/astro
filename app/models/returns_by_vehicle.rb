# encoding: utf-8
class ReturnsByVehicle
  def initialize(returns)
    @returns = Array(returns)
  end

  def each(returns_data_class = ReturnsDashboardData)
    returns.each do |returns|
      yield(returns_data_class.new(returns))
    end
  end

  private

  attr_reader :returns
end
