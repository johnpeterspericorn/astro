# encoding: utf-8
class UserVersion < PaperTrail::Version
  self.table_name = :user_versions

  def version_author
    User.find_by_id(whodunnit).try(:username) || 'unknown'
  end

  def version_user
    User.find_by_id(item_id).try(:username)
  end

  scope :initiated_at, ->(time) { where(created_at: time) }
end
