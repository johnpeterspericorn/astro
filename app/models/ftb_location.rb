# encoding: utf-8
class FtbLocation
  YAML_FILE_PATH = Rails.root.join('config', 'ftb_locations.yml')

  def self.all
    YAML.load_file(YAML_FILE_PATH)
  end
end
