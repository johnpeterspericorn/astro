# encoding: utf-8
class ReturnsDashboardData
  def initialize(returns)
    @returns = Array(returns)
  end

  delegate :count, to: :returns

  def average_vehicle_price
    returns.map(&:vehicle_purchase_price).instance_eval { reduce(0, :+) / size.to_f }
  end

  def field(field_value)
    returns.map(&:"#{field_value}").join
  end

  private

  attr_reader :returns
end
