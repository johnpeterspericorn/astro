# encoding: utf-8
module Autoims
  class RemoteClient
    attr_reader :request

    def self.create_drop_notice(return_invoice)
      request = CreateRequest.new(return_invoice)
      new(request).process_request
    end

    def self.delete_drop_notice(return_invoice)
      request = DeleteRequest.new(return_invoice)
      new(request).process_request
    end

    def initialize(request)
      @request = request
    end

    def process_request
      request.run
      response_body = request.response.try(:body)
      parsed_response = Nokogiri::XML(response_body)

      status_code = parsed_response.xpath('//Status/Code').text.to_i
      description = parsed_response.xpath('//Status/Description').text

      Logger.info("Request body: #{request.body}")
      Logger.info("Request params: #{request.params}")
      Logger.info("Response: #{response_body}")

      if status_code == 1
        Logger.info("Successful AutoIMS #{request.request_method} for vnum #{request.vnum}")
      else
        Logger.error("AutoIMS #{request.request_method} for vnum #{request.vnum} failed")
        raise "Response is unsuccessful: #{description} (status code #{status_code})"
      end
    end
  end
end
