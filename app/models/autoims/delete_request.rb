# encoding: utf-8
module Autoims
  class DeleteRequest < Request
    def initialize(return_invoice)
      super(return_invoice, :delete)
    end

    def params
      base_params = super

      if request_method == :delete
        base_params.merge(
          clientStockNum: drop_notice.vehicle_purchase_price,
          vnum: drop_notice.vnum
        )
      else
        base_params
      end
    end
  end
end
