# encoding: utf-8
module Autoims
  class Request
    attr_reader :drop_notice, :request_method
    delegate :run, :response, to: :request
    delegate :vnum, to: :drop_notice

    def initialize(return_invoice, request_method)
      @drop_notice = DropNotice.new(return_invoice)
      @request_method = request_method
    end

    def body
      # noop - override me
    end

    def params
      { clientId: 'MYRD' }
    end

    private

    def request
      @request ||= Typhoeus::Request.new(
        url,
        method: request_method,
        body: body,
        params: params,
        headers: { Accept: 'text/xml' }
      )
    end

    def url
      "https://#{USERNAME}:#{PASSWORD}@#{AUTOIMS_URL}"
    end
  end
end
