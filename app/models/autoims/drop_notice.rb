# encoding: utf-8
module Autoims
  class DropNotice
    DROP_OFF_NOTE_HEAD = 'AgreementShield Return Vehicle'.freeze

    delegate :vehicle_purchase_price, :vnum, to: :promise_offer

    def initialize(return_invoice)
      @return_invoice = return_invoice
      @promise_offer = return_invoice.promise_offer
    end

    def body_xml
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.Vehicle('xmlns' => XMLNS_URL, 'xmlns:xsi' => XSI_URL) do
          xml.ClientAccountNumber client_account_number
          xml.VNUM vnum
          xml.DecodeVNUM true
          xml.ClientField1 return_invoice.id
          xml.ClientField2 purchase_location
          xml.ClientField3 purchase_and_payer_type
          xml.ClientField4 reason_for_return
          xml.PrivateBookValue vehicle_purchase_price
          xml.PrivateDate1 promise_offer.purchased_at.strftime('%Y-%m-%d')
          xml.PrivateDate2 return_invoice.expiration_date.strftime('%Y-%m-%d')
          xml.PrivateCode2 return_location
          xml.PawnAssignment do
            xml.PawnID return_location
            xml.DropOffDate returned_at.strftime('%Y-%m-%d')
            xml.PickupOrDropOffNote drop_off_note
          end
          xml.Note do
            xml.NoteText drop_off_note
          end
        end
      end

      builder.to_xml
    end

    private

    attr_reader :return_invoice, :promise_offer

    def drop_off_note
      "#{DROP_OFF_NOTE_HEAD} OrigWO: $#{purchase_location}$ ##{work_order_number}#"
    end

    def purchase_and_payer_type
      "#{promise_offer.kind} #{payer_type}"
    end

    def payer_type
      promise_offer.seller_paid? ? 'seller' : 'buyer'
    end

    def work_order_number
      promise_offer.work_order_number
    end

    def purchase_location
      promise_offer.location_initials
    end

    def return_location
      return_invoice.pawn_location.initials
    end

    def reason_for_return
      return_invoice.additional_information.first(30) if return_invoice.additional_information.present?
    end

    def returned_at
      return_invoice.created_at
    end

    def client_account_number
      "#{return_invoice.id}.#{vehicle_purchase_price.round}"
    end
  end
end
