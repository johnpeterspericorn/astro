# encoding: utf-8
module Autoims
  class CreateRequest < Request
    def initialize(return_invoice)
      super(return_invoice, :post)
    end

    def body
      drop_notice.body_xml
    end
  end
end
