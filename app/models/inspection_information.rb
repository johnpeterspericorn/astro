# encoding: utf-8
require 'active_record_attribute_sanitizer'

class InspectionInformation < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer
  belongs_to :promise_offer
  delegate :country, to: :promise_offer
end
