# encoding: utf-8
class PaymentResponse < ActiveRecord::Base
  belongs_to :payment
  belongs_to :promise_purchase
  belongs_to :cancelled_purchase, foreign_key: :promise_purchase_id, primary_key: :promise_purchase_id

  def success?
    error_message.nil?
  end

  def error?
    !success?
  end

  def error_code
    return unless error?
    val = error_message.match(/\(SOAP-ENV:Server\)\s+(\d+)/)[1]
    val.nil? ? nil : val.to_i
  end

  def error_message_sanitized
    return unless error?
    error_message.sub(/(\(SOAP-ENV:Server\))\s*\d+\:*\s*(Error.\s)*/, '')
  end

  def error_message_internationalized
    return unless error?
    I18n.t("payments.errors.#{error_code}")
  end
end
