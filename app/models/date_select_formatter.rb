# encoding: utf-8
class DateSelectFormatter
  include ActiveModel::Validations

  attr_reader :year, :month, :day, :date_select_hash

  validate :date

  DATE_SELECT_PARAMETERS = {
    year:  '1i',
    month: '2i',
    day:  '3i'
  }.freeze

  def initialize(date_select_hash)
    @date_select_hash = date_select_hash

    DATE_SELECT_PARAMETERS.each do |k, v|
      instance_variable_set("@#{k}", date_select_hash.select { |param, _value| param.include? v }.values[0].to_i)
    end
  end

  def format
    Date.new(year, month, day)
  rescue ArgumentError => e
    Rails.logger.error "Error message: #{e.message}"
    nil
  end

  private

  def invalid_date?
    (date_select_hash.any? { |_k, v| v.blank? }) || !(Date.valid_date? year, month, day)
  end

  def date
    errors.add(:base, I18n.t('date_select_validation.invalid')) if invalid_date?
  end
end
