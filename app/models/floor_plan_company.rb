# encoding: utf-8
class FloorPlanCompany < ActiveRecord::Base
  has_many(
    :floor_plan_branches,

    foreign_key: :company_code,
    primary_key: :company_code,
    inverse_of: :floor_plan_company
  )

  validates :company_code, presence: true, uniqueness: true
end
