# encoding: utf-8
class TransactionIdGenerator
  BLOB_HALF_LENGTH = 6

  def self.random_blob(half_length = BLOB_HALF_LENGTH)
    SecureRandom.hex(half_length)
  end

  attr_reader :pawn_access_number

  def initialize(pawn_access_number = nil)
    @pawn_access_number = pawn_access_number || 0
  end

  def generate(time = Time.current, blob = self.class.random_blob)
    timestamp = time.to_i
    [pawn_access_number, timestamp, blob].join('_')
  end
end
