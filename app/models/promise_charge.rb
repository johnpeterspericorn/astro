# encoding: utf-8
class PromiseCharge < Struct.new(:promise_offer)
  delegate :location_initials, :lane_no, :run_no, :sale_no, :work_order_number, to: :pawn_information
  delegate :days_selected,
           :miles_selected,
           :rounded_promise_price,
           :vnum,
           :pawn_information,
           to: :promise_offer

  def submit
    log_charge
    charge.submit!
    promise_offer.mark_charged!
  rescue => e
    promise_offer.mark_charge_failed!
    raise e
  end

  def attributes
    { amount:                    amount,
      pawn_location_initials: location_initials,
      description:               selection_description,
      lane_number:               lane_no,
      run_number:                run_no,
      sale_number:               sale_no,
      work_order_number:         work_order_number }
  end

  private

  def log_charge
    ChargesLogger.info <<-LOG
      Submitting charge for purchase:
        VNUM:            #{vnum}
    LOG
  end

  def charge
    @charge ||= Astroheim::Charge.new(attributes)
  end

  def decorated_promise_offer
    @decorated_promise_offer ||= promise_offer.decorate
  end

  def amount
    rounded_promise_price
  end

  def selection_description
    decorated_promise_offer.selection_description
  end
end
