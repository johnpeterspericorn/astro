# encoding: utf-8
class SalesData
  def initialize(sales)
    @sales = Array(sales)
  end

  delegate :count, to: :sales

  def agreement_count
    sales.map(&:agreement_no).uniq.size
  end

  def auto_purchased_sales
    sales.select(&:automatic_purchase)
  end

  def seller_paid_sales
    sales.select(&:seller_paid)
  end

  def agreement_360_count
    auto_purchased_sales.map(&:agreement_no).uniq.size
  end

  def seller_paid_agreement_count
    seller_paid_sales.map(&:agreement_no).uniq.size
  end

  def walkup_agreements_count
    agreement_count - (agreement_360_count + seller_paid_agreement_count)
  end

  def walkup_ratio
    (count - (auto_purchased_sales.size + seller_paid_sales.size)).fdiv(count) * 100
  end

  def sellerside_ratio
    seller_paid_sales.size.fdiv(count) * 100
  end

  def ds_price
    sales.map(&:promise_price).compact.reduce(0, :+) / count
  end

  private

  attr_reader :sales
end
