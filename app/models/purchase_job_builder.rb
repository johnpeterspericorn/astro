# encoding: utf-8
class PurchaseJobBuilder < ChargesJobBuilder
  def self.enqueue_job(offer)
    new(offer).enqueue
  end

  def job
    if charge_bsc?
      BscPurchaseJob
    elsif charge_sows?
      SowsPurchaseJob
    elsif charge_g2gws_orders?
      G2gwsOrdersApiPurchaseJob
    end
  end
end
