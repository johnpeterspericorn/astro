# encoding: utf-8
class SilentPromisePurchaser < PromisePurchaser
  def record_purchase
    ActiveRecord::Base.transaction do
      create_purchase
      record_selection
      record_transaction
    end

    purchase
  end
end
