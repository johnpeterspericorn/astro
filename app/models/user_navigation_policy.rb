# encoding: utf-8
class UserNavigationPolicy < Struct.new(:user)
  def authorized_to_purchase?
    user.super_user? || !user.buyer_user? || user.authorized_to_purchase?
  end
end
