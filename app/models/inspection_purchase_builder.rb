# encoding: utf-8
class InspectionPurchaseBuilder < Struct.new(:purchase_queries)
  def inspection_purchases(options = { include_bundled_promises: true })
    queries_with_new_inspections(options[:include_bundled_promises])
      .map(&:inspection_purchase)
  end

  def unbundled_inspection_purchases
    inspection_purchases(include_bundled_promises: false)
  end

  private

  def queries_with_new_inspections(include_bundled_promises)
    purchase_queries
      .reject { |query| !include_bundled_promises && !query.inspection_only? }
      .select(&:inspection_selected?)
      .select(&:eligible_for_inspection?)
  end
end
