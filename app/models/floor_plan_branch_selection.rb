# encoding: utf-8
class FloorPlanBranchSelection < ActiveRecord::Base
  delegate :floor_plan_company, to: :floor_plan_branch
  delegate :name, to: :floor_plan_branch, prefix: true
  delegate :name, to: :floor_plan_company, prefix: true

  def floor_plan_branch
    @floor_plan_branch ||=
      FloorPlanBranch.find_by_company_code_and_site_no(company_code, site_no)
  end
end
