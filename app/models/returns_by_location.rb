# encoding: utf-8
class ReturnsByLocation
  def initialize(returns)
    @returns = Array(returns)
  end

  def each(returns_data_class = ReturnsDashboardData)
    location_grouped_returns.each do |location, returns|
      yield(location, returns_data_class.new(returns))
    end
  end

  private

  attr_reader :returns

  def location_grouped_returns
    returns
      .group_by(&:pawn_location)
  end
end
