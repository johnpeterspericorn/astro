# encoding: utf-8
require 'column_conversions'

class Settings < Setler::Settings
  extend ColumnConversions

  def self.set_many(settings_params)
    success = true
    settings_params.each do |flag, value|
      setting_value = value_to_boolean(value)
      Settings[flag] = setting_value
      success = false if Settings[flag] != setting_value
    end
    success
  end
end
