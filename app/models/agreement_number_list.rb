# encoding: utf-8
class AgreementNumberList
  include ActiveModel::Validations

  validates :pawn_access_number, presence: true

  delegate :each, :to_s, to: :agreement_numbers

  def initialize(pawn_access_number)
    @pawn_access_number = pawn_access_number
  end

  private

  attr_reader :pawn_access_number

  def agreement_numbers
    @agreement_numbers ||= astroheim_authorization.get_agreement_numbers_to_names
  end

  def astroheim_authorization
    @astroheim_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number)
  end
end
