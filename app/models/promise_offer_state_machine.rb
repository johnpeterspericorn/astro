# encoding: utf-8
module PromiseOfferStateMachine
  def self.extended(base)
    # TODO: remove whiny transitions #92375108
    base.aasm column: :promise_status, whiny_transitions: false do
      state :unselected, initial: true
      state :promised
      state :pending
      state :charged
      state :charge_failed

      event :pending_transition do
        transitions to: :pending
      end

      event :promised_transition do
        transitions from: :unselected, to: :promised
        transitions from: :charged, to: :promised
        transitions from: :charge_failed, to: :promised

        after do
          touch(:promised_at)
        end
      end

      event :allow_pending do
        transitions from: :pending, to: :promised

        after do
          promise_purchase.set_transaction_id

          touch(:promised_at)
          touch(:approved_at)

          PurchaseJobBuilder.enqueue_job(self)

          PromiseChargeJob.enqueue_from_offer(self) if charge_on_finalization?

          promise_purchase.send_confirmation(emails)
        end
      end

      event :process_cancellation do
        transitions to: :unselected
        transitions from: :pending, to: :unselected do
          after do
            touch(:invalidated_at)
          end
        end
      end

      event :mark_charged do
        transitions from: :promised, to: :charged
      end

      event :mark_charge_failed do
        transitions from: :promised, to: :charge_failed
      end
    end
  end
end
