# encoding: utf-8
class Shipment < ActiveRecord::Base
  has_one :return_invoice

  delegate :agreement_information, :buyer_email, to: :return_invoice

  validates :easypost_shipment_id, :tracking_code, presence: true
end
