# encoding: utf-8
class FloorPlanBranch < ActiveRecord::Base
  scope :active, -> { where(active: true) }

  belongs_to(
    :floor_plan_company,

    foreign_key: :company_code,
    primary_key: :company_code,
    inverse_of: :floor_plan_branches
  )
  delegate :generate_shipping_label, to: :floor_plan_company
  validates :phone_number, allow_blank: true, length: { is: 10 }, numericality: { only_integer: true }

  def to_address(verify_flag = true)
    Address.new(
      address_street,
      address_suite,
      address_city,
      address_state,
      address_zipcode,
      verify_flag
    )
  end

  def to_shipment_info
    FloorPlanBranchShipmentInfo.new(self)
  end
end
