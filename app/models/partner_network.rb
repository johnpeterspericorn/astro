# encoding: utf-8
class PartnerNetwork
  NAMES = [
    US_ASTROHEIM = 'US-Astroheim'.freeze,
    US_NON_ASTROHEIM = 'US-NonAstroheim'.freeze,
    CA_ASTROHEIM = 'CA-Astroheim'.freeze
  ].freeze
end
