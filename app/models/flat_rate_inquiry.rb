# encoding: utf-8
class FlatRateInquiry < ActiveRecord::Base
  belongs_to :user
  delegate :username, to: :user
end
