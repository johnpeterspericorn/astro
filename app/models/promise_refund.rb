# encoding: utf-8
class PromiseRefund < PromiseCharge
  def initialize(promise_offer, cancelled_purchase)
    super(promise_offer)
    @cancelled_purchase = cancelled_purchase
  end

  def submit
    log_charge
    @cancelled_purchase.update_attributes!(charge_failed: true) unless charge.submit
  end

  private

  def amount
    -rounded_promise_price
  end
end
