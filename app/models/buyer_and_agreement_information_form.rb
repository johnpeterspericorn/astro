# encoding: utf-8
require 'column_conversions'

class BuyerAndAgreementInformationForm
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ColumnConversions

  BUYER_INFORMATION_ATTRIBUTES  = BuyerInformation.sanitized_attribute_names
  DEALER_INFORMATION_ATTRIBUTES = AgreementInformation.sanitized_attribute_names

  delegate(*BUYER_INFORMATION_ATTRIBUTES,  to: :buyer_information)
  delegate(*DEALER_INFORMATION_ATTRIBUTES, to: :agreement_information)

  validates(
    :name,
    :emails,
    :address_street,
    :address_city,
    :address_state,
    :address_zipcode,

    presence: true
  )
  # represents the agreementship telephone saved in AgreementInformation#phone
  validates :phone, presence: true, if: :validate_agreement_contact_phone?

  validate :address_exists

  def initialize(pawn_access_number, agreement_number, enable_verification_params, country)
    @pawn_access_number = pawn_access_number
    @agreement_number         = agreement_number
    @enable_verification = value_to_boolean(enable_verification_params)
    @country = country
  end

  def update(attributes)
    attributes ||= {}
    update_buyer_information(attributes)
    update_agreement_information(attributes)
    canadian_purchase? ? true : valid?
  end

  def persisted?
    false
  end

  private

  attr_reader :pawn_access_number, :agreement_number, :country

  def buyer_information
    @buyer_information ||= BuyerInformation.build_with_defaults(pawn_access_number)
  end

  def agreement_information
    @agreement_information ||= AgreementInformation.build_with_defaults(pawn_access_number, agreement_number)
  end

  def update_buyer_information(attributes)
    attributes = attributes
                 .with_indifferent_access
                 .slice(*BUYER_INFORMATION_ATTRIBUTES)

    buyer_information.update_attributes(attributes)
  end

  def update_agreement_information(attributes)
    attributes = attributes
                 .with_indifferent_access
                 .slice(*DEALER_INFORMATION_ATTRIBUTES)

    agreement_information.update_attributes!(attributes)
  end

  def address_exists
    address = agreement_information.to_address(@enable_verification)

    return if address.valid?
    error_message = I18n.t('return_invoice.address.invalid')
    errors.add(:address, error_message)
  end

  def canadian_purchase?
    country == SupportedCountry::Canada
  end

  def validate_agreement_contact_phone?
    Settings.fedex_shipments_enabled
  end
end
