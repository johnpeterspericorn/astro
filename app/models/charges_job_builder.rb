# encoding: utf-8
ChargesJobBuilder = Struct.new(:promise_offer) do
  def enqueue
    job.enqueue(promise_offer) if job && charge?
  end

  def charge_sows?
    Settings.sows_enabled && promise_offer.sows_eligible? && !bsc_location?
  end

  def charge_bsc?
    Settings.bsc_enabled && bsc_location?
  end

  def charge_g2gws_orders?
    Settings.g2gws_orders_api_enabled? && promise_offer.g2gws_eligible? && !bsc_location?
  end

  private

  def charge?
    !promise_offer.bypass_charge?
  end

  def bsc_location?
    bsc_locations.include?(promise_offer.location_initials)
  end

  def bsc_locations
    @bsc_locations ||= Settings.bsc_locations || []
  end
end
