# encoding: utf-8
class CancelledReturnReport
  delegate :presigned_url, to: :s3_file
  delegate :empty?, to: :@cancelled_returns

  def initialize(cancelled_returns = [])
    @cancelled_returns = cancelled_returns
  end

  def create_and_upload
    create
    upload
  end

  def month
    Date.current.strftime('%B %Y')
  end

  def self.monthly_report
    date_from = Date.current.beginning_of_month
    date_to = Date.current.end_of_day

    report = new(ReturnInvoice.unscoped.where(cancelled_at: date_from..date_to))
    report.create_and_upload
    report
  end

  private

  def create
    require 'csv'

    CSV.open(local_file_path, 'w') do |csv|
      csv << ['VRA#', 'Cancelled At']
      @cancelled_returns.each do |cancelled_return|
        csv << [cancelled_return.id, cancelled_return.cancelled_at]
      end
    end
  end

  def upload
    s3_file.upload(local_file_path)
  end

  def s3_file
    @s3_file ||= S3File.new("reports/#{file_name}")
  end

  def file_name
    @file_name ||= "cancelled_vras_#{Time.current.utc.strftime('%Y%m%d%H%M%S')}.csv"
  end

  def local_file_path
    "/tmp/#{file_name}"
  end
end
