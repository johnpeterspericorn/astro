# encoding: utf-8
class VinScanRecord < ActiveRecord::Base
  validates :pawn_access_no, presence: true
end
