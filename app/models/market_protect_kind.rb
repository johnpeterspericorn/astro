# encoding: utf-8
class MarketProtectKind
  include ProtectKind
  EXTRA_DAY = 1.day
  THREE_DAYS = 3.days
  MINIMUM_RETURN_TIME_DAYS = 60
  MINIMUM_RETURN_TIME_DAYS_OFFSET = MINIMUM_RETURN_TIME_DAYS.days

  def promise_expiration_date
    time = (purchase_date || Time.current) + purchase.days_until_return_window(MINIMUM_RETURN_TIME_DAYS).days
    time.end_of_day + extended_days.days
  end

  def receive_by_date
    if DateUtils.is_date_before_latter((purchase.return_invoice.try(:created_at) || Time.current), promise_expiration_date)
      promise_expiration_date
    else
      promise_expiration_date + EXTRA_DAY
    end
  end

  def return_expiration_date
    date = promise_expiration_date
    date += THREE_DAYS if purchase.program360?
    date
  end

  def expiration_date
    date = [return_expiration_date, receive_by_date].max
    DateUtils.resolve_to_weekday(date)
  end

  def min_return_time
    purchase_date.end_of_day + MINIMUM_RETURN_TIME_DAYS_OFFSET
  end

  alias final_window_expiration_date expiration_date
end
