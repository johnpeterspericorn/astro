# encoding: utf-8
class FloorPlanBranchShipmentInfo
  delegate :to_address, to: :floor_plan_branch

  def initialize(floor_plan_branch)
    @floor_plan_branch = floor_plan_branch
  end

  def company
    floor_plan_company.name
  end

  def name
    floor_plan_branch.shipment_name
  end

  def phone
    floor_plan_branch.phone_number
  end

  private

  attr_reader :floor_plan_branch
  delegate :floor_plan_company, to: :floor_plan_branch
end
