# encoding: utf-8
class BuyerInformationDefaults
  def initialize(pawn_access_number, agreement_number, auth_class = Astro::BUYER_AUTHORIZATION_CLASS)
    @pawn_access_number = pawn_access_number
    @agreement_number         = agreement_number
    @auth_class            = auth_class
  end

  def name
    auth.get_agreement_name.presence
  end

  def email
    auth.get_agreement_email.presence
  end

  def cell_phone
    auth.get_agreement_phone.presence
  end

  def agreementship_name
    auth.get_agreementship_name_by_agreement_number(agreement_number.to_s).presence
  end

  def address_street
    auth_agreementship_addr.line1.presence
  end

  def address_suite
    auth_agreementship_addr.line2.presence
  end

  def address_city
    auth_agreementship_addr.city.presence
  end

  def address_state
    auth_agreementship_addr.state.presence
  end

  def address_zipcode
    auth_agreementship_addr.zip.presence
  end

  private

  attr_reader :pawn_access_number, :agreement_number, :auth_class

  def auth
    if pawn_access_number
      @auth ||= auth_class.new(pawn_access_number: pawn_access_number)
    else
      null_auth_class.new
    end
  end

  def auth_agreementship_addr
    auth.get_agreementship_addr || null_address_class.new
  end

  def null_auth_class
    Naught.build do |c|
      c.mimic(Astro::BUYER_AUTHORIZATION_CLASS)
    end
  end

  def null_address_class
    Naught.build
  end
end
