# encoding: utf-8
class CancelledPurchase < ActiveRecord::Base
  has_paper_trail

  belongs_to :purchasing_user, class_name: 'User'
  belongs_to :cancelling_user, class_name: 'User'

  scope :will_appear_on_accounts_receivable_report_at, ->(time) do
    where(charge_failed: true)
      .where('created_at > ?', time - AccountsReceivableMailer::INTERVAL)
  end

  def self.create_from_purchase!(reason, promise_purchase, user)
    offer = promise_purchase.promise_offer
    create_from!(
      reason,
      offer.pawn_information,
      offer.vehicle_information,
      offer,
      promise_purchase,
      offer.inspection_purchase,
      user
    )
  end

  def self.create_from!(reason, pawn_information, vehicle_information, promise_offer, promise_purchase, inspection_purchase, user)
    attributes = {
      reason:                   reason,
      buyer_email:              promise_offer.emails,
      pawn_access_no:        pawn_information.pawn_access_no,
      agreement_no:                pawn_information.agreement_no,
      universal_no:             pawn_information.universal_no,
      buy_fee:                  pawn_information.buy_fee,
      vnum:                      vehicle_information.vnum,
      work_order_number:        pawn_information.work_order_number,
      promise_offer_id:       promise_offer.id,
      promise_days_selected:  promise_offer.days_selected,
      promise_miles_selected: promise_offer.miles_selected,
      promise_if_bid:         promise_offer.if_bid,
      promise_seller_paid:    promise_offer.seller_paid,
      automatic_purchase:       promise_offer.automatic_purchase,
      vehicle_purchase_price:   pawn_information.vehicle_purchase_price,
      promise_price:          promise_offer.promise_price,
      inspection_price:         inspection_purchase && inspection_purchase.price,
      transaction_id:           promise_purchase.transaction_id,
      purchased_at:             promise_purchase.created_at,
      promise_purchase_id:    promise_purchase.id,
      promise_kind:           promise_purchase.kind,
      promise_additional_days: promise_purchase.additional_days,
      location_initials:        promise_offer.location_initials,
      purchasing_user:          promise_purchase.purchasing_user,
      cancelling_user:          user
    }
    CancelledPurchase.create!(attributes)
  end

  def calculated_days_selected
    case promise_kind
    when PromisePurchase::EXTENDED_PROTECT
      PromisePurchase::EP_OFFSET
    when PromisePurchase::TOTAL_PROTECT
      PromisePurchase::TP_OFFSET
    else
      (promise_days_selected || Promise::BASE_DURATION + promise_additional_days)
    end
  end
end
