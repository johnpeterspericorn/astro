# encoding: utf-8
require 'active_record_attribute_sanitizer'

class BuyerInformation < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer

  has_many :pawn_information, foreign_key: :pawn_access_no, primary_key: :pawn_access_no
  validates :pawn_access_no, presence: true, uniqueness: true

  scope :for_agreement_number, ->(agreement_number) do
    includes(:pawn_information)
      .where(pawn_information: { agreement_no: agreement_number })
  end

  def self.build_with_defaults(
      pawn_access_number,
      defaults = BuyerInformationDefaults.new(pawn_access_number, nil)
  )

    buyer_info = find_or_initialize_by(pawn_access_no: pawn_access_number.to_s)

    buyer_info.name ||= defaults.name
    buyer_info.emails ||= defaults.email
    buyer_info.cell_phone ||= defaults.cell_phone

    buyer_info
  end

  def agreement_numbers
    pawn_information.pluck('distinct agreement_no').compact
  end
end
