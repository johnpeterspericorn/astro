# encoding: utf-8
class InspectionPurchase < ActiveRecord::Base
  belongs_to :promise_offer
  has_one :promise_purchase, through: :promise_offer

  delegate :pawn_location, to: :promise_offer
  delegate :country, to: :promise_offer

  scope :unbundled_for_transaction_id, ->(transaction_id) do
    if Settings.inspections_enabled
      joins(:promise_offer)
        .joins('LEFT OUTER JOIN promise_purchases ON promise_purchases.promise_offer_id = promise_offers.id')
        .where(transaction_id: transaction_id)
        .where(promise_purchases: { promise_offer_id: nil })
    else
      InspectionPurchase.where(transaction_id: transaction_id)
    end
  end
end
