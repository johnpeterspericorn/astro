# encoding: utf-8
class PromisePurchaser < Hashie::Dash
  property :offer,           required: true
  property :additional_days, required: true
  property :miles,           required: true
  property :channel,         required: true
  property :email
  property :price
  property :partner
  property :kind, default: PromisePurchase::PURCHASE_PROTECT

  attr_reader :purchase

  def record_purchase
    ActiveRecord::Base.transaction do
      update_offer_attributes
      create_purchase
      record_selection
      record_transaction
      charge_purchase
      send_confirmation
    end

    purchase
  end

  private

  def create_purchase
    attributes = {
      additional_days: additional_days,
      miles_selected:  miles,
      promise_offer: offer,
      channel: channel,
      kind: kind,
      partner: partner
    }
    @purchase = PromisePurchase.create!(attributes)
  end

  def update_offer_attributes
    offer.update_attributes!(promise_price: price) if override_price?
  end

  def override_price?
    Settings.disable_purchase_api_price_match && price
  end

  def record_selection
    PromiseSelectionRecorder.record_selected([@purchase], email)
    offer.reload # bug https://www.pivotaltracker.com/story/show/61237376
  end

  def record_transaction
    @purchase.update_attributes!(transaction_id: transaction_id)
  end

  def transaction_id
    TransactionIdGenerator.new(offer.pawn_access_no).generate
  end

  def charge_purchase
    PurchaseJobBuilder.enqueue_job(offer)
    PromiseChargeJob.enqueue_from_offer(offer) if charge_purchase?
  end

  def send_confirmation
    if offer.pending?
      PromisePurchaseMailer.pending_confirmation([], [@purchase], email).deliver_later
    else
      @purchase.send_confirmation(email)
    end
  end

  def charge_purchase?
    !offer.pending? && !offer.seller_paid? && !offer.bypass_charge?
  end
end
