# encoding: utf-8
require 'date_utils'
class PerformanceAlert < ActiveRecord::Base
  include DateUtils

  after_create :send_email

  belongs_to :current_user, class_name: 'User'
  belongs_to :relationship_manager

  delegate :name, :email, to: :relationship_manager

  validate :return_rate_or_lpc, :follow_up_date_input
  validates :relationship_manager, presence: true

  DEFAULT_FOLLOW_UP_DAYS = 30

  scope :for_agreement_no, lambda { |agreement_number|
    where(agreement_no: agreement_number.to_s)
  }

  def agreement_info
    AgreementInformation.for_agreement_no(agreement_no).first
  end

  def send_email
    PerformanceAlertMailer.send_email(self)
  end

  def follow_up_date_local
    follow_up_date.in_time_zone
  end

  private

  def return_rate_or_lpc
    errors.add(:return_rate_or_purchase_quality, 'must be selected') unless return_rate || lpc
  end

  def default_follow_up_date
    (DateUtils.now + DEFAULT_FOLLOW_UP_DAYS.days).end_of_day
  end

  def follow_up_date_input
    errors.add(:follow_up_date, 'must be within 30 days from now') unless (DateUtils.now...default_follow_up_date).cover?(follow_up_date)
  end
end
