# encoding: utf-8
class PurchaseProtectKind
  include ProtectKind
  EXTRA_DAY = 1.day
  THREE_DAYS = 3.days

  def promise_expiration_date
    time = (purchase_date || Time.current) + purchase.days_selected.days
    time.end_of_day + extended_days.days
  end

  def receive_by_date
    if DateUtils.is_date_before_latter((purchase.return_invoice.try(:created_at) || Time.current), promise_expiration_date)
      promise_expiration_date
    else
      promise_expiration_date + EXTRA_DAY
    end
  end

  def return_expiration_date
    date = promise_expiration_date
    date += THREE_DAYS if purchase.program360?
    date
  end

  def expiration_date
    date = [return_expiration_date, receive_by_date].max
    DateUtils.resolve_to_weekday(date)
  end

  alias final_window_expiration_date expiration_date
end
