# encoding: utf-8
require 'csv'

class PromiseExtension < ActiveRecord::Base
  validates :vnum, presence: true

  def self.import_from_csv(csv_file)
    CSV.foreach(csv_file.path, headers: true) do |row|
      vnum = row[0]
      data = {
        vnum: vnum,
        additional_days: row[1],
        reason: row[2]
      }
      existing = PromiseExtension.find_by(vnum: vnum)
      if existing
        existing.update(data.merge(status: 'new'))
      else
        PromiseExtension.create!(data)
      end
    end

    Delayed::Job.enqueue AddExtensionDaysJob.new
  end
end
