# encoding: utf-8
class FlatRateAttribute < Hashie::Dash
  property :flat_rate, required: true
  property :miles
  property :psi
  property :palv

  def target_price
    flat_rate.public_send("target_price_#{selected_miles_and_psi}")
  end

  def target_price_for_miles
    flat_rate.public_send("target_price_#{selected_miles}")
  end

  def available?
    !target_price.nil?
  end

  private

  def selected_miles_and_psi
    return 'lv' if palv
    psi ? "#{miles}_psi" : miles
  end

  def selected_miles
    palv ? 'lv' : miles.to_s
  end
end
