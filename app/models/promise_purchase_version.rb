# encoding: utf-8
class PromisePurchaseVersion < PaperTrail::Version
  self.table_name = :promise_purchase_versions

  def version_author
    User.find_by_id(whodunnit).try(:username) || 'unknown'
  end

  scope :initiated_at, ->(time) { where(created_at: time) }
  scope :purchase_versions, ->(promise_purchase, promise_offer, vehicle_information, pawn_information) { where("(item_id = #{promise_purchase.id} and item_type = '#{promise_purchase.class.name}') or (item_id = #{promise_offer.id} and item_type = '#{promise_offer.class.name}') or (item_id = #{vehicle_information.id} and item_type = '#{vehicle_information.class.name}') or (item_id = #{pawn_information.id} and item_type = '#{pawn_information.class.name}')") }
end
