# encoding: utf-8
require 'active_record_attribute_sanitizer'

# rubocop:disable Metrics/ClassLength
class PromiseOffer < ActiveRecord::Base #
  include AASM

  PURCHASED_STATES = %w(promised charged charge_failed).freeze
  DAYS = %w(7 14 21).freeze

  include ActiveRecordAttributeSanitizer
  include VehicleMixins::DomainConcerns # defines #eligible? and #ineligible?

  extend PromiseOfferStateMachine

  has_paper_trail class_name: 'PromisePurchaseVersion'

  before_save :set_ineligibility_for_invalid_pricing

  has_one :pawn_information, dependent: :destroy
  has_one :inspection_information, dependent: :destroy
  has_one :price_factor, dependent: :destroy
  has_one :vehicle_information, dependent: :destroy
  has_one :sold_vehicle, through: :vehicle_information
  has_one :promise_purchase,  dependent: :destroy
  has_one :inspection_purchase, dependent: :destroy
  has_one :discount, dependent: :destroy
  has_many :flat_rate_offers, through: :pawn_information
  has_one :coupon, through: :promise_purchase
  belongs_to :changing_user, class_name: 'User'
  belongs_to :ineligibility_condition_record, class_name: IneligibilityCondition, foreign_key: :ineligibility_condition
  belongs_to :pending_reason, primary_key: :code, foreign_key: :pending_reason_code

  delegate(*PawnInformation.sanitized_attribute_names, to: :pawn_information, allow_nil: true)
  delegate(*InspectionInformation.sanitized_attribute_names, to: :inspection_information, allow_nil: true)
  delegate(*PriceFactor.sanitized_attribute_names, to: :price_factor, allow_nil: true)
  delegate(*VehicleInformation.sanitized_attribute_names, to: :vehicle_information, allow_nil: true)
  delegate(*Promise.price_methods, to: :promise)
  delegate :amount, :description, to: :discount, prefix: true
  delegate :pawn_location, to: :pawn_information
  delegate :name, :partner_network, :g2g_converted, to: :pawn_location, prefix: true, allow_nil: true
  delegate :country, to: :pawn_location, allow_nil: true
  delegate :floor_plan_company, to: :adjustments, allow_nil: true
  delegate :paid_by_floor_plan?, to: :adjustments, allow_nil: true, prefix: true
  delegate :paid_by_buyer?, to: :adjustments, allow_nil: true

  accepts_nested_attributes_for :vehicle_information
  accepts_nested_attributes_for :pawn_information
  accepts_nested_attributes_for :discount, update_only: true

  def self.find_by_vnum(vnum)
    by_vnum(vnum).first
  end

  def self.find_by_universal_no(universal_no)
    by_universal_no(universal_no).first
  end

  scope :created_today, -> { where('promise_offers.created_at > ?', DateTime.current.beginning_of_day) }
  scope :promised, -> { where(promise_status: PURCHASED_STATES) }
  scope :pending,    -> { where(promise_status: 'pending') }
  scope :purchased_at, ->(time) { where(purchased_at: time) }
  scope :post_pending, -> { where('invalidated_at IS NOT NULL OR approved_at IS NOT NULL') }
  scope :unpurchased, -> { includes(:promise_purchase).where(promise_purchases: { id: nil }) }
  scope :with_purchase,    ->() { joins(:promise_purchase).readonly(false) }
  scope :without_purchase, ->() { joins('LEFT OUTER JOIN promise_purchases ON promise_purchases.promise_offer_id = promise_offers.id').readonly(false).where(promise_purchases: { promise_offer_id: nil }) }
  scope :left_lot,     ->() { where(left_lot: true) }
  scope :not_left_lot, ->() { where(left_lot: false) }
  scope :past_cutoff, -> { where('promise_offers.must_be_promised_at < ? AND promise_offers.must_be_promised_at IS NOT NULL', DateTime.current) }
  scope :not_past_cutoff, -> { where('promise_offers.must_be_promised_at > ? OR promise_offers.must_be_promised_at IS NULL', DateTime.current) }

  scope :by_vnum_and_universal_no, ->(sold_vehicle) {
    by_vnum(sold_vehicle.vnum)
      .by_universal_no(sold_vehicle.universal_no)
  }

  scope :by_make, ->(make) do
    includes(:vehicle_information).where(vehicle_information: { make: make })
  end

  scope :by_vnum, ->(vnum) do
    includes(:vehicle_information).where(vehicle_information: { vnum: vnum })
  end

  scope :by_badge_no, ->(badge_no) do
    includes(:pawn_information).where(pawn_information: { badge_no: badge_no })
  end

  scope :by_pawn_access_no, ->(pawn_access_no) do
    includes(:pawn_information).where(pawn_information: { pawn_access_no: pawn_access_no })
  end

  scope :by_agreement_no, ->(agreement_no) do
    includes(:pawn_information).where(pawn_information: { agreement_no: agreement_no })
  end

  scope :by_universal_no, ->(universal_no) do
    includes(:pawn_information).where(pawn_information: { universal_no: universal_no })
  end

  scope :by_location_initials, ->(location_initials) do
    includes(:pawn_information).where(pawn_information: { location_initials: location_initials })
  end

  scope :by_work_order_number, ->(work_order_number) do
    includes(:pawn_information).where(pawn_information: { work_order_number: work_order_number })
  end

  scope :by_seller_no, ->(seller_no) do
    includes(:pawn_information).where(pawn_information: { seller_no: seller_no })
  end

  scope :left_lot_or_paid_by_floor_plan, ->(left_lot, paid_by_floor_plan) do
    where <<-SQL, left_lot, paid_by_floor_plan
        promise_offers.left_lot = ?
        OR
        promise_offers.paid_by_floor_plan = ?
      SQL
  end

  scope :for_pawn_access_number, ->(filter_query_data) do
    includes(:pawn_information)
      .where("pawn_information.pawn_access_no = ? OR (pawn_information.pawn_access_no = '0' AND pawn_information.agreement_no IN (?))",
             filter_query_data.pawn_access_number,
             filter_query_data.agreement_numbers)
      .references(:pawn_information)
  end

  scope :for_agreement, ->(pawn_access_number) do
    includes(:pawn_information)
      .merge(PawnInformation.for_agreement(pawn_access_number))
      .references(:pawn_information)
  end

  def cancelled_purchase
    CancelledPurchase.where(promise_offer_id: id).last
  end

  def past_cutoff?
    must_be_promised_at.present? && must_be_promised_at.past?
  end

  def canadian_location?
    pawn_location_partner_network == PartnerNetwork::CA_ASTROHEIM
  end

  def not_past_cutoff?
    must_be_promised_at.nil? || must_be_promised_at.future?
  end

  def purchased?
    promise_purchase.present?
  end

  def limited_quantity?
    NewProductKind::DEFAULT_VOLUME_TIERS.keys.map(&:to_s).include?(limited_volume)
  end

  def pplv?
    limited_quantity? && percent_coverage.blank?
  end

  def part_of_agreement_group?
    group_code.present?
  end

  def value_protected
    buy_fee.to_f + vehicle_price_covered
  end

  def kind
    promise_purchase.kind if purchased?
  end

  def flat_rate_offer
    flat_rate_offers.find { |flat_rate_offer| flat_rate_offer.try(:country) == country }
  end

  def has_flat_rate_offer?
    valid_flat_rate_offer? && flat_rate_offer_batch.try(:approved?)
  end

  def valid_flat_rate_offer?
    flat_rate_offer.present?
  end

  def flat_rate_price
    flat_rate_offer.price if has_flat_rate_offer?
  end

  def flat_rate_offer_batch
    flat_rate_offer.try(:flat_rate_offer_batch)
  end

  def location_purchases_enabled?
    pawn_location.try(:purchases_enabled?)
  end

  def eligible_for_inspection?
    !has_inspection? && !purchased? && purchase_location_has_inspections?
  end

  def purchase_location_has_inspections?
    pawn_information && pawn_information.location_has_inspections?
  end

  def has_inspection_pricing?
    inspection_information.present? && inspection_information.inspection_only_price.present? && inspection_information.inspection_bundle_price.present?
  end

  def has_inspection?
    inspection_purchase.present?
  end

  def attributes_including_delegate_association_attributes
    attributes
      .merge(pawn_information.sanitized_attributes)
      .merge(inspection_information.sanitized_attributes)
      .merge(price_factor.sanitized_attributes)
      .merge(vehicle_information.sanitized_attributes)
  end

  def eligible_for_purchase?
    (ineligibility_condition == 0 || ineligibility_condition.nil?) &&
      !left_lot? &&
      !past_cutoff?
  end

  def ineligibility_notice
    ineligibility_condition_record.notice if ineligibility_condition_record
  end

  def mobile_ineligibility_notice
    return unless ineligibility_condition
    default_condition = IneligibilityCondition::INVALID_PRICING
    mobile_notices = I18n.t('ineligibility_conditions.mobile')
    mobile_notices[ineligibility_condition] || mobile_notices[default_condition]
  end

  def promise
    @promise ||= Promise.new(self)
  end

  def promise_price
    price = self[:promise_price]

    return unless price
    if price - coupon_amount < 0
      0
    else
      price - coupon_amount
    end
  end

  def price_in_cents
    Integer(promise_price * 100)
  end

  def discount_enabled?
    Settings.discounts_enabled
  end

  def discounted_promise_price
    return unless promise_price
    if discount_enabled? && discounted?
      discount_price
    else
      promise_price
    end
  end

  def rounded_discounted_price
    discounted_promise_price.try(:round)
  end

  def rounded_promise_price
    promise_price.try(:round)
  end

  def update_ods_attributes
    self.paid_by_floor_plan ||= paid_in_ods_by_floor_plan?
    self.left_lot ||= left_lot_in_ods? && !paid_by_floor_plan?
    save!
  end

  def payment_type
    adjustments.how_paid if adjustments.present?
  end

  def buy_net
    adjustments.buy_net if adjustments.present?
  end

  def fees
    if adjustments
      adjustments.fees
    else
      Sas::Fees.new
    end
  end

  def adjustments_description
    adjustments.description if adjustments.present?
  end

  def adjustments_buy_fee
    adjustments.buy_fee.to_f if adjustments.present?
  end

  def select_for_purchase
    if pending_reason_code.present? && pending_reason_code != 0
      pending_transition!
    else
      check_pending_conditions
    end

    promised_transition!
  end

  def finalize_pending(user)
    self.changing_user = user
    allow_pending
    save!
  end

  def promised?
    PURCHASED_STATES.include?(promise_status)
  end

  def charge_on_finalization?
    !seller_paid? && !bypass_charge? && !paid_by_floor_plan?
  end

  def sows_eligible?
    !pending? && sblu.present? && !pawn_location_g2g_converted
  end

  def g2gws_eligible?
    pawn_location_g2g_converted
  end

  def bsc_location?
    bsc_locations.include?(location_initials)
  end

  def bsc_locations
    @bsc_locations ||= Settings.bsc_locations || []
  end

  def discount_amount
    discount.try(:amount).to_f
  end

  def discounted?
    discount_amount > 0
  end

  def left_lot_in_ods?
    if pawn_information.present?
      vehicle_query_interface.vehicle_has_left_lot?
    else
      true
    end
  end

  def charge_present_in_ods?
    vehicle_query_interface.agreementshield_charged?
  end

  def purchase_location
    /(?<initials>[a-z]+)/i =~ universal_no

    PawnLocation.find_by_initials(initials.upcase) if initials.present?
  end

  private

  def vehicle_price_covered
    vehicle_purchase_price.to_f * (coverage_percentage / 100.0)
  end

  def coverage_percentage
    percent_coverage? ? percent_coverage : 100.0
  end

  def coupon_amount
    coupon ? coupon.amount : 0.0
  end

  def adjustments
    adjustments_request.adjustments
  end

  def adjustments_request
    @adjustments_request ||=
      Sas::VehicleAdjustmentsRequest.new(vnum, agreement_no).tap do |r|
        begin
          r.load
        rescue Sas::ApiError, Sas::VehicleAdjustmentsApiClient::VehicleNotFound
          # noop
        end
      end
  end

  def check_pending_conditions
    pending_conditions = PendingConditions.new(self)
    return unless pending_conditions.met?
    self.pending_reason_code = pending_conditions.code
    pending_transition!
  end

  def vehicle_query_interface
    @vehicle_query_interface ||= OdsQueryInterface.new(agreement_no, location_initials, universal_no, sale_no, lane_no, run_no)
  end

  def paid_in_ods_by_floor_plan?
    if pawn_information.present?
      vehicle_query_interface.vehicle_paid_by_floor_plan?
    else
      false
    end
  end

  def set_ineligibility_for_invalid_pricing
    self.ineligibility_condition = IneligibilityCondition::INVALID_PRICING if eligible? && prices_missing?
  end

  def prices_missing?
    [promise_250, promise_500, additional_day].any?(&:nil?)
  end

  def discount_price
    discounted_price = promise_price - discount_amount
    discounted_price < 0 ? 0 : discounted_price
  end
end
