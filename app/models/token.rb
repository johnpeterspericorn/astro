# encoding: utf-8

class Token < ActiveRecord::Base
  def request_token(endpoint, username, password)
    endpoint_url = endpoint + '/oauth2/token'
    method = :post
    userpwd = username + ':' + password
    headers = { 'Content-Type' => 'application/json', 'grant_type' => 'client_credentials' }
    request = Typhoeus::Request.new(endpoint_url, method: method, userpwd: userpwd, headers: headers, verbose: true).run
    JSON.parse(request.response_body)['accessToken']
  end

  def persist_token(service_name, endpoint, username, password)
    Token.where(service_name: service_name).delete_all
    access_token = request_token(endpoint, username, password)
    update_attributes(
      service_name: service_name,
      access_token: access_token,
      expires_at: (Time.zone.now + 22.hours)) if access_token.present?
    access_token
  end

  def remove_expired_token(service_name)
    Token.where('service_name = ? OR expires_at < ?', service_name, Time.zone.now).destroy_all
  end

  def valid_token(service_name)
    Token.where('service_name = ? AND expires_at > ?', service_name, Time.zone.now)
  end

  def generate_token(service_name, endpoint, username, password)
    valid_token = valid_token(service_name)
    valid_token.present? ? valid_token[0][:access_token] : persist_token(service_name, endpoint, username, password)
  end
end
