# encoding: utf-8
class EmailTracking < ActiveRecord::Base
  self.table_name = 'email_tracking'

  after_create :copy_sent_info
  after_update :copy_viewed_at_info

  def copy_viewed_at_info
    extract_action.update_attributes(email_viewed_at: letter_opened)
  end

  def copy_sent_info
    extract_action.update_attributes(date_sent: letter_sent)
  end

  private

  def extract_action
    action_type_id = unique_code.split('_').first
    ActionType.find(action_type_id)
  end
end
