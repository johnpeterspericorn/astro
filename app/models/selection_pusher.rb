# encoding: utf-8
class SelectionPusher < Struct.new(:updater)
  DASHBOARD_CHANNEL = 'promise_offers'.freeze

  def push_to_dashboard(event)
    Pusher.trigger(DASHBOARD_CHANNEL, event, attributes_to_push.to_json)
  rescue Pusher::Error => e
    Rails.logger.error e.to_s
  end

  private

  def attributes_to_push
    promise_purchase = updater.promise_purchase
    updater.offer_attributes
           .merge(vnum: promise_purchase.promise_offer.vnum)
  end
end
