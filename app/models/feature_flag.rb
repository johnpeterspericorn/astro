# encoding: utf-8
class FeatureFlag
  YAML_FILE_PATH = Rails.root.join('config', 'feature_flags.yml')

  def self.each(&block)
    yaml.each(&block)
  end

  def self.yaml
    YAML.load_file(YAML_FILE_PATH).with_indifferent_access
  end
end
