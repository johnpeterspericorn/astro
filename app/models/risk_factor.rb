# encoding: utf-8
class RiskFactor < ActiveRecord::Base
  DEFAULT_RISK_FACTOR_MODIFIER = 0.0

  has_many :price_ranges, -> { order(:vehicle_code) }

  def self.default
    find_by_risk_modifier(DEFAULT_RISK_FACTOR_MODIFIER)
  end
end
