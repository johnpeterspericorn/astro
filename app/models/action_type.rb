# encoding: utf-8
class ActionType < ActiveRecord::Base
  default_scope { order('date_sent DESC') }

  scope :for_agreement_no, lambda { |agreement_no|
    where(agreement_no: agreement_no)
  }

  scope :for_action_type, lambda { |action_type|
    where(action_type_name: action_type.class.name, type_id: action_type.id)
  }

  def self.process(action_type, state = '', letter_sent = '', emails, parameters)
    ActionType.create!(agreement_no: action_type.agreement_no, action_type_name: action_type.class.name, type_id: action_type.id, status: state, date_sent: letter_sent, emails: emails, parameters: parameters)
  end
end
