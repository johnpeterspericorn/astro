# encoding: utf-8
class Quote
  include ActiveModel::SerializerSupport

  delegate :low_valuation, :average_valuation, :high_valuation, :seller_no, :mobile_ineligibility_notice, to: :presale_vehicle
  delegate :flat_rate, :price, to: :quotes, allow_nil: true
  delegate :country, to: :presale_vehicle

  attr_reader :buyer_number

  def initialize(presale_vehicle, buyer_number, given_price)
    @presale_vehicle = presale_vehicle
    @buyer_number  = buyer_number.to_i
    @given_price   = given_price
  end

  def miles
    Array(quotes.try(:miles))
  end

  def days
    Array(quotes.try(:days))
  end

  def quote_price
    (given_price || average_valuation).to_i
  end

  private

  attr_reader :presale_vehicle, :given_price

  delegate :quotes, to: :quote_request

  def quote_request
    @quote_request ||=
      begin
        Sas::QuoteRequest.new(
          vnum:               presale_vehicle.vnum,
          seller_5m:         seller_no,
          buyer_5m:          buyer_number,
          sale_price:        quote_price,
          location_initials: presale_vehicle.location_initials
        ).tap(&:load)
      rescue Sas::QuoteResponse::InvalidQuoteType
        QuotesLogger.error 'Failure: invalid quote type'
        Naught.build.new
      end
  end
end
