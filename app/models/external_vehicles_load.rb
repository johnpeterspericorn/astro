# encoding: utf-8
class ExternalVehiclesLoad < ActiveRecord::Base
  include AASM
  before_create :default_values

  scope :unposted, ->() { where.not(status: 'posted') }
  scope :rejected, ->() { where(status: 'rejected') }
  scope :unapproved, ->() { where(status: 'unapproved') }

  belongs_to :external_vehicles_batch
  belongs_to :user
  belongs_to :approvnumg_user, class_name: 'User'
  belongs_to :rejecting_user, class_name: 'User'

  delegate :email, to: :user

  def default_values
    self.sale_no ||= 0
    self.run_no ||= 0
    self.lane_no ||= 0
  end

  def process_approve
    if unapproved? || rejected?
      approve!
      NetworkPlusPostJob.new(id).post
      external_vehicles_batch.update_status
      true
    else
      errors.add(:base, 'Unable to approve')
      return false
    end
  end

  def process_reject(params)
    if unapproved?
      reject!
      external_vehicles_batch.update_status
    else
      errors.add(:base, 'Unable to reject')
      return false
    end
    update_attributes(rejection_comment: params[:rejection_comment]) if params[:rejection_comment].present?
    true
  end

  aasm column: :status do
    state :unapproved, initial: true
    state :approved
    state :rejected
    state :accepted
    state :posted
    state :unposted

    event :approve do
      transitions from: :unapproved, to: :approved
      transitions from: :rejected, to: :approved
    end

    event :reject do
      transitions from: :unapproved, to: :rejected
    end

    event :accept do
      transitions from: :posted, to: :accepted
    end

    event :post do
      transitions from: :approved, to: :posted
    end

    event :unpost do
      transitions from: :approved, to: :unposted
    end
  end
end
