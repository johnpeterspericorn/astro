# encoding: utf-8
class Payment < ActiveRecord::Base
  has_one :payment_response
  has_one :promise_purchase, through: :payment_response
  has_one :cancelled_purchase, through: :payment_response
  has_one :promise_offer, through: :promise_purchase
  has_many :payment_adjustment_fees

  delegate :country, to: :promise_offer

  validates :order_id, :authorization_code, presence: true

  scope :for_vnum_number, ->(vnum) do
    joins(promise_purchase: { promise_offer: :vehicle_information })
      .where(vehicle_information: { vnum: vnum })
  end

  scope :for_agreement_number, ->(agreement_number) do
    joins(promise_purchase: { promise_offer: :pawn_information })
      .where(pawn_information: { agreement_no: agreement_number })
  end

  scope :for_yesterday, -> do
    for_date(Date.yesterday)
  end

  scope :for_date, ->(date) do
    bod = date.beginning_of_day
    eod = date.end_of_day

    where(created_at: bod..eod)
  end

  scope :created_at, ->(date) do
    where(created_at: date)
  end

  def cancelled?
    cancelled_purchase.present?
  end

  def self.send_daily_report
    the_subject = I18n.t('payments.email.subject.daily_report', today: Time.zone.today.to_s(:number))
    PaymentsMailer.report(for_yesterday.to_a, subject: the_subject).deliver_later
  end
end
