# encoding: utf-8
class EnvironmentConfig
  def self.fetch(*args)
    ENV.to_hash.fetch(*args)
  end
end
