# encoding: utf-8
class ReturnsByAgreement
  def initialize(returns)
    @returns = Array(returns)
  end

  def each(returns_data_class = ReturnsDashboardData)
    agreement_grouped_returns.each do |agreement, returns|
      yield(agreement, returns_data_class.new(returns))
    end
  end

  private

  attr_reader :returns

  def agreement_grouped_returns
    returns
      .group_by(&:agreement_information)
  end
end
