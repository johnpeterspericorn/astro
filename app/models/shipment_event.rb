# encoding: utf-8
require 'ostruct'

class ShipmentEvent
  TRACKER_UPDATED_EVENT = 'tracker.updated'.freeze

  def initialize(description, result)
    @description = description
    @result      = OpenStruct.new(result)
  end

  delegate :shipment_id, :status, to: :result

  def tracker_update?
    description == TRACKER_UPDATED_EVENT
  end

  private

  attr_reader :description, :result
end
