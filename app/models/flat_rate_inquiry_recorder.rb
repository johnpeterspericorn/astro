# encoding: utf-8
class FlatRateInquiryRecorder < Struct.new(:flat_rate, :pawn_access_no, :miles_selected, :psi_eligible, :user, :palv)
  def self.record(flat_rates, pawn_access_no, miles_selected, psi_eligible, user, palv)
    flat_rates.map do |flat_rate|
      recorder = new(flat_rate, pawn_access_no, miles_selected, psi_eligible, user, palv)
      recorder.create_flat_rate_inquiry
    end.compact
  end

  def create_flat_rate_inquiry
    flat_rate_inquiry = FlatRateInquiry.new(attributes)

    if flat_rate_inquiry.save
      flat_rate_inquiry
    else
      Rails.logger.error "FlatRateInquiry Failed To Create: Error message: #{flat_rate_inquiry.errors}"
      return
    end
  end

  private

  def attributes
    {
      user: user,
      psi_eligible: psi_eligible,
      palv: palv,
      miles_selected: miles_selected,
      agreement_no: flat_rate.agreement_no,
      target_price: flat_rate_attribute.target_price,
      pawn_access_no: pawn_access_no,
      agreement_name: buyer_authorization.get_agreement_name,
      agreement_phone: buyer_authorization.get_agreement_phone,
      agreement_email: buyer_authorization.get_agreement_email
    }
  end

  def flat_rate_attribute
    flat_rate.flat_rate_attribute
  end

  def buyer_authorization
    @authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_no)
  end
end
