# encoding: utf-8
require 'csv'

class OfferCsv
  delegate :to_s, to: :csv

  private

  def csv
    @csv ||= CSV.generate do |csv|
      csv << self.class.column_names
      @offers.each do |offer|
        csv << self.class.column_names.map do |column|
          offer.public_send column
        end
      end
    end
  end
end
