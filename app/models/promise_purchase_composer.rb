# encoding: utf-8
class PromisePurchaseComposer
  attr_reader :pawn_access_number, :email, :purchasing_user, :purchase_queries

  def initialize(attributes)
    @pawn_access_number = attributes[:pawn_access_number]
    @email                 = attributes[:email]
    @purchasing_user       = attributes[:purchasing_user]
    @purchase_queries      = attributes[:purchase_queries]
  end

  def complete_purchases
    apply_coupons
    confirm_promise_purchases
    confirm_inspection_purchases if inspections_enabled?
    charge_promise_purchases   if charges_enabled?
    create_sows_purchases
    mail_confirmation
  end

  def promise_purchases
    @promise_purchases ||= if inspections_enabled?
                               unbundled_promise_purchases
                             else
                               new_promise_purchases
                             end.map(&:decorate)
  end

  def inspection_purchases
    @inspection_purchases ||= if inspections_enabled?
                                unbundled_inspection_purchases
                              else
                                new_inspection_purchases
                              end.map(&:decorate)
  end

  def bundled_purchases(reload = false)
    if inspections_enabled?
      bundled_offer_ids.map do |id|
        BundledPurchase.new(
          promise_purchase:  new_promise_purchases(reload).find  { |purchase| purchase.promise_offer_id == id },
          inspection_purchase: new_inspection_purchases(reload).find { |purchase| purchase.promise_offer_id == id }
        )
      end
    else
      []
    end
  end

  def transaction_id
    @transaction_id ||= TransactionIdGenerator.new(pawn_access_number).generate
  end

  private

  def apply_coupons
    new_promise_purchases.each do |purchase|
      coupon_code = purchase_queries.find { |query| query.vnum == purchase.vnum }.coupon_code
      coupon = Coupon.find_by_code(coupon_code.try(:upcase))
      purchase.coupon = coupon if coupon
    end
  end

  def new_promise_purchases(reload = false)
    if @new_promise_purchases.nil? || reload
      @new_promise_purchases = promise_purchase_builder.changed_purchases_without_existing_inspection
    end
    update_promise_kind(@new_promise_purchases)
  end

  def unbundled_promise_purchases(reload = false)
    if @unbundled_promise_purchases.nil? || reload
      @unbundled_promise_purchases = promise_purchase_builder.unbundled_promise_purchases
    end
    update_promise_kind(@unbundled_promise_purchases)
  end

  def new_inspection_purchases(reload = false)
    if @new_inspection_purchases.nil? || reload
      @new_inspection_purchases = inspection_purchase_builder.inspection_purchases
    end
    update_promise_kind(@new_inspection_purchases)
  end

  def unbundled_inspection_purchases(reload = false)
    if @unbundled_inspection_purchases.nil? || reload
      @unbundled_inspection_purchases = inspection_purchase_builder.unbundled_inspection_purchases
    end
    update_promise_kind(@unbundled_inspection_purchases)
  end

  def new_chargable_promise_purchases
    new_promise_purchases.reject do |p|
      p.promise_offer.paid_by_floor_plan? || p.promise_offer.pending? || p.promise_offer.bypass_charge?
    end
  end

  def new_floor_plan_promise_purchases
    new_promise_purchases.select { |p| p.promise_offer.paid_by_floor_plan? }
                           .reject(&:offer_pending?)
  end

  def charge_promise_purchases
    new_chargable_promise_purchases.map(&:promise_offer).each do |offer|
      promise_charge = PromiseCharge.new(offer)
      PromiseChargeJob.enqueue(promise_charge)
    end
  end

  def create_sows_purchases
    new_promise_purchases.each do |purchase|
      offer = purchase.promise_offer
      PurchaseJobBuilder.enqueue_job(offer)
    end
  end

  def all_promise_purchases
    @all_promise_purchases ||= promise_purchase_builder.promise_purchases
    update_promise_kind(@all_promise_purchases)
  end

  def promise_purchase_builder
    @promise_purchase_builder ||= PromisePurchaseBuilder.with_filtered_queries(purchasing_user, purchase_queries)
  end

  def confirm_promise_purchases
    set_transaction_ids_on_purchases
    PromiseSelectionRecorder.record_selected(new_promise_purchases, email)
  end

  def inspection_purchase_builder
    @inspection_purchase_builder ||= InspectionPurchaseBuilder.new(purchase_queries)
  end

  def confirm_inspection_purchases
    set_transaction_ids_on_inspections
  end

  def mail_confirmation
    bundled_purchases(true).reject! { |record| !record.persisted? }
    unbundled_promise_purchases(true).reject! { |record| !record.persisted? }

    pending_bundled, nonpending_bundled = bundled_purchases.map(&:promise_purchase).partition(&:offer_pending?)
    pending_unbundled, nonpending_unbundled = unbundled_promise_purchases.partition(&:offer_pending?)

    mailer_params = {
      user_email:           email,
      inspection_purchases: unbundled_inspection_purchases(true)
    }

    PromisePurchaseMailer.purchase_confirmation(mailer_params.merge(
                                                    bundled_purchases: nonpending_bundled,
                                                    promise_purchases: nonpending_unbundled
    )).deliver_later

    PromisePurchaseMailer.pending_confirmation(
      pending_bundled,
      pending_unbundled,
      email
    ).deliver_later
  end

  def set_transaction_ids_on_purchases
    new_promise_purchases.each do |purchase|
      purchase.update_attributes(transaction_id: transaction_id)
    end
  end

  def set_transaction_ids_on_inspections
    new_inspection_purchases.each do |inspection|
      inspection.update_attributes(transaction_id: transaction_id)
    end
  end

  def bundled_offer_ids
    promise_purchase_offer_ids  = new_promise_purchases.map(&:promise_offer_id)
    inspection_purchase_offer_ids = new_inspection_purchases.map(&:promise_offer_id)
    promise_purchase_offer_ids & inspection_purchase_offer_ids
  end

  def charges_enabled?
    Settings.charges_enabled
  end

  def inspections_enabled?
    Settings.inspections_enabled
  end

  def update_promise_kind(purchases)
    purchases.each { |purchase| purchase.kind = purchase.promise_offer.preselected_promise_kind }
  end
end
