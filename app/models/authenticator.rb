# encoding: utf-8
class Authenticator
  attr_reader :user
  attr_reader :error
  attr_accessor :access_token

  def initialize(login, pass)
    user = User.find_for_database_authentication(username: login)
    if user && user.valid_password?(pass)
      @user = user
      return
    end

    user = User.authenticate_with_astroheim_auth(username: login, password: pass)
    if user
      @user = user
    else
      @error = 'Bad username/password combination'
    end
  end

  def successful?
    !@user.nil?
  end
end
