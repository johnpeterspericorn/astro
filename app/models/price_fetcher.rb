# encoding: utf-8
class PriceFetcher < Struct.new(:offer)
  def pricing_information
    response = {
      promise_250: offer.promise_250.round,
      promise_500: offer.promise_500.round,
      additional_day: offer.additional_day.round
    }

    response.merge!(inspection_only_pricing) if has_inspection_only_pricing?
    response.merge!(inspection_bundle_pricing) if has_inspection_bundle_pricing?
    response
  end

  private

  def inspection_information
    offer.inspection_information
  end

  def has_inspection?
    inspection_information.present?
  end

  def has_inspection_only_pricing?
    has_inspection? && inspection_information.inspection_only_price.present?
  end

  def has_inspection_bundle_pricing?
    has_inspection? && inspection_information.inspection_bundle_price.present?
  end

  def inspection_only_pricing
    { inspection_only_price: inspection_information.inspection_only_price.round }
  end

  def inspection_bundle_pricing
    { inspection_bundle_price: inspection_information.inspection_bundle_price.round }
  end
end
