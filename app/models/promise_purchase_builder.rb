# encoding: utf-8
class PromisePurchaseBuilder
  def self.with_filtered_queries(user, purchase_queries)
    queries = purchase_queries
              .select { |query| query.promise_selected? && !query.inspection_only? }
    new(user, queries)
  end

  def initialize(user, purchase_queries, purchase_factory = PromisePurchase)
    @user = user
    @purchase_queries = purchase_queries
    @purchase_factory = purchase_factory
  end

  def promise_purchases
    purchases
  end

  def changed_purchases_without_existing_inspection
    purchases.select { |purchase| purchase.changed? && !purchase.has_inspection? }
  end

  def unbundled_promise_purchases
    queries = purchase_queries.reject(&:inspection_selected?)
    purchases(queries)
  end

  private

  attr_reader :user, :purchase_queries, :purchase_factory

  def purchases(queries = purchase_queries)
    queries
      .map { |query| purchase_factory.from_options(query.vnum, query.miles, query.days, user) }
      .compact
  end
end
