# encoding: utf-8
class ReturnInvoiceFilter < Struct.new(:current_user, :params)
  def promise_purchases
    @promise_purchases ||= filter.search
                                   .decorate
                                   .select(&:displayable_for_return?)
                                   .sort_by(&:priority)
  end

  def any_eligible_returns?
    @any_eligible_returns ||= promise_purchases.map(&:eligible_for_return?).any?
  end

  def valid_filter?
    attempted? && valid_search_options?
  end

  def attempted?
    filter.present?
  end

  delegate :valid_search_options?, to: :filter

  def error_message
    filter.error_message if attempted?
  end

  private

  def filter
    @return_invoice_filter ||= [
      ReturnInvoiceFilter::AgreementNumberFilter.new(params),
      ReturnInvoiceFilter::VinNumberFilter.new(params),
      ReturnInvoiceFilter::PawnAccessNumberFilter.new(params), # this must be last because AA# is in all params
    ].find(&:attempted?)
  end
end
