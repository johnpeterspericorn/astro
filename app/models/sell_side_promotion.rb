class SellSidePromotion
  def initialize(promise_purchase)
    @promise_purchase = promise_purchase
    @group_code = @promise_purchase.group_code
    @agreement_no = @promise_purchase.agreement_no
  end

  def return_count
    @return_count ||= ReturnInvoice.for_seller_paid(true).for_agreement_group_code(@agreement_no, @group_code).count
  end

  def purchase_count
    @purchase_count ||= PromisePurchase.for_seller_paid(true).for_agreement_group_code(@agreement_no, @group_code).count
  end

  def promo_code
    @group_code
  end

  def valid?
    return_count < allowed_returns
  end

  def allowed_returns
    [10, 3*(purchase_count/10 + 1)].min
  end
end
