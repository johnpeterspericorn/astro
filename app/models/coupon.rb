# encoding: utf-8
class Coupon < ActiveRecord::Base
  has_paper_trail
  CODE_LENGTH = 6

  STATUSES = {
    'consumed' => true,
    'unconsumed' => false
  }.freeze

  before_save { code.upcase! }

  belongs_to :promise_purchase
  has_one :coupon_batch, through: :batch_coupon
  has_one :batch_coupon

  delegate :name, to: :coupon_batch, prefix: true, allow_nil: true

  validates :amount, numericality: true
  validates :expiration_date, presence: true
  validates :country, presence: true

  scope :for_status, ->(status) do
    status ? where.not(promise_purchase_id: nil) : where(promise_purchase_id: nil)
  end

  scope :for_batch_name, ->(name) do
    joins(:coupon_batch)
      .where(coupon_batches: { name: name })
  end

  def self.new_with_code(*args)
    new(*args).tap do |coupon|
      # The arg to hex is the number of bytes in the result (1 byte = 2 chars)
      # http://ruby-doc.org/stdlib-2.1.2/libdoc/securerandom/rdoc/SecureRandom.html#method-c-hex
      coupon.code = SecureRandom.hex(CODE_LENGTH / 2)
    end
  end

  def consumed?
    promise_purchase_id.present?
  end

  def expired?
    expiration_date.past?
  end

  def restricted?(purchase_country)
    convert(purchase_country) != convert(country)
  end

  def convert(name)
    ApplicationController.helpers.countrify(name).alpha3
  end
end
