# encoding: utf-8
class OriginatingPurchasesReport
  delegate :presigned_url, to: :s3_file
  attr_reader :return_invoices
  include Rails.application.routes.url_helpers

  def initialize(return_invoices = [], pawn_location = [], date_from = nil, date_to = nil)
    @return_invoices = return_invoices
    @pawn_location = pawn_location
    @date_from = date_from
    @date_to = date_to
  end

  def create_and_upload
    create
    upload
  end

  def self.weekly_report
    date_from = Date.current.at_beginning_of_week
    date_to = Date.current.end_of_day

    return_invoices_with_aa = ReturnInvoice.unscoped.by_purchase_location(date_from, date_to)
    return_invoices_with_aa.each do |invoice|
      report = new(invoice.last, invoice.first, date_from, date_to)
      report.create_and_upload
      report.send_mail
    end
  end

  def send_mail
    ReturnInvoiceMailer.weekly_vra_report_originated_pawn_location(self, @pawn_location.accounting_email, @date_from, @date_to).deliver_now
  end

  private

  def create
    require 'csv'
    CSV.open(local_file_path, 'w') do |csv|
      csv << ['VRA#', 'VNUM', 'Sale No', 'Lane No', 'Run No', 'DS Fee', 'VRA link', 'Cancelled?', 'Order ID', 'Credit Card Number (last 4 digits)', 'Payment Date']
      @return_invoices.each do |return_invoice|
        csv << [return_invoice.id, return_invoice.vnum, return_invoice.promise_purchase.sale_no, return_invoice.promise_purchase.lane_no, return_invoice.promise_purchase.run_no, return_invoice.promise_purchase.promise_price, return_certificate_link(return_invoice), return_invoice.cancelled?, order_id(return_invoice), credit_card_number(return_invoice), payment_date(return_invoice)]
      end
    end
  end

  def order_id(return_invoice)
    return_invoice.promise_purchase.payments.first.try(:order_id)
  end

  def credit_card_number(return_invoice)
    return_invoice.promise_purchase.payments.first.try(:credit_card_number)
  end

  def payment_date(return_invoice)
    return_invoice.promise_purchase.payments.first.try(:created_at)
  end

  def upload
    s3_file.upload(local_file_path)
  end

  def s3_file
    @s3_file ||= S3File.new("reports/#{file_name}")
  end

  def file_name
    @file_name ||= "pawn_controller_report_#{@pawn_location.initials}_#{Time.current.utc.strftime('%Y%m%d%H%M%S')}.csv"
  end

  def local_file_path
    "/tmp/#{file_name}"
  end

  def return_certificate_link(return_invoice)
    host = ActionMailer::Base.default_url_options[:host]
    port = ActionMailer::Base.default_url_options[:port]
    if return_invoice.canadian_return?
      return_invoice.slug.present? ? Rails.application.routes.url_helpers.obfuscated_return_certificate_url(slug: return_invoice.slug, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(return_invoice.id), host: host, port: port) : Rails.application.routes.url_helpers.return_certificate_url(return_invoice_id: return_invoice.id, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(return_invoice.id), host: host, port: port)
    else
      return_invoice.slug.present? ? Rails.application.routes.url_helpers.obfuscated_return_certificate_url(slug: return_invoice.slug, host: host, port: port) : Rails.application.routes.url_helpers.return_certificate_url(return_invoice_id: return_invoice.id, host: host, port: port)
    end
  end
end
