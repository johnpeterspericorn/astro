# encoding: utf-8
class CouponVerifier
  attr_reader :errors

  def initialize(vehicle_params)
    @vehicle_params = vehicle_params
    @errors = []
  end

  def verified?
    verify
    errors.empty?
  end

  private

  attr_reader :vehicle_params

  def coupon_codes
    vehicle_params.map do |_, opts|
      opts[:coupon_code] unless opts[:coupon_code].blank?
    end.compact
  end

  def verify
    coupon_codes.each do |coupon_code|
      coupon = Coupon.find_by_code(coupon_code.upcase)
      verify_exists(coupon, coupon_code)
      verify_consumed(coupon)
      verify_expired(coupon)
      verify_country if coupon
    end
  end

  def verify_exists(coupon, coupon_code)
    errors << I18n.t('coupons.missing_error', code: coupon_code) unless coupon
  end

  def verify_consumed(coupon)
    return unless coupon
    errors << I18n.t('coupons.consumed_error', code: coupon.code) if coupon.consumed?
  end

  def verify_expired(coupon)
    return unless coupon
    errors << I18n.t('coupons.expired_error', code: coupon.code) if coupon.expired?
  end

  def verify_country
    vehicle_params.map do |key, value|
      if value[:coupon_code].present? && find_country_for_vehicle(key) != find_country_for_coupon(value[:coupon_code])
        errors << I18n.t('coupons.show.coupon_codes_invalid_error')
        break
      end
    end
  end

  def find_country_for_coupon(code)
    Coupon.find_by_code(code.upcase).country
  end

  def find_country_for_vehicle(vnum)
    VehicleInformation.find_by_vnum(vnum).country
  end
end
