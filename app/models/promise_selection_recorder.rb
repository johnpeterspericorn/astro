# encoding: utf-8
class PromiseSelectionRecorder < Struct.new(:promise_purchase, :email)
  def self.record_selected(promise_purchases, email = nil)
    promise_purchases.each do |promise_purchase|
      updater = new(promise_purchase, email)
      updater.record_selected

      SelectionPusher.new(updater).push_to_dashboard('update_selected')
    end
  end

  def record_selected
    promise_purchase.save!
    promise_purchase.reload

    promise_offer.update_attributes!(offer_attributes)
    promise_offer.select_for_purchase

    validate_recordings(promise_offer, promise_purchase)
  end

  def offer_attributes
    {
      miles_selected:     promise_purchase.miles_selected,
      days_selected:      promise_purchase.additional_days + Promise::BASE_DURATION,
      promise_price:    promise_purchase.calculated_promise_price,
      emails: email
    }.select { |_k, v| v.present? }
  end

  private

  def promise_offer
    offer = promise_purchase.promise_offer

    if offer.respond_to?(:source)
      offer.source
    else
      offer
    end
  end

  def validate_recordings(promise_offer, promise_purchase)
    validate_recorded_offer(promise_offer, promise_purchase)
    validate_recorded_purchase(promise_offer, promise_purchase)
  end

  def validate_recorded_offer(promise_offer, promise_purchase)
    promise_offer = promise_offer.reload
    InfrastructureMailer.bad_offer(promise_offer, promise_purchase).deliver_later if promise_offer.promised? && (promise_offer.promised_at.nil? || promise_offer.promise_purchase.nil?)
  end

  def validate_recorded_purchase(promise_offer, promise_purchase)
    InfrastructureMailer.bad_purchase(promise_offer, promise_purchase).deliver_later if promise_purchase.promise_offer.nil?
  end
end
