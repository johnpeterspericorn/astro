# encoding: utf-8
class SalesLocations
  def initialize(sales)
    @sales = Array(sales)
  end

  def each(sales_data_class = SalesData)
    location_grouped_sales.each do |location, sales|
      sales_data = sales_data_class.new(sales)
      yield(location, sales_data)
    end
  end

  def total_sales(sales_data_class = SalesData)
    sales_data_class.new(sales) unless sales.blank?
  end

  private

  attr_reader :sales

  def location_grouped_sales
    sales.group_by(&:pawn_location)
  end
end
