# encoding: utf-8
require 'ostruct' # NOTE: added for "Stubbing" of Astroheim auth.  authN classes inheriting from this will need to override `#get_agreement_contact_info`
class BuyerAuthorization
  def initialize(options = {})
  end

  def get_pawn_access_number
    nil
  end

  def get_agreement_numbers
    []
  end

  def get_agreement_numbers_to_names
    {}
  end

  def get_agreementship_name_by_agreement_number(agreement_number)
    get_agreement_numbers_to_names[agreement_number]
  end

  def get_agreementship_addr
    OpenStruct.new
  end

  def get_agreement_contact_info
    OpenStruct.new(name: '', phone: '', email: '')
  end

  def get_agreement_name
    get_agreement_contact_info.name
  end

  def get_agreement_phone
    get_agreement_contact_info.phone
  end

  def get_agreement_email
    get_agreement_contact_info.email
  end

  def has_contact_details?
    true # TODO: fix this in #53662405
  end
end
