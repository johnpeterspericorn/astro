# encoding: utf-8
class Transaction < Hashie::Dash
  include Hashie::Extensions::Dash::PropertyTranslation
  PURCHASE_PROTECT_LIMITED_VOLUME = 'pplv'.freeze

  property :purchased_at, transform_with: ->(v) { DateTime.parse(v).utc }
  property :product_name
  property :promise_purchase_id, transform_with: ->(v) { v.to_i }
  property :days_selected, transform_with: ->(v) { v.to_i }
  property :miles_selected, transform_with: ->(v) { v.to_i }
  property :days_left
  property :expires_at, transform_with: ->(v) { DateTime.parse(v).utc }
  property :vnum
  property :additional_days, transform_with: ->(v) { v.to_i }
  property :year, transform_with: ->(v) { v.to_i }
  property :make
  property :model
  property :odometer, transform_with: ->(v) { v.to_i }
  property :location_initials
  property :location_name
  property :purchase_price, transform_with: ->(v) { v.to_i }
  property :limited_volume, transform_with: ->(v) { NewProductKind::DEFAULT_VOLUME_TIERS.keys.map(&:to_s).include?(v) }
  property :obfuscated_id
  property :return_invoice_id
  property :bill_of_sale_id

  class << self
    def by_agreement_number(attributes = {})
      days_as_text = attributes[:days].to_i.days.ago
      days_left_for_promise = attributes[:days_left].to_i if attributes[:days_left]
      sql = <<-SQL
        SELECT promise_purchases.promised_at as purchased_at,
               promise_purchases.id as promise_purchase_id,
               promise_purchases.kind as product_name,
               promise_offers.days_selected  as days_selected,
               promise_purchases.promised_at + interval '1' day * (promise_offers.days_selected) as expires_at,
               promise_purchases.additional_days,
               promise_purchases.miles_selected,
               promise_offers.limited_volume,
               vehicle_information.vnum as vnum,
               vehicle_information.year as year,
               vehicle_information.make as make,
               vehicle_information.model as model,
               vehicle_information.odometer_reading as odometer,
               pawn_information.location_initials as location_initials,
               pawn_information.vehicle_purchase_price as purchase_price,
               return_invoices.id as return_invoice_id,
               return_invoices.slug as obfuscated_id,
               bill_of_sales.id as bill_of_sale_id
        FROM promise_purchases
        inner join  vehicle_information on promise_purchases.promise_offer_id = vehicle_information.promise_offer_id
        inner join pawn_information  on pawn_information.promise_offer_id = vehicle_information.promise_offer_id
        inner join promise_offers on promise_offers.id = promise_purchases.promise_offer_id
        left join return_invoices on return_invoices.promise_purchase_id = promise_purchases.id
        left join bill_of_sales on bill_of_sales.return_invoice_id = return_invoices.id
        WHERE pawn_information.agreement_no = :agreement_number
        AND promise_purchases.promised_at >= :days
        #{filter_query(attributes)}
        ORDER BY purchased_at DESC
        OFFSET :offset LIMIT :limit
      SQL

      filter_values = {
        agreement_number: attributes[:agreement_number].to_s,
        days: days_as_text,
        location_initial: attributes[:location_initial].to_s,
        days_left: days_left_for_promise,
        offset: attributes[:offset].to_i,
        limit: attributes[:limit].to_i
      }
      relation = ActiveRecord::Base.send :sanitize_sql_array, [sql, filter_values]
      records = ActiveRecord::Base.connection.select_all(relation)
      records.map { |r| new r.symbolize_keys }
    end

    def filter_query(attributes = {})
      conditions = ''
      conditions << "AND (promise_purchases.promised_at + INTERVAL '1' DAY * ( 1 + promise_offers.days_selected)) >= NOW() " if attributes[:active]
      conditions << 'AND pawn_information.location_initials = :location_initial ' if attributes[:location_initial]
      conditions << "AND (DATE(promise_purchases.promised_at + INTERVAL '1' DAY * (1 + promise_offers.days_selected)) - CURRENT_DATE) <= :days_left " if attributes[:days_left]

      if attributes[:returned]
        conditions << 'AND return_invoices.id is not NULL '
      elsif attributes[:returned] == false
        conditions << 'AND return_invoices.id is NULL '
      end
      conditions
    end
  end

  def initialize(*args)
    super
    days_left = ((1 + days_selected).days.since(purchased_at).to_date - DateTime.now.utc).ceil
    days_left = 0 if days_left < 0
    location = PawnLocation.find_by_initials location_initials
    self.product_name = PURCHASE_PROTECT_LIMITED_VOLUME if limited_volume && product_name == 'purchase_protect'
    self.product_name = PromisePurchase::BRANDED_KINDS[product_name]
    update_attributes!(days_left: days_left,
                       location_name: location.name, product_name: product_name)
  end
end
