# encoding: utf-8
require 'column_conversions'

class PaymentForm
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ColumnConversions

  ATTRIBUTES = [
    :user,
    :token,
    :promise_purchase,
    :send_receipt_confirmation,
    :return_fee
  ].freeze

  attr_reader(*ATTRIBUTES)

  validates(
    :user,
    :promise_purchase,

    presence: true
  )

  def initialize(attributes = {})
    ATTRIBUTES.each do |attr|
      instance_variable_set "@#{attr}", attributes[attr]
    end
  end

  def process
    return unless valid?

    if user.payment_customer_id.nil?
      PaymentService::Customer.create(user)
      user.reload
    end
    PaymentService::Customer.new(user).add_card(token) unless token.nil?

    begin
      charge.run
    rescue PaymentService::Charge::Error => e
      errors[:base] << e.message
    end

    return unless success?
    create_payment
    create_payment_response
    log_adjustment_fees
    send_receipt if email_confirmation?
    log_payment_request
  end

  delegate :success?, to: :charge

  def persisted?
    false
  end

  def error_full_message(separator = ' ')
    errors.full_messages.join(separator)
  end

  def amount_charged_in_cents
    (amount_charged * 100).to_i
  end

  private

  attr_reader :payment

  def card
    @card ||= charge.respond_to?(:card) ? charge.card : Hashie::Mash.new
  end

  def create_payment
    attributes = {
      order_id:           order_id,
      authorization_code: charge.balance_transaction,
      credit_card_number: card.last4,
      credit_card_type:   card.brand,
      amount:             amount_charged,
      promise_price: promise_purchase.credit_card_payment_price,
      return_fee: return_fee,
      address_city: card.address_city,
      address_state: card.address_state,
      address_zip: card.address_zip,
      address_street: [card.address_line1, card.address_line2].compact.join(' '),
      address_country: card.address_country
    }
    @payment = Payment.create!(attributes)
  end

  def create_payment_response
    attributes = {
      promise_purchase: promise_purchase,
      payment:            payment,
      error_message:      error_full_message,
      raw_response:       '',
      json_response:      charge.response.to_json
    }
    @payment_response ||= PaymentResponse.create!(attributes)
  end

  def log_payment_request
    request_attributes = {
      avs_address1:       card.address_line1,
      avs_address2:       card.address_line2,
      avs_city:           card.address_city,
      avs_state:          card.address_state,
      avs_zip:            card.address_zip,
      avs_country:        card.address_country,
      order_id:           order_id,
      amount:             amount_charged_in_cents,
      price:              promise_purchase.credit_card_payment_price,
      return_fee:         return_fee
    }
    request_attributes[:return_fee_captured] = true if return_fee.present?
    PaymentsLogger.info("Request attributes: #{request_attributes}; Response: #{@payment_response.attributes}")
  end

  def log_adjustment_fees
    promise_purchase.fees.nonreimbursable_fees.each do |fee|
      PaymentAdjustmentFee.create(item_description: fee.name, fee_amount: fee.amount, payment_id: payment.id, adjustment_code: fee.code)
    end
  end

  def charge
    @charge ||= PaymentService::Charge.promise_purchase(@user,
                                                          @promise_purchase,
                                                          amount_charged_in_cents,
                                                          idempotency_key: idempotency_key,
                                                          order_id: order_id)
  end

  def order_id
    @order_id ||= "#{promise_purchase.id}-#{SecureRandom.hex(5)}"
  end

  def idempotency_key
    @idempotency_key ||= return_fee.present? ? "#{promise_purchase.to_gid}-return-fee" : promise_purchase.to_gid.to_s
  end

  def amount_charged
    return_fee.present? ? return_fee : promise_purchase.credit_card_payment_price
  end

  def send_receipt
    PaymentsMailer.receipt(user.email, payment, order_id: order_id, amount: amount_charged).deliver_later
  end

  def email_confirmation?
    value_to_boolean(send_receipt_confirmation)
  end
end
