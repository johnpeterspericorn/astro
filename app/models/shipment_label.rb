# encoding: utf-8
class ShipmentLabel
  def initialize(easypost_shipment)
    @easypost_shipment = easypost_shipment
  end

  def self.find(easypost_shipment_id)
    easypost_shipment = EasyPost::Shipment.retrieve(easypost_shipment_id)
    new(easypost_shipment)
  end

  def self.buy(shipment_information)
    easypost_shipment = ShipmentHandler.new(shipment_information).buy
    new(easypost_shipment)
  end

  delegate :id, to: :easypost_shipment

  def url
    easypost_shipment.postage_label.label_url
  end

  delegate :tracking_code, to: :easypost_shipment

  def carrier
    easypost_shipment.tracker.carrier
  end

  private

  attr_reader :easypost_shipment
end
