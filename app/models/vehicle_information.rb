# encoding: utf-8
require 'active_record_attribute_sanitizer'

class VehicleInformation < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer
  belongs_to :promise_offer
  belongs_to :sold_vehicle, primary_key: :vnum, foreign_key: :vnum
  delegate :country, to: :promise_offer

  validates :make, :vnum, presence: true

  has_paper_trail class_name: 'PromisePurchaseVersion'
end
