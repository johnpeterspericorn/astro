# encoding: utf-8
class AgreementOptionList < Struct.new(:vehicles)
  def select_options
    vehicles_for_select.map do |vehicle|
      agreement_number = vehicle.agreement_no
      agreement_name   = numbers_to_names[agreement_number]

      [agreement_name, agreement_number]
    end
  end

  private

  def numbers_to_names
    @numbers_to_names ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number).get_agreement_numbers_to_names
  end

  def pawn_access_number
    vehicles.map(&:pawn_access_no).compact.first
  end

  def vehicles_for_select
    vehicles.to_a.uniq(&:agreement_no)
  end
end
