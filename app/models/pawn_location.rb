# encoding: utf-8
class PawnLocation < ActiveRecord::Base
  NON_ASTROHEIM_AUCTION_INITIALS = %w(NONM DLRM FTB DMMD).freeze

  scope :accepts_returns, -> { where(accepts_returns: true) }
  scope :for_country, ->(country) { where(country: country) }
  scope :with_purchases_enabled, -> { where(purchases_enabled: true) }

  delegate :presale_lane_numbers, to: :presale_vehicles

  has_many :presale_vehicles, foreign_key: :location_initials, primary_key: :initials do
    def presale_lane_numbers
      select(:lane_no)
        .uniq
        .map(&:lane_no)
        .sort_by(&:to_i)
    end
  end

  def self.array_fields(*fields)
    fields.each do |field|
      serialize field, Array
      define_method("#{field}=") { |values| array_setter(field, values) }
    end
  end

  array_fields :return_emails, :accounting_email

  private

  def array_setter(field, values)
    values = values.split(/,\s*/) if values.is_a?(String)
    self[field] = values
  end
end
