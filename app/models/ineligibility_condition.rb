# encoding: utf-8
class IneligibilityCondition < ActiveRecord::Base
  INVALID_PRICING = -1
  CANCELLED_PURCHASE = -2
  PAST_PURCHASE_TIME = 11

  def self.notice_for_condition(condition_id)
    find_by_condition_id(condition_id).try(:notice)
  end

  def self.to_hash
    conditions = all.map do |condition|
      [condition.condition_id, condition.notice]
    end

    Hash[conditions]
  end
end
