# encoding: utf-8
require 'ods/client'

class OdsQueryInterface < Struct.new(:agreement_no, :location_initials, :universal_no, :sale_no, :lane_no, :run_no)
  def vehicle_has_left_lot?
    valid_ods_query.present? ? valid_ods_query[:left_lot] : false
  end

  def vehicle_paid_by_floor_plan?
    valid_ods_query.present? ? valid_ods_query[:paid_by_floor_plan] : false
  end

  def payment_type
    valid_ods_query.present? ? valid_ods_query[:payment_type] : false
  end

  def valid_ods_query
    if required_values_present?
      @vehicle_info ||= default_false_when_ods_disabled { ods_client.request_vehicle_info }
    else
      {}
    end
  end

  def left_lot?
    if required_values_present?
      @vehicle_info ||= default_false_when_ods_disabled { ods_client.left_lot? }
    else
      {}
    end
  end

  def agreementshield_charged?
    # Can throw an exception in case of Timeout / ODS service unavailable
    # to fail DelayedJob, so that it is retried
    request_info = default_false_when_ods_disabled { ods_client.request_vehicle_info }
    request_info[:has_agreementshield_charge] if request_info
  end

  private

  def ods_client
    @ods_client ||= Ods::Client.new(agreement_no: agreement_no, location_initials: location_initials, universal_no: universal_no, sale_no: sale_no, lane_no: lane_no, run_no: run_no)
  end

  def required_values_present?
    ![agreement_no, location_initials, universal_no, sale_no, lane_no, run_no].any?(&:nil?)
  end

  def default_false_when_ods_disabled(&block)
    if query_ods_service?
      safely_query_ods(&block)
    else
      false
    end
  end

  def safely_query_ods
    yield
  rescue Errno::ECONNRESET, Errno::ETIMEDOUT, Timeout::Error
    Rails.logger.info 'ODS service unavailable. Defaulting `left_lot` to false.'
    false
  end

  def query_ods_service?
    Settings.query_ods
  end
end
