# encoding: utf-8
class ReplicateSoldVehicle
  attr_reader :vehicle

  def initialize(vehicle)
    @vehicle = vehicle
  end

  def call
    create_offer_and_associated_records
  end

  private

  def create_offer_and_associated_records
    PromiseOffer.transaction do
      PromiseOffer.create!(new_offer_attributes)
    end
  end

  def new_offer_attributes
    sliced_vehicle_attributes(PromiseOffer.sanitized_attribute_names)
      .merge(
        promise_status: :unselected,
        pawn_information: pawn_information,
        inspection_information: inspection_information,
        price_factor: price_factor,
        vehicle_information: vehicle_information,
        discount: discount
      )
  end

  def pawn_information
    PawnInformation.new(pawn_information_attributes)
  end

  def inspection_information
    InspectionInformation.new(inspection_information_attributes)
  end

  def price_factor
    PriceFactor.new(price_factor_attributes)
  end

  def vehicle_information
    VehicleInformation.new(vehicle_information_attributes)
  end

  def discount
    Discount.new(discount_attributes)
  end

  def sliced_vehicle_attributes(sliced_attributes)
    vehicle.attributes.slice(*sliced_attributes)
  end

  def pawn_information_attributes
    sliced_vehicle_attributes(PawnInformation.sanitized_attribute_names)
  end

  def inspection_information_attributes
    sliced_vehicle_attributes(InspectionInformation.sanitized_attribute_names)
  end

  def price_factor_attributes
    sliced_vehicle_attributes(PriceFactor.sanitized_attribute_names)
  end

  def vehicle_information_attributes
    sliced_vehicle_attributes(VehicleInformation.sanitized_attribute_names)
  end

  def discount_attributes
    sliced_vehicle_attributes([:discount, :discount_description])
  end
end
