# encoding: utf-8
class BundledPurchase
  include Draper::Decoratable

  def initialize(options = {})
    @promise_purchase  = options[:promise_purchase].decorate
    @inspection_purchase = options[:inspection_purchase].decorate

    raise(ArgumentError, 'both a promise purchase and an inspection purchase are required') unless @promise_purchase.present? && @inspection_purchase.present?
  end
  attr_reader :promise_purchase, :inspection_purchase

  delegate :miles_selected,
           :promise_offer,
           :duration,
           :price,
           :persisted?,
           :offer_pending?,
           :pawn_location,
           :next_day_return_eligible?,
           to: :promise_purchase
end
