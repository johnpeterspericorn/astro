# encoding: utf-8
class TotalProtectKind
  include ProtectKind
  TWO_DAYS = 2.days
  SECOND_WINDOW_OFFSET_IN_DAYS = 60.days

  def receive_by_date
    if before_first_window? || within_first_window?
      first_window_receive_by_date
    else
      second_window_receive_by_date
    end
  end

  def expiration_date
    date = receive_by_date + TWO_DAYS
    DateUtils.resolve_to_weekday(date || receive_by_date)
  end

  def final_window_expiration_date
    date = second_window_receive_by_date + TWO_DAYS
    DateUtils.resolve_to_weekday(date || second_window_receive_by_date)
  end

  def max_return_time
    time = (purchase_date || Time.current) + base_duration_offset
    time.end_of_day
  end

  def within_window?
    within_first_window? || within_second_window?
  end

  def first_window_min_return_time
    min_return_time + 1
  end

  def first_window_max_return_time
    days_selected_after_purchase
  end

  def second_window_min_return_time
    min_return_time + TotalProtectKind::SECOND_WINDOW_OFFSET_IN_DAYS + 1
  end

  def second_window_max_return_time
    min_return_time + base_duration_offset
  end

  private

  def before_max_return_time?
    (min_return_time + base_duration_offset).future?
  end

  def after_min_return_time?
    (min_return_time + SECOND_WINDOW_OFFSET_IN_DAYS).past?
  end

  def before_min_return_time?
    days_selected_after_purchase.future?
  end

  def within_first_window?
    min_return_time.past? && before_min_return_time?
  end

  def before_first_window?
    Date.current <= min_return_time
  end

  def within_second_window?
    after_min_return_time? && before_max_return_time?
  end

  def end_of_day_tomorrow
    Time.current.tomorrow.end_of_day
  end

  def first_window_receive_by_date
    days_selected_after_purchase > end_of_day_tomorrow ? days_selected_after_purchase : end_of_day_tomorrow
  end

  def second_window_receive_by_date
    max_return_time > end_of_day_tomorrow ? max_return_time : end_of_day_tomorrow
  end

  def base_duration_offset
    Promise::BASE_DURATION.days + SECOND_WINDOW_OFFSET_IN_DAYS + extended_days.days
  end

  def days_selected_after_purchase
    min_return_time + purchase.days_selected.days + extended_days.days
  end
end
