# encoding: utf-8
require 'active_record_attribute_sanitizer'

class AgreementInformation < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer

  validates :agreement_no, presence: true

  alias_attribute :name, :agreementship_name

  scope :for_agreement_no, lambda { |agreement_number|
    where(agreement_no: agreement_number.to_s)
  }

  def company
    'Attn. Accounting'
  end

  def to_address(verify_flag = true)
    Address.new(
      address_street,
      address_suite,
      address_city,
      address_state,
      address_zipcode,
      verify_flag
    )
  end

  def self.agreement_informations_hash(agreements)
    agreement_info = AgreementInformation.select(:agreement_no, :agreementship_name).where(agreement_no: agreements)
    agreement_info.map { |agreement| [agreement.agreement_no, agreement.agreementship_name] }.to_h
  end

  def self.build_with_defaults(
      pawn_access_number,
      agreement_number,
      defaults = BuyerInformationDefaults.new(pawn_access_number, agreement_number)
  )

    agreement_info = find_or_initialize_by(agreement_no: agreement_number.to_s)

    agreement_info.agreementship_name ||= defaults.agreementship_name
    agreement_info.address_street  ||= defaults.address_street
    agreement_info.address_suite   ||= defaults.address_suite
    agreement_info.address_city    ||= defaults.address_city
    agreement_info.address_state   ||= defaults.address_state
    agreement_info.address_zipcode ||= defaults.address_zipcode

    agreement_info
  end
end
