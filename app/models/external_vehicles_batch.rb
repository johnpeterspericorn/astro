# encoding: utf-8
class ExternalVehiclesBatch < ActiveRecord::Base
  has_many :external_vehicles_loads
  has_many :external_vehicles_documents
  belongs_to :user
  delegate :username, to: :user

  scope :unposted, ->() { where.not(status: 'posted') }
  STATUSES = %w(rejected unapproved unposted posted)

  def update_status
    status = if external_vehicles_loads.rejected.present?
               'rejected'
             elsif external_vehicles_loads.unapproved.present?
               'unapproved'
             elsif external_vehicles_loads.unposted.present?
               'unposted'
             else
               'posted'
             end
    update_attributes(status: status)
  end

  def agreement_numbers
    external_vehicles_loads.map(&:agreement_no).uniq
  end
end
