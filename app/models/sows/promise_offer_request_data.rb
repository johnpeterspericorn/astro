# encoding: utf-8
require 'astroheim/sows/request_data'

module Sows
  class PromiseOfferRequestData < Astroheim::Sows::RequestData
    attr_reader :promise_offer

    delegate(
      :location_initials,
      :sblu,
      :work_order_number,
      :seller_paid?,
      :bypass_charge?,
      :vnum,

      to: :promise_offer
    )

    def initialize(promise_offer)
      @promise_offer = promise_offer
      @charge_code_mapping = if Settings.product_mapping_enabled
                               ChargeCodeMapping.load(promise_offer)
                             else
                               Hashie::Mash.new
                             end
    end

    def product_type
      "#{promise_offer.days_selected} Days, #{promise_offer.miles_selected} Miles"
    end

    def description
      "agreementshield #{promise_offer.days_selected}D #{promise_offer.miles_selected}M"
    end

    def amount
      promise_offer.rounded_discounted_price
    end

    def charge_code
      @charge_code_mapping.charge_code
    end

    def call_id
      @charge_code_mapping.call_id
    end
  end
end
