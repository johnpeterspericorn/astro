# encoding: utf-8
class VehicleFilter
  FILTER_TYPES = %w(badge_number pawn_access_number location_initials agreement_number).freeze
end
