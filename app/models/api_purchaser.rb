# encoding: utf-8
class ApiPurchaser < Hashie::Trash
  property :email, required: true, from: :email_address
  property :agreement_number, required: true, from: :buyer_agreement_number
  property :vnum, required: true
  property :additional_days, from: :days, required: true, with: ->(total_days) { total_days - Promise::BASE_DURATION if total_days }
  property :miles, required: true
  property :silent, default: false
  property :agreementshield_quoted_price, required: true, transform_with: ->(v) { v.to_f }

  attr_reader :filter_query_data, :promise_purchase

  def initialize(options = {})
    super options.symbolize_keys
  end

  def purchase_vehicle
    return unless valid?
    purchaser.record_purchase
    @promise_purchase = purchaser.purchase
  end

  def purchase_response
    if valid?
      fields = %w(transaction_id created_at promise_offer_id)
      promise_purchase
        .attributes
        .slice(*fields)
        .merge(return_code: return_code, promise_purchase_id: promise_purchase.id)
    else
      { return_code: return_code }
    end
  end

  def status
    if valid?
      :ok
    else
      :bad_request
    end
  end

  def valid?
    !error_types.any? do |_, predicate|
      predicate.call
    end
  end

  private

  def error_types
    {
      12 => -> { no_such_vehicle? },
      13 => -> { vehicle_agreement_mismatch? },
      8  => -> { vehicle.left_lot? },
      10 => -> { vehicle.ineligible? },
      4  => -> { !valid_options? },
      5  => -> { price_mismatch? },
      9  => -> { promise_offer.try(:past_cutoff?) },
      11 => -> { promise_purchase.blank? && promise_offer.purchased? }
    }
  end

  def price_mismatch?
    promise_price.present? && promise_price != agreementshield_quoted_price unless Settings.disable_purchase_api_price_match
  end

  def conditions
    error_types.merge(1 => -> { promise_purchase.try(:offer_pending?) })
  end

  def no_such_vehicle?
    !SoldVehicle.exists?(vnum: vnum)
  end

  def vehicle_agreement_mismatch?
    vehicle.blank?
  end

  def purchaser
    @purchaser ||= purchaser_klass.new(
      offer: promise_offer,
      additional_days: additional_days,
      miles: miles,
      channel: PromiseChannel::API,
      email: (buyer_emails.present? ? buyer_emails : email),
      price: agreementshield_quoted_price
    )
  end

  def purchaser_klass
    if silent
      SilentPromisePurchaser
    else
      PromisePurchaser
    end
  end

  def promise_offer
    @promise_offer ||= PromiseOfferRecorder.record_offer(vehicle) if vehicle.present?
  end

  def buyer_emails
    BuyerInformation.find_by_pawn_access_no(promise_offer.pawn_access_no).try(:emails)
  end

  def promise_price
    Promise.new(promise_offer).price(miles, additional_days).round if promise_offer.present?
  end

  def vehicle
    @vehicle ||= filter_query_data.vehicles_for_display.try(:first)
  end

  def filter_query_data
    @filter_query_data ||= FilterQueryData.new(agreement_number: agreement_number, vnum: vnum)
  end

  def return_code
    conditions.each do |err, predicate|
      return err if predicate.call
    end

    0
  end

  def valid_options?
    Promise::LENGTHS.include?(additional_days) && Promise::DISTANCES.include?(miles)
  end
end
