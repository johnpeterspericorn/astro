# encoding: utf-8
class HowPaidInformation < ActiveRecord::Base
  validates :payment_type, presence: true
end
