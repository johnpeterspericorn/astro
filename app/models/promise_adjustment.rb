# encoding: utf-8
class PromiseAdjustment < PromiseCharge
  def initialize(promise_offer, old_price, new_price)
    @old_price = old_price.to_f.round
    @new_price = new_price.to_f.round
    super(promise_offer)
  end

  private

  attr_reader :old_price, :new_price

  def amount
    new_price - old_price
  end
end
