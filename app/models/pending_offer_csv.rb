# encoding: utf-8
class PendingOfferCsv < OfferCsv
  def self.column_names
    %w(
      vnum
      vehicle_purchase_price
      vehicle_purchased_at
      location_initials
      buyer_agreement_number
      seller_agreement_number
      promise_price
      if_bid
      ineligible
      repurchased
      approved_at
      invalidated_at
      pending_reason
      current_status
      returned_at
      allowed_username
      invalidated_username
    )
  end

  def initialize(offers)
    @offers = PendingOfferDecorator.decorate_collection(offers)
  end
end
