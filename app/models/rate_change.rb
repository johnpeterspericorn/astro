# encoding: utf-8
require 'date_utils'
class RateChange < ActiveRecord::Base
  include DateUtils

  after_create :process

  belongs_to :rate_changing_user, class_name: 'User'
  belongs_to :link_clicked_user, class_name: 'User'
  belongs_to :relationship_manager

  delegate :name, :email, to: :relationship_manager

  validates :return_rate, numericality: { greater_than: 0 }
  validate :effective_date_input
  validates :relationship_manager, presence: true

  DEFAULT_EFFECTIVE_DATE_DAYS = 1

  scope :for_agreement_no, lambda { |agreement_number|
    where(agreement_no: agreement_number.to_s)
  }

  def process_user(user)
    link_clicked_at.present? ? link_user(user).create_link_action_type : link_user(user).process_link_click
  end

  def process
    RateChangeMailer.rate_change_notification(self).deliver_now
    update_attributes(letter_sent: DateUtils.now)
    ActionType.process(self, '', letter_sent, emails, parameters)
  end

  def country
    SupportedCountry::DEFAULT
  end

  private

  def effective_date_input
    errors.add(:effective_date, 'must be after today') unless effective_date.end_of_day > Time.current.end_of_day
  end

  def link_user(user)
    ExternalUser.new(self, DateUtils.now, user)
  end

  def parameters
    "new rate: #{return_rate}, effective_date: #{effective_date}"
  end
end
