# encoding: utf-8
class SupportedCountry
  DEFAULT = 'United States of America'.freeze

  NAMES = [
    USA = 'United States of America'.freeze,
    CA = 'Canada'.freeze
  ].freeze

  Canada = 'Canada'.freeze
end
