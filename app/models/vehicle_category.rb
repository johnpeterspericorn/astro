# encoding: utf-8
class VehicleCategory < ActiveRecord::Base
  has_one :pricing_distribution
end
