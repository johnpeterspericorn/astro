# encoding: utf-8
require 'active_record_attribute_sanitizer'
require 'astroheim/vehicle_decode_api'

class SoldVehicle < ActiveRecord::Base
  include VehicleMixins::DomainConcerns # defines #eligible? and #ineligible?
  include ActiveRecordAttributeSanitizer

  INELIGIBLE_BY_MISSING_MMR = 5
  IGNORED_INELIGIBILITY_CONDITIONS = [4, INELIGIBLE_BY_MISSING_MMR, 8, 9].freeze

  GUARANTEE_DISTANCE1_US = 250
  GUARANTEE_DISTANCE1_CA = 600

  has_many :vehicle_information, primary_key: :vnum, foreign_key: :vnum
  has_many :promise_offers, through: :vehicle_information
  belongs_to :pawn_location, primary_key: :initials, foreign_key: :location_initials
  delegate :country, to: :pawn_location

  validates :preselected_promise_days, :preselected_promise_miles, :automatic_purchase_email, presence: true, if: :automatic_purchase?

  validates(
    :make,
    :vnum,
    :lane_no,
    :run_no,
    :vehicle_purchase_price,
    :sale_no,
    :purchased_at,
    :location_initials,

    presence: true
  )

  validates :sblu, :universal_no, :work_order_number, presence: true, unless: 'PawnLocation::NON_ASTROHEIM_AUCTION_INITIALS.include?(location_initials)'
  validate :check_offer_duplicate, on: :create

  scope :for_pawn_access_number, ->(filter_query_data) do
    where("sold_vehicles.pawn_access_no = ? OR (sold_vehicles.pawn_access_no = '0' AND sold_vehicles.agreement_no IN (?))",
          filter_query_data.pawn_access_number,
          filter_query_data.agreement_numbers)
  end

  scope :automatic_purchase, -> { where(automatic_purchase: true) }
  scope :seller_paid,        -> { where(seller_paid:        true) }
  scope :unprocessed,        -> { where(automatic_purchase_processed: false) }

  scope :outstanding_automatic_purchase, -> do
    automatic_purchase
      .unprocessed
      .eligible_for_automatic_purchase
  end

  scope :outstanding_seller_paid, -> do
    seller_paid
      .unprocessed
      .eligible_for_automatic_purchase
  end

  scope :outstanding_ineligible, -> do
    automatic_purchase
      .unprocessed
      .ineligible_for_automatic_purchase
  end

  scope :eligible_for_automatic_purchase, -> do
    # Missing MMR is still eligible for automatic purchase #54056781
    where <<-SQL, [0, INELIGIBLE_BY_MISSING_MMR]
      ineligibility_condition IS NULL
      OR
      ineligibility_condition IN (?)
    SQL
  end

  scope :ineligible_for_automatic_purchase, -> do
    # Missing MMR is still eligible for automatic purchase #54056781
    # Certain other conditions are ignored as ineligible
    where <<-SQL, [0, *IGNORED_INELIGIBILITY_CONDITIONS]
      ineligibility_condition NOT IN (?)
    SQL
  end

  def valid_for_automated_purchase?
    valid? && !purchase_exists?
  end

  def preselected_promise_miles
    preselected_miles = self[:preselected_promise_miles]
    return preselected_miles if country_code != 'CA'
    # MAS will send 250Miles for canada when it should be 600Km
    # hardcoding to 600 in case of CA
    if country_code == 'CA'
      preselected_miles = (preselected_miles == GUARANTEE_DISTANCE1_US) ? GUARANTEE_DISTANCE1_CA : preselected_miles
    end
    preselected_miles
  end

  def promise_purchased?
    PromiseOffer.by_vnum_and_universal_no(self).with_purchase.present?
  end

  def past_cutoff?
    must_be_promised_at.present? && must_be_promised_at.past?
  end

  def eligible_for_inspection?
    inspection_bundle_price.present? && inspection_only_price.present?
  end

  def available_for_purchase?
    created_at > 24.hours.ago &&
      eligible?               &&
      !past_cutoff?           &&
      !left_lot?
  end

  def left_lot?
    OdsQueryInterface.new(agreement_no, location_initials, universal_no, sale_no, lane_no, run_no).vehicle_has_left_lot?
  end

  private

  def check_offer_duplicate
    errors.add(:vnum, I18n.t('sold_vehicles.errors.duplicate_check')) if duplicate_vehicle_exists?
  end

  def duplicate_vehicle_exists?
    sold_vehicle_exists? || (!automatic_purchase && offer_exists?) || (automatic_purchase && purchase_exists?)
  end

  def sold_vehicle_exists?
    return false if automatic_purchase
    SoldVehicle.exists?(vnum: vnum, sblu: sblu, work_order_number: work_order_number, location_initials: location_initials)
  end

  def offer_exists?
    PromiseOffer
      .joins(:vehicle_information, :pawn_information)
      .exists?(
        pawn_information: {
          sblu: sblu,
          work_order_number: work_order_number,
          location_initials: location_initials
        },
        vehicle_information: { vnum: vnum })
  end

  def purchase_exists?
    PromiseOffer
      .joins(:vehicle_information, :pawn_information, :promise_purchase)
      .exists?(
        pawn_information: {
          sblu: sblu,
          work_order_number: work_order_number,
          location_initials: location_initials
        },
        vehicle_information: { vnum: vnum })
  end

  def country_object
    Country.find_country_by_name(country || SupportedCountry::DEFAULT)
  end

  def country_code
    country_object.try(:alpha2)
  end

  def self.decode_vnum(vnum)
    client = Astroheim::VehicleDecodeApi::Client.new(vnum)
    client.decode_vnum
  end
end
