# encoding: utf-8
require 'column_conversions'
class PromisePurchase < ActiveRecord::Base
  include ColumnConversions
  KINDS = [
    PURCHASE_PROTECT = 'purchase_protect'.freeze,
    MARKET_PROTECT = 'market_protect'.freeze,
    EXTENDED_PROTECT = 'extended_protect'.freeze,
    TOTAL_PROTECT = 'total_protect'.freeze
  ].freeze

  BRANDED_KINDS = {
    'purchase_protect' => 'Purchase Advantage',
    'extended_protect' => 'Extended Advantage',
    'total_protect' => 'Total Advantage',
    'market_protect' => 'Market Advantage*',
    'pplv' => 'Purchase Advantage LV'
  }.freeze

  THIRTY_DAY_OFFSET = 30

  EP_OFFSET = 30 # for Extended Protect
  TP_OFFSET = 67 # for Total Protect

  has_paper_trail class_name: 'PromisePurchaseVersion'

  belongs_to :promise_offer
  belongs_to :purchasing_user, class_name: 'User'
  has_one    :return_invoice
  has_one    :pawn_information, through: :promise_offer
  has_one    :coupon
  has_many   :payment_responses
  has_many   :payments, through: :payment_responses

  validates :channel, inclusion: { in: PromiseChannel::ALL }
  validates :partner, length: { maximum: 255 }

  before_create :set_promised_at
  before_create :set_extension_days

  delegate(
    :pawn_location,
    :country,
    :location_initials,
    :make,
    :model,
    :year,
    :vnum,
    :agreement_no,
    :seller_no,
    :automatic_purchase,
    :seller_paid,
    :fees,
    :odometer_reading,
    :emails,
    :has_inspection?,
    :left_lot?,
    :adjustments_buy_fee,
    :limited_volume,
    :percent_coverage,
    :canadian_location?,
    :paid_by_buyer?,

    to: :promise_offer
  )

  delegate :tax_rate, to: :pawn_location, allow_nil: true

  delegate(:username, to: :purchasing_user, prefix: true, allow_nil: true)

  delegate(
    :vehicle_purchase_price,
    :sale_no,
    :lane_no,
    :run_no,
    :universal_no,

    to: :pawn_information
  )

  delegate(
    :expiration_date,
    :min_return_time,
    :max_return_time,
    :receive_by_date,
    :final_window_expiration_date,

    to: :promise_kind
  )

  scope :for_agreement, ->(pawn_access_number) do
    includes(:pawn_information)
      .merge(PromiseOffer.for_agreement(pawn_access_number))
  end

  scope :for_agreement_group_code, ->(agreement_no, group_code) do
    includes(:pawn_information)
      .where(
        pawn_information: { agreement_no: agreement_no },
        promise_offers: { group_code: group_code }
      )
  end

  scope :for_agreement_number, ->(agreement_number) do
    includes(:return_invoice, promise_offer: :pawn_information)
      .where(pawn_information: { agreement_no: agreement_number })
  end

  scope :for_vnum_number, ->(vnum) do
    includes(promise_offer: :vehicle_information)
      .includes(:return_invoice)
      .where(vehicle_information: { vnum: vnum })
  end

  scope :for_transaction_id, ->(transaction_id) do
    where(transaction_id: transaction_id)
  end

  scope :for_seller_paid, ->(seller_paid) do
    includes(:promise_offer)
      .where(promise_offers: { seller_paid: seller_paid })
  end

  scope :unbundled, -> do
    if Settings.inspections_enabled
      joins(:promise_offer)
        .joins('LEFT OUTER JOIN inspection_purchases ON inspection_purchases.promise_offer_id = promise_offers.id')
        .where(inspection_purchases: { promise_offer_id: nil })
    else
      all
    end
  end

  scope :bundled, -> do
    if Settings.inspections_enabled
      joins(:promise_offer, promise_offer: :inspection_purchase)
    else
      where(id: [])
    end
  end

  scope :promised_at, ->(from, to) do
    where(promised_at: from..to)
  end

  def set_transaction_id
    transaction_id = TransactionIdGenerator.new(promise_offer.pawn_access_no).generate
    update_attributes(transaction_id: transaction_id)
  end

  def promise
    Promise.new(promise_offer)
  end

  def seller_info
    @agreement_info ||= AgreementInformation.find_by_agreement_no(agreement_no)
  end

  delegate :promise_price, to: :promise_offer, allow_nil: true
  def calculated_promise_price
    if promise_price
      promise_price
    else
      promise.price(miles_selected, additional_days)
    end
  end

  def self.from_options(vnum, miles, days, user = nil)
    promise_offer = EligiblePromiseOfferQuery.new(PromiseOffer.by_vnum(vnum)).by_user(user).order('promise_offers.updated_at').last
    if promise_offer.present?
      purchase = promise_offer.promise_purchase
      if purchase.blank?
        promise_offer.update_attributes!(promise_price: nil) unless promise_offer.promise_price.nil?
        purchase = PromisePurchase.new(promise_offer: promise_offer)
      end

      purchase.miles_selected  = miles
      purchase.additional_days = days
      purchase.purchasing_user = user
      purchase.channel = PromiseChannel::ASTRO
      purchase.partner = promise_offer.partner
    end
    purchase
  end

  def offer_pending?
    promise_offer.reload.try(:pending?)
  end

  def updated_before_save?
    (changed? && promise_offer.promised_at.present?) if promise_offer.present?
  end

  def updated_after_save?
    promise_offer.promised_at.present? && promise_offer.promised_at < promise_offer.updated_at if promise_offer.present?
  end

  def days_selected
    extended_protect? ? THIRTY_DAY_OFFSET : (promise_offer.days_selected || Promise::BASE_DURATION + additional_days)
  end

  def calculated_days_selected
    case kind || promise_offer.preselected_promise_kind
    when EXTENDED_PROTECT
      EP_OFFSET
    when TOTAL_PROTECT
      TP_OFFSET
    else
      (promise_offer.days_selected || Promise::BASE_DURATION + additional_days)
    end
  end

  def extended_protect?
    kind == EXTENDED_PROTECT
  end

  def total_protect?
    kind == TOTAL_PROTECT
  end

  def promise_kind
    @promise_kind ||= promise_kind_class.new(self)
  end

  def bundled_purchase
    inspection_purchase = InspectionPurchase.find_by(promise_offer_id: promise_offer_id)
    return unless !Settings.inspections_enabled || !inspection_purchase.nil?
    BundledPurchase.new(
      promise_purchase:  self,
      inspection_purchase: inspection_purchase
    )
  end

  def return_initiated?
    return_invoice.present? &&
      return_invoice.persisted? &&
      !return_invoice.cancelled?
  end

  def after_min_return_time?
    min_return_time.past?
  end

  def before_min_return_time?
    min_return_time.future?
  end

  def before_max_return_time?
    max_return_time.future?
  end

  def after_max_return_time?
    max_return_time.past?
  end

  def eligible_for_return?
    return false if promise_offer.pending?
    !return_cancellation_present? && !return_initiated? && within_return_window? && !expire_promise
  end

  def priority
    if eligible_for_return?
      1
    elsif return_initiated?
      2
    else
      3
    end
  end

  def return_fee_captured?
    fee_payment_captures = payments.select { |payment| (!payment.return_fee.nil? && !payment.return_fee.zero?) }
    !fee_payment_captures.empty?
  end

  def promise_payments_captured?
    purchase_payments_captures = payments.select { |payment| payment.return_fee.nil? && payment.promise_price.present? && !payment.promise_price.zero? }
    !purchase_payments_captures.empty?
  end

  def paid?
    promise_offer.seller_paid? || (payments.present? && promise_payments_captured?)
  end

  def unpaid?
    !paid?
  end

  delegate :pplv?, :limited_quantity?, :part_of_agreement_group?, :group_code, to: :promise_offer

  def price
    promise_offer.rounded_promise_price || calculated_promise_price
  end

  def total_nonreimbursable_fees
    fees.nonreimbursable_fees.total
  end

  def credit_card_payment_price
    if Settings.buynet_during_return
      total_nonreimbursable_fees
    else
      price
    end
  end

  def send_confirmation(email)
    case kind
    when MARKET_PROTECT
      PromisePurchaseRefreshMailer.refresh_confirmation(promise_offer).deliver_later
    when PURCHASE_PROTECT, TOTAL_PROTECT, EXTENDED_PROTECT
      PromisePurchaseMailer.purchase_confirmation(promise_purchases: [self], user_email: email).deliver_later
    end
  end

  def return_cancellation_present?
    return_invoice_with_cancelled.present?
  end

  def days_until_return_window(extra_day)
    days_selected + extra_day
  end

  def program360?
    value_to_boolean(automatic_purchase) && !value_to_boolean(seller_paid)
  end

  def next_day_return_eligible?
    return (min_return_time < (promised_at.end_of_day + 1.day)) unless promised_at.nil?
    false
  end

  def fee_data
    @fee_data ||= new_product_information.fee_details
  end

  def first_window_min_return_time
    promise_kind.first_window_min_return_time.to_s(:slashed)
  end

  def first_window_max_return_time
    promise_kind.first_window_max_return_time.to_s(:slashed)
  end

  def second_window_min_return_time
    promise_kind.second_window_min_return_time.to_s(:slashed)
  end

  def second_window_max_return_time
    promise_kind.second_window_max_return_time.to_s(:slashed)
  end

  def exceeds_promo_returns?
    group_code && seller_paid && !sell_side_promo.valid?
  end

  def sell_side_promo
    @sell_side_promo ||= SellSidePromotion.new(self)
  end

  private

  def set_extension_days
    return unless promise_extension
    self.extension_days = promise_extension.try(:additional_days).to_i
    self.extension_reason = promise_extension.try(:reason)
  end

  def within_return_window?
    promise_kind.within_window?
  end

  def promise_extension
    @promise_extension ||= PromiseExtension.find_by(vnum: vnum)
  end

  def record_promised_at
    self.promised_at ||= promise_offer.try(:purchased_at) || Time.current
  end

  def set_promised_at
    self.promised_at = promise_offer.purchased_at || Time.current
  end

  def return_invoice_with_cancelled
    @return_invoice_with_cancelled ||= ReturnInvoice.unscoped.where(promise_purchase_id: id)
  end

  def new_product_information
    NewProductFeeInformation.new(self)
  end

  def promise_kind_class
    if limited_quantity? || percent_coverage.present?
      NewProductKind
    else
      "#{kind}_kind".classify.constantize
    end
  end
end
