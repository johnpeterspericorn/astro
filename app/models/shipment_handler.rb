# encoding: utf-8
class ShipmentHandler
  PARCEL_PREDEFINED_WEIGHT = 3.0 # ounces

  CARRIER_PARAMS = [
    UPS = { predefined_package: 'UPSLetter', service: 'NextDayAir', secondary_service: nil }.freeze,
    FEDEX = { predefined_package: 'FedExEnvelope', service: 'STANDARD_OVERNIGHT', secondary_service: 'FEDEX_2_DAY' }.freeze
  ].freeze
  attr_reader :shipment_information, :carrier_params

  def initialize(shipment_information)
    @shipment_information = shipment_information
    @carrier_params = select_carrier_params
  end

  def buy
    easypost_shipment.buy(rate: carrier_rate)
  end

  private

  def select_carrier_params
    if Settings.fedex_shipments_enabled
      FEDEX
    else
      UPS
    end
  end

  def carrier_rate
    service_rate = easypost_shipment.rates.find do |rate|
      rate.service == carrier_params[:service]
    end
    service_rate.blank? ? secondary_carrier_rate : service_rate
  end

  def secondary_carrier_rate
    easypost_shipment.rates.find do |rate|
      rate.service == carrier_params[:secondary_service]
    end
  end

  def address
    @address ||= shipment_information.to_address
  end

  def easypost_to_address
    address_attributes = {
      name:    shipment_information.name,
      company: shipment_information.company,
      street1: address.street,
      street2: address.suite,
      city:    address.city,
      state:   address.state,
      zip:     address.zipcode
    }
    address_attributes[:phone] = shipment_phone if Settings.fedex_shipments_enabled
    @easypost_to_address ||= EasyPost::Address.create(address_attributes)
  end

  def easypost_from_address
    @from_address ||= EasyPost::Address.create_and_verify(
      name:    'AgreementShield',
      company: 'Returns Department',
      street1: '2002 Summit Blvd.',
      street2: '1000',
      city:    'Chicago',
      state:   'GA',
      zip:     '30319',
      phone:   '8552463232', # Contact number of AgreementShield Customer Service
    )
  end

  def shipment_phone
    # Default to customer care phone number if agreement/floor plan phone is not available.
    shipment_information.phone.blank? ? '8552463232' : shipment_information.phone
  end

  def parcel
    @parcel ||= EasyPost::Parcel.create(parcel_params)
  end

  def easypost_shipment
    @shipment ||= EasyPost::Shipment.create(shipment_params)
  end

  def parcel_params
    {
      predefined_package: carrier_params[:predefined_package],
      weight:             PARCEL_PREDEFINED_WEIGHT
    }
  end

  def shipment_params
    Settings.fedex_shipments_enabled? ? fedex_shipment_params : ups_shipment_params
  end

  def ups_shipment_params
    {
      to_address:   easypost_to_address,
      from_address: easypost_from_address,
      parcel:       parcel,
      options:      { label_format: label_url_format }
    }
  end

  def fedex_shipment_params
    {
      to_address:   easypost_from_address,
      from_address: easypost_to_address,
      parcel:       parcel,
      is_return:    true,
      options:      { label_format: label_url_format }
    }
  end

  def label_url_format
    Settings.shipment_label_pdf_enabled? ? 'PDF' : 'PNG'
  end
end
