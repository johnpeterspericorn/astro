# encoding: utf-8
class ChargeCodeMapping < ActiveRecord::Base
  validates :charge_code, presence: true

  NONEXISTENT_PRODUCT_NAME = 'nonexistent'.freeze
  VALID_PRODUCTS = %w(
    purchase_protect
    extended_protect
    market_protect
    total_protect
  ).freeze

  class << self
    def load(promise_offer)
      product = VALID_PRODUCTS.include?(promise_offer.preselected_promise_kind) ? promise_offer.preselected_promise_kind : NONEXISTENT_PRODUCT_NAME
      find_by(
        product: product,
        seller_paid: promise_offer.seller_paid?,
        automatic_purchase: promise_offer.automatic_purchase,
        limited_volume: promise_offer.limited_quantity?,
        percent_coverage: promise_offer.percent_coverage.present?
      )
    end

    def insert_into_db
      Rails.logger.debug "Inserting records into #{self.class.name}"
      records_as_yaml = <<-YAML
---
-
  seller_paid: false
  automatic_purchase: true
  product: purchase_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 962
  call_id: DEALB
-
  seller_paid: false
  automatic_purchase: true
  product: extended_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 962
  call_id: DEALB
-
  seller_paid: false
  automatic_purchase: true
  product: market_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: false
  automatic_purchase: true
  product: total_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: false
  automatic_purchase: true
  product: #{NONEXISTENT_PRODUCT_NAME}
  limited_volume: false
  percent_coverage: false
  charge_code: 928
  call_id: DSNPB
-
  seller_paid: true
  automatic_purchase: true
  product: purchase_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 963
  call_id: DEALS
-
  seller_paid: true
  automatic_purchase: true
  product: extended_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 963
  call_id: DEALS
-
  seller_paid: true
  automatic_purchase: true
  product: market_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: true
  automatic_purchase: true
  product: total_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: true
  automatic_purchase: true
  product: #{NONEXISTENT_PRODUCT_NAME}
  limited_volume: false
  percent_coverage: false
  charge_code: 927
  call_id: DSNPS
-
  seller_paid: false
  automatic_purchase: false
  product: purchase_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 929
  call_id: DEALA
-
  seller_paid: false
  automatic_purchase: false
  product: extended_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 962
  call_id: DEALB
-
  seller_paid: false
  automatic_purchase: false
  product: market_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: false
  automatic_purchase: false
  product: total_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: false
  automatic_purchase: false
  product: #{NONEXISTENT_PRODUCT_NAME}
  limited_volume: false
  percent_coverage: false
  charge_code: 926
  call_id: DSNPA
-
  seller_paid: true
  automatic_purchase: false
  product: purchase_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 963
  call_id: DEALS
-
  seller_paid: true
  automatic_purchase: false
  product: extended_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 963
  call_id: DEALS
-
  seller_paid: true
  automatic_purchase: false
  product: market_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: true
  automatic_purchase: false
  product: total_protect
  limited_volume: false
  percent_coverage: false
  charge_code: 925
  call_id: DSMPS
-
  seller_paid: true
  automatic_purchase: false
  product: #{NONEXISTENT_PRODUCT_NAME}
  limited_volume: false
  percent_coverage: false
  charge_code: 927
  call_id: DSNPS
-
  seller_paid: false
  automatic_purchase: true
  product: purchase_protect
  limited_volume: false
  percent_coverage: true
  charge_code: 928
  call_id: DSNPB
-
  seller_paid: true
  automatic_purchase: true
  product: purchase_protect
  limited_volume: false
  percent_coverage: true
  charge_code: 927
  call_id: DSNPS
-
  seller_paid: false
  automatic_purchase: true
  product: purchase_protect
  limited_volume: true
  percent_coverage: false
  charge_code: 928
  call_id: DSNPB
-
  seller_paid: true
  automatic_purchase: true
  product: purchase_protect
  limited_volume: true
  percent_coverage: false
  charge_code: 927
  call_id: DSNPS
YAML
      records = YAML.load(records_as_yaml)
      ActiveRecord::Base.connection.execute('TRUNCATE charge_code_mappings RESTART IDENTITY')
      records.each do |record|
        attrs = {
          seller_paid: record['seller_paid'],
          automatic_purchase: record['automatic_purchase'],
          product: record['product'],
          charge_code: record['charge_code'],
          call_id: record['call_id'],
          limited_volume: record['limited_volume'],
          percent_coverage: record['percent_coverage']
        }
        create! attrs
      end
    end
  end
end
