# encoding: utf-8
require 'column_conversions'

class ReturnInvoiceCancellation < Hashie::Dash
  include ActiveModel::Validations
  include ActiveModel::Conversion
  include ColumnConversions

  BUSINESS_DAYS_OFFSET = 3

  property :return_invoice, required: true
  property :user, required: true
  property :reason
  property :send_email
  property :user_email

  delegate :promise_purchase, to: :return_invoice, allow_nil: true
  delegate :promise_offer, to: :promise_purchase, allow_nil: true
  delegate :pawn_information, :vehicle_information, to: :promise_offer
  delegate :country, to: :promise_offer

  validates :promise_purchase, :reason, presence: true

  def initialize(options = {})
    super options.symbolize_keys
  end

  def process
    return false unless valid?
    ActiveRecord::Base.transaction do
      cancel_return
      return_invoice.vra_cancellation_adjustments_job
      delete_drop_notice if Settings.return_cancellation_delete_drop_notice
      add_internet_note if Settings.salvage_internet_notes_enabled
      send_cancellation_notification
      send_cancellation_notification_to_accountant
    end
    true
  end

  def reinstate
    ActiveRecord::Base.transaction do
      reinstate_return
      return_invoice.vra_initiation_adjustments_job
      create_drop_notice
      return_invoice.add_internet_notes if Settings.salvage_internet_notes_enabled
      notify_return_reinstatement
    end
  end

  def persisted?
    false
  end

  private

  def cancel_return
    return_invoice.update_attributes(reason: reason, cancelling_user: user)
    return_invoice.archive
  end

  def delete_drop_notice
    DeleteDropNoticeJob.enqueue(return_invoice)
  end

  def add_internet_note
    AddInternetNoteJob.enqueue(
      vnum: return_invoice.vnum,
      pawn_location_initials: return_invoice.pawn_location.initials,
      sblu: return_invoice.sblu,
      work_order: return_invoice.work_order_number,
      notes: {
        added_by: 'ASTRSHIELD',
        text:     'ASTRSHIELD103 Vehicle Return Authorization (VRA) Cancelled/Expired',
        type:     'DEAL'
      }
    )
  end

  def send_cancellation_notification
    if promise_purchase.eligible_for_return?
      ReturnInvoiceMailer.cancelled_and_eligible_for_return(return_invoice, recepient).deliver_later
    else
      ReturnInvoiceMailer.cancelled_and_ineligible_for_return(return_invoice, recepient).deliver_later
    end
  end

  def send_cancellation_notification_to_accountant
    ReturnInvoiceMailer.cancellation_notification_to_accountant(return_invoice).deliver_later if return_fee_assessed?
  end

  def return_fee_assessed?
    return_invoice.new_product_fee.to_i > 0
  end

  def notify_return_reinstatement
    ReturnInvoiceMailer.return_reinstatement_notification(return_invoice, user).deliver_later
  end

  def reinstate_return
    return_invoice.restore
    return_invoice.update_attributes(expiration_date: expiration_date)
  end

  def expiration_date
    [business_days_from_now, invoice_expiration_date].max
  end

  def business_days_from_now
    current_time = Time.current
    BUSINESS_DAYS_OFFSET.business_days.after(current_time).to_date
  end

  def invoice_expiration_date
    return_invoice.expiration_date
  end

  def create_drop_notice
    CreateDropNoticeJob.enqueue(return_invoice)
  end

  def recepient
    user_email if update_and_send_email_to_user?
  end

  def update_and_send_email_to_user?
    if value_to_boolean(send_email)
      return_invoice.update_user_email(user_email)
      true
    else
      false
    end
  end
end
