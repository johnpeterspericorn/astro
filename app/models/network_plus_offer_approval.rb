# encoding: utf-8
class NetworkPlusOfferApproval
  def initialize(network_plus_offer, user, effective_date, approved_at = Time.current, mailer = NetworkPlusOfferMailer)
    @network_plus_offer = network_plus_offer
    @user = user
    @approved_at = approved_at
    @mailer = mailer
    @effective_date = effective_date
  end

  def approve
    accept_offer
    send_accepted_notification
  end

  def reject
    reject_offer
    send_rejected_notification
  end

  private

  attr_reader :network_plus_offer, :user, :approved_at, :mailer, :effective_date

  def accept_offer
    network_plus_offer.accept(user, approved_at)
  end

  def reject_offer
    network_plus_offer.reject!
  end

  def send_accepted_notification
    mailer.offer_accepted(network_plus_offer, user, approved_at.to_s, effective_date.to_s).deliver_later
  end

  def send_rejected_notification
    mailer.offer_rejection(network_plus_offer).deliver_later
  end
end
