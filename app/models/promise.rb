# encoding: utf-8
class Promise
  class Invalid < StandardError; end

  include Draper::Decoratable

  BASE_DURATION = 7
  DISTANCES = [250, 500, 360, 600, 800].freeze # distance1, distance2, distance3
  LENGTHS   = [0, 7, 14].freeze
  SELECTION_COMBINATIONS = DISTANCES.product(LENGTHS)

  attr_reader :promise_offer
  delegate :vnum, :flat_rate_price, :automatic_purchase?, :country, to: :promise_offer

  def initialize(promise_offer)
    @promise_offer = promise_offer
  end

  def price(distance, additional_days)
    walkup_price(distance, additional_days)
  end

  def discounted_price(distance, additional_days)
    full_price = price(distance, additional_days)
    price = full_price - promise_offer.discount_amount

    if price < 0
      0
    else
      price
    end
  end

  def self.price_method_name(miles, days)
    "price_#{miles}_by_#{days + BASE_DURATION}"
  end

  def self.price_methods
    SELECTION_COMBINATIONS.map { |(miles, days)| price_method_name(miles, days) }
  end

  SELECTION_COMBINATIONS.each do |(miles, days)|
    define_method price_method_name(miles, days) do
      price(miles, days)
    end
  end

  def distances
    i18n_distances.values
  end

  def distance1
    I18n.translate("offer_distances.#{country_code}.distance1")
  end

  def distance2
    I18n.translate("offer_distances.#{country_code}.distance2")
  end

  def distance3
    I18n.translate("offer_distances.#{country_code}.distance3")
  end

  private

  def prices_by_distance
    {
      distance1: promise_offer.promise_250,
      distance2: promise_offer.promise_500,
      distance3: promise_offer.promise_250
    }
  end

  def i18n_distances
    I18n.translate("offer_distances.#{country_code}")
  end

  def country_object
    Country.find_country_by_name(country || SupportedCountry::DEFAULT)
  end

  def country_code
    country_object.try(:alpha2)
  end

  def additional_day_price
    promise_offer.additional_day
  end

  def validate_distance_and_length(distance, additional_days)
    raise Promise::Invalid unless distances.include?(distance.to_i) && Promise::LENGTHS.include?(additional_days.to_i)
  end

  def walkup_price(distance, additional_days)
    validate_distance_and_length(distance, additional_days)

    distance_mapping = i18n_distances.invert[distance]
    base_price = prices_by_distance[distance_mapping]
    return unless base_price

    base_price + additional_days * additional_day_price if additional_day_price
  end

  def inspection_price
    promise_offer.inspection_bundle_price || 0
  end
end
