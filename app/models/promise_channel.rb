# encoding: utf-8
module PromiseChannel
  API                  = 'api'.freeze
  API_BUYER_PURCHASES  = 'api_buyer_promise_purchases'.freeze
  AUTOMATIC            = 'automatic'.freeze
  ASTRO                = 'astro'.freeze

  ALL = [API, API_BUYER_PURCHASES, AUTOMATIC, ASTRO].freeze
end
