# encoding: utf-8
class User < ActiveRecord::Base
  include EnsureAuthToken
  include PgSearch
  pg_search_scope :combined_search, against: [:username, :email, :authentication_token, :location_initials]

  paginates_per 25

  ROLES = [
    ADMIN = 'admin'.freeze,
    API_USER = 'api_user'.freeze,
    BUYER_USER = 'buyer_user'.freeze,
    FEATURE_FLAG_EDITOR = 'feature_flag_editor'.freeze,
    GUARANTEE_ADMIN_VIEWER = 'promise_admin_viewer'.freeze,
    GUARANTEE_ADMIN_EDITOR = 'promise_admin_editor'.freeze,
    USER_ADMIN_EDITOR = 'user_admin_editor'.freeze,
    INVOICEABLE = 'invoiceable'.freeze,
    KIOSK_USER = 'kiosk_user'.freeze,
    SUPER_USER = 'super_user'.freeze,
    VEHICLE_CREATOR = 'vehicle_creator'.freeze,
    LOSS_PREVENTION_VIEWER = 'loss_prevention_viewer'.freeze,
    FINANCE_ADMIN_VIEWER = 'finance_admin_viewer'.freeze,
    FLAT_RATE_INQUIRY_VIEWER = 'flat_rate_inquiry_viewer'.freeze,
    FLAT_RATE_EDITOR = 'flat_rate_editor'.freeze,
    DEALER_TRANSACTION_VIEWER = 'agreement_transaction_viewer'.freeze,
    DEALER_SCORE_VIEWER = 'agreement_score_viewer'.freeze,
    EXTERNAL_VEHICLE_ENTERER = 'external_vehicle_enterer'.freeze,
    EXTERNAL_VEHICLE_APPROVER = 'external_vehicle_approver'.freeze
  ].freeze

  devise :database_authenticatable, :astroheim_authenticatable, :timeoutable, :recoverable, :trackable

  has_paper_trail class_name: 'UserVersion', ignore: [:last_sign_in_at, :current_sign_in_at, :last_sign_in_ip, :current_sign_in_ip, :sign_in_count, :encrypted_password, :authentication_token, :updated_at, :reset_password_token, :reset_password_sent_at]

  has_many :return_invoices, as: :returning_user
  has_many :flat_rate_inquiries, dependent: :destroy

  validates :username, presence: true
  validates :email, presence: true, if: :super_user?
  validates :username, uniqueness: { message: 'Username has already been taken' }
  validates :roles, presence: { message: 'user must have at least one role (api, buyer, kiosk, super)' }
  validates :password, confirmation: true

  has_many :return_invoices, as: :returning_user

  scope :with_roles, ->(roles) {
    # This scope would look like:
    # kiosk_user = 't' OR api_user = 't'
    condition = roles
                .map { |r| "#{r} = 't'" }
                .join(' AND ')

    where(condition)
  }

  before_create :set_invoiceable
  after_create :create_payment_profile

  # This is a hook from the devise astroheim authentication strategy
  def after_astroheim_auth
    self.buyer_user = true if new_record?
  end

  # This overrides a devise model method. Decorating the existing implementation
  # by a check that the record's `active` flag is true.
  def active_for_authentication?
    super && active?
  end

  def pawn_access_number
    buyer_authorization.get_pawn_access_number if buyer_user?
  end

  def agreement_numbers
    if buyer_user?
      buyer_authorization.get_agreement_numbers
    else
      []
    end
  end

  def agreement_numbers_to_names
    if buyer_user?
      buyer_authorization.get_agreement_numbers_to_names
    else
      {}
    end
  end

  def purchased_vehicle?(sold_vehicle)
    agreement_numbers.include?(sold_vehicle.agreement_no)
  end

  def owns_offer?(promise_offer)
    offer_pawn_access_number = promise_offer.pawn_access_no

    if offer_pawn_access_number == '0'
      agreement_numbers.include?(promise_offer.agreement_no)
    else
      true
    end
  end

  def authorized_to_purchase?
    pawn_access_number.present?
  end

  def can_logout?
    !kiosk_user?
  end

  def roles
    ROLES.select { |role| self[role] }
  end

  private

  def buyer_authorization
    Astro::BUYER_AUTHORIZATION_CLASS.new(username: username)
  end

  def set_invoiceable
    self.invoiceable = true
  end

  def create_payment_profile
    PaymentService::Customer.delay.create(self)
  end
end
