# encoding: utf-8
class PendingConditions
  APPROVAL_REQUIRED            = 1
  BLACKLISTED                  = 2
  GUARANTEE_PRICE_EXCEEDED     = 3
  VEHICLE_PRICE_RATIO_EXCEEDED = 4
  VEHICLE_PRICE_EXCEEDED       = 5
  IF_BID                       = 6
  SELLER_BLACKLISTED           = 11
  VEHICLE_BLACKLISTED          = 14

  def initialize(promise_offer, options = {})
    @promise_offer = promise_offer
    @condition_types = Array(options.fetch(:conditions) { CONDITION_TYPES })
  end

  def met?
    met_condition.present?
  end

  def code
    @pending_reason_code ||= met_condition.code if met?
  end

  private

  def met_condition
    @met_condition ||= conditions.find(&:met?)
  end

  attr_reader :promise_offer, :condition_types

  def conditions
    condition_types.map { |c| c.new(promise_offer) }
  end

  PendingCondition = Struct.new(:promise_offer)

  class BlacklistedCondition < PendingCondition
    def code
      if vnum_blacklisted?
        VEHICLE_BLACKLISTED
      elsif seller_no_blacklisted?
        SELLER_BLACKLISTED
      else
        BLACKLISTED
      end
    end

    def met?
      agreementship_blacklisted? || vnum_blacklisted? || pawn_access_number_blacklisted? || seller_no_blacklisted?
    end

    private

    delegate :agreement_no, :vnum, :pawn_access_no, :seller_no, to: :promise_offer

    def agreementship_blacklisted?
      AgreementshipBlacklist.active.exists?(agreement_no: agreement_no)
    end

    def vnum_blacklisted?
      VinBlacklist.active.exists?(vnum: vnum)
    end

    def pawn_access_number_blacklisted?
      PawnAccessNumberBlacklist.active.exists?(pawn_access_no: pawn_access_no)
    end

    def seller_no_blacklisted?
      AgreementshipBlacklist.active.exists?(agreement_no: seller_no)
    end
  end

  class PromisePriceOverLimitCondition < PendingCondition
    GUARANTEE_PRICE_LIMIT_FOR_APPROVAL = 500

    def code
      GUARANTEE_PRICE_EXCEEDED
    end

    def met?
      promise_price_over_limit?
    end

    private

    delegate :promise_price, to: :promise_offer

    def promise_price_over_limit?
      promise_price > GUARANTEE_PRICE_LIMIT_FOR_APPROVAL
    end
  end

  class VehiclePriceOverLimitCondition < PendingCondition
    VEHICLE_PRICE_LIMIT_FOR_APPROVAL = 50_000

    def code
      VEHICLE_PRICE_EXCEEDED
    end

    def met?
      vehicle_price_over_limit?
    end

    private

    delegate :vehicle_purchase_price, to: :promise_offer

    def vehicle_price_over_limit?
      vehicle_purchase_price > VEHICLE_PRICE_LIMIT_FOR_APPROVAL
    end
  end

  class IfBidVehicleCondition < PendingCondition
    def code
      IF_BID
    end

    def met?
      Settings.if_bid_pending_enabled && promise_offer.if_bid?
    end
  end

  CONDITION_TYPES = [
    BlacklistedCondition,
    PromisePriceOverLimitCondition,
    VehiclePriceOverLimitCondition,
    IfBidVehicleCondition
  ].freeze
end
