# encoding: utf-8
class FlatRateOfferBatchApproval
  def initialize(batch, user, effective_date, approved_at = Time.current, mailer = FlatRateMailer)
    @batch          = batch
    @user           = user
    @approved_at    = approved_at
    @mailer         = mailer
    @effective_date = effective_date
  end

  def approve
    accept_offers
    send_accepted_notification
  end

  def reject
    reject_offers
    send_rejected_notification
  end

  private

  attr_reader :batch, :user, :approved_at, :mailer, :effective_date

  def accept_offers
    batch.flat_rate_offers.each do |offer|
      offer.accept(user, approved_at)
    end
  end

  def reject_offers
    batch.flat_rate_offers.each(&:reject)
  end

  def send_accepted_notification
    mailer.offer_accepted(batch, user, approved_at.to_s, effective_date.to_s).deliver_later
  end

  def send_rejected_notification
    mailer.offer_rejection(batch).deliver_later
  end
end
