# encoding: utf-8
require Rails.root.join('lib', 'astroheim', 'salvage_api')

class InternetNote
  def self.create(*args)
    new(*args).create
  end

  def initialize(attributes, client = Astroheim::SalvageApi::Client)
    @message_data = Astroheim::SalvageApi::AddNoteMessageData.new(attributes)
    @client       = client
  end

  def create
    client.call(:add_internet_notes, message: message_data.to_h)
  end

  private

  attr_reader :message_data, :client
end
