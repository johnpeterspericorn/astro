# encoding: utf-8
class ReturnInvoiceFilter
  class AgreementNumberFilter < Struct.new(:params)
    def search
      agreement_number = params[:agreement_number]
      PromisePurchase.for_agreement_number(agreement_number)
    end

    def attempted?
      params[:agreement_number].present?
    end

    def valid_search_options?
      attempted?
    end

    def error_message
      I18n.t('return_invoice.agreement_filter_error.missing_agreement')
    end
  end
end
