# encoding: utf-8
class ReturnInvoiceFilter
  class PawnAccessNumberFilter < Struct.new(:params)
    def search
      pawn_access_number = params[:pawn_access_number]
      PromisePurchase.for_agreement(pawn_access_number)
    end

    def attempted?
      params[:pawn_access_number].present?
    end

    def valid_search_options?
      attempted? && valid_pawn_access_number?
    end

    def valid_pawn_access_number?
      params[:pawn_access_number].to_i > 0
    end

    def error_message
      if valid_pawn_access_number?
        I18n.t('return_invoice.pawn_access_number_filter_error.missing_pawn_access_no')
      else
        I18n.t('return_invoice.pawn_access_number_filter_error.invalid_min_range')
      end
    end
  end
end
