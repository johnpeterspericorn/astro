# encoding: utf-8
class ReturnInvoiceFilter
  class VinNumberFilter < Struct.new(:params)
    def search
      vnum = params[:vnum_number]
      PromisePurchase.for_vnum_number(vnum)
    end

    def attempted?
      params[:vnum_number].present?
    end

    def valid_search_options?
      attempted?
    end

    def error_message
      I18n.t('return_invoice.vnum_filter_error.missing_vnum')
    end
  end
end
