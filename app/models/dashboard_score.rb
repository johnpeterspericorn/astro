# encoding: utf-8
class DashboardScore < ActiveRecord::Base
  validates :date, :buyer_num, :payment_kind, presence: true
end
