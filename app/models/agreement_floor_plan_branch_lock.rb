# encoding: utf-8
class AgreementFloorPlanBranchLock < ActiveRecord::Base
  delegate :floor_plan_company, to: :floor_plan_branch
  delegate :name, to: :floor_plan_branch, prefix: true
  delegate :name, to: :floor_plan_company, prefix: true

  validates :company_code, uniqueness: { scope: :agreement_no }
  before_save { self.company_code = company_code.split('-').last }

  def floor_plan_branch
    @floor_plan_branch ||=
      FloorPlanBranch.find_by_company_code_and_site_no(company_code, site_no)
  end
end
