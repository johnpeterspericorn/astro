# encoding: utf-8
require 'active_record_attribute_sanitizer'

class PriceFactor < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer
  belongs_to :promise_offer
end
