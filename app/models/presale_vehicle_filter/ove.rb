# encoding: utf-8
module PresaleVehicleFilter
  class OVE < Struct.new(:options)
    def filtered
      PresaleVehicle.ove.where(model: options[:vehicle_model]).order('year DESC')
    end

    def valid_search_options?
      attempted?
    end

    def attempted?
      options[:filter_type] == 'ove' || options[:vehicle_model].present?
    end

    def error_message
      I18n.t('presale_vehicle_filter.ove.error')
    end

    def range_for_page(page)
      data = filtered.page(page)
      "#{data.first.year} - #{data.last.year}"
    end
  end
end
