# encoding: utf-8
module PresaleVehicleFilter
  class VNUM < Struct.new(:options)
    def filtered
      if attempted_vnum_suffix?
        PresaleVehicle.where(['vnum LIKE ?', "%#{options[:vnum_suffix]}"])
      elsif attempted_vnum?
        PresaleVehicle.where(vnum: options[:vnum])
      end
    end

    def error_message
      I18n.t('presale_vehicle_filter.vnum.error')
    end

    def valid_search_options?
      attempted? && valid_vnum_query?
    end

    def attempted?
      options[:filter_type] == 'vnum' || attempted_vnum_suffix? || attempted_vnum?
    end

    def range_for_page(page)
      page.to_s
    end

    private

    def valid_vnum_query?
      valid_vnum_suffix? || valid_vnum?
    end

    def attempted_vnum_suffix?
      options[:vnum_suffix].present?
    end

    def attempted_vnum?
      options[:vnum].present?
    end

    def valid_vnum_suffix?
      options[:vnum_suffix] =~ /\A[a-z0-9]{6,}\z/i
    end

    def valid_vnum?
      options[:vnum] =~ /\A[a-z0-9]{17}\z/i
    end
  end
end
