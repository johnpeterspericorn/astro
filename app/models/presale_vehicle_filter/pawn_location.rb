# encoding: utf-8
module PresaleVehicleFilter
  class PawnLocation < Struct.new(:options)
    def filtered
      vehicles = pawn_location
                 .presale_vehicles
                 .where(lane_no: options[:lane_no])
                 .sort_by { |v| v.run_no.to_i }
      Kaminari.paginate_array(vehicles)
    end

    def error_message
      I18n.t('presale_vehicle_filter.pawn_location.error')
    end

    def valid_search_options?
      attempted? && valid_lane_no?
    end

    def range_for_page(page)
      data = filtered.page(page)
      "#{data.first.run_no} - #{data.last.run_no}"
    end

    def attempted?
      options[:filter_type] == 'pawn' || options[:location_initials].present?
    end

    def valid_lane_no?
      options[:lane_no].present?
    end

    private

    def pawn_location
      ::PawnLocation
        .with_purchases_enabled
        .find_by_initials(options[:location_initials])
    end
  end
end
