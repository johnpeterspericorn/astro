# encoding: utf-8
ExternalUser = Struct.new(:rate_change, :link_clicked_at, :link_clicked_user) do
  def process_link_click
    rate_change.link_clicked_at = link_clicked_at
    rate_change.link_clicked_user = link_clicked_user
    rate_change.save(validate: false)
    create_link_action_type
  end

  def create_link_action_type
    parameters = "viewed_at: #{link_clicked_at}, viewed_user: #{link_clicked_user.username}"
    ActionType.process(rate_change, '', rate_change.letter_sent, rate_change.emails, parameters)
  end
end
