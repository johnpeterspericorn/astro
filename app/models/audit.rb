# encoding: utf-8
class Audit < ActiveRecord::Base
  include AASM

  serialize :reason
  validates :emails, presence: true

  has_many :reasons, class_name: 'AuditReason', dependent: :destroy

  accepts_nested_attributes_for :reasons, allow_destroy: true

  def human_state_name
    state.humanize
  end

  aasm column: :state do
    state :opened, initial: true
    state :cleared
    state :flagged

    event :clear do
      transitions from: :opened, to: :cleared
    end

    event :flag do
      transitions from: :opened, to: :flagged
    end
  end
end
