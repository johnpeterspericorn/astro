# encoding: utf-8
require 'active_record_attribute_sanitizer'

class PawnInformation < ActiveRecord::Base
  include ActiveRecordAttributeSanitizer
  belongs_to :promise_offer
  belongs_to :pawn_location, primary_key: :initials, foreign_key: :location_initials
  has_many :flat_rate_offers, primary_key: :agreement_no, foreign_key: :agreement_no

  delegate :vnum, to: :promise_offer
  delegate :country, to: :pawn_location

  validates :lane_no, :run_no, :vehicle_purchase_price, :sale_no, presence: true

  has_paper_trail class_name: 'PromisePurchaseVersion'

  scope :for_agreement, ->(pawn_access_number) do
    buyer_authorization = Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number)
    agreement_numbers = buyer_authorization.get_agreement_numbers

    where('pawn_information.pawn_access_no = ? OR pawn_information.agreement_no IN (?)',
          pawn_access_number,
          agreement_numbers)
  end

  def flat_rate_offer
    flat_rate_offers.find { |flat_rate_offer| flat_rate_offer.try(:flat_rate).try(:country) == country }
  end

  def location_has_inspections?
    pawn_location && pawn_location.inspections_enabled?
  end

  def touch_vehicle_price(request_class = Sas::VehicleAdjustmentsRequest)
    request_class.new(vnum, agreement_no).load do |adjustments|
      new_price = adjustments.vehicle_price.to_f
      self.vehicle_purchase_price = new_price unless new_price.zero?
      save!
    end
  end
end
