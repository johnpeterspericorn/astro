class MarketFactor < ActiveRecord::Base
  default_scope { order(:id) }
  scope :ordered, ->() { order(:id) }

  COLORS = {
    'RED' => '#F58673',
    'WHITE' => '#FAF7EC',
    'GREEN' => '#9BD093'
  }

  belongs_to :market_index
end
