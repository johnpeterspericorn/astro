# encoding: utf-8
class ChargesStatus < ServiceStatus
  def down?
    super(&:submit!)
  rescue Astroheim::Charges::AddChargeError
    false
  end

  def self.scout
    Astroheim::Charge.new(Naught.build.new)
  end
end
