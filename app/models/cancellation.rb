# encoding: utf-8
require 'column_conversions'

class Cancellation
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ColumnConversions

  attr_reader :promise_purchase,
              :ineligibility_condition,
              :ineligible_for_repurchase,
              :skip_applying_credit,
              :reason,
              :user,
              :silent

  delegate :promise_offer,     to: :promise_purchase
  delegate :pawn_information, :vehicle_information, :inspection_purchase, to: :promise_offer

  validates :promise_purchase, :reason, presence: true

  def initialize(attributes = {})
    @promise_purchase        = PromisePurchase.find(attributes[:promise_purchase_id]) if attributes.key?(:promise_purchase_id)
    @ineligibility_condition   = attributes[:ineligibility_condition] || default_ineligibility_condition
    @ineligible_for_repurchase = value_to_boolean(attributes[:ineligible_for_repurchase])
    @skip_applying_credit      = value_to_boolean(attributes[:skip_applying_credit])
    @reason                    = attributes[:reason]
    @user                      = attributes[:user]
    @silent                    = value_to_boolean(attributes[:silent])
  end

  def process
    return false unless valid?

    ActiveRecord::Base.transaction do
      cancelled_purchase
      blacklist_vnum
      issue_refund
      destroy_purchases
      update_offer
      send_email if send_cancellation_confirmation?
    end

    true
  end

  def persisted?
    false
  end

  private

  def refundable?
    Settings.charges_enabled && !promise_offer.pending?
  end

  def destroy_purchases
    promise_purchase.destroy
    inspection_purchase.destroy if inspection_purchase
  end

  def cancelled_purchase
    @cancelled_purchase ||= CancelledPurchase.create_from!(
      reason,
      pawn_information,
      vehicle_information,
      promise_offer,
      promise_purchase,
      inspection_purchase,
      user
    )
  end

  def blacklist_vnum
    VinBlacklist.create!(vnum: promise_purchase.vnum) if ineligible_for_repurchase?
  end

  def update_offer
    promise_offer.update_attributes!(offer_attributes)
    promise_offer.process_cancellation!
  end

  def refund
    @promise_refund ||= PromiseRefund.new(promise_offer, cancelled_purchase)
  end

  def send_email
    mailer_method = ineligible_for_repurchase? ? :cancellation_confirmation_ineligible_for_repurchase : :cancellation_confirmation_eligible_for_repurchase
    promise_purchase_for_email = PromisePurchaseForEmail.new(promise_purchase).as_json
    CancellationMailer.public_send(mailer_method, promise_offer, promise_purchase_for_email, reason).deliver_later
  end

  def issue_refund
    PromiseChargeJob.enqueue(refund) if refundable?
    CancellationJobBuilder.enqueue_job(promise_offer) unless skip_applying_credit?
  end

  def offer_attributes
    attributes = { miles_selected: nil, days_selected: nil }
    attributes[:ineligibility_condition] = ineligibility_condition if ineligible_for_repurchase?
    attributes
  end

  def send_cancellation_confirmation?
    !@silent
  end

  def default_ineligibility_condition
    IneligibilityCondition::CANCELLED_PURCHASE
  end

  def ineligible_for_repurchase?
    @ineligible_for_repurchase
  end

  def skip_applying_credit?
    @skip_applying_credit
  end
end
