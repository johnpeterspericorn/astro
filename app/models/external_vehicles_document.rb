# encoding: utf-8
class ExternalVehiclesDocument < ActiveRecord::Base
  mount_uploader :document, DocumentUploader
end
