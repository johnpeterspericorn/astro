# encoding: utf-8
class BillOfSale < ActiveRecord::Base
  validates :vnum, :make, :year, :seller_agreement_name, :seller_address, :seller_address, :seller_postal_code, :seller_phone, presence: true
  validates :price, :odometer_reading, :seller_5mil, presence: true, numericality: true
  belongs_to :return_invoice
  belongs_to :promise_purchase
  delegate :tax_rate, to: :promise_purchase

  scope :for_vnum, ->(vnum) do
    where(vnum: vnum)
  end

  scope :for_return_invoice, ->(return_invoice_id) do
    where(return_invoice_id: return_invoice_id)
  end

  def price_with_fee
    (price || promise_purchase.vehicle_purchase_price) + fees + buy_fee
  end

  def tax_for_price
    ((tax_rate * price_with_fee) / 100) if tax_rate.present?
  end

  def total
    if persisted? && price.present? && tax.present?
      price.to_f + tax.to_f
    elsif (price_with_fee > 0) && tax_rate.present?
      (price_with_fee.to_f + tax_for_price.to_f)
    end
  end

  def fees
    promise_purchase.fees.reimbursable_fees.total || 0.0
  end

  def buy_fee
    promise_purchase.adjustments_buy_fee || 0.0
  end
end
