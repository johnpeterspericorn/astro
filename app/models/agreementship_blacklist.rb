# encoding: utf-8
class AgreementshipBlacklist < ActiveRecord::Base
  scope :active, -> { where('expiration_date IS NULL OR expiration_date >= ?', Date.current) }

  def expired?
    expiration_date && expiration_date.past?
  end
end
