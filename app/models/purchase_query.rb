# encoding: utf-8
class PurchaseQuery
  attr_reader :vnum
  attr_reader :coupon_code

  def initialize(vnum, options, promise_offer = PromiseOffer.by_vnum(vnum).last)
    @vnum = vnum
    @promise_offer = promise_offer
    @promise_options = options.fetch(:promise_options) { 'none' }
    @inspection_selected = options[:inspection]
    @coupon_code = options[:coupon_code]
  end

  def eligible_for_inspection?
    promise_offer.reload.eligible_for_inspection?
  end

  def inspection_selected?
    @inspection_selected == 'true'
  end

  def inspection_only?
    promise_options == 'inspection_only'
  end

  def inspection_purchase
    @inspection_purchase ||= promise_offer.build_inspection_purchase(price: inspection_price)
  end

  def promise_selected?
    promise_options != 'none'
  end

  def miles
    separated_options.first
  end

  def days
    separated_options.last
  end

  private

  attr_reader :promise_offer, :promise_options

  def promised_or_selected?
    promise_offer.purchased? || (promise_selected? && !inspection_only?)
  end

  def separated_options
    promise_options.split('_')
  end

  def inspection_price
    if promised_or_selected?
      promise_offer.inspection_bundle_price
    else
      promise_offer.inspection_only_price
    end
  end
end
