# encoding: utf-8
Address = Struct.new(:street, :suite, :city, :state, :zipcode, :verify_flag) do
  EASYPOST_ERRORS_TO_IGNORE = [
    'Default address: The address you entered was found but more information is needed (such as an apartment, suite, or box number) to match to a specific address.'
  ].freeze
  # http://rubular.com/r/XxXvbBuhjX
  POST_OFFICE_BOX_EXPRESSION = /p(ost)?[\s.]*o(ffice)?[\s.]+box\b/i

  include ActiveModel::Validations

  validate :is_verifiable, :is_not_a_post_office_box

  def to_easypost_address(attributes = {})
    create_address = verify_flag ? 'create_and_verify' : 'create'
    EasyPost::Address.public_send(create_address, build_address(attributes))
  end

  def post_office_box?
    !!(street =~ POST_OFFICE_BOX_EXPRESSION)
  end

  private

  def is_verifiable
    to_easypost_address if Settings.address_verification_enabled
  rescue EasyPost::Error => error
    errors.add(:base, error.message) unless EASYPOST_ERRORS_TO_IGNORE.include?(error.message)
  end

  def is_not_a_post_office_box
    errors.add(:base, 'Address must not be a post office box') if post_office_box?
  end

  def build_address(attributes)
    {
      name:    attributes[:name],
      company: attributes[:company],
      street1: street,
      street2: suite,
      city:    city,
      state:   state,
      zip:     zipcode
    }
  end
end
