# encoding: utf-8
class PresaleVehicle < ActiveRecord::Base
  include VehicleMixins::DomainConcerns # defines #eligible? and #ineligible?

  belongs_to :pawn_location, primary_key: :initials, foreign_key: :location_initials
  belongs_to :ineligibility_condition_record, class_name: IneligibilityCondition, foreign_key: :ineligibility_condition

  scope :at_pawn_location, ->(pawn_location_initials) {
    where(location_initials: pawn_location_initials)
  }

  delegate :country, to: :pawn_location

  scope :ove, -> { where(ove: true) }

  scope :pawn_ordered, -> do
    includes(:pawn_location)
      .order("pawn_locations.name, to_number(lane_no, '99'), to_number(run_no, '9999')")
  end

  def self.ove_models
    ove.order(:model).uniq.pluck(:model)
  end

  def self.ove_makes
    ove.order(:make).uniq.pluck(:make)
  end

  def ineligibility_notice
    ineligibility_condition_record.notice if ineligibility_condition_record
  end

  def mobile_ineligibility_notice
    return unless ineligibility_condition
    default_condition = IneligibilityCondition::INVALID_PRICING
    mobile_notices = I18n.t('ineligibility_conditions.mobile')
    mobile_notices[ineligibility_condition] || mobile_notices[default_condition]
  end

  def location_name
    pawn_location.try(:name)
  end

  def country_code
    country_object.try(:alpha2)
  end

  def eligible_for_purchase?
    eligible?
  end

  paginates_per Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PAGE

  def self.lanes_for_pawn(pawn_initials = Astro::DEFAULT_AUCTION_LOCATION)
    at_pawn_location(pawn_initials)
      .order(:lane_no)
      .uniq
      .pluck(:lane_no)
  end

  private

  def country_object
    Country.find_country_by_name(country || SupportedCountry::DEFAULT)
  end
end
