# encoding: utf-8
class PromiseSelectionPolicy
  def initialize(user, _options = {})
    @user = user
  end
  attr_accessor :user

  def authorized_for_index?(pawn_access_number)
    return false if restricted_pawn_access_number?(pawn_access_number)

    if user.buyer_user?
      user.pawn_access_number == pawn_access_number
    else
      Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number).has_contact_details?
    end
  end

  def restricted_pawn_access_number?(pawn_access_number = nil)
    pawn_access_number.to_i <= 0
  end
end
