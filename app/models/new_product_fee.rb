# encoding: utf-8
class NewProductFee < ActiveRecord::Base
  METHODS_RENDERED = [].freeze
  FIELDS_RENDERED = [:id, :agreement_no, :seller_no, :seller_paid, :timeframe, :count, :fee, :updated_at].freeze
  has_paper_trail class_name: 'NewProductFeeVersion'

  validates :timeframe, :count, presence: true

  def self.get_fee_records(seller_paid, seller_no, agreement_no)
    options = seller_paid ? { seller_no: seller_no } : { agreement_no: agreement_no }
    options.update(options.keys.first => [nil, '']) if NewProductFee.where(options.merge!(seller_paid: seller_paid)).blank?
    NewProductFee.where(options).order(:count)
  end

  def as_json(_options = {})
    super(methods: NewProductFee::METHODS_RENDERED,
          only: NewProductFee::FIELDS_RENDERED)
  end
end
