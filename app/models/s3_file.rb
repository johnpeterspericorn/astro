# encoding: utf-8
require 'aws-sdk'

class S3File
  DEFAULT_REGION = 'us-west-2'.freeze

  def initialize(remote_file_name)
    @remote_file_name = remote_file_name
    configure
  end

  def upload(local_file)
    s3_object(@remote_file_name).upload_file(local_file)
  end

  def presigned_url(expires_in = 1.week)
    @s3_object.presigned_url(:get, expires_in: expires_in)
  end

  private

  def s3_object(remote_path)
    @s3_object ||= s3_bucket.object(remote_path)
  end

  def s3_bucket
    s3 = Aws::S3::Resource.new(region: DEFAULT_REGION)
    s3.bucket(bucket_name)
  end

  def bucket_name
    "astro-#{Rails.env}"
  end

  def configure
    Aws.config.update(
      region: DEFAULT_REGION,
      credentials: Aws::Credentials.new(ENV['S3_KEY'], ENV['S3_SECRET'])
    )
  end
end
