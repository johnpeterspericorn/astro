# encoding: utf-8
module Sas
  class FlatRateQuoteResponse < Response
    property_from_camel :Message
    property_from_camel :EligCode
    property_from_camel :OfferToken
    property_from_camel :ResultType
    property :rc

    property :flat_price, from: :Result1, with: ->(price) { price.to_f }
    property :number_of_days, from: :Result2, with: ->(price) { price.to_i }
    property :number_of_miles, from: :Result3, with: ->(price) { price.to_i }
    property :transportation_reimbursement, from: :Result4, with: ->(price) { price.to_f }

    def miles
      Array(number_of_miles)
    end

    def days
      Array(number_of_days)
    end

    def price(_, _)
      flat_price
    end

    def flat_rate
      true
    end
  end
end
