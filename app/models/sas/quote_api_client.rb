# encoding: utf-8
module Sas
  class QuoteApiClient < Struct.new(:request)
    ENDPOINT_URL = EnvironmentConfig.fetch('QUOTE_API_ENDPOINT')

    def post
      QuotesLogger.info "Fetching quote for #{request.vnum}"
      QuotesLogger.info "Request: #{request_body}"
      api_request.run
      QuotesLogger.info "Response: #{api_request.response.body}"
      processed_response
    end

    private

    def api_request
      @api_request ||= Typhoeus::Request.new(
        ENDPOINT_URL,
        method: :post,
        timeout: Integer(ENV.fetch('MAS_TIMEOUT')),
        body: request_body
      )
    end

    def request_body
      {
        UserName:   ENV.fetch('QUOTE_API_USER'),
        Password:   ENV.fetch('QUOTE_API_PASSWORD'),
        VNUM:        request.vnum,
        Seller5Mil: request.seller_5m,
        Buyer5Mil:  request.buyer_5m,
        SalePrice:  request.sale_price,
        Location:   request.location_initials
      }.to_query
    end

    def processed_response
      JSON.parse(api_request.response.body).fetch('outputParameters')
    rescue JSON::ParserError => e
      QuotesLogger.error "Failure for #{request.vnum}: #{e.message}"
      e.extend(ApiError)
      raise
    end
  end
end
