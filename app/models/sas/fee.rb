# encoding: utf-8
module Sas
  class Fee
    def self.build(attributes)
      [attributes].flatten.map do |attrs|
        new(attrs)
      end
    end

    AMOUNT_KEY = 'Amount'.freeze
    BUCKET_KEY = 'Bucket'.freeze
    CODE_KEY = 'Code'.freeze
    FEE_KEY = 'Fee'.freeze

    attr_reader :amount, :bucket, :code, :name

    def initialize(attributes)
      @amount = attributes[AMOUNT_KEY].to_i
      @bucket = attributes[BUCKET_KEY]
      @code = attributes[CODE_KEY]
      @name = attributes[FEE_KEY]
    end

    def reimbursable?
      bucket == 'Reimbursable'
    end

    def nonreimbursable?
      bucket == 'NonReimbursable'
    end
  end
end
