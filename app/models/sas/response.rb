# encoding: utf-8
module Sas
  class Response < Hashie::Dash
    include Hashie::Extensions::Dash::PropertyTranslation
    include Hashie::Extensions::IgnoreUndeclared

    def initialize(args)
      super args.symbolize_keys
    end

    def self.property_from_camel(name, options = {})
      property name.to_s.underscore.to_sym, options.merge(from: name)
    end
  end
end
