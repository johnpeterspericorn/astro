# encoding: utf-8
module Sas
  class VehicleAdjustmentsApiClient
    class VehicleNotFound < StandardError; end

    ENDPOINT_URL = 'https://.ondemand.mas.com/MASBIWS/json/storedProcesses/MHA//AdjustmentsAPI'.freeze

    def post(parameters)
      request = api_request(
        :post,
        vnum:   parameters.vnum,
        buyer: parameters.agreement_number
      )

      request.run
      process(request.response.body)
    end

    private

    def api_request(method, params)
      Typhoeus::Request.new(
        ENDPOINT_URL,
        method: method,
        timeout: Integer(ENV.fetch('MAS_TIMEOUT')),
        body:   params.to_query
      )
    end

    def process(raw_response)
      response = JSON.parse(raw_response).fetch('outputParameters')
      msg = response['msg']

      raise VehicleNotFound if (msg == 'VehicleNotFound') || msg.empty?

      response
    rescue JSON::ParserError => e
      e.extend(ApiError)
      raise
    end
  end
end
