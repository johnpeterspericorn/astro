# encoding: utf-8
module Sas
  module FeeParser
    DS_FEE_CODE = 'DEALB'.freeze

    def self.parse(fees_string)
      parse_fees(fees_string)
    end

    def self.parse_fees(fees_string, delimeter = '.')
      fees_string
        .split(delimeter)
        .map { |fee_string| parse_attributes(fee_string) }
    end

    def self.parse_attributes(attributes_string, delimeter = ',')
      attributes_string
        .split(delimeter)
        .each_with_object({}) do |pair_string, attributes|
          key, value = parse_pair(pair_string)
          key = remove_digits_from_fee_key(key)
          attributes[key] = value
        end
    end

    def self.parse_pair(pair_string, delimeter = ':')
      pair_string
        .split(delimeter)
        .map(&:strip)
    end

    def self.remove_digits_from_fee_key(key)
      key.gsub(/Fee\d+/, 'Fee')
    end
  end
end
