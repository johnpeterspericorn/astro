# encoding: utf-8
module Sas
  class VehicleAdjustmentsResponse < Response
    property_from_camel :OVESuccessFee,      required: true
    property_from_camel :OVEFacilitationFee, required: true
    property_from_camel :BuyNet,             required: true
    property_from_camel :VehiclePrice,       required: true
    property_from_camel :BuyFee,             required: true
    property_from_camel :HowPaid,            required: true
    property_from_camel :Description,        required: true
    property_from_camel :PaymentStatus,      required: true
    property_from_camel :CertifiedFee,       required: true
    property_from_camel :FloorAgency,        required: true
    property_from_camel :TotalAdjustments,   required: true
    property_from_camel :Msg,                required: true

    def floor_plan_company(model = FloorPlanCompany)
      @floor_plan_company ||= model.find_by_company_code(floor_agency)
    end

    def paid_by_floor_plan?
      how_paid == 'F' || how_paid == 'G'
    end

    def paid_by_buyer?
      payment_status == 'PROBABLY BUYER PAID' || payment_status == 'BUYER PAID'
    end

    def fees(parser = FeeParser, builder = Fee, collection = Fees)
      fees_attrs = parser.parse(description)
      fees = builder.build(fees_attrs)
      collection.new(fees)
    end
  end
end
