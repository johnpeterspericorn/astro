# encoding: utf-8
module Sas
  class QuoteResponse < Response
    class InvalidQuoteType < StandardError; end

    property_from_camel :Message
    property_from_camel :EligCode
    property_from_camel :OfferToken
    property_from_camel :ResultType
    property :rc

    property :base_price_250, from: :Result1, with: ->(price) { price.to_f }
    property :base_price_500, from: :Result2, with: ->(price) { price.to_f }
    property :additional_day_price, from: :Result3, with: ->(price) { price.to_f }
    property :transportation_reimbursement_per_100, from: :Result4, with: ->(price) { price.to_f }

    def initialize(args)
      super args.symbolize_keys
    end

    def self.build(raw_response)
      case raw_response.fetch('ResultType')
      when 'Q'
        new(raw_response)
      when 'S', 'B'
        FlatRateQuoteResponse.new(raw_response)
      else
        raise InvalidQuoteType
      end
    end

    def miles
      Promise::DISTANCES - [360]
    end

    def days
      [7, 14, 21]
    end

    def price(miles, days)
      base_prices[miles] + additional_day_price * (days - base_days)
    end

    def flat_rate
      false
    end

    private

    def base_prices
      {
        250 => base_price_250,
        500 => base_price_500,
        600 => base_price_250,
        800 => base_price_500
      }
    end

    def base_days
      7
    end
  end
end
