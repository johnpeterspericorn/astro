# encoding: utf-8
module Sas
  class QuoteRequest < Hashie::Dash
    property :vnum, required: true
    property :seller_5m, required: true
    property :buyer_5m, required: true
    property :sale_price, required: true
    property :location_initials, required: true

    attr_reader :quotes

    def load
      @quotes = QuoteResponse.build(api_client.post)
    rescue ApiError
      Rails.logger.error 'An error with the API has prevented us from processing this request.'
    end

    private

    def api_client
      QuoteApiClient.new(self)
    end
  end
end
