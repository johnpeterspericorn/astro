# encoding: utf-8
module Sas
  class VehicleAdjustmentsRequest
    attr_reader :vnum, :agreement_number, :adjustments

    def initialize(vnum, agreement_number, api_client = VehicleAdjustmentsApiClient.new)
      @vnum           = vnum
      @agreement_number = agreement_number
      @api_client    = api_client
    end

    def load
      raw_response = api_client.post(self)
      @adjustments = VehicleAdjustmentsResponse.new(raw_response)

      yield(adjustments) if block_given?
    rescue VehicleAdjustmentsApiClient::VehicleNotFound
      Rails.logger.error "No vehicle adjustments found for VNUM: #{vnum}"
      raise
    rescue ApiError
      Rails.logger.error 'An error with the API has prevented us from processing this request.'
      raise
    end

    private

    attr_reader :api_client
  end
end
