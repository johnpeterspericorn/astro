# encoding: utf-8
require 'forwardable'

module Sas
  class Fees
    include Enumerable
    extend Forwardable
    def_delegators :fees, :each, :empty?

    def initialize(fees = nil)
      @fees = Array(fees)
    end

    def reimbursable_fees
      self.class.new(fees.select(&:reimbursable?))
    end

    def nonreimbursable_fees
      self.class.new(fees.reject(&:reimbursable?))
    end

    def total
      fees.map(&:amount).reduce(:+).to_f
    end

    private

    attr_reader :fees
  end
end
