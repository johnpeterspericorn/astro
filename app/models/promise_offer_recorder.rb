# encoding: utf-8
class PromiseOfferRecorder
  def self.record_offers(vehicles)
    vehicles.map { |vehicle| record_offer(vehicle) }.compact
  end

  def self.record_offer(vehicle)
    new(vehicle).record_offer
  end

  attr_reader :previous_offers, :replicator, :sold_vehicle

  def initialize(vehicle,
                 previous_offers = existing_offers(vehicle),
                 replicator = ReplicateSoldVehicle.new(vehicle))
    @sold_vehicle = vehicle
    @previous_offers = previous_offers
    @replicator = replicator
  end

  def record_offer
    if previous_offers.empty?
      replicator.call
    else
      updated_offers = previous_offers.map do |previous_offer|
        update_offer_attributes(previous_offer, replicator)
      end

      updated_offers.last
    end
  end

  private

  def update_offer_attributes(offer, replicator)
    if offer.updated_at < sold_vehicle.updated_at && offer.promise_purchase.blank?
      offer.destroy
      replicator.call
    else
      offer
    end
  end

  def existing_offers(vehicle)
    if vehicle.automatic_purchase?
      existing_offer_records(vehicle).with_purchase
    else
      existing_offer_records(vehicle)
    end
  end

  def existing_offer_records(vehicle)
    PromiseOffer
      .joins(:vehicle_information, :pawn_information)
      .where(
        pawn_information: {
          sblu: vehicle.sblu,
          work_order_number: vehicle.work_order_number,
          location_initials: vehicle.location_initials
        },
        vehicle_information: { vnum: vehicle.vnum })
  end
end
