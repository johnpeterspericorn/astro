# encoding: utf-8
class VickiStatus < ServiceStatus
  def down?
    super(&:five_million_numbers_to_names)
  end

  def self.scout
    auth_service = AstroheimAuthorizationService::AuthorizationService.new(Astro::AUTHORIZATION_HOST, Astro::AUTHORIZATION_PORT, Astro::APP_ID)
    AstroheimAuthorizationService::UserAuthorization.new(auth_service, universal_id: '100686866')
  end
end
