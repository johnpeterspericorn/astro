# encoding: utf-8
class FilterQueryData < Hashie::Dash
  property :badge_number
  property :pawn_access_number
  property :vnums
  property :agreement_numbers
  property :agreement_number

  attr_accessor :buyer_authorization

  def initialize(options = {})
    super options.symbolize_keys.slice(*self.class.properties)
    self.buyer_authorization = Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number)
    self.vnums                = [options[:vnum]] if options[:vnum]
  end

  def filter_method
    case
    when badge_number.present?
      :badge_number
    when pawn_access_number.present?
      :pawn_access_number
    when agreement_number.present?
      :agreement_number
    end
  end

  def filtered_by?(method)
    filter_method == method
  end

  def vehicles_for_display
    return [] if filter_method.blank?

    vehicles = case filter_method
               when :badge_number
                 SoldVehicle.where(badge_no: badge_number)
               when :pawn_access_number
                 SoldVehicle.for_pawn_access_number(self)
               when :agreement_number
                 SoldVehicle.where(agreement_no: agreement_number)
               end

    vehicles = vehicles.where(vnum: vnums) if vnums.present?
    vehicles
  end
end
