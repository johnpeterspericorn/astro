# encoding: utf-8
require 'date_utils'
class NetworkPlusOffer < ActiveRecord::Base
  include DateUtils
  include AASM

  after_create :capture_action_type_parameters
  after_update :capture_status

  belongs_to :offering_user, class_name: 'User'
  belongs_to :accepting_user, class_name: 'User'

  validates :existing_network_rate, :network_plus_surcharge, numericality: { greater_than: 0 }
  validate :expiration_date_input

  scope :for_agreement_no, lambda { |agreement_number|
    where(agreement_no: agreement_number.to_s)
  }

  scope :active, lambda {
    where('expiration_date > ?', Time.current)
  }

  has_paper_trail

  DEFAULT_EXPIRY_DAYS = 30
  DEFAULT_MILES = 360
  DEFAULT_COUNTRY = SupportedCountry::DEFAULT
  NOW = DateUtils.now
  DEFAULT_NETWORK_PLUS_SURCHARGE = 35
  LIMITS = { max_odometer_limit: '250,000 miles', max_vehicle_value: '$100,000' }.freeze
  UNITS = 'Miles'.freeze

  def agreement_info
    AgreementInformation.for_agreement_no(agreement_no).first
  end

  def accept(user, time)
    update_attributes!(
      accepting_user: user,
      accepted_at:    time,
      accepted: true
    )
    approve!
  end

  def price
    existing_network_rate + network_plus_surcharge
  end

  def miles_selected
    DEFAULT_MILES
  end

  def apply_change(change)
    update_attributes!(price: change.price)
  end

  aasm column: :state do
    state :unconfirmed, initial: true
    state :approved
    state :rejected
    state :confirmed

    event :approve do
      transitions from: :confirmed, to: :approved
    end

    event :reject do
      transitions from: :confirmed, to: :rejected
    end

    event :confirm do
      transitions from: :unconfirmed, to: :confirmed
    end
  end

  def send_email
    NetworkPlusOfferMailer.offer_confirmation(self).deliver_now
  end

  def capture_email_sent
    update_attributes(letter_sent: NOW)
    ActionType.for_action_type(self).first.update_attributes!(date_sent: letter_sent)
  end

  def eligible_for_acceptance?
    !expired? && confirmed?
  end

  def default_expiration_date
    (NOW + DEFAULT_EXPIRY_DAYS.days).end_of_day
  end

  def effective_date
    1.day.from_now.utc.to_date
  end

  def max_model_year
    (Time.zone.today - 19.years).year
  end

  def expiration_date_input
    errors.add(:expiration_date, 'must be within 30 days from now') unless (NOW...default_expiration_date).cover?(expiration_date)
  end

  def limits
    LIMITS
  end

  def units
    UNITS
  end

  def capture_action_type_parameters
    ActionType.process(self, state, letter_sent, emails, "existing_network_rate : #{existing_network_rate}, network_plus_surcharge : #{network_plus_surcharge}")
  end

  def capture_status
    action_type = ActionType.for_action_type(self).first
    action_type.update_attributes!(status: state) if action_type.present?
  end

  private

  def expired?
    expiration_date && expiration_date.past?
  end
end
