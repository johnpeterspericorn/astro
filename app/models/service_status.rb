# encoding: utf-8
class ServiceStatus
  def self.up?(service = scout)
    new(service).up?
  end

  def self.down?(service = scout)
    new(service).down?
  end

  def initialize(service)
    @service = service
  end

  def up?
    !down?
  end

  def down?
    yield(service)
    false
  rescue SocketError, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Timeout::Error
    true
  end

  private

  attr_reader :service
end
