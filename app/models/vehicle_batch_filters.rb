class VehicleBatchFilters
  include ActiveModel::Model
  attr_accessor :agreement_no, :vnum, :batch_id, :status, :user_id

  def initialize(args={})
    args.each do |k,v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end unless args.nil?
  end
end
