# encoding: utf-8
class CancellationJobBuilder < ChargesJobBuilder
  def self.enqueue_job(offer)
    new(offer).enqueue
  end

  def job
    if charge_bsc?
      BscCancellationJob
    elsif charge_sows?
      SowsCancellationJob
    elsif charge_g2gws_orders?
      G2gwsOrdersApiCancellationJob
    end
  end

  private

  def charge?
    return false if charge_sows? && !promise_charge_present?
    !promise_offer.bypass_charge?
  end

  def promise_charge_present?
    promise_offer.charge_present_in_ods?
  end
end
