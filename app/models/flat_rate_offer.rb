# encoding: utf-8
class FlatRateOffer < ActiveRecord::Base
  include AASM

  belongs_to :flat_rate
  belongs_to :flat_rate_offer_batch
  belongs_to :offering_user, class_name: 'User'
  belongs_to :accepting_user, class_name: 'User'

  delegate :pawn_access_no, :buyer_information, :user_email, to: :flat_rate_offer_batch
  delegate :agreementship_name, to: :agreement_information
  delegate :no_fee_returns, :lv_tier, :closed_factory_exclusion, to: :flat_rate, allow_nil: true

  validate :unique_agreement_for_country, on: :create
  validates :price, numericality: { greater_than: 0 }
  validates :flat_rate_id, :price, presence: true
  validate :min_price_range, if: -> { flat_rate.present? && offering_user.present? && offering_user.flat_rate_inquiry_viewer? }

  before_validation :copy_fields
  before_create :copy_country

  has_paper_trail

  def agreement_information
    @agreement_information ||=
      AgreementInformation.build_with_defaults(pawn_access_no, agreement_no)
  end

  def accept(user, time)
    update_attributes!(
      accepting_user: user,
      accepted_at:    time
    )
    approve!
  end

  def apply_change(change)
    update_attributes!(price: change.price)
  end

  aasm column: :state do
    state :unapproved, initial: true
    state :approved
    state :rejected

    event :approve do
      transitions from: :unapproved, to: :approved
    end

    event :reject do
      transitions from: :unapproved, to: :rejected
    end
  end

  def selected_flat_rate
    @flat_rate_attribute ||= FlatRateAttribute.new(flat_rate: flat_rate, miles: miles_selected, psi: psi_eligible, palv: palv)
  end

  def target_price
    flat_rate.present? ? selected_flat_rate.target_price : self[:target_price]
  end

  def target_price_for_miles
    flat_rate.present? ? selected_flat_rate.target_price_for_miles : self[:target_price_for_miles]
  end

  private

  def copy_fields
    self.agreement_no = flat_rate.agreement_no unless agreement_no.present?
    return unless flat_rate.present?
    copy_country
    copy_price
  end

  def copy_country
    self.country = flat_rate.country
  end

  def copy_price
    self.target_price = selected_flat_rate.target_price
    self.target_price_for_miles = selected_flat_rate.target_price_for_miles
  end

  def min_price_range
    errors.add(:base, I18n.t('flat_rates.errors.min_price_range')) if price < selected_flat_rate.target_price
  end

  def unique_agreement_for_country
    existing_flat_rate_offer = FlatRateOffer.where(agreement_no: agreement_no, country: country)

    errors.add(:agreement_no, 'This agreement already has an offer') if existing_flat_rate_offer.present?
  end
end
