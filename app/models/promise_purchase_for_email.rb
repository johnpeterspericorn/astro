# encoding: utf-8
class PromisePurchaseForEmail
  attr_reader :pawn_location, :created_at, :distance, :duration, :expiration_date, :price

  def initialize(promise_purchase)
    promise_purchase = promise_purchase.decorate
    @pawn_location = promise_purchase.pawn_location.name
    @created_at = promise_purchase.created_at.to_s(:slashed)
    @distance = promise_purchase.distance_selected_with_units
    @duration = promise_purchase.duration
    @expiration_date = promise_purchase.expiration_date
    @price = promise_purchase.price
  end
end
