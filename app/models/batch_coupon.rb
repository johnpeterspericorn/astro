# encoding: utf-8
class BatchCoupon < ActiveRecord::Base
  belongs_to :coupon
  belongs_to :coupon_batch
end
