# encoding: utf-8
class VehicleEligibility < Struct.new(:vehicle, :seller_no)
  include ActiveModel::Serialization

  delegate :vnum, to: :vehicle

  MISMATCHED_SELLER_NUMBER_INELIGIBILITY_CONDITION = 10

  def eligible_for_purchase?
    seller_matches? && vehicle.eligible_for_purchase?
  end

  def vehicle
    super.decorate
  end

  def ineligibility_reason
    if seller_matches?
      vehicle.ineligibility_reason
    else
      IneligibilityCondition.notice_for_condition(
        MISMATCHED_SELLER_NUMBER_INELIGIBILITY_CONDITION
      )
    end
  end

  private

  def seller_matches?
    seller_no == vehicle.seller_no
  end
end
