# encoding: utf-8
class PurchasedPromiseForm
  include ActiveModel::Model

  VEHICLE_INFORMATION_ATTRIBUTES  = VehicleInformation.sanitized_attribute_names
  AUCTION_INFORMATION_ATTRIBUTES  = PawnInformation.sanitized_attribute_names

  delegate :promise_offer, to: :promise_purchase
  delegate :vehicle_information, to: :promise_offer
  delegate :pawn_information, to: :promise_offer
  delegate :return_invoice, to: :promise_purchase
  delegate(*VEHICLE_INFORMATION_ATTRIBUTES, to: :vehicle_information)
  delegate(*AUCTION_INFORMATION_ATTRIBUTES, to: :pawn_information)
  delegate :expiration_date, :receive_by_date, to: :return_invoice, prefix: true, allow_nil: true
  delegate :promised_at, :max_return_time, :miles_selected, :return_initiated?, :kind, to: :promise_purchase
  delegate :emails, :transport_reimbursement, :promise_price, :days_selected, :miles_selected, to: :promise_offer
  delegate :distance_units, to: :decorated_promise_purchase
  delegate :partner, to: :promise_purchase

  def initialize(promise_purchase)
    @promise_purchase = promise_purchase
  end

  def update(attributes = {})
    @attributes = attributes
    assign_attributes

    return false unless valid?

    begin
      ActiveRecord::Base.transaction do
        promise_offer.save!
        vehicle_information.save!
        promise_purchase.save!
        pawn_information.save!
      end
      send_confirmation
      true
    rescue ActiveRecord::StatementInvalid, ActiveRecord::RecordNotSaved, ActiveRecord::RecordInvalid, ArgumentError => e
      Rails.logger.error "Error message: #{e.message}"
      return false
    end
  end

  def validation_errors
    promise_purchase_errors + vehicle_information_errors + pawn_information_errors + promise_offer_errors
  end

  def persisted?
    true
  end

  def send_confirmation
    promise_purchase.send_confirmation(promise_offer.emails) unless promise_offer.pending?
  end

  def odometer_limit
    return_certificate = return_invoice.return_certificate
    return_certificate.odometer_limit
  end

  private

  def valid?
    vehicle_information.valid? && promise_purchase.valid? && pawn_information.valid? && promise_offer.valid?
  end

  def decorated_promise_purchase
    @promise_purchase.decorate
  end

  def promise_purchase_errors
    promise_purchase.errors.full_messages
  end

  def vehicle_information_errors
    vehicle_information.errors.full_messages
  end

  def promise_offer_errors
    promise_offer.errors.full_messages
  end

  def pawn_information_errors
    pawn_information.errors.full_messages
  end

  def assign_attributes
    promise_offer_attributes
    promise_purchase_attributes
    vehicle_information_attributes
    pawn_information_attributes
  end

  def promise_offer_attributes
    promise_offer.assign_attributes(attributes.slice(:emails, :transport_reimbursement, :promise_price, :days_selected, :miles_selected))
  end

  def promise_purchase_attributes
    promise_purchase.assign_attributes(attributes.slice(:promised_at, :miles_selected, :kind, :partner))
  end

  def vehicle_information_attributes
    vehicle_information.assign_attributes(attributes.slice(:year, :make, :model, :odometer_reading))
  end

  def pawn_information_attributes
    pawn_information.assign_attributes(attributes.slice(:pawn_access_no, :agreement_no, :buy_fee, :vehicle_purchase_price))
  end

  def id
    promise_purchase.id
  end

  attr_reader :promise_purchase, :attributes
end
