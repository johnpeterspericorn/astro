# encoding: utf-8
class Failover < ActiveRecord::Base
  def self.active?
    Settings.failover_mode_enabled
  end

  def self.begin
    failover = create!
    failover.touch(:began_at)
    Settings.failover_mode_enabled = true
  end

  def self.terminate
    failover = current
    failover.touch(:ended_at) if failover.present?
    Settings.failover_mode_enabled = false
  end

  def self.current
    order('began_at DESC').first if active?
  end
end
