# encoding: utf-8
class NewProductKind
  include ProtectKind
  EIGHT_DAYS = 8.days
  EXTRA_DAY = 1.day
  THREE_DAYS = 3.days
  TWENTY_ONE_DAYS = 21.days
  LEVEL_3 = 3
  LEVEL_5 = 5
  LEVEL_7 = 7
  DEFAULT_VOLUME_TIERS = { LEVEL_3 => 6, LEVEL_5 => 8, LEVEL_7 => 10 }.freeze
  DEFAULT_COUNT = 999_999
  TIMEFRAME_365 = 365
  LIMITED_VOLUME = 'LV'.freeze

  def min_return_time
    purchase_date.beginning_of_day + EIGHT_DAYS
  end

  def expiration_date
    date = [return_expiration_date, receive_by_date].max
    DateUtils.resolve_to_weekday(date)
  end

  def receive_by_date
    if DateUtils.is_date_before_latter((purchase.return_invoice.try(:created_at) || Time.current), promise_expiration_date)
      promise_expiration_date
    else
      promise_expiration_date + EXTRA_DAY
    end
  end

  def return_expiration_date
    date = promise_expiration_date
    date += THREE_DAYS if purchase.program360?
    date
  end

  def promise_expiration_date
    time = (purchase_date || Time.current) + TWENTY_ONE_DAYS
    time.end_of_day + extended_days.days
  end

  alias final_window_expiration_date expiration_date
end
