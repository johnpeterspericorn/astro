# encoding: utf-8
class Error < ActiveRecord::Base
  validates :message, presence: true
end
