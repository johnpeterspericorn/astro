# encoding: utf-8
class FlatRate < ActiveRecord::Base
  has_many :flat_rate_offers
  validates :inspection_adjustment_price, :days_adjustment_price, :miles_adjustment_price, :agreement_no, presence: true
  validates :target_price_360, presence: true
  validates :target_price_500, presence: true
  validates :target_price_360_psi, presence: true
  validates :target_price_500_psi, presence: true
  validates :country, presence: true

  alias_attribute :target_price_250, :target_price_360
  alias_attribute :target_price_250_psi, :target_price_360_psi

  def eligible?
    ineligibility_condition.blank?
  end

  def no_fee_returns
    lv_tier.to_i
  end
end
