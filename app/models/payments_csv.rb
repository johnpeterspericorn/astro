# encoding: utf-8
require 'csv'

class PaymentsCsv
  COLUMN_NAMES = %w(
    order_id
    authorization_code
    amount_charged
    promise_price
    return_fee
    sale_year
    location_initials
    item_description
    unit_price
    adjustment_code
    credit_card_number
    credit_card_type
    created_at
    vnum
    agreement_no
    cancelled?
  ).freeze

  FEES_COLUMN_NAMES = %w(
    item_description
    unit_price
    adjustment_code
  ).freeze

  attr_reader :decorated_payments

  def initialize(payments)
    @decorated_payments = Array(payments).map(&:decorate)
  end

  def to_s
    csv
  end

  private

  def csv
    @csv ||= CSV.generate do |csv|
      csv << COLUMN_NAMES
      decorated_payments.each do |payment|
        if payment_adjustment_fees(payment).present?
          append_payment_adjustment_fees_data(csv, payment) unless payment_adjustment_fees(payment).empty?
        else
          csv << COLUMN_NAMES.map { |attr| FEES_COLUMN_NAMES.include?(attr) ? '' : payment.public_send(attr) }
        end
      end
    end
  end

  def append_payment_adjustment_fees_data(csv, payment)
    payment_adjustment_fees(payment).each do |fee|
      csv << COLUMN_NAMES.map do |attr|
        if !FEES_COLUMN_NAMES.include? attr
          payment.public_send(attr)
        elsif attr == 'unit_price'
          format_price(fee.fee_amount, payment)
        else
          fee.public_send(attr)
        end
      end
    end
  end

  def payment_adjustment_fees(payment)
    payment.payment_adjustment_fees
  end

  def format_price(price, payment)
    ApplicationController.helpers.currency(price, payment.country)
  end
end
