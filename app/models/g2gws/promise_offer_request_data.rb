# encoding: utf-8
module G2gws
  class PromiseOfferRequestData
    attr_reader :promise_offer
    attr_accessor :charge

    SELLER_SUBSCRIPTION = 'DS_SELLER_SUB'.freeze
    BUYER_SUBSCRIPTION = 'DS_BUYER_SUB'.freeze
    ALACARTE_SUBSCRIPTION = 'DS_BUYER_A_LA_CARTE'.freeze
    BUYER_TYPE = 'US ASSURANCE'.freeze
    ORDER_LINE_STATUS = 'Fulfill'.freeze

    delegate(
      :location_initials,
      :sblu,
      :work_order_number,
      :seller_paid,
      :seller_paid?, # Keeping this repetition until we clean-up the old G2GWS service that depends on this predicate
      :bypass_charge?,
      :vnum,
      :agreement_no,
      :seller_no,
      :automatic_purchase,
      :pawn_location,
      :pplv?,
      :consignment_id,
      :promise_purchase,
      :cancelled_purchase,
      :days_selected,
      :miles_selected,

      to: :promise_offer
    )

    delegate(:return_invoice, to: :promise_purchase)

    def initialize(promise_offer)
      @promise_offer = promise_offer
      @charge_code_mapping = if Settings.product_mapping_enabled
                               ChargeCodeMapping.load(promise_offer)
                             else
                               Hashie::Mash.new
                             end
    end

    def product_type
      "#{days_selected} Days, #{miles_selected} Miles"
    end

    def description
      "agreementshield #{days_selected}D #{miles_selected}M"
    end

    def order_sales_channel
      if seller_paid
        SELLER_SUBSCRIPTION
      elsif automatic_purchase
        BUYER_SUBSCRIPTION
      else
        ALACARTE_SUBSCRIPTION
      end
    end

    def order_customer_number
      seller_paid ? seller_no : agreement_no
    end

    def order_reference_id
      sblu
    end

    def order_type
      BUYER_TYPE
    end

    def order_line_status
      ORDER_LINE_STATUS
    end

    def order_line_item
      item_line = if promise_purchase.present?
                    "#{promise_purchase.calculated_days_selected}D#{promise_purchase.miles_selected}M"
                  else
                    "#{cancelled_purchase.calculated_days_selected}D#{cancelled_purchase.promise_miles_selected}M"
                  end
      item_line += NewProductKind::LIMITED_VOLUME if pplv?
      item_line
    end

    def amount
      promise_offer.rounded_discounted_price
    end

    def charge_code
      @charge_code_mapping.charge_code
    end

    def call_id
      @charge_code_mapping.call_id
    end

    def promise_purchase_id
      promise_purchase.present? ? promise_purchase.id : cancelled_purchase.promise_purchase_id
    end
  end
end
