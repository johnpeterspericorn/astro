# encoding: utf-8
module G2gws
  class Invoices2RequestData
    attr_reader :agreement_number, :sblu, :pawn_location

    def initialize(offer)
      @agreement_number = offer.agreement_no
      @sblu = offer.sblu
      @is_seller_paid = offer.seller_paid?
      @pawn_location = offer.try(:pawn_information).try(:location_initials)
    end

    def seller_paid?
      @is_seller_paid
    end
  end
end
