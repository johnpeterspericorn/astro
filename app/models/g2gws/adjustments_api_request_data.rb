# encoding: utf-8
module G2gws
  class AdjustmentsApiRequestData < PromiseOfferRequestData
    BUYER = 'Buyer'.freeze

    attr_reader :return_invoice_id, :promise_offer

    def initialize(options = {})
      @promise_offer = options[:promise_offer]
      @return_invoice_id = options[:return_invoice_id]
    end

    def customer_type
      BUYER
    end
  end
end
