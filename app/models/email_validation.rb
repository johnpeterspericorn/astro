# encoding: utf-8
class EmailValidation
  include ActiveModel::Validations

  # http://rubular.com/r/n0SHv6ykUK
  EMAIL_REGEX = /\A[\w.%+-]+@[\w.-]+\.[a-z]{2,4}\z/i

  attr_reader :email_addresses, :required
  alias required? required

  validates :email_addresses, presence: true, if: :required?
  validate :email_address_is_validated

  def self.valid?(emails)
    new(emails, required: true).valid?
  end

  def initialize(email_addresses, options = {})
    @email_addresses = Array(email_addresses)
    @required = options[:required]
  end

  def valid_emails
    email_addresses.map { |email_address| email_address =~ EMAIL_REGEX ? email_address : nil }.compact
  end

  private

  def email_address_is_validated
    errors.add(:base, I18n.t('email_validation.invalid')) if email_addresses.present? && email_address_is_invalid?
  end

  def email_address_is_valid?
    email_addresses.all? { |email_address| email_address =~ EMAIL_REGEX }
  end

  def email_address_is_invalid?
    !email_address_is_valid?
  end
end
