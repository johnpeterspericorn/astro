# encoding: utf-8
class AccountsReceivableCsv < OfferCsv
  def self.column_names
    %w(
      pawn_location
      purchased_at
      agreement_no
      agreementship_name
      vnum
      work_order_number
      ar_amount
      days_selected
      miles_selected
    )
  end

  def initialize(offers)
    @offers = offers
  end
end
