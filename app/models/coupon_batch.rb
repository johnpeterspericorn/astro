# encoding: utf-8
class CouponBatch < ActiveRecord::Base
  has_many :coupons, through: :batch_coupons
  has_many :batch_coupons

  scope :batch_names, -> { uniq.pluck(:name) }
end
