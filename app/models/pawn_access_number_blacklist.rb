# encoding: utf-8
class PawnAccessNumberBlacklist < ActiveRecord::Base
  scope :active, -> { where('expiration_date IS NULL OR expiration_date >= ?', Date.current) }

  validates :pawn_access_no, presence: true

  def expired?
    expiration_date && expiration_date.past?
  end
end
