# encoding: utf-8
require 'column_conversions'
class ReturnInvoice < ActiveRecord::Base
  include ColumnConversions
  has_paper_trail
  acts_as_paranoid column: :cancelled_at
  before_restore :clear_cancellation_data
  after_create :slugify
  after_create :set_expiration_date

  delegate(
    :pawn_access_no,
    :agreement_no,
    :sblu,
    :work_order_number,
    :seller_no,
    :seller_paid,
    :pplv?,
    :purchase_location,
    :canadian_location?,

    to: :promise_offer,
    allow_nil: true
  )
  delegate(
    :vnum,
    :vehicle_purchase_price,
    :odometer_reading,
    :year,
    :make,
    :model,
    :receive_by_date,
    :group_code,
    :exceeds_promo_returns?,

    to: :promise_purchase,
    allow_nil: true
  )
  delegate :resolved_by, :human_state_name, :opened?, to: :audit, prefix: true
  delegate :name, to: :pawn_location, prefix: true
  delegate :reasons, :emails, to: :audit, prefix: true

  belongs_to :promise_purchase
  has_one :promise_offer, through: :promise_purchase
  has_one :pawn_information, through: :promise_purchase
  belongs_to :pawn_location
  belongs_to :returning_user, class_name: 'User'
  belongs_to :cancelling_user, class_name: 'User'
  belongs_to :shipment, inverse_of: :return_invoice
  has_one :audit, as: :auditee
  has_one :floor_plan_branch_selection
  has_one :bill_of_sale

  delegate :floor_plan_branch, to: :floor_plan_branch_selection, allow_nil: true
  delegate :country, :partner_network, to: :pawn_location
  delegate :location_initials, :universal_no, :sale_no, :lane_no, :run_no, to: :pawn_information

  accepts_nested_attributes_for :audit, update_only: true
  accepts_nested_attributes_for :bill_of_sale

  validates :promise_purchase, presence: true
  validates :odometer_on_return, presence: true
  validates :pawn_location, presence: true

  validate :must_be_within_miles_range
  validate :vehicle_must_be_eligible, on: :create
  validate :some_reason_is_given
  validate :title_status_must_be_valid
  validate :returning_user_is_valid
  validate :promo_return_is_valid, on: :create

  alias archive destroy

  RETURN_REASONS = %w(
    mechanical_issues
    cosmetic_issues
    unable_to_sell
    changed_mind
    other
    no_answer
  ).freeze

  scope :incomplete, -> do
    joins(:pawn_location)
      .where('shipment_id IS NULL and pawn_locations.country = ?', SupportedCountry::DEFAULT)
  end

  scope :for_vnum_number, ->(vnum) do
    joins(promise_purchase: { promise_offer: :vehicle_information })
      .where(vehicle_information: { vnum: vnum })
  end

  scope :for_vra_number, ->(vra) do
    where(id: vra)
  end

  scope :for_agreement_number, ->(agreement_no) do
    joins(promise_purchase: { promise_offer: :pawn_information })
      .where(pawn_information: { agreement_no: agreement_no })
  end

  scope :for_agreement_number_with_palv_buyer_paid, ->(agreement_no, country) do
    includes(promise_purchase: { pawn_information: :pawn_location })
      .where(pawn_information: { agreement_no: agreement_no },
             pawn_locations: { country: country },
             promise_offers: { limited_volume: NewProductKind::DEFAULT_VOLUME_TIERS.keys, seller_paid: false })
  end

  scope :for_palv_group_code, ->(group_code, country) do
                                includes(promise_purchase: { pawn_information: :pawn_location })
                                  .where(
                                    pawn_locations: { country: country },
                                    promise_offers: { group_code: group_code, seller_paid: false }
                                  )
                              end

  scope :for_agreement_group_code, ->(agreement_no, group_code) do
    includes(promise_purchase: :pawn_information)
      .merge(PromisePurchase.for_agreement_group_code(agreement_no, group_code))
  end

  scope :for_seller_paid, ->(seller_paid) do
    includes(:promise_purchase)
      .merge(PromisePurchase.for_seller_paid(seller_paid))
  end

  scope :for_seller_number, ->(seller_no) do
    joins(promise_purchase: { promise_offer: :pawn_information })
      .where(pawn_information: { seller_no: seller_no })
  end

  scope :initiated_at, ->(time) do
    where(created_at: time)
  end

  scope :valid_and_initiated_at_within_timeframe, ->(timeframe) do
    where('return_invoices.created_at > ? and cancelled_at IS NULL', timeframe.days.ago)
  end

  scope :by_purchase_location, ->(date_from, date_to) do
    where(created_at: date_from..date_to)
      .includes(promise_purchase: { promise_offer: :pawn_information })
      .group_by(&:purchase_location)
  end

  def return_certificate
    @return_certificate ||= ReturnCertificate.new(self)
  end

  def purchase_date
    promise_purchase.promised_at
  end

  def set_expiration_date
    update_attributes(expiration_date:  promise_purchase.final_window_expiration_date) if promise_purchase.present?
  end

  def expiration_date
    self[:expiration_date] || (promise_purchase&.expiration_date)
  end

  def odometer_under_minimum?
    odometer_on_return.present? && odometer_on_return < odometer_reading
  end

  def odometer_over_maximum?
    miles_range                = promise_purchase.miles_selected.to_i
    maximum_allowable_odometer = odometer_reading + miles_range
    odometer_on_return.present? && odometer_on_return > maximum_allowable_odometer
  end

  def reason_given?
    RETURN_REASONS.collect { |reason| public_send reason }.any?
  end

  def missing_data?
    !odometer_on_return.present? || !reason_given? || odometer_under_minimum? || title_status.blank? || odometer_over_maximum?
  end

  def cancelled?
    cancelled_at.present?
  end

  def purchase_cancelled?
    promise_purchase.nil?
  end

  def canadian_return?
    country == 'Canada'
  end

  def audited?
    audit.present?
  end

  def email
    user_email || buyer_email
  end

  def buyer_email
    buyer_information.emails
  end

  def buyer_information
    @buyer_information ||= BuyerInformation.build_with_defaults(pawn_access_no)
  end

  def agreement_information
    @agreement_information ||= AgreementInformation.build_with_defaults(pawn_access_no, agreement_no)
  end

  def purchase_location
    promise_offer.try(:pawn_location)
  end

  def location_updated?
    previous_changes.include?(:pawn_location_id)
  end

  def g2gws_eligible?
    promise_offer.g2gws_eligible? && !promise_offer.bsc_location?
  end

  def g2gws_adjustments_api_eligible?
    Settings.g2gws_adjustments_api_enabled && g2gws_eligible?
  end

  def update_drop_notice
    ActiveRecord::Base.transaction do
      if Settings.return_cancellation_delete_drop_notice
        DeleteDropNoticeJob.enqueue(previous_version)
      end

      CreateDropNoticeJob.enqueue(self)
    end
  end

  def update_user_email(email)
    update_attributes(user_email: email)
  end

  def finalize(options = {})
    other_branch_notes = options[:other_branch_notes]

    ShipmentLabelJob.enqueue(return_invoice: self) if shipment_eligible?(options)
    ReturnInvoiceMailer.pawn_location_return_notification(self, I18n.locale.to_s).deliver_later
    ReturnInvoiceMailer.return_confirmation(self, I18n.locale.to_s).deliver_later
    if other_branch_notes.present?
      ReturnInvoiceMailer.other_branch_selected(self, other_branch_notes).deliver_later
    end
    CreateDropNoticeJob.enqueue(self)
    ReturnInvoiceMailer.pawn_location_left_lot_notification(self, I18n.locale.to_s).deliver_later if send_left_lot_mail?
    add_internet_notes
  end

  def shipment_information
    if floor_plan_branch
      floor_plan_branch.to_shipment_info
    else
      agreement_information
    end
  end

  def new_product_information
    NewProductFeeInformation.new(promise_purchase)
  end

  def fee_data
    @fee_data ||= new_product_information.fee_details
  end

  def log_new_product_fee
    self.new_product_fee = fee_data[:fee] if !fee_data.empty? && promise_offer.pplv? && !promise_offer.seller_paid
  end

  def left_lot_in_ods?
    if pawn_information.present?
      if Settings.query_ods?
        ods_query_interface = OdsQueryInterface.new(agreement_no, location_initials, universal_no, sale_no, lane_no, run_no)
        ods_query_interface.left_lot?
      end
    else
      true
    end
  end

  def purchase_and_create_shipment
    label = ShipmentLabel.buy(shipment_information)
    build_shipment(
      easypost_shipment_id: label.id,
      tracking_code:        label.tracking_code,
      label_url:            label.url,
      carrier:              label.carrier
    ).save!
  end

  def vra_initiation_adjustments_job
    G2gwsVraInitiationAdjustmentsJob.enqueue(return_invoice_id: id, promise_offer: promise_offer) if g2gws_adjustments_api_eligible?
  end

  def vra_cancellation_adjustments_job
    G2gwsVraCancellationAdjustmentsJob.enqueue(return_invoice_id: id, promise_offer: promise_offer) if g2gws_adjustments_api_eligible?
  end

  def add_internet_notes
    return unless Settings.salvage_internet_notes_enabled
    AddInternetNoteJob.enqueue(
      vnum: vnum,
      pawn_location_initials: purchase_location.initials,
      sblu: sblu,
      work_order: work_order_number,
      notes: {
        added_by: 'ASTRSHIELD',
        text:     'ASTRSHIELD101 Vehicle Return Authorization (VRA) requested',
        type:     'DEAL'
      }
    )
  end

  def clear_cancellation_data
    update_attributes(reason: nil, cancelling_user: nil)
  end

  def slugify
    slug = Digest::MD5.hexdigest(id.to_s)
    update_attributes(slug: slug)
  end

  private

  def must_be_within_miles_range
    if odometer_under_minimum?
      errors[:base] << I18n.t('return_invoice.odometer.too_low_error')
    elsif odometer_over_maximum?
      errors[:base] << I18n.t('return_invoice.odometer.too_high_error')
    end
  end

  def promo_return_is_valid
    errors[:base] << I18n.t('return_invoice.invalid_promo_return') if exceeds_promo_returns?
  end

  def vehicle_must_be_eligible
    errors[:base] << I18n.t('return_invoice.ineligible.error') unless promise_purchase.eligible_for_return?
  end

  def some_reason_is_given
    errors[:base] << I18n.t('return_invoice.return_reasons.none_specified_error') unless reason_given?
  end

  def title_status_must_be_valid
    errors[:base] << I18n.t('return_invoice.invalid_title_status') unless title_status.present? && I18n.t('return_invoice.title_statuses').keys.include?(title_status)
  end

  def returning_user_is_valid
    errors[:base] << 'invalid returning user' unless returning_user.present?
  end

  def shipment_eligible?(options)
    return false if canadian_return?
    create_label_for_branch? && !(shipment.present? || options[:skip_label])
  end

  def create_label_for_branch?
    return true unless floor_plan_branch
    floor_plan_branch.generate_shipping_label
  end

  def incomplete_return
    !shipment.present? && !canadian_return?
  end

  def send_left_lot_mail?
    value_to_boolean(Settings.left_lot_email) && !left_lot
  end
end
