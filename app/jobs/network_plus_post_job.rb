# encoding: utf-8
class NetworkPlusPostJob
  require 'network_plus/network_plus_api_client'

  def initialize(external_vehicles_id)
    @external_vehicles_id = external_vehicles_id
  end

  def post
    response = NetworkPlus::NetworkPlusApiClient.new(external_vehicles_load).post
    response == 200 ? @external_vehicle.post! : @external_vehicle.unpost!
  end

  private

  def external_vehicles_load
    @external_vehicle ||= ExternalVehiclesLoad.find(@external_vehicles_id)
  end
end
