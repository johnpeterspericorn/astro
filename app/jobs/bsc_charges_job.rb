# encoding: utf-8
class BscChargesJob < NotifyingJob
  extend Enqueuable

  def initialize(promise_offer)
    @promise_offer = promise_offer
  end

  private

  attr_reader :promise_offer
end
