# encoding: utf-8
class CancelledReturnReportJob
  include Delayed::RecurringJob
  run_every 1.month
  run_at '25'

  def perform
    ReturnInvoiceMailer.monthly_cancelled_vra_report.deliver_now
  end
end
