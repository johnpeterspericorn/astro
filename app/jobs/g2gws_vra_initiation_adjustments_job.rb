# encoding: utf-8
class G2gwsVraInitiationAdjustmentsJob
  extend Enqueuable

  def initialize(options = {})
    @return_invoice_id = options[:return_invoice_id]
    @promise_offer = options[:promise_offer]
  end

  def perform(g2gws_client = G2gws::AdjustmentsApi::Client)
    request_data = G2gws::Invoices2RequestData.new(@promise_offer)

    begin
      unless @promise_offer.consignment_id?
        consignment_id = G2gws::InvoicesApi::Client.get_consignment(request_data)
        @promise_offer.update_attributes(consignment_id: consignment_id) if consignment_id
      end

      g2gws_client.vra_initiate(G2gws::AdjustmentsApiRequestData.new(promise_offer: @promise_offer, return_invoice_id: @return_invoice_id))
    rescue G2gws::RetryableError => e
      raise G2gws::RetryableError, "Error: #{e.message}"
    rescue G2gws::NonRetryableError => ne
      Rollbar.error(ne)
    end
  end
end
