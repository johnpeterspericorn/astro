# encoding: utf-8
class AccountsReceivableReportJob
  include Delayed::RecurringJob
  run_every AccountsReceivableMailer::INTERVAL
  run_at 'Thursday 00:00'

  def perform
    AccountsReceivableMailer.ledger_report.deliver_now
  end
end
