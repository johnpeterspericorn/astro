# encoding: utf-8
class G2gwsChargesJob < NotifyingJob
  extend Enqueuable

  def initialize(promise_offer)
    @promise_offer = promise_offer
    @promise_purchase_id = promise_offer.promise_purchase.id
  end

  def last_failure(_job, exception)
    G2gwsChargesMailer.notify_last_failure(promise_offer, exception.message).deliver_now
  end

  private

  attr_reader :promise_offer

  def offer_request_data
    @offer_request_data ||= G2gws::PromiseOfferRequestData.new(promise_offer)
  end
end
