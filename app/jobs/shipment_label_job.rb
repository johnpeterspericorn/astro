# encoding: utf-8
class ShipmentLabelJob
  extend Enqueuable

  def initialize(options = {})
    @return_invoice = options[:return_invoice]
  end

  def perform
    @return_invoice.purchase_and_create_shipment
  end
end
