# encoding: utf-8
class DropNoticeJob < NotifyingJob
  extend Enqueuable

  def initialize(return_invoice)
    @return_invoice = return_invoice
  end

  def last_failure(_job, exception, mailer = DropNoticeMailer)
    mailer.notify_last_failure(return_invoice, exception.message).deliver_now
  end

  private

  attr_reader :return_invoice
end
