# encoding: utf-8
class SowsCancellationJob < SowsJob
  def initialize(promise_offer)
    super
    @days = promise_offer.days_selected
    @miles = promise_offer.miles_selected
  end

  def perform
    # it isn't completely clear why this assignment is needed but this behavior
    # was removed from the sows client, so it was moved here to be functionally
    # equivalent #87820598
    promise_offer.days_selected = @days
    promise_offer.miles_selected = @miles
    Astroheim::Sows.client.cancel(offer_request_data, @promise_purchase_id)
  end
end
