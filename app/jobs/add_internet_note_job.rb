# encoding: utf-8
class AddInternetNoteJob < NotifyingJob
  extend Enqueuable

  # Internet notes can only be created for Astroheim locations. These are
  # non-Astroheim locations and will therefore fail.
  EXCLUDED_LOCATIONS = %w(DLRM FTB NONM).freeze

  def initialize(attributes)
    @attributes = attributes
    @vnum = attributes.fetch(:vnum)
  end

  def perform
    log_excluded_location && return if location_excluded?

    SalvageLogger.info("Adding note for: #{vnum}")
    InternetNote.create(attributes)
  end

  def last_failure(_job, exception, mailer = InternetNoteMailer)
    mailer.notify_last_failure(attributes, exception.message).deliver_now
  end

  private

  attr_reader :attributes, :vnum

  def location_initials
    attributes.fetch(:pawn_location_initials)
  end

  def location_excluded?
    EXCLUDED_LOCATIONS.include?(location_initials)
  end

  def log_excluded_location
    SalvageLogger.info("Skipping excluded location #{location_initials} for: #{vnum}")
  end
end
