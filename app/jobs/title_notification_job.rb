# encoding: utf-8
class TitleNotificationJob
  include Delayed::RecurringJob
  run_every 1.day
  run_at '18:00'

  def perform
    return unless Settings.title_notifications_enabled

    return_invoices = ReturnInvoice
                        .where(["vehicle_received = ? AND title_received = ? AND title_notification_sent = ? AND expiration_date > ?",
                          true, false, false, Date.current])

    return_invoices.each do |return_invoice|
      ReturnInvoiceMailer.title_expiration_notification(return_invoice).deliver_later
      return_invoice.update(title_notification_sent: true)
    end
  end
end
