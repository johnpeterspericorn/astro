# encoding: utf-8
class AutomaticPurchasesJob
  include Delayed::RecurringJob
  run_every 5.minutes

  def perform
    AutomatedPromisePurchaser.process_outstanding_vehicles_for_automatic_purchase
  end

  def before(job)
    DelayedJobLogger.debug "starting automated purchaser job#{job.id}"
  end

  def after(job)
    DelayedJobLogger.debug "ending automated purchaser job#{job.id}"
  end
end
