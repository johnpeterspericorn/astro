# encoding: utf-8
class NotifyingJob
  MAX_ATTEMPTS = 10

  def error(job, exception)
    if job.attempts.zero?
      first_failure(job, exception)
    elsif job.attempts == last_attempt
      last_failure(job, exception)
    end
  end

  def first_failure(job, exception)
    # noop - override me
  end

  def last_failure(job, exception)
    # noop - override me
  end

  def max_attempts
    MAX_ATTEMPTS
  end

  private

  def last_attempt
    max_attempts - 1
  end
end
