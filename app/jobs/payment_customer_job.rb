# encoding: utf-8
class PaymentCustomerJob
  extend Enqueuable

  def initialize(id)
    @customer = User.find id
  end

  def perform
    @customer.create_payment_profile
  end
end
