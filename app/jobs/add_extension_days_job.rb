# encoding: utf-8
class AddExtensionDaysJob
  def perform
    sql = <<-SQL
      UPDATE promise_purchases AS gp
      SET extension_days = ge.additional_days,
      extension_reason = ge.reason
      FROM promise_extensions AS ge
      INNER JOIN vehicle_information vi ON vi.vnum = ge.vnum
      INNER JOIN promise_offers go ON vi.promise_offer_id = go.id
      INNER JOIN promise_purchases gp1 ON gp1.promise_offer_id = go.id
      WHERE gp.id = gp1.id
      AND ge.status = 'new'
    SQL

    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute(sql)
      PromiseExtension.where(status: 'new').update_all(status: 'processed_existing')
    end
  end
end
