# encoding: utf-8
class CreateDropNoticeJob < DropNoticeJob
  def perform
    Autoims::CLIENT.create_drop_notice(return_invoice)
  end
end
