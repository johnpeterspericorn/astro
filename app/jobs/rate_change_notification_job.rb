# encoding: utf-8
class RateChangeNotificationJob
  extend Enqueuable

  def initialize(rate_change)
    @rate_change = rate_change
  end

  def perform
    RateChangeMailer.rate_change_notification(rate_change).deliver_now
    update_attributes(letter_sent: DateUtils.now)
    ActionType.process(rate_change, '', rate_change.letter_sent, rate_change.emails, rate_change.parameters)
  end
end
