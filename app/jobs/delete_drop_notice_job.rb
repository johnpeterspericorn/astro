# encoding: utf-8
class DeleteDropNoticeJob < DropNoticeJob
  def perform
    Autoims::CLIENT.delete_drop_notice(return_invoice)
  end

  def max_attempts
    1
  end
end
