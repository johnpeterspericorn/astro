# encoding: utf-8
class SowsPurchaseJob < SowsJob
  def perform(sows_client = Astroheim::Sows.client)
    sows_client.purchase(offer_request_data, @promise_purchase_id)
  end
end
