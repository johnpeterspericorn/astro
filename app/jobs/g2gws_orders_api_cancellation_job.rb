# encoding: utf-8
class G2gwsOrdersApiCancellationJob < G2gwsChargesJob
  def perform(g2gws_client = G2gws::OrdersApi::Client)
    request_data = G2gws::Invoices2RequestData.new(promise_offer)
    begin
      consignment_id = G2gws::InvoicesApi::Client.get_consignment(request_data)
      promise_offer.update_attributes(consignment_id: consignment_id) if consignment_id

      g2gws_client.cancel(G2gws::PromiseOfferRequestData.new(promise_offer))
    rescue G2gws::RetryableError => e
      raise G2gws::RetryableError, "Error: #{e.message}"
    rescue G2gws::NonRetryableError => ne
      Rollbar.error(ne)
    end
  end
end
