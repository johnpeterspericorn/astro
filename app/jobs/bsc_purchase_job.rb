# encoding: utf-8
require 'bsc_charges/client'

class BscPurchaseJob < BscChargesJob
  def perform(bsc_client = BscCharges::Client)
    bsc_client.purchase(promise_offer)
  end
end
