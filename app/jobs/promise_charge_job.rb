# encoding: utf-8
class PromiseChargeJob < NotifyingJob
  extend Enqueuable

  def self.enqueue_from_offer(promise_offer)
    if Settings.charges_enabled
      charge = PromiseCharge.new(promise_offer)
      enqueue(charge)
    end
  end

  def initialize(promise_charge)
    @promise_charge = promise_charge
  end

  def perform
    promise_charge.submit
  end

  def first_failure(_job, exception)
    ChargesMailer.notify_first_failure(promise_charge, exception.message).deliver_now
  end

  def last_failure(_job, exception)
    ChargesMailer.notify_last_failure(promise_charge, exception.message).deliver_now
  end

  private

  attr_reader :promise_charge
end
