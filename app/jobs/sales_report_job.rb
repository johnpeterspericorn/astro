# encoding: utf-8
class SalesReportJob
  include Delayed::RecurringJob
  run_every 1.day
  run_at '06:00'

  def perform
    SalesMailer.report.deliver_now
  end
end
