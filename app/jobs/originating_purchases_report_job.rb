# encoding: utf-8
class OriginatingPurchasesReportJob
  include Delayed::RecurringJob
  run_every 1.week
  run_at 'saturday 11:30pm'

  def perform
    Rails.logger.info("Triggering OriginatingPurchasesReport weekly @ #{Time.now.utc}")
    OriginatingPurchasesReport.weekly_report
  end
end
