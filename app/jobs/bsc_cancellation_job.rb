# encoding: utf-8
require 'bsc_charges/client'

class BscCancellationJob < BscChargesJob
  def perform(bsc_client = BscCharges::Client)
    bsc_client.cancel(promise_offer)
  end
end
