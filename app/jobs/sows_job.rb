# encoding: utf-8
class SowsJob < NotifyingJob
  extend Enqueuable

  def initialize(promise_offer)
    @promise_offer = promise_offer
    @promise_purchase_id = promise_offer.promise_purchase.id
  end

  def name
    self.class.name
  end

  def last_failure(_, error, mailer = SowsMailer)
    mailer.notify_last_failure(promise_offer, error.message, self).deliver_now
  end

  private

  attr_reader :promise_offer

  def offer_request_data
    @offer_request_data ||= Sows::PromiseOfferRequestData.new(promise_offer)
  end
end
