# encoding: utf-8
class PaymentReportJob
  include Delayed::RecurringJob
  run_every 1.day
  run_at '05:00'

  def perform
    Payment.send_daily_report
  end
end
