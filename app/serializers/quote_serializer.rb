# encoding: utf-8
class QuoteSerializer < ActiveModel::Serializer
  delegate :quote_price, to: :object
  attributes :low_valuation, :average_valuation, :high_valuation, :quote_price, :quotes, :flat_rate, :ineligibility_notice

  def quotes
    price_pairs = object.miles.product(object.days)

    price_pairs.each_with_object({}) do |(miles, days), quotes|
      quotes[miles] ||= {}
      quotes[miles][days] = object.price(miles, days).round
    end
  end

  def low_valuation
    object.low_valuation || object.average_valuation
  end

  def high_valuation
    object.high_valuation || object.average_valuation
  end

  def ineligibility_notice
    object.mobile_ineligibility_notice
  end
end
