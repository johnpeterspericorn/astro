# encoding: utf-8
class CouponSerializer < ActiveModel::Serializer
  attributes :amount, :code
end
