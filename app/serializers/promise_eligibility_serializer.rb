# encoding: utf-8
class PromiseEligibilitySerializer < ActiveModel::Serializer
  attributes :vnum, :promise_eligible, :ineligibility_reason

  def promise_eligible
    object.eligible_for_purchase?
  end

  # This method is available on decorated offers. For some reason, the default
  # behavior attempts to send this message to undecorated records. When we
  # explicitly define this method it works as expected. Interesting using
  # delegate also fails due to undecorated records.
  delegate :ineligibility_reason, to: :object
end
