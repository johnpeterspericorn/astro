# encoding: utf-8
class PromiseOfferSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :vnum,
    :year,
    :make,
    :model,
    :odometer_reading,
    :vehicle_purchase_price,
    :lane_no,
    :run_no,
    :price_250_by_7,
    :price_250_by_14,
    :price_250_by_21,
    :price_500_by_7,
    :price_500_by_14,
    :price_500_by_21,

    :location_initials,
    :location_name,
    :agreement_no,
    :agreement_name,
    :flat_rate_price,
    :mobile_ineligibility_notice,
    :ineligibility_notice,

    :age_rating,
    :autocheck_rating,
    :make_model_rating,
    :odometer_rating,
    :seller_rating,
    :estimate_vs_price_rating,

    :offer_type
  )

  delegate :location_name, to: :object

  # To keep from breaking clients deployed in the field, we must continue to
  # respond with this alias. This is confusing because the offer object also
  # implements a method by this name.
  def ineligibility_notice
    object.mobile_ineligibility_notice
  end

  delegate :agreement_name, to: :object

  def make_model_rating
    object.mmr_rating
  end

  def estimate_vs_price_rating
    object.bid_risk_rating
  end

  def offer_type
    case
    when !object.eligible_for_purchase?
      0
    when object.has_flat_rate_offer?
      1
    else
      2
    end
  end

  def model
    object.vehicle_information.model
  end

  delegate :vehicle_purchase_price, to: :object
end
