# encoding: utf-8
class PawnLocationSerializer < ActiveModel::Serializer
  attributes :id, :name, :initials, :presale_lane_numbers
end
