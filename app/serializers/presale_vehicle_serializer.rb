# encoding: utf-8
class PresaleVehicleSerializer < ActiveModel::Serializer
  attributes(
    :id,
    :vnum,
    :make,
    :model,
    :year,
    :lane_no,
    :run_no,
    :odometer_reading,
    :location_initials,
    :location_name,
    :age_rating,
    :autocheck_rating,
    :mid_rating,
    :mmr_rating,
    :odometer_rating,
    :seller_rating,
    :ineligibility_condition,
    :ineligibility_notice
  )

  def ineligibility_notice
    object.mobile_ineligibility_notice
  end
end
