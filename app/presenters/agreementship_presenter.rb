# encoding: utf-8
class AgreementshipPresenter < Struct.new(:pawn_access_number)
  include ActionView::Helpers::TagHelper

  def options
    agreementships.map { |agreement_no, agreement_name| ["#{agreement_no} - #{agreement_name}", agreement_no] }
  end

  private

  def agreementships
    @agreementship_addr ||= buyer_authorization.get_agreement_numbers_to_names
  end

  def buyer_authorization
    @buyer_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number)
  end
end
