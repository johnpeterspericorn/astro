# encoding: utf-8
class ReturnCertificate < Struct.new(:return_invoice, :promise_purchase, :promise_offer)
  include HasBarcode
  include ActionView::Helpers::NumberHelper

  delegate(
    :vnum,
    :odometer_reading,
    :vehicle_full_description,

    :pawn_access_no,
    :agreement_no,
    :badge_no,
    :universal_no,
    :location_initials,
    :work_order_number,
    :country,

    :payment_type,
    :buy_net,
    :adjustments_description,
    :left_lot?,
    :if_bid?,
    :floor_plan_company,
    :has_transportation_coverage?,

    to: :promise_offer
  )

  delegate :floor_plan_company, to: :promise_offer, allow_nil: true

  delegate :name, to: :floor_plan_company, allow_nil: true, prefix: true

  delegate :purchase_date, :receive_by_date, to: :return_invoice

  def floor_plan_branches
    floor_plan_company.try(:floor_plan_branches) || []
  end

  def floor_plan_branch
    floor_plan_branches.first
  end

  delegate(
    :address_street,
    :address_suite,
    :address_city,
    :address_state,
    :address_zipcode,
    :country_code,

    to:        :floor_plan_branch,
    allow_nil: true,
    prefix:    true
  )

  EXTRA_DAY = 1

  def initialize(return_invoice)
    promise_purchase = return_invoice.promise_purchase
    promise_offer = promise_purchase.promise_offer
    super return_invoice, promise_purchase.decorate, promise_offer.decorate
  end

  has_barcode :barcode,
              outputter: :svg,
              type: :code_39,
              value: proc { |certificate| certificate.vnum }

  def initiation_date
    return_invoice.created_at
  end

  def odometer_limit
    odometer_reading.to_i + promise_purchase.miles_selected
  end

  def return_location
    return_invoice.pawn_location.name
  end

  def return_instructions
    return_invoice.pawn_location.return_instructions
  end

  def canadian_return_location?
    return_invoice.partner_network == PartnerNetwork::CA_ASTROHEIM
  end

  def vra_number
    return_invoice.id
  end

  def payment_type_copy
    I18n.t('promise_offers.payment_types')[payment_type.to_sym]
  end

  def return_reasons
    unless @return_reasons
      @return_reasons = ReturnInvoice::RETURN_REASONS.select { |reason| return_invoice.send(reason) }
                                                     .collect { |reason| I18n.t("return_invoice.return_reasons.#{reason}") }
                                                     .join(', ')
      @return_reasons += ": #{return_invoice.additional_information}" if return_invoice.additional_information
    end
    @return_reasons
  end

  def title_status
    I18n.t('return_invoice.title_statuses')[return_invoice.title_status]
  end

  def vehicle_purchase_date
    promise_offer.purchased_at.strftime('%m/%d/%y')
  end

  def vehicle_paid_by_floor_plan?
    promise_offer.adjustments_paid_by_floor_plan?
  end

  delegate :vehicle_purchase_price, to: :promise_offer

  def adjustments_buy_fee
    ApplicationController.helpers.currency((promise_offer.adjustments_buy_fee || 0.0), country)
  end

  def transportation_coverage_value
    ApplicationController.helpers.currency(promise_offer.transport_reimbursement, country)
  end

  def payment_description
    "DS#{location_initials}#{vnum[-8..-1]}"
  end

  def total_refund
    if promise_offer.source.vehicle_purchase_price
      buy_fee = promise_offer.adjustments_buy_fee || 0.0
      ApplicationController.helpers.currency((promise_offer.source.vehicle_purchase_price + buy_fee), country, separator: '.', delimiter: ',')
    else
      'N/A'
    end
  end

  def purchase_location
    promise_offer.purchase_location ? promise_offer.purchase_location.name : 'N/A'
  end

  def year_make_model
    promise_offer.year_make_model('/')
  end

  def description
    "AgreementShield Return #{promise_purchase.id}"
  end

  def agreement_name
    buyer_information.name
  end

  delegate :agreementship_name, to: :agreement_information

  def agreementship_addr
    @agreementship_addr ||= buyer_authorization.get_agreementship_addr
  end

  def agreementship_addr_street
    agreement_information.address_street
  end

  def agreementship_addr_suite
    agreement_information.address_suite
  end

  def agreementship_addr_city
    agreement_information.address_city
  end

  def agreementship_addr_state
    agreement_information.address_state
  end

  def agreementship_addr_zipcode
    agreement_information.address_zipcode
  end

  private

  def buyer_authorization
    @buyer_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_no)
  end

  def buyer_information
    return_invoice.buyer_information
  end

  def agreement_information
    return_invoice.agreement_information
  end
end
