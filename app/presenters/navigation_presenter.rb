# encoding: utf-8
class NavigationPresenter < Struct.new(:user)
  def self.back_path(area_of_responsibility, params)
    send("#{area_of_responsibility}_back_path", params)
  end

  def self.pagination_label(filter)
    case filter
    when PresaleVehicleFilter::OVE
      'Years'
    when PresaleVehicleFilter::VNUM, PresaleVehicleFilter::PawnLocation
      'Run #s'
    end
  end

  def show_badge_button?
    Settings.show_badge_ui || user.super_user?
  end

  def show_agreement_filter?
    Settings.show_agreement_filter_ui && user.super_user?
  end
end
