# encoding: utf-8
class JsTranslationPresenter < Struct.new(:selection)
  def process_return_cancellation
    data_feed = []
    data_hash = {}
    data_hash[:reason_prompt] = I18n.t('return_invoice.cancellation.cancellation_reason_prompt')
    if selection != I18n.t('return_invoice.cancellation.cancellation_reason_prompt')
      data_hash[:cancellation_reason] = selection
      data_hash[:prenote] = I18n.t('return_invoice.cancellation.cancellation_prenote')
      data_hash[:note] = I18n.t('return_invoice.cancellation.cancellation_note')
      data_hash[:submit_disable] = true
    else
      data_hash[:cancellation_reason] = ''
      data_hash[:note] = I18n.t('return_invoice.cancellation.cancellation_note')
      data_hash[:submit_disable] = false
    end
    data_feed << data_hash
  end
end
