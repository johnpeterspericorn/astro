# encoding: utf-8
class PromiseOfferFormPresenter < Struct.new(:promise_offer, :current_user)
  include ActionView::Helpers::TagHelper

  def coupon_label
    "vehicle[#{promise_offer.vnum}][coupon_code]"
  end

  delegate :country, to: :promise_offer

  def vnum_label
    "vehicle[#{promise_offer.vnum}][promise_options]"
  end

  def inspection_label
    "vehicle[#{promise_offer.vnum}][inspection]"
  end

  def inspection_disabled_css_class
    :disabled unless eligible_for_inspection?
  end

  def disable_radio_buttons?
    promise_offer.purchased? || cannot_be_selected?
  end

  def cannot_be_selected?
    !can_be_selected?
  end

  def can_be_selected?
    not_yet_promised? && !has_inspection? && !left_lot_and_disabled? && (promise_offer.not_past_cutoff? || current_user.super_user?) && !seller_paid
  end

  def offer_vehicle_classes
    classes = []
    classes << 'disabled_vehicle'                 if disabled?
    classes << 'seller_paid_vehicle'              if seller_paid?
    classes << 'promise_purchased_for_vehicle'  if promise_purchase.present?
    classes << 'inspection_purchased_for_vehicle' if inspection_purchase.present?
    classes << 'vehicle_with_inspection_ui'       if vehicle_has_inspection_ui?
    classes << 'vehicle_without_inspection_ui'    unless vehicle_has_inspection_ui?
    classes << 'past_cutoff_vehicle' if promise_offer.past_cutoff? && !current_user.super_user?
    classes << 'pending_vehicle' if pending?
    classes
  end

  def vehicle_has_inspection_ui?
    purchase_location_has_inspections? && has_inspection_pricing?
  end

  def offer_selection_class
    css_classes = ['none']
    css_classes << 'selected' if can_be_selected?
    css_classes.join(' ')
  end

  def ineligibility_copy
    css_class, copy = if left_lot_and_disabled?
                        ['left_lot', I18n.t('promise_offers.vehicles.left_lot_copy')]
                      elsif promise_offer.past_cutoff? && !current_user.super_user?
                        ['past_cutoff', I18n.t('promise_offers.vehicles.past_cutoff_copy')]
                      elsif promise_offer.seller_paid?
                        ['seller_paid', I18n.t('promise_offers.vehicles.seller_paid_copy')]
                      elsif ineligible?
                        ['ineligible_by_condition', ineligibility_reason]
                      elsif pending?
                        ['pending', I18n.t('promise_offers.vehicles.pending_copy')]
                      end
    content_tag(:div, copy, class: css_class)
  end

  def partial_to_render
    case
    when promise_offer.eligible? && !promise_offer.seller_paid? || current_user.super_user?
      if Settings.inspections_enabled && vehicle_has_inspection_ui?
        'inspections_pricing_matrix'
      else
        'no_inspections_pricing_matrix'
      end
    when promise_offer.seller_paid?
      'distance_and_time_grid'
    end
  end

  def method_missing(meth, *args, &block)
    promise_offer.send(meth, *args, &block)
  end

  def respond_to_missing?(method_name, include_private = false)
    promise_offer.respond_to?(method_name, include_private) || super
  end

  private

  def disabled?
    left_lot_and_disabled? || ineligible?
  end

  def left_lot_and_disabled?
    left_lot? && !paid_by_floor_plan?
  end
end
