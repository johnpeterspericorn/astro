# encoding: utf-8
class UserAgentConstraint
  def initialize(expression)
    @expression = expression
  end

  def matches?(request)
    user_agent = request.env['HTTP_USER_AGENT'].to_s
    user_agent.match(expression)
  end

  private

  attr_reader :expression
end
