jQuery ->
  terms_and_conditions = $('#terms_and_conditions')
  terms_and_conditions.hide()

  $('.show-terms-and-conditions').click (e) ->
    e.preventDefault()
    $('#review_selections').hide()
    terms_and_conditions.show()

  # disable checkout link after first click to prevent double clicks
  $(".checkout_btn").click (e) ->
    $(this).bind('click', false)

  $('#allow_continue').click ->
    checked = $(this).is(':checked')
    $('.checkout_btn').attr('disabled', !checked)

  $('.dialog').dialog({
    autoOpen: false,
    draggable: false,
    resizable: false
  })

  $('.pop').click (e) ->
    e.preventDefault()
    theTitle = $(this).attr('title')
    open = $(this).attr('href')
    $(open).dialog("open", {title: theTitle})

  $('#submit_payment').click (e) ->
    $('#submit_payment').attr("disabled", true)
