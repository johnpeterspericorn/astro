//= require lib/Coupons/Applicator
//= require lib/Coupons/Coupon
//= require lib/Coupons/PromisePrice
//= require lib/Coupons/Registry

$(document).ready(function(){
  var couponRegistry = new Coupons.Registry();

  $('.js-promise-purchase').each(function(){
    var promisePurchaseElement = $(this);
    var couponCodeElement = promisePurchaseElement.find('.js-coupon-code');
    var promisePriceElement = promisePurchaseElement.find('.js-promise-price');
    var country = promisePurchaseElement.find('.js-purchase-country');

    var applyButton = promisePurchaseElement.find('.js-apply-coupon')
    var resetButton = promisePurchaseElement.find('.js-reset-coupon');
    var promisePrice = new Coupons.PromisePrice(promisePriceElement, couponRegistry);
    var applicator = new Coupons.Applicator(promisePrice, couponRegistry);

    var clearCoupon = function() {
      couponCodeElement.val('');
    };

    var lockCoupon = function() {
      couponCodeElement.attr('readonly', true);
    };

    var unlockCoupon = function(){
      couponCodeElement.removeAttr('readonly');
    };

    var hideApply = function() {
      applyButton.hide();
      resetButton.show();
    };

    var hideReset = function() {
      resetButton.hide();
      applyButton.show();
    };

    var success = function() {
      lockCoupon();
      hideApply();
    };

    var failure = function() {
      clearCoupon();
    };

    applyButton.click(function(e){
      e.preventDefault();
      applicator.apply(country.val(), couponCodeElement.val())
        .done(success)
        .fail(failure);
    });

    resetButton.click(function(e){
      e.preventDefault();
      promisePrice.resetCoupon();
      unlockCoupon();
      hideReset();
      clearCoupon();
    });
  });
});
