extract_pawn_access_number_from_track_data = (trackData)->
  pawnAccessNumberGroup = trackData.split(";")[1]
  pawnAccessNumber = pawnAccessNumberGroup.split("=")[1] if pawnAccessNumberGroup
  pawnAccessNumber

isValidPawnAccessNumber = (pawnAccessNumber)->
  /^1\d{8}$/.test(pawnAccessNumber)

pawnAccessNumberFormExist = ->
  $("#pawn_access_number_filter").length > 0

window.parseTrackData = (data)->
  # track data is of the form '%0000000^100000123^00?;0000000=100000123=00?';
  # split on semi-colon and grab the second element
  # split the second group by '=' and grab the second element
  # that's the pawn access number
  pawnAccessNumber = extract_pawn_access_number_from_track_data(data)
  if pawnAccessNumber and isValidPawnAccessNumber(pawnAccessNumber)
    $("#pawn_access_number").val(pawnAccessNumber)
    $("#pawn_access_number_filter").submit()
  else
    $(".swipe_error").text("Please swipe your Pawn Access card again.")

jQuery ->
  if pawnAccessNumberFormExist()
    window.kp_LilitabCardReader_swipeDidFinishWithData = (data)->
      window.parseTrackData data

    window.kp_LilitabCardReader_swipeDidFailWithError = (errorDomain, errorCode)->
      alert 'Card Swipe Failed'
      window.kp_LilitabCardReader_requestSwipe()

    # only show when on iPad
    if /(iPad)/.test navigator.userAgent
      window.kp_LilitabCardReader_requestSwipe()
