jQuery ->
  $('#js-flat-rate-batch-submit').click (e) ->
    $('#js-flat-rate-batch-submit').prop('disabled', true)
    e.preventDefault()
    sales_rep_email = $('#flat_rate_offer_batch_sales_rep_email').val()
    flat_rate_offer_batch_id = $('#flat_rate_offer_batch_id').val()
    $.ajax
        url: "/flat_rate_offer_batches/" + flat_rate_offer_batch_id + "/validate_sales_rep_email"
        data: { sales_rep_email: sales_rep_email }
        success: (data, textStatus, jqXHR) ->
          if(data == 'true')
            $('#js-flat-rate-batch-submit').parents('form:first').submit()
          else
            alert(data)
            $('#js-flat-rate-batch-submit').prop('disabled', false)
