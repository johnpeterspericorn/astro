
var renderQuotePartial = function(partial, $quoteContainer) {
  $quoteContainer.html(partial);
};
var getQuoteDetails = function($container){
  var obj = {};
  // get vnum
  var $vehicleEl = $container.find('.js-vnum');
  var vnumNum = $vehicleEl.attr('data-vnum');
  obj.vnum = vnumNum;
  // get agreement
  var agreementNum;
  var queryStr = window.location.search;
  queryStr = queryStr.substr(1);
  var queryArr = queryStr.split('&');
  queryArr.forEach(function(str){
    var obj = {};
    var split = str.split('=');
    if(split[0] === 'agreement'){
      agreementNum = split[1];
    }
  });
  obj.buyer_number = agreementNum;
  return obj;
};
var toggleLoadingOn = function($loader){
  if($loader.hasClass('l-hidden')){
    $loader.removeClass('l-hidden');
  }
};
var toggleLoadingOff = function($loader){
  if(!$loader.hasClass('l-hidden')){
    $loader.addClass('l-hidden');
  }
};
var submitRequestForQuote = function(val, $container) {
  if($container.attr('data-loading') !== 'true'){
    var $quoteContainer = $container.find('.js-quote-matrix');
    var $quoteLoader = $container.find('.js-loader');

    var reqObj = {};
    // what key and values do I need
    // reqObj.vnum = from list item value, put in a data attribute of the inputs
    // reqObj.buyer_number = 5000001; get buyer_number from query params
    // reqObj.price = ''; get price from input field
    reqObj = getQuoteDetails($container);
    reqObj.price = val;

    // turn on loading
    toggleLoadingOn($quoteLoader);
    $container.attr('data-loading', 'true');
    $.ajax({
      url: "/api/quote",
      data: reqObj,
      dataType: "html",
      cache: false,
      error: function(jqXHR, textStatus, errorThrown ) {
        // turn off loading
        toggleLoadingOff($quoteLoader);
        // display error as Alert
        alert('Error Loading Quote: ' + errorThrown);
        $container.attr('data-loading', 'false');
      }
    }).done(function(data) {
      $quoteContainer = $container.find('.js-quote-matrix');
      // turn off loading
      toggleLoadingOff($quoteLoader);
      renderQuotePartial(data, $quoteContainer);
      $container.attr('data-loading', 'false');
    });
  }
};





// method for rendering data to DOM
$(document).ready(function(){
  $('.js-quote-input').each(function(){
    var $target = $(this);
    var val = $target.val();
    if(val !== ''){
      var $container = $target.closest('.js-quote-container');
      var $quoteContainer = $container.find('.js-quote-el');
    }
  });
  $(document).on('click', '.js-toggle-accordion', function(event){
    var $target = $(event.target);
    var $par = $target.closest('.js-quote-container');
    var $icon = $par.find('i.fa-plus');
    var $accordion = $par.find('.js-accordion');
    var el = $accordion[0];
    var h = $accordion.height();
    var doc = document.documentElement;
    var y = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if(h > 0){
      $icon.removeClass('is_rotated');
      $accordion.addClass('accordion_closed');
    } else {
      $icon.addClass('is_rotated');
      $accordion.removeClass('accordion_closed');
      // setTimeout(function(){
      //   el.scrollIntoView();
      // }, 400);
    }
  });
  $(document).on('click', '.js-quote-amount', function(e){
    e.preventDefault();
    var $target = $(e.target);
    var $quoteInput = $target.closest('.js-amount-box').find('.js-quote-input');
    var $container = $target.closest('.js-quote-container');
    var $curActive = $container.find('.active');
    var targetValue = $target.attr('data-amount');
    if($container.attr('data-loading') !== 'true'){
      $curActive.removeClass('active');
      $target.addClass('active');
      $quoteInput.val(targetValue);
      submitRequestForQuote(targetValue, $container);
    }
  });
  $(document).on('keypress', '.js-quote-input', function(e){
    var code = e.keyCode || e.which;
    var $target = $(e.target);
    var val = $target.val();
    var $container = $target.closest('.js-quote-container');
    if($container.attr('data-loading') !== 'true'){
      if(code == 13 && val && val > 0) { //Enter keycode --  && confirm('Do you want a custom quote of: $' + val)
        var $quoteContainer = $container.find('.js-quote-el');
        submitRequestForQuote(val, $container);
      }
    }
  });
  $(document).on('focus', '.js-quote-input', function(e){
    var $target = $(e.target);
    $target.val('');
  });
  $('.wrapper').on('submit', '.js-estimate-form', function(e){
    e.preventDefault();
    var $target = $(e.target);
    var $input = $target.find('input.js-quote-input');
    var val = $input.val();
    var $container = $target.closest('.js-quote-container');
    if($container.attr('data-loading') !== 'true'){
      if(val && val > 0) {
        var $quoteContainer = $container.find('.js-quote-el');
        submitRequestForQuote(val, $container);
      }
    }
  });
});


// var fixture_quote_1 = {
//   "low_valuation":     1000,
//   "average_valuation": 1500,
//   "high_valuation":    2000,
//   "quote_price":       1234,
//   "flat_rate":         false,
//   "quotes": {
//     "250": {
//       "7":  100,
//       "14": 150,
//       "21": 200
//     },
//     "500": {
//       "7":  200,
//       "14": 250,
//       "21": 300
//     },
//     "transportation_reimbursement": 75
//   }
// };
// var fixture_quote_2 = {
//   "low_valuation":     1000,
//   "average_valuation": 1500,
//   "high_valuation":    2000,
//   "quote_price":       1234,
//   "flat_rate":         false,
//   "quotes": {
//     "250": {
//       "7":  100,
//       "14": 150,
//       "21": 200
//     },
//     "500": {
//       "14": 250,
//       "7":  200,
//       "21": 300
//     },
//     "transportation_reimbursement": 75
//   }
// };
// var fixture_quote_3 = {
//   "low_valuation":     1000,
//   "average_valuation": 1500,
//   "high_valuation":    2000,
//   "quote_price":       1234,
//   "flat_rate":         false,
//   "quotes": {
//     "250": {
//       "7":  100,
//       "14": 150,
//       "21": 200
//     },
//     "500": {
//       "14": 250,
//       "7":  200,
//       "21": 300
//     },
//     "transportation_reimbursement": 75
//   }
// };
// var fixture_quote_4 = {
//   "low_valuation":     1000,
//   "average_valuation": 1500,
//   "high_valuation":    2000,
//   "quote_price":       1234,
//   "flat_rate":         false,
//   "quotes": {
//     "250": {
//       "7":  100,
//       "14": 150,
//       "21": 200
//     },
//     "500": {
//       "14": 250,
//       "7":  200,
//       "21": 300
//     },
//     "transportation_reimbursement": 75
//   }
// };

