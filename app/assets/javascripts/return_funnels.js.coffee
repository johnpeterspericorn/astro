$ ->
  show_address = (id) ->
    $('.floor_plan_address').hide()
    if id != undefined && id != ''
      $(".floor_plan_address[data-branch-id~=#{id}]").show()

  $floor_plan_select = $('select#floor_plan_branch_id')
  show_address($floor_plan_select.val())

  $floor_plan_select.change ->
    id = $(this).val()
    $notes = $('.floor_plan_address textarea')

    if id == 'other'
      $notes.prop('required')
      $notes.prop('disabled', false)
    else
      $notes.removeProp('required')
      $notes.prop('disabled', true)

    show_address(id)
