@PromiseOffers =
  price_factors_toggler : (link) ->
    $(link).parents("li").find(".price_factors").toggle()
    false

  filter_by_agreement : ->
    agreements = PromiseOffers.selected_agreements()
    if agreements.length > 0
      $('li.vehicle').each ->
        if $(this).attr('data-agreement-no') in agreements
          $(this).show()
        else
          $(this).hide()
    else
      # Show all if no agreement #s are selected
      $('li.vehicle').each ->
          $(this).show()

  selected_agreements : ->
    agreements = []
    $('select#agreementship option:selected').each ->
      agreements.push $(this).val()
    return agreements

  vehicles_with_pricing : ->
    $(".vehicle").not(".off_lot_vehicle").not('.past_cutoff_vehicle').not(".seller_paid_vehicle")

  enable_next : ->
    PromiseOffers._set_next_state(true)

  disable_next : ->
    PromiseOffers._set_next_state(false)

  _set_next_state : (state)->
    $("input.next_btn").attr("disabled", !state)

jQuery ->
  if $("#vehicles_index_view").length
    $('#agreement_filter').click ->
      PromiseOffers.filter_by_agreement()

    PromiseOffers.disable_next()

    $(".price_factors_link").click(
      (event) ->
        event.stopImmediatePropagation()
        PromiseOffers.price_factors_toggler($(this))
    )
