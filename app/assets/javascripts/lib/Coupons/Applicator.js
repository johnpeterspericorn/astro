//= require lib/Coupons

Coupons.Applicator = function(promisePrice, couponRegistry, display) {
  display = display || alert;

  var handleSuccess = function(data) {
    var coupon = new Coupons.Coupon(data.coupon);
    promisePrice.applyCoupon(coupon);
    display('The coupon "' + coupon.code + '" has been applied');
  }

  var handleError = function(xhr) {
    var response = $.parseJSON(xhr.responseText);
    display('The coupon failed with the error: ' + response.errors);
  };

  this.apply = function(purchase_country, couponCode, ajax) {
    ajax = ajax || $.ajax
    var couponURI = '/coupons/' + couponCode + '.json';

    return ajax({ url: couponURI, data: {country: purchase_country, applied_coupons: couponRegistry.codes} })
      .done(handleSuccess)
      .fail(handleError);
  };
};

