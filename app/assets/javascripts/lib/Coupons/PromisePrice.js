//= require lib/Coupons

Coupons.PromisePrice = function(element, couponRegistry) {
  var price = element.data('price');
  var existingPrice = price;
  var couponCode;


  this.applyCoupon = function(coupon) {
    if (!couponRegistry.isRegistered(coupon.code)) {
      couponRegistry.register(coupon.code);
      couponCode = coupon.code;

      price -= coupon.amount;
      if (price < 0) price = 0;

      refreshView();
    }
  }

  this.resetCoupon = function() {
    price = existingPrice;
    couponRegistry.deregister(couponCode);
    refreshView();
  }

  function refreshView() {
    currency_format = "$"
    if ((element.text() != undefined) || (element.text() != null)){
     currency_format = element.text().trim().split(' ')[0]
    }
    element.text(currency_format + ' ' + Math.round(price));
  }
};

