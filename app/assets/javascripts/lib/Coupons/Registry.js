//= require lib/Coupons

Coupons.Registry = function(codes) {
  this.codes = codes || [];

  this.register = function(code) {
    this.codes.push(code);
  };

  this.deregister = function(code) {
    var index = this.codes.indexOf(code);
    this.codes.splice(index, 1);
  };

  this.isRegistered = function(code) {
    return $.inArray(code, this.codes) > -1;
  };
};
