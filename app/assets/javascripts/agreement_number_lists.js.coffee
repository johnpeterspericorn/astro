jQuery ->
  $('#select_all_agreement_numbers').change ->
    select_all = $(this)
    $('.flat-rate-agreement-number').prop('checked', select_all.is(':checked'))

  $('#flat_rate_param_country').change ->
    onchange_url = $(this).attr('data-onchangeurl')
    country = $(this).val()
    location.href = "#{onchange_url}&country=#{country}"
