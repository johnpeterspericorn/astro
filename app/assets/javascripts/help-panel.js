$(document).ready(function(){

  // var submitRequestForUserData = function(val) {
  //   var reqObj = {};
  //   // what key and values do I need
  //   reqObj.auth_token = "ABC123";
  //   reqObj.presale_vehicle_id = 1;
  //   reqObj.buyer_number = 5000001;

  //   $.ajax({
  //     url: "test.html",
  //     data: reqObj,
  //     dataType: "json",
  //     cache: false,
  //     context: document.body
  //   }).done(function(data) {
  //     debugger;
  //     // fill out the form with user data
  //   });
  // };

  var $doc = $(document);
  $doc.on('click', '.js-quick-guide-link', function(ev){
      var $target = $(ev.target);
      if (/AstroNativeApp/.exec(navigator.userAgent)) {
        var url = window.location.origin + $target.attr('href');
        $target.attr('href', ViewPDF.view(url));
      } else {
        $target.attr('target', '_blank');
      }
  });
  $doc.on('click', '.js-email-quick-guides', function(ev){
    ev.preventDefault();
    var $target = $(ev.target);
    var $section = $target.parents('.help-panel-l-section');
    var $list = $section.find('.' + $target.attr('data-list'));
    var $form = $section.find('.' + $target.attr('data-form'));

    if($form.is(':visible')){
      $form.addClass('l-hidden');
      $list.removeClass('l-hidden');
    } else {
      $form.removeClass('l-hidden');
      $list.addClass('l-hidden');
    }

  });
  $doc.on('click', '.js-request-phone-btn', function(ev){
    // if form isn't filled out or open, open accordion
    var $target = $(ev.target);
    var $container = $target.closest('.help-panel-l-panel-container');
    var $form = $target.closest('form');
    var formEl = $form[0];
    var $accordion = $form.find('.js-accordion');
    if($accordion.hasClass('accordion_closed')){
      ev.preventDefault();
      $($form.find('input:visible')[0]).focus();
    } else {
      return validate_form($form);
    }
  });
  var toggleHelpNav = function(ev){
    var $target = $(ev.target);
    var $parent = $target.closest('.js-help-panel');
    var $container = $parent.find('.help-panel-l-panel-container');
    var $formAccordion = $parent.find('form .js-accordion');
    var $bg = $('.js-help-panel-bg');
    // reset the help panel
    // open the help panel to the first level
    if($('.js-help-panel').hasClass('help-panel-is-open')) {
      // close
      $('.js-help-panel').removeClass('help-panel-is-open');
      // reset the container form accordion
      if(!$formAccordion.hasClass('accordion_closed')){
        $formAccordion.addClass('accordion_closed');
      }
      // reset the container scroll
      $container.animate({ scrollTop: "0px" }, 1000);
      $bg.off('click', toggleHelpNav);
      $bg.hide();

    } else {
      // open
      $('.js-help-panel').addClass('help-panel-is-open');
      $bg.on('click', toggleHelpNav);
      $bg.show();
    }
  };
  $doc.on('click', '.js-help-panel-nav', toggleHelpNav);
  $doc.on('focus', '.js-request-phone-call input#first_name', function(ev){
    var $target = $(ev.target);
    var $container = $target.closest('.help-panel-l-panel-container');
    var $form = $target.closest('form');
    var formEl = $form[0];
    var $accordion = $form.find('.js-accordion');
    if($accordion.hasClass('accordion_closed')){
      open_request_form($accordion, $container);
    }
  });
  $doc.on('change', '#problem_type', function(ev) {
    var $selectEl = $(ev.target);
    var $optionEl = $selectEl.find(':selected');
    var $otherFieldContainer = $('.js-other-field');
    if($optionEl.val() === 'Other' && $otherFieldContainer.hasClass('l-hidden')) {
      $otherFieldContainer.removeClass('l-hidden');
    } else if($optionEl.val() !== 'Other' && !$otherFieldContainer.hasClass('l-hidden')){
      $otherFieldContainer.addClass('l-hidden');
      $('#other_problem_type').val('');
    }
  });

  $doc.on('submit', '.js-request-guides', submit_guides_form);
});
var open_request_form = function($accordion, $container){
  $accordion.removeClass('accordion_closed');
  setTimeout(function(){
    $container.animate({ scrollTop: "196px" }, 1000);
  });
};
var prefillFormData = function(data){
  // fill out the user data in the form for email, phone, name, etc.

};
var validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var submit_guides_form = function(evt) {
  // check all the checkbox field to see there is a $('input:checked')
  //
  var $form = $(evt.target);
  $form.find('.alert').remove();
  var $checked = $form.find('input:checked');
  var $emailField = $form.find('#guide_email');
  var errorStr = '';
  var canSubmit = true;
  if($checked.length <= 0){
    errorStr += 'Please select one or more guides to be emailed.<br>';
    canSubmit = false;
  }
  if(!validateEmail($emailField.val())) {
    errorStr += 'Email is not valid format (test@example.com).<br>';
    canSubmit = false;
  }
  if(!canSubmit){
    // show error message
    var alertEl = $('<div class="alert alert-danger">' + errorStr +'</div>');
    $form.prepend(alertEl);

    evt.preventDefault();
  }


};
var validate_form = function($formEl) {
  // check each field for appropriate data
  // first name, last name
  // email - valid email address
  var emailRegEx = new RegExp(/^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/);
  var formElements = $formEl.find('*');
  var flag = false;
  var errors = [];
  var fields = [];
  var $retURLField = $('#js-retURL');
  var currentPath = window.location;
  formElements.filter(':input').each(function(){
    var inputName = $(this).attr('title') || $(this).attr('name');
    var isRequired = $(this).hasClass('is-required');
    var val = $(this).val();
    if(isRequired) {
      switch($(this).attr('type')) {
        case 'email':
          if(val === '') {
            flag = true;
            fields.push($(this));
            errors.push('Email is Required');
          } else if(!emailRegEx.test(val)) {
            flag = true;
            fields.push($(this));
            errors.push('Email is not valid format. (test@example.com)');
          }
        break;
        case 'tel':
          // phone number - is required
          if(val === '') {
            flag = true;
            fields.push($(this));
            errors.push('Phone Number is Required');
          }
          // phone number - valid US phone number
        break;
        case 'text':
          if(val === '') {
            fields.push($(this));
            flag = true;
            errors.push(inputName + ' is Required');
          }
        break;
        default:
        break;
      }
    }
  });
  if(flag){
    var str = '';
    errors.forEach(function(txt) {
      str += txt + "\n";
    });
    fields[0].focus();
    alert(str);
    return false;
  } else {
    var url = currentPath.href;
    if(url.indexOf('?') !== -1) {
      url += '&msg=thank_you_message_call_request';
    } else {
      url += '?msg=thank_you_message_call_request';
    }
    debugger;
    $retURLField.val(url);
    return true;
  }
};

