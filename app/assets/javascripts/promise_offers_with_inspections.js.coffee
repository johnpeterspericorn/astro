@PromiseOffersWithInspections =
  select_vehicles_with_previous_promises : () ->
    vehicles_with_selected_promises = $(PromiseOffersWithInspections.vehicles_with_inspection_ui()).filter(".promise_purchased_for_vehicle")
    $.each vehicles_with_selected_promises, (_, selected_promise) ->
      selected_distance = $(selected_promise).find(".price_matrix input[type='radio']:checked").val().split("_")[0]
      distance_radio_button = $(selected_promise).find(".distance_selections input[type='radio'][value='#{selected_distance}']")
      distance_radio_button.prop('checked', true)
      PromiseOffersWithInspections.toggle_distance_matrix(distance_radio_button)

  select_vehicles_with_previous_inspections : () ->
    inspections_only = $(PromiseOffersWithInspections.vehicles_with_inspection_ui()).filter(".inspection_purchased_for_vehicle:not(.promise_purchased_for_vehicle)")
    $.each inspections_only, (_, inspection_only) ->
      radio_button = $(inspection_only).find("input[type='radio']")
      PromiseOffersWithInspections._get_vehicle_description_from_radio_button(radio_button).addClass("selected")
      current_options     = PromiseOffersWithInspections._current_options(radio_button)
      $(current_options).find(".none").removeClass('selected')         # deselect "no coverage"
      $(current_options).find(".inspection_only").addClass('selected') # deselect "inspection only"
      $(current_options).find(".inspection_only input[type='radio']").prop("checked", true)

  promise_options_selection_handler : (radio_button) ->
    if $(radio_button).parents("td").hasClass('selected')
      this._select_no_promise_options(radio_button)
    else
      this._select_new_promise_options(radio_button)

  vehicle_description_handler : (descriptionContainer) ->
    PromiseOffersWithInspections._deselect_all_promise_options()

  toggle_distance_matrix : (radio_button) ->
    radio_button.parents("label").addClass("selected")
    other_mileage_button = radio_button.parent().siblings("label").find("input[type='radio'][name$='distance']")
    other_mileage_button.parents("label").removeClass("selected")

    clicked_milage = radio_button.val()
    otherMileage = other_mileage_button.first().val()

    promiseOptions = $(radio_button).parents(".promise_options")
    rows_container = promiseOptions.find("tbody")
    old_pricing_rows = rows_container.find(".distance_#{otherMileage}_row")
    pricing_radio_button = old_pricing_rows.find("input[type='radio']")
    PromiseOffersWithInspections._get_inspection_checkbox_from_radio_button(pricing_radio_button).prop("checked", false)
    pricing_radio_button.prop('checked', false)
    old_pricing_rows.hide()

    new_pricing_rows = rows_container.find(".distance_#{clicked_milage}_row")
    new_pricing_rows.show()

  inspection_only_handler : (radio_button) ->
    PromiseOffersWithInspections._get_vehicle_description_from_radio_button(radio_button).addClass("selected")

  vehicles_with_inspection_ui : () ->
    $(PromiseOffers.vehicles_with_pricing()).filter(".vehicle_with_inspection_ui")

  _select_no_promise_options : (radio_button) ->
    PromiseOffersWithInspections._deselect_all_promise_options()

  _select_new_promise_options : (radio_button) ->
    PromiseOffers.enable_next()
    if $(radio_button).val() == "none" || $(radio_button).val() == "inspection_only"
      PromiseOffersWithInspections._select_inspection_only_or_no_coverage(radio_button)
    else # selected a promise price
      PromiseOffersWithInspections._select_pricing_option(radio_button)

  _select_inspection_only_or_no_coverage : (radio_button) ->
    current_options     = PromiseOffersWithInspections._current_options(radio_button)
    vehicle_description = PromiseOffersWithInspections._get_vehicle_description_from_radio_button(radio_button)
    inspection_checkbox = PromiseOffersWithInspections._get_inspection_checkbox_from_radio_button(radio_button)

    $(current_options).find(".price_matrix tbody td").removeClass('selected')       # deselect all options in the pricing matrix
    $(radio_button).parents("label").siblings("label").removeClass('selected')      # deselect "inspection only" or "no coverage" depending on which is selected
    vehicle_description.removeClass('selected')                                     # dehighlight vehicle description
    inspection_checkbox.prop("checked", $(radio_button).val() == "inspection_only") # deselect inspections

    $(radio_button).parents("label").addClass('selected')                           # mark none as selected

  _select_pricing_option : (radio_button) ->
    current_options     = PromiseOffersWithInspections._current_options(radio_button)
    vehicle_description = PromiseOffersWithInspections._get_vehicle_description_from_radio_button(radio_button)
    inspection_checkbox = PromiseOffersWithInspections._get_inspection_checkbox_from_radio_button(radio_button)

    $(current_options).find(".price_matrix tbody td").removeClass('selected') # remove all current price selections
    $(current_options).find(".none").removeClass('selected')                  # deselect "no coverage"
    $(current_options).find(".inspection_only").removeClass('selected')       # deselect "inspection only"
    $(radio_button).parents("td").addClass('selected')                        # select selected promise option

    vehicle_description.addClass('selected')                                  # highlight vehicle description
    inspection_checkbox.prop("checked", $(radio_button).parents(".bundled_inspections").length > 0) # if we selected a bundled promise, select the inspection

  _get_inspection_checkbox_from_radio_button : (radio_button) ->
    PromiseOffersWithInspections._current_options(radio_button).find(".inspection input[type='checkbox']").first()

  _get_vehicle_description_from_radio_button : (radio_button) ->
    siblings = PromiseOffersWithInspections._current_options(radio_button).siblings()
    siblings.find('.vehicle_description')

  _current_options : (radio_button) ->
    current_options = $(radio_button).parents(".promise_options")

  _deselect_all_promise_options : () ->
    $(".none input[type='radio']").click()

jQuery ->
  if $("#vehicles_index_view").length
    PromiseOffersWithInspections.select_vehicles_with_previous_promises()
    PromiseOffersWithInspections.select_vehicles_with_previous_inspections()

    $(PromiseOffersWithInspections.vehicles_with_inspection_ui()).not(".promise_purchased_for_vehicle")
      .find('.price_matrix input[type="radio"], .no_promise_options input[type="radio"]').click (event) ->
          PromiseOffersWithInspections.promise_options_selection_handler($(this))

    $(PromiseOffersWithInspections.vehicles_with_inspection_ui()).not(".promise_purchased_for_vehicle")
      .find('.distance_selections input[type="radio"]').click (event) ->
        PromiseOffersWithInspections.toggle_distance_matrix($(this))

    $(PromiseOffersWithInspections.vehicles_with_inspection_ui()).find('.vehicle_description').click (event) ->
          PromiseOffersWithInspections.vehicle_description_handler($(this))

    $(PromiseOffersWithInspections.vehicles_with_inspection_ui())
      .find(".no_promise_options .inspection_only input[type='radio']").click (event) ->
        PromiseOffersWithInspections.inspection_only_handler($(this))
