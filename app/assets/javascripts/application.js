// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require jquery.datetimepicker
//= require handlebars
//= require html5shiv-printshiv
//= require quote
//= require help-panel
//= require datetimepicker
//= require clipboard.min.js
//= require jszip.js
//= require papaparse.min.js
//= require select2.full.min.js
//= require xlsx.js
//= require js.cookie.min
//= require locales
//= require components
//= require_tree ./templates
//= require_directory .


(function(document,navigator,standalone) {
  // prevents links from apps from openning in mobile safari
  // this javascript must be the first script in your <head>
  if ((standalone in navigator) && navigator[standalone]) {
    var curnode, location=document.location, stop=/^(a|html)$/i;
    document.addEventListener('click', function(e) {
      curnode=e.target;
      while (!(stop).test(curnode.nodeName)) {
        curnode=curnode.parentNode;
      }
      // Conditions to do this only on links to your own app
      // if you want all links, use if('href' in curnode) instead.
      if ('href' in curnode
          && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) )
        && !(curnode.attributes.getNamedItem('data-method'))
         ) {
           e.preventDefault();
           location.href = curnode.href;
         }
    },false);
  }
})(document,window.navigator,'standalone');

formated_datetime = function(datetime){
  datetime = new Date(datetime)
  if(datetime != 'Invalid Date'){
    return (datetime.getMonth()+1) + "/" + datetime.getDate() + "/" +datetime.getFullYear().toString().substr(2,2)+ " " + datetime.getHours() + ":" + datetime.getMinutes()
  }
}
