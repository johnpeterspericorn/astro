jQuery ->
  readonly = $(".unpurchased_form").attr("data-readonly");
  if readonly == "true"
    $(".unpurchased_form :input").prop('disabled', true)
  else
    $(".unpurchased_form :input").removeAttr("disabled");
