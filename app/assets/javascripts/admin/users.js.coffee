jQuery ->
  toggleChildren = (input)->
    checkbox = $(input)
    if checkbox.is(':checked')
      checkbox.parent().next().show()
    else
      checkbox.parent().next().hide()
      checkbox.parent().next().find(':checkbox:checked').click()

  $('.toggle-dependent-roles').click ->
    toggleChildren(this)

  $('.toggle-dependent-roles').each ->
    toggleChildren(this)
