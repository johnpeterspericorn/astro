jQuery ->
  $(document).on 'change', '.audit_select select', ->
    selection_text = $('option:selected', this).text()
    $(this)
      .parents('.audit_reason')
      .find('textarea')
      .text(selection_text)
