jQuery ->
  $(".js-miles-selected").on("change", (e) ->
    odometer_limit_element = $(".js-odometer-limit");

    if(odometer_limit_element.length)
      odometer_reading = parseInt($(".js-odometer-reading").val()) || 0
      miles_selected = parseInt(this.value)
      odometer_limit_element.val(odometer_reading + miles_selected)
  )
