jQuery ->
  $("input[type='checkbox'][name^='settings']").on("change", ->
    $(this).parents("form").submit()
  )
  $("form").on("submit", (e) ->
    $(this).find(".status").text("Spinner")
  ).on("ajax:success", (event, data, status, xhr)->
    $(this).find(".status").text("Updated Successfully")
  ).on("ajax:error", (event, xhr, status, error)->
    $(this).find(".status").text("Error Updating")
  )
