jQuery ->
  $('.data-table').each ->
    table = $(this)
    sort_column    = table.data('sort-column')    || 0
    sort_direction = table.data('sort-direction') || 'asc'
    number_of_rows = table.data('number-of-rows')
    paginate = table.data('paginate') || false
    table.dataTable
      bPaginate: paginate,
      bLengthChange: false,
      bSort:     true,
      aaSorting: [[sort_column, sort_direction]],
      iDisplayLength: number_of_rows

