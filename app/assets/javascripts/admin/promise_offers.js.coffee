@AdminPromiseOffers =
  recordSelected : (data) ->
    row = $("td.vnum:contains('#{data.vnum}')").parent()
    jQuery.each(Object.keys(data),
      (idx, key) ->
        row.find("td.#{key}").text(data[key])
    )

jQuery ->
  if $("#promise_offers_view").length
    pusher = new Pusher('0a5e410bf13be0ffabeb')
    channel = pusher.subscribe('promise_offers')

    channel.bind('record_offered',
      (data) ->
        AdminPromiseOffers.recordSelected(data)
    )
