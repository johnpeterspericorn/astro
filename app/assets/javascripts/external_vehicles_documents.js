$(document).ready(function(){
  $(document.body).on('click', '.doc_upload_div #add_more_docs' ,function(e){
    e.preventDefault();
    batch_id = $('#external_vehicles_images__external_vehicles_batch_id').val();
    $('.doc_upload_div .upload_docs:last').after("<div class='upload_docs'><input type='file' name='external_vehicles_documents[]' id='document'><a id='remove_doc' class='button' href='#' style='margin-left: 5px;'>-</a><a id='add_more_docs' class='button' href='#'  style='margin-left: 4px;'>+</a></div>");
  });

  $(document.body).on('click', '.doc_upload_div #remove_doc' ,function(){
    if($('.upload_docs').length==1){
      $(this).closest('.upload_docs').find('#document').val('')
      $(this).closest('.upload_docs').find('#document').removeClass('file-loaded');
      return false;
    }
    $(this).closest('.upload_docs').remove();
  });
});
