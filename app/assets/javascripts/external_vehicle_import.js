$(document).ready(function(){

  function parse_csv(data){
    roa = Papa.parse(data, {header: true, skipEmptyLines:true})
    if(roa.data.length > 0){
      $(".sold_vehicles_entry_table tr:not(tr#labels)").remove();
      fill_sold_vehicles_entry_table(roa.data);
    }
  }

  function to_json(workbook) {
    workbook.SheetNames.forEach(function(sheetName) {
      var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
      if(roa.length > 0){
        $(".sold_vehicles_entry_table tr:not(tr#labels)").remove();
        fill_sold_vehicles_entry_table(roa);
      }
    });
  }

  function fill_sold_vehicles_entry_table(roa){
    roa.forEach(function (value) {
      if(isEmpty(value)){ return false }
      $('.sold_vehicles_entry_table tr:last .add_row input').remove()
      populate_grid_row();
      var new_row = $(".sold_vehicles_entry_table tr:last");
      var vnum = new_row.find('.vnum')
      var make = new_row.find('.make')
      var model = new_row.find('.model')
      var year = new_row.find('.modal_year_select')
      vnum.val(findValueByPrefix(value, "VNUM"));
      make.val(findValueByPrefix(value, "Make"));
      model.val(findValueByPrefix(value, "Model"));
      year.val(findValueByPrefix(value, "Year"));
      new_row.find('.odometer_reading').val(findValueByPrefix(value, "Odometer"));
      new_row.find('.agreement_no').val(findValueByPrefix(value, "Agreement")).trigger("change");
      if(findValueByPrefix(value, "Vehicle Purchase")){
        new_row.find('.vehicle_purchase_price').val(findValueByPrefix(value, "Vehicle Purchase")).trigger("change").addClass('option_filled')
      }
      new_row.find('.buy_fee').val(findValueByPrefix(value, "Buy")).trigger("change");
      new_row.find('.vehicle_total').val(findValueByPrefix(value, "Vehicle Total"));
      if(findValueByPrefix(value, "Location")){
        new_row.find('.location_initials').val(findValueByPrefix(value, "Location")).addClass('option_filled');
      }
      new_row.find('.automatic_purchase_email').val(findValueByPrefix(value, "Email"));
      new_row.find('.purchased_at').val(formated_datetime(findValueByPrefix(value, "Promise"))).trigger("change");
      new_row.find('.seller_name').val(findValueByPrefix(value, "Seller"));
      if(make.val()=='' || model.val()=='' || year.val() ==  null){
        vnum.trigger("change");
      }
      initialize_select_2();
    });
    copy_data_to_clipboard_text($('.vnum'));
  }

  function findValueByPrefix(object, prefix) {
    for (var property in object) {
      if (object.hasOwnProperty(property) && property.toString().startsWith(prefix)) {
        return object[property].trim()
      }
    }
  }

  function isEmpty(object){
    for (var property in object) {
      if(object.hasOwnProperty(property)){
        if(object[property].trim() != ''){
          return false
        }
      }
    }
    return true
  }

  var xlf = document.getElementById('xlf');
  function handleFile(e) {
    var files = xlf.files;
    if(files.length >0){
      var f = files[0];
      {
        var reader = new FileReader();
        var name = f.name;
        var re = /(\.xlsx|\.xls|\.csv)$/i;
        if(!re.exec(name))
        {
          alert("File extension not supported!");
        }else{
          reader.onload = function(e){
            var data = e.target.result;
            if(/(\.xlsx|\.xls)$/i.exec(name)){
              var wb;
              wb = XLSX.read(data, {type: 'binary'});
              to_json(wb)
            }else{
              parse_csv(data)
            }
          };
          reader.readAsBinaryString(f);
        }
      }
    }else{
      alert("Please choose a file");
    }

  }

  $(document.body).on('click', '.xlsx_import_div #upload_data' ,function(e){
    handleFile(e);
  });
});
