jQuery ->
  if $("#pricing_distribution_view").length

    $(".pricing_distribution").first().show()
    $(".vehicle_category_link").first().addClass('green')

    $(".vehicle_category_link").click (e)->
      e.preventDefault()
      $(this).addClass('green').siblings('.vehicle_category_link').removeClass('green')
      $(".pricing_distribution").hide()
      show_category_id = $(this).data("category")
      $(".pricing_distribution.category-"+show_category_id).show()
