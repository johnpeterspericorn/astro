@PromiseOffersWithoutInspections =
  promise_options_selection_handler : (radioButton) ->
    if $(radioButton).parents("td").hasClass('selected')
      this._select_no_promise_options(radioButton)
    else
      this._select_new_promise_options(radioButton)

  vehicle_description_handler : (descriptionContainer) ->
    $(descriptionContainer).removeClass('selected')
    siblings = $(descriptionContainer).parent().siblings()
    siblings.find('td').removeClass('selected')
    siblings.find('input[type="radio"]').attr('checked',false)
    siblings.find('td.none').addClass('selected')
    radioButton = siblings.find('td.none').find("input[type='radio']")
    radioButton.attr('checked', true)

  select_previously_selected_vehicles : () ->
    checked_radio_buttons = $('input[type="radio"]:checked')

  _select_no_promise_options : (radioButton) ->
    $(radioButton).attr('checked',false)
    tableBody = $(radioButton).parents("tbody")
    tableBody.find('td').removeClass('selected')
    tableBody.find('td.none').addClass('selected')
    tableBody.find('td.none').find("input[type='radio']").attr('checked', true)
    $(radioButton).parents(".promise_options").siblings().find('.vehicle_description').removeClass('selected')

  _select_new_promise_options : (radioButton) ->
    $(radioButton).parents("tbody").find('td').removeClass('selected')
    $(radioButton).parents("td").addClass('selected')

    vehicle_description = this._get_vehicle_description_from_radio_button(radioButton)
    if $(radioButton).val() == "none"
      vehicle_description.removeClass('selected')
    else
      PromiseOffers.enable_next()
      vehicle_description.addClass('selected')

  _get_vehicle_description_from_radio_button : (radioButton) ->
    this._get_radio_button_vehicle_siblings(radioButton).find('.vehicle_description')

  _get_radio_button_vehicle_siblings : (radioButton) ->
    $(radioButton).parents(".promise_options").siblings()

jQuery ->
  if $("#vehicles_index_view").length
    PromiseOffersWithoutInspections.select_previously_selected_vehicles()

    vehicles_without_inspection_ui = ->
      $(PromiseOffers.vehicles_with_pricing()).filter(".vehicle_without_inspection_ui")

    $(vehicles_without_inspection_ui()).not(".promise_purchased_for_vehicle")
      .find('.promise_options input[type="radio"]').click(
        (event) ->
          PromiseOffersWithoutInspections.promise_options_selection_handler($(this))
      )
    $(vehicles_without_inspection_ui()).find('.vehicle_description').click(
        (event) ->
          PromiseOffersWithoutInspections.vehicle_description_handler($(this))
      )
