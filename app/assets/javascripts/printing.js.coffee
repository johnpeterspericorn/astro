@Printing =
  print : () ->
    if /AstroNativeApp/.exec(navigator.userAgent)
      document.location = "astro:printView:"
    else
      if window.print
        window.print()

jQuery ($) ->
  $('.print_btn').click(
    (event) ->
      Printing.print()
  )
