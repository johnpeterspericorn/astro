minWidth = 480
layoutMargin = -3

layoutTest = ->
  winWidth = $(window).width()

  if (winWidth > minWidth)
    layoutMargin = 17

    if ($('.checkout_bar').length)
      barheight = $('.checkout_bar').outerHeight()
      $('body').css({ top: barheight + layoutMargin })
      $('.js-help-panel').css({ top: barheight + layoutMargin + 4})
    if ($('.site-footer').length)
      barheight = $('.site-footer').outerHeight()
      $('body').css({ bottom: barheight + layoutMargin })
      $('.js-help-panel').css({ bottom: barheight + layoutMargin + 4 })

  else
    $('body').css({ bottom: 0, top: 0 })
    $('.js-help-panel').css({ bottom: 0, top: 0 })

jQuery ->
  layoutTest()
  $(window).resize ->
    layoutTest()
