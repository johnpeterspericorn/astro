$(document).ready(function(){

  // $(document).on('submit', '.js-filter-pawn', function(ev){
  //   ev.preventDefault();
  //   var $target = $(ev.target);
  //   var queryStr = window.location.search.substring(1);
  //   var queryArray = queryStr.split('&');
  //   var keyArray = [];
  //   queryArray.forEach(function(val) {
  //     var obj = {};
  //     var splitArray = val.split("=");
  //     obj.key = splitArray[0];
  //     obj.val = splitArray[1];
  //     keyArray.push(obj);
  //   });
  //   var action = $target.attr('action');
  //   debugger;
  // });
  $(document).on('change', '.js-agreement-list', function(ev){
    var $this = $(ev.target);
    var $btnRow = $('.js-search-btns');
    if($this.val()!== ''){
      if($btnRow.hasClass('l-hidden')) {
        $btnRow.removeClass('l-hidden');
      }
    } else {
      if(!$btnRow.hasClass('l-hidden')) {
        $btnRow.addClass('l-hidden');
      }
    }
  });
  $(document).on('click', '.js-filter-search-btn', function(ev) {
    ev.preventDefault();
    var $target = $(ev.target);
    var href = $target.attr('href');
    var $container = $target.closest('.js-filter-container');
    var $agreementList = $container.find('.js-agreement-list');
    if($agreementList.find(':selected') !== '' && $agreementList.find(':selected') !== undefined){
      href += '&agreement=' + $agreementList.val();
      $target.attr('href', href);
    }
    window.location = $target.attr('href');
  });
});
