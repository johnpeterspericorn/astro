jQuery ($) ->
  $(document).on 'submit', 'form.prevent-multiple-submissions', ->
    $(this).find(':submit').attr('disabled', true)
