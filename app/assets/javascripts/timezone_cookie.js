$(document).ready(function(){
  var offset = - new Date().getTimezoneOffset();
  Cookies.set('browser.tzoffset', offset, { expires: 1 });
});
