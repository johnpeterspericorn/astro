# encoding: utf-8
CancellationPolicy = Struct.new(:user, :promise_purchase) do
  def create?
    user.super_user? && !promise_purchase.return_initiated?
  end

  def may_make_ineligible?
    user.admin?
  end

  def permitted_attributes
    if user.admin?
      [:reason, :ineligible_for_repurchase, :skip_applying_credit]
    else
      [:reason]
    end
  end

  def cancellation_reason_options
    cancellation_reasons.tap do |reasons|
      reasons.concat(ineligible_cancellation_reasons) if user.admin?
    end
  end

  private

  def cancellation_reasons
    I18n.t('cancellation.cancellation_reasons').values
  end

  def ineligible_cancellation_reasons
    I18n.t('cancellation.ineligible_cancellation_reasons').values
  end
end
