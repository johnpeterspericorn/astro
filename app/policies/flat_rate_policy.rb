# encoding: utf-8
FlatRatePolicy = Struct.new(:user, :flat_rate) do
  def index?
    user.flat_rate_inquiry_viewer?
  end

  def new?
    index? && user.flat_rate_inquiry_viewer?
  end

  def create?
    index? && user.flat_rate_editor?
  end

  def update?
    index? && user.flat_rate_editor?
  end

  def destroy?
    index? && user.flat_rate_editor?
  end

  def build_offer?
    flat_rate.eligible? || (index? && flat_rate.persisted?)
  end
end
