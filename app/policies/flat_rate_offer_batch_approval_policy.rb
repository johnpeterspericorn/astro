# encoding: utf-8
class FlatRateOfferBatchApprovalPolicy < FlatRateOfferBatchPolicy
  def edit?
    show?
  end

  def create?
    show?
  end

  def destroy?
    show?
  end
end
