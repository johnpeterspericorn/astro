# encoding: utf-8
DocPolicy = Struct.new(:user) do
  def index?
    user.super_user?
  end

  def show?
    index?
  end
end
