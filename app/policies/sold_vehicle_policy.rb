# encoding: utf-8
class SoldVehiclePolicy
  def initialize(user)
    @user = user
  end

  def show?
    user.vehicle_creator?
  end

  def new?
    show?
  end

  def edit?
    show?
  end

  def create?
    show?
  end

  def update?
    show?
  end

  private

  attr_reader :user
end
