# encoding: utf-8
class ExternalVehiclesLoadPolicy
  def initialize(user)
    @user = user
  end

  def show?
    true
  end

  def new?
    !(approver? && !enterer?)
  end

  def edit?
    !(approver? && !enterer?)
  end

  def create?
    !(approver? && !enterer?)
  end

  def update?
    !(approver? && !enterer?)
  end

  def approve?
    approver?
  end

  private

  attr_reader :user

  def enterer?
    user.external_vehicle_enterer?
  end

  def approver?
    user.external_vehicle_approver?
  end
end
