# encoding: utf-8
AdminPolicy = Struct.new(:user) do
  def authorized?
    user.admin?
  end

  def authorized_to_edit_feature_flags?
    authorized? && user.feature_flag_editor?
  end

  def authorized_to_view_promise_admin?
    user.promise_admin_viewer?
  end

  def authorized_to_edit_promise_admin?
    user.promise_admin_editor?
  end

  def authorized_to_edit_user_admin?
    authorized? && user.user_admin_editor?
  end

  def authorized_to_view_promise_admin_section?
    authorized_to_view_promise_admin? || authorized_to_edit_promise_admin?
  end

  def authorized_to_view_loss_prevention_section?
    user.loss_prevention_viewer?
  end

  def authorized_to_view_finance_admin_section?
    user.finance_admin_viewer?
  end

  def authorized_to_view_flat_rate_inquiry_section?
    user.flat_rate_inquiry_viewer?
  end

  def authorized_to_view_flat_rate_inquiry_admin_section?
    authorized? && authorized_to_view_flat_rate_inquiry_section?
  end

  def authorized_to_edit_flat_rate_admin_section?
    authorized? && user.flat_rate_editor?
  end

  def authorized_to_enter_external_vehicle?
    authorized? && user.external_vehicle_enterer?
  end

  def authorized_to_approve_external_vehicle?
    authorized? && user.external_vehicle_approver?
  end
end
