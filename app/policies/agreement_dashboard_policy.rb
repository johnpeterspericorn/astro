# encoding: utf-8
AgreementDashboardPolicy = Struct.new(:user, :pawn_access_number) do
  def show?
    user.super_user? || (user.buyer_user? && pawn_access_number == user.pawn_access_number) || user.kiosk_user?
  end
end
