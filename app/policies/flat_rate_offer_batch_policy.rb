# encoding: utf-8
FlatRateOfferBatchPolicy = Struct.new(:user, :flat_rate_offer_batch) do
  def show?
    user.flat_rate_inquiry_viewer? || pawn_access_number_match? || user_has_batch_agreement_numbers?
  end

  def edit?
    show? && user.flat_rate_editor?
  end

  def update?
    show? && user.flat_rate_editor?
  end

  def create?
    show? && user.flat_rate_editor?
  end

  def destroy?
    show? && user.flat_rate_editor?
  end

  private

  def pawn_access_number_match?
    user.pawn_access_number == flat_rate_offer_batch.pawn_access_no
  end

  def user_has_batch_agreement_numbers?
    (flat_rate_offer_batch.offered_agreement_numbers - user.agreement_numbers).empty?
  end
end
