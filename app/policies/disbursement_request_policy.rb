# encoding: utf-8
DisbursementRequestPolicy = Struct.new(:user) do
  def show?
    user.super_user?
  end
end
