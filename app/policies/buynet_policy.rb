# encoding: utf-8
BuynetPolicy = Struct.new(:nonreimbursable_fees, :user) do
  def skip_payment?
    (user.super_user? || user.buyer_user? && nonreimbursable_fees.total > 0) && user.invoiceable?
  end
end
