# encoding: utf-8
ReturnInvoicePolicy = Struct.new(:user, :return_invoice, :pawn_access_number) do
  def create?
    if user.super_user? || user.kiosk_user?
      true
    else
      user.owns_offer?(return_invoice.promise_offer)
    end
  end

  def show?
    pawn_access_number = self.pawn_access_number || user.buyer_user? && user.pawn_access_number

    if user.super_user? || user.kiosk_user?
      true
    elsif pawn_access_number.present? && user.buyer_user? && return_invoice
      user.owns_offer?(return_invoice.promise_offer)
    else
      false
    end
  end

  def update?
    user.super_user? && !return_invoice.vehicle_received
  end

  def destroy?
    user.super_user? && !return_invoice.vehicle_received?
  end
end
