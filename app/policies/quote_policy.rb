# encoding: utf-8
QuotePolicy = Struct.new(:user, :quote) do
  def show?
    user.api_user? || user.agreement_numbers.include?(quote.buyer_number.to_s)
  end
end
