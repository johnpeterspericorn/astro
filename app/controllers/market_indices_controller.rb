# encoding: utf-8
class MarketIndicesController < ApplicationController
  respond_to :json

  def show
    market_index = MarketIndex.first

    respond_with market_index, include: :market_factors
  end
end
