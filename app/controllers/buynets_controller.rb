# encoding: utf-8
class BuynetsController < ApplicationController
  def show
    @fees = promise_purchase.fees
    @country = promise_purchase.country
  end

  private

  def promise_purchase_id
    params[:promise_purchase_id]
  end
  helper_method :promise_purchase_id

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find(promise_purchase_id)
  end
  helper_method :promise_purchase

  def promise_offer
    promise_purchase.promise_offer
  end
  helper_method :promise_offer

  def floor_plan_company
    promise_offer.floor_plan_company
  end
  helper_method :floor_plan_company
end
