# encoding: utf-8
class AdminController < ApplicationController
  layout 'admin'

  before_action :authorize_admin

  private

  def admin_policy
    @admin_policy ||= AdminPolicy.new(current_user)
  end

  def authorize_admin
    authorize(admin_policy.authorized?)
  end

  def authorize_finance_sections_viewer
    authorize(admin_policy.authorized_to_view_finance_admin_section?)
  end

  def authorize_flat_rate_inquiry_viewer
    authorize(admin_policy.authorized_to_view_flat_rate_inquiry_section?)
  end

  def authorize_flat_rate_admin_editor
    authorize(admin_policy.authorized_to_edit_flat_rate_admin_section?)
  end

  def authorize_promise_admin_editor
    authorize(admin_policy.authorized_to_edit_promise_admin?)
  end

  def authorize_feature_flag_editor
    authorize(admin_policy.authorized_to_edit_feature_flags?)
  end

  def authorize_user_admin_editor
    authorize(admin_policy.authorized_to_edit_user_admin?)
  end

  def admin_sections
    %w(
      pawn_locations failover
    )
  end

  def finance_sections
    %w(
      payments floor_plan_companies floor_plan_branch_selections accounts_receivable_report_offers coupons
    )
  end
  helper_method :admin_sections, :finance_sections, :admin_policy
end
