# encoding: utf-8
class PaymentsController < ApplicationController
  def new
    update_return_fee
    @country = promise_purchase.country
    @payment_type = params[:payment_type]
    set_invoice_in_session if pplv_return_invoice? && !promise_purchase.seller_paid
  end

  def create
    update_return_fee
    payment_form.process
    if payment_form.success?
      process_return_fee_payment_and_vra_generation && return if return_fee_eligible?
      redirect_to root_path, notice: t('return_payment.successful')
    else
      flash.now[:notice] = t('return_payment.failure')
      render :new
    end
  end

  private

  def payment_form
    @payment_form ||= PaymentForm.new(payment_form_attributes)
  end
  helper_method :payment_form

  def payment_form_attributes
    payment_form_params = params.fetch(:payment_form) { Hash.new }
    payment_form_params[:promise_purchase] = promise_purchase
    payment_form_params[:user] = current_user
    payment_form_params[:token] = params[:stripeToken] if params.key?(:stripeToken)
    payment_form_params[:return_fee] = session[:return_fee].to_f if return_fee_eligible?
    payment_form_params
  end

  def payment_form_path
    promise_purchase_payment_path(promise_purchase_id)
  end
  helper_method :payment_form_path

  def promise_purchase_id
    params[:promise_purchase_id]
  end

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find(promise_purchase_id)
  end

  def decorated_promise_purchase
    promise_purchase.decorate
  end
  helper_method :decorated_promise_purchase

  def process_return_fee_payment_and_vra_generation
    clear_return_fee_info_session_params
    if return_processed?
      create_floor_plan_branch_selection(return_invoice) if floor_plan_branch
      return_invoice.finalize(other_branch_notes: other_branch_notes)
      handle_return_invoice_savnumg_success
    else
      handle_return_invoice_savnumg_failure
    end
  end

  def other_branch_notes
    session[:other_branch_notes]
  end

  def floor_plan_branch
    session[:floor_plan_branch]
  end

  def return_invoice
    session[:return_invoice]
  end

  def set_invoice_in_session
    session[:return_invoice] = ReturnInvoice.new(params.require(:return_invoice).permit!)
  end

  def clear_return_info_session_params
    [:return_invoice, :promise_purchase_id, :floor_plan_branch, :other_branch_notes].each { |x| session.delete(x) }
  end

  def clear_return_fee_info_session_params
    [:skip_fee_payment, :return_fee].each { |x| session.delete(x) }
  end

  def agreement_number
    promise_purchase.promise_offer.agreement_no
  end

  def create_floor_plan_branch_selection(return_invoice)
    FloorPlanBranchSelection.create!(
      agreement_no: agreement_number,
      company_code: floor_plan_branch.company_code,
      site_no: floor_plan_branch.site_no,
      return_invoice_id: return_invoice.id
    )
  end

  def handle_return_invoice_savnumg_success
    session.delete(:pawn_access_number)
    flash[:success] = t('return_invoice.finalize_return.success_message')
    options = { slug: return_invoice.slug, pawn_access_number: return_invoice.pawn_access_no }
    clear_return_info_session_params
    redirect_to obfuscated_return_certificate_path(options), notice: t('return_payment.successful')
  end

  def handle_return_invoice_savnumg_failure
    if return_invoice.missing_data?
      render 'return_funnel/verifications/new', return_invoice: return_invoice
    else
      session.delete(:pawn_access_number)
      render 'return_funnel/failure'
    end
  end

  def return_processed?
    return_invoice.log_new_product_fee
    return_invoice.save!
    return_invoice.vra_initiation_adjustments_job
    true
  rescue => e
    Rails.logger.error "Error message: #{e.message}"
    return false
  end

  def pplv_return_invoice?
    params[:return_invoice].present? && promise_purchase.pplv?
  end

  def return_fee_eligible?
    session[:return_fee].present? && promise_purchase.pplv? && (params[:payment_type] != 'promise') && !promise_purchase.seller_paid
  end

  def fee_data
    @fee_data ||= NewProductFeeInformation.new(promise_purchase).fee_details
  end

  def update_return_fee
    return unless return_fee_eligible?
    fee = fee_data[:fee]
    session[:return_fee] = fee if session[:return_fee] != fee.to_s
  end
end
