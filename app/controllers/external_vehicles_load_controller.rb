# encoding: utf-8
class ExternalVehiclesLoadController < ApplicationController
  def new
    authorize(policy.new?)
  end

  def create
    authorize(policy.create?)
    external_vehicle_service = ExternalVehiclesLoadService.new(params, current_user)
    external_vehicle_service.save
    redirect_to new_external_vehicles_load_path, notice: 'Successfully added Vehicles for approval'
  rescue ExternalVehiclesLoadService::DataInvalidationError => e
    flash[:error] = e.message
    render :new
  end

  def update
    authorize(policy.update?)
    external_vehicle = ExternalVehiclesLoad.find(params[:id])
    rejected = external_vehicle.rejected?

    params[:external_vehicles_load][:automatic_purchase_email] = automatic_purchase_email
    params[:external_vehicles_load][:purchased_at] = format_purchased_at(params[:external_vehicles_load][:purchased_at])

    if external_vehicle.update_attributes(external_vehicles_params)
      if rejected
        external_vehicle.update_attributes(status: 'unapproved')
        external_vehicle.external_vehicles_batch.update_status
      end
      render json: { id: external_vehicle.id, status: :success }
    else
      render json: { errors: external_vehicle.errors.to_a }, status: :unprocessable_entity
    end
  end

  def approve
    authorize(policy.approve?)
    external_vehicle = ExternalVehiclesLoad.find(external_vehicles_id)
    if external_vehicle.process_approve
      external_vehicle.update_attributes(approvnumg_user: current_user)
      render json: { id: external_vehicle.id, status: :success }
    else
      render json: { errors: external_vehicle.errors.to_a }, status: :unprocessable_entity
    end
  end

  def reject
    authorize(policy.approve?)
    external_vehicle = ExternalVehiclesLoad.find(params[:external_vehicles_load_id])
    external_vehicle.update_attributes(rejecting_user: current_user)
    if external_vehicle.process_reject params
      ExternalVehicleNotificationMailer.vehicle_rejection(external_vehicle).deliver_later
      render json: { id: external_vehicle.id, status: :success }
    else
      render json: { errors: external_vehicle.errors.to_a }, status: :unprocessable_entity
    end
  end

  private

  def policy
    @policy ||= ExternalVehiclesLoadPolicy.new(current_user)
  end

  def external_vehicles_id
    params[:external_vehicles_load_id]
  end

  def automatic_purchase_email
    params[:external_vehicles_load][:automatic_purchase_email].first
  end

  def format_purchased_at(purchased_at)
    DateTime.strptime("#{purchased_at} #{Time.zone.now.zone}", '%m/%d/%y %H:%M %Z')
  end

  def external_vehicles_params
    params
      .require(:external_vehicles_load)
      .permit(
        :vnum,
        :make,
        :year,
        :model,
        :odometer_reading,
        :agreement_no,
        :vehicle_purchase_price,
        :buy_fee,
        :vehicle_total,
        :location_initials,
        :purchased_at,
        :automatic_purchase_email,
        :seller_name,
        :rejection_comment
      )
  end
end
