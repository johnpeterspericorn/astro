# encoding: utf-8
class ReturnCancellationsController < ApplicationController
  def new
    authorize(return_invoice_policy.destroy?)
    @cancellation = ReturnInvoiceCancellation.new(return_invoice: return_invoice, user: current_user)
  end

  def create
    authorize(return_invoice_policy.destroy?)
    @cancellation = ReturnInvoiceCancellation.new(return_invoice_params)

    if @cancellation.process
      redirect_to root_path
    else
      flash.now[:error] = @cancellation.errors.full_messages.to_sentence
      render :new
    end
  end

  def destroy
    invoice = ReturnInvoice.unscoped.find(return_invoice_id)
    authorize(ReturnInvoicePolicy.new(current_user, invoice).create?)
    @invoice = ReturnInvoiceCancellation.new(return_invoice: invoice, user: current_user)
    @invoice.reinstate
    redirect_to root_path
  end

  def js_translation_feed
    data_feed = JsTranslationPresenter.new(params[:cancellation_reason]).process_return_cancellation
    respond_to do |format|
      format.json { render json: data_feed, status: :ok }
    end
  end

  private

  def return_invoice_policy
    @return_invoice_policy ||= ReturnInvoicePolicy.new(current_user, return_invoice)
  end
  helper_method :return_invoice_policy

  def return_invoice_params
    params
      .require(:return_invoice_cancellation)
      .permit(:reason, :send_email, :user_email)
      .merge(return_invoice: return_invoice, user: current_user)
  end

  def return_invoice_id
    params[:return_invoice_id]
  end

  def return_invoice
    @return_invoice ||= ReturnInvoice.find(return_invoice_id)
  end
  helper_method :return_invoice

  def form_path
    return_invoice_return_cancellation_path(return_invoice_id)
  end
  helper_method :form_path
end
