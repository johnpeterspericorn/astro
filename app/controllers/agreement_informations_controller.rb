# encoding: utf-8
class AgreementInformationsController < ApplicationController
  respond_to :json, :xml

  def show
    @agreement = AgreementInformation.find_by_agreement_no(params[:agreement_no])
    if @agreement
      render json: { agreement_no: @agreement.agreement_no, agreementship_name: agreement_name }
    else
      render json: nil
    end
  end

  private

  def agreement_name
    @agreement.agreementship_name.split(' ').map(&:capitalize).join(' ')
  end
end
