# encoding: utf-8
class PawnLocationsController < ApplicationController
  skip_before_action :authenticate_user!
  respond_to :html, :json

  def index
    @pawn_locations = PawnLocation.with_purchases_enabled.order(:name)

    respond_with(@pawn_locations) do |format|
      format.html
      format.json
    end
  end
end
