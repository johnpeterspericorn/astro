# encoding: utf-8
class DisbursementRequestsController < ApplicationController
  def show
    authorize(disbursement_request_policy.show?)

    redirect_to root_path, notice: t('return_invoice.disbursement.not_found') unless return_invoice
  end

  private

  def return_invoice_id
    params[:return_invoice_id]
  end

  def return_invoice
    @return_invoice ||= ReturnInvoice.find_by_id(return_invoice_id)
  end

  def return_certificate
    @return_certificate ||= ReturnCertificate.new(return_invoice)
  end

  def disbursement_request_policy
    @disbursement_request_policy ||= DisbursementRequestPolicy.new(current_user)
  end

  helper_method :return_certificate
end
