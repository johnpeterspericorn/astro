# encoding: utf-8
class VehicleFiltersController < ApplicationController
  def show
    if current_user.buyer_user?
      pawn_access_number = current_user.pawn_access_number
      session[:pawn_access_number] = pawn_access_number
      redirect_to promise_offers_path(pawn_access_number: pawn_access_number)
    elsif user_can_skip_vehicle_filter_selection?
      redirect_to edit_vehicle_filter_path(filter_type: :pawn_access_number)
    end
  end

  def edit
    @partial = params[:filter_type] if VehicleFilter::FILTER_TYPES.include?(params[:filter_type])
  end

  def update
    if filter_query_data.filtered_by?(:pawn_access_number)
      session[:pawn_access_number] = filter_query_data.pawn_access_number
    end
    redirect_to promise_offers_path(vehicle_filter_params)
  end

  private

  def user_can_skip_vehicle_filter_selection?
    current_user.kiosk_user? && !show_badge_button? && !show_agreement_filter?
  end

  def filter_query_data
    @filter_query_data ||= FilterQueryData.new(vehicle_filter_params)
  end

  def vehicle_filter_params
    @vehicle_filter_params ||= params.slice(*VehicleFilter::FILTER_TYPES)
  end
end
