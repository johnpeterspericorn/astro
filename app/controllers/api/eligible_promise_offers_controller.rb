# encoding: utf-8
module Api
  class EligiblePromiseOffersController < EndUserApiController
    respond_to :json

    before_action :record_vehicle_offers, only: :index

    ##
    #
    # @resource /api/eligible_promise_offers
    # @action GET
    #
    # NOTE: This endpoint returns *all* purchased & unpurchased offers for the current user,
    # not just eligible ones. This is a contradiction in terms, on track to be
    # fixed in a later build of the iOS app that consumes this API.
    def index
      offers = PromiseOffer
               .not_past_cutoff
               .by_pawn_access_no(current_user.pawn_access_number)
               .map(&:decorate)
      respond_with(offers, each_serializer: PromiseOfferSerializer)
    end

    private

    def record_vehicle_offers
      vehicles = SoldVehicle.for_pawn_access_number(current_user)
      PromiseOfferRecorder.record_offers(vehicles)
    end
  end
end
