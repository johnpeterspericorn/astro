# encoding: utf-8
module Api
  class SoldVehiclesController < ApiController
    respond_to :json

    ##
    # Creates a new SoldVehicle
    #
    # @resource /api/sold_vehicles
    # @action POST
    #
    # @required [String] pawn_access_no
    # @required [String] lane_no
    # @required [String] make
    # @required [DateTime] purchased_at
    # @required [String] run_no
    # @required [String] sale_no
    # @required [Float] vehicle_purchase_price
    # @required [String] vnum
    # @optional [Float]additional_day
    # @optional [Integer] age_rating
    # @optional [Integer] autocheck_rating
    # @optional [Boolean] automatic_purchase
    # @required [String] automatic_purchase_email
    # @optional [Boolean] automatic_purchase_processed
    # @optional [String] badge_no
    # @optional [String] bid_risk_rating
    # @optional [Float] buy_fee
    # @optional [Boolean] bypass_charge
    # @optional [String] agreement_no
    # @optional [Float] promise_250
    # @optional [Float] promise_500
    # @optional [Boolean] promise_prepaid
    # @optional [Integer] promise_price_threshold
    # @optional [Boolean]if_bid
    # @optional [Integer] ineligibility_condition
    # @optional [Float] inspection_bundle_price
    # @optional [Float] inspection_only_price
    # @required [String] location_initials
    # @optional [Integer] mid_rating
    # @optional [Integer] mmr_rating
    # @optional [String] model
    # @optional [Integer] odometer_rating
    # @optional [Integer] odometer_reading
    # @optional [Boolean] ove
    # @optional [Boolean] past_time
    # @optional [Boolean] presale_inspection
    # @required [Integer] preselected_promise_days
    # @required [Integer] preselected_promise_miles
    # @optional [Boolean] require_approval
    # @required [String] sblu
    # @optional [String] seller_no
    # @optional [Boolean] seller_paid
    # @optional [Integer] seller_rating
    # @required [String] universal_no
    # @optional [Integer] vehicle_code
    # @required [String] work_order_number
    # @required [Integer] year
    #
    # @example_request
    #   ```json
    #   {
    #      "pawn_access_no": "100000000",
    #      "location_initials" : "AAA",
    #      "vnum" : "RRRRUT",
    #      "year" : "2018",
    #      "make" : "1212",
    #      "vehicle_purchase_price" : "1221",
    #      "lane_no" : "212",
    #      "run_no" : "212",
    #      "sale_no" : "35",
    #      "preselected_promise_days" : "7",
    #      "preselected_promise_miles" : "250",
    #      "automatic_purchase_email" : "asdsd@ssad.com",
    #      "sblu" : "1212",
    #      "universal_no" : "212",
    #      "work_order_number" : "212",
    #      "purchased_at" : "2018-08-29 12:42:50 +0530"
    #   }
    #   ```
    #
    # @example_response
    #   ```json
    #   {
    #     "id": 24,
    #     "created_at": "2018-08-31T08:33:06.632Z",
    #     "updated_at": "2018-08-31T08:33:06.632Z",
    #     "model": null,
    #     "odometer_reading": null,
    #     "year": 2018,
    #     "make": "1212",
    #     "vnum": "RRRRUT",
    #     "lane_no": "212",
    #     "run_no": "212",
    #     "universal_no": "212",
    #     "pawn_access_no": "100000000",
    #     "badge_no": null,
    #     "agreement_no": null,
    #     "promise_250": null,
    #     "additional_day": null,
    #     "promise_500": null,
    #     "vehicle_purchase_price": 1221,
    #     "odometer_rating": null,
    #     "autocheck_rating": null,
    #     "mmr_rating": null,
    #     "age_rating": null,
    #     "seller_rating": null,
    #     "mid_rating": null,
    #     "bid_risk_rating": null,
    #     "ineligibility_condition": null,
    #     "if_bid": null,
    #     "work_order_number": "212",
    #     "location_initials": "AAA",
    #     "ove": null,
    #     "presale_inspection": null,
    #     "buy_fee": null,
    #     "promise_prepaid": null,
    #     "inspection_only_price": null,
    #     "inspection_bundle_price": null,
    #     "automatic_purchase": null,
    #     "preselected_promise_days": 7,
    #     "preselected_promise_miles": 250,
    #     "automatic_purchase_email": "asdsd@ssad.com",
    #     "automatic_purchase_processed": false,
    #     "vehicle_code": null,
    #     "promise_price_threshold": null,
    #     "seller_paid": false,
    #     "sale_no": "35",
    #     "seller_no": null,
    #     "sblu": 1212,
    #     "bypass_charge": false,
    #     "purchased_at": "2018-08-29T07:12:50.000Z",
    #     "pending_reason_code": null,
    #     "transport_reimbursement": 0,
    #     "must_be_promised_at": null,
    #     "discount": 0,
    #     "discount_description": "",
    #     "preselected_promise_kind": "purchase_protect",
    #     "partner": null,
    #     "percent_coverage": null,
    #     "limited_volume": null,
    #     "group_code": null,
    #     "group": null,
    #     "source_document_link": null
    #   }
    #   ```
    def create
      @sold_vehicle = SoldVehicle.new(sold_vehicle_params)
      if @sold_vehicle.save
        render json: @sold_vehicle
      else
        head :unprocessable_entity
      end
    end

    private

    def sold_vehicle_params
      params
        .require(:sold_vehicle)
        .permit(*SoldVehicle.sanitized_attribute_names)
    end
  end
end
