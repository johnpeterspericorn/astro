# encoding: utf-8
class Api::PresaleVehiclesController < EndUserApiController
  ##
  # Get a listing of presale vehicles, filtered by given params
  #
  # @resource /api/presale_vehicles
  # @action GET
  #
  # @required [String] auth_token
  # @required [Hash] filter hash of filter options
  # @optional [String] filter.location_initials 4-letter initials of pawn location
  # @optional [String] filter.lane_no Lane Number (for use with location filter)
  # @optional [Boolean] filter.ove Set to `true` to filter by OVE vehicles only
  # @optional [String] filter.make
  # @optional [String] filter.model
  # @optional [String] filter.vnum_suffix Matches the end of the vehicle's vnum
  #
  # @example_request
  #   ```json
  #   {
  #     "auth_token": "ASZNfqL4kFFMezw6xTW5",
  #     "filter": {
  #       "location_initials": "SVAA",
  #       "lane_no": 5
  #     }
  #   }
  #   ```
  # @example_response
  #   ```json
  #   {
  #     "vehicles": [
  #       {
  #         "vnum": "236AB38404AFD0884",
  #         "year": 1975,
  #         "make": "FORD",
  #         "model": "PINTO",
  #         "run_no": "1",
  #         "odometer_reading": 10023,
  #         "location_initials": "AAA",
  #         "location_name": "Astroheim Chicago"
  #         "age_rating":       1,
  #         "autocheck_rating": 2,
  #         "mid_rating":       5,
  #         "mmr_rating":       5,
  #         "odometer_rating":  3,
  #         "seller_rating":    4,
  #         "ineligibility_condition": 1,
  #         "ineligibility_notice": "..."
  #       }
  #     ]
  #   }
  #   ```
  # @example_response_description Response is ordered by location_initials, lane_no, then run_no
  def index
    head(:bad_request) && return if no_filters?

    vehicles = PresaleVehicle.all
    vehicles = vehicles.where('vnum LIKE ?', "%#{vnum_suffix}") if vnum_suffix
    vehicles = vehicles.where(filter_params)
    vehicles = vehicles.pawn_ordered

    render json: vehicles, root: :vehicles
  end

  private

  def filter_params
    params
      .require(:filter)
      .permit(
        :location_initials,
        :lane_no,
        :ove,
        :make,
        :model
      )
  end

  def no_filters?
    params.fetch(:filter, {}).values.all?(&:blank?)
  end

  def vnum_suffix
    params.fetch(:filter) { Hash.new }.fetch(:vnum_suffix, nil)
  end
end
