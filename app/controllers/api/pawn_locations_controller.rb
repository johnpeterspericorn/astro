# encoding: utf-8
module Api
  class PawnLocationsController < EndUserApiController
    respond_to :json

    ##
    # List all purchase enabled pawn locations
    #
    # @resource /api/pawn_locations
    # @action GET
    #
    # @example_request
    #   curl https://promise.agreementshield.com/api/pawn_locations.json?auth_token=XXX
    #
    # @example_response
    #   ```json
    #   [
    #     {
    #       "id": 1,
    #       "name": "Astroheim Chicago",
    #       "initials": "AAA",
    #       "presale_lane_numbers": [1,2,3,4]
    #     }
    #   ]
    #   ```
    def index
      respond_with PawnLocation.with_purchases_enabled.order(:name)
    end
  end
end
