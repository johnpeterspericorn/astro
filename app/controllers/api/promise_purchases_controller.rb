# encoding: utf-8
module Api
  class PromisePurchasesController < ApiController
    respond_to :json, :xml

    ##
    # Purchase a promise offer
    #
    # @resource /api/promise_purchase
    # @action POST
    #
    # @required [String] buyer_agreement_number The Five Million number of the buyer
    # @required [String] vnum
    # @required [Integer] miles One of 250 or 500
    # @required [Integer] days One of 7, 14 or 21
    # @required [String] email_address email address to which purchase confirmation should be sent
    # @required [Integer] agreementshield_quoted_price Expected price for promise. It is an error for this value to not match the price that would be charged.
    # @optional [Boolean] silent Defaults false. When true, purchase will be made, but no charge or notification is triggered as a result of the purchase
    #
    # @response_field [Integer] return_code 1 - Promise Offer is pending, but will allow successful purchase
    # @response_field [Integer] return_code 4 - Invalid options provided for miles or days
    # @response_field [Integer] return_code 5 - Price proviced does not match agreementshield_quoted_price
    # @response_field [Integer] return_code 8 - Vehicle has left the lot
    # @response_field [Integer] return_code 9 - Promise Offer is past cutoff
    # @response_field [Integer] return_code 10 - Vehicle is ineligible
    # @response_field [Integer] return_code 11 - Promise Offer is already purchased
    # @response_field [Integer] return_code 12 - Vehicle does not exist
    # @response_field [Integer] return_code 13 - Vehicle does exist, but with a different agreement number
    #
    # @example_request
    #   ```json
    #     {
    #       "buyer_agreement_number": "5000000",
    #       "vnum": "VNUM12345",
    #       "miles": 500,
    #       "days": 14,
    #       "email_address": "adama@caprica.gov",
    #       "agreementshield_quoted_price": "100"
    #     }
    #   ```
    #
    # @example_response
    #   ```json
    #     {
    #       "transaction_id": "ABC123",
    #       "created_at": "2018-01-01 00:00",
    #       "promise_offer_id": 123,
    #       "return_code": 0
    #     }
    #   ```
    def create
      head(:service_unavailable) && return unless Settings.purchase_api_enabled

      api_purchaser = ApiPurchaser.new(promise_purchase_params)

      api_purchaser.purchase_vehicle

      @purchase_response = api_purchaser.purchase_response
      PurchasesLogger.info("Params: #{params}; Response: #{@purchase_response}")

      if notified_error_types.include? return_code
        PromisePurchaseMailer.notify_api_error(promise_purchase_params, return_code).deliver_later
      end

      respond_with @purchase_response, status: api_purchaser.status, location: nil
    rescue ArgumentError # catch errors in the AutomatedPurchaser
      PurchasesLogger.error("Params: #{params}; Response: bad request")
      render json: { return_code: 3 }, status: :bad_request
    end

    private

    def promise_purchase_params
      params.slice(:email_address, :buyer_agreement_number, :vnum, :days, :miles, :silent, :agreementshield_quoted_price)
    end

    def notified_error_types
      [12, 13]
    end

    def return_code
      @purchase_response.fetch(:return_code)
    end
  end
end
