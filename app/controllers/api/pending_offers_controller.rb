# encoding: utf-8
module Api
  class PendingOffersController < ApiController
    respond_to :json

    ##
    # Accepts a pending promise offer
    #
    # @resource /api/pending_offers
    # @action PUT
    #
    # @required [Integer] promise_offer_id The id of the offer to approve
    #
    # @example_request
    #   curl http://promise.agreementshield.com/api/pending_offers.json?auth_token=XXX
    #
    # @example_response
    #   200 OK - empty body
    def update
      promise_offer = PromiseOffer.find(params[:promise_offer_id])
      promise_offer.finalize_pending(current_user)

      head :ok
    end
  end
end
