# encoding: utf-8
module Api
  class PromisePricesController < ApiController
    respond_to :json, :xml

    ##
    # Get pricing data for a vehicle
    #
    # @resource /api/promise_price
    # @action GET
    #
    # @required [String] pawn_access_number
    # @required [String] vnum
    #
    # @response_field [Float] promise_250
    # @response_field [Float] promise_500
    # @response_field [Float] additional_day
    # @response_field [Float] inspection_only_price
    # @response_field [Float] inspection_bundle_price
    def show
      if Settings.pricing_api_enabled
        head(:not_found) && return if offer.blank? || offer.pawn_access_no != params[:pawn_access_number]
        pricing_information = PriceFetcher.new(offer).pricing_information

        respond_with pricing_information
      else
        head :service_unavailable
      end
    end

    private

    def offer
      vnum = params[:vnum]

      offer = PromiseOffer.find_by_vnum(vnum)

      if offer.present?
        offer
      else
        vehicle = SoldVehicle.find_by_vnum(vnum)
        PromiseOfferRecorder.record_offers([vehicle]).first if vehicle.present?
      end
    end
  end
end
