# encoding: utf-8
module Api
  class CancellationsController < ApiController
    ##
    # Creates a promise purchase cancellation
    #
    # @resource /api/cancellations
    # @action POST
    #
    # @optional [String] universal_no the "universal_no" of the promised vehicle
    # @optional [String] purchase_id the primary key of the promise purchase (universal_no or purchase_id is required to fetch promise_offer)
    # @required [String] reason an indication of reason for cancellation
    # @optional [Boolean] ineligible_for_repurchase flags the vehicle's VNUM as ineligible for promise purchase
    # @optional [Integer] ineligibility_condition the identify corresponding to an purchase ineligibility condition
    # @optional [Boolean] silent defaults false.When true, cancellation confirmation mailer will not be sent.
    # @optional [Boolean] bypass_credit Defaults false. When true, cancellation will be made, but no charge is triggered as a result of the cancellation
    #
    # @example_request
    #   ```json
    #   {
    #     "universal_no: "EXAMPLEUNIVERSAL"
    #     "reason": "IF bid"
    #   }
    #   ```
    #
    # @example_response {}
    #
    # @example_response_description 200 response code for success; 422 response code for validation errors
    #
    def create
      head(:unprocessable_entity) && return if unprocessable_entity?
      head(:not_found) && return unless promise_offer.present? && promise_purchase.present?
      if cancellation.process
        render json: {}
      else
        render json: { errors: cancellation.errors.to_a }, status: :unprocessable_entity
      end
    end

    private

    delegate :promise_purchase, to: :promise_offer, allow_nil: true

    def promise_offer
      if params[:universal_no]
        @promise_offer ||= PromiseOffer.find_by_universal_no(params[:universal_no])
      elsif params[:purchase_id]
        @promise_offer ||= promise_offer_from_purchase_id
      end
    end

    def promise_offer_from_purchase_id
      PromisePurchase.find_by_id(params[:purchase_id]).try(:promise_offer)
    end

    def return_invoice
      promise_purchase.try(:return_invoice)
    end

    def unprocessable_entity?
      params[:universal_no].blank? && params[:purchase_id].blank? || return_invoice.present?
    end

    def cancellation_params
      {
        promise_purchase_id:     promise_purchase.id,
        ineligibility_condition:   params[:ineligibility_condition],
        ineligible_for_repurchase: params[:ineligible_for_repurchase],
        reason:                    params[:reason],
        user:                      current_user,
        skip_applying_credit:      params[:bypass_credit],
        silent:                    params[:silent]
      }
    end

    def cancellation
      @cancellation ||= Cancellation.new(cancellation_params)
    end
  end
end
