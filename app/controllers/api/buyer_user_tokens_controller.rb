# encoding: utf-8
module Api
  class BuyerUserTokensController < ApplicationController
    skip_before_action :authenticate_user!

    ##
    # Resets a user's auth token and responds with new token
    #
    # @resource /api/buyer_user_token
    # @action POST
    #
    # @required [String] buyer_user[username]
    # @required [String] buyer_user[password]
    #
    # @example_request
    #   ```json
    #   {
    #     "buyer_user": {
    #       "username": "cooley",
    #       "password": "s3kr3t"
    #     }
    #   }
    #   ```
    #
    # @example_response
    #   ```json
    #   {
    #     success: true,
    #     auth_token: '2fTtrzrgSwquQNVUoNBn'
    #   }
    #   ```
    def create
      token_response = TokenResponse.new(buyer_user_params)

      if token_response.user_authenticated?
        render json: token_response.to_json
      else
        head :not_found
      end
    end

    private

    def buyer_user_params
      params.require(:buyer_user).permit(:username, :password)
    end
  end
end

class TokenResponse < Struct.new(:params)
  def to_json
    if valid_for_api?
      buyer_user.assign_attributes(buyer_user: true) if buyer_user.new_record?
      buyer_user.reset_authentication_token!
      { success: true, auth_token: buyer_user.authentication_token }
    else
      { success: false }
    end
  end

  def valid_for_api?
    buyer_user.pawn_access_number.present?
  end

  def user_authenticated?
    buyer_user.present?
  end

  private

  def buyer_user
    @buyer_user ||= User.authenticate_with_astroheim_auth(params) if params.present?
  end
end
