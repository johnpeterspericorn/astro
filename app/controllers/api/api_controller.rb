# encoding: utf-8
module Api
  class ApiController < ApplicationController
    before_action :authorize_api_user!

    def authorize_api_user!
      authorize(current_user.api_user?)
    end
  end
end
