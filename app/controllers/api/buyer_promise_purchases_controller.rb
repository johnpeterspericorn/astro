# encoding: utf-8
module Api
  class BuyerPromisePurchasesController < EndUserApiController
    respond_to :json

    ##
    # Purchase a promise offer
    #
    # Errors:
    #   * _422_: The given parameters are not valid to complete purchase (e.g. invalid email provided)
    #   * _404_: The vehicle with the given VNUM does not exist in our system, or if
    #     the vehicle exists but doesn't belong to the logged-in user.
    #   * _400_: A required parameter is missing.
    #
    # @resource /api/buyer_promise_purchase.json
    # @action POST
    #
    #
    # @required [Array]   offers An array of offers to be purchased
    # @required [Integer] offers.id The ID provided by the promise estimates WS for the promise to be purchased
    # @required [Integer] offers.miles One of 250 or 500
    # @required [Integer] offers.additional_days Additional days on the promise. One of 0, 7 or 14
    # @optional [String] email_address Email to be set on offer for confirmation and future correspondence
    #
    # @example_request
    #   ```json
    #   {
    #     "offers": [
    #       {
    #         "id": 3
    #         "miles": 500,
    #         "additional_days": 14
    #       }
    #     ]
    #   }
    #   ```
    #
    # @example_response
    #   ```json
    #     {
    #       "purchases": [
    #         {
    #           "id": 3
    #           "success": true,
    #           "expired": false,
    #           "ineligibility_notice": "vehicle is past cutoff"
    #         }
    #       ]
    #     }
    #   ```
    def create
      unprocessable_entity && return unless valid_email?
      not_found && return if missing_offers?
      bad_request && return if missing_fields?

      response = offer_params.map do |offer, miles, additional_days, ineligibility_notice|
        if offer.past_cutoff?
          success = false
          expired = true
          { id: offer.id, success: false, expired: true, ineligibility_notice: ineligibility_notice }
        else
          PromisePurchaser.new(
            offer: offer,
            email: email_address || offer.emails,
            additional_days: additional_days,
            miles: miles,
            channel: PromiseChannel::API_BUYER_PURCHASES
          ).record_purchase

          success = true
          expired = false
        end

        { id: offer.id, success: success, expired: expired, ineligibility_notice: ineligibility_notice }
      end

      log_purchase_attempt(response)
      render json: { purchases: response }
    rescue Promise::Invalid
      unprocessable_entity
    end

    private

    def email_address
      params[:email_address]
    end

    def offer_params
      params.require(:offers).map do |offer_params|
        offer = PromiseOffer
                .by_pawn_access_no(current_user.pawn_access_number)
                .where(id: offer_params[:id])
                .first

        miles = offer_params[:miles].try(:to_i)
        additional_days = offer_params[:additional_days].try(:to_i)
        ineligibility_notice = offer.ineligibility_notice if offer.present?

        [offer, miles, additional_days, ineligibility_notice]
      end
    end

    def unprocessable_entity
      log_purchase_attempt('unprocessable_entity')
      head :unprocessable_entity
    end

    def not_found
      log_purchase_attempt('not_found')
      head :not_found
    end

    def bad_request
      log_purchase_attempt('bad_request')
      head :bad_request
    end

    def valid_email?
      EmailValidation.new(email_address).valid?
    end

    def missing_offers?
      offer_params.any? { |offer, _, _| offer.blank? }
    end

    def missing_fields?
      offer_params.any? { |_, miles, additional_days| miles.blank? || additional_days.blank? }
    end

    def log_purchase_attempt(response)
      PurchasesLogger.info "Submitting for purchase: #{params}, response: #{response}"
    end
  end
end
