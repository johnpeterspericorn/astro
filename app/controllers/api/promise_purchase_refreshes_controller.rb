# encoding: utf-8
module Api
  class PromisePurchaseRefreshesController < ApiController
    ##
    # Refresh the promised_at timestamp on a promise purchase
    #
    # @resource /api/promise_purchase_refresh.json
    # @action PUT
    #
    # @required [String] universal_no
    # @optional [Boolean] silent Defaults to `false`. If true, sends a confirmation of the purchase refresh to the user.
    #
    # @example_request
    #   ```json
    #     {
    #       "universal_no": "100686866",
    #       "silent": false
    #     }
    #   ```
    #
    # @example_response
    #   ```json
    #     {}
    #   ```

    respond_to :json

    def update
      head(:not_found) && return unless promise_purchase.present?

      promise_purchase.touch(:promised_at)

      if send_confirmation?
        PromisePurchaseRefreshMailer.refresh_confirmation(promise_offer).deliver_later
      end

      render json: {}
    end

    private

    def promise_offer
      PromiseOffer.find_by_universal_no(params[:universal_no])
    end

    def promise_purchase
      promise_offer.try(:promise_purchase)
    end

    def send_confirmation?
      !params[:silent]
    end
  end
end
