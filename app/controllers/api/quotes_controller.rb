# encoding: utf-8
module Api
  class QuotesController < ApplicationController
    before_action :ensure_vehicle

    ##
    # Get a promise quote
    #
    # @resource /api/quote
    # @action GET
    #
    # @required [String] auth_token
    # @required [String] vnum
    # @required [Integer] buyer_number
    # @optional [Integer] price
    #
    # @example_request
    #   ```json
    #   {
    #     "auth_token": "ABC123",
    #     "presale_vehicle_id": 1,
    #     "buyer_number",  5000001
    #   }
    #   ```
    #
    # @example_response_description A quote for a flat rate will only responds with a single miles/days price
    # @example_response
    #   ```json
    #   {
    #     "low_valuation":     1000,
    #     "average_valuation": 1500,
    #     "high_valuation":    2000,
    #     "quote_price":       1234,
    #     "flat_rate":         false,
    #     "ineligiblity_notice": "Promise pricing unavailable.  Contact AgreementShield Customer Service for details."
    #     "quotes": {
    #       "250": {
    #         "7":  100,
    #         "14": 150,
    #         "21": 200
    #       },
    #       "500": {
    #         "14": 250,
    #         "7":  200,
    #         "21": 300
    #       },
    #       "transportation_reimbursement": 75
    #     }
    #   }
    #   ```
    def show
      @quote = Quote.new(presale_vehicle, buyer_number, quote_price)
      policy = QuotePolicy.new(agreement_number_container, @quote)
      authorize(policy.show?)

      respond_to do |format|
        format.json { render json: @quote, root: false }

        format.html do
          @quote = QuoteDecorator.new(@quote)
          render partial: 'pricing_matrix'
        end
      end
    end

    private

    def presale_vehicle_id
      params[:presale_vehicle_id]
    end

    def vnum
      params[:vnum]
    end

    def buyer_number
      params.require(:buyer_number)
    end

    def quote_price
      params.fetch(:price) { presale_vehicle.average_valuation }
    end

    def presale_vehicle
      @presale_vehicle ||= if presale_vehicle_id.present?
                             PresaleVehicle.find(presale_vehicle_id)
                           else
                             PresaleVehicle.find_by_vnum(vnum)
                           end
    end

    def distances
      i18n_distances.values - [360]
    end
    helper_method :distances

    def distance_units
      I18n.translate("distance_units.#{country_code}.unit")
    end
    helper_method :distance_units

    def i18n_distances
      I18n.translate("offer_distances.#{country_code}")
    end

    def country_code
      presale_vehicle.country_code
    end

    def ensure_vehicle
      head :not_found unless presale_vehicle
    end

    def pawn_access_number
      session[:pawn_access_number] || current_user.pawn_access_number
    end

    def buyer_authorization
      @buyer_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number)
    end

    def agreement_number_container
      OpenStruct.new(agreement_numbers: buyer_authorization.get_agreement_numbers)
    end
  end
end
