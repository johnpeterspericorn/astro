# encoding: utf-8
module Api
  class UserAgreementshipsController < EndUserApiController
    respond_to :json

    ##
    # Get a listing of a user's associated agreementships
    #
    # @resource /api/user_agreementships
    # @action GET
    #
    # @required [String] auth_token
    #
    # @example_request
    #   ```json
    #   {
    #     "auth_token": "ASZNfqL4kFFMezw6xTW5"
    #   }
    #   ```
    #
    # @example_response
    #   ```json
    #   {
    #     "agreementships": [
    #       {"agreementNumber": "5277869", "agreementName": "CANDY LAND AUTO SALES (TEST ACCOUNT)"},
    #       {"agreementNumber": "5189755", "agreementName": "PANIAGUA AUTO SALES INC"}
    #     ]
    #   }
    #   ```
    def index
      render json: { agreementships:  agreementships }
    end

    private

    def agreementships
      current_user.agreement_numbers_to_names.sort_by(&:first).map do |agreement_no, agreement_name|
        { agreementNumber: agreement_no, agreementName: agreement_name }
      end
    end
  end
end
