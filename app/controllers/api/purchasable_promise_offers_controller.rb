# encoding: utf-8
module Api
  class PurchasablePromiseOffersController < EndUserApiController
    respond_to :json

    def index
      offers = PromiseOffer.unpurchased.by_pawn_access_no(current_user.pawn_access_number)
      offers = offers.by_vnum(vnum_filter) if vnum_filter

      @offers = offers.map(&:decorate)

      respond_with(@offers, each_serializer: PromiseOfferSerializer)
    end

    private

    def filters
      params.fetch(:filter) { Hash.new }
    end

    def vnum_filter
      filters.fetch(:vnum, nil)
    end
  end
end
