# encoding: utf-8
class Api::NewProductFeesController < Api::ApiController
  respond_to :json

  ##
  # List all new product fees
  #
  # @resource /api/new_product_fees
  # @action GET
  #
  # @example_request
  #   curl https://promise.agreementshield.com/api/new_product_fees?auth_token=XXX
  #
  # @example_response
  #   ```json
  #   [
  #     {
  #       "id": 3,
  #       "agreement_no": "asaSdddd1",
  #       "seller_no": "sA",
  #       "seller_paid": true,
  #       "timeframe": 365,
  #       "count": 999999,
  #       "fee": 0,
  #       "updated_at": "2018-03-23T06:31:07.377Z"
  #     }
  #   ]
  #   ```

  def index
    new_product_fees = NewProductFee.all
    render status: :ok, json: { new_product_fees: new_product_fees }
  end

  ##
  # Get new product fee details
  #
  # @resource /api/new_product_fees/:id
  # @action GET
  #
  # @example_request
  #   curl https://promise.agreementshield.com/api/new_product_fees/1?auth_token=XXX
  #
  # @example_response
  #   ```json
  #   {
  #     "new_product_fee":
  #     {
  #       "id": 1,
  #       "agreement_no": "sad",
  #       "seller_no": "sdsad",
  #       "seller_paid": false,
  #       "timeframe": 365,
  #       "count": 999999,
  #       "fee": 0,
  #       updated_at": "2018-03-21T12:29:26.217Z"
  #     }
  #   }
  #   ```

  def show
    new_product_fee = NewProductFee.find_by_id(params[:id])
    render status: :ok, json: { new_product_fee: new_product_fee }
  end

  ##
  # Create new product fee
  #
  #
  # @resource /api/new_product_fees
  # @action POST
  #
  #
  # @required [String] new_product_fee.agreement_no
  # @required [String] new_product_fee.seller_no
  # @required [Boolean] new_product_fee.seller_paid
  # @required [integer] new_product_fee.timeframe
  # @required [integer] new_product_fee.count
  # @required [Float] new_product_fee.fee
  #
  # @example_request
  #   ```json
  #   {
  #     "new_product_fee":
  #      {
  #        "agreement_no": 3
  #        "seller_no": 500,
  #        "seller_paid": true,
  #        "timeframe": 14,
  #        "count": 4,
  #        "fee": 200
  #      }
  #   }
  #   ```
  #
  # @example_response
  #   ```json
  #     {
  #      "new_product_fee": {
  #        "id": 6,
  #        "agreement_no": "3",
  #        "seller_no": "500",
  #        "seller_paid": true,
  #        "timeframe": 14,
  #        "count": 4,
  #        "fee": 200,
  #        "updated_at": "2018-03-28T05:32:48.635Z"
  #      },
  #      "message": "Successfully created the New Product Fee"
  #    }
  #   ```

  def create
    new_product_fee = NewProductFee.new(new_product_fee_params)
    if new_product_fee.save
      render status: :ok, json: { new_product_fee: new_product_fee, message: 'Successfully created the New Product Fee' }
    else
      render status: :bad_request, json: { message: new_product_fee.errors.full_messages }
    end
  end

  ##
  # Update new product fee
  #
  #
  # @resource /api/new_product_fees/:id
  # @action PUT
  #
  #
  # @required [String] new_product_fee.agreement_no
  # @required [String] new_product_fee.seller_no
  # @required [Boolean] new_product_fee.seller_paid
  # @required [integer] new_product_fee.timeframe
  # @required [integer] new_product_fee.count
  # @required [Float] new_product_fee.fee
  #
  # @example_request
  #   ```json
  #   {
  #     "new_product_fee":
  #      {
  #        "agreement_no": 3
  #        "seller_no": 500,
  #        "seller_paid": true,
  #        "timeframe": 14,
  #        "count": 4,
  #        "fee": 200
  #      }
  #   }
  #   ```
  #
  # @example_response
  #   ```json
  #     {
  #      "new_product_fee": {
  #        "id": 6,
  #        "agreement_no": "3",
  #        "seller_no": "500",
  #        "seller_paid": true,
  #        "timeframe": 14,
  #        "count": 4,
  #        "fee": 200,
  #        "updated_at": "2018-03-28T05:32:48.635Z"
  #      },
  #      "message": "Successfully updated the New Product Fee"
  #    }
  #   ```

  def update
    new_product_fee = NewProductFee.find_by_id(params[:id])
    if new_product_fee.update_attributes(new_product_fee_params)
      render status: :ok, json: { new_product_fee: new_product_fee, message: 'Successfully updated the New Product Fee' }
    else
      render status: :bad_request, json: { message: new_product_fee.errors.full_messages }
    end
  end

  ##
  # Delete new product fee
  #
  #
  # @resource /api/new_product_fees/:id
  # @action DELETE
  #
  #
  #
  # @example_response
  #   ```json
  #    {
  #      "message": "Successfully Deleted the New Product Fee"
  #    }
  #   ```

  def destroy
    new_product_fee = NewProductFee.find_by_id(params[:id])
    if new_product_fee.nil?
      render status: :not_found, json: { message: 'No Found' }
    else
      new_product_fee.destroy
      render status: :ok, json: { message: 'Successfully Deleted the New Product Fee' }
    end
  end

  private

  def new_product_fee_params
    params
      .require(:new_product_fee)
      .permit(
        :agreement_no,
        :seller_no,
        :seller_paid,
        :timeframe,
        :count,
        :fee
      )
  end
end
