# encoding: utf-8
module Api
  class PromiseExtensionsController < ApiController
    respond_to :json

    ##
    # Creates new Promise Extensions
    #
    # @resource /api/promise_extensions
    # @action POST
    #
    # @required [String] vnum
    # @required [Integer] additional_days
    # @required [String] reason
    #
    # @example_request
    #  ```json
    #  {
    #    "promise_extension": {
    #      "vnum": "35BFBBF81AA1AEA24",
    #      "additional_days": "12",
    #      "reason": "Ready Auto"
    #    }
    #  }
    #  ```
    #
    # @example_response
    #  ```json
    #  {
    #    "id": 23,
    #    "created_at": "2018-08-31T08:33:06.632Z",
    #    "updated_at": "2018-08-31T08:33:06.632Z",
    #    "vnum": "35BFBBF81AA1AEA24",
    #    "additional_days" : "12",
    #    "reason" : "Ready Auto"
    #  }
    #  ```
    ##
    def create
      existing = PromiseExtension.find_by(vnum: promise_extension_params[:vnum])

      process_status = if existing
                         @promise_extension = existing
                         @promise_extension.update(promise_extension_params)
                       else
                         @promise_extension = PromiseExtension.new(promise_extension_params)
                         @promise_extension.save
                       end
      if process_status
        Delayed::Job.enqueue AddExtensionDaysJob.new
        render json: @promise_extension
      else
        head :unprocessable_entity
      end
    end

    private

    def promise_extension_params
      params
        .require(:promise_extension)
        .permit(:vnum, :additional_days, :reason)
        .merge(status: 'new')
    end
  end
end
