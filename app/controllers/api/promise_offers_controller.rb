# encoding: utf-8
module Api
  class PromiseOffersController < ApiController
    ##
    # Update a purchased promise offer
    #
    # @resource /api/promise_purchases/:promise_purchase_id/promise_offer
    # @action PUT
    #
    # @required [String] vnum
    # @optional [String] agreement_no
    # @optional [String] pawn_access_no
    # @optional [String] seller_no
    # @optional [Float] vehicle_purchase_price
    # @optional [Float] buy_fee
    # @optional [Fixnum] days_selected
    # @optional [Fixnum] miles_selected
    #
    # @example_request
    #   ```json
    #     {
    #       "vnum": "VNUM12345",
    #       "agreement_no": 5004123,
    #       "seller_no": 5001234,
    #       "vehicle_purchase_price": 10013.0,
    #       "buy_fee": 150.50,
    #       "days_selected": 250,
    #       "miles_selected": 250
    #     }
    #   ```
    #
    # @example_response
    #
    # 200 response code for success; 422 response code for validation errors
    #

    def update
      if promise_offer.update_attributes(offer_params)
        head :ok
      else
        head :unprocessable_entity
      end
    end

    private

    def promise_purchase
      @promise_purchase ||= PromisePurchase.find(params[:promise_purchase_id])
    end

    def promise_offer
      promise_purchase.promise_offer
    end

    def pawn_information
      promise_offer.pawn_information
    end

    def pawn_information_params
      params
        .permit(:agreement_no, :pawn_access_no, :seller_no, :vehicle_purchase_price, :buy_fee)
        .merge(id: pawn_information.id)
    end

    def offer_params
      params
        .permit(:days_selected, :miles_selected)
        .merge(pawn_information_attributes: pawn_information_params)
    end
  end
end
