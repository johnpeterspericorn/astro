# encoding: utf-8
module Api
  class ShipmentEventsController < ApiController
    def create
      if shipment_event.tracker_update?
        shipment = Shipment.find_by_easypost_shipment_id(shipment_event.shipment_id)
        shipment.update_attributes!(status: shipment_event.status)

        if Settings.tracking_update_notification_enabled
          ShipmentsMailer.delay.tracking_update(shipment)
        end
      end

      head :ok
    rescue KeyError
      head :unprocessable_entity
    end

    private

    def request_body
      @body ||= JSON.parse(request.body.read).with_indifferent_access
    end

    def description
      request_body.fetch(:description)
    end

    def result
      request_body.fetch(:result)
    end

    def shipment_event
      @shipment_event ||= ShipmentEvent.new(description, result)
    end
  end
end
