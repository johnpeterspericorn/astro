# encoding: utf-8
module Api
  class PromiseEligibilitiesController < ApiController
    ##
    # Show promise purchase eligibility information for presale vehicles
    #
    # @resource /api/promise_eligibility
    # @action GET
    #
    # @required [Hash] vehicles
    # @required [String] vehicles.vnum The VNUM of the vehicle
    # @required [String] vehicles.seller_no The Five Million number of the seller
    #
    # @response_field [Bool] promise_eligible Flag indicating that the car is eligible or ineligible for a promise
    # @response_field [String] ineligibility_reason Message field to explain why the car is ineligible for a promise
    #
    # @example_request
    #   ```json
    #     {
    #       "vehicles": [
    #         { "vnum": "VNUM12345", "seller_no": "5000000" }
    #       ]
    #     }
    #   ```
    #
    # @example_response
    #   ```json
    #     [
    #       {
    #         "vnum": "VNUM12345",
    #         "promise_eligible": "false",
    #         "ineligibility_reason": "We are currently unable to process this vehicle until the if bid is finalized. Please check back once it has been keyed in Astroheim's system. If you need assistance, please contact us at (855) 246-3232."
    #       }
    #     ]
    #   ```
    def show
      vnums = vehicles_params.map { |v| v['vnum'] }
      promise_offers = PresaleVehicle.where(vnum: vnums).map do |vehicle|
        seller_no = vehicles_params.detect { |v| v['vnum'] == vehicle.vnum }['seller_no']
        VehicleEligibility.new(vehicle, seller_no)
      end

      head(:not_found) && return if promise_offers.empty?

      render json: promise_offers, each_serializer: PromiseEligibilitySerializer
    end

    alias create show

    private

    def vehicles_params
      Array(params.require(:vehicles))
    end
  end
end
