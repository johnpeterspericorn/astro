# encoding: utf-8
module Api
  class ReturnInvoicesController < ApiController
    respond_to :json

    ##
    # Update a return invoice
    #
    # @resource /api/return_invoices/{:id}
    # @action PUT
    #
    # @required [Boolean] vehicle_received
    # @required [Boolean] title_received
    # @required [Boolean] refund_processed
    # @optional [Integer] pawn_location_id
    #
    # @example_request
    #   ```json
    #     {
    #       "vehicle_received": "1",
    #       "title_received": "1",
    #       "refund_processed": "0",
    #       "pawn_location_id": "79"
    #     }
    #   ```
    #
    # @example_response
    #
    # 200 response code for success; 422 response code for validation errors
    #
    def update
      if return_invoice.update_attributes(return_params)
        head :ok
      else
        head :unprocessable_entity
      end
    end

    private

    def return_invoice
      @return_invoice ||= ReturnInvoice.find(params[:id])
    end

    def return_params
      params.permit(:vehicle_received, :title_received, :refund_processed, :pawn_location_id)
    end
  end
end
