# encoding: utf-8
class AgreementsController < ApplicationController
  respond_to :json, :xml

  ##
  # Provide an array of agreements based on the pawn_access_number
  #
  # @resource /agreements
  # @action GET
  #
  # @required [String] pawn_access_number The 100 Million number
  # @example_request
  #   ```json
  #     {
  #       "id":36,
  #       "agreement_no":"5082345",
  #       "agreementship_name":"SABINAS CARS AND TRUCKS INC",
  #       "address_street":"5711 W MONTGOMERY RD",
  #       "address_suite":"",
  #       "address_city":"HOUSTON",
  #       "address_state":"TX",
  #       "address_zipcode":"770914229",
  #       "created_at":"2018-10-02T18:33:48.843-04:00",
  #       "updated_at":"2018-10-02T18:33:48.843-04:00",
  #       "phone":null
  #      }

  def index
    buyer_authorization = Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: params[:pawn_access_number])
    agreements = buyer_authorization.get_agreement_numbers_to_names.map do |agreement_no, agreementship_name|
      { agreement_no: agreement_no, agreementship_name: agreementship_name }
    end
    respond_with agreements
  end
end
