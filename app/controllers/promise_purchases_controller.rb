# encoding: utf-8
require 'column_conversions'

class PromisePurchasesController < ApplicationController
  include ColumnConversions

  def new
    @promise_purchases  = promise_purchases
    @inspection_purchases = inspection_purchases
    @bundled_purchases    = bundled_purchases
    @remembered_email     = buyer_email || user_emails

    return unless has_no_selected_products?
    flash[:alert] = t('promise_purchase.no_promises_selected_copy')
    redirect_to(promise_offers_path(pawn_access_number: pawn_access_number)) && return
  end

  def create
    @promise_purchases  = promise_purchases
    @inspection_purchases = inspection_purchases
    @bundled_purchases    = bundled_purchases
    @remembered_email     = buyer_email || user_emails

    email_validation = EmailValidation.new(arrayify(user_emails), required: true)

    if email_validation.valid?
      buyer_information.update_attributes(buyer_information_params)
    else
      flash.now[:alert] = email_validation.errors.values.flatten.join
      render(:new) && return
    end

    coupon_verifier = CouponVerifier.new(params[:vehicle])

    unless coupon_verifier.verified?
      flash.now[:alert] = coupon_verifier.errors.join
      render(:new) && return
    end

    ActiveRecord::Base.transaction do
      purchase_composer.complete_purchases
    end
    redirect_to(transaction_path(id: purchase_composer.transaction_id)) && return
  end

  private

  def promise_purchases
    @sorted_promise_purchases ||= purchase_composer.promise_purchases.map(&:decorate)
  end

  def inspection_purchases
    @inspection_purchases ||= purchase_composer.inspection_purchases.map(&:decorate)
  end

  def bundled_purchases
    @bundled_purchases ||= purchase_composer.bundled_purchases
  end

  def has_no_selected_products?
    promise_purchases.empty? && inspection_purchases.empty? && bundled_purchases.empty?
  end

  def purchase_composer
    vehicle_params = params[:vehicle] || {}
    purchase_queries = vehicle_params.map { |vnum, options| PurchaseQuery.new(vnum, options) }

    composer_attributes = {
      email: params[:emails],
      purchase_queries:      purchase_queries,
      pawn_access_number: pawn_access_number,
      purchasing_user:       current_user
    }
    @purchase_composer ||= PromisePurchaseComposer.new(composer_attributes)
  end

  def buyer_authorization
    @buyer_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(username: current_user.username)
  end

  def pawn_access_number
    session[:pawn_access_number] || buyer_authorization.get_pawn_access_number
  end

  def user_emails
    params[:emails] || agreement_email
  end
  helper_method :user_emails

  def agreement_email
    buyer_authorization.get_agreement_email
  end

  def buyer_information
    @buyer_information ||= BuyerInformation.build_with_defaults(pawn_access_number)
  end

  def buyer_email
    buyer_information.emails
  end

  def buyer_information_params
    params
      .permit(:send_updates, :emails)
  end
end
