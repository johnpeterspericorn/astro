# encoding: utf-8
class GraphqlsController < ActionController::Metal
  def create
    query = params[:query]
    variables = params[:variables] || {}
    context = {
      current_user: ->() { find_user_with_jwt(request.headers['HTTP_AUTHORIZATION']) }
    }
    result = AstroSchema.execute(query, variables: variables, context: context)
    set_cors_headers
    self.response_body = result.to_json
  end

  def options
    set_cors_headers
    self.response_body = ''
  end

  private

  def find_user_with_jwt(token)
    return if token.nil?
    access_token = AccessToken.new token
    access_token.user
  end

  def set_cors_headers
    self.headers = {
      'Access-Control-Allow-Credentials' => 'true',
      'Access-Control-Allow-Headers' => 'Content-Type, Accept, Authorization, Cache-Control, If-None-Match, If-Modified-Since, Origin',
      'Access-Control-Allow-Methods' => 'POST, OPTIONS',
      'Access-Control-Allow-Origin' => '*',
      'Access-Control-Expose-Headers' => 'Cache-Control, Content-Language, Content-Type, Expires, Last-Modified, Pragma',
      'Access-Control-Max-Age' => '1728000'
    }
  end
end
