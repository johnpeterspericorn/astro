# encoding: utf-8
class ReturnFunnel::BillOfSalesController < ApplicationController
  def new
    @bill_of_sale = BillOfSale.new
    @bill_of_sale.promise_purchase = promise_purchase
    @bill_of_sale = @bill_of_sale.decorate
  end

  def create
    bill_of_sale_params[:promise_purchase_id] = promise_purchase_id
    @bill_of_sale = BillOfSale.new(bill_of_sale_params)
    options = { promise_purchase_id: promise_purchase_id, return_invoice: session[:return_invoice], bill_of_sale_processed: true }
    if @bill_of_sale.valid?
      remember_bill_of_sale
      redirect_to new_return_funnel_return_invoice_path(options)
    else
      flash[:error] = t('return_funnel.bill_of_sale.error')
      redirect_to :back
    end
  end

  private

  def promise_purchase_id
    session[:promise_purchase_id] ||= params[:promise_purchase_id]
  end

  def remember_bill_of_sale
    session[:bill_of_sale] = bill_of_sale_params
  end

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find(promise_purchase_id)
  end

  def bill_of_sale_params
    params.require(:bill_of_sale).permit(
      :promise_purchase_id,
      :vra_no,
      :vnum,
      :make,
      :model,
      :year,
      :price,
      :tax,
      :fees,
      :odometer_reading,
      :seller_5mil,
      :seller_agreement_name,
      :seller_address,
      :seller_postal_code,
      :seller_city,
      :seller_phone,
      :seller_fax,
      :selling_sales_person_name,
      :selling_sales_person_signature,
      :selling_sales_person_registration_no,
      :selling_hst_registration_no,
      :selling_qst_registration_no,
      :buying_sales_person_name,
      :buying_sales_person_signature,
      :buying_sales_person_registration_no,
      :buying_hst_registration_no,
      :buying_qst_registration_no
    )
  end
end
