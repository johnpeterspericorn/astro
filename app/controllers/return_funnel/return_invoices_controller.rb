# encoding: utf-8
require 'column_conversions'

class ReturnFunnel::ReturnInvoicesController < ApplicationController
  include ColumnConversions

  def new
    @return_invoice = ReturnInvoice.new(return_invoice_params)
    @left_lot_in_ods = @return_invoice.left_lot_in_ods?
    if !@return_invoice.valid?
      handle_return_invoice_savnumg_failure
    elsif bill_of_sale_not_processed_for_canada?
      collect_bill_of_sale_info
    else
      return
    end
  end

  def create
    ensure_return_fee_processing if promise_purchase.pplv? && !promise_purchase.seller_paid
    parameterize_invoice_for_canada
    @return_invoice = ReturnInvoice.new(return_invoice_params)
    @return_invoice.user_email = user_email
    @return_invoice.title_received = true if floor_plan_company_holds_title?
    unless floor_plan_branch_selected?
      unless update_buyer_and_agreement_information
        errors = buyer_and_agreement_information.errors
        redirect_to request.referer, alert: errors.map { |_, message| message.humanize }.join('. ')
        return
      end
    end

    collect_payment && return if payment_required? && !promise_offer.canadian_location?
    policy = ReturnInvoicePolicy.new(current_user, @return_invoice)
    authorize(policy.create?)
    process_return_fee_payment && return if return_fee_eligible? && !canadian_return?

    if return_processed?
      clear_invoice_session_params
      create_floor_plan_branch_selection(@return_invoice) if floor_plan_branch
      @return_invoice.finalize(other_branch_notes: other_branch_notes)
      handle_return_invoice_savnumg_success
    else
      handle_return_invoice_savnumg_failure
    end
  end

  private

  def user_email
    buyer_and_agreement_information_params.fetch(:emails, nil)
  end

  def promise_offer
    @promise_purchase.promise_offer
  end
  helper_method :promise_offer

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find(params[:promise_purchase_id])
  end

  helper_method :floor_plan_company
  def floor_plan_company
    @return_invoice.return_certificate.floor_plan_company || (FloorPlanCompany.find(params[:floor_plan_company_id]) if floor_plan_company_paid?)
  end

  def floor_plan_company_holds_title?
    floor_plan_branch.try(:floor_plan_company).try(:holds_title)
  end

  helper_method :floor_plan_company_paid?
  def floor_plan_company_paid?
    params[:floor_plan_company_id].present?
  end

  helper_method :floor_plan_branch_options
  def floor_plan_branch_options
    if agreement_floor_plan_branch_lock.present?
      floor_plan_branch = agreement_floor_plan_branch_lock.floor_plan_branch
      options = [[floor_plan_branch.name, floor_plan_branch .id]]
    else
      options = floor_plan_company.floor_plan_branches.active.map do |branch|
        [branch.name, branch.id]
      end

      options << %w(Other other)
    end
    options
  end

  def agreement_floor_plan_branch_lock
    @agreement_branch_lock ||= AgreementFloorPlanBranchLock.find_by_agreement_no_and_company_code(agreement_number, floor_plan_company.company_code)
  end

  def last_company_selection
    agreement_floor_plan_branch_lock.present? ? agreement_floor_plan_branch_lock : FloorPlanBranchSelection.where(agreement_no: agreement_number).last
  end

  def last_selected_branch
    last_company_selection.try(:floor_plan_branch)
  end
  helper_method :last_selected_branch

  def return_invoice_params
    params
      .require(:return_invoice)
      .permit(
        :mechanical_issues,
        :cosmetic_issues,
        :unable_to_sell,
        :changed_mind,
        :other,
        :no_answer,
        :additional_information,
        :odometer_on_return,
        :title_status,
        :left_lot
      )
      .merge(
        pawn_location: pawn_location,
        promise_purchase: promise_purchase,
        returning_user: current_user
      )
  end

  helper_method :buyer_and_agreement_information
  def buyer_and_agreement_information
    @buyer_and_agreement_information ||= BuyerAndAgreementInformationForm.new(
      pawn_access_number || promise_purchase.promise_offer.pawn_access_no,
      agreement_number, enable_address_verification_params, promise_purchase.country
    )
  end

  helper_method :pawn_access_number
  def pawn_access_number
    @pawn_access_number ||= params[:pawn_access_number] || session[:pawn_access_number]
    session[:pawn_access_number] ||= @pawn_access_number
  end

  def agreement_number
    promise_purchase.promise_offer.agreement_no
  end

  helper_method :return_email
  def return_email
    promise_purchase && promise_purchase.promise_offer.emails
  end

  def handle_return_invoice_savnumg_success
    session.delete(:pawn_access_number)
    flash[:success] = t('return_invoice.finalize_return.success_message')
    options = { slug: @return_invoice.slug, pawn_access_number: @return_invoice.pawn_access_no }
    options[:bill_of_sale_id] = @bill_of_sale.id if canadian_return?
    redirect_to obfuscated_return_certificate_path(options)
  end

  def handle_return_invoice_savnumg_failure
    if @return_invoice.missing_data?
      render 'return_funnel/verifications/new', return_invoice: @return_invoice
    else
      session.delete(:pawn_access_number)
      render 'return_funnel/failure'
    end
  end

  def collect_payment
    redirect_to new_promise_purchase_payment_path(@promise_purchase)
  end

  def collect_and_process_return_fee_payment
    session[:other_branch_notes] = params[:other_branch_notes]
    redirect_to new_promise_purchase_payment_path(@promise_purchase, return_invoice: @return_invoice.attributes)
  end

  def collect_bill_of_sale_info
    clear_invoice_session_params
    set_invoice_session
    redirect_to new_return_funnel_bill_of_sale_path(return_invoice_params)
  end

  def bill_of_sale_not_processed_for_canada?
    !value_to_boolean(params[:bill_of_sale_processed]) && promise_purchase.country == 'Canada'
  end

  def pawn_location
    @pawn_location ||= PawnLocation.find_by_initials(params[:return_invoice][:location_initials])
  end

  def update_buyer_and_agreement_information
    buyer_and_agreement_information.update(buyer_and_agreement_information_params)
  end

  def floor_plan_branch_selected?
    floor_plan_branch_id.present?
  end

  def floor_plan_branch_id
    params[:floor_plan_branch_id]
  end

  def floor_plan_branch
    @floor_plan_branch ||= FloorPlanBranch.find_by_id(floor_plan_branch_id)
  end

  def other_branch_notes
    params[:other_branch_notes]
  end

  def create_floor_plan_branch_selection(return_invoice)
    FloorPlanBranchSelection.create!(
      agreement_no: agreement_number,
      company_code: floor_plan_branch.company_code,
      site_no: floor_plan_branch.site_no,
      return_invoice_id: return_invoice.id
    )
  end

  def return_processed?
    ActiveRecord::Base.transaction do
      @return_invoice.log_new_product_fee if promise_purchase.pplv?
      @return_invoice.save!
      @return_invoice.vra_initiation_adjustments_job
      process_bill_of_sale
      true
    end
  rescue => e
    Rails.logger.error "Error message: #{e.message}"
    return false
  end

  def process_bill_of_sale
    return unless canadian_return?
    @bill_of_sale = BillOfSale.new(bill_of_sale_params.merge!(promise_purchase_id: params[:promise_purchase_id]))
    @bill_of_sale.return_invoice = @return_invoice
    @bill_of_sale.save!
  end

  def buyer_and_agreement_information_params
    params
      .require(:buyer_and_agreement_information_form)
      .permit(
        :address_street,
        :address_suite,
        :address_city,
        :address_state,
        :address_zipcode,
        :name,
        :emails,
        :cell_phone,
        :office_phone,
        :notes,
        :phone
      )
  end

  def payment_required?
    return false if current_user.invoiceable?
    !current_user.super_user? && @promise_purchase.unpaid?
  end

  def enable_address_verification_params
    params[:enable_verification].present? ? params[:enable_verification] : 'true'
  end

  def set_invoice_session
    session[:return_invoice] = params[:return_invoice]
    session[:promise_purchase_id] = params[:promise_purchase_id]
  end

  def set_data_in_session_for_return_fee_processing
    session[:floor_plan_branch] = floor_plan_branch
  end

  def clear_invoice_session_params
    session.delete(:return_invoice)
    session.delete(:promise_purchase_id)
    session.delete(:bill_of_sale)
  end

  def process_return_fee_payment
    set_data_in_session_for_return_fee_processing
    collect_and_process_return_fee_payment
  end

  def bill_of_sale_for_vnum
    BillOfSale.for_vnum(@return_invoice.vnum)
  end

  def parameterize_invoice_for_canada
    params[:return_invoice] ||= session[:return_invoice]
    params[:promise_purchase_id] ||= session[:promise_purchase_id]
    params[:bill_of_sale] ||= session[:bill_of_sale]
  end

  def bill_of_sale_params
    params.require(:bill_of_sale).permit(
      :promise_purchase_id,
      :vra_no,
      :vnum,
      :make,
      :model,
      :year,
      :price,
      :tax,
      :fees,
      :odometer_reading,
      :seller_5mil,
      :seller_agreement_name,
      :seller_address,
      :seller_postal_code,
      :seller_city,
      :seller_phone,
      :seller_fax,
      :selling_sales_person_name,
      :selling_sales_person_signature,
      :selling_sales_person_registration_no,
      :selling_hst_registration_no,
      :selling_qst_registration_no,
      :buying_sales_person_name,
      :buying_sales_person_signature,
      :buying_sales_person_registration_no,
      :buying_hst_registration_no,
      :buying_qst_registration_no
    )
  end

  def canadian_return?
    @return_invoice.country == SupportedCountry::Canada
  end

  def new_product_information
    NewProductFeeInformation.new(promise_purchase)
  end

  def fee_data
    @fee_data ||= new_product_information.fee_details
  end

  def return_fee_eligible?
    !value_to_boolean(session[:skip_fee_payment]) && session[:return_fee].present? && promise_purchase.pplv? && !promise_purchase.seller_paid && fee_data.present? && !fee_data[:fee].zero?
  end

  def ensure_return_fee_processing
    session[:return_fee] = fee_data[:fee] if fee_data.present? && !fee_data[:fee].zero? && !promise_purchase.return_fee_captured? && !session[:return_fee].present? && !value_to_boolean(session[:skip_fee_payment])
  end
end
