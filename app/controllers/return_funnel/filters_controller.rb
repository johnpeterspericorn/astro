# encoding: utf-8
class ReturnFunnel::FiltersController < ApplicationController
  def index
    session.delete(:pawn_access_number) # cf. Chore #69005222
    redirect_to return_funnel_filter_path(:pawn_access_number_filter) unless current_user.super_user?
  end

  def show
    return unless current_user.buyer_user?
    pawn_access_number = Astro::BUYER_AUTHORIZATION_CLASS.new(username: current_user.username).get_pawn_access_number
    redirect_to return_funnel_filtered_purchases_path(filter_type: :pawn_access_number, pawn_access_number: pawn_access_number)
  end

  private

  helper_method :filter_type
  def filter_type
    params[:id] || 'pawn_access_number_filter'
  end

  def current_filter_human_name
    filter_type.gsub(/_filter$/, '').humanize
  end
end
