# encoding: utf-8

class ReturnFunnel::ReturnFeeNotificationsController < ApplicationController
  def new
    @return_fee_data = ReturnFeeNotificationForPromisePurchase.new(promise_purchase_id)
  rescue ReturnFeeNotificationForPromisePurchase::ReturnFeeIneligibleError
    session[:skip_fee_payment] = false
    redirect_to new_return_funnel_verification_path(promise_purchase_id: promise_purchase_id)
  end

  private

  def promise_purchase_id
    params[:promise_purchase_id]
  end
end
