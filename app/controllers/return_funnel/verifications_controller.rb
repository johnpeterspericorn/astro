# encoding: utf-8
require 'column_conversions'

class ReturnFunnel::VerificationsController < ApplicationController
  include ColumnConversions

  rescue_from 'Sas::ApiError', 'Sas::VehicleAdjustmentsApiClient::VehicleNotFound', with: :redirect_to_how_paid_page

  def new
    set_fee_processing_in_session if promise_purchase.pplv?
    @return_invoice     = ReturnInvoice.new
    @promise_purchase = promise_purchase
    @floor_plan_company_id = params[:floor_plan_company_id]

    promise_offer
      .pawn_information
      .touch_vehicle_price unless value_to_boolean(params[:bypass_adjustment_request_error])
    # demeter faints here
    collect_payment if payment_required? && !promise_offer.canadian_location?
  end

  def redirect_to_how_paid_page
    redirect_to new_how_paid_information_path(vnum: @promise_purchase.vnum, promise_purchase_id: @promise_purchase.id)
  end

  private

  def collect_payment
    redirect_to new_promise_purchase_payment_path(@promise_purchase)
  end

  def promise_offer
    @promise_offer ||= promise_purchase.promise_offer
  end
  helper_method :promise_offer

  def promise_purchase
    @promise_purchase ||= PromisePurchase.find(params[:promise_purchase_id])
  end

  def payment_required?
    return false if current_user.invoiceable?
    !current_user.super_user? && @promise_purchase.unpaid?
  end

  def set_fee_processing_in_session
    session[:return_fee] = params[:return_fee] if params[:return_fee].present?
    session[:skip_fee_payment] = params[:skip_fee_payment] if params[:skip_fee_payment].present?
  end
end
