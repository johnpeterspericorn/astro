# encoding: utf-8

class ReturnFunnel::PaymentNotificationsController < ApplicationController
  def new
    if continue_with_return?
      redirect_to return_link_for_pplv_nonpplv
    else
      @floor_plan_company = promise_offer.floor_plan_company if promise_offer.adjustments_paid_by_floor_plan?
      @purchase = purchase.decorate
    end
  end

  private

  def purchase
    @purchase ||= PromisePurchase.find(params[:promise_purchase_id])
  end

  def promise_offer
    purchase.promise_offer
  end

  def continue_with_return?
    purchase.paid? || promise_offer.canadian_location? || purchase.paid_by_buyer?
  end

  def return_link_for_pplv_nonpplv
    args = { promise_purchase_id: purchase.id }
    if purchase.pplv?
      new_return_funnel_return_fee_notification_path(args)
    else
      new_return_funnel_verification_path(args)
    end
  end
end
