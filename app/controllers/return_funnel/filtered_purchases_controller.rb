# encoding: utf-8
class ReturnFunnel::FilteredPurchasesController < ApplicationController
  helper_method :pawn_access_number, :buyer_and_agreement_information, :agreement_name, :filter_type, :promise_purchases, :any_eligible_returns?, :return_email, :paginated_purchases

  def index
    return if return_invoice_filter.valid_filter?

    flash[:alert] = if return_invoice_filter.attempted?
                      return_invoice_filter.error_message
                    else
                      I18n.t('return_invoice.missing_filter_error', filter_type: current_filter_human_name)
                    end

    redirect_to return_funnel_filter_path(filter_type)
  end

  private

  def promise_purchases
    purchases = return_invoice_filter.promise_purchases
    purchases.select! { |purchase| purchase.vnum.end_with?(params[:vnum_suffix]) } if params[:vnum_suffix].present?
    purchases
  end

  def filter_type
    params[:filter_type] || 'pawn_access_number_filter'
  end

  def current_filter_human_name
    filter_type.gsub(/_filter$/, '').humanize
  end

  def return_invoice_filter
    @return_invoice_filter ||= ReturnInvoiceFilter.new(current_user, params.merge(pawn_access_number: pawn_access_number))
  end

  def any_eligible_returns?
    return_invoice_filter.any_eligible_returns?
  end

  def pawn_access_number
    @pawn_access_number ||= params[:pawn_access_number] || session[:pawn_access_number]
    session[:pawn_access_number].blank? ? @pawn_access_number : session[:pawn_access_number]
  end

  def paginated_purchases
    sorted_purchases = promise_purchases.sort_by(&:return_funnel_priority)

    Kaminari.paginate_array(sorted_purchases)
            .page(params[:page] || 1)
            .per(10)
            .tap do |purchases|
      purchases.map(&:promise_offer).each(&:update_ods_attributes)
    end
  end

  def agreement_name
    unless @agreement_name
      @agreement_name = if pawn_access_number.present?
                       Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: pawn_access_number).get_agreement_name
                     else
                       ''
                     end
    end
    @agreement_name
  end
end
