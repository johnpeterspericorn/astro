# encoding: utf-8
class FloorPlanBranchesController < ApplicationController
  def index
    floor_plan_branches = floor_plan_company.floor_plan_branches
    respond_to do |format|
      format.json { render json: { floor_plan_branches: floor_plan_branches }, status: :ok }
    end
  end

  private

  def floor_plan_company
    @floor_plan_company ||= FloorPlanCompany.find(params[:floor_plan_company_id])
  end
end
