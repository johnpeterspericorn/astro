# encoding: utf-8
class FlatRates::AgreementNumberListsController < ApplicationController
  before_action :swipe, only: :show

  def show
    authorize(flat_rate_policy.index?)

    @buyer_information = BuyerInformation.build_with_defaults(pawn_access_number).tap(&:save!)
    @agreement_number_list = AgreementNumberList.new(pawn_access_number)

    return if @agreement_number_list.valid?
    flash.now[:notice] = 'Agreement number list could not be determined. Please provide an pawn access number.'
    render :new
  end

  private

  def swipe
    redirect_to new_swipe_path(swipe_to: request.path, swipe_method: :get) unless pawn_access_number.present?
  end

  def pawn_access_number
    params[:pawn_access_number]
  end
  helper_method :pawn_access_number

  def distance1
    I18n.translate("offer_distances.#{country_code}.distance1")
  end
  helper_method :distance1

  def distance2
    I18n.translate("offer_distances.#{country_code}.distance2")
  end
  helper_method :distance2

  def distance3
    I18n.translate("offer_distances.#{country_code}.distance3")
  end
  helper_method :distance3

  def distances
    i18n_distances.values
  end

  def i18n_distances
    I18n.translate("offer_distances.#{country_code}")
  end

  def country_code
    country = country_object || default_country_object
    country.try(:alpha2)
  end

  def default_country_object
    Country.find_country_by_name(SupportedCountry::DEFAULT)
  end

  def country_object
    Country.find_country_by_name(country_param)
  end

  def country_param
    params[:country]
  end
  helper_method :country_param

  def flat_rate_offer_batch
    @flat_rate_offer_batch ||=
      FlatRateOfferBatch.find_by_pawn_access_no(pawn_access_number)
  end

  def flat_rate_policy
    @flat_rate_policy ||= FlatRatePolicy.new(current_user)
  end
end
