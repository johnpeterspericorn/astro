# encoding: utf-8
class ExternalVehiclesDocumentsController < ApplicationController
  def destroy
    external_vehicles_doc = ExternalVehiclesDocument.find(params[:id])
    if external_vehicles_doc.destroy
      render json: { status: :success }
    else
      render json: { errors: external_vehicles_doc.errors.to_a }, status: :unprocessable_entity
    end
  end

  def create
    external_vehicles_doc = ExternalVehiclesDocument.new(external_vehicles_documents_params)
    the_path = 'external_vehicles_approver_path'
    options = { external_vehicle_id: rejected_vnum_id, id: external_vehicles_doc.external_vehicles_batch_id }
    the_path = 'admin_external_vehicles_batch_path' if rejected_vnum_id.blank?
    options[:notice] = 'Not able to save file' unless external_vehicles_doc.save
    redirect_to send(the_path.to_sym, options)
  end

  private

  def external_vehicles_documents_params
    params
      .require(:external_vehicles_document)
      .permit(
        :document,
        :external_vehicles_batch_id
      )
  end

  def rejected_vnum_id
    params[:rejected_vnum_id]
  end
end
