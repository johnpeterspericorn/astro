# encoding: utf-8
class UserDetailsController < ApplicationController
  respond_to :json, :xml

  def index
    user_details = {  agreement_transaction_viewer: current_user.agreement_transaction_viewer, agreement_score_viewer: current_user.agreement_score_viewer, buyer_user: current_user.buyer_user, pawn_access_number: current_user.pawn_access_number
                   }
    respond_with user_details
  end
end
