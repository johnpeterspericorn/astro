# encoding: utf-8
class CancellationsController < ApplicationController
  def new
    authorize(cancellation_policy.create?)
    @cancellation = Cancellation.new
  end

  def create
    authorize(cancellation_policy.create?)
    @cancellation = Cancellation.new(cancellation_params)

    if @cancellation.process
      redirect_to root_path
    else
      flash.now[:error] = @cancellation.errors.full_messages.to_sentence
      render :new
    end
  end

  private

  def promise_purchase
    PromisePurchase.find(promise_purchase_id)
  end

  def cancellation_policy
    @cancellation_policy ||= CancellationPolicy.new(current_user, promise_purchase)
  end

  helper_method :cancellation_policy

  def cancellation_params
    params
      .require(:cancellation)
      .permit(*cancellation_policy.permitted_attributes)
      .merge(promise_purchase_id: promise_purchase_id, user: current_user)
  end

  def promise_purchase_id
    params[:promise_purchase_id]
  end

  def form_path
    promise_purchase_cancellations_path(promise_purchase_id)
  end
  helper_method :form_path
end
