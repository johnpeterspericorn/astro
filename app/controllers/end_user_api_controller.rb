# encoding: utf-8
class EndUserApiController < ApplicationController
  include Authorizable
  before_action :authorize_buyer_user!

  def authorize_buyer_user!
    authorize(current_user.buyer_user?)
  end
end
