# encoding: utf-8
class CouponsController < ApplicationController
  def show
    coupon = Coupon.find_by_code(params[:id].upcase)
    is_applied = params[:applied_coupons].include?(params[:id]) if params[:applied_coupons]

    respond_to do |format|
      if !coupon
        errors = { errors: [t('coupons.missing_error', code: params[:id])] }
        format.json { render json: errors, status: 404 }
      elsif coupon.consumed? || is_applied
        errors = { errors: [t('coupons.consumed_error', code: coupon.code)] }
        format.json { render json: errors, status: 410 }
      elsif coupon.restricted?(params[:country])
        errors = { errors: [t('coupons.show.restricted_error', code: coupon.code)] }
        format.json { render json: errors, status: 410 }
      else
        format.json { render json: coupon }
      end
    end
  end
end
