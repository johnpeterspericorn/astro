# encoding: utf-8
class ExternalVehiclesApproverController < ApplicationController
  def show
    authorize(policy.approve? || policy.show?)
    @external_vehicle_id = external_vehicle_id
    @vehicle_creator = policy.create?
    @vehicle_approver = policy.approve?
    @batch = ExternalVehiclesBatch.find(batch_id)
    @external_vehicles = @batch.external_vehicles_loads
    @external_vehicles_documents = @batch.external_vehicles_documents
    @vehicle_noneditable = vehicle_noneditable?
    agreements = @external_vehicles.pluck(:agreement_no).uniq
    @agreement_informations = AgreementInformation.agreement_informations_hash agreements
  end

  private

  def policy
    @policy ||= ExternalVehiclesLoadPolicy.new(current_user)
  end

  def batch_id
    params[:id]
  end

  def external_vehicle_id
    params[:external_vehicle_id]
  end

  def rejected_vehicle
    @rejected_vehicle ||= ExternalVehiclesLoad.find(@external_vehicle_id)
  end

  def vehicle_noneditable?
    rejected_vehicle.approved? || rejected_vehicle.unposted? || rejected_vehicle.posted?
  end
end
