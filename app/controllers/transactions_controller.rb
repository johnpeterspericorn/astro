# encoding: utf-8
class TransactionsController < ApplicationController
  respond_to :json, :html
  include ColumnConversions
  before_action :transactions_viewable, only: [:index]

  def index
    offset = params.fetch(:offset) { 0 }
    limit = params.fetch(:limit) { 10 }
    days = params.fetch(:days) { 90 }
    active = params.fetch(:active) { true }
    returned = params.fetch(:returned) { nil }

    attributes = {
      agreement_number: params[:agreement_number],
      returned: value_to_boolean(returned),
      days: days,
      offset: offset,
      limit: limit,
      active: value_to_boolean(active),
      location_initial: params[:location_initial],
      days_left: params[:days_left]
    }
    @transactions = Transaction.by_agreement_number(attributes)
    respond_with @transactions
  end

  def show
    @promise_purchases          = promise_purchases
    @inspection_purchases         = inspection_purchases
    @bundled_purchases            = bundled_purchases
    @pending_purchases            = pending_purchases
    @purchase_total               = purchase_total

    session.delete(:pawn_access_number)

    respond_to do |format|
      format.html { render }
      format.json { render json: json_body }
    end
  end

  private

  def transactions_viewable
    return true if current_user.admin?
    authorize current_user.agreement_transaction_viewer?
  end

  def bundled_purchases
    promise_purchases = PromisePurchase
                          .bundled
                          .for_transaction_id(params[:id])
                          .map(&:decorate)
                          .reject { |purchase| purchase.promise_offer.pending? }

    promise_purchases.map do |promise_purchase|
      BundledPurchase.new(promise_purchase: promise_purchase, inspection_purchase: promise_purchase.promise_offer.inspection_purchase)
    end
  end

  def pending_purchases
    PromisePurchase
      .where(transaction_id: params[:id])
      .map(&:decorate)
      .select { |purchase| purchase.promise_offer.pending? }
  end

  def promise_purchases
    PromisePurchase
      .unbundled
      .for_transaction_id(params[:id])
      .map(&:decorate)
      .reject { |purchase| purchase.promise_offer.pending? }
  end

  def inspection_purchases
    if Settings.inspections_enabled
      InspectionPurchase.unbundled_for_transaction_id(params[:id]).map(&:decorate)
    else
      InspectionPurchase.where(transaction_id: params[:id])
    end
  end

  def purchase_total
    bundle_total     = bundled_purchases.map    { |purchase| purchase.promise_purchase.source.promise_price + purchase.inspection_purchase.source.price }.sum
    promise_total  = promise_purchases.map  { |purchase| purchase.source.promise_price }.sum
    inspection_total = inspection_purchases.map { |inspection| inspection.source.price       }.sum

    bundle_total + promise_total + inspection_total
  end

  def json_body
    sold_vehicles = {}
    promise_purchases.each do |purchase|
      sold_vehicles[purchase.promise_offer_id] = PromiseOffer.find(purchase.promise_offer_id)
    end
    { promise_purchases: promise_purchases, sold_vehicles: sold_vehicles, purchase_total: purchase_total }
  end
end
