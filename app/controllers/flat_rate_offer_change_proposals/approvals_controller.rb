# encoding: utf-8
class FlatRateOfferChangeProposals::ApprovalsController < ApplicationController
  def create
    flat_rate_offer_change_proposal.approve
    flash[:success] = 'AgreementShield 360 change approved!'
    redirect_to flat_rate_offer_batch_approval_path(batch)
  end

  private

  def flat_rate_offer_change_proposal
    @flat_rate_offer_change_proposal ||=
      FlatRateOfferChangeProposal.find(params[:flat_rate_offer_change_proposal_id])
  end

  def batch
    flat_rate_offer_change_proposal.flat_rate_offer.flat_rate_offer_batch
  end
end
