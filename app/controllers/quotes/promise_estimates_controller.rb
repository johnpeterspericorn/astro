# encoding: utf-8
class Quotes::PromiseEstimatesController < ApplicationController
  FILTER_TYPES = %w(pawn vnum ove).freeze

  def index
    @partial = "shared/#{params[:filter_type]}_filter" if FILTER_TYPES.include?(params[:filter_type])

    if presale_vehicle_filter.blank?
      flash[:error] = I18n.t('promise_estimates.missing_info')
      redirect_to(:back) && return
    elsif presale_vehicle_filter.valid_search_options?
      @filtered_presale_vehicles = presale_vehicle_filter.filtered.page(params[:page])
      @vehicle_pages = vehicle_pages
    else
      @filtered_presale_vehicles = []
      @vehicle_pages = []
    end

    respond_to do |format|
      format.html
      format.json { render json: json_body }
    end
  end

  private

  def vehicle_pages
    vehicle_pages = []
    1.upto(print_page_count) do |page_num|
      vehicle_pages << presale_vehicle_filter.filtered
                       .page(page_num)
                       .per(Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PRINT_PAGE)
    end
    vehicle_pages
  end

  def distance1_label(country_code)
    I18n.translate("offer_distances.#{country_code}.distance1").to_s + ' ' + distance_units(country_code)
  end
  helper_method :distance1_label

  def distance2_label(country_code)
    I18n.translate("offer_distances.#{country_code}.distance2").to_s + ' ' + distance_units(country_code)
  end
  helper_method :distance2_label

  def distance_units(country_code)
    I18n.translate("distance_units.#{country_code}.unit")
  end

  def presale_vehicle_filter
    @presale_vehicle_filter ||= [
      PresaleVehicleFilter::OVE.new(params),
      PresaleVehicleFilter::VNUM.new(params),
      PresaleVehicleFilter::PawnLocation.new(params)
    ].find(&:attempted?)
  end

  def print_page_count
    (presale_vehicle_filter.filtered.count.to_f / Astro::GUARANTEE_ESTIMATES_VEHICLES_PER_PRINT_PAGE).ceil
  end

  def json_body
    {
      vehicle_data: presale_vehicle_filter.filtered,
      ineligibility_conditions: IneligibilityCondition.to_hash
    }
  end

  def location_initials
    params[:location_initials].blank? ? current_user.location_initials : params[:location_initials]
  end
  helper_method :location_initials
end
