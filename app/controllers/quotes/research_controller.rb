# encoding: utf-8
class Quotes::ResearchController < ApplicationController
  FILTER_TYPES = %w(pawn vnum ove).freeze
  before_action :require_pawn_access_number

  def index
    session[:pawn_access_number] = params[:pawn_access_number]
    @agreementships = AgreementshipPresenter.new(pawn_access_number)
  end

  def edit
    @partial = "shared/#{params[:filter_type]}_filter" if FILTER_TYPES.include?(params[:filter_type])
  end

  private

  def require_pawn_access_number
    redirect_to new_swipe_path(swipe_to: quotes_research_path, swipe_method: :get) unless pawn_access_number.present?
  end

  def pawn_access_number
    params[:pawn_access_number] || current_user.pawn_access_number
  end

  def location_initials
    params[:location_initials] || current_user.location_initials
  end
  helper_method :location_initials
end
