# encoding: utf-8
require 'column_conversions'

module FlatRateOfferBatches
  class ApprovalsController < ApplicationController
    include ColumnConversions

    def show
      if flat_rate_offer_batch.offers?
        populate_offer_values
        authorize(approval_policy.show?)
      else
        flash[:error] = I18n.t('flat_rates.offer_not_valid')
        redirect_to root_path
      end
    end

    def create
      authorize(approval_policy.create?)
      if flat_rate_offer_batch.valid_offers?
        if flat_rate_offer_batch_approval.approve
          flash[:success] = I18n.t('flat_rates.accepted')
        else
          flash[:error] = I18n.t('flat_rates.not_accepted')
        end
      else
        flash[:error] = "#{I18n.t('flat_rates.offer_not_valid')} #{I18n.t('flat_rates.not_accepted')}"
      end

      redirect_to root_path
    end

    def destroy
      authorize(approval_policy.destroy?)
      if flat_rate_offer_batch.valid_offers?
        if flat_rate_offer_batch_approval.reject
          flash[:success] = I18n.t('flat_rates.rejected')
        else
          flash[:error] = I18n.t('flat_rates.not_rejected')
        end
      else
        flash[:error] = "#{I18n.t('flat_rates.offer_not_valid')} #{I18n.t('flat_rates.not_accepted')}"
      end

      redirect_to root_path
    end

    private

    def populate_offer_values
      @flat_rate_offer_batch = flat_rate_offer_batch.decorate
      max_offer = @flat_rate_offer_batch.flat_rate_offers.sort_by(&:price).last.source
      @example = FlatRateOfferExampleDecorator.new(FlatRateOfferExample.new(max_offer, FlatRateOfferExample::EXAMPLE_VEHICLE_PRICE))
      if @flat_rate_offer_batch.belongs_to_canada?
        @limits = { max_odometer_limit: '400,000 kilometers', max_vehicle_value: '$120,000' }
        @units = 'Kilometers'
      else
        @limits = { max_odometer_limit: '250,000 miles', max_vehicle_value: '$100,000' }
        @units = 'Miles'
      end
    end

    def computed_effective_date
      Date.current
    end

    def flat_rate_offer_batch
      @flat_rate_offer_batch ||=
        FlatRateOfferBatch.find(params[:flat_rate_offer_batch_id])
    end

    def flat_rate_offer_batch_approval
      @flat_rate_offer_batch_approval ||=
        FlatRateOfferBatchApproval.new(flat_rate_offer_batch, current_user, computed_effective_date)
    end

    def approval_policy
      @flat_rate_offer_batch_policy ||=
        FlatRateOfferBatchApprovalPolicy.new(current_user, flat_rate_offer_batch)
    end
  end
end
