# encoding: utf-8
module Authorizable
  class NotAuthorizedError < StandardError; end

  def self.included(base)
    base.class_eval do
      rescue_from NotAuthorizedError do |exception|
        respond_to do |format|
          format.html { redirect_to root_path, alert: exception.message }
          format.json { head :unauthorized }
          format.xml { head :unauthorized }
        end
      end

      def authorize(is_authorized, options = {})
        message = options.fetch(:error, I18n.t('not_allowed'))
        raise(NotAuthorizedError, message) unless is_authorized
      end
    end
  end
end
