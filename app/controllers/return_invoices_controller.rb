# encoding: utf-8
require 'column_conversions'

class ReturnInvoicesController < ApplicationController
  include ColumnConversions

  def edit
    authorize(return_invoice_policy.update?)
  end

  def update
    authorize(return_invoice_policy.update?)

    if return_invoice.update_attributes(return_invoice_params)
      return_invoice.update_drop_notice if return_invoice.location_updated?
      email_updated_vra
      redirect_to return_funnel_filters_path
    else
      render :edit
    end
  end

  private

  def return_invoice_id
    params[:id]
  end

  def return_invoice_params
    params
      .require(:return_invoice)
      .permit(:pawn_location_id, :title_status)
  end

  def return_invoice
    @return_invoice ||= ReturnInvoice.find(return_invoice_id).decorate
  end
  helper_method :return_invoice

  def return_invoice_policy
    @return_invoice_policy ||= ReturnInvoicePolicy.new(current_user, return_invoice)
  end

  def email_updated_vra
    ReturnInvoiceMailer.return_confirmation(return_invoice.object).deliver_later if return_invoice.location_updated? || email_confirmation?
  end

  def email_confirmation?
    value_to_boolean(params[:send_return_confirmation])
  end
end
