# encoding: utf-8
class Admin::VinBlacklistsController < AdminController
  def index
    @vnum_blacklists = VinBlacklist.page(params[:page])
    @vnum_blacklists = @vnum_blacklists.where(vnum: vnum_filter) if vnum_filter.present?
    @vnum_blacklists = PaginatingDecorator.new(@vnum_blacklists)
  end

  def new
    @vnum_blacklist = VinBlacklist.new
  end

  def edit
    @vnum_blacklist = VinBlacklist.find(params[:id]).decorate
  end

  def create
    @vnum_blacklist = VinBlacklist.new(vnum_blacklist_params)

    if @vnum_blacklist.save
      flash[:success] = 'Vin Blacklist created'
      redirect_to admin_vnum_blacklists_path
    else
      flash[:error] = "Error creating Vin Blacklist: #{@vnum_blacklist.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    @vnum_blacklist = VinBlacklist.find(params[:id]).decorate

    if @vnum_blacklist.update_attributes(vnum_blacklist_params)
      flash[:success] = 'Successfully updated Vin Blacklist'
      redirect_to admin_vnum_blacklists_path
    else
      flash[:error] = "Error updating Vin Blacklist: #{@vnum_blacklist.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    VinBlacklist.find(params[:id]).destroy
    redirect_to admin_vnum_blacklists_path
  end

  private

  def fields
    {
      'VNUM'                  => :vnum,
      'Expiration Date'      => :expiration_date,
      'Status' => :status,
      'Created At'           => :created_at,
      'Notes'                => :notes,
      'Edit Vin Blacklist'   => :edit_vnum_blacklist_link,
      'Delete Vin Blacklist' => :delete_vnum_blacklist_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter

  def vnum_blacklist_params
    params
      .require(:vnum_blacklist)
      .permit(:expiration_date, :vnum, :notes)
  end
end
