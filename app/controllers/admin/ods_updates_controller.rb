# encoding: utf-8
module Admin
  class OdsUpdatesController < AdminController
    def update
      promise_offer.update_ods_attributes
      redirect_to :back, notice: 'ODS attributes have been updated.'
    end

    private

    def promise_offer_id
      params[:promise_offer_id]
    end

    def promise_offer
      @promise_offer ||= PromiseOffer.find(promise_offer_id)
    end
  end
end
