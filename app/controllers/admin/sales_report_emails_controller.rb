# encoding: utf-8
class Admin::SalesReportEmailsController < AdminController
  def update
    Settings.sales_report_email = params[:email]
    redirect_to :back, notice: 'Sales report email updated!'
  end
end
