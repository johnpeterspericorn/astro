# encoding: utf-8
class Admin::FloorPlanCompaniesController < AdminController
  before_action :authorize_finance_sections_viewer

  def index
    @floor_plan_companies = FloorPlanCompany.all

    if company_code_filter
      @floor_plan_companies = @floor_plan_companies.where(company_code: company_code_filter)
    end

    @floor_plan_companies = @floor_plan_companies.page(params[:page])
    @floor_plan_companies = PaginatingDecorator.new(@floor_plan_companies)
  end

  def new
    @floor_plan_company = FloorPlanCompany.new
  end

  def create
    floor_plan_company = FloorPlanCompany.new(floor_plan_company_params)
    floor_plan_company.save!

    flash[:success] = 'Created floor plan company'
    redirect_to admin_floor_plan_companies_path
  end

  def edit
    @floor_plan_company = FloorPlanCompany.find(params[:id])
  end

  def update
    floor_plan_company = FloorPlanCompany.find(params[:id])
    floor_plan_company.update_attributes!(floor_plan_company_params)

    flash[:success] = 'Updated floor plan company'
    redirect_to admin_floor_plan_companies_path
  end

  def fields
    {
      'Name'            => :admin_link,
      'Company Code'    => :company_code,
      'Holds Title?'    => :holds_title,
      'Generate Label?' => :generate_shipping_label,
      'Skip DS Fee?'    => :skip_ds_payment,
      'Oracle ID'       => :oracle_id,
      'Branches'        => :branches_link
    }
  end

  helper_method :fields

  private

  def floor_plan_company_params
    params
      .require(:floor_plan_company)
      .permit(
        :name,
        :company_code,
        :holds_title,
        :generate_shipping_label,
        :oracle_id,
        :skip_ds_payment
      )
  end

  def filters
    params.fetch(:filters) { Hash.new }
  end

  def company_code_filter
    filters[:company_code]
  end
  helper_method :company_code_filter
end
