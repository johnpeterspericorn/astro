# encoding: utf-8
module Admin
  class ExpiringTitleNotificationsController < AdminController
    def create
      ReturnInvoiceMailer.title_expiration_notification(return_invoice.object).deliver_later
      flash[:success] = "Title Notice sent for VRA# #{return_invoice.id}"
      redirect_to :back
    end

    private

    def return_invoice
      @return_invoice ||= ReturnInvoice.find(params[:return_invoice_id]).decorate
    end
  end
end
