# encoding: utf-8
class Admin::BuyerInformationController < AdminController
  def index
    @buyer_information = BuyerInformation.page(params[:page])
    @buyer_information = @buyer_information.where(pawn_access_no: pawn_access_no_filter) if pawn_access_no_filter.present?
    @buyer_information = @buyer_information.for_agreement_number(agreement_number_filter) if agreement_number_filter.present?
    @buyer_information = PaginatingDecorator.new(@buyer_information)
  end

  def new
    @buyer_information = BuyerInformation.new
  end

  def create
    @buyer_information = BuyerInformation.new(buyer_information_params)

    if @buyer_information.save
      flash[:success] = 'Buyer Information created'
      redirect_to admin_buyer_information_index_path
    else
      flash[:error] = "Error creating BuyerInformation: #{@buyer_information.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @buyer_information = BuyerInformation.find(params[:id]).decorate
  end

  def update
    @buyer_information = BuyerInformation.find(params[:id]).decorate

    if @buyer_information.update_attributes(buyer_information_params)
      flash[:success] = 'Successfully updated Buyer Information'
      redirect_to admin_buyer_information_index_path
    else
      flash[:error] = "Error updating Buyer Information: #{@buyer_information.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    BuyerInformation.find(params[:id]).destroy
    redirect_to admin_buyer_information_index_path
  end

  private

  def fields
    {
      'Name'             => :name,
      'Email'            => :emails,
      'Cell Phone'       => :cell_phone,
      'Office Phone'     => :office_phone,
      'Pawn Access #' => :pawn_access_no,
      'Agreement #'         => :agreement_numbers,
      'Edit'             => :edit_buyer_information_link,
      'Delete'           => :delete_buyer_information_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def agreement_number_filter
    filters[:agreement_number]
  end
  helper_method :agreement_number_filter

  def pawn_access_no_filter
    filters[:pawn_access_no]
  end
  helper_method :pawn_access_no_filter

  def buyer_information_params
    params
      .require(:buyer_information)
      .permit(:name, :emails, :cell_phone, :office_phone, :pawn_access_no)
  end
end
