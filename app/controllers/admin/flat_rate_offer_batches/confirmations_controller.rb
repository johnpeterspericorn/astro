# encoding: utf-8
class Admin::FlatRateOfferBatches::ConfirmationsController < AdminController
  def create
    if flat_rate_offer_batch.confirmed?
      flash[:alert] = 'This batch has already been confirmed.'
    else
      flat_rate_offer_batch.confirm!
      flash[:success] = 'Batch confirmed.'
    end

    redirect_to :back
  end

  private

  def flat_rate_offer_batch_id
    params[:flat_rate_offer_batch_id]
  end

  def flat_rate_offer_batch
    @flat_rate_offer_batch ||=
      FlatRateOfferBatch.find(flat_rate_offer_batch_id)
  end
end
