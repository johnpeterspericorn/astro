# encoding: utf-8
class Admin::PromiseOffersController < AdminController
  def index
    @promise_offers = PromiseOffer.includes(:pawn_information)
                                      .includes(:vehicle_information)
                                      .order('promise_offers.updated_at DESC')
                                      .page(params[:page])
                                      binding.pry
    @promise_offers = @promise_offers.by_vnum(filters.vnum) if filters.vnum.present?
    @promise_offers = @promise_offers.by_agreement_no(filters.agreement_no) if filters.agreement_no.present?
    @promise_offers = @promise_offers.purchased_at(date_from..date_to) if date_from.present? && date_to.present?
    @promise_offers = PaginatingDecorator.new(@promise_offers)
  end

  def edit
    @promise_offer = PromiseOffer.find(params[:id])
  end

  def update
    promise_offer = PromiseOffer.find(params[:id])

    if promise_offer.update_attributes(offer_params)
      flash[:success] = 'Successfully updated offer'

      adjustment = promise_offer.previous_changes['promise_price']
      if adjustment.present? && charge_adjustment?
        PromiseChargeJob.enqueue(
          PromiseAdjustment.new(
            promise_offer,
            *adjustment
          )
        )
      end

      if params[:update_confirmation] && !promise_offer.pending?
        PromisePurchaseMailer.purchase_confirmation(
          user_email: promise_offer.emails,
          promise_purchases: [promise_offer.promise_purchase]
        ).deliver_later
      end
    else
      flash[:error] = "Error updating offer: #{promise_offer.errors.full_messages.to_sentence}"
    end

    redirect_to :back
  end

  def fields
    {
      'Agreement #'                  => :agreement_no,
      'Pawn Access #'          => :pawn_access_no,
      'Badge #'                   => :badge_no,
      'Pawn Code'              => :universal_no,
      'VNUM'                       => :vnum,
      'Vehicle'                   => :vehicle_full_description,
      'Odometer Reading'          => :odometer_reading,
      'Days Selected'             => :days_selected,
      'Miles Selected'            => :miles_selected,
      'Promise Status'          => :promise_status,
      'Promise Price'           => :promise_price,
      'Purchased At'              => :purchased_at,
      'Last Updated'              => :updated_at,
      'Provided Email (optional)' => :emails,
      'Eligible for purchase?'    => :eligible_for_purchase?,
      ''                          => :ods_update_link,
      'Edit'                      => :edit_promise_link,
      'Cancelled Purchases'       => :cancelled_purchases_link
    }
  end
  helper_method :fields

  private

  def charge_adjustment?
    params[:charge] == 'as400'
  end

  def filter_params
    params.fetch(:filters) { Hash.new }
  end

  def filters
    @filters ||= OpenStruct.new(filter_params)
  end
  helper_method :filters

  def date_from
    Time.zone.parse(filters.date_from) if filters.date_from.present?
  end
  helper_method :date_from

  def date_to
    Time.zone.parse(filters.date_to) if filters.date_to.present?
  end
  helper_method :date_to

  def offer_params
    params[:promise_offer].permit(
      :days_selected,
      :miles_selected,
      :emails,
      :promise_price,
      :transport_reimbursement,
      :must_be_promised_at,
      :bypass_charge,
      vehicle_information_attributes: [:odometer_reading, :id, :make, :model, :year],
      pawn_information_attributes: [:vehicle_purchase_price, :id]
    )
  end
end
