# encoding: utf-8
require 'column_conversions'

module Admin
  class AuditsController < AdminController
    include ColumnConversions
    DEFAULT_DURATION = 2.business_days

    def new
      @return_certificate = ReturnCertificate.new(return_invoice)
      @audit = audit || return_invoice.build_audit(audit_params)
      @audit.reasons.build
    end

    def create
      @audit = return_invoice.build_audit(audit_params)

      if @audit.save
        email_opening_audit
        redirect_to admin_return_invoices_path, notice: I18n.t('return_invoice.audit.success_message', vra: return_invoice.id)
      else
        flash[:error] = "Audit Creation Failed: #{@audit.errors.full_messages.to_sentence}"
        render :new
      end
    end

    def edit
      @audit = audit
    end

    def update
      @audit = audit

      if @audit.update_attributes(audit_params)
        flash[:success] = 'Successfully updated audit'
        redirect_to admin_audited_returns_path
      else
        flash[:error] = "Edit failed: #{@audit.errors.to_a.to_sentence}"
        render :edit
      end
    end

    def clear
      audit.clear!
      AuditMailer.notify_return_audit_cleared(return_invoice).deliver_later
      redirect_to :back
    end

    private

    def return_invoice_id
      params.fetch(:return_invoice_id)
    end

    def email_opening_audit
      AuditMailer.notify_opening_audit(return_invoice).deliver_later if email_confirmation?
    end

    def email_confirmation?
      value_to_boolean(params[:send_email])
    end

    def return_certificate
      return_invoice.return_certificate
    end
    helper_method :return_certificate

    def return_invoice
      @return_invoice ||= ReturnInvoice.find(return_invoice_id)
    end

    def audit
      return_invoice.audit
    end

    def audit_params
      params
        .fetch(:audit, default_params)
        .permit(:resolved_by, :emails, reasons_attributes: [:id, :description, :_destroy])
    end

    def default_params
      { resolved_by: DEFAULT_DURATION.from_now.to_date }
    end
  end
end
