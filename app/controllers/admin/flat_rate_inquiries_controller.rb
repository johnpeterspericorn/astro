# encoding: utf-8
class Admin::FlatRateInquiriesController < AdminController
  before_action :authorize_flat_rate_inquiry_viewer

  def index
    @flat_rate_inquiries = FlatRateInquiry.order(:pawn_access_no).page(params[:page])
    @flat_rate_inquiries = @flat_rate_inquiries.where(agreement_no: agreement_no_filter) if agreement_no_filter.present?
    @flat_rate_inquiries = @flat_rate_inquiries.where(pawn_access_no: pawn_access_no_filter) if pawn_access_no_filter.present?
    @flat_rate_inquiries = @flat_rate_inquiries.where(created_at: date_from..date_to) if date_from.present? && date_to.present?
  end

  private

  def fields
    {
      'Username'         => :username,
      'Pawn Access #' => :pawn_access_no,
      'Agreement#'          => :agreement_no,
      'Target Price'     => :target_price,
      'Miles Selected'   => :miles_selected,
      'PSI Eligible'     => :psi_eligible,
      'Agreement Name'      => :agreement_name,
      'Agreement Phone'     => :agreement_phone,
      'Agreement Email'     => :agreement_email,
      'Created At'       => :created_at
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def agreement_no_filter
    filters.fetch(:agreement_no, '').strip
  end
  helper_method :agreement_no_filter

  def pawn_access_no_filter
    filters.fetch(:pawn_access_no, '').strip
  end
  helper_method :pawn_access_no_filter

  def date_to
    Time.zone.parse(filters[:date_to]) if filters[:date_to].present?
  end
  helper_method :date_to

  def date_from
    Time.zone.parse(filters[:date_from]) if filters[:date_from].present?
  end
  helper_method :date_from
end
