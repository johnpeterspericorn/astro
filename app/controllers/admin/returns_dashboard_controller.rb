# encoding: utf-8
class Admin::ReturnsDashboardController < AdminController
  def index
    @filters = OpenStruct.new(filter_params)
    @agreement_returns = ReturnsByAgreement.new(filtered_returns)
    @location_returns = ReturnsByLocation.new(filtered_returns)
    @vehicle_returns = ReturnsByVehicle.new(filtered_returns)
  end

  private

  def filtered_returns
    ReturnInvoice.initiated_at(date_from..date_to)
  end

  def filter_params
    params.fetch(:filters, {})
          .permit(:date_from, :date_to)
  end

  def date_from_param
    filter_params[:date_from]
  end

  def date_to_param
    filter_params[:date_to]
  end

  def date_from
    parse_time(date_from_param, DateTime.current.beginning_of_day)
  end
  helper_method :date_from

  def date_to
    parse_time(date_to_param, DateTime.current)
  end
  helper_method :date_to

  def parse_time(time_string, default)
    return default unless time_string.present?
    Time.zone.parse(time_string)
  end
end
