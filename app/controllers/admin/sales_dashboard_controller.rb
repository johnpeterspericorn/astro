# encoding: utf-8
class Admin::SalesDashboardController < AdminController
  def index
    @location_sales = SalesLocations.new(filtered_sales)
    @total_sales = @location_sales.total_sales
  end

  private

  def filters
    @filters ||= Hashie::Mash.new(filter_params)
  end
  helper_method :filters

  def filtered_sales
    sales = PromisePurchase.joins(promise_offer: { pawn_information: :pawn_location })
                             .promised_at(date_from, date_to)
    sales = sales.merge(PromiseOffer.where(seller_paid: filters.seller_paid)) if filters.seller_paid.present?
    sales = sales.merge(PromiseOffer.where(automatic_purchase: filters.automatic_purchase)) if filters.automatic_purchase.present?
    sales = sales.merge(PawnLocation.where(initials: filters.location_initials)) if filters.location_initials.present?
    sales
  end

  def filter_params
    params.fetch(:filters, {})
          .permit(:date_from, :date_to, :automatic_purchase, :seller_paid, :location_initials)
  end

  def date_from_param
    filter_params[:date_from]
  end

  def date_to_param
    filter_params[:date_to]
  end

  def date_from
    parse_time_else_use_default(date_from_param, DateTime.current.beginning_of_day)
  end
  helper_method :date_from

  def date_to
    parse_time_else_use_default(date_to_param, DateTime.current)
  end
  helper_method :date_to

  def parse_time_else_use_default(time_string, default)
    return default unless time_string.present?
    Time.zone.parse(time_string)
  end
end
