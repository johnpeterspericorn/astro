# encoding: utf-8
require 'csv'

class Admin::PawnLocationsController < AdminController
  respond_to :html, :csv

  def index
    @pawn_locations = PawnLocation.all.decorate
    respond_with(@pawn_locations) do |format|
      format.html
      format.csv { render text: locations_csv }
    end
  end

  def new
    @pawn_location = PawnLocation.new
  end

  def edit
    @pawn_location = PawnLocation.find params[:id]
  end

  def create
    @pawn_location = PawnLocation.create(pawn_location_params)
    respond_with :admin, @pawn_location, location: admin_pawn_locations_path
  end

  def update
    @pawn_location = PawnLocation.find params[:id]
    @pawn_location.update_attributes(pawn_location_params)
    respond_with :admin, @pawn_location, location: admin_pawn_locations_path
  end

  private

  def pawn_location_params
    params
      .require(:pawn_location)
      .permit(
        :name,
        :initials,
        :return_emails,
        :accounting_email,
        :accepts_returns,
        :inspections_enabled,
        :purchases_enabled,
        :return_instructions,
        :g2g_converted,
        :bsc_location,
        :partner_network,
        :country
      )
  end

  def locations_csv
    column_names = %w(id name initials inspections_enabled accepts_returns)
    array_column_names = %w(return_emails)
    CSV.generate do |csv|
      csv << column_names + array_column_names
      @pawn_locations.each do |location|
        csv << location.attributes.values_at(*column_names) + location.attributes.values_at(*array_column_names).map { |array| array.join(',') }
      end
    end
  end
end
