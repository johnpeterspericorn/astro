# encoding: utf-8
require 'column_conversions'
class Admin::FloorPlanBranchesController < AdminController
  include ColumnConversions

  def index
    @floor_plan_branches = floor_plan_company
                           .floor_plan_branches
                           .page(params[:page])
    @floor_plan_branches = PaginatingDecorator.new(@floor_plan_branches)
  end

  def new
    @floor_plan_branch = FloorPlanBranch.new
  end

  def create
    @floor_plan_branch = FloorPlanBranch.new(floor_plan_branch_params)
    if @floor_plan_branch.to_address(enable_address_verification_flag).valid?
      if @floor_plan_branch.save
        flash[:success] = 'Created floor plan branch'
        redirect_to admin_floor_plan_company_floor_plan_branches_path(floor_plan_company)
      else
        flash[:error] = @floor_plan_branch.errors.full_messages.to_sentence
        render :new
      end
    else
      flash[:error] = I18n.t('admin.floor_plan_branches.address.invalid')
      @floor_plan_branch.errors.add(:address, flash[:error])
      render :new
    end
  end

  def edit
    @floor_plan_branch = FloorPlanBranch.find(params[:id])
  end

  def update
    @floor_plan_branch = FloorPlanBranch.find(params[:id])
    if @floor_plan_branch.update_attributes(floor_plan_branch_params)
      flash[:success] = 'Updated floor plan branch'
      redirect_to admin_floor_plan_company_floor_plan_branches_path(floor_plan_company)
    else
      flash[:error] = @floor_plan_branch.errors.full_messages.to_sentence
      render :edit
    end
  end

  def fields
    {
      'Name'            => :admin_link,
      'Site no'         => :site_no,
      'Street Address'  => :address_street,
      'Suite'           => :address_suite,
      'Zipcode'         => :address_zipcode,
      'City'            => :address_city,
      'State'           => :address_state,
      'Company Code'    => :company_code,
      'Country'         => :country_name,
      'Active'          => :active
    }
  end

  helper_method :fields

  private

  def floor_plan_branch_params
    params
      .require(:floor_plan_branch)
      .permit(
        :name,
        :site_no,
        :branch_code,
        :address_street,
        :address_suite,
        :address_city,
        :address_state,
        :address_zipcode,
        :phone_number,
        :country_code,
        :company_code,
        :shipment_name,
        :oracle_site,
        :active
      )
  end

  def floor_plan_company_id
    params[:floor_plan_company_id]
  end

  def floor_plan_company
    @floor_plan_company ||= FloorPlanCompany.find(floor_plan_company_id)
  end
  helper_method :floor_plan_company

  def enable_address_verification_flag
    enable_verification = params[:enable_verification]
    enable_verification.present? ? value_to_boolean(enable_verification) : true
  end
end
