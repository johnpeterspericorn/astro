# encoding: utf-8
class Admin::FeatureFlagsController < AdminController
  before_action :authorize_feature_flag_editor

  def index
  end

  def create
    success = Settings.set_many(params[:settings])
    head success ? :ok : :internal_server_error
  end
end
