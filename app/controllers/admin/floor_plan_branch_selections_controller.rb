# encoding: utf-8
class Admin::FloorPlanBranchSelectionsController < AdminController
  def index
    latest_floor_plan_selection_ids = FloorPlanBranchSelection.group(:agreement_no).maximum(:id).values
    @agreement_floor_plan_list = FloorPlanBranchSelection.where(id: latest_floor_plan_selection_ids).order(:agreement_no).page(params[:page])
  end

  private

  def fields
    {
      'Agreement#'        => :agreement_no,
      'Company Name'   => :floor_plan_company_name,
      'Company Code'   => :company_code,
      'Branch Name'    => :floor_plan_branch_name,
      'Site No'        => :site_no
    }
  end
  helper_method :fields
end
