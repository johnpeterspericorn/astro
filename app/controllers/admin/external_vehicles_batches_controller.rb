# encoding: utf-8
module Admin
  class ExternalVehiclesBatchesController < ApplicationController
    def index
      authorize(policy.show?)

      batches = ExternalVehiclesBatch.joins(:external_vehicles_loads).unposted
      batches = batches.where(external_vehicles_loads: { vnum: filters.vnum }) if filters.vnum.present?
      batches = batches.where(external_vehicles_loads: { agreement_no: filters.agreement_no }) if filters.agreement_no.present?
      batches = batches.where(id: filters.batch_id) if filters.batch_id.present?
      batches = batches.where(user_id: filters.user_id) if filters.user_id.present?
      batches = batches.where(status: filters.status) if filters.status.present?

      @external_vehicle_batches = ExternalVehiclesBatch.includes(:user, :external_vehicles_loads).where(id: batches.map(&:id)).order(id: :desc).page(params[:page])
      @available_users = User.where(id: ExternalVehiclesBatch.pluck(:user_id)).map { |u| [u.username, u.id] }
    end

    def show
      @vehicle_creator = policy.create?
      @vehicle_approver = policy.approve?
      @batch = ExternalVehiclesBatch.find(batch_id)
      @external_vehicles = @batch.external_vehicles_loads
      @external_vehicles_documents = @batch.external_vehicles_documents
      agreements = @external_vehicles.pluck(:agreement_no).uniq
      @agreement_informations = AgreementInformation.agreement_informations_hash agreements

      # rubocop:disable Style/GuardClause
      if policy.show? && !policy.approve?
        @external_vehicles = PaginatingDecorator.new(@external_vehicles.page(params[:page]))
        render :readonly
      end
      # rubocop:enable Style/GuardClause
    end

    private

    def fields
      {
        I18n.t('external_vehicles_load.form.labels.vnum') => :vnum,
        I18n.t('external_vehicles_load.form.labels.model_year') => :year,
        I18n.t('external_vehicles_load.form.labels.make') => :make,
        I18n.t('external_vehicles_load.form.labels.model') => :model,
        I18n.t('external_vehicles_load.form.labels.odometer_reading') => :odometer_reading,
        I18n.t('external_vehicles_load.form.labels.agreement_no') => :agreement_no,
        I18n.t('external_vehicles_load.form.labels.vehicle_purchase_price') => :vehicle_purchase_price,
        I18n.t('external_vehicles_load.form.labels.buy_fee') => :buy_fee,
        I18n.t('external_vehicles_load.form.labels.vehicle_total') => :vehicle_total,
        I18n.t('external_vehicles_load.form.labels.location_initials') => :location_initials,
        I18n.t('external_vehicles_load.form.labels.automatic_purchase_email') => :automatic_purchase_email,
        I18n.t('external_vehicles_load.form.labels.purchased_at') => :formatted_purchased_at,
        I18n.t('external_vehicles_load.form.labels.seller_name') => :seller_name,
        I18n.t('external_vehicles_load.form.labels.rejection_comment') => :rejection_comment
      }
    end
    helper_method :fields

    def policy
      @policy ||= ExternalVehiclesLoadPolicy.new(current_user)
    end

    def batch_id
      params[:id]
    end

    def filters
      @filters = ::VehicleBatchFilters.new(params[:vehicle_batch_filters])
    end
    helper_method :filters
  end
end
