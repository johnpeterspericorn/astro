# encoding: utf-8
require 'column_conversions'

class Admin::PaymentReceiptEmailsController < AdminController
  include ColumnConversions

  def create
    if EmailValidation.valid?(user_emails)
      PaymentsMailer.receipt(user_emails, payment).deliver_later
      flash[:success] = t('payments.email.success')
      redirect_to admin_payments_path
    else
      flash[:error] = t('payments.email.failure')
      render :new
    end
  end

  private

  def user_emails
    arrayify(params[:emails])
  end

  def payment
    Payment.find(params[:payment_id])
  end
end
