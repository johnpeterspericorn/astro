# encoding: utf-8
class Admin::AgreementFloorPlanBranchLocksController < AdminController
  def index
    @agreement_floor_plan_branches = AgreementFloorPlanBranchLock.where(agreement_no: agreement_number).page(params[:page])
    @agreement_floor_plan_branches = PaginatingDecorator.new(@agreement_floor_plan_branches)
  end

  def new
    @agreement_floor_plan_branch = AgreementFloorPlanBranchLock.new(agreement_no: params[:agreement_number])
    @branches = []
  end

  def edit
    @agreement_floor_plan_branch = AgreementFloorPlanBranchLock.find(params[:id])
    floor_plan_details
  end

  def create
    @agreement_floor_plan_branch = AgreementFloorPlanBranchLock.new(agreement_floor_plan_branch_params)
    if @agreement_floor_plan_branch.save
      flash[:success] = 'Agreement Floorplan Lock created'
      redirect_to admin_agreement_floor_plan_branch_locks_path(agreement_number: @agreement_floor_plan_branch.agreement_no)
    else
      @branches = []
      flash[:error] = "Error creating Agreement Floorplan Lock: #{@agreement_floor_plan_branch.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def update
    @agreement_floor_plan_branch = AgreementFloorPlanBranchLock.find(params[:id])

    respond_to do |format|
      if @agreement_floor_plan_branch.update_attributes(agreement_floor_plan_branch_params)
        format.html { redirect_to admin_agreement_floor_plan_branch_locks_path(agreement_number: @agreement_floor_plan_branch.agreement_no), notice: 'Agreement Floorplan Lock updated' }
      else
        flash.now[:error] = @agreement_floor_plan_branch.errors.full_messages.to_sentence
        floor_plan_details
        format.html { render :edit }
      end
    end
  end

  def agreement_number
    params[:agreement_number]
  end
  helper_method :agreement_number

  def floor_plan_companies
    FloorPlanCompany.all.map { |company| ["#{company.company_code}-#{company.name}", "#{company.id}-#{company.company_code}"] }
  end
  helper_method :floor_plan_companies

  private

  def floor_plan_details
    company = @agreement_floor_plan_branch.floor_plan_company
    @branch = @agreement_floor_plan_branch.floor_plan_branch
    @branches = company.floor_plan_branches.map { |branch| ["#{branch.site_no}-#{branch.name}", branch.site_no] }
    @selected_floor_plan_company = "#{company.id}-#{company.company_code}"
    @selected_floor_plan_branch = @branch.site_no
  end

  def fields
    {
      'Agreement#'        => :agreement_no,
      'Company Name'   => :floor_plan_company_name,
      'Company Code'   => :company_code,
      'Branch Name'    => :floor_plan_branch_name,
      'Site No'        => :site_no,
      'Edit'           => :agreement_floor_plan_branch_lock_edit_link
    }
  end
  helper_method :fields

  def agreement_floor_plan_branch_params
    params
      .require(:agreement_floor_plan_branch_lock)
      .permit(:agreement_no, :company_code, :site_no)
  end
end
