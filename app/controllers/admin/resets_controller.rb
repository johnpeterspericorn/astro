# encoding: utf-8
class Admin::ResetsController < AdminController
  def create
    flat_rate_offer_batch = FlatRateOfferBatch.find(params[:flat_rate_offer_batch_id])

    if flat_rate_offer_batch.reset!
      flash[:success] = 'Successfully reset this batch.'
    else
      flash[:alert] = 'Could not reset this batch.'
    end

    redirect_to :back
  end
end
