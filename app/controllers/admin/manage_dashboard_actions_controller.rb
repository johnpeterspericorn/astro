# encoding: utf-8
require 'column_conversions'

module Admin
  class ManageDashboardActionsController < AdminController
    before_action :authorize_flat_rate_inquiry_viewer
    include ColumnConversions

    def new
      authorize(flat_rate_policy.new?)
      @agreement_no = agreement_no
      @agreementship_name = agreementship_name
      @decorated_action_types = decorated_actions
    rescue AgreementService::AgreementInformationMissingError
      redirect_to admin_dashboard_scores_path, notice: 'Please enter the agreement information for this agreement'
    end

    def create
      authorize(flat_rate_policy.new?)
      if action_type == 'parameter_change'
        flash.now[:notice] = 'Sorry you cannot select this at this time'
        @decorated_action_types = decorated_actions
        render :new, agreement_no: agreement_no, agreementship_name: agreementship_name
      elsif !emails_valid?
        flash.now[:notice] = 'The email address(es) entered is invalid'
        @decorated_action_types = decorated_actions
        render :new, agreement_no: agreement_no, agreementship_name: agreementship_name
      else
        redirect_to send(action_path_mappings[action_type.to_sym], agreement_no: agreement_no, emails: emails)
      end
    end

    private

    def action_type
      params[:action_type]
    end

    def agreement_no
      @agreement_no ||= params[:agreement_no]
    end

    def emails
      @emails ||= params[:emails]
    end

    def agreementship_name
      @agreementship_name ||= agreement_service.agreement_info.first.agreementship_name
    end

    def agreement_service
      @agreement_service ||= AgreementService.new(agreement_no)
    end

    def emails_valid?
      EmailValidation.valid?(arrayify(emails))
    end

    def flat_rate_policy
      @flat_rate_policy ||= FlatRatePolicy.new(current_user)
    end

    def decorated_actions
      action_types = ActionType.for_agreement_no(agreement_no).page(params[:page])
      PaginatingDecorator.new(action_types)
    end

    def action_path_mappings
      { 'network_plus_addition': 'new_admin_network_plus_offer_path', 'performance_alert': 'new_admin_performance_alert_path', 'rate_change': 'new_admin_rate_change_path' }
    end
  end
end
