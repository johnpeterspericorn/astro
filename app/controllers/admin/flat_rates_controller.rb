# encoding: utf-8
class Admin::FlatRatesController < AdminController
  def index
    @flat_rates = FlatRate.all
    if agreement_no_filter.blank?
      records_per_page = 1
    else
      @flat_rates = @flat_rates.where(agreement_no: agreement_no_filter)
      records_per_page = 10
    end
    @flat_rates = @flat_rates.page(params[:page]).per(records_per_page)
    @flat_rates = PaginatingDecorator.new(@flat_rates)
  end

  def new
    @flat_rate = FlatRate.new.decorate
  end

  def create
    @flat_rate = FlatRate.new(flat_rate_params).decorate

    if @flat_rate.save
      flash[:success] = 'Created Flat Rate'
      redirect_to admin_flat_rates_path
    else
      flash[:error] = "Error creating FlatRate: #{@flat_rate.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @flat_rate = FlatRate.find(params[:id])
  end

  def update
    @flat_rate = FlatRate.find(params[:id]).decorate

    if @flat_rate.update_attributes(flat_rate_params)
      flash[:success] = 'Successfully updated FlatRate'
      redirect_to admin_flat_rates_path
    else
      flash[:error] = "Error updating FlatRate: #{@flat_rate.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  private

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def fields
    {
      'Agreementship/Five Mil'    => :agreement_no,
      'Target Price 360 miles' => :target_price_360,
      'Target Price 360 PSI'   => :target_price_360_psi,
      'Target Price 500'       => :target_price_500,
      'Target Price 500 PSI'   => :target_price_500_psi,
      'Limited volume tier'    => :lv_tier,
      'Target price PALV'      => :target_price_lv,
      'Country'                => :country,
      'Edit'                   => :edit_flat_rate_link
    }
  end
  helper_method :fields

  def agreement_no_filter
    filters[:agreement_no].try(:strip)
  end
  helper_method :agreement_no_filter

  def flat_rate_params
    params
      .require(:flat_rate)
      .permit(:agreement_no, :inspection_adjustment_price, :days_adjustment_price, :miles_adjustment_price, :target_price_360,
              :target_price_360_psi, :target_price_500, :target_price_500_psi, :ineligibility_condition, :target_customer,
              :major_customer, :country, :lv_tier, :target_price_lv, :closed_factory_exclusion).merge(lv_tier: lv_tier_param)
  end

  def lv_tier_param
    params[:flat_rate][:lv_tier].blank? ? nil : params[:flat_rate][:lv_tier]
  end
end
