# encoding: utf-8
module Admin
  class PromisePurchaseVersionsController < AdminController
    def index
      @purchase_audit = PromisePurchaseAudit.new(params)
    end
  end
end
