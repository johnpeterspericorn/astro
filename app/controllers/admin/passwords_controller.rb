# encoding: utf-8
class Admin::PasswordsController < Devise::PasswordsController
  # since this controller is inheriting from Devise we need the mase
  # functionality as the AdminController to do this.
  layout 'admin'

  def after_sending_reset_password_instructions_path_for(_resource_name)
    admin_users_path
  end
end
