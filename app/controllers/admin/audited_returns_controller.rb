# encoding: utf-8
class Admin::AuditedReturnsController < AdminController
  def index
    @audited_returns = ReturnInvoice.joins(:audit)
                                    .order("audits.state='opened' DESC, id")
                                    .page(params[:page])
    @audited_returns = PaginatingDecorator.new(@audited_returns)
  end

  private

  def fields
    {
      'Edit'                              => :edit_return_link,
      'VRA#'                              => :vra,
      'Current State of the Audit'        => :audit_human_state_name,
      'Audit Resolution Date'             => :audit_resolved_by,
      'Edit Audit'                        => :edit_audit_link,
      'Clear Audit'                       => :clear_audit_link,
      'Flag Audit'                        => :flag_audit_link
    }
  end
  helper_method :fields
end
