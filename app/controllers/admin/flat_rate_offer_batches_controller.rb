# encoding: utf-8
module Admin
  class FlatRateOfferBatchesController < AdminController
    before_action :authorize_flat_rate_inquiry_viewer
    before_action :validate_sales_rep_email, only: :update

    PER_PAGE = 25

    def index
      offer_batches = FlatRateOfferBatch.all
      offer_batches = offer_batches.where(pawn_access_no: pawn_access_no_filter) if pawn_access_no_filter.present?
      offer_batches = offer_batches.joins(:flat_rate_offers).where(flat_rate_offers: { agreement_no: agreement_no_filter }) if agreement_no_filter.present?
      offer_batches = offer_batches.page(params[:page]).per(PER_PAGE)
      @flat_rate_offer_batches = PaginatingDecorator.new(offer_batches)
    end

    def edit
      @flat_rate_offer_batch = flat_rate_offer_batch.decorate
      authorize(flat_rate_offer_batch_policy.edit?)
    end

    def update
      authorize(flat_rate_offer_batch_policy.update?)
      buyer_information.update_attributes!(buyer_information_params)

      if flat_rate_offer_batch.update_attributes(flat_rate_offer_batch_params)
        flash[:success] = 'AgreementShield 360 batch updated!'
        redirect_to admin_flat_rate_offer_batches_path
      else
        flash[:error] = flat_rate_offer_batch.errors.full_messages.to_sentence
        redirect_to :back
      end
    end

    def destroy
      authorize(flat_rate_offer_batch_policy.destroy?)
      flat_rate_offer_batch.destroy
      flash[:success] = 'Deleted AgreementShield 360 Offer batch successfully.'
      redirect_to :back
    end

    private

    def filters
      params.fetch(:filters) { Hash.new }
    end
    helper_method :filters

    def pawn_access_no_filter
      filters[:pawn_access_no].try(:strip)
    end
    helper_method :pawn_access_no_filter

    def agreement_no_filter
      filters[:agreement_no].try(:strip)
    end
    helper_method :agreement_no_filter

    def validate_sales_rep_email
      email_validation = EmailValidation.new(sales_rep_email, required: true)
      redirect_to :back, notice: I18n.t('flat_rate_offer_batches.form.errors.invalid_sales_rep_email') unless email_validation.valid?
    end

    def flat_rate_offer_batch_policy
      @flat_rate_offer_batch_policy =
        FlatRateOfferBatchPolicy.new(current_user, flat_rate_offer_batch)
    end

    def buyer_information_params
      params
        .require(:buyer_information)
        .permit(:emails)
    end

    def flat_rate_offer_batch_params
      params
        .require(:flat_rate_offer_batch)
        .permit(:pawn_access_no, :expiration_date, :sales_rep_email)
    end

    def sales_rep_email
      params
        .require(:flat_rate_offer_batch)
        .fetch(:sales_rep_email)
    end

    def flat_rate_offer_batch
      @flat_rate_offer_batch ||= FlatRateOfferBatch.find(params[:id])
    end
    helper_method :flat_rate_offer_batch

    def buyer_information
      @buyer_information ||=
        BuyerInformation.build_with_defaults(flat_rate_offer_batch.pawn_access_no)
    end
    helper_method :buyer_information
  end
end
