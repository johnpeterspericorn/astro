# encoding: utf-8
module Admin
  class ExpiringReturnNotificationsController < AdminController
    def create
      ReturnInvoiceMailer.return_expiration_notification(return_invoice.object).deliver_later
      flash[:success] = "Return Notice sent for VRA# #{return_invoice.id}"
      redirect_to :back
    end

    private

    def return_invoice
      @return_invoice ||= ReturnInvoice.find(params[:return_invoice_id]).decorate
    end
  end
end
