# encoding: utf-8
class Admin::SoldVehiclesController < AdminController
  def index
    @sold_vehicles = SoldVehicle.page(params[:page])
    @sold_vehicles = @sold_vehicles.where(vnum: vnum_filter) if vnum_filter.present?
    @sold_vehicles = @sold_vehicles.where(agreement_no: agreement_number_filter) if agreement_number_filter.present?
    @sold_vehicles = SoldVehiclesDecorator.new(@sold_vehicles)
  end

  def edit
    @sold_vehicle = sold_vehicle.decorate
  end

  def update
    sold_vehicle.update_attributes(update_sold_vehicle_params)
    redirect_to admin_sold_vehicles_path
  end

  helper_method :fields

  private

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter

  def agreement_number_filter
    filters[:agreement_number]
  end
  helper_method :agreement_number_filter

  def sold_vehicle
    SoldVehicle.find(params[:id])
  end

  def update_sold_vehicle_params
    params.require(:sold_vehicle).permit(:automatic_purchase_processed, :ineligibility_condition)
  end
end
