# encoding: utf-8
module Admin
  class FlatRateOffersController < AdminController
    before_action :authorize_flat_rate_inquiry_viewer

    def index
      @flat_rate_offers = FlatRateOffer.all.page(params[:page])
    end

    def new
      authorize(flat_rate_policy.new?)
      @flat_rate_offer = FlatRateOffer.new(flat_rate_offer_batch: flat_rate_offer_batch)
    end

    def create
      authorize(flat_rate_policy.create?)
      @flat_rate_offer = FlatRateOffer.new(flat_rate_offer_params)

      if @flat_rate_offer.save
        flash[:success] = 'AgreementShield 360 offer created!'
        redirect_to admin_flat_rate_offer_batches_path
      else
        render :new
      end
    end

    def destroy
      authorize(flat_rate_policy.destroy?)
      flat_rate_offer = FlatRateOffer.find(params[:id])
      flat_rate_offer.destroy
      flash[:success] = 'Deleted AgreementShield 360 Offer successfully.'
      redirect_to :back
    end

    private

    def flat_rate_offer_params
      params
        .require(:flat_rate_offer)
        .permit(:flat_rate_id, :price, :miles_selected)
        .merge(
          flat_rate_offer_batch: flat_rate_offer_batch,
          offering_user:         current_user
        )
    end

    def flat_rate_policy
      @flat_rate_policy ||= FlatRatePolicy.new(current_user)
    end

    def flat_rate_offer_batch
      @flat_rate_offer_batch ||=
        FlatRateOfferBatch.find(params[:flat_rate_offer_batch_id])
    end
    helper_method :flat_rate_offer_batch

    def flat_rates
      @flat_rates ||=
        FlatRate.where(agreement_no: flat_rate_offer_batch.eligible_agreement_numbers)
    end
    helper_method :flat_rates
  end
end
