# encoding: utf-8
require 'csv'

class Admin::DashboardScoresController < AdminController
  respond_to :html, :csv

  def index
    @dashboard_scores = if agreement_number.present?
                          DashboardScore.where(buyer_num: agreement_number).order('id desc').page(params[:page])
                        else
                          DashboardScore.all.order('id desc').page(params[:page])
                        end

    @dashboard_scores = PaginatingDecorator.new(@dashboard_scores)
    if emails.present?
      report = EmailDashboardScoreReport.new(params)
      report.send_email
      flash[:notice] = t('dashboard_scores.email.notification')
    end

  rescue EmailDashboardScoreReport::EmailInvalidError
    flash[:error] = t('dashboard_scores.email.failure')
    render :index
  end

  private

  def fields
    {
      'Agreement Number'       => :buyer_num,
      'Payment Kind'        => :payment_kind,
      'Average Loss'        => :avg_loss_with_precision,
      'Return Rate'         => :return_rate_with_precision,
      'Volume'              => :volume_with_precision,
      'Margin'              => :margin_with_precision,
      'Earnings'            => :earnings_with_precision,
      'Return Score'        => :return_score_with_precision,
      'Purchase Quality'    => :purchase_quality,
      'Margin Score'        => :margin_score_with_precision,
      'Volume Score'        => :volume_score_with_precision,
      'Earnings Score'      => :earnings_score_with_precision,
      'Date'                => :date,
      ''                    => :manage_link
    }
  end
  helper_method :fields

  def agreement_number
    params[:dashboard_score][:agreement_no] if params[:dashboard_score].present?
  end
  helper_method :agreement_number

  def emails
    params[:dashboard_score][:emails] if params[:dashboard_score].present?
  end
end
