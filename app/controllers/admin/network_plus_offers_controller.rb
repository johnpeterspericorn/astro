# encoding: utf-8
module Admin
  class NetworkPlusOffersController < AdminController
    before_action :authorize_flat_rate_inquiry_viewer
    def new
      authorize(flat_rate_policy.new?)
      @network_plus_offer = NetworkPlusOffer.new
      @agreement_no = params['agreement_no']
      @emails = params['emails']
    end

    def create
      authorize(flat_rate_policy.new?)
      @network_plus_offer = NetworkPlusOffer.new(network_plus_offer_params)
      begin
        active_offer.check_for_existing_offer
      rescue NetworkPlusOfferService::NetworkPlusValidOfferExistsError
        flash[:notice] = 'The agreement already has an offer'
        redirect_to admin_network_plus_offer_approval_url(active_offer.valid_offer.first.id)
        return
      end
      if @network_plus_offer.save
        flash[:success] = 'Network Plus offer created!'
        @network_plus_offer.confirm!
        @network_plus_offer.send_email
        @network_plus_offer.capture_email_sent
        redirect_to admin_dashboard_scores_path
      else
        flash[:error] = display_errors
        render :new, agreement_no: agreement_no, emails: emails, expiration_date: expiration_date
      end
    end

    private

    def network_plus_offer_params
      params
        .require(:network_plus_offer)
        .permit(:emails, :existing_network_rate, :network_plus_surcharge, :expiration_date, :agreement_no)
        .merge!(
          offering_user: current_user
        )
    end

    def agreement_no
      @agreement_no ||= params[:network_plus_offer][:agreement_no]
    end

    def emails
      @emails ||= params[:network_plus_offer][:emails]
    end

    def expiration_date
      params_expiration_date || view_context.offset_from_now(NetworkPlusOffer::DEFAULT_EXPIRY_DAYS)
    end
    helper_method :expiration_date

    def params_expiration_date
      (params[:network_plus_offer].present? && params[:network_plus_offer][:expiration_date].present?) ? params[:network_plus_offer][:expiration_date] : nil
    end

    def display_errors
      @network_plus_offer.errors.map { |attribute, message| ('The ' + attribute.to_s + ' ' + message).humanize }.join('. ')
    end

    def active_offer
      NetworkPlusOfferService.new(agreement_no)
    end

    def flat_rate_policy
      @flat_rate_policy ||= FlatRatePolicy.new(current_user)
    end
  end
end
