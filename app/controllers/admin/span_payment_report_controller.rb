# encoding: utf-8
module Admin
  class SpanPaymentReportController < AdminController
    before_action :authorize_finance_sections_viewer

    def create
      report = EmailPaymentReport.new(params)
      report.send_email
      flash[:success] = t('payments.email.success')
      redirect_to admin_index_path
    rescue EmailPaymentReport::EmailInvalidError
      flash[:error] = t('payments.email.failure')
      render :new
    end
  end
end
