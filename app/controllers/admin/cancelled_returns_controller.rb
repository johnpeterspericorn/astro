# encoding: utf-8
class Admin::CancelledReturnsController < AdminController
  def index
    cancelled_returns = ReturnInvoice.only_deleted.order('updated_at DESC').page(params[:page])
    cancelled_returns = cancelled_returns.for_vnum_number(vnum_filter) if vnum_filter.present?
    cancelled_returns = cancelled_returns.for_vra_number(vra_filter) if vra_filter.present?
    @cancelled_returns = PaginatingDecorator.new(cancelled_returns)
  end

  private

  def fields
    {
      'VRA#'                              => :vra,
      'Return Location'                   => :return_location_initials,
      'VNUM'                               => :vnum,
      'Odometer on Return'                => :odometer_on_return,
      'Vehicle Price'                     => :vehicle_price,
      'Title Status'                      => :title_status_name,
      'Return Initiated At'               => :return_initiated_at,
      'Return Expiration Date'            => :expiration_date,
      'Return Initiated By'               => :returning_user_name,
      'Return Cancelled By'               => :cancelling_username,
      'Cancelled At'                      => :cancelled_at,
      'Edit Promise Offer'              => :edit_promise_offer_link,
      'Reinstate Return'                  => :reinstate_return_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter

  def vra_filter
    filters[:return_invoice_id]
  end
  helper_method :vra_filter
end
