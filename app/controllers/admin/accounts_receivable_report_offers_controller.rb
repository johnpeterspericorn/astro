# encoding: utf-8
class Admin::AccountsReceivableReportOffersController < AdminController
  before_action :authorize_finance_sections_viewer

  def index
    next_report_time = Time.current.beginning_of_week

    promise_offers = AccountsReceivablePromiseOfferQuery.new
                                                            .will_appear_on_accounts_receivable_report_at(next_report_time)
                                                            .map { |go| AccountsReceivableOfferDecorator.new(go) }

    cancelled_purchases = CancelledPurchase
                          .will_appear_on_accounts_receivable_report_at(next_report_time)
                          .map { |cp| AccountsReceivableCancelledPurchaseDecorator.decorate(cp) }

    @failed_offers = Kaminari.paginate_array(promise_offers + cancelled_purchases)
                             .page(params[:page])
  end

  def fields
    Hash[
      AccountsReceivableCsv.column_names.map { |col| [col.titleize, col] }
    ]
  end

  helper_method :fields
end
