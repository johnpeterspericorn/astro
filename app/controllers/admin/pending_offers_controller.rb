# encoding: utf-8
class Admin::PendingOffersController < AdminController
  def index
    respond_to do |fmt|
      fmt.html do
        promise_offers = PromiseOffer.pending.with_purchase
        promise_offers = promise_offers.by_vnum(vnum_filter) if vnum_filter.present?
        promise_offers = promise_offers.by_agreement_no(agreement_filter) if agreement_filter.present?
        promise_offers = promise_offers.by_location_initials(location_initials_filter) if location_initials_filter.present?
        @promise_offers = Kaminari.paginate_array(
          promise_offers.map { |go| AdminPendingOfferDecorator.new(go) }
        ).page(params[:page])
      end

      fmt.csv do
        @promise_offers = PromiseOffer
                            .post_pending
                            .includes(:vehicle_information, :pawn_information, :promise_purchase)

        pending_offer_csv = PendingOfferCsv.new(@promise_offers)
        render text: pending_offer_csv.to_s, content_type: 'text/csv'
      end
    end
  end

  def show
    @promise_offer = PromiseOffer.find(params[:id]).decorate
    @external_vehicle_batch = ExternalVehiclesBatch
                              .joins(:external_vehicles_loads)
                              .where(external_vehicles_loads: { vnum: @promise_offer.vnum }).first
  end

  def update
    promise_offer = PromiseOffer.find(params[:id])
    promise_offer.finalize_pending(current_user)

    redirect_to admin_pending_offers_path
  end

  def destroy
    promise_offer = PromiseOffer.find(params[:id])
    redirect_to new_promise_purchase_cancellation_path(promise_purchase_id: promise_offer.promise_purchase.id)
  end

  def fields
    {
      'Details'                   => :admin_pending_link,
      'Agreement #'                  => :agreement_no,
      'Pawn Access #'          => :pawn_access_no,
      'Badge #'                   => :badge_no,
      'Pawn Code'              => :universal_no,
      'VNUM'                       => :vnum,
      'Vehicle'                   => :vehicle_full_description,
      'Vehicle Price'             => :purchase_price,
      'Odometer Reading'          => :odometer_reading,
      'Days Selected'             => :days_selected,
      'Miles Selected'            => :miles_selected,
      'Promise Status'          => :promise_status,
      'Promise Price'           => :promise_price,
      'Last Updated'              => :updated_at,
      'Provided Email (optional)' => :emails,
      'Eligible for purchase?'    => :eligible_for_purchase?,
      'Pending Reason'            => :pending_reason_notice,
      'If Bid?'                   => :if_bid?,
      ''                          => :ods_update_link
    }
  end

  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter

  def location_initials_filter
    filters[:location_initials].try(:strip)
  end
  helper_method :location_initials_filter

  def agreement_filter
    filters[:agreement].try(:strip)
  end
  helper_method :agreement_filter
end
