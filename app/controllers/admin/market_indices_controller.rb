# encoding: utf-8
class Admin::MarketIndicesController < AdminController
  def show
    market_index
  end

  def edit
    market_index
  end

  def update
    if market_index.update(market_index_params)
      flash[:notice] = 'Market Index updated successfully'
    else
      flash[:notice] = market_index.errors.full_messages.to_sentence
    end
    redirect_to admin_market_index_path
  end

  private

  def market_index
    @market_index ||= MarketIndex.first_or_create
  end

  def market_index_params
    params.require(:market_index).permit(:value, :notes)
  end
end
