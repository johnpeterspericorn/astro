# encoding: utf-8
class Admin::NewProductFeesController < AdminController
  before_action :authorize_feature_flag_editor
  respond_to :html, :csv

  def index
    product_fees = NewProductFee.page(params[:page])
    @new_product_fees = PaginatingDecorator.new(product_fees)
    respond_with(@new_product_fees) do |format|
      format.html
      format.csv { render text: new_product_fees_csv }
    end
  end

  def new
    @new_product_fee = NewProductFee.new
  end

  def edit
    @new_product_fee = NewProductFee.find params[:id]
  end

  def create
    @new_product_fee = NewProductFee.new(new_product_fee_params)
    if @new_product_fee.save
      respond_with :admin, @new_product_fee, location: admin_new_product_fees_path
    else
      render :new
    end
  end

  def update
    @new_product_fee = NewProductFee.find params[:id]
    if @new_product_fee.update_attributes(new_product_fee_params)
      respond_with :admin, @new_product_fee, location: admin_new_product_fees_path
    else
      render :edit
    end
  end

  def destroy
    @new_product_fee = NewProductFee.find params[:id]
    @new_product_fee.destroy
    respond_with :admin, @new_product_fee, location: admin_new_product_fees_path
  end

  private

  def new_product_fee_params
    params
      .require(:new_product_fee)
      .permit(
        :agreement_no,
        :seller_no,
        :seller_paid,
        :timeframe,
        :count,
        :fee
      )
  end

  def new_product_fees_csv
    column_names = %w(id agreement_no seller_no seller_paid timeframe count fee)
    CSV.generate do |csv|
      csv << column_names
      @new_product_fees.each do |fee|
        csv << fee.attributes.values_at(*column_names)
      end
    end
  end
end
