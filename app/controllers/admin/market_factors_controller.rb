# encoding: utf-8
class Admin::MarketFactorsController < AdminController
  def new
    @market_factor = market_index.market_factors.build
  end

  def create
    @market_factor = market_index.market_factors.build(market_factor_params)
    if @market_factor.save
      flash[:notice] = 'Market Factor updated successfully'
    else
      flash[:notice] = @market_factor.errors.full_messages.to_sentance
    end
    redirect_to admin_market_index_path
  end

  def edit
    @market_factor = MarketFactor.find(params[:id])
  end

  def update
    @market_factor = MarketFactor.find(params[:id])
    if @market_factor.update(market_factor_params)
      flash[:notice] = 'Market Factor updated successfully'
    else
      flash[:notice] = @market_factor.errors.full_messages.to_sentance
    end
    redirect_to admin_market_index_path
  end

  def destroy
    @market_factor = market_index.market_factors.find(params[:id])
    if @market_factor.destroy
      flash[:notice] = 'Market Factor deleted successfully'
    end
    redirect_to admin_market_index_path
  end

  private

  def market_index
    @market_index ||= MarketIndex.first_or_create
  end

  def market_factor_params
    params.require(:market_factor).permit(:title, :description, :color)
  end
end
