# encoding: utf-8
module Admin
  class PaymentsController < AdminController
    respond_to :html, :csv

    before_action :authorize_finance_sections_viewer

    def index
      @decorated_payments = Payment.page(params[:page])
      @decorated_payments = @decorated_payments.for_vnum_number(vnum_filter) if vnum_filter.present?
      @decorated_payments = @decorated_payments.for_agreement_number(agreement_number_filter) if agreement_number_filter.present?
      @decorated_payments = PaginatingDecorator.new(@decorated_payments)

      respond_to do |format|
        format.html
        format.csv do
          payments = Payment.order('created_at DESC')
          render csv: PaymentsCsv.new(payments).to_s, filename: 'payments'
        end
      end
    end

    def fields
      {
        'Location'                           => :location_initials,
        'Sale Year'                          => :sale_year,
        'Lane #'                             => :lane_no,
        'Run #'                              => :run_no,
        'Days Selected'                      => :days_selected,
        'Miles Selected'                     => :miles_selected,
        'VNUM #'                              => :vnum,
        'Agreement#'                            => :agreement_no,
        'Item'                               => :item_description,
        'Unit Price'                         => :fee_amount,
        'Adjustment Code'                    => :adjustment_code,
        'Amount Charged'                     => :amount_charged,
        'Promise Price'                    => :promise_price,
        'ReturnFee' => :return_fee,
        'Order ID'                           => :order_id,
        'Credit Card Number (last 4 digits)' => :credit_card_number,
        'Credit Card Type'                   => :credit_card_type,
        'Auth Code'                          => :authorization_code,
        'Payment Date'                       => :created_at,
        'Cancelled?'                         => :cancelled?,
        'Error Message'                      => :error_message,
        ''                                   => :send_email_receipt
      }
    end
    helper_method :fields

    def filters
      params.fetch(:filters) { Hash.new }
    end
    helper_method :filters

    def vnum_filter
      filters[:vnum].try(:strip)
    end
    helper_method :vnum_filter

    def agreement_number_filter
      filters[:agreement_number]
    end
    helper_method :agreement_number_filter
  end
end
