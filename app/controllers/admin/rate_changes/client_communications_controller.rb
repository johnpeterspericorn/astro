# encoding: utf-8
require 'column_conversions'

module Admin
  module RateChanges
    class ClientCommunicationsController < ApplicationController
      include ColumnConversions

      def show
        rate_change.process_user(current_user) unless current_user.super_user?
        @rate_change = rate_change.decorate
      end

      private

      def rate_change
        @rate_change ||=
          RateChange.find(params[:rate_change_id])
      end
    end
  end
end
