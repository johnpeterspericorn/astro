# encoding: utf-8
class Admin::PromisePurchasesController < AdminController
  before_action :authorize_promise_admin_editor, only: [:expire_promise, :unexpire_promise]

  def index
    @active_purchases = PromisePurchase.page(params[:page])
    @active_purchases = @active_purchases.for_vnum_number(vnum_filter) if vnum_filter.present?
    @promise_offers = @active_purchases
                        .map(&:promise_offer)
                        .compact
                        .map { |go| AdminPurchasedOfferDecorator.new(go) }
  end

  def edit
    @promise_purchase = PurchasedPromiseForm.new(promise_purchase)
  end

  def update
    @promise_purchase = PurchasedPromiseForm.new(promise_purchase)

    if @promise_purchase.update(promise_purchase_params)
      flash[:success] = 'Promise purchase updated!'
      redirect_to admin_promise_purchases_path
    else
      flash[:error] = "Error updating Promise purchase: #{@promise_purchase.validation_errors.to_sentence}"
      render :edit
    end
  end

  def expire_promise
    promise_purchase.update_attributes(expire_promise: true)
    flash[:success] = 'Promise purchase expired!'
    redirect_to admin_promise_purchases_path
  end

  def unexpire_promise
    promise_purchase.update_attributes(expire_promise: false)
    flash[:success] = 'Promise purchase activated!'
    redirect_to admin_promise_purchases_path
  end

  private

  def fields
    {
      'Edit'                   => :edit_link,
      'Left lot?'              => :left_lot?,
      'Purchasing Username'    => :purchasing_username,
      'Location'               => :location_initials,
      'Sale'                   => :sale_no,
      'Lane'                   => :lane_no,
      'Run'                    => :run_no,
      'Description'            => :full_description,
      'VNUM'                    => :vnum,
      'Mileage'                => :odometer_reading,
      'Kind'                   => :kind,
      'DS Days'                => :days_selected,
      'DS Distance'            => :distance_selected_with_units,
      'DS Price'               => :promise_price,
      'Vehicle Purchase Price' => :vehicle_purchase_price,
      'Partner'                => :purchase_partner,
      'Email'                  => :emails,
      'Promised At'          => :promised_at,
      'Expire Promise'       => :expire_promise_link
    }
  end
  helper_method :fields

  def promise_purchase_params
    params
      .require(:purchased_promise_form)
      .permit(
        :promised_at, :year, :model, :make, :odometer_reading, :buy_fee,
        :pawn_access_no, :vehicle_purchase_price, :agreement_no, :emails,
        :transport_reimbursement, :promise_price, :kind, :days_selected,
        :miles_selected, :partner
      )
  end

  def promise_purchase
    PromisePurchase.find_by_id(params[:id]) || PromisePurchase.find_by_id(params[:promise_purchase_id])
  end

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter
end
