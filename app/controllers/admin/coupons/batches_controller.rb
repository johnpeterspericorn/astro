# encoding: utf-8
module Admin
  module Coupons
    class BatchesController < AdminController
      def new
      end

      def create
        batch = CouponBatch.new(batch_params)
        begin
          batch.save!
          coupon_count.times do
            coupon = Coupon.new_with_code(coupon_params)
            batch.coupons.create!(coupon.attributes)
          end
          redirect_to admin_coupons_path
        rescue ActiveRecord::RecordInvalid => e
          flash.now[:error] = "Coupon not created: #{e.message}"
          render :new
        end
      end

      private

      def batch_params
        params.require(:batch).permit(:name)
      end

      def coupon_params
        params.require(:coupon).permit(:amount, :expiration_date, :country)
      end

      def coupon_count
        params.fetch(:count).to_i
      end
    end
  end
end
