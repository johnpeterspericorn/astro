# encoding: utf-8
class Admin::AgreementInformationController < AdminController
  def edit
    @agreement_information = AgreementInformation.build_with_defaults(params[:pawn_access_no], params[:agreement_no])
    @agreement_information.save
  end

  def update
    agreement_information = AgreementInformation.find(params[:id])

    if agreement_information.update_attributes(agreement_information_params)
      flash[:success] = 'Successfully updated agreement information'
    else
      flash[:error] = "Edit failed: #{agreement_information.errors.to_a.to_sentence}"
    end

    redirect_to :back
  end

  private

  def agreement_information_params
    params[:agreement_information].permit(
      :address_street,
      :address_suite,
      :address_city,
      :address_state,
      :address_zipcode
    )
  end
end
