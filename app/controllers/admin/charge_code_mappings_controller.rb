# encoding: utf-8
class Admin::ChargeCodeMappingsController < ApplicationController
  respond_to :html, :csv

  def index
    @charge_code_mappings = ChargeCodeMapping.order('id desc').page(params[:page]).per(10)
    respond_with(@charge_code_mappings) do |format|
      format.html
      format.csv { render text: charge_code_mappings_csv }
    end
  end

  def new
    @charge_code_mapping = ChargeCodeMapping.new
  end

  def edit
    @charge_code_mapping = ChargeCodeMapping.find(params[:id])
  end

  def create
    @charge_code_mapping = ChargeCodeMapping.new(charge_code_mapping_params)
    if @charge_code_mapping.save
      respond_with :admin, @charge_code_mapping, location: admin_charge_code_mappings_path
    else
      render :new
    end
  end

  def update
    @charge_code_mapping = ChargeCodeMapping.find(params[:id])
    if @charge_code_mapping.update_attributes(charge_code_mapping_params)
      respond_with :admin, @charge_code_mapping, location: admin_charge_code_mappings_path
    else
      render :edit
    end
  end

  private

  def charge_code_mapping_params
    params
      .require(:charge_code_mapping)
      .permit(
        :product,
        :automatic_purchase,
        :seller_paid,
        :charge_code,
        :call_id,
        :percent_coverage,
        :limited_volume
      )
  end

  def charge_code_mappings_csv
    column_names = %w(id product automatic_purchase seller_paid charge_code call_id percent_coverage limited_volume)
    CSV.generate do |csv|
      csv << column_names
      ChargeCodeMapping.order('id desc').each do |fee|
        csv << fee.attributes.values_at(*column_names)
      end
    end
  end
end
