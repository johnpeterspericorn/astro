# encoding: utf-8
class Admin::PaymentsReportEmailsController < AdminController
  def update
    Settings.payments_report_email = params[:email]
    redirect_to :back, notice: 'Payments report email updated!'
  end
end
