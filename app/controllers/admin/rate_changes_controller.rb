# encoding: utf-8
module Admin
  class RateChangesController < AdminController
    before_action :authorize_flat_rate_inquiry_viewer
    def new
      authorize(flat_rate_policy.new?)
      @rate_change = RateChange.new
      @agreement_no = params['agreement_no']
      @emails = params['emails']
    end

    def create
      authorize(flat_rate_policy.new?)
      @rate_change = RateChange.new(rate_change_params)
      if @rate_change.save
        flash[:success] = 'Rate Change Email sent'
        redirect_to admin_dashboard_scores_path
      else
        flash[:error] = display_errors
        render :new, agreement_no: agreement_no, emails: emails, effective_date: effective_date
      end
    end

    private

    def rate_change_params
      params
        .require(:rate_change)
        .permit(:emails, :relationship_manager_id, :return_rate, :effective_date, :agreement_no)
        .merge!(
          rate_changing_user: current_user
        )
    end

    def display_errors
      @rate_change.errors.map { |attribute, message| ('The ' + attribute.to_s + ' ' + message).humanize }.join('. ')
    end

    def agreement_no
      @agreement_no ||= params[:rate_change][:agreement_no]
    end

    def emails
      @emails ||= params[:rate_change][:emails]
    end

    def return_rate
      @return_rate ||= params[:rate_change][:return_rate]
    end

    def effective_date
      params_effective_date || view_context.offset_from_now(RateChange::DEFAULT_EFFECTIVE_DATE_DAYS)
    end
    helper_method :effective_date

    def params_effective_date
      (params[:rate_change].present? && params[:rate_change][:effective_date].present?) ? params[:rate_change][:effective_date] : nil
    end

    def flat_rate_policy
      @flat_rate_policy ||= FlatRatePolicy.new(current_user)
    end
  end
end
