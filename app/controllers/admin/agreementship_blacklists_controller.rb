# encoding: utf-8
class Admin::AgreementshipBlacklistsController < AdminController
  def index
    @agreementship_blacklists = AgreementshipBlacklist.page(params[:page])
    @agreementship_blacklists = @agreementship_blacklists.where(agreement_no: agreement_number_filter) if agreement_number_filter.present?
    @agreementship_blacklists = PaginatingDecorator.new(@agreementship_blacklists)
  end

  def new
    @agreementship_blacklist =  AgreementshipBlacklist.new
  end

  def create
    @agreementship_blacklist = AgreementshipBlacklist.new(agreementship_blacklist_params)

    if @agreementship_blacklist.save
      flash[:success] = 'Agreementship Blacklist created'
      redirect_to admin_agreementship_blacklists_path
    else
      flash[:error] = "Error creating Agreementship Blacklist: #{@agreementship_blacklist.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @agreementship_blacklist = AgreementshipBlacklist.find(params[:id]).decorate
  end

  def update
    @agreementship_blacklist = AgreementshipBlacklist.find(params[:id]).decorate

    if @agreementship_blacklist.update_attributes(agreementship_blacklist_params)
      flash[:success] = 'Successfully updated Agreementship Blacklist'
      redirect_to admin_agreementship_blacklists_path
    else
      flash[:error] = "Error updating Agreementship Blacklist: #{@agreementship_blacklist.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    AgreementshipBlacklist.find(params[:id]).destroy
    redirect_to admin_agreementship_blacklists_path
  end

  private

  def fields
    {
      'Agreement#'                 => :agreement_no,
      'Expiration Date'         => :expiration_date,
      'Status'                  => :status,
      'Created At'              => :created_at,
      'Notes'                   => :notes,
      'Edit'                    => :edit_agreementship_blacklist_link,
      'Delete'                  => :delete_agreementship_blacklist_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def agreement_number_filter
    filters[:agreement_no].try(:strip)
  end
  helper_method :agreement_number_filter

  def agreementship_blacklist_params
    params
      .require(:agreementship_blacklist)
      .permit(:expiration_date, :agreement_no, :notes)
  end
end
