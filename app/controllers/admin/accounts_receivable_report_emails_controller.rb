# encoding: utf-8
class Admin::AccountsReceivableReportEmailsController < AdminController
  def show
  end

  def update
    Settings.accounts_receivable_report_emails = params.fetch(:emails)
    redirect_to :back, notice: 'Accounts receivable report emails updated!'
  end
end
