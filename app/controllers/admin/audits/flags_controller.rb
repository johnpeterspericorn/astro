# encoding: utf-8
require 'column_conversions'

class Admin::Audits::FlagsController < AdminController
  include ColumnConversions

  def new
    @audit = audit
  end

  def create
    @audit = audit

    if @audit.update_attributes(audit_params)
      @audit.flag!
      email_audit_flagged

      flash[:success] = 'Successfully updated audit'
      redirect_to admin_audited_returns_path
    else
      flash[:error] = "Edit failed: #{@audit.errors.to_a.to_sentence}"
      render :new
    end
  end

  private

  def audit_params
    params
      .fetch(:audit) { Hash.new }
      .permit(:resolved_by, :emails, reasons_attributes: [:id, :description, :_destroy])
  end

  def audit
    return_invoice.audit
  end

  def email_audit_flagged
    AuditMailer.notify_return_audit_flagged(return_invoice).deliver_later if email_confirmation?
  end

  def email_confirmation?
    value_to_boolean(params[:send_email])
  end

  def return_invoice
    @return_invoice ||= ReturnInvoice.find(params.fetch(:return_invoice_id))
  end
  helper_method :return_invoice

  def return_certificate
    return_invoice.return_certificate
  end
  helper_method :return_certificate
end
