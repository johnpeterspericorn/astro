# encoding: utf-8
class Admin::ApiUserTokensController < AdminController
  def create
    @api_user = User.find(params[:id])
    @api_user.reset_authentication_token!
    flash[:success] = "Auth token set to: #{@api_user.authentication_token}"
    redirect_to :back
  end
end
