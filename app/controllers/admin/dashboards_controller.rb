# encoding: utf-8
module Admin
  class DashboardsController < AdminController
    def show
      @sold_vehicles = SoldVehicle.order('id DESC').limit(10)
    end
  end
end
