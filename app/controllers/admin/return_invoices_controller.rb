# encoding: utf-8
require 'column_conversions'

class Admin::ReturnInvoicesController < AdminController
  include ColumnConversions

  def index
    @return_invoices = ReturnInvoice.order('updated_at DESC').page(params[:page])
    @return_invoices = @return_invoices.for_vnum_number(vnum_filter) if vnum_filter.present?
    @return_invoices = @return_invoices.for_vra_number(vra_filter) if vra_filter.present?
    @return_invoices = @return_invoices.for_agreement_number(agreement_no_filter) if agreement_no_filter.present?
    @return_invoices = PaginatingDecorator.new(@return_invoices)
  end

  def edit
    return_invoice
  end

  def update
    if return_invoice.update_attributes(invoice_params)
      update_floor_plan_branch_selection if floor_plan_branch_changed?
      notify_location_change if return_invoice.location_updated?
      email_updated_vra
      flash[:success] = 'Successfully updated return'
      redirect_to admin_return_invoices_path
    else
      flash[:error] = "Error updating return: #{return_invoice.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  private

  def fields
    {
      ''                                  => :edit_return_link,
      'VRA#'                              => :vra_link,
      'Return Location'                   => :return_location_initials,
      'VNUM'                               => :vnum,
      'Odometer on Return'                => :odometer_on_return,
      'Vehicle Received?'                 => :vehicle_received?,
      'Title Received?'                   => :title_received?,
      'Refund Processed?'                 => :refund_processed?,
      'Return Initiated At'               => :return_initiated_at,
      'Return Expiration Date'            => :expiration_date,
      'Return Initiated By'               => :returning_user_name,
      'Vehicle Price'                     => :vehicle_price,
      'Shipping Label'                    => :shipment_label_link,
      'Agreement#'                           => :agreement_no,
      'Edit Promise Offer'              => :edit_promise_offer_link,
      'Cancel Return'                     => :cancel_return_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def vnum_filter
    filters[:vnum].try(:strip)
  end
  helper_method :vnum_filter

  def vra_filter
    filters[:vra]
  end
  helper_method :vra_filter

  def agreement_no_filter
    filters[:agreement_no]
  end
  helper_method :agreement_no_filter

  def invoice_params
    params
      .require(:return_invoice)
      .permit(:mechanical_issues, :cosmetic_issues, :unable_to_sell, :changed_mind, :other, :no_answer,
              :additional_information, :estimated_cost, :odometer_on_return, :pawn_location_id,
              :title_status, :user_email, :vehicle_received, :title_received, :refund_processed, :expiration_date, :new_product_fee,
              bill_of_sale_attributes: [:seller_agreement_name, :seller_address, :seller_city, :seller_postal_code, :seller_phone, :odometer_reading, :price, :tax, :buying_sales_person_registration_no, :buying_hst_registration_no, :selling_sales_person_registration_no, :selling_hst_registration_no, :id])
  end

  def email_updated_vra
    ReturnInvoiceMailer.return_confirmation(return_invoice.object).deliver_later if return_invoice.location_updated? || email_confirmation?
  end

  def email_confirmation?
    value_to_boolean(params[:send_return_confirmation])
  end

  def return_invoice
    @return_invoice ||= ReturnInvoice.find(params[:id]).decorate
  end

  def floor_plan_branch_id
    params[:floor_plan_branch_id]
  end

  def floor_plan_company_id
    params[:floor_plan_company_id]
  end

  def floor_plan_company
    @floor_plan_company ||= FloorPlanCompany.find(floor_plan_company_id)
  end

  def floor_plan_branch
    @floor_plan_branch ||= return_invoice.floor_plan_branch
  end
  helper_method :floor_plan_branch

  def floor_plan_company_options
    FloorPlanCompany.pluck(:name, :id)
  end
  helper_method :floor_plan_company_options

  def floor_plan_branch_options
    floor_plan_company = return_invoice.floor_plan_branch.floor_plan_company
    options = floor_plan_company.floor_plan_branches.map do |branch|
      [branch.name, branch.id]
    end
    options
  end
  helper_method :floor_plan_branch_options

  def floor_plan_branch_changed?
    floor_plan_branch_id.present? &&
      return_invoice.floor_plan_branch.id != floor_plan_branch_id
  end

  def update_floor_plan_branch_selection
    branch_selection = FloorPlanBranchSelection.where(return_invoice_id: return_invoice.id).first
    new_floor_plan_branch = FloorPlanBranch.find(floor_plan_branch_id)
    branch_selection.update_attributes(site_no: new_floor_plan_branch.site_no, company_code: floor_plan_company.company_code)
  end

  def notify_location_change
    return_invoice.update_drop_notice
    ReturnInvoiceMailer.location_update_notification(return_invoice.object).deliver_later
  end
end
