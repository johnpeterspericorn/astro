# encoding: utf-8
require 'column_conversions'

module Admin
  module NetworkPlusOffers
    class ApprovalsController < ApplicationController
      include ColumnConversions

      def show
        @network_plus_offer = network_plus_offer.decorate
      end

      def create
        if network_plus_offer_approval.approve
          flash[:success] = I18n.t('admin.network_plus_offer.approval.accepted')
        else
          flash[:error] = I18n.t('admin.network_plus_offer.approval.not_accepted')
        end

        redirect_to root_path
      end

      def destroy
        if network_plus_offer_approval.reject
          flash[:success] = I18n.t('admin.network_plus_offer.approval.rejected')
        else
          flash[:error] = I18n.t('admin.network_plus_offer.approval.not_rejected')
        end

        redirect_to root_path
      end

      private

      def network_plus_offer
        @network_plus_offer ||=
          NetworkPlusOffer.find(params[:network_plus_offer_id])
      end

      def network_plus_offer_approval
        @network_plus_offer_batch_approval ||=
          NetworkPlusOfferApproval.new(network_plus_offer, current_user, network_plus_offer.effective_date)
      end
    end
  end
end
