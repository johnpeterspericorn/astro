# encoding: utf-8
class Admin::FailoversController < AdminController
  def index
  end

  def create
    Failover.begin
    redirect_to :back
  end

  def update
    Failover.terminate
    redirect_to :back
  end
end
