# encoding: utf-8
module Admin
  class PerformanceAlertsController < AdminController
    include ColumnConversions

    before_action :authorize_flat_rate_inquiry_viewer
    def new
      authorize(flat_rate_policy.new?)
      @performance_alert = PerformanceAlert.new
      @agreement_no = params['agreement_no']
      @emails = params['emails']
    end

    def create
      authorize(flat_rate_policy.new?)
      @performance_alert = PerformanceAlert.new(performance_alert_params)
      if @performance_alert.save
        flash[:success] = 'Performance Alert Email sent'
        redirect_to admin_dashboard_scores_path
      else
        flash[:error] = display_errors
        render :new, agreement_no: agreement_no, emails: emails, follow_up_date: follow_up_date
      end
    end

    private

    def performance_alert_params
      params
        .require(:performance_alert)
        .permit(:emails, :relationship_manager_id, :return_rate, :lpc, :follow_up_date, :agreement_no)
        .merge!(
          current_user: current_user
        )
    end

    def display_errors
      @performance_alert.errors.map { |attribute, message| ('The ' + attribute.to_s + ' ' + message).humanize }.join('. ')
    end

    def agreement_no
      @agreement_no ||= params[:performance_alert][:agreement_no]
    end

    def emails
      @emails ||= params[:performance_alert][:emails]
    end

    def return_rate
      @return_rate ||= params[:performance_alert][:return_rate]
    end

    def lpc
      @lpc ||= params[:performance_alert][:lpc]
    end

    def follow_up_date
      params_follow_up_date || view_context.offset_from_now(PerformanceAlert::DEFAULT_FOLLOW_UP_DAYS)
    end
    helper_method :follow_up_date

    def params_follow_up_date
      params[:performance_alert].present? && params[:performance_alert][:follow_up_date].present? ? params[:performance_alert][:follow_up_date] : nil
    end

    def flat_rate_policy
      @flat_rate_policy ||= FlatRatePolicy.new(current_user)
    end
  end
end
