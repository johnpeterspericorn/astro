# encoding: utf-8
require 'csv'

class Admin::UsersController < AdminController
  before_action :authorize_user_admin_editor
  respond_to :html, :csv

  def index
    search_params = params['/missioncontrol/users'] || {}
    query = search_params.fetch(:search) { '' }
    @users = User.combined_search(query).page(params[:page])
    respond_with(@users) do |format|
      format.html
      format.csv { render text: users_csv }
    end
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      @user.send_reset_password_instructions if @user.super_user?
      redirect_to admin_user_path(@user)
    else
      flash[:error] = @user.errors.messages.values.flatten.to_sentence
      render :new
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      redirect_to admin_user_path(@user)
    else
      flash[:error] = @user.errors.messages.values.flatten.to_sentence
      render :edit
    end
  end

  def show
    @user = User.find(params[:id])
  end

  private

  def users_csv
    column_names = %w(username email location_initials active)
    CSV.generate do |csv|
      csv << column_names
      @users.each do |user|
        csv << user.attributes.values_at(*column_names).map { |attribute| attribute.blank? ? '' : attribute }
      end
    end
  end

  def user_params
    params
      .require(:user)
      .permit(
        :username,
        :email,
        :location_initials,
        :active,
        *User::ROLES
      )
  end

  def filters
    params.fetch(:filters) { Hash.new }
  end

  # By default, Rails helper method adds a hidden field before a multiple select.
  # Referece commit: https://github.com/rails/rails/commit/faba406fa15251cdc9588364d23c687a14ed6885
  def filtered_roles
    filters
      .fetch(:roles) { [] }
      .reject(&:empty?)
  end
end
