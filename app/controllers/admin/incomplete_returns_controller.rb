# encoding: utf-8
class Admin::IncompleteReturnsController < AdminController
  def index
    @return_invoices = ReturnInvoice
                       .incomplete
                       .order('created_at DESC')
                       .page(params[:page])
  end

  def update
    return_invoice = ReturnInvoice.find(params[:id])

    return_invoice.finalize(params.slice(:skip_label))

    flash[:success] = t('admin.incomplete_returns.success')
    redirect_to :back
  end
end
