# encoding: utf-8
require 'csv'
require 'column_conversions'

class Admin::CouponsController < AdminController
  include ColumnConversions
  respond_to :html, :csv

  def index
    filtered_coupons = Coupon.unscoped
    filtered_coupons = filtered_coupons.for_batch_name(name_filter) if name_filter.present?
    filtered_coupons = filtered_coupons.for_status(value_to_boolean(status_filter)) if status_filter.present?
    @coupons = filtered_coupons.page(params[:page])

    respond_to do |format|
      format.html do
        @coupons = PaginatingDecorator.new(@coupons)
      end
      format.csv { render text: coupons_csv(filtered_coupons.decorate) }
    end
  end

  def new
    @coupon = Coupon.new
  end

  def edit
    @coupon = Coupon.find(params[:id])
  end

  def create
    @coupon = Coupon.new_with_code(coupon_params)

    respond_to do |format|
      if @coupon.save
        format.html { redirect_to admin_coupon_path(@coupon), notice: I18n.t('admin.coupons.notice.create_message') }
      else
        flash.now[:error] = @coupon.errors.full_messages.to_sentence
        format.html { render :new }
      end
    end
  end

  def show
    @coupon = Coupon.find(params[:id])
  end

  def update
    @coupon = Coupon.find(params[:id])

    respond_to do |format|
      if @coupon.update_attributes(coupon_params)
        format.html { redirect_to admin_coupon_path(@coupon), notice: I18n.t('admin.coupons.notice.update_message') }
      else
        flash.now[:error] = @coupon.errors.full_messages.to_sentence
        format.html { render :edit }
      end
    end
  end

  def destroy
    @coupon = Coupon.find(params[:id])
    @coupon.destroy
    flash[:success] = I18n.t('admin.coupons.notice.delete_message')
    redirect_to admin_coupons_path
  end

  private

  def fields
    {
      'Coupon Code'         => :code,
      'Batch Name'          => :name,
      'ID'                  => :id,
      'Amount'              => :amount,
      'Expiration Date'     => :expiration_date,
      'Edit'                => :edit_coupon_link,
      'Delete'              => :delete_coupon_link,
      'Show'                => :show_coupon_link,
      'Status'              => :status,
      'Country'             => :country
    }
  end
  helper_method :fields

  def coupon_params
    params.require(:coupon).permit(:amount, :expiration_date, :country)
  end

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def name_filter
    filters[:name].try(:strip)
  end
  helper_method :name_filter

  def status_filter
    filters[:status]
  end
  helper_method :status_filter

  def coupons_csv(coupons)
    column_names = %w(code name id amount expiration_date status)
    CSV.generate do |csv|
      csv << column_names
      coupons.each do |coupon|
        csv << column_names.map { |attr| coupon.public_send(attr) }
      end
    end
  end
end
