# encoding: utf-8
class Admin::PawnAccessNumberBlacklistsController < AdminController
  def index
    @pawn_access_number_blacklists = PawnAccessNumberBlacklist.page(params[:page])
    @pawn_access_number_blacklists = @pawn_access_number_blacklists.where(pawn_access_no: pawn_access_number_filter) if pawn_access_number_filter.present?
    @pawn_access_number_blacklists = PaginatingDecorator.new(@pawn_access_number_blacklists)
  end

  def new
    @pawn_access_number_blacklist =  PawnAccessNumberBlacklist.new
  end

  def create
    @pawn_access_number_blacklist = PawnAccessNumberBlacklist.new(pawn_access_number_blacklist_params)

    if @pawn_access_number_blacklist.save
      flash[:success] = 'Pawn Access Number Blacklist created'
      redirect_to admin_pawn_access_number_blacklists_path
    else
      flash[:error] = "Error creating Pawn Access Number Blacklist: #{@pawn_access_number_blacklist.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @pawn_access_number_blacklist = PawnAccessNumberBlacklist.find(params[:id]).decorate
  end

  def update
    @pawn_access_number_blacklist = PawnAccessNumberBlacklist.find(params[:id]).decorate

    if @pawn_access_number_blacklist.update_attributes(pawn_access_number_blacklist_params)
      flash[:success] = 'Successfully updated Pawn Access Number Blacklist'
      redirect_to admin_pawn_access_number_blacklists_path
    else
      flash[:error] = "Error updating Pawn Access Number Blacklist: #{@pawn_access_number_blacklist.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    PawnAccessNumberBlacklist.find(params[:id]).destroy
    redirect_to admin_pawn_access_number_blacklists_path
  end

  private

  def fields
    {
      'Pawn Access#'         => :pawn_access_no,
      'Expiration Date'         => :expiration_date,
      'Status'                  => :status,
      'Created At'              => :created_at,
      'Notes'                   => :notes,
      'Edit'                    => :edit_pawn_access_number_blacklist_link,
      'Delete'                  => :delete_pawn_access_number_blacklist_link
    }
  end
  helper_method :fields

  def filters
    params.fetch(:filters) { Hash.new }
  end
  helper_method :filters

  def pawn_access_number_filter
    filters[:pawn_access_no].try(:strip)
  end
  helper_method :pawn_access_number_filter

  def pawn_access_number_blacklist_params
    params
      .require(:pawn_access_number_blacklist)
      .permit(:expiration_date, :pawn_access_no, :notes)
  end
end
