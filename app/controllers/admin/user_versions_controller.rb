# encoding: utf-8
module Admin
  class UserVersionsController < AdminController
    def index
      if user_id
        @versions = UserVersion.where(item_id: user_id)
        @versions = @versions.initiated_at(date_from..date_to) if date_from.present? && date_to.present?
        @versions = @versions.order('id desc').page(params[:page]).per(20)
      else
        @versions = UserVersion.order('id desc').page(params[:page]).per(20)
      end
    end

    def filter_params
      params.fetch(:filters, {})
            .permit(:date_from, :date_to, :user_id)
    end

    def user_id
      filter_params[:user_id] || params[:user_id]
    end
    helper_method :user_id

    def date_from
      filter_params[:date_from]
    end
    helper_method :date_from

    def date_to
      filter_params[:date_to]
    end
    helper_method :date_to
  end
end
