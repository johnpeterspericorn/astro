# encoding: utf-8
class Admin::PromiseExtensionsController < AdminController
  before_action :authorize_promise_admin_editor

  def index
    extensions = PromiseExtension.all.page(params[:page])
    @promise_extensions = PaginatingDecorator.new(extensions)
  end

  def import
    if file_params.present?
      PromiseExtension.import_from_csv(file_params[:extensions_file])
      redirect_to admin_promise_extensions_path, notice: 'File imported.'
    else
      redirect_to admin_promise_extensions_path, alert: 'Please select a file to upload.'
    end
  end

  private

  def fields
    {
      'VNUM'               => :vnum,
      'Extension Days'    => :additional_days,
      'Extension Reason'  => :reason,
      'Created At'        => :created_at
    }
  end
  helper_method :fields

  def file_params
    params.require(:import).permit(:extensions_file)
  end
end
