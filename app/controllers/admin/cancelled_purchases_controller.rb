# encoding: utf-8
module Admin
  class CancelledPurchasesController < AdminController
    def index
      @cancelled_purchases = if vnum_filter.blank?
                               CancelledPurchase.all
                             else
                               CancelledPurchase.where(vnum: vnum_filter)
                             end
      @cancelled_purchases = @cancelled_purchases.page(params[:page])
      @cancelled_purchases = PaginatingDecorator.new(@cancelled_purchases)
    end

    private

    def fields
      {
        'Reason'              => :reason,
        'Buyer Email'         => :buyer_email,
        'Cancelling Username' => :cancelling_username,
        'Pawn Access #'    => :pawn_access_no,
        'Agreement #'            => :agreement_no,
        'VNUM'                 => :vnum,
        'Universal #'         => :universal_no,
        'Days Selected'       => :promise_days_selected,
        'Miles Selected'      => :promise_miles_selected,
        'If Bid?'             => :promise_if_bid?,
        'Seller Paid?'        => :promise_seller_paid?,
        'Automatic Purchase?' => :automatic_purchase?,
        'Vehicle Price'       => :vehicle_purchase_price,
        'Promise Price'     => :promise_price,
        'Inspection Price'    => :inspection_price,
        'Transaction ID'      => :transaction_id,
        'Purchase Date'       => :purchased_at,
        'Cancellation Date'   => :created_at
      }
    end
    helper_method :fields

    def filters
      params.fetch(:filters) { Hash.new }
    end
    helper_method :filters

    def vnum_filter
      filters[:vnum].try(:strip)
    end
    helper_method :vnum_filter
  end
end
