# encoding: utf-8
class Admin::UnpurchasedOffersController < AdminController
  def edit
    @unpurchased_offer = PromiseOffer.find(params[:id])
    redirect_to admin_promise_offers_path if @unpurchased_offer.promised?
  end

  def update
    unpurchased_offer = PromiseOffer.find(params[:id])

    if editable?
      if unpurchased_offer.update_attributes(offer_params)
        flash[:success] = 'Successfully updated offer'
      else
        flash[:error] = "Error updating offer: #{unpurchased_offer.errors.full_messages.to_sentence}"
      end
    else
      flash[:error] = "You do not have sufficient rights to update offer: #{unpurchased_offer.errors.full_messages.to_sentence}"
    end
    redirect_to :back
  end

  private

  def offer_params
    params
      .require(:promise_offer)
      .permit(
        :must_be_promised_at,
        :bypass_charge,
        :left_lot,
        :promised_at,
        :ineligibility_condition,
        :pending_reason_code,
        :promise_250,
        :promise_500,
        :additional_day,
        vehicle_information_attributes: [:odometer_reading, :id, :make, :model, :year],
        pawn_information_attributes: [:id, :agreement_no, :pawn_access_no],
        discount_attributes: [:amount, :description]
      )
  end

  def admin_policy
    @admin_policy ||= AdminPolicy.new(current_user)
  end

  def editable?
    admin_policy.authorized_to_edit_promise_admin?
  end
  helper_method :editable?
end
