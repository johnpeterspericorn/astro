# encoding: utf-8
require 'column_conversions'

class FlatRateOfferBatchesController < ApplicationController
  include ColumnConversions
  before_action :require_flat_rate_offers, only: [:new]

  def new
    authorize(flat_rate_policy.new?)

    @flat_rate_offer_batch = FlatRateOfferBatch.new(
      pawn_access_no: pawn_access_number,
      flat_rate_offers_attributes: flat_rate_offers_attributes
    ).decorate

    return if @flat_rate_offer_batch.save
    flash[:error] = @flat_rate_offer_batch.errors.messages.values.flatten.to_sentence
    redirect_to :back
  end

  def show
    @flat_rate_offer_batch = flat_rate_offer_batch.decorate
    authorize(flat_rate_offer_batch_policy.show?)
  end

  def edit
    @flat_rate_offer_batch = flat_rate_offer_batch.decorate
    authorize(flat_rate_offer_batch_policy.edit?)
  end

  def update
    authorize(flat_rate_policy.update?)

    buyer_information.update_attributes!(buyer_information_params)
    flat_rate_offer_batch.update_attributes(user_email: user_email, sales_rep_email: sales_rep_email)
    flash[:notice] = if flat_rate_offer_batch.confirmed?
                       I18n.t('flat_rates.errors.already_exists')
                     else
                       flat_rate_offer_batch.confirm!
                       I18n.t('flat_rates.confirmed')
                     end
    redirect_to root_path
  end

  def validate_sales_rep_email
    email_validation = EmailValidation.new(params[:sales_rep_email], required: true)
    render text: email_validation.valid? ? true : I18n.t('flat_rate_offer_batches.form.errors.invalid_sales_rep_email')
  end

  def destroy
    authorize(flat_rate_offer_batch_policy.destroy?)
    flat_rate_offer_batch.destroy!
    flash[:notice] = I18n.t('flat_rates.destroyed')
    redirect_to flat_rates_agreement_number_list_path
  end

  private

  def flat_rate_offers_attributes
    params
      .require(:flat_rate_offer)
      .map { |attrs| attrs.permit(:flat_rate_id, :price, :miles_selected, :psi_eligible, :palv).merge(offering_user: current_user) }
  end

  def flat_rate_offer_batch
    @flat_rate_offer_batch ||= FlatRateOfferBatch.find(params[:id])
  end

  def flat_rate_offer_batch_policy
    @flat_rate_offer_batch_policy =
      FlatRateOfferBatchPolicy.new(current_user, flat_rate_offer_batch)
  end

  def flat_rate_policy
    @flat_rate_policy ||= FlatRatePolicy.new(current_user)
  end

  def pawn_access_number
    if params[:id].present?
      flat_rate_offer_batch.pawn_access_no
    else
      params.fetch(:pawn_access_number)
    end
  end
  helper_method :pawn_access_number

  def sales_rep_email
    params.require(:flat_rate_offer_batch).fetch(:sales_rep_email)
  end

  def buyer_information_params
    params
      .require(:buyer_information)
      .permit(:name, :emails, :cell_phone, :office_phone)
  end

  def buyer_information
    @buyer_information ||= BuyerInformation.build_with_defaults(pawn_access_number)
  end
  helper_method :buyer_information

  def user_email
    params.fetch(:user_email)
  end

  def require_flat_rate_offers
    redirect_to :back, notice: I18n.t('flat_rates.errors.confirm_selection') if !params[:flat_rate_offer].present? || flat_rate_offers_attributes.all? { |attrs| attrs[:price].blank? }
  end
end
