# encoding: utf-8
class DashboardFiltersController < ApplicationController
  def show
    redirect_to dashboard_redirect_url(
      pawn_access_number: current_user.pawn_access_number,
      next: navigate_to
    ) if current_user.buyer_user?
  end

  def edit
    @partial = params[:filter_type] if FILTER_TYPES.include?(params[:filter_type])
  end

  def update
    redirect_to dashboard_redirect_url(dashboard_filter_params)
  end

  private

  FILTER_TYPES = %w(pawn_access_number agreement_number).freeze

  def navigate_to
    params[:next]
  end
  helper_method :navigate_to

  def dashboard_redirect_url(options = {})
    agreement_params = options.slice(:agreement_number, :pawn_access_number)
    pawn_access_number = params[:pawn_access_number] || agreement_params[:pawn_access_number]

    if pawn_access_number.blank? && params[:agreement_number].blank?
      flash[:notice] = 'Please provide an pawn access number or a agreement number.'
      current_user.buyer_user? ? root_path : dashboard_filter_path(dashboard_filter_params)
    elsif !AgreementDashboardPolicy.new(current_user, pawn_access_number).show?
      flash[:error] = "You are not authorized to access the AA# #{pawn_access_number}."
      current_user.buyer_user? ? root_path : dashboard_filter_path(dashboard_filter_params)
    elsif options[:next] == 'agreement_dashboard' || options[:next] == 'performance'
      # agreement_dashboard_path(agreement_params)
      market_index_params = '&market_index=1' if Settings.market_index_enabled
      agreement_dashboard_path + "#/agreement_dashboard?#{agreement_params.first[0]}=#{agreement_params.first[1]}#{market_index_params}"
    else
      root_path
    end
  end

  def dashboard_filter_params
    params.permit([:next, :pawn_access_number, :agreement_number])
  end
end
