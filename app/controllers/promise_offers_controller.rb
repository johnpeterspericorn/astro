# encoding: utf-8
class PromiseOffersController < ApplicationController
  before_action :ensure_authorized

  def index
    session.delete(:pawn_access_number) if filter_query_data.filtered_by?(:badge_number)

    if filter_query_data.filter_method.present?
      record_vehicles
      ensure_before_cutoff!

      @current_offer_count = current_offers.length
      @search_criteria = PromiseOfferDecorator.search_criteria(filter_query_data)
      @agreementships = AgreementOptionList.new(current_offers).select_options

      respond_to do |format|
        format.html
        format.json { render json: json_body }
      end
    else
      redirect_to edit_vehicle_filter_path(filter_type: :pawn_access_number), alert: I18n.t('promise_offers.missing_pawn_access_number_message')
    end
  end

  private

  def ensure_before_cutoff!
    filtered_offers.each do |offer|
      if offer.past_cutoff? && offer.eligible?
        offer.update_attribute(:ineligibility_condition, IneligibilityCondition::PAST_PURCHASE_TIME)
      end
    end
  end

  def authorized?
    !unauthorized?
  end

  def unauthorized?
    filter_query_data.filtered_by?(:pawn_access_number) && !selection_policy.authorized_for_index?(params[:pawn_access_number])
  end

  def ensure_authorized
    return if authorized?
    if current_user.buyer_user?
      render nothing: true, status: :forbidden
    else
      redirect_to edit_vehicle_filter_path(filter_type: :pawn_access_number), alert: unauthorized_alert_message
    end
  end

  def unauthorized_alert_message
    if selection_policy.restricted_pawn_access_number?(params[:pawn_access_number])
      I18n.t('promise_offers.invalid_pawn_access_number')
    else
      I18n.t('promise_offers.missing_pawn_access_number_message')
    end
  end

  def selection_policy
    @selection_policy ||= PromiseSelectionPolicy.new(current_user)
  end

  def record_vehicles
    vehicles = filter_query_data.vehicles_for_display
    PromiseOfferRecorder.record_offers(vehicles)
  end

  def eligible_offers
    @eligible_offers ||= EligiblePromiseOfferQuery.new(filtered_offers).by_user(current_user)
                                                    .tap { |offers| offers.each(&:update_ods_attributes) }
  end

  def new_offers
    @new_offers ||= eligible_offers.without_purchase
                                   .map { |offer| PromiseOfferFormPresenter.new(offer.decorate, current_user) }
  end
  helper_method :new_offers

  def previously_purchased_offers
    @previously_purchased_offers ||= eligible_offers.with_purchase
                                                    .map { |offer| PromiseOfferFormPresenter.new(offer.decorate, current_user) }
  end
  helper_method :previously_purchased_offers

  def ineligible_offers
    @ineligible_offers ||= IneligiblePromiseOfferQuery.new(filtered_offers).by_user(current_user)
                                                        .map { |offer| PromiseOfferFormPresenter.new(offer.decorate, current_user) }
  end
  helper_method :ineligible_offers

  def filtered_offers
    offers = ErrorCorrectingPromiseOfferQuery.new
                                               .by_filter_query_data(filter_query_data)
                                               .created_today
    offers = offers.by_location_initials(params[:location_initials]) if params[:location_initials].present?
    offers
  end

  def current_offers
    @current_offers ||= new_offers + previously_purchased_offers + ineligible_offers
  end

  def filter_query_data
    @filter_query_data ||= FilterQueryData.new(massage_params_for_filter_query_data)
  end

  def massage_params_for_filter_query_data
    query_params = params.dup
    query_params.delete(:agreement_numbers) # do not allow users to pass their own list
    query_params[:buyer_authorization] = buyer_authorization

    if query_params[:pawn_access_number]
      agreement_numbers = buyer_authorization.get_agreement_numbers
      query_params[:agreement_numbers] = agreement_numbers
    end
    query_params
  end

  def buyer_authorization
    @buyer_authorization ||= Astro::BUYER_AUTHORIZATION_CLASS.new(pawn_access_number: params[:pawn_access_number])
  end

  def json_body
    current_offers.map do |offer|
      { model: offer.attributes_including_delegate_association_attributes }
    end
  end
end
