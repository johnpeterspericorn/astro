# encoding: utf-8
class SoldVehiclesController < ApplicationController
  def new
    authorize(policy.new?)
    country = Country.find_country_by_name(SupportedCountry::DEFAULT)
    pawn_location = PawnLocation.new(country: country)
    @sold_vehicle = SoldVehicle.new(pawn_location: pawn_location)
  end

  def show
    authorize(policy.show?)
    @sold_vehicle = SoldVehicle.find(params[:id])
  end

  def edit
    authorize(policy.edit?)
    @sold_vehicle = SoldVehicle.find(params[:id])
  end

  def create
    authorize(policy.create?)

    @sold_vehicle = SoldVehicle.new(sold_vehicle_params)

    respond_to do |format|
      if @sold_vehicle.save
        format.json { render json: {}, status: :created }
      else
        format.json { render json: { errors: @sold_vehicle.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def update
    authorize(policy.update?)

    @sold_vehicle = SoldVehicle.find(params[:id])

    respond_to do |format|
      if @sold_vehicle.update_attributes(sold_vehicle_params)
        format.html { redirect_to @sold_vehicle, notice: 'Sold vehicle was successfully updated.' }
      else
        format.html { render action: 'edit' }
      end
    end
  end

  def decode
    # /sold_vehicles/:vnum/decode
    response = SoldVehicle.decode_vnum(params[:vnum])

    # RESPONSE :
    # S: {make: 'FORD', model: 'ROADSTER', year: '2018', mid: '', errors: ''}
    # F: {errors: 'Validation Failed'}
    respond_to do |format|
      format.js { render json: response.to_json }
    end
  end

  private

  def policy
    @policy ||= SoldVehiclePolicy.new(current_user)
  end

  def sold_vehicle_params
    params.require(:sold_vehicle).permit!
  end
end
