# encoding: utf-8
class HelpPanel::GuidesController < ApplicationController
  skip_before_action :authenticate_user!

  def create
    HelpPanelMailer.send_guides(address, guide_names).deliver_later
    flash[:notice] = I18n.t('help.thank_you_message_guides_request')
    redirect_to :back
  end

  private

  def guide_names
    params[:guide_id].keys
  end

  def address
    params[:guide_email]
  end
end
