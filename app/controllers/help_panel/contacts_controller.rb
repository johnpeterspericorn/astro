# encoding: utf-8
class HelpPanel::ContactsController < ApplicationController
  skip_before_action :authenticate_user!

  def create
    HelpPanelMailer.contact(user_info).deliver_later
    flash[:notice] = I18n.t('help_panels.thank_you_message')
    redirect_to :back
  end

  private

  def user_info
    params.permit(:last_name, :first_name, :phone, :email, :problem_type, :other_problem_type)
  end
end
