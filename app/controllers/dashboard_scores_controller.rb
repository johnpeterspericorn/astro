# encoding: utf-8
class DashboardScoresController < ApplicationController
  respond_to :json
  before_action :scores_viewable, only: [:index]

  ##
  # Provide Agreement dashboard scores
  #
  # @resource /dashboard_scores
  # @action GET
  #
  # @required [String] agreement_id The Five Million number of the agreement
  # @example_request
  #   ```json
  #     {
  #       "agreement_number": "5000123",
  #     }

  def index
    @score = DashboardScore.where(buyer_num: params[:agreement_number]).limit(1).order('date DESC').first
    respond_with(@score || {})
  end

  private

  def scores_viewable
    return true if current_user.admin?
    authorize current_user.agreement_score_viewer?
  end
end
