# encoding: utf-8
class HowPaidInformationController < ApplicationController
  def new
    @vnum = params[:vnum]
    @promise_purchase_id = params[:promise_purchase_id]
    @how_paid_information = HowPaidInformation.new
  end

  def create
    @how_paid_information = HowPaidInformation.new(how_paid_information_params)

    options = { promise_purchase_id: params[:promise_purchase_id], bypass_adjustment_request_error: true }
    options[:floor_plan_company_id] = floor_plan_company_params if company_selected?

    if selection_validated?
      if @how_paid_information.save
        redirect_to new_return_funnel_verification_path(options)
      else
        prompt_error(@how_paid_information.errors.full_messages.to_sentence)
      end
    else
      prompt_error(t('vehicles.how_paid.none_selected'))
    end
  end

  private

  def floor_plan_companies
    FloorPlanCompany.pluck(:name, :id)
  end
  helper_method :floor_plan_companies

  def how_paid_information_params
    params.require(:how_paid_information).permit(:payment_type, :vnum)
  end

  def selection_validated?
    (floor_plan_paid? && company_selected?) || buyer_paid?
  end

  def prompt_error(message)
    flash[:error] = message
    redirect_to :back
  end

  def company_selected?
    floor_plan_company_params.present?
  end

  def floor_plan_paid?
    how_paid_information_params[:payment_type] == 'floor_plan_paid'
  end

  def buyer_paid?
    how_paid_information_params[:payment_type] == 'buyer_paid'
  end

  def floor_plan_company_params
    params[:floor_plan_company]
  end
end
