# encoding: utf-8
class PagesController < HighVoltage::PagesController
  skip_before_action :authenticate_user!, if: :page_does_not_require_authentication?

  private

  def page_name
    params[:id] # high_voltage uses the :id param for the page name
  end

  def page_does_not_require_authentication?
    unauthenticated_static_pages.include?(page_name)
  end

  def unauthenticated_static_pages
    %w(privacy_policy visitor_agreement learn_about_agreementshield)
  end
end
