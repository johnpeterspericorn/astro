# encoding: utf-8
class FlatRatesController < ApplicationController
  include ColumnConversions
  before_action :require_agreement_numbers

  def index
    authorize(flat_rate_policy.index?)
    if flat_rate_offer_batch
      flash[:error] = I18n.t('flat_rates.errors.already_exists')
      redirect_to flat_rate_offer_batch_path(flat_rate_offer_batch)
    end
    @flat_rates = agreement_numbers.map do |no|
      flat_rate = palv? ? flat_rate_with_lv(no, country, limited_volume) : flat_rate(no, country)
      FlatRatePriceDecorator.new(flat_rate, flat_rate_miles, psi_eligible?, palv?)
    end

    recorded_inquiries = FlatRateInquiryRecorder.record(@flat_rates, pawn_access_number, flat_rate_miles, psi_eligible?, current_user, palv?)
    FlatRateMailer.lookup_notification(recorded_inquiries).deliver_later unless recorded_inquiries.blank?

    @flat_rate_offer = FlatRateOffer.new
  end

  private

  def flat_rate_with_lv(no, country, limited_volume)
    FlatRate.find_or_initialize_by(agreement_no: no, country: country, lv_tier: limited_volume) do |new_flat_rate|
      new_flat_rate.ineligibility_condition = 0
    end
  end

  def flat_rate(no, country)
    FlatRate.find_or_initialize_by(agreement_no: no, country: country) do |new_flat_rate|
      new_flat_rate.ineligibility_condition = 0
    end
  end

  def pawn_access_number
    params.fetch(:pawn_access_number)
  end
  helper_method :pawn_access_number

  def flat_rate_params
    params.fetch(:flat_rates, {})
  end

  def flat_rate_policy
    @flat_rate_policy ||= FlatRatePolicy.new(current_user)
  end

  def agreement_numbers
    Array(flat_rate_params[:agreement_numbers])
  end

  def country
    flat_rate_params[:country] || SupportedCountry::DEFAULT
  end
  helper_method :country

  def limited_volume
    flat_rate_params[:limited_volume].blank? ? nil : flat_rate_params[:limited_volume]
  end

  def distance_units
    I18n.translate("distance_units.#{country_code}.unit")
  end
  helper_method :distance_units

  def country_code
    Country.find_country_by_name(country).try(:alpha2)
  end

  def flat_rate_miles
    flat_rate_params[:miles]
  end
  helper_method :flat_rate_miles

  def flat_rate_distance_with_units
    distance_map = {
      '360' => 'distance3',
      '500' => 'distance2',
      '250' => 'distance1'
    }
    "#{send(distance_map[flat_rate_miles])} #{distance_units}"
  end
  helper_method :flat_rate_distance_with_units

  def distance1
    I18n.translate("offer_distances.#{country_code}.distance1")
  end

  def distance2
    I18n.translate("offer_distances.#{country_code}.distance2")
  end

  def distance3
    I18n.translate("offer_distances.#{country_code}.distance3")
  end

  def psi_eligible?
    !flat_rate_params[:psi].to_i.zero?
  end
  helper_method :psi_eligible?

  def palv?
    NewProductKind::DEFAULT_VOLUME_TIERS.keys.map(&:to_s).include?(limited_volume) && value_to_boolean(flat_rate_params[:lv].to_i)
  end
  helper_method :palv?

  def require_agreement_numbers
    redirect_to :back, notice: I18n.t('flat_rates.errors.select_agreement_numbers') if agreement_numbers.empty?
  end

  def flat_rate_offer_batch
    @flat_rate_offer_batch ||=
      FlatRateOfferBatch.includes(flat_rate_offers: :flat_rate)
                        .where(pawn_access_no: pawn_access_number)
                        .where(flat_rates: { country: country }).first
  end
end
