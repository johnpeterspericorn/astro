# encoding: utf-8
class ApplicationController < ActionController::Base
  include Authorizable

  before_action :set_request_id
  before_action :authenticate_user_with_token!, except: [:set_locale]
  before_action :authenticate_user!, :set_alerts, except: [:set_locale]
  before_action :set_locale

  around_action :set_time_zone

  rescue_from ActionController::ParameterMissing do
    head :bad_request
  end

  protect_from_forgery

  FEATURE_FLAG_HELPERS = [:show_badge_button?, :show_agreement_filter?].freeze
  delegate(*FEATURE_FLAG_HELPERS, to: :navigation_presenter)
  helper_method(*FEATURE_FLAG_HELPERS)

  def navigation_presenter
    @navigation_presenter ||= NavigationPresenter.new(current_user)
  end

  def set_alerts
    flash[:notice] = I18n.t("help.#{params[:msg]}") if params[:msg].present?
  end

  def authenticate_user_with_token!
    token = params[:auth_token]
    return unless token
    user = User.find_by(authentication_token: token)
    sign_in(:user, user, store: false) unless user.nil?
  end

  private

  # For lograge custom field in log json
  def append_info_to_payload(payload)
    super
    payload[:request_id] = request.env["action_dispatch.request_id"]
  end

  def set_time_zone
    if js_timezone_offset.present?
      offset_seconds = js_timezone_offset.to_i * 60
      timezone = ActiveSupport::TimeZone[offset_seconds]
      Time.use_zone(timezone) { yield }
    else
      yield
    end
  end

  def js_timezone_offset
    # returns timezone offset in minutes
    cookies['browser.tzoffset']
  end

  def set_request_id
    RequestStore.store[:request_id] = request.env["action_dispatch.request_id"]
  end

  def set_locale
    cookies[:locale] = params[:locale] if I18n.locale_available?(params[:locale])
    I18n.locale = cookies[:locale] || I18n.default_locale
  end
end
