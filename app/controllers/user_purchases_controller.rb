# encoding: utf-8
class UserPurchasesController < ApplicationController
  def index
    @promise_purchases = PromisePurchase.for_agreement(current_user.pawn_access_number).page(params[:page])
  end
end
