# encoding: utf-8
class LaneNumbersController < ApplicationController
  def index
    location_initials = params[:location_initials]

    if location_initials.present?
      render inline: PresaleVehicleDecorator.options_for_lane_select(location_initials)
    else
      render nothing: true, status: :not_found
    end
  end
end
