# encoding: utf-8
class ReturnCertificatesController < ApplicationController
  respond_to :html, :json

  def show
    authorize(return_invoice_policy.show?)
    if return_invoice
      @return_certificate = ReturnCertificate.new(return_invoice)
      @response = { return_certificate: @return_certificate }
      @response[:bill_of_sale] = bill_of_sale if return_invoice.country == 'Canada'
    end
    respond_with @response
  end

  private

  def pawn_access_number
    params[:pawn_access_number]
  end

  def return_invoice_policy
    @return_invoice_policy ||= ReturnInvoicePolicy.new(current_user, return_invoice, pawn_access_number)
  end

  def return_invoice
    @return_invoice ||= if params[:slug]
                          ReturnInvoice.includes(promise_purchase: { promise_offer: :pawn_information }).find_by_slug(params[:slug])
                        else
                          ReturnInvoice.includes(promise_purchase: { promise_offer: :pawn_information }).find_by_id(params[:return_invoice_id])
                        end
  end

  def bill_of_sale
    @bill_of_sale ||= BillOfSale.find(params[:bill_of_sale_id])
  end
end
