# encoding: utf-8
class DocsController < ApplicationController
  respond_to :html
  layout false

  def index
    authorize(doc_policy.index?)
    render file: Rails.root.join('api_doc', 'index.html')
  end

  def show
    authorize(doc_policy.show?)
    render file: Rails.root.join('api_doc', params[:path])
  end

  private

  def doc_policy
    DocPolicy.new(current_user)
  end
end
