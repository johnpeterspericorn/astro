# encoding: utf-8
module Types
  DashboardScoreType = GraphQL::ObjectType.define do
    name 'DashboardScore'
    description 'this is the risk scoring system for agreements'
    interfaces [GraphQL::Relay::Node.interface]

    field :id, !types.ID
    field :buyerNumber, !types.Int, property: :buyer_num
    field :paymentKind, !types.String, property: :payment_kind
    field :avgLoss, !types.Float, property: :avg_loss
    field :returnRate, !types.Float, property: :return_rate
    field :volume, !types.Float
    field :margin, !types.Float
    field :earnings, !types.Float
    field :returnScore, !types.Int, property: :return_score
    field :purchaseQuality, !types.Int, property: :purchase_quality
    field :marginScore, !types.Int, property: :margin_score
    field :volumeScore, !types.Int, property: :volume_score
    field :earningScore, !types.Int, property: :earnings_score
    field :date, !types.String

    field :groupName, types.String, property: :group_name
    field :groupAvgLoss, types.Float, property: :group_avg_loss
    field :groupReturnRate, types.Float, property: :group_return_rate
    field :groupVolume, types.Float, property: :group_volume
    field :groupMargin, types.Float, property: :group_margin
    field :groupEarnings, types.Float, property: :group_earnings
    field :groupPurchaseQuality, types.Int, property: :group_purchase_quality
    field :groupReturnScore, types.Int, property: :group_return_score
    field :groupMarginScore, types.Int, property: :group_margin_score
    field :groupVolumeScore, types.Int, property: :group_volume_score
    field :groupEarningsScore, types.Int, property: :group_earnings_score
  end
end
