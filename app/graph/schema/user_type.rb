# encoding: utf-8
module Types
  UserType = GraphQL::ObjectType.define do
    name 'User'
    description 'a user can be an admin, agreement, customer service rep, etc'
    interfaces [GraphQL::Relay::Node.interface]

    field :id, !types.ID
    field :username, !types.String
    field :email, !types.String
    field :roles, types[!types.String]
    field :pawnAccessNumber, types.Int, property: :pawn_access_number
    field :agreementNumbers, types[types.Int], property: :agreement_numbers
    field :dashboardScore, DashboardScoreType do
      argument :buyerNumber, !types.Int
      # rubocop:disable Style/Lambda
      resolve ->(obj, args, _ctx) {
        agreement = args[:buyerNumber]
        if obj.admin? || obj.super_user? || obj.agreement_numbers.include?(agreement.to_s)
          DashboardScore.order('date desc').find_by(buyer_num: agreement)
        end
      }
      # rubocop:enable Style/Lambda
    end
    field :transactions, types[TransactionType] do
      argument :agreementNumber, !types.String
      argument :locationInitial, types.String
      argument :daysLeft, types.Int
      argument :returned, types.Boolean
      argument :active, types.Boolean, default_value: true
      argument :offset, types.Int, default_value: 0
      argument :days, types.Int, default_value: 90
      argument :limit, types.Int, default_value: 10

      # rubocop:disable Style/Lambda
      resolve ->(obj, args, _ctx) {
        agreement_number = args[:agreementNumber]
        if obj.admin? || obj.super_user? || (obj.agreement_transaction_viewer? && obj.agreement_numbers.include?(agreement_number))
          Transaction.by_agreement_number(
            agreement_number: agreement_number,
            returned: args[:returned],
            days: args[:days],
            offset: args[:offset],
            limit: args[:limit],
            active: args[:active],
            location_initial: args[:locationInitial],
            days_left: args[:daysLeft]
          )
        end
      }
      # rubocop:enable Style/Lambda
    end
    field :presaleVehicles, types[PresaleVehicleType] do
      argument :vnum, !types.String
      # rubocop:disable Style/Lambda
      resolve ->(obj, args, _ctx) {
        vnum = args[:vnum]
        PresaleVehicle.where(vnum: vnum) if obj.admin? && obj.agreementcheck_viewer?
      }
      # rubocop:enable Style/Lambda
    end
  end
end
