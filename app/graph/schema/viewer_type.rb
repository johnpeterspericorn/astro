# encoding: utf-8
module Types
  ViewerType = GraphQL::ObjectType.define do
    name 'Viewer'
    description 'Viewer based on the session'

    field :user, UserType do
      resolve ->(obj, _args, _ctx) { obj }
    end
  end
end
