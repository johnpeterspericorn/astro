# encoding: utf-8
module Types
  PresaleVehicleType = GraphQL::ObjectType.define do
    name 'PresaleVehicle'
    description 'this is the presale vehicle information per vnum'
    interfaces [GraphQL::Relay::Node.interface]

    field :id, !types.ID
    field :vnum, !types.String, property: :vnum
    field :promise_250_low, !types.Float
    field :promise_250_low, !types.Float

    field :odometer_reading, types.Int, property: :odometer_reading
    field :year, types.Int, property: :year
    field :model, types.String, property: :model
    field :make, types.String, property: :make
    field :lane_no, types.String, property: :lane_no
    field :run_no, types.String, property: :run_no
    field :ineligibility_condition, types.Int, property: :ineligibility_condition
    field :location_initials, types.String, property: :location_initials
    field :average_valuation, types.Int, property: :average_valuation
    field :vnum_risk, types.Float, property: :vnum_risk
    field :odometer_rating, types.Int, property: :odometer_rating
    field :autocheck_rating, types.Int, property: :autocheck_rating
    field :mmr_rating, types.Int, property: :mmr_rating
    field :age_rating, types.Int, property: :age_rating
    field :seller_rating, types.Int, property: :seller_rating
    field :mid_rating, types.Int, property: :mid_rating
    field :promise_250_medium, types.Float, property: :promise_250_medium
    field :ove, types.Boolean, property: :ove
    field :promise_prepaid, types.Boolean, property: :promise_prepaid
    field :automatic_purchase, types.Boolean, property: :automatic_purchase
    field :vehicle_code, types.Int, property: :vehicle_code
    field :promise_price_threshold, types.Int, property: :promise_price_threshold
    field :seller_paid, types.Boolean, property: :seller_paid
    field :low_valuation, types.Int, property: :low_valuation
    field :high_valuation, types.Int, property: :high_valuation
    field :seller_no, types.String, property: :seller_no
  end
end
