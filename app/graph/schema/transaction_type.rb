# encoding: utf-8
module Types
  TransactionType = GraphQL::ObjectType.define do
    name 'Transaction'
    description 'Vehicle that have a promise purchased'
    interfaces [GraphQL::Relay::Node.interface]

    field :id, !types.ID
    field :purchasedAt, types.String, property: :purchased_at
    field :productName, types.String, property: :product_name
    field :promisePurchaseId, types.Int, property: :promise_purchase_id
    field :daysSelected, types.Int, property: :days_selected
    field :milesSelected, types.Int, property: :miles_selected
    field :daysLeft, types.Int, property: :days_left
    field :expiresAt, types.String, property: :expires_at
    field :vnum, types.String
    field :additionalDays, types.Int, property: :additional_days
    field :year, types.Int
    field :make, types.String
    field :model, types.String
    field :odometer, types.Int
    field :locationInitials, types.String, property: :location_initials
    field :locationName, types.String, property: :location_name
    field :purchasePrice, types.Int, property: :purchase_price
    field :limited_volume, types.Boolean
    field :obfuscatedId, types.String, property: :obfuscated_id
    field :returnInvoiceId, types.String, property: :return_invoice_id
    field :billOfSaleId, types.String, property: :bill_of_sale_id
  end
end
