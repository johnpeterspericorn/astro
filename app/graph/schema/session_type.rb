# encoding: utf-8
module Types
  SessionType = GraphQL::ObjectType.define do
    name 'Session'
    description 'an session model'

    field :error, types.String
    field :accessToken, types.String, property: :access_token
    field :viewer, ViewerType do
      resolve ->(obj, _args, _ctx) { obj.user }
    end
  end
end
