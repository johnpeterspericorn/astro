# encoding: utf-8
AstroSchema = GraphQL::Schema.define do
  query Types::QueryType
  # mutation MutationType
  max_depth 12

  # rubocop:disable Style/Lambda
  resolve_type ->(object, _ctx) {
    type_name = object.class.name
    Schema.types[type_name]
  }

  id_from_object ->(object, type_definition, _query_ctx) {
    GraphQL::Schema::UniqueWithinType.encode(type_definition.name, object.id)
  }

  object_from_id ->(id, _query_ctx) {
    class_name, item_id = GraphQL::Schema::UniqueWithinType.decode(id)
    Object.const_get(class_name).find item_id
  }
  # rubocop:enable Style/Lambda
end
