# encoding: utf-8
module Types
  QueryType = GraphQL::ObjectType.define do
    name 'Query'
    description 'The query root of this schema'
    field :node, GraphQL::Relay::Node.field

    field :session do
      type Types::SessionType
      description 'Find a user by username and password'
      argument :username, !types.String
      argument :password, !types.String
      # rubocop:disable Style/Lambda
      resolve ->(_obj, args, _ctx) do
        auth = Authenticator.new(args['username'], args['password'])
        auth.access_token = AccessToken.encode(auth.user) if auth.successful?
        auth
      end
      # rubocop:enable Style/Lambda
    end

    field :viewer do
      type ViewerType
      description 'Find a user based on session'
      resolve ->(_obj, _args, ctx) { ctx[:current_user].call }
    end

    field :user do
      type UserType
      description 'Find a user by id'
      argument :id, !types.ID
      resolve ->(_obj, args, _ctx) { User.find args['id'] }
    end
  end
end
