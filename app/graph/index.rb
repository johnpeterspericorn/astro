# encoding: utf-8
Dir[
  Rails.root.join('app', 'graph', 'schema', '*.rb')
].each do |file|
  require file
end
require Rails.root.join('app', 'graph', 'query')
require Rails.root.join('app', 'graph', 'root')
