# encoding: utf-8
class ApplicationMailer < ActionMailer::Base
  around_action :set_time_zone

  def asset_to_filepath(asset_name)
    Astro::Application.assets.find_asset(asset_name).pathname
  end

  def attach_branding
    attachments.inline['email-header.png'] = File.read(asset_to_filepath('email-header.png'))
    attachments.inline['email-footer.png'] = File.read(asset_to_filepath('email-footer.png'))
  end

  private

  def set_time_zone
    Time.use_zone('Eastern Time (US & Canada)') { yield }
  end
end
