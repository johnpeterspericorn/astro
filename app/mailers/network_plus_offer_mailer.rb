# encoding: utf-8
class NetworkPlusOfferMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL

  def offer_confirmation(network_plus_offer)
    @agreement_info = network_plus_offer.agreement_info
    @days_from_expiry = days_from_expiry(network_plus_offer.expiration_date)
    @network_plus_offer_approval_url = admin_network_plus_offer_approval_url(network_plus_offer)

    attach_branding
    attachments.inline['email-signature.png'] = File.read(asset_to_filepath('email-signature.png'))

    mail(
      subject: I18n.t('network_plus_offer_mailer.offer_confirmation.subject'),
      to:      network_plus_offer.emails
    )
  end

  def offer_rejection(network_plus_offer)
    @network_plus_offer = network_plus_offer
    @agreement_info = network_plus_offer.agreement_info
    cc_recipients = network_plus_offer.emails
    attach_branding

    mail(
      subject: I18n.t('network_plus_offer_mailer.offer_rejection.subject'),
      to: Astro::AUTOMATIC_EMAIL,
      cc: cc_recipients
    )
  end

  def offer_accepted(network_plus_offer, accepting_user, accepted_at, effective_date)
    cc_recipients = network_plus_offer.emails

    attach_branding

    @network_plus_offer = network_plus_offer
    @accepting_user = accepting_user
    @accept_date = Time.zone.parse(accepted_at).to_date
    @network_plus_offer_approval_url = admin_network_plus_offer_approval_url(network_plus_offer)
    @agreement_info = network_plus_offer.agreement_info
    @effective_date = Date.parse(effective_date)
    generate_offer_pdf do |file|
      attachments['network_plus_offer_doc.pdf'] = File.read(file)
      mail(
        subject: I18n.t('network_plus_offer_mailer.offer_accepted.subject'),
        to: cc_recipients,
        bcc: Astro::MAS_EMAIL
      )
    end
  end

  private

  def days_from_expiry(expiration_date)
    (expiration_date.to_date - Time.zone.today).to_i
  end

  def generate_offer_pdf
    file_path = "#{Rails.root}/public/network_plus_offer_doc_#{@network_plus_offer.id}.pdf"
    html = render_to_string(template: 'admin/network_plus_offers/approvals/offer_doc.html.haml', locals: { network_plus_offer: @network_plus_offer, limits: @limits, effective_date: @effective_date }, layout: false)
    pdf = PDFKit.new(html, print_media_type: true)
    pdf.to_file(file_path)
    begin
      yield file_path
    ensure
      FileUtils.rm(file_path)
    end
  end
end
