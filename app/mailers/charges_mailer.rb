# encoding: utf-8
class ChargesMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL

  def notify_first_failure(promise_charge, error_message)
    subject    = 'Charge failed for promise purchase on Astro'
    recipients = Astro::MAS_EMAIL
    notify_failure(subject, recipients, promise_charge, error_message)
  end

  def notify_last_failure(promise_charge, error_message)
    subject    = "Charge aborted for promise purchase on Astro (#{promise_charge.location_initials}/#{promise_charge.lane_no}/#{promise_charge.run_no})"
    recipients = [Astro::MAS_EMAIL, Astro::SUPPORT_EMAIL, Astro::ACCOUNTING_EMAIL]
    notify_failure(subject, recipients, promise_charge, error_message)
  end

  private

  def notify_failure(subject, recipients, promise_charge, error_message)
    @charge        = promise_charge
    @error_message = error_message
    mail subject: subject, to: recipients
  end
end
