# encoding: utf-8
class ShipmentsMailer < ActionMailer::Base
  default from: Astro::NO_REPLY_EMAIL

  def tracking_update(shipment)
    recipients = [Astro::SUPPORT_EMAIL]
    recipients << shipment.buyer_email if shipment.buyer_email

    @shipment = shipment.decorate

    mail subject: 'Shipment Tracking Update', to: recipients
  end
end
