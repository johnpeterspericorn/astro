# encoding: utf-8
class AccountsReceivableMailer < ApplicationMailer
  INTERVAL = 1.week

  default from: Astro::APP_EMAIL

  def ledger_report
    promise_offers = AccountsReceivablePromiseOfferQuery.new
                                                            .will_appear_on_accounts_receivable_report_at(Time.current)
                                                            .map { |go| AccountsReceivableOfferDecorator.new(go) }

    cancelled_purchases = CancelledPurchase
                          .will_appear_on_accounts_receivable_report_at(Time.current)
                          .map { |cp| AccountsReceivableCancelledPurchaseDecorator.decorate(cp) }

    csv = AccountsReceivableCsv.new(promise_offers + cancelled_purchases)

    attachments['report.csv'] = csv.to_s

    mail(
      subject: I18n.t('promise_purchase.accounts_receivable_report.subject'),
      to: Settings.accounts_receivable_report_emails
    )
  end
end
