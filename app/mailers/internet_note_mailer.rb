# encoding: utf-8
class InternetNoteMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL

  def notify_last_failure(note_attributes, failure_message)
    @vnum = note_attributes.fetch(:vnum)
    @location_initials = note_attributes.fetch(:pawn_location_initials)
    @sblu = note_attributes.fetch(:sblu)
    @work_order = note_attributes.fetch(:work_order)
    @notes = note_attributes.fetch(:notes)
    @failure_message = failure_message

    recipients = [Astro::SUPPORT_EMAIL, Astro::astrdian_INVENTORY_EMAIL]
    subject = 'AgreementShield Return - Internet Notes call failed'

    mail to: recipients, subject: subject
  end
end
