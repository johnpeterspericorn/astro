# encoding: utf-8
class PromisePurchaseMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL

  def get_user_information_from_offer(offer)
    @badge_number   = offer.badge_no
    @agreement_number  = offer.agreement_no
    @pawn_code   = offer.pawn_code
    @pawn_access_number = offer.pawn_access_no
  end

  def purchase_confirmation(options)
    options.default = []
    bundled_purchases = options[:bundled_purchases].map(&:bundled_purchase).compact

    @bundled_purchases = bundled_purchases.map(&:decorate)
    @promise_only_purchases = options[:promise_purchases].map(&:decorate)
    @inspection_only_purchases = options[:inspection_purchases].map(&:decorate)

    all_purchases = [
      @bundled_purchases,
      @promise_only_purchases,
      @inspection_only_purchases
    ].flatten
    return if all_purchases.empty?

    user_email = options[:user_email]

    promise_offer = all_purchases.first.promise_offer
    get_user_information_from_offer(promise_offer.decorate)

    recipients = [Astro::PROTECTED_EMAIL]
    recipients << user_email if user_email.present?

    attach_branding
    mail subject: purchase_confirmation_subject(all_purchases),
         to: recipients
  end

  def pending_confirmation(pending_bundled, pending_unbundled, user_email)
    pending_bundled_purchases = pending_bundled.map(&:bundled_purchase).compact

    @pending_bundled_purchases = pending_bundled_purchases.map(&:decorate)
    @pending_promise_only_purchases = pending_unbundled.map(&:decorate)

    all_purchases = [
      @pending_bundled_purchases,
      @pending_promise_only_purchases
    ].flatten
    return if all_purchases.empty?

    promise_offer = all_purchases.first.promise_offer
    get_user_information_from_offer(promise_offer.decorate)

    recipients = [Astro::PROTECTED_EMAIL, user_email]

    attach_branding
    mail subject: I18n.t('promise_purchase_mailer.pending_confirmation.subject'), from: Astro::PENDING_EMAIL, to: recipients
  end

  def notify_pending(promise_purchase)
    @promise_purchase  = promise_purchase.decorate
    @promise_offer     = @promise_purchase.promise_offer

    @agreement_number         = @promise_offer.agreement_no
    @pawn_access_number = @promise_offer.pawn_access_no
    @badge_number          = @promise_offer.badge_no

    attach_branding
    mail subject: 'Pending Promise', from: Astro::PENDING_EMAIL, to: Astro::SUPPORT_EMAIL
  end

  def notify_ineligible(vehicle)
    @vehicle = vehicle.decorate
    recipients = [Astro::SUPPORT_EMAIL]
    recipients << vehicle.automatic_purchase_email if vehicle.automatic_purchase_email

    attach_branding
    mail(
      subject: 'Vehicle Not Covered',
      from: Astro::SUPPORT_EMAIL,
      to: recipients,
      bcc: Astro::AUTOMATIC_EMAIL
    )
  end

  def notify_api_error(request, return_code)
    @request = request
    @return_code = I18n.t('purchase_api.return_code')[return_code]
    recipients = [Astro::MAS_EMAIL]

    mail(
      subject: "Astro Purchase API failure VNUM: #{@request['vnum']}",
      from: Astro::APP_EMAIL,
      to: recipients
    )
  end

  private

  def purchase_confirmation_subject(all_purchases)
    if all_purchases.size == 1
      I18n.t('promise_purchase.purchase_confirmation.subject', vnum: all_purchases.first.vnum)
    else
      I18n.t('promise_purchase.multiple_purchase_confirmation.subject')
    end
  end
end
