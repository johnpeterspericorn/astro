# encoding: utf-8
require 'date_utils'
class PerformanceAlertMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL
  helper :mailer
  include DateUtils

  def self.send_email(performance_alert)
    return_rate = performance_alert.return_rate
    lpc = performance_alert.lpc
    emails = performance_alert.emails

    emails.split(',').each do |email|
      if return_rate && lpc
        return_rate_and_lpc_warning(performance_alert, email).deliver_later
      elsif return_rate
        return_rate_warning(performance_alert, email).deliver_later
      elsif lpc
        lpc_warning(performance_alert, email).deliver_later
      end
    end
  end

  def return_rate_warning(performance_alert, email)
    @performance_alert = performance_alert
    @agreement_info = performance_alert.agreement_info
    @email = email
    email_action = ActionType.process(performance_alert, email, 'Return rate warning email sent')
    @code = "#{email_action.id}_return_rate_#{DateUtils.timestamp}"
    attach_branding
    mail(
      subject: I18n.t('performance_alert_mailer.return_rate_version.subject'),
      to:      email
    )
  end

  def lpc_warning(performance_alert, email)
    @performance_alert = performance_alert
    @agreement_info = performance_alert.agreement_info
    @email = email
    email_action = ActionType.process(performance_alert, email, 'Purchase quality warning email sent')
    @code = "#{email_action.id}_lpc_#{DateUtils.timestamp}"
    attach_branding
    mail(
      subject: I18n.t('performance_alert_mailer.lpc_version.subject'),
      to:      email
    )
  end

  def return_rate_and_lpc_warning(performance_alert, email)
    @performance_alert = performance_alert
    @agreement_info = performance_alert.agreement_info
    @email = email
    email_action = ActionType.process(performance_alert, email, 'Return rate and purchase quality warning email sent')
    @code = "#{email_action.id}_lpc_return_rate_#{DateUtils.timestamp}"
    attach_branding
    mail(
      subject: I18n.t('performance_alert_mailer.return_rate_and_lpc_version.subject'),
      to:      email
    )
  end
end
