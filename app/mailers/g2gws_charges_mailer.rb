# encoding: utf-8
class G2gwsChargesMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL

  def notify_last_failure(promise_offer, error_message)
    subject    = "G2gws OrdersAPI call aborted in Astro for VNUM: #{promise_offer.vnum}, ConsignmentID: #{promise_offer.consignment_id}"
    recipients = [Astro::MAS_EMAIL]
    @offer = promise_offer
    @error_message = error_message

    mail subject: subject, to: recipients
  end
end
