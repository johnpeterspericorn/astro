# encoding: utf-8
class HelpPanelMailer < ActionMailer::Base
  def send_guides(address, guide_names)
    guide_names.each do |guide_name|
      attachments[guide_name] = File.read(Rails.root.join('app', 'assets', 'pdfs', 'quick_guides', guide_name))
    end

    mail from: Astro::APP_EMAIL, to: [address]
  end

  def contact(user_info)
    @first_name         = user_info[:first_name]
    @last_name          = user_info[:last_name]
    @phone              = user_info[:phone]
    @email              = user_info[:email]
    @problem_type       = user_info[:problem_type]
    @other_problem_type = user_info[:other_problem_type] if @problem_type == 'Other'

    mail from: 'Get Help <info@agreementshield.com>', to: Astro::SUPPORT_EMAIL, subject: "Contact Me - #{@problem_type}"
  end
end
