# encoding: utf-8
class CancellationMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL

  def cancellation_confirmation_eligible_for_repurchase(promise_offer, promise_purchase_for_email, reason)
    @promise_offer = promise_offer.decorate
    @promise_purchase_details = promise_purchase_for_email
    @reason = reason
    recipients = [Astro::PROTECTED_EMAIL]
    recipients << @promise_offer.emails
    recipients << [Astro::CANADIAN_ASTRSHIELD_EMAIL] if promise_offer.canadian_location?

    attach_branding
    mail(
      subject: I18n.t('cancellation.email.eligible_for_repurchase_subject', location_initials: promise_offer.location_initials, pawn_access_number: promise_offer.pawn_access_no),
      to: recipients,
      bcc: Astro::ACCOUNTING_EMAIL
    )
  end

  def cancellation_confirmation_ineligible_for_repurchase(promise_offer, promise_purchase_for_email, reason)
    @promise_offer = promise_offer.decorate
    @promise_purchase_details = promise_purchase_for_email
    @reason = reason
    recipients = [Astro::PROTECTED_EMAIL]
    recipients << @promise_offer.emails
    recipients << [Astro::CANADIAN_ASTRSHIELD_EMAIL] if promise_offer.canadian_location?

    attach_branding
    mail(
      subject: I18n.t('cancellation.email.ineligible_for_repurchase_subject', location_initials: promise_offer.location_initials, pawn_access_number: promise_offer.pawn_access_no),
      to: recipients,
      bcc: Astro::ACCOUNTING_EMAIL
    )
  end
end
