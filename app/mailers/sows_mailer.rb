# encoding: utf-8
class SowsMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL

  def notify_last_failure(promise_offer, error_message, job)
    @promise_offer = promise_offer
    @error_message = error_message
    @job_name = job.name

    recipients = [Astro::SUPPORT_EMAIL, Astro::ACCOUNTING_EMAIL]
    subject = 'Settlement Option Webservice failure'
    mail to: recipients, subject: subject
  end
end
