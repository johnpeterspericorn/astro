# encoding: utf-8
class ReturnInvoiceMailer < ApplicationMailer
  default from: Astro::NO_REPLY_EMAIL

  # This is needed to allow form rendering in mailer views
  def protect_against_forgery?
    false
  end
  helper_method :protect_against_forgery?

  def return_confirmation(return_invoice, locale = nil)
    recipients = [Astro::SUPPORT_EMAIL]
    recipients << return_invoice.email if return_invoice.email.present?

    blind_copy_recipients = return_invoice.pawn_location.return_emails

    @return_certificate = ReturnCertificate.new(return_invoice)
    @return_certificate_url = return_certificate_link(return_invoice)

    attach_branding
    I18n.with_locale(locale) do
      mail subject: subject(return_invoice), to: recipients, bcc: blind_copy_recipients
    end
  end

  def pawn_location_return_notification(return_invoice, locale = nil)
    recipients = [Astro::SUPPORT_EMAIL]
    recipients << (return_invoice.canadian_return? ? [Astro::CANADIAN_ACCOUNTING_EMAIL] : [Astro::ACCOUNTING_EMAIL])

    @return_invoice     = return_invoice.id
    @return_certificate = ReturnCertificate.new(return_invoice)
    @shipment           = return_invoice.shipment
    @floor_plan_branch  = return_invoice.floor_plan_branch
    @return_certificate_url = return_certificate_link(return_invoice)

    attach_branding
    I18n.with_locale(locale) do
      mail subject: pawn_location_subject, to: recipients
    end
  end

  def pawn_location_left_lot_notification(return_invoice, locale = nil)
    recipients      = [return_invoice.pawn_location.return_emails].flatten!
    recipients      = [Astro::LEFT_LOT_DEFAULT_EMAIL] if recipients.blank?

    @return_invoice = return_invoice.decorate
    attach_branding
    I18n.with_locale(locale) do
      mail subject: left_lot_notification_subject, to: recipients
    end
  end

  def cancelled_and_eligible_for_return(return_invoice, user_email)
    send_cancellation(return_invoice, user_email)
  end

  def cancelled_and_ineligible_for_return(return_invoice, user_email)
    send_cancellation(return_invoice, user_email)
  end

  def cancellation_notification_to_accountant(return_invoice)
    @return_invoice = return_invoice.decorate
    recipients = [Astro::ACCOUNTING_EMAIL]
    attach_branding
    mail subject: I18n.t('return_invoice_mailer.notification_to_accountant.subject', vra: return_invoice.id), to: recipients
  end

  def monthly_cancelled_vra_report
    @report = CancelledReturnReport.monthly_report
    recipients = [Astro::ACCOUNTING_EMAIL]
    attach_branding

    mail subject: I18n.t('return_invoice_mailer.cancelled_vra_report.subject', month: @report.month), to: recipients
  end

  def weekly_vra_report_originated_pawn_location(report, accounting_email, date_from, date_to)
    recipients = [accounting_email]
    recipients = [Astro::LEFT_LOT_DEFAULT_EMAIL] if recipients.blank?
    @presigned_url = report.presigned_url
    @date_from = date_from.strftime('%Y-%m-%d')
    @date_to = date_to.strftime('%Y-%m-%d')
    attach_branding

    mail subject: I18n.t('return_invoice_mailer.weekly_vra_report.subject'), to: recipients
  end

  def return_reinstatement_notification(return_invoice, reinstating_user)
    recipients = [Astro::REQUEST_EMAIL]
    @return_invoice = return_invoice.decorate
    @return_certificate_url = return_certificate_link(return_invoice)
    @reinstating_username = reinstating_user.username

    attach_branding
    mail subject: 'VRA Reinstatement', to: recipients
  end

  def other_branch_selected(return_invoice, notes)
    @return_certificate = ReturnCertificate.new(return_invoice)
    @notes = notes

    attach_branding
    mail subject: "Return of #{@return_certificate.vnum} - Other Branch selected", to: [Astro::SUPPORT_EMAIL, Astro::ACCOUNTING_EMAIL]
  end

  def title_expiration_notification(return_invoice)
    recipients = [return_invoice.email] if return_invoice.email.present?
    @return_certificate = ReturnCertificate.new(return_invoice)
    @return_certificate_url = return_certificate_link(return_invoice)

    attach_branding
    mail from: Astro::SUPPORT_EMAIL, subject: "AgreementShield Return: Courtesy Notice for VNUM: #{@return_certificate.vnum}", to: recipients, cc: [Astro::SUPPORT_EMAIL], bcc: [Astro::PROTECTED_EMAIL]
  end

  def return_expiration_notification(return_invoice)
    send_expiry_notification(return_invoice)
  end

  def location_update_notification(return_invoice)
    @return_invoice = return_invoice.decorate
    @previous_pawn_location = return_invoice.previous_version.try(:pawn_location)

    attach_branding
    mail subject: location_update_notification_subject(@return_invoice), to: [Astro::REQUEST_EMAIL]
  end

  private

  def return_certificate_link(return_invoice)
    if return_invoice.canadian_return?
      return_invoice.slug.present? ? obfuscated_return_certificate_url(slug: return_invoice.slug, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(return_invoice.id)) : return_certificate_url(return_invoice_id: return_invoice.id, bill_of_sale_id: BillOfSale.find_by_return_invoice_id(return_invoice.id))
    else
      return_invoice.slug.present? ? obfuscated_return_certificate_url(slug: return_invoice.slug) : return_certificate_url(return_invoice_id: return_invoice.id)
    end
  end

  def subject(return_invoice)
    return_location = return_invoice.pawn_location.name
    I18n.t('return_invoice.return_confirmation.subject', return_location: return_location, vnum: @return_certificate.vnum)
  end

  def pawn_location_subject
    I18n.t('return_invoice.return_confirmation.pawn_location_subject', vnum: @return_certificate.vnum)
  end

  def left_lot_notification_subject
    I18n.t('return_invoice_mailer.left_lot_email.left_lot_notification_subject', vnum: @return_invoice.vnum, vra: @return_invoice.vra)
  end

  def cancellation_subject
    I18n.t('return_invoice.cancellation.subject')
  end

  def send_cancellation(return_invoice, user_email)
    @return_invoice      = return_invoice
    @vehicle_information = return_invoice.promise_purchase.promise_offer.vehicle_information

    recipients = [user_email] if user_email.present?

    bcc_recipients = return_invoice.pawn_location.return_emails
    bcc_recipients << Astro::ACCOUNTING_EMAIL
    bcc_recipients << Astro::REQUEST_EMAIL
    bcc_recipients << Astro::SUPPORT_EMAIL
    bcc_recipients << Astro::PROTECTED_EMAIL
    bcc_recipients << Astro::CANADIAN_ASTRSHIELD_EMAIL if return_invoice.canadian_return?

    attachments.inline['email-header.png'] = File.read(asset_to_filepath('email-header.png'))
    attachments.inline['email-footer.png'] = File.read(asset_to_filepath('email-footer.png'))

    mail subject: cancellation_subject, to: recipients, bcc: bcc_recipients
  end

  def location_update_notification_subject(return_invoice)
    "VNUM: #{return_invoice.vnum} | VRA: #{return_invoice.vra} | Return Location Updated"
  end

  def send_expiry_notification(return_invoice)
    recipients = [return_invoice.email] if return_invoice.email.present?
    @return_certificate = ReturnCertificate.new(return_invoice)
    @return_certificate_url = return_certificate_link(return_invoice)

    attach_branding
    mail subject: "AgreementShield Return: Courtesy Notice for VNUM: #{@return_certificate.vnum}", to: recipients, cc: [Astro::SUPPORT_EMAIL]
  end
end
