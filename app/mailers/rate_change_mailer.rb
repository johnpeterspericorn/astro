# encoding: utf-8
class RateChangeMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL

  def rate_change_notification(rate_change)
    @link_url = admin_rate_change_client_communication_url(rate_change_id: rate_change.id)

    attach_branding
    attachments.inline['email-signature.png'] = File.read(asset_to_filepath('email-signature.png'))

    mail(
      subject: I18n.t('rate_change_mailer.return_rate_version.subject'),
      to:      rate_change.emails
    )
  end
end
