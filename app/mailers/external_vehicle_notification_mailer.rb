# encoding: utf-8
require 'column_conversions'

class ExternalVehicleNotificationMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL
  include ColumnConversions

  def vehicle_rejection(external_vehicle)
    @external_vehicle = external_vehicle
    @batch_id = external_vehicle.external_vehicles_batch_id
    @id = external_vehicle.id
    @vnum = external_vehicle.vnum
    @rejection_comment = external_vehicle.rejection_comment
    @external_vehicles_approval_url = external_vehicles_approver_url(external_vehicle.external_vehicles_batch, external_vehicle_id: external_vehicle.id)
    cc_recipients = external_vehicle.rejecting_user.email
    to_addr = external_vehicle.email

    attach_branding

    mail(
      subject: I18n.t('external_vehicles_load.email.subject'),
      cc: cc_recipients,
      to: to_addr
    )
  end
end
