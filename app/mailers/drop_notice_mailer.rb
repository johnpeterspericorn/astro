# encoding: utf-8
class DropNoticeMailer < ApplicationMailer
  default from: Astro::APP_EMAIL

  def notify_last_failure(return_invoice, error_message)
    promise_offer  = return_invoice.promise_offer
    @error_message   = error_message
    @vnum             = promise_offer.vnum
    @seller_number   = promise_offer.seller_no
    @returned_at     = return_invoice.created_at
    @return_id       = return_invoice.id
    @return_location = return_invoice.pawn_location.initials

    mail subject: t('drop_notice_failure.subject'), to: Astro::REQUEST_EMAIL
  end
end
