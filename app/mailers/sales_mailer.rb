# encoding: utf-8
class SalesMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL

  def report(recipient = Settings.sales_report_email)
    sales = PromisePurchase
            .joins(promise_offer: { pawn_information: :pawn_location })
            .promised_at(date_range[:date_from], date_range[:date_to])
    @sales_report = SalesLocations.new(sales)
    @total_sales = @sales_report.total_sales
    @sales_dashboard_url = admin_sales_dashboard_index_url(filters: date_range)
    subject = "AgreementShield Daily Sales Report: #{Date.today.to_s(:number)}"

    mail subject: subject, to: recipient
  end

  private

  def date_range
    {
      date_from: Date.yesterday.beginning_of_day,
      date_to: Date.yesterday.end_of_day
    }
  end
end
