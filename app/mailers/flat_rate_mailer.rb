# encoding: utf-8
require 'column_conversions'

class FlatRateMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL
  include ColumnConversions

  def lookup_notification(flat_rate_inquiries)
    @flat_rate_inquiries = flat_rate_inquiries
    attach_branding
    mail subject: 'Flat Rate Lookup', to: Astro::AUTOMATIC_EMAIL
  end

  def offer_confirmation(flat_rate_offer_batch)
    @buyer_info = flat_rate_offer_batch.buyer_information
    @batch_url  = flat_rate_offer_batch_approval_url(flat_rate_offer_batch)
    cc_recipients = [flat_rate_offer_batch.sales_rep_email] if flat_rate_offer_batch.sales_rep_email.present?

    attach_branding
    user_emails = EmailValidation.new(arrayify(flat_rate_offer_batch.user_email), required: true).valid_emails
    attachments.inline['email-signature.png'] = File.read(asset_to_filepath('email-signature.png'))
    mail(
      subject: I18n.t('flat_rate_mailer.offer_confirmation.subject'),
      to:      @buyer_info.emails,
      cc:      cc_recipients,
      bcc:     [user_emails, Astro::AUTOMATIC_EMAIL]
    )
  end

  def offer_rejection(flat_rate_offer_batch)
    @flat_rate_offer_batch = flat_rate_offer_batch
    @buyer_information = flat_rate_offer_batch.buyer_information
    @pawn_access_number = flat_rate_offer_batch.pawn_access_no
    cc_recipients = [flat_rate_offer_batch.sales_rep_email] if flat_rate_offer_batch.sales_rep_email.present?

    attach_branding

    mail(
      subject: I18n.t('flat_rate_mailer.offer_rejection.subject'),
      cc: cc_recipients,
      to: Astro::AUTOMATIC_EMAIL
    )
  end

  def offer_accepted(flat_rate_offer_batch, accepting_user, accepted_at, effective_date)
    user_email = EmailValidation.new(arrayify(flat_rate_offer_batch.user_email), required: true).valid_emails
    attach_branding

    @flat_rate_offer_batch = flat_rate_offer_batch
    @accepting_user       = accepting_user
    @accept_date          = Time.parse(accepted_at).to_date
    @batch_url            = flat_rate_offer_batch_approval_url(flat_rate_offer_batch)
    @buyer_info           = flat_rate_offer_batch.buyer_information
    @offers               = flat_rate_offer_batch.flat_rate_offers.decorate
    @effective_date       = Date.parse(effective_date)
    @canadian_offer_batch = flat_rate_offer_batch.belongs_to_canada?

    generate_offer_pdf do |file|
      recipients = [Astro::AUTOMATIC_EMAIL]
      recipients << [Astro::CANADIAN_ASTRSHIELD_EMAIL] if @canadian_offer_batch
      cc_recipients = [flat_rate_offer_batch.sales_rep_email] if flat_rate_offer_batch.sales_rep_email.present?
      attachments['flat_rate_offer_doc.pdf'] = File.read(file)
      mail(
        subject: @canadian_offer_batch ? I18n.t('flat_rate_mailer.offer_accepted.subject_canada') : I18n.t('flat_rate_mailer.offer_accepted.subject'),
        to:      recipients,
        cc:      cc_recipients,
        bcc:     [user_email, Astro::MAS_EMAIL]
      )
    end
  end

  def offer_reset(flat_rate_offer_batch)
    @flat_rate_offer_batch = flat_rate_offer_batch
    @offers                = flat_rate_offer_batch.flat_rate_offers
    @buyer_info            = flat_rate_offer_batch.buyer_information
    cc_recipients = [flat_rate_offer_batch.sales_rep_email] if flat_rate_offer_batch.sales_rep_email.present?

    attach_branding

    mail(
      subject: I18n.t('flat_rate_mailer.offer_reset.subject'),
      cc: cc_recipients,
      to: Astro::AUTOMATIC_EMAIL
    )
  end

  private

  def populate_offer_values
    max_offer = @flat_rate_offer_batch.decorate.flat_rate_offers.sort_by(&:price).last.source
    @example = FlatRateOfferExampleDecorator.new(FlatRateOfferExample.new(max_offer, FlatRateOfferExample::EXAMPLE_VEHICLE_PRICE))
    if @flat_rate_offer_batch.belongs_to_canada?
      @limits = { max_odometer_limit: '400,000 kilometers', max_vehicle_value: '$120,000' }
      @units = 'Kilometers'
    else
      @limits = { max_odometer_limit: '250,000 miles', max_vehicle_value: '$100,000' }
      @units = 'Miles'
    end
  end

  def generate_offer_pdf
    populate_offer_values
    file_path = "#{Rails.root}/public/flat_rate_offer_doc_#{@flat_rate_offer_batch.id}.pdf"
    html = render_to_string(template: 'flat_rate_offer_batches/approvals/offer_doc.html.haml', locals: { flat_rate_offer_batch: @flat_rate_offer_batch, limits: @limits, effective_date: @effective_date }, layout: false)
    pdf = PDFKit.new(html, print_media_type: true)
    pdf.to_file(file_path)
    begin
      yield file_path
    ensure
      FileUtils.rm(file_path)
    end
  end
end
