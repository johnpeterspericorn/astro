# encoding: utf-8
class PaymentsMailer < ApplicationMailer
  helper :application
  default from: Astro::NO_REPLY_EMAIL

  def report(payments, to: Settings.payments_report_email, subject: I18n.t('payments.email.subject.span_report'))
    @payments_csv = PaymentsCsv.new(payments).to_s
    attachments['payments.csv'] = { mime_type: 'text/csv', content: @payments_csv }
    mail subject: subject, to: to
  end

  def receipt(recipients, payment, order_id: nil, amount: nil)
    @payment   = payment
    @order_id  = order_id || payment.order_id
    @purchase  = payment.promise_purchase
    @nonreimbursable_fees = @purchase.fees.nonreimbursable_fees.select { |fee| fee.code == Sas::FeeParser::DS_FEE_CODE }
    @amount    = amount || payment.amount
    subject    = 'Receipt for Credit Card Payment'
    @recipients = recipients

    attach_branding
    mail subject: subject, to: recipients, bcc:  [Astro::SUPPORT_EMAIL, Astro::ACCOUNTING_EMAIL]
  end
end
