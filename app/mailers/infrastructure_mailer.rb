# encoding: utf-8
class InfrastructureMailer < ActionMailer::Base
  default from: Astro::APP_EMAIL, to: Astro::SUPPORT_EMAIL

  def bad_offer(promise_offer, promise_purchase)
    @promise_offer = promise_offer
    @promise_purchase = promise_purchase

    mail subject: 'Bad Promise Offer created on Astro'
  end

  def bad_purchase(promise_offer, promise_purchase)
    @promise_offer = promise_offer
    @promise_purchase = promise_purchase

    mail subject: 'Bad Promise Purchase created on Astro'
  end

  def automatic_purchase_failed(vehicle)
    @vehicle = vehicle

    mail subject: "Automated Purchase of #{vehicle.vnum} (#{vehicle.universal_no}) Failed", to: Astro::MAS_EMAIL
  end
end
