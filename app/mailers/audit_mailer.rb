# encoding: utf-8
class AuditMailer < ApplicationMailer
  default from: Astro::AUDIT_EMAIL

  def notify_opening_audit(return_invoice)
    @return_certificate = return_invoice.return_certificate
    @audit_reasons = return_invoice.audit_reasons

    attach_branding
    mail(
      subject: "AgreementShield: Return Status Update VNUM: #{@return_certificate.vnum} - Audit Pending",
      to: return_invoice.audit_emails,
      cc: [Astro::SUPPORT_EMAIL, Astro::AUDIT_EMAIL],
      bcc: Astro::ACCOUNTING_EMAIL,
      importance: 'High'
    )
  end

  def notify_return_audit_cleared(return_invoice)
    @return_certificate = return_invoice.return_certificate

    attach_branding
    mail(
      subject: "AgreementShield: Return Status Update VNUM: #{@return_certificate.vnum} - Audit Complete",
      to: return_invoice.audit_emails,
      cc: [Astro::SUPPORT_EMAIL, Astro::AUDIT_EMAIL],
      bcc: Astro::ACCOUNTING_EMAIL,
      importance: 'High'
    )
  end

  def notify_return_audit_flagged(return_invoice)
    @return_certificate = return_invoice.return_certificate
    @audit_reasons = return_invoice.audit_reasons

    attach_branding
    mail(
      subject: "AgreementShield: Return Status Update VNUM: #{@return_certificate.vnum} - Audit Complete",
      to: return_invoice.audit_emails,
      cc: [Astro::SUPPORT_EMAIL, Astro::AUDIT_EMAIL],
      bcc: Astro::ACCOUNTING_EMAIL,
      importance: 'High'
    )
  end
end
