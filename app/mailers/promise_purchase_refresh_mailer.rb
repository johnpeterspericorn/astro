# encoding: utf-8
class PromisePurchaseRefreshMailer < ApplicationMailer
  default from: Astro::PROTECTED_EMAIL

  def refresh_confirmation(promise_offer)
    @promise_offer = promise_offer.decorate

    attach_branding
    mail subject: I18n.t('promise_purchase.market_protect_confirmation.subject', vnum: @promise_offer.vnum), to: promise_offer.emails
  end
end
