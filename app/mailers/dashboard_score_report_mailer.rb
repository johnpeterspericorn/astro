# encoding: utf-8
class DashboardScoreReportMailer < ApplicationMailer
  helper :application
  default from: Astro::NO_REPLY_EMAIL

  def report(params, subject: I18n.t('dashboard_score.email.subject'))
    @dashboard_scores_csv = EmailDashboardScoreReport.new(params).dashboard_scores_csv.to_s
    attachments["dashboard#{Time.zone.today}.csv"] = { mime_type: 'text/csv', content: @dashboard_scores_csv }
    mail subject: subject, to: params[:dashboard_score][:emails]
  end
end
