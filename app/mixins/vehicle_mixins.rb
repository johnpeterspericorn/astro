# encoding: utf-8
# These mixins are for things things that quack like vehicles.
# Currently these things are PresaleVehicle, SoldVehicle, and PromiseOffer
#
# At this point PresaleVehicle, SoldVehicle, and PromiseOffer have an integer
# column "ineligibility_condition" which determines if a vehicle is eligible for
# purchase of promises on them.  If not, there is an associated textual reason
# (which is mostly a view concern.
module VehicleMixins
  module DomainConcerns
    def eligible?
      !ineligible?
    end

    def ineligible?
      ineligibility_condition.present? && ineligibility_condition != 0
    end
  end

  module ViewConcerns
    def full_description
      "#{model.year} #{model.make} #{model.model}".titleize
    end

    def ineligibility_reason
      IneligibilityCondition.notice_for_condition(source.ineligibility_condition)
    end

    def vehicle_code
      I18n.t('pricing_schedules.vehicle_codes')[source.vehicle_code] || ''
    end

    def promise_price_threshold
      currency(model.promise_price_threshold)
    end

    def location_name
      pawn_location.try(:name) || 'N/A'
    end
  end
end
