# encoding: utf-8
require 'date_utils'

module ProtectKind
  attr_accessor :purchase

  def initialize(promise_purchase)
    @purchase = promise_purchase
  end

  def within_window?
    Time.now.between?(min_return_time, max_return_time)
  end

  def min_return_time
    purchase_date.end_of_day
  end

  def max_return_time
    promise_expiration_date
  end

  private

  def purchase_date
    purchase.promised_at
  end

  def extended_days
    purchase.extension_days || 0
  end
end
