# encoding: utf-8
module EnsureAuthToken
  def self.included(base)
    base.before_save :ensure_authentication_token
  end

  def reset_authentication_token!
    self.authentication_token = generate_authentication_token
    save!
  end

  def ensure_authentication_token
    self.authentication_token = generate_authentication_token if authentication_token.blank?
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless self.class.where(authentication_token: token).first
    end
  end
end
