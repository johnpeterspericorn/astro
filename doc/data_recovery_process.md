# Data Recovery Process for Astroheim's Astro Application

## Background

The Astro application (known publicly as 100BBG - Buy Back Promise) is used by
Astroheim to sell promises on vehicles sold at pawn.  The infrastructure
currently exists within Astroheim's data-centers but are not managed by Astroheim
IT.  Instead it is managed by developers from BigNerdRanch who are developing
the application.  While those developers have some experience with DevOps, and
are using the Chef infrastructure, their process differs radically from Astroheim
IT's process.  The goal of this document is to provide new developers and
Astroheim IT with an understanding of how to recover servers from a major outage.

## Provisioning Servers with Chef

### Ruby version management
Ruby on the Astro servers is managed by [`RVM`](https://rvm.io/).  During
initial provisioning the default Ruby installation (Ruby Enterprise Edition or
REE) is removed from the servers and replaced with Ruby 1.8.7 from RPM.  With
that we configure Chef to execute which installs RVM and our preferred Ruby to
be used with it.

To check the version of Ruby currently in use simply check `ruby -v`. If
it is not 1.9.3 then please use rvm to change to it via `rvm 1.9.3`.  If, for
some reason you need the system's ruby instead (which has been changed to 1.8.7
from the default REE) simply execute `rvm system`.

### Installing and setting up Chef

The repository in which our Chef cookbooks reside is in the Astroheim
organization on Github: [chef](https://github.com/Astroheim/chef).
Configuration of Chef and provisioning of hosts is documented in the `README.md`
contained within that repository.

## Validating the database server configuration

The Astro application relies on an instance of PostgreSQL running on it's
associated database server.  On the database server first verify that the
Postgres daemon is running with `service(8)` via

```
[deploy@mdx-dc3prod2-db01 ~]$ service postgresql status
postmaster (pid  22808) is running...
```

To verify that the Astro application itself can connect to the server begin with
checking that the `psql` command-line utility can connect to the database server
as username 'astro' via `psql -h mdx-dc3prod2-db01.ove.local -U astro`.  This
verifies the authentication configuration of Postgres on the database server
itself.  If this fails edit the `pg_hba.conf` file on the database server, as
the postgres user.  There should be a row similar to the following with the IP
address of the web-server in place:

```
# TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD
host    astro       astro       10.141.65.87/32       trust
```

Also, make sure that the postgres daemon is binding to all IP addresses by
inspecting the configuration within `postgresql.conf` (which is well self
documented).  The `list_addresses` configuration option should be set to `*`.

After making any changes to the configuration do not forget to restart the
Postgres service in order to reload configurations.

Finally, to verify that the Rails components of Astro are able to communicate to
the database server use a rails server session and execute a query such as the
following session shows.

```
ssh astro_prod_www
Last login: Mon Jun 10 12:38:02 2018 from 10.141.101.4
Kickstarted on 2018-11-12
[deploy@mdx-dc3prod2-web01 ~]$ cd /var/www/current/
[deploy@mdx-dc3prod2-web01 current]$ script/rails c
SoldVehicle.count
Loading production environment (Rails 3.2.13)
1.9.1 :001 > SoldVehicle.count
   (4.6ms)  SELECT COUNT(*) FROM "sold_vehicles"
 => 1063
```

Note that if the database server has had to be rebuilt and does not yet contain
data the number returned may be zero instead of the abvoe 1063. If there is no
error from the query itself the database server is configured as expected.

## Validating the Web Server configuration

### Webserver configuration notes

Astro's production webservice is terminated on an F5 load balancer within
Astroheim's infrastructure.  It terminates SSL for the application and proxies all
requests to the Astro application itself.  The load-balancer automatically
redirects all HTTP requests to HTTPS requests on the load-balancer itself, but
then proxies all requests over HTTP to Astro.  Astro's webserver is
[`nginx`](http://nginx.org/)+[`unicorn`](http://unicorn.bogomips.org/).  Unicorn
does the heavy lifting of actually servnumg requests while ngnix proxies requests
to Unicorn directly.  This configuration is lifted from GitHub's architecture as
documented [here](https://github.com/blog/517-unicorn).

### Service management with God
A Ruby service called [`god`](http://godrb.com/) is used to monitor services and
restart them if they fail.  It's configuration lives in `/etc/god`

To check the current status of services managed by `god` execute `god status`
and you should see output similar to the following:

```txt
[root@mdx-dc3prod2-web01 ~]# god status
delayed_job: up
unicorn: up
```

Each service `god` manages is configured with its own file in `/etc/god/conf.d`.
So, for instance unicorn is configured by `/etc/god/conf.d/unicorn.god`.  The
god configuration file specifies all execution parameters of the application
being run via its Ruby DSL.  Please note that if you must change parameters in a
God configuration you must instruct it to reload its configuration files before
restarting the application itself with god will be aware of those changes.  So,
for instance, if you change the location of the PID file in Unicorn's
configuration you must execute the following steps to attempt to restart unicorn
using `god`:

```
[deploy@mdx-dc3prod2-web01 current]$ sudo su -
[root@mdx-dc3prod2-web01 ~]# vim /etc/god/conf.d/unicorn.god
[root@mdx-dc3prod2-web01 ~]# /etc/init.d/god reload
Using /usr/local/rvm/gems/ruby-1.9.3-p327
Sending 'load' command with action 'leave'

The following tasks were affected:
  unicorn
  delayed_job
[root@mdx-dc3prod2-web01 ~]# god status
delayed_job: up
unicorn: up
```

Now you may execute `god` commands such as `god restart unicorn`.

### Verifying that nginx is running

Using `service nginx status` you should be able to query the
status of nginx.  If it is not started, start it using `service nginx start` as well.  If
it fails to start investigate by checking its log files.  If nginx is running
but Unicorn is not then you will see a `502 Bad Gateway` error.

### Verifying that Unicorn is running.

You can verify that unicorn is running via `god status` or by checking the
output of `ps`.  Checking `ps` output you should see one process listed as
master and 8 (though this is configurable) child workers:

```
[root@mdx-dc3prod2-web01 ~]# ps ax | grep -i unicorn
 1429 ?        Sl     6:34 unicorn master -c /var/www/current/config/unicorn.rb -E production -D
26226 ?        Sl     0:00 unicorn worker[4] -c /var/www/current/config/unicorn.rb -E production -D
26229 ?        Sl     0:00 unicorn worker[2] -c /var/www/current/config/unicorn.rb -E production -D
26232 ?        Sl     0:00 unicorn worker[5] -c /var/www/current/config/unicorn.rb -E production -D
26235 ?        Sl     0:00 unicorn worker[0] -c /var/www/current/config/unicorn.rb -E production -D
26238 ?        Sl     0:01 unicorn worker[1] -c /var/www/current/config/unicorn.rb -E production -D
26241 ?        Sl     0:01 unicorn worker[3] -c /var/www/current/config/unicorn.rb -E production -D
26244 ?        Sl     0:00 unicorn worker[6] -c /var/www/current/config/unicorn.rb -E production -D
26247 ?        Sl     0:00 unicorn worker[7] -c /var/www/current/config/unicorn.rb -E production -D
```

If you see fewer or you see them constantly restarting it means they are not
able to boot the rails environment.  Check the log files (their path is
specified in the unicorn `god` configuration but likely points to
`/var/www/shared/log/unicorn.std(err|out).log`)

If you've verified that you can connect to the database server in a Rails
console above, and both nginx and unicorn are running, then you should be able
to hit the application itself via `https://promise.agreementshield.com`
