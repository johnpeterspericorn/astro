## Authentication

The authentication model in Astro is a hybrid between authenticating against
Astroheim's Vicki authentication service for remote users (`BuyerUsers`) and locally
authenticated users.  The latter are `KioskUsers` and `SuperUsers`,
database-authenticated users. `KioskUser` are used by Astroheim
employees to login via the AstroApp native iOS application.  The flow through
the application will vary based on which type of user is logged in as
`BuyerUsers` should never have to enter their Pawn Access #s (we retrieve
them from the Astroheim Authorization service) while `KioskUsers` will be
presented with the Pawn Access # swipe workflow, and `SuperUsers` will be
able to enter Pawn Access #s, VNUMs, and 5m#s as
appropriate.

The login workflow takes credentials from the standard Devise session path
`users/sign_in` and first attempts to authenticate against the Astroheim
authentication service.  If it succeeds the user is logged in as a Astroheim user.
If that fails it then attempts local database authorization for the
username/password pair.  If that succeeds the user is logged in as a SuperUser
or KioskUser (in that order).  If it still fails then the user is not logged in.
This logic required a custom Devise authentication strategy implemented in
`lib/devise_hybrid_authenticatable.rb` along with non-standard Devise routes.

VickiUsers may also be "promoted" from a BuyerUser to a Super or
(later) Pawn user.  This promotion occurs after authenticating against Vicki.
If the authentication is successful then the local database is queried for a
`SuperUser` with the mase username.  If such a user is found then that user is
instead logged into the application.
