# AutoIMS Integration Tests

The AutoIMS webservice can only be reached by whitelisted IPs. To re-record
HTTP requests, you must make requests from a whitelisted IP. We accomplished
this by tunnelling a connection through the production webserver:

```sh
ssh -L 4430:wstls.autoims.com:443 astro_prod
```

## Environment

This allows requests to `localhost:4430` to be fordwarded transparently through
the SSH connection and appear to come from the remote server from AutoIMS'
perspectively.

You must update your environment to specify
`AUTOIMS_URL=localhost:4430/ClientWebservice`.

## Certificate Verification
Because this is a secure request it is encrypted with a certificate. Since the
issued certificate is for the AutoIMS service, it will be seen as invalid by
our HTTP library. To get around this, we must temporarily disable host
verification. Add the following to the [request options](https://github.com/Astroheim/astro/blob/f02c27a8b6a68b72577b0e11171757a63af8eeab/app/models/autoims/request.rb#L25-L28):

```diff
  method: request_method,
  body: body,
  params: params,
- headers: { Accept: "text/xml" }
+ headers: { Accept: "text/xml" },
+ ssl_verifyhost: 0,
```

## VCR

Finally, in order to record VCR cassettes for requests to `localhost` you need
to commend out the `ignore_localhost` option in the [spec helper](https://github.com/Astroheim/astro/blob/f02c27a8b6a68b72577b0e11171757a63af8eeab/spec/spec_helper.rb#L87):

```diff
- config.ignore_localhost = true
+ # config.ignore_localhost = true
```

## "This sucks!"

Yep, but it's a constraint of working with this webservice. Would _love_ to see
it improved!
