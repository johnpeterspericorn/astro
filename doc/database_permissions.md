Granting New Database Permissions to Astroheim MAS Group
---

To edit permissions of the MAS database user, do the following:

1. Edit the `MAS_WRITE_TABLES` constant at the beginning of the rake file:
`lib/tasks/database_permissions.rake`

2. Login to the web server for the environment whose permissions you'd like to change and run the rake task:

```
bundle exec rake db:grant_permissions
```
