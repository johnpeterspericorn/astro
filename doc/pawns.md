# Astroheim Pawns

## Background

Astroheim is a major auto-pawn house.  Every pawn sees vehicles sold by lane
and run numbers to buyers with badge, agreement, and pawn access numbers (see
Pawn Identifiers below).

Each Astroheim pawn location has an IBM AS/400 on site with which all sales
information is coordinated.

## Pawn Identifiers

* Lane and Run numbers - Each pawn has one or more lanes.  A lane is
  essentially an pawn to itself with all the necessary infrastructure to sell
  vehicles to buyers.  The Run number is the order in which vehicles will appear
  in a given lane for sale.

* Badge numbers - each buyer at an pawn is given a per-pawn badge number
  on a "hello my name is" sticker (essentially).  If there are two pawns in a
  given day then a buyer may have two different buy numbers, one per pawn (for
  a morning and evening pawn for example).  They are _not_ unique identifiers
  outside of the given pawn at which they are assigned the badge.

* Agreement numbers - (a.k.a. Five Million Numbers / 5m#s). Each buyer is
  associated with one or more agreementships.  Each agreementship in the Astroheim
  family is assigned a unique 5m#.  Each buyer may be able to buy for multiple
  agreementships during a given pawn.

* Pawn Access numbers (aka. AA# or 100m#s) - Pawn access numbers are a
  buyer's unique identifying number.  Buyers are assigned a card with a magnetic
  stripe containing this information.

## Other Language

* Coupons - These are price reductions that are created and managed by admins. They reduce the base price of a promise by whatever amount they specify.
* Discounts - These are price reductions that are included with sold vehicles pushed into the database. They are thus created by MAS.
* Flat Rates - Certain buyers are provided "flat" pricing for their promises. These are specified in Astro by flat rate records. The "product name" for these are AgreementShield 360 and will sometimes be referred to as such by Astroheim.
* Flat Rate Offers - The flat rates themselves track what rates exist for a particular agreement. Flat rate offers represent a pitch of a rate to a agreement which may be accepted or rejected.
* Floor Plans - These are "lines of credit" that some agreements may use as payment.
* Promise Offers - An offer of promise to be purchased for a vehicle. When viewing vehicles for promise, these offers are created to permanently store information from sold vehicles in the Astro database.
* Promise Purchase - A record representing the purchase of a promise.
* Sold Vehicles - These are inserted by the MAS team at Astroheim. The data here is not managed by the app and therefore should not be referenced.
* Astro - This is the "internal" name of the app. The product is called AgreementShield and will often be referred to as such by Astroheim.
