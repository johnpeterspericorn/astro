Setting up VPN Access
---

Paste this URL into browser:
(https://vpn.ove.com/+CSCOE+/logon.html)
Enter the credentials with group set to OVE-ADMIN

This should install a Cisco client applet for VPN access, where you enter the
mase info as on the site (again with OVE-ADMIN) to connect. If the download
succeeds but installation fails you may need to adjust to your application
download settings. In System Preferences\General\Allow apps downloaded from,
select "anywhere"

After that, you should be able to hit http://10.141.163.233/ and see the astro
app.