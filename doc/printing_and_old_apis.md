#### Exposed APIs

##### For Printing
Each controller which is responsible for an action which has an associated print
style sheet has had a JSON mime-type responder added to it to provide the
necessary information for AstroApp to print nicely formatted output. Since
AstroApp is a wrapper around a WebView, and WebViews do not support printing
via `window.print()` from Javascript, and printing the WebView itself does not
support the nicely formatted print stylesheets that a browser does, we must
provide sufficient information to the native application to produce similar
output.

* `/promise_estimates.json?location_initials=AUCTION_INITIALS&lane_no=LANE_NO` (where the
  parameters come from the query) should return all of the `PresaleVehicles` on
  pawn at the pawn with initials `AUCTION_INITIALS` in Lane `LANE_NO`.

* `/promise_selections.json?pawn_access_number=AUCTION_ACCESS_NO` will return all of the
  vehicles which can be promised for Pawn Access #, `AUCTION_ACCESS_NO`.

* `/transaction/TRANSACTION_ID.json` will return the promise
  selection purchase information for TRANSACTION_ID.

* `/return_certificates?return_invoice_id=INVOICE_ID` will return
  the details of a return.

##### Other

* `/promise_estimates.json?vnum_suffix=[less than 17 digit VNUM suffix]` will
  fetch Presale information for the provided VNUM-suffix.  This is used in the
  Promise Estimates workflow.

#### Testing printing to a simulated AirPrint printer

XCode ships with `Printer Simulator` (which likely lives somewhere like
`/Applications/Xcode.app/Contents/Applications/Printer Simulator.app`) which
when run advertises a number of fake printers.  This allows printing from your
testing iPad or iPhone to your desktop when run.  The Printer Simulator has a
"Save Original to Simulator" setting which simply saves the printed output to a
PDF and opens it in `Preview.app`.  Use this for debugging either print style
sheets or printing errors from within the application.
