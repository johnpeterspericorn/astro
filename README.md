# Astroheim astro


## Description

Astro is an application which allows representatives at Astroheim auto pawns to receive quotes on and purchase buy-back promises for vehicles. Users can also return vehicles on which they have previously purchased promises. The application includes both a kiosk interface (the web views in this Rails app) and an iOS client (which accesses the app via its JSON API).

## Getting Started

### Requirements

1. Ruby 2.3.0
2. PostgreSQL (e.g., [Postgres.app](http://postgresapp.com/))

### Setup


```
git clone https://johnpeterspericorn@bitbucket.org/johnpeterspericorn/astro.git
cd astro
bin/bootstrap
```

### Testing

```
bin/rspec
```

### JavaScript Unit Tests
```
grunt jasmine
# or run full-suite and JS tests
bundle exec rspec && grunt jasmine
```

### Running Astro

```
foreman start
```


