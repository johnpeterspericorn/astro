module.exports = function(grunt) {
  grunt.initConfig({
    jasmine: {
      astro: {
        src: 'app/assets/javascripts/lib/**/*.js',
        options: {
          specs: 'spec/javascripts/lib/**/*Spec.js',
          vendor: 'vendor/assets/javascripts/jquery-1.8.2.min.js'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.registerTask('default', 'jasmine');
};
